<!---Deprecated, not used anymore --->
<cfimport prefix="k4yalerts" taglib="/sis_modules/Alert/AlertPL/ctags">
<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<style type="text/css">
		div.tbl_toolbar {
			width: 350px !important;
			float: right;
			padding: 5px;
			text-align: right;
		}
		tr.group-row td {
			background: ##ddd !important;
			font-size:12px;
			font-weight:bold;
		}
		.highlight {
			background-color: yellow;
		}
		.original-letter-bullet {
			color:blue;
			font-size:13px;
		}
		.revised-letter-bullet {
			color:orange;
			font-size:13px;
		}
		.legend-control {
			margin-right:10px;
		}
		.loa-config .modal-body {
			overflow: visible;
		}
		table##lettersTbl {
			width: 100% !important;
		}
		.ts-select {
		    margin: 0px 3px 0px 0px;
		    position: relative;
		    top: -1px;
		}
	</style>
	<cfif attributes.renderMessage>
		#MsgBox.renderIt(clearMessage=true, header=true)#
	</cfif>
	<div id="submit-on-behalf-frm" style="display:none;"></div>
	<div class="row-fluid">
		<div class="head clearfix">
			<div class="isw-list"></div>
			<h1><k4y:text dictKey="lbl.LETTERS">Letters</k4y:text></h1>
			<cfif stdTrainingSessions.recordCount GT 0>
				<div class="pull-right">
					<form id="letterSessionFilter" action="#request.app.self#?fuseaction=#XFA.self#" method="POST">
						<div class="ts-label">
							Session:
						</div>
						<div class="ts-select" style="width:100px">
							<k4y:multiselect
								name="trainingSessionCD"
								id="filterSessionCD"
								query="stdTrainingSessions"
								keyField="TRAINING_SESSION_CD"
								descField="TRAINING_SESSION_NAME"
								selectValues="#attributes.trainingSessionCD#"
								allowNA=false
								single=true
							/>
						</div>
					</form>
				</div>
			</cfif>
		</div>
		<div class="block-fluid table-sorting clearfix no-marg-bot">
			<k4yalerts:alert metadata='{"STUDENT_ID": "#studentID#", "TRAINING_SESSION_CD": "#attributes.trainingSessionCD#"}' id="letter_alerts" category="#attributes.alertCategories#">
			<table class="table" id="lettersTbl">
				<thead>
					<tr>
						<th>Letter Instance</th>
						<th width="200">Name</th>
						<th>Details</th>
						<th width="150" style="text-align:center">Generated</th>
						<th width="70" style="text-align:center">Status</th>
						<th width="110" style="text-align:center">Action</th>
					</tr>
				</thead>
				<tbody>
					<cfset doaInstance = "">
					<cfloop query="docQry">
						<tr>
							<td>#docQry.INSTANCE_NAME#</td>
							<td>
								<cfif docQry.DOC_TYPE eq "Original"><span class="ico ico-circle original-letter-bullet"></span><cfelse><span class="ico ico-circle revised-letter-bullet"></span></cfif>&nbsp;
								<a target="_blank" <cfif docQry.DOC_PROOF_FILE_CD neq "">href="javascript:;" style="text-decoration: none;"<cfelse>style="text-decoration: underline;"  href="#request.app.myself##XFA.view#&instance_id=#docQry.INSTANCE_ID#&doc_snapshot_cd=#docQry.DOC_SNAPSHOT_CD#&isDocument=1"</cfif>>#docQry.LETTER_TEMPLATE_NAME#</a>
								<cfif docQry.DOC_PROOF_FILE_CD neq "">
									<k4y:downloadFile fileCD="#docQry.DOC_PROOF_FILE_CD#" showIcon="true" showFileName="true" />
								</cfif>
							</td>
							<td>
								#Replace(docQry.DOCUMENT_DESC, chr(10) & chr(13), "", "all")#<cfif docQry.ENABLE_PUBLISH EQ "Y">, Letter Type: #docQry.DOC_TYPE#</cfif>
							</td>
							<td style="text-align:center">
								#DateFormat(docQry.GENERATION_DT, medsis_date_format)#</br>
								#docQry.GENERATED_BY#
							</td>
							<td style="text-align:center">
								<cfif docQry.ENABLE_PUBLISH EQ "Y">
									<cfswitch expression="#UCase(docQry.PUBLISHED_STATUS)#">
										<cfcase value="PUBLISHED">
											<span class="label" style="background-color: ##468847;">#docQry.PUBLISHED_STATUS#</span></br>
											<span class="date-container">#DateFormat(docQry.DOC_PUBLISHED_DT, medsis_date_format)#</span>
										</cfcase>
										<cfcase value="NOT PUBLISHED">
											<span class="label" style="background-color: ##990000;">#docQry.PUBLISHED_STATUS#</span>
										</cfcase>
									</cfswitch>
								<cfelse>
									<span class="label">Not Publishable</span>
								</cfif>
							</td>
							<td style="text-align:center">
								<cfif docQry.ACTION_CODE EQ "AGREE">
									<cfif docQry.PUBLISHED_STATUS eq "PUBLISHED">
										<cfswitch expression="#UCase(docQry.SIGNED_STATUS)#">
											<cfcase value="SIGNED">
												<cfif docQry.DOC_PROOF_FILE_CD neq "">
													<span class="label" style="background-color: ##468847;">Signed [Signature proof]</span><br/>
												<cfelse>
													<span class="label" style="background-color: ##468847;">#docQry.SIGNED_STATUS#</span><br/>
													<span class="date-container">#DateFormat(docQry.DOC_SUBMITTED_DT, medsis_date_format)#</span>
												</cfif>
											</cfcase>
											<cfcase value="NOT SIGNED">
												<span class="label" style="background-color: ##990000;">#docQry.SIGNED_STATUS#</span><br/>
												<a onclick="showSubmitOnBehalfForm(#docQry.student_id#,#docQry.doc_snapshot_cd#)" href="javascript:;">Submit on Behalf</a>
											</cfcase>
										</cfswitch>
									</cfif>
								</cfif>
							</td>
						</tr>
					</cfloop>
				</tbody>
			</table>
		</div>
	</div>

	<cfif XFA.generate neq "">
		<div id="loaDialogContent" style="display: none;">
			<div class="row-fluid" style="padding: 10px 0;">
				<div class="row-form clearfix no-border">
					<div class="span12"><k4y:text>Please select the letter type you want to generate</k4y:text></div>
				</div>
				<div class="row-form clearfix no-border">
					<div class="span2"><k4y:label required="true">Letter Type</k4y:label></div>
					<div class="span10">
						<k4y:multiSELECT
							name="instance_id"
							id="instanceId"
							allowNa="false"
							keyfield="INSTANCE_ID"
							descField="INSTANCE_NAME"
							url="#request.app.self#?fuseaction=#XFA.getInstances#"
							selectValues="#instanceSelectValues#"
							selectDescriptions="#instanceSelectDescriptions#"
							maxHeight="150"
							single="true"
							onchange="onLetterInstance()"
						/>
					</div>
				</div>
				<div class="extra-filters"></div>
			</div>
		</div>
	</cfif>

	<script type="text/javascript">
		var bGenerateInit = false;
		showSubmitOnBehalfForm = function(studentID,docSnapshotCD) {
			$('##submit-on-behalf-frm').load('#request.app.self#?fuseaction=#XFA.SubmitOnBehalfForm#&student_id=' + studentID + '&doc_snapshot_cd=' + docSnapshotCD).show();
		}
		cancelSubmitOnBehalf = function() {
			$('##submit-on-behalf-frm').html('').hide();
		}
		<cfif XFA.generate neq "">
			onLetterInstance = function() {
				$('##generateLetterBtn').attr('disabled', true);
				var instanceId = getMultiselectValues('instanceId', 'values').join(',');
				if (instanceId === '') {
					$('.extra-filters').empty();
				} else {
					$('.extra-filters').load('#request.app.myself##XFA.getLetterTypeFields#&student_id=#studentID#&training_session_cd=#attributes.trainingSessionCD#&instance_id=' + instanceId);
				}
			}
		</cfif>

		$(function() {
			<cfif StructKeyExists(url, "view_doc_snapshot_cd") and isNumeric(url.view_doc_snapshot_cd)>
				var previewUrl = '#request.app.myself##XFA.view#&isDocument=true';
				var previewParams = '&instance_id=#attributes.instance_id#&doc_snapshot_cd=#url.view_doc_snapshot_cd#';
				window.open(previewUrl + previewParams, '_blank');
				window.history.replaceState({}, document.title, '#request.app.myself##XFA.self#&trainingSessionCd=#attributes.trainingSessionCD#');
			</cfif>
			$('##filterSessionCD').on('change', function(){
				$('##letterSessionFilter').submit();
			});
			oTable = $("##lettersTbl").dataTable({
				"sDom": '<"top"f<"tbl_toolbar">>t<"bottom"lpr><"clear">',
				"aaSorting":  [],
				"bPaginate": false,
				"bStateSave": true,
				"aoColumns": [
					{ "bVisible": false},
					{ "bSortable": false },
					{ "bSortable": false },
					{ "bSortable": false },
					{ "bSortable": false },
					{ "bSortable": false }
				 ],
				"fnDrawCallback": function ( oSettings ) {
					if ( oSettings.aiDisplay.length == 0 ) {
						return;
					}
					var nTrs = $('##lettersTbl tbody tr');
					var iColspan = nTrs[0].getElementsByTagName('td').length;
					var sLastGroup = "";
					for ( var i=0 ; i<nTrs.length ; i++ ) {
						$(nTrs[i]).unhighlight().highlight($('##lettersTbl_filter input').val());
						var iDisplayIndex = oSettings._iDisplayStart + i;
						var sGroup = oSettings.aoData[ oSettings.aiDisplay[iDisplayIndex] ]._aData[0];
						if ( sGroup != sLastGroup )
						{
							var nGroup = document.createElement( 'tr' );
							nGroup.className = 'group-row';
							var nCell = document.createElement( 'td' );
							nCell.colSpan = iColspan;
							nCell.innerHTML = sGroup;
							nGroup.appendChild( nCell );
							nTrs[i].parentNode.insertBefore( nGroup, nTrs[i] );
							sLastGroup = sGroup;
						}
					}
				}
			});

			<cfif XFA.generate neq "">
				$("div.tbl_toolbar").append('<div class="legend-control"><span class="ico ico-circle original-letter-bullet"></span>&nbsp;Original&nbsp;&nbsp;<span class="ico ico-circle revised-letter-bullet"></span>&nbsp;Revised</div><button type="button" style="margin-right: 5px;" class="btn btn-mini" id="btnGenerate"><span class="ico ico-bolt"></span>#moduleConfig.getResource4JS(resource="btn.GENERATE", default="Generate")#</button>');

				docGenerateDialog = new BootstrapDialog({
					cssClass: "loa-config",
					autodestroy: false,
					title: '<span class="ico ico-bolt"></span>Generate Letters',
					closable: true,
					draggable: true,
					message: $('##loaDialogContent .row-fluid'),
					buttons: [{
						id: 'generateLetterBtn',
						icon: 'ico ico-bolt',
						label: 'Generate',
						cssClass: 'btn-mini btn-primary',
						autospin: false,
						action: function(dialogRef) {
							var urlParams = $('.loa-config input:hidden').serialize();
							$('##generateLetterBtn').attr('triggered-text', 'Generating...');
							setButtonState($('##generateLetterBtn'), 'triggered');
							$.post('#request.app.myself##XFA.generate#&' + urlParams, function(jsonResponse) {
								var jsonResponse = $.parseJSON(jsonResponse);
								var redirectUrl = jsonResponse.REDIRECT_URL;
								redirectUrl += '&instance_id=' + $('##instanceId').val();
								if (jsonResponse.STATUS === 'success') {
									var docSnapshotCDs = jsonResponse.DATA.DOC_SNAPSHOT_CD.toString();
									var docSnapshotArr = docSnapshotCDs.split(',');
									if (docSnapshotArr.length == 1) {
										redirectUrl += '&view_doc_snapshot_cd=' + docSnapshotCDs;
									}
								}
								window.location.href = redirectUrl;
							});
						}
					},{
						id: 'closeLetterFilterBtn',
						icon: 'ico ico-times',
						label: 'Close',
						cssClass: 'btn-mini',
						autospin: false,
						action: function(dialogRef) {
							dialogRef.close();
						}
					}],
					onshow: function(dialog) {
						if (!bGenerateInit) {
							dialog.getButton('generateLetterBtn').attr('disabled', true);
						}
					},
					onshown: function(dialog) {
						if (!bGenerateInit) {
							onLetterInstance();
							bGenerateInit = true;
						}
					}
				});

				$('##btnGenerate').on("click", function() {
					docGenerateDialog.open();
				});

				<cfif attributes.initGenerate>
					$('##btnGenerate').trigger('click');
				</cfif>
			</cfif>

			<cfif XFA.publish neq "">
				$("div.tbl_toolbar").append('<button type="button" class="btn btn-mini btn-green btn-primary" id="btnPublish"><span class="ico ico-paper-plane"></span>#moduleConfig.getResource4JS(resource="btn.PUBLISH", default="Publish")#</button>');

				$('##btnPublish').confirmation({
					title: 'Are you sure you want to publish these letters?',
					content: 'Once you publish them, the letters will be made available for the trainees.',
					btnOkLabel: '<span class="ico ico-ok"></span> Yes',
					btnCancelLabel: '<span class="ico ico-cancel"></span> No',
					template: '<div class="popover publis-letter-popover-instance" style="width: 340px;">' +
						'<div class="arrow"></div>' +
						'<h3 class="popover-title"></h3>' +
						'<div class="popover-content text-center">' +
							'<p class="popover-content-inside"></p>' +
							'<a class="btn btn-mini marg-right5" href="" target=""></a>' +
							'<a class="btn btn-mini" data-dismiss="confirmation"></a>' +
						'</div>' +
					'</div>',
					placement: 'left',
					onConfirm: function(ev){
						ev.preventDefault();
						$('##btnPublish').attr('triggered-text', 'Publishing...');
						setButtonState($('##btnPublish'), 'triggered');
						$('##btnPublish').confirmation('hide');
						$.post('#request.app.myself & XFA.publish#', {STUDENT_ID : #studentID#, TRAINING_SESSION_CD: #attributes.trainingSessionCD#}, function(jsonResponse){
							var jsonResponse = $.parseJSON(jsonResponse);
							window.location.href = jsonResponse.REDIRECT_URL;
						});
					},
					onCancel: function(){
						$('##btnPublish').confirmation('hide');
					}
				});
			</cfif>
		});
	</script>
</cfoutput>