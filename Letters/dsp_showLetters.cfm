<!---Deprecated, not used anymore --->
<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfimport prefix="k4yalerts" taglib="/sis_modules/Alert/AlertPL/ctags">
<cfoutput>
	<style type="text/css">
		div.tbl_toolbar {
		    width: 280px !important;
		    float: right;
		    line-height: 16px;
		    padding: 5px;
		    text-align: right;
		}
		.original-letter-bullet {
			color:blue;
			font-size:13px;
		}
		.revised-letter-bullet {
			color:orange;
			font-size:13px;
		}
	</style>
	<cfif attributes.renderMessage>
		#MsgBox.renderIt(clearMessage=true, header=true)#
	</cfif>
	<div class="row-fluid">
		<div class="head clearfix">
			<div class="isw-list"></div>
			<h1><k4y:text dictKey="lbl.LETTERS">Letters</k4y:text></h1>
		</div>
		<div class="block-fluid table-sorting clearfix no-marg-bot">
			<k4yalerts:alert metadata='{"STUDENT_ID": "#studentID#"}' id="letter_alerts" category="#attributes.alertCategories#" matchCategories="#attributes.alertCategoryMatchType#" disableAcknowledgeAction="#attributes.alertDisableAcknowledgeAction#">
			<table class="table" id="lettersTbl">
				<thead>
					<tr>
						<th width="200">Name</th>
						<th>Details</th>
						<th width="120">Received Date</th>
						<th width="200">Status</th>
					</tr>
				</thead>
				<tbody>
					<cfloop query="docQry">
						<tr>
							<td>
			        			<cfif docQry.DOC_TYPE eq "Original"><span class="ico ico-circle original-letter-bullet"></span><cfelse><span class="ico ico-circle revised-letter-bullet"></span></cfif>&nbsp;
								<a target="_blank" <cfif docQry.DOC_PROOF_FILE_CD neq "">href="javascript:;" style="text-decoration: none !important;"<cfelse>style="text-decoration: underline;" href="#request.app.myself##XFA.view#&instance_id=#docQry.INSTANCE_ID#&doc_snapshot_cd=#docQry.DOC_SNAPSHOT_CD#&isDocument=1"</cfif>>#docQry.LETTER_TEMPLATE_NAME#</a>
								<cfif docQry.DOC_PROOF_FILE_CD neq "">
									<k4y:downloadFile fileCD="#docQry.DOC_PROOF_FILE_CD#" showIcon="true" showFileName="true" />
								</cfif>
							</td>
							<td>
								#Replace(docQry.DOCUMENT_DESC, chr(10) & chr(13), "", "all")#<cfif docQry.ENABLE_PUBLISH EQ "Y">, Letter Type: #docQry.DOC_TYPE#</cfif>
							</td>
							<td>#DateFormat(docQry.DOC_PUBLISHED_DT, medsis_date_format)#</td>
							<td>
								<cfif docQry.ACTION_CODE EQ "AGREE">
									<cfif docQry.DOC_SUBMITTED_DT eq "">
										<span class="label" style="background-color: ##990000;">Not Signed</span>
									<cfelse>
										<cfif docQry.DOC_PROOF_FILE_CD neq "">
											<span class="label" style="background-color: ##468847;">Signed [Signature proof]</span><br/>
										<cfelse>
											<span class="label" style="background-color: ##468847;">Signed</span> on <span class="date-container">#DateFormat(docQry.DOC_SUBMITTED_DT, medsis_date_format)#</span>
										</cfif>
									</cfif>
								</cfif>
							</td>
						</tr>
					</cfloop>
				</tbody>
			</table>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
			oTable = $("##lettersTbl").dataTable({
		        "sDom": '<"top"f<"tbl_toolbar">>t<"bottom"lpr><"clear">',
				"aaSorting":  [["2", "desc"]],
				"bPaginate": false,
		        "aoColumns": [
					{ "bSortable": true },
					{ "bSortable": false },
					{ "bSortable": true },
					{ "bSortable": false }
				 ],
				"fnDrawCallback": function ( oSettings ) {
		            if ( oSettings.aiDisplay.length == 0 ) {
		                return;
		            }
		            var nTrs = $('##lettersTbl tbody tr');
		            for ( var i=0 ; i<nTrs.length ; i++ ) {
		            	$(nTrs[i]).unhighlight().highlight($('##lettersTbl_filter input').val());
		            }
		        }
			});
			$("div.tbl_toolbar").append('<div class="legend-control"><span class="ico ico-circle original-letter-bullet"></span>&nbsp;Original&nbsp;&nbsp;<span class="ico ico-circle revised-letter-bullet"></span>&nbsp;Revised</div>');
		});
	</script>
</cfoutput>