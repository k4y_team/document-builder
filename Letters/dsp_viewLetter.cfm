<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
    <link rel='stylesheet' type="text/css" href="#application.cbController.getSetting('modulesApplicationURL')#/modules/AgreementLetters/includes/css/letter.css?v=#application.cbcontroller.getSetting("versionJS_CSS")#" />

	<style type="text/css">
		.info-area .btn.btnSubmit {
			padding: 3px 10px !important;
			background: none;
		}
		.info-area .btn.btnCancel {
			background:##fff;
		}
		.info-area .btn.btnPrint, .info-area .btn.btnClose, .info-area .btn.btnPublish, .info-area .btn.btnLetterMail {
			margin-right: 7px;
			margin-top: 7px;
		}
		.info-area .btn.btnLetterMail {
			margin-top: 7px;
			font-size: 12px;
		}
		.unsigned .head {
			background: ##D6D386 !important;
		}
		.unsigned .instrtuction {
			background: ##F3F0A1;
		}
		.signed .head {
			background: ##8EC590 !important;
		}
		.signed .instrtuction {
			background: ##B7EC9C;
		}
		.red-border {
			border: 1px solid ##CF1800 !important;
		}
		.btn.btn-primary.btn-green.btnSubmit[status=triggered],.btn.btn-primary.btn-green.btnSubmit[status=triggered] * {
			color:white !important;
		}
	</style>

	<div class="smart-doc-details" style="border: none; border-radius: 0px;">
		<div class="smart-doc-name">
			#docQry.LETTER_TEMPLATE_NAME#
		</div>
		<div class="smart-doc-toolbar-actions">
			<cfif XFA.publish neq "" AND docQry.ENABLE_PUBLISH EQ "Y" AND docQry.DOC_PUBLISHED_DT eq "">
				<k4y:button type="button" class="btn btnPublish"  dictKey="Publish Document"></k4y:button>
			</cfif>
		</div>
	</div>

	<cfset docOptions = {
		"doc_config_id" = 'DOC_CONFIG_LETTER_TEMPLATE_#docQry.DOC_LETTER_TEMPLATE_CD#',
		"enable_autosave" = true,
		"enable_log" = true,
		"document" = "#document_options#"
	}>


	<div class="smart-doc">
		#renderedDoc#
	</div>
	<script type="text/javascript">
		$(function() {
			<cfif XFA.publish neq "" AND docQry.ENABLE_PUBLISH EQ "Y" AND docQry.DOC_PUBLISHED_DT eq "">
				$('.btnPublish').confirmation({
					title: 'Publish Letter',
					content: 'By publishing letters, you make them available to users.<br/> Are you sure you want to publish this letter?',
					btnOkLabel: '<span class="ico ico-ok"></span> #moduleConfig.getResource4JS(resource="btn.YES", default="Yes")#',
					btnCancelLabel: '<span class="ico ico-cancel"></span> #moduleConfig.getResource4JS(resource="btn.NO", default="No")#',
					template: '<div class="popover publis-letter-popover-instance" style="width: 340px;">' +
						'<div class="arrow"></div>' +
						'<div class="popover-title"></div>' +
						'<div class="popover-content text-center">' +
							'<p class="popover-content-inside"></p>' +
							'<a class="btn btn-mini marg-right5" href="" target=""></a>' +
							'<a class="btn btn-mini" data-dismiss="confirmation"></a>' +
						'</div>' +
					'</div>',
					placement: 'left',
					onConfirm: function(ev){
						ev.preventDefault();
						$('.btnPublish').attr('triggered-text','Publishing...');
						setButtonState($('.btnPublish'), 'triggered');
							$.ajax({
	           					type: "POST",
								async: true,
	           					url: '#request.app.urlPublicRoot##request.app.myself##XFA.publish#&instance_id=#docQry.INSTANCE_ID#&doc_snapshot_cd=#docQry.DOC_SNAPSHOT_CD#&DOC_LETTER_TEMPLATE_cd=#docQry.DOC_LETTER_TEMPLATE_CD#',
	           					data:'' ,
	           						success: function(response) {
	           							setButtonState($('.btnPublish'),'active');
	           							location.reload();
	           						},
	 								error: function(){
	           							setButtonState($('.btnPublish'),'active');
			    						var data = $.parseJSON(html);
	 								}
	           					});
							$('.btnPublish').confirmation('hide');
					},
					onCancel: function(){
						$('.btnPublish').confirmation('hide');
					}
				});
			</cfif>
		});
	</script>


</cfoutput>