<!--- <cfif NOT fusebox.isCustomTag> --->
	<cfswitch expression="#fusebox.fuseaction#">
		<cfcase value="viewLetter,viewTesLetter,viewSetupLetter">
			<cfset medsis_layout_path = "#fusebox.rootpath#../#GetFileFromPath(ExpandPath('/medsis'))#/layouts" />
			<cfset layoutInnerContent = fusebox.layout />

			<cfset fusebox.layoutFile="#medsis_layout_path#/Layout.SmartDocument.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfdefaultcase>
			<cfset fusebox.layoutFile="">
			<cfset fusebox.layoutDir="">
		</cfdefaultcase>
	</cfswitch>
<!--- <cfelse>
	<cfset fusebox.layoutFile="">
	<cfset fusebox.layoutDir="">
</cfif> --->