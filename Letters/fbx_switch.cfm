<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="showLetters">
		<cfparam name="attributes.XFA_view" default="DocumentLetters.viewLetter" />
		<cfparam name="attributes.renderMessage" default="true" />
		<cfparam name="attributes.alertCategories" default="DISPLAY_ON_LETTERS" />
		<cfparam name="attributes.alertCategoryMatchType" default="ANY" />
		<cfparam name="attributes.alertDisableAcknowledgeAction" default="false" />

		<cfset XFA.view = attributes.XFA_view />
		<cfset studentID = attributes.STUDENT_ID />

		<cfset loaTrainee = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.LOATrainee').init(-1, attributes.student_id) />
		<cfset docQry = loaTrainee.getAllPublished() />

		<cfinclude template="dsp_showLetters.cfm" />
	</cfcase>

	<cfcase value="showAllLetters">
		<cfparam name="attributes.XFA_self" default="DocumentLetters.showAllLetters" />
		<cfparam name="attributes.XFA_view" default="DocumentLetters.viewLetter" />
		<cfparam name="attributes.XFA_publish" default="" />
		<cfparam name="attributes.XFA_generate" default="" />
		<cfparam name="attributes.renderMessage" default="true" />
		<cfparam name="attributes.alertCategories" default="DISPLAY_ON_LETTERS" />
		<cfparam name="attributes.initGenerate" default="false" />
		<cfparam name="attributes.letterInstanceCode" default="" />

		<cfset XFA.submitOnBehalfForm = "DocumentLetters.SubmitOnBehalfForm">
		<cfset XFA.showFiltering = "DocumentLetters.showFiltering"/>
		<cfset XFA.getInstances = "DocumentLetters.getInstances">
		<cfset XFA.getLetterTypeFields = "DocumentLetters.getLetterTypeFields">

		<cfset XFA.publish = attributes.XFA_publish />
		<cfset XFA.generate = attributes.XFA_generate />
		<cfset XFA.view = attributes.XFA_view />
		<cfset XFA.self = attributes.XFA_self />
		<cfset studentID = attributes.STUDENT_ID />

		<cfset externalService = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.ExternalService").init() />
		<cfset stdTrainingSessions = ExternalService.getStdTrainingSessions(attributes.STUDENT_ID) />

		<cfif NOT isDefined("attributes.trainingSessionCD")>
			<cfif isDefined('session.training_session_cd')>
				<cfset trainingSessionCd = session.training_session_cd />
			<cfelse>
				<cfset currentSessionQry = ExternalService.getDefaultTrainingSession() />
				<cfset trainingSessionCd = currentSessionQry.TRAINING_SESSION_CD />
			</cfif>
			<cfif ListFind(ValueList(stdTrainingSessions.TRAINING_SESSION_CD), trainingSessionCd)>
				<cfset attributes.trainingSessionCD = trainingSessionCd />
			<cfelse>
				<cfset attributes.trainingSessionCD = stdTrainingSessions.TRAINING_SESSION_CD />
			</cfif>
		</cfif>
		<cfif attributes.trainingSessionCD EQ "">
			<cfset attributes.trainingSessionCD = -1 />
		</cfif>

		<cfset loaTrainee = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.LOATrainee').init(-1, attributes.student_id) />
		<cfset DocInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocInstances").init() />
		<cfset docQry = loaTrainee.getAllUnpublished(attributes.trainingSessionCD) />

		<cfset instanceQry = DocInstances.getByCode(attributes.letterInstanceCode) />
		<cfset instanceSelectValues = instanceQry.INSTANCE_ID />
		<cfset instanceSelectDescriptions = instanceQry.INSTANCE_NAME />

		<cfinclude template="dsp_showAllLetters.cfm" />
	</cfcase>

	<cfcase value="viewLetter">
		<cfparam name="attributes.XFA_sign" default="DocumentLetters.signLetter" />
		<cfparam name="attributes.XFA_download" default="DocumentLetters.downloadLetter" />
		<cfparam name="attributes.XFA_back" default="DocumentLetters.showLetters" />

		<cfset XFA.sign = attributes.XFA_sign />
		<cfset XFA.back = attributes.XFA_back />
		<cfset XFA.generateAttachments = "DocumentLetters.generateAttachments" />

		<cfif isDefined("attributes.XFA_close")>
			<cfset XFA.close = attributes.XFA_close />
			<cfif XFA.close neq XFA.back>
				<cfset XFA.close = XFA.back />
			</cfif>
		</cfif>

		<cfif isDefined("attributes.XFA_publish")>
			<cfset XFA.publish = attributes.XFA_publish />
		<cfelse>
			<cfset XFA.publish = "" />
		</cfif>
		<cfif isDefined("attributes.XFA_download")>
			<cfset XFA.download = attributes.XFA_download />
		<cfelse>
			<cfset XFA.download = "" />
		</cfif>

		<cfif isDefined("attributes.pdf") and attributes.pdf EQ "1">
			<cfset renderType ="PDF">
		<cfelse>
			<cfset renderType ="HTML">
		</cfif>

		<cfif isDefined("attributes.doc_letter_template_cd")>
			<cfset DocRenderer	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocRender').init(attributes.instance_id)>
			<cfset renderedDoc 	= DocRenderer.dummyLOA(attributes.doc_letter_template_cd)>
			<cfset DocTemplate = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager').init(attributes.instance_id) />
			<cfset docQry = DocTemplate.getTemplate(attributes.doc_letter_template_cd) />
			<cfset document_options = {"filename" = "#Replace(docQry.LETTER_TEMPLATE_CODE," ","_","all")#","page" = {"orientation" = "landscape"} }>
			<cfset preview = true />
			<cfset readonly = true />
			<cfset isStdPreview = false />
		<cfelse>
			<cfset DocRenderer	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocRender').init(attributes.instance_id, 'HTML') />
			<cfset renderedDoc 	= DocRenderer.renderLOA(attributes.doc_snapshot_cd) />

			<cfset DocInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocInstances").init() />
			<cfset doaInstanceQry = DocInstances.getInstanceInfo(attributes.instance_id) />

			<cfset loaTrainee = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.LOATrainee').init(-1, attributes.student_id) />
			<cfset docQry = loaTrainee.getLOAInfo(attributes.doc_snapshot_cd) />
			<cfset document_options = {"filename" = "#docQry.LETTER_TEMPLATE_CODE#","page" = {"orientation" = "landscape"} }>

			<cfset readonly = true />
			<cfset preview = true />
			<cfset isStdPreview = false />

			<cfif docQry.recordCount eq 0>
				<cfset docQry = loaTrainee.getUnpublishedLOAInfo(attributes.doc_snapshot_cd) />
				<cfset preview = true />
				<cfset isStdPreview = true />
			<cfelseif isDefined("attributes.stdPreview")>
				<cfif docQry.DOC_SUBMITTED_DT eq "">
					<cfset preview = true />
					<cfset isStdPreview = true />
				</cfif>
			<cfelse>
				<cfif docQry.DOC_SUBMITTED_DT eq "">
					<cfset readonly = false />
				</cfif>
			</cfif>
		</cfif>
		<cfset userSessionData = application.wirebox.getInstance(dsl='coldbox:plugin:SessionStorage' ).getVar("LoggedInUser")>

		<cfinclude template="dsp_viewLetter.cfm" />
	</cfcase>
	<cfcase value="viewSetupLetter">
       	<cfset DocSetup = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSetup" ).init()/>
		<cfset docSetupQry = DocSetup.getDocSetupByPk(attributes)>
		<cfset XFA.publish = "" />
		<cfset DocTemplate = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager').init(docSetupQry.instance_id) />
		<cfset docQry = DocTemplate.getTemplate(docSetupQry.doc_letter_template_cd) />
		<cfset docTypeRender = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocTypeRender").init(docSetupQry.instance_id,'',docSetupQry.DOC_LETTER_TEMPLATE_CD)/>
		<cfset renderedDoc  = docTypeRender.renderSetupDocument(attributes)>
		<cfset document_options = {"filename" = "#Replace(docQry.LETTER_TEMPLATE_CODE," ","_","all")#","page" = {"orientation" = "landscape"} }>
		<cfinclude template="dsp_viewLetter.cfm" />
	</cfcase>

	<cfcase value="viewTesLetter">
		<cfparam name="attributes.XFA_sign" default="DocumentLetters.signLetter" />
		<cfparam name="attributes.XFA_back" default="DocumentLetters.showLetters" />

		<cfset XFA.sign = attributes.XFA_sign />
		<cfset XFA.back = attributes.XFA_back />
		<cfset XFA.generateAttachments = "DocumentLetters.generateAttachments" />

		<cfif isDefined("attributes.XFA_close")>
			<cfset XFA.close = attributes.XFA_close />
			<cfif XFA.close neq XFA.back>
				<cfset XFA.close = XFA.back />
			</cfif>
		</cfif>

		<cfif isDefined("attributes.XFA_publish")>
			<cfset XFA.publish = attributes.XFA_publish />
		<cfelse>
			<cfset XFA.publish = "" />
		</cfif>
		<cfif isDefined("attributes.XFA_download")>
			<cfset XFA.download = attributes.XFA_download />
		<cfelse>
			<cfset XFA.download = "" />
		</cfif>
		<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
		<cfset DOCSnapshot = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot").init() />
		<cfset snapshotBaseQry = DOCSnapshot.getByPk(attributes.DOC_SNAPSHOT_CD)/>
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset attributes.filter_DOC_LETTER_INSTANCE_CD = snapshotBaseQry.instance_id>
		<cfset attributes.filter_DOC_SNAPSHOT_CD =attributes.DOC_SNAPSHOT_CD>
		<cfset attributes.filter_params = '{}'>
		<cfset filter.initFromStruct(attributes) />
		<cfset docQry = DocReport.getDocuments(filter)>

		<cfif isDefined("attributes.pdf") and attributes.pdf EQ "1">
			<cfset renderType ="PDF">
		<cfelse>
			<cfset renderType ="HTML">
		</cfif>
		<cfset DocRenderer	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocRender').init(attributes.filter_DOC_LETTER_INSTANCE_CD, renderType) />
		<cfset renderedDoc 	= DocRenderer.renderTesLOA(attributes.doc_snapshot_cd) />
		<cfset DocInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocInstances").init() />
		<cfset doaInstanceQry = DocInstances.getInstanceInfo(attributes.filter_DOC_LETTER_INSTANCE_CD) />
		<cfset filename="#docQry.USER_NAME#_#docQry.REPORT_NAME#(#docQry.EVALUATION_PERIOD#)">
		<cfset document_options = {"filename" = "#Replace(REPLACE(filename," ","_","all"),",","_","all")#","page" = {"orientation" = "landscape"} }>
		<cfset readonly = true />
		<cfset preview = true />
		<cfset isStdPreview = false />
		<cfset userSessionData = application.wirebox.getInstance(dsl='coldbox:plugin:SessionStorage' ).getVar("LoggedInUser")>
		<cfinclude template="dsp_viewLetter.cfm" />
	</cfcase>

	<cfcase value="signLetter">
		<cfparam name="attributes.XFA_back" default="DocumentLetters.viewLetter" />

		<cfset XFA.back = attributes.XFA_back />

		<cfset SignFVO = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.SignFVO').init()>
		<cfset docSnapshot = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot').init(attributes.instance_id)>
		<cfset loaData = docSnapshot.get(attributes.doc_snapshot_cd)>

		<cfset signFVO.initFromStruct(attributes)>
		<cfif structKeyExists(attributes, "signature")>
			<cfset bSuccess = docSnapshot.traineeSignOff(SignFVO)>
		<cfelse>
			<cfset bSuccess = docSnapshot.traineeSignOff(SignFVO, false)>
		</cfif>

		<cfif bSuccess>
			<cfset Response.setSTATUS("success")>
			<cfset Response.setREDIRECT_URL(request.app.urlPublicRoot & 'index.cfm?fuseaction=' & XFA.back)>
		<cfelse>
			<cfset Response.setSTATUS("error")>
		</cfif>
		<cfoutput>#Response.generateJSON()#</cfoutput><cfabort>
	</cfcase>

	<cfcase value="downloadLetter">
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset filter.initFromStruct(attributes) />
		<cfset filter.setFILTER_ITEM('PARAMS','{}') />
		<cfset filter.setFILTER_ITEM('DOC_SNAPSHOT_CD',attributes.DOC_SNAPSHOT_CD) />
		<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init(variables.instance_id) />
		<cfset DocReportQry = DocReport.getDocuments(filter)>
		<cfset htmlToPDF = application.wirebox.getInstance("HTMLToPDF@CORE")>
		<cfset generatePDFOptions = {}>
		<cfset PDFfileName = Replace(DocReportQry.USER_NAME," ","","all")&"_" & DocReportQry.DOC_SNAPSHOT_CD &".pdf">
		<cfset generatePDFOptions.fileName = PDFfileName>
		<cfset generatePDFOptions.orientation = "landscape" >
		<cfset pdfContent = htmlToPDF.fromUrl("#application.cbController.getSetting('modulesApplicationURL')#/index.cfm?fuseaction=DocumentLetters.viewTesLetter&instance_id=#DocReportQry.instance_id#&doc_snapshot_cd=#DocReportQry.DOC_SNAPSHOT_CD#&isDocument=1&pdf=true",generatePDFOptions,false)>
		<cfheader name="Set-Cookie" value="fileDownload=true; path=/" />
		<cfheader name="Content-Disposition" value="attachment;filename=#rc.document_filename#.pdf">
		<cfcontent type="application/pdf" variable="#pdfContent#" />
		<cfabort />
	</cfcase>


	<cfcase value="submitOnBehalfForm">
		<cfset XFA.submit = "DocumentLetters.submitOnBehalf">
		<cfset LoaTrainee = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.LOATrainee').init(-1, attributes.student_id)>
		<cfset letterQry = LoaTrainee.getLOAInfo(attributes.doc_snapshot_cd)>
		<cfinclude template="dsp_submitOnBehalf.cfm" />
		<cfabort>
	</cfcase>

	<cfcase value="submitOnBehalf">
		<cfset SignFormVO = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.SignFVO').init()>
		<cfset DocSnapshotService = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot').init(attributes.instance_id)>
		<cfset SignFormVO = SignFormVO.initFromStruct(form)>
		<cfset success = DocSnapshotService.submitOnBehalf(SignFormVO)>
		<cfif success>
			<cfset Response.setSTATUS("success")>
		<cfelse>
			<cfset Response.setSTATUS("error")>
		</cfif>
		<cfoutput>#Response.generateJSON()#</cfoutput><cfabort>
	</cfcase>

	<cfcase value="getInstances">
		<cfset DocInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocInstances").init() />
		<cfset instanceData = DocInstances.getInstances('json', 'INSTANCE_ID,INSTANCE_NAME') />
		<cfoutput>#instanceData#</cfoutput><cfabort />
	</cfcase>

	<cfcase value="getLetterTypeFields">
		<cfset DocType = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocType").init(attributes.instance_id) />
		<cfoutput>#DocType.renderExtraFilters(attributes.student_id, attributes.training_session_cd)#</cfoutput><cfabort />
	</cfcase>

	<cfcase value="generateAttachments">
		<cfset DOCSnapshot = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocSnapshot").init(attributes.instance_id)>
		<cfset attachmentArr = DOCSnapshot.getAttachmentFiles(attributes.student_id, attributes.doc_snapshot_cd)>
		<cfoutput>#SerializeJson(attachmentArr)#</cfoutput><cfabort />
	</cfcase>

</cfswitch>