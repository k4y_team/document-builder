<cfcomponent name="CalculatEligibleUsersForm" displayname="CalculatEligibleUsersForm" output="false" extends="sis_core.model.blAutomation.EntityBase" accessors="true" code="CALCULATE_USERS_FORM">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="INSTANCE_NAME"  type="string" label="Document Type" id="instance_id" validations="required">

 	<cffunction name="getRenderInfo" returntype="struct">
         <cfset var fieldDataDefault = super.getRenderInfo()>
		 <cfset fieldDataDefault["DEFAULT"]["INSTANCE_NAME"]["renderType"] = "SELECT">
         <cfreturn fieldDataDefault>
    </cffunction>

	<cffunction name="getDataProvider_INSTANCE_NAME" returntype="string" output="false" access="public">
		<cfset var sql = "">
		<cfsavecontent variable ="sql">
			<cfoutput>
				SELECT
       				 	DISTINCT
                        A.INSTANCE_ID as idField,
                        A.INSTANCE_NAME as descField,
                        A.INSTANCE_CODE
                    FROM
                       DOC_INSTANCE A
                       WHERE 1=1
                    ORDER BY UPPER(A.INSTANCE_NAME)
			</cfoutput>
		</cfsavecontent>

		<cfreturn sql/>
	</cffunction>
</cfcomponent>