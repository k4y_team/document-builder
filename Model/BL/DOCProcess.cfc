component name="DocProcess" output="false" extends="ServiceBase" {

	DocProcess function init(numeric userID = "-1") {
		super.init();
		variables.userID = arguments.userID;
		return this;
	}

	public void function run(data) {
		var mainProcessID = startprocess("Generate letters","Start processing");
		var DocumentTasks = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocumentTasks" ).init();
		var result = DocumentTasks.generateTes(arguments.data);

		if (result.status eq "SUCCESS") {
			endProcess(mainProcessID, "Generate Letters successfully completed.");
		} else {
			endProcess(mainProcessID, "Generate Letters failed. #result.status_details#");
		}

		var interceptor = application.wirebox.getInstance("Interceptor@CORE");
		interceptor.inform("LOA:AGREEMENT_LETTER:AFTER:GENERATE",{training_session_cd=arguments.data.training_session_cd});
	}

	public numeric function startProcess(required string processName, required string processLog, numeric mainProcessID = -1, numeric instanceID = -1) {
		var docProcessDAO = variables.dbDocumentFactory.create("DOCProcessDAO");
		var processVO = variables.dbDocumentFactory.create("DOCProcessVO").init();
		processVO.setPROCESS_STATUS("PROGRESS");
		processVO.setPROCESS_NAME(processName);
		processVO.setLOG_DETAIL(processLog & chr(10) & chr(13));
		processVO.setPARENT_PROCESS_ID(mainProcessID);
		processVO.setCREATION_ID(variables.userID);
		processVO.setCREATION_DT(Now());
		processVO.setSTART_DT(Now());
		if (arguments.instanceId neq "") {
			processVO.setINSTANCE_ID(arguments.instanceId);
		}

		processID = docProcessDAO.add(processVO);

		return processID;
	}

	public void function endProcess(required numeric processId, required string processLog) {
		var docProcessDAO = variables.dbDocumentFactory.create("DOCProcessDAO");
		var processVO = docProcessDAO.read(processId);
		processVO.setPROCESS_STATUS("COMPLETED");
		processVO.setLOG_DETAIL( processVO.getLOG_DETAIL() & processLog);
		processVO.setEND_DT(Now());
		processVO.setMODIFICATION_ID(variables.userID);
		processVO.setMODIFICATION_DT(Now());
		docProcessDAO.update(processVO);
	}

	public void function logProcessProgress(required numeric processID, required string processLog, required string processStatusCode) {
		try {
			var docProcessDAO = variables.dbDocumentFactory.create("DOCProcessDAO");
			var processVO = docProcessDAO.read(processID);
			processVO.setPROCESS_STATUS(arguments.processStatusCode);
			processVO = docProcessDAO.read(processID);
			processLog = processVO.getLOG_DETAIL() & arguments.processLog;
			processVO.setLOG_DETAIL(processLog & chr(10) & chr(13));
			processVO.setMODIFICATION_ID(variables.userID);
			processVO.setMODIFICATION_DT(Now());
			docProcessDAO.update(processVO);
		} catch (any e) {
			WriteLog(file="power", text="#cfcatch.message#::#cfcatch.detail#");
		}
	}

	public string function setOperationList(required string steps) {
		var operationList = "";
		var procStepArr = getProcessSteps();
		listEach(arguments.steps, function(step, idx) {
			if ( ArrayIsDefined(procStepArr,step) ) {
				if (not ListFindNoCase(operationList,procStepArr[step])) {
					operationList = ListAppend(operationList,procStepArr[step]);
				}
			} else {
				throw(message="No step defined for this code: #step#!");
			}
		});

		return operationList;
	}

	private array function getProcessSteps() {
		var stepArr = ArrayNew(1);
		stepArr[1] = "CHECK_SESSION";
		stepArr[2] = "PROCESS_INSTANCE";
		stepArr[3] = "ELIGIBLE_SESSION";
		stepArr[4] = "PUBLISH_LOA";
		stepArr[5] = "GENERATE_ORIGINAL_LOA";
		stepArr[6] = "GENERATE_REVISED_LOA";

		return stepArr;
	}

	private array function getInstanceStatus() {
		var statusArr = ArrayNew(1);
		statusArr[1] = "NOT STARTED";
		statusArr[2] = "ACTIVE";
		statusArr[3] = "CLOSED";

		return statusArr;
	}

	public boolean function isProcessActive(any docLetterTemplateCD = "") {
		var docProcessGtw = variables.dbDocumentFactory.create("DOCProcessGtw");

		return docProcessGtw.isProcessActive(arguments.docLetterTemplateCD);
	}

	public query function getLastProcess() {
		var docProcessGtw = variables.dbDocumentFactory.create("DOCProcessGtw");
		return docProcessGtw.getLastProcess();
	}

	public query function getAllLogs() {
		var docProcessGtw = variables.dbDocumentFactory.create("DOCProcessGtw", true);
		return docProcessGtw.getAllLogs("");
	}

	public query function getLog(required numeric processID) {
		var docProcessGtw = variables.dbDocumentFactory.create("DOCProcessGtw", true);
		return docProcessGtw.getProcessLog(processID);
	}

	public void function calculateInstance(required numeric trSessCD, required numeric instanceId) {
		var trSessionQry = variables.ExternalService.getTrainingSessionByCD(arguments.trSessCd);
		var docProcessInstanceDAO = variables.dbFactory.create("DOCProcessInstanceDAO");
		var docProcessInstanceVO = docProcessInstanceDAO.read(arguments.instanceID, arguments.trSessCd);

		if (trSessionQry.start_year GTE variables.eligibleStartYear) {
			if (Val(docProcessInstanceVO.getTRAINING_SESSION_CD()) EQ 0) {
				docProcessInstanceVO.setTRAINING_SESSION_CD(arguments.trSessCd);
				docProcessInstanceVO.setINSTANCE_ID(arguments.instanceId);
				docProcessInstanceVO.setSTART_DT(now());
				docProcessInstanceVO.setEND_DT(trSessionQry.end_dt);
				docProcessInstanceVO.setSTATUS(calculateInstanceStatus(arguments.trSessCd));
				docProcessInstanceVO.setCREATION_DT(now());
				docProcessInstanceDAO.add(docProcessInstanceVO);
			} else if (docProcessInstanceVO.getSTATUS() NEQ variables.instanceStatusArr[3]) {
				docProcessInstanceVO.setSTATUS(calculateInstanceStatus(arguments.trSessCd));
				docProcessInstanceVO.setMODIFICATION_DT(now());
				docProcessInstanceDAO.update(docProcessInstanceVO);
			}
		} else {
			if (Val(docProcessInstanceVO.getTRAINING_SESSION_CD()) GT 0 AND docProcessInstanceVO.getSTATUS() NEQ variables.instanceStatusArr[3]) {
				docProcessInstanceVO.setSTATUS(variables.instanceStatusArr[3]);
				docProcessInstanceVO.setMODIFICATION_DT(now());
				docProcessInstanceDAO.update(docProcessInstanceVO);
			}
		}
	}

	public string function calculateInstanceStatus(required numeric trSessCD) {
		var trSessionQry = variables.ExternalService.getTrainingSessionByCD(arguments.trSessCd);
		var status = "";

		if (NOT variables.ExternalService.checkRecordsInSession(arguments.trSessCd)) {
			status = variables.instanceStatusArr[1];
		} else {
			currentDt = CreateDate(Year(now()),Month(now()),Day(now()));
			trSessEndDt = CreateDate(Year(trSessionQry.end_dt),Month(trSessionQry.end_dt),Day(trSessionQry.end_dt));
			if (DateDiff("d", currentDt, trSessEndDt) GE 0) {
				status = variables.instanceStatusArr[2];
			} else {
				status = variables.instanceStatusArr[3];
			}
		}

		return status;
	}
}