<cfcomponent name="DocAdmin" displayname="DocAdmin" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocAdmin">
		<cfargument name="instance_id" type="numeric" required="false" default="-1">
		<cfset super.init()>
		<cfset var docInstanceGtw = variables.dbDocumentFactory.create("docInstanceGtw") />
		<cfset variables.instance_id = arguments.instance_id>

		<cfif arguments.instance_id gt 0>
			<cfset variables.instanceQry = docInstanceGtw.getByPK(arguments.instance_id)>
		<cfelse>
			<cfset variables.instanceQry = docInstanceGtw.getAll()>
		</cfif>
		<cfif variables.SessionStorage.exists("LoggedInUser")>
			<cfset variables.docProcess = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocGenerate").init(variables.SessionStorage.getVar('LoggedInUser').user_id)>
		<cfelse>
			<cfset variables.docProcess = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocGenerate").init()>
		</cfif>

		<cfreturn this>
	</cffunction>

	<cffunction name="lastRun" access="public" output="false" returntype="query">
		<cfset var DOCProcessGtw = variables.dbDocumentFactory.create("DOCProcessGtw") />
		<cfset var DocProcessQry = DOCProcessGtw.getLastProcess() />
		<cfreturn DocProcessQry />
	</cffunction>

	<cffunction name="getGenerated" access="public" output="false" returntype="query">
		<cfargument name="filterVO" type="LoaFilterSortVO">
		<cfreturn this>
	</cffunction>

	<cffunction name="confirm" access="public" output="false" returntype="boolean">
       	<cfargument name="filterVO" type="sis_core.model.SearchFilter" required="false" />
		<cfargument name="showMessage" type="boolean" required="false" default="true">
		<cfargument name="instanceID" type="numeric" required="false" default="-1">

		<cfset var DocReport = "" />
		<cfset var LoaConfirmQry = "" />
		<cfset var Loa_snapshot_cd_List = "" />

		<!--- if selected all records --->
		<cfif arguments.filterVO.getFILTER_ITEM("SELECT_ALL") EQ "Y">
			<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
			<cfset LoaConfirmQry = DocReport.getLoaConfirm(arguments.filterVO,arguments.instanceID)>
			<cfset Loa_snapshot_cd_List = ValueList(LoaConfirmQry.Loa_snapshot_cd)>
			<cfset arguments.filterVO.setFILTER_ITEM("DOC_SNAPSHOT_CD",Loa_snapshot_cd_List)>
		</cfif>

		<cfset var success = true />
		<cfset var DocSnapshotBaseDAO = variables.dbDocumentFactory.create("DocSnapshotBaseDAO") />
		<cfset var DocSnapshotBaseVO = "" />
		<!--- Here is the list of all snapshot that need to be confirmed --->
		<cfset var list = arguments.filterVO.getFILTER_ITEM("DOC_SNAPSHOT_CD")>

		<cftransaction action="begin">
			<cftry>
				<cfloop list="#list#" index="doc_snapshot_cd">
			    	<cfset DocSnapshotBaseVO = DocSnapshotBaseDAO.read(doc_snapshot_cd)>
					<cfset DocSnapshotBaseVO.setDOC_CONFIRMED_USER_ID(variables.SessionStorage.getVar('LoggedInUser').user_id) />
					<cfset DocSnapshotBaseVO.setDOC_CONFIRMED_DT(Now()) />
								<!--- confirm all selected Loa --->
					<cfset DocSnapshotBaseVO.setMODIFICATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id) />
					<cfset DocSnapshotBaseVO.setMODIFICATION_DT(Now()) />
					<cfset DocSnapshotBaseDAO.update(DocSnapshotBaseVO) />
				</cfloop>
				<cfif arguments.showMessage>
					<cfset variables.MsgBox.success("Letters successfully confirmed.")>
				</cfif>
				<cftransaction action="commit"/>
				<cfcatch>
					<cftransaction action="rollback"/>
					<cfif arguments.showMessage>
						<cfset variables.MsgBox.error("There was an error confirming letters.") >
					</cfif>
					<!---TODO: use power log files  --->
					<!--- <cflog file="power" type="error" text="LOA Confirmation: #cfcatch.message# #cfcatch.Detail#"> --->
					<cfset success = false>
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn success />
	</cffunction>

	<cffunction name="unconfirm" access="public" output="false" returntype="boolean">
       <cfargument name="filterVO" type="sis_core.model.SearchFilter" required="false" />

		<!--- if selected all records --->
		<cfif arguments.filterVO.getFILTER_ITEM("SELECT_ALL") EQ "Y">
			<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
			<cfset LoaConfirmQry = DocReport.getLoaConfirm(filterVO)>
			<cfset Loa_snapshot_cd_List = ValueList(LoaConfirmQry.Loa_snapshot_cd)>
			<cfset arguments.filterVO.setFILTER_ITEM("DOC_SNAPSHOT_CD",Loa_snapshot_cd_List)>
		</cfif>
		<cfset var success = true />

		<!--- Here is the list of all snapshot that need to be confirmed --->
		<cfset list = filterVO.getFILTER_ITEM("DOC_SNAPSHOT_CD")>

		<cftransaction action="begin">
			<cftry>
				<cfloop list="#list#" index="doc_snapshot_cd">
					<cfset unconfirmSnapshot(doc_snapshot_cd) />
				</cfloop>
				<cfset variables.MsgBox.success("Letter confirmations successfully canceled.")>
				<cftransaction action="commit"/>
				<cfcatch>
					<cftransaction action="rollback"/>
					<cfset variables.MsgBox.error("There was an error cancelling confirmations.")>
					<cfset success = false>
				</cfcatch>
			</cftry>
		</cftransaction>

		<cfreturn success />
	</cffunction>

	<cffunction name="unconfirmSnapshot" access="private" output="false">
		<cfargument name="docSnapshotCD" type="numeric" required="true">
		<cfscript>
			var DocSnapshotBaseDAO = variables.dbDocumentFactory.create("DocSnapshotBaseDAO");
			var DocSnapshotBaseVO = DocSnapshotBaseDAO.read(arguments.docSnapshotCD);
			DocSnapshotBaseVO.setDOC_CONFIRMED_USER_ID("");
			DocSnapshotBaseVO.setDOC_CONFIRMED_DT("");
			DocSnapshotBaseVO.setMODIFICATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id);
			DocSnapshotBaseVO.setMODIFICATION_DT(Now());
			DocSnapshotBaseDAO.update(DocSnapshotBaseVO);
		</cfscript>
	</cffunction>

	<cffunction name="publish" access="public" output="false" returntype="struct">
       <cfargument name="data" type="struct" default="#StructNew()#">

		<cfset var success = true />
		<cfset var docSnapshotsQry = "">
		<cfif variables.sessionStorage.exists("LoggedInUser")>
			<cfset userID = variables.sessionStorage.getVar("LoggedInUser").USER_ID>
		<cfelse>
			<cfset userID = "1">
		</cfif>

		<cfif NOT isDefined("arguments.data.DOC_SNAPSHOT_CD")>
			<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
			<cfset DocReportQry = DocReport.getProcessDocuments(arguments.data)>
			<cfset docSnapshotCdList = ListRemoveDuplicates(ValueList(DocReportQry.DOC_SNAPSHOT_CD))>
		<cfelse>
			<cfset var docSnapshotCdList = arguments.data.DOC_SNAPSHOT_CD>
		</cfif>
		<cfset docInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceInfoQry = docInstances.getInstanceInfo(arguments.data.instance_id)>

		<cfset var updateQry = "">

		<cfset var queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils")>
		<cfquery name="updateQry" datasource="#variables.dbDocumentFactory.getDatasource()#">
			update
				doc_snapshot_base
			set
				is_current = 'Y',
				doc_published_dt = sysdate,
				doc_published_user_id = '#userID#',
				doc_confirmed_dt = sysdate,
				doc_confirmed_user_id = '#userID#'
			where
				doc_published_dt is null and
				#queryUtils.buildInSql(docSnapshotCdList, "DOC_SNAPSHOT_CD")#
		</cfquery>

		<cfif success>
			<cfset variables.MsgBox.success("All selected documents successfully published.")>
			<cfset variables.Interceptor.inform("DOCUMENT_BUILDER:DOCUMENT:AFTER:PUBLISH",{INSTANCE_CODE = instanceInfoQry.INSTANCE_CODE})>
		<cfelse>
			<cfset variables.MsgBox.error("Publishing selected documents failed.")>
		</cfif>

		<cfset var result = StructNew()>
		<cfset result.status = "SUCCESS">
		<cfreturn result>

	</cffunction>

	<cffunction name="generateMultipleOnDemand" access="public" output="false" returntype="string">
		<cfargument name="stdId" type="numeric" required="true" />
		<cfargument name="trSessCD" type="any" required="false" default="*"/>
		<cfargument name="filter" type="struct" required="false" default="#StructNew()#" />
		<cfargument name="onDemand" type="boolean" required="false" default="false" />

		<cfset var losSnapshotCdList = generateOnDemand(stdId=arguments.stdId, trSessCD=arguments.trSessCD, onDemand=arguments.onDemand)/>

		<cfset var interceptor = application.wirebox.getInstance("Interceptor@CORE")>
		<cfset var informParams = {
			student_id = arguments.stdId
		}>
		<cfif arguments.trSessCD neq "">
			<cfset informParams["training_session_cd"] = arguments.trSessCD>
		</cfif>
		<cfset interceptor.inform("LOA:AGREEMENT_LETTER:AFTER:GENERATE",informParams)>

		<cfreturn losSnapshotCdList />
	</cffunction>

	<cffunction name="publishMultipleOnDemand" access="public" output="false" returntype="boolean">
		<cfargument name="studentID" type="numeric" required="true" />

		<cfset var result = true />
		<cfset var allOK = true />

		<cfset var loaExtServ = createObject('component', 'medsisOld.modules.DocumentBuilder.Model.BL.ExternalService').init() />
		<cfset var trainingSessionQry = loaExtServ.getTrainingSessions() />
		<cfset var trainingSessionList = '' />

		<cfif trainingSessionQry.recordcount GT 0>
			<cfloop query="trainingSessionQry">
				<cfset trainingSessionList = ListRemoveDuplicates(listAppend(trainingSessionList, trainingSessionQry.TRAINING_SESSION_CD)) />
			</cfloop>
		</cfif>

		<cfset var doaInstances = createObject('component', 'medsisOld.modules.DocumentBuilder.Model.BL.LoaInstances').init() />
		<cfset var instancesQry = doaInstances.getInstances() />

		<cfif instancesQry.recordcount GT 0>
			<cfloop query="instancesQry">
				<cfset this.init(instancesQry.instance_id) />
				<cfset result = publishOnDemand(arguments.studentID, trainingSessionList) />
				<cfif !result>
					<cfset allOK = false />
				</cfif>
			</cfloop>
		</cfif>

		<cfif allOK>
			<cfset variables.MsgBox.clear() />
			<cfset variables.MsgBox.success("Publish letters successfully completed.") />
		</cfif>

		<cfreturn allOK />
	</cffunction>

	<cffunction name="generateOnDemand" access="public" output="false" returntype="string">
		<cfargument name="stdId" type="string" required="true" />
		<cfargument name="trSessCD" type="any" required="false" default="" />
		<cfargument name="onDemand" type="boolean" required="false" default="false" />

		<cfscript>
			var DocumentTasks = createObject('component', 'medsisOld.modules.DocumentBuilder.Model.BL.DocumentTasks').init();
			var ExternalService = createObject('component', 'medsisOld.modules.DocumentBuilder.Model.BL.ExternalService' ).init();
			var trSessionList = "";

			/* setup sessions */
			if (NOT isNumeric(arguments.trSessCD) OR listLen(arguments.trSessCD) EQ 0) {
				var currTrSessionQry = ExternalService.getCurrentTrainingSession();
				trSessionList = ListAppend(trSessionList, currTrSessionQry.training_session_cd);
				var nextTrSessionQry = ExternalService.getTrainingSessionByStartYear(currTrSessionQry.start_year + 1);
				if (nextTrSessionQry.recordcount gt 0) {
					trSessionList = ListAppend(trSessionList, nextTrSessionQry.training_session_cd);
				}
			} else {
				trSessionList = arguments.trSessCD;
			}

			var result = DocumentTasks.generateLetters({instance_id=ValueList(variables.instanceQry.instance_id),student_id=arguments.stdId,training_session_cd=trSessionList}, arguments.onDemand);
			if (result.status eq "SUCCESS") {
				var isOk = true;
				variables.MsgBox.success("Generate letters successfully completed.");
			} else {
				var isOk = false;
				if (StructKeyExists(result,"STATUS_DETAILS")) {
					var errorMsg = result["STATUS_DETAILS"];
				} else {
					var errorMsg = "Generate letters failed.";
				}
				variables.MsgBox.error(errorMsg);
			}
			if (StructKeyExists(result,"DOC_SNAPSHOT_CD")) {
				return ListRemoveDuplicates(result["DOC_SNAPSHOT_CD"]);
			} else {
				return "";
			}
		</cfscript>
	</cffunction>

	<cffunction name="publishOnDemand" access="public" output="false" returntype="boolean">
		<cfargument name="stdId" type="numeric" required="true" />
		<cfargument name="trSessCD" type="any" required="false" default="" />

		<cfscript>
			var isOk = false;
			var isSigned = false;
			var overwrite_current = "";
			var docSnapshotGtw = "";
			var generatedDocQry = "";
			var isSignedQry = "";
			var docSnapshotBaseDAO = "";
			var docSnapshotBaseVO = "";
			var trSessionList = "";
			var DocSettings = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSettings").init(variables.instance_id);
			var LOATrainee = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.LOATrainee").init(variables.instance_id,arguments.stdId);
			var settingsQry = "";

			if (NOT isNumeric(arguments.trSessCD) OR listLen(arguments.trSessCD) EQ 0) {
				var ExternalService = createObject('component', 'medsisOld.modules.DocumentBuilder.Model.BL.ExternalService' ).init();
				var currTrSessionQry = ExternalService.getCurrentTrainingSession();
				trSessionList = ListAppend(trSessionList, currTrSessionQry.training_session_cd);
				trSessionList = ListAppend(trSessionList, ExternalService.getTrainingSessionByStartYear(currTrSessionQry.start_year + 1).training_session_cd);
			} else {
				trSessionList = ListAppend(trSessionList, arguments.trSessCD);
			}

			for (var i = 1; i <= ListLen(trSessionList); i++) {
				var sessionIndex = ListGetAt(trSessionList, i);
				lockName = '#variables.instanceQry.INSTANCE_NAME#_'& arguments.stdId & '_' & sessionIndex;
				lock name=lockName type="exclusive" timeout="120" {

					transaction{
						try{
							docSnapshotGtw  = variables.dbDocumentFactory.create("docSnapshotGtw");
							/* check if LOA generated */
							generatedDocQry = docSnapshotGtw.getGeneratedLOA(variables.instance_id,arguments.stdId, sessionIndex);

							/*check if current LOA is signed */
							isSignedQry = LOATrainee.getCurrent(sessionIndex);
							if (isSignedQry.recordCount and isSignedQry.DOC_SIGN_STATUS neq "OUTSTANDING") {
								isSigned = true;
							}
							settingsQry = DocSettings.getSettings();
							if(isSigned) {
								overwrite_current = 'N';
							} else {
								overwrite_current = settingsQry.OVERWRITE_CURRENT;
							}

							/* don't process if multiple LOAs generated */
							//if (generatedDocQry.recordcount GT 0) {
								/* confirm LOA */
								for (var kk = 1; kk <= generatedDocQry.recordCount; kk++) {
									docSnapshotBaseDAO = variables.dbDocumentFactory.create("DOCSnapshotBaseDAO");
									docSnapshotBaseVO = docSnapshotBaseDAO.read(generatedDocQry["DOC_SNAPSHOT_CD"][kk]);
									docSnapshotBaseVO.setDOC_CONFIRMED_USER_ID(variables.SessionStorage.getVar('LoggedInUser').user_id);
									docSnapshotBaseVO.setDOC_CONFIRMED_DT(Now());
									docSnapshotBaseVO.setMODIFICATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id);
									docSnapshotBaseVO.setMODIFICATION_DT(Now());
									docSnapshotBaseDAO.update(docSnapshotBaseVO);

									/* publish LOA */
									variables.docProcess.publishConfirmed(generatedDocQry["DOC_LETTER_TEMPLATE_CD"][kk], generatedDocQry["DOC_SNAPSHOT_CD"][kk], overwrite_current);
								}
								TransactionCommit();

								isOk = true;
								variables.Interceptor.inform("LOA:AGREEMENT_LETTER:AFTER:PUBLISH",{instance_id=variables.instance_id, student_id=arguments.stdId, training_session_cd=sessionIndex});
								variables.MsgBox.success("Publish letters successfully completed.");
							//} else {
								//isOk = false;
								//variables.MsgBox.error("Multiple #variables.instanceQry.instance_name# letters have been found. Please contact support.");
							//}
						}catch(any e){
							TransactionRollback();
							isOk = false;
							variables.MsgBox.error("Publish letters failed.");
						}
					}
				}
			}

			return isOk;
		</cfscript>

	</cffunction>

	<cffunction name="removeTraineeGenerated" access="public" output="false" returntype="boolean" description="Delete the Trainee Generated LOA">
		<cfargument name="studentId" type="numeric" required="true" />
		<cfscript>
			var isOk = true;
			var snapshotCd = "";
			var DocSnapshotDetailGTW  = variables.dbDocumentFactory.create("DocSnapshotDetailGTW");
			var DOCSnapshotGtw  = variables.dbDocumentFactory.create("DOCSnapshotGtw");
			var generatedDocQry = DocSnapshotDetailGTW.getGenerated(studentId=arguments.studentId);
			var snaphostList = ValueList(generatedDocQry.DOC_SNAPSHOT_CD);
			var l = ListLen(snaphostList);

	  		transaction{
				try{

					for (i = 1; i lte l; i++){
						/* first we unconfirmed the LOA*/
						snapshotCd = ListGetAt(snaphostList, i);
						unconfirmSnapshot(snapshotCd);

						/* then delete LOA*/
						DOCSnapshotGtw.deleteNotConfirmed(variables.instance_id, snapshotCd);
					}
					TransactionCommit();
				}catch(any e){
					TransactionRollback();
					rethrow;
					isOk = false;
				}
			}

			return isOk;
		</cfscript>
	</cffunction>

</cfcomponent>