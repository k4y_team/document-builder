<cfcomponent displayname="DocBL" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocBL">
		<cfargument name="instanceID" type="numeric" required="false" default="1">
		<cfset super.init()>
		<cfset variables.instanceID = arguments.instanceID>
		<cfset variables.ExternalService = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.ExternalService" ).init() />
		<cfreturn this/>
	</cffunction>

	<cffunction name="getInstance" access="public" returntype="query" output="false">
		<cfset var docInstanceGtw = variables.dbFactory.create("docInstanceGtw") />
		<cfset var doaInstanceQry = docInstanceGtw.getByPK(variables.instanceId) />
		<cfreturn doaInstanceQry />
	</cffunction>

	<cffunction name="getDocTypeCdByTrainee" access="public" output="false" returntype="numeric" hint="returns -1 if no LOA Type identified or the CD of the found record">
		<cfargument name="stdId" type="numeric" required="true">
		<cfargument name="trSessionCD" type="numeric" required="true">
		<cfargument name="letterTemplateCD" type="string" required="false" default="*">

		<cfset var LogicalCondition = application.wirebox.getInstance('LogicalCondition@CORE') />
		<cfset var QueryBuilder = application.wirebox.getInstance('QueryBuilder@CORE') />
		<cfset var DocTypeGTW = variables.dbFactory.create('DocTypeGTW')>
		<cfset var qDocType = DocTypeGTW.getAllWithMap(variables.instanceId, arguments.letterTemplateCD)>
		<cfset var docTypeSql = ''>
		<cfset var DocTypeCD = -1>
		<cftransaction action="commit">
		<cftransaction action="begin">
			<cfloop query="qDocType">
				<cfset var entityFields = qDocType.DOC_TYPE_CD & ' AS DOC_TYPE_CD' />
				<cfset var docTypeRestrictionSql = LogicalCondition.getCondition(qDocType.RESTRICTION_CONDITION_ID).getCONDITION_TEXT() />
				<cfset docTypeRestrictionSql = '(' & docTypeRestrictionSql & ') AND TraineeRegistration.TRAINING_SESSION_CD = ' & arguments.trSessionCD & ' AND TRAINEE.STUDENT_ID = ' & arguments.stdID />
				<cfset docTypeRestrictionSql = LogicalCondition.getSql(docTypeRestrictionSql, entityFields, false) />
				<cfif qDocType.currentRow GT 1>
					<cfset docTypeSql = docTypeSql & ' UNION ' />
				</cfif>
				<cfset docTypeSql = docTypeSql & docTypeRestrictionSql />
			</cfloop>
			<cfset var docTypeQry = QueryBuilder.runQuery(docTypeSql) />
			<cfif docTypeQry.recordCount>
				<cfset DocTypeCD = docTypeQry.DOC_TYPE_CD />
			</cfif>
			<cftransaction action="commit">
		</cftransaction>

		<cfreturn DocTypeCD>
	</cffunction>

</cfcomponent>