component name="DocGenerate" output="false" extends="ServiceBase" {

	DocGenerate function init(numeric userID = "-1", string steps = "", string instanceID = "*") {
		super.init();

		variables.steps = arguments.steps;
		variables.userID = arguments.userID;
		variables.docTypeList = "ORIGINAL,REVISED";
		variables.objectFactory = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocObjectFactory").init();
		variables.DocProcess  = variables.objectFactory.create("DOC_PROCESS_COMPONENT").init(user_id=arguments.userID,instance_id=arguments.instanceID);

		return this;
	}

	private string function joinConditionWithRefersTo(required string condition, required string keyList) {
		var newCondition = arguments.condition;
		if (Trim(newCondition) eq '') {
			newCondition = "1 = 1";
		}
		for (var key in arguments.keyList) {
			newCondition &= " AND " & key & " IS NOT NULL";
		}
		return newCondition;
	}

	private struct function decodeContextualKey(required string encodedKey) {
		var result = {};

		for (var i = 1; i <= ListLen(arguments.encodedKey, "+"); i++) {
			var key = ListGetAt(arguments.encodedKey, i, "+");
			if (ListLen(key, "=") GT 1) {
				result[ListFirst(key, "=")] = ListLast(key, "=");
			} else {
				result[ListFirst(key, "=")] = "";
			}
		}

		return result;
	}

	private string function generateDocumentDescription(required any loaDataProvider, required string contextKey) {
		var desc = "";
		var contextStruct = decodeContextualKey(arguments.contextKey);
		var tsQry = "";

		if (StructKeyExists(contextStruct, "TRAINING_SESSION_CD")) {
			tsQry = arguments.loaDataProvider.getTrainingSession(contextStruct["TRAINING_SESSION_CD"]);
			desc &= "Session: " & tsQry.START_YEAR & "-" & tsQry.END_YEAR & chr(10) & chr(13);
		}

		return desc;
	}

	public string function generateLetterContent(required any loaDataProvider, required string contextKey, required string tagList) {
		var letterContent = "";
		var contextStruct = decodeContextualKey(arguments.contextKey);

		/*<cfquery name="getComputerAccess" dbtype="query">
			select *
			from getComputerAccess
			where
				ACCESS_TYPE_DESC = <cfqueryparam cfsqltype="cf_sql_varchar" value="Network / Email" />
		</cfquery>

		<cfif variables.instanceQry.instance_name eq "HAL">
			<cfquery name="getStudentTraining" dbtype="query">
				select *
				from getStudentTraining
				where
					funding_type_desc = <cfqueryparam cfsqltype="cf_sql_varchar" value="OMH" /> and
					training_level_type_desc = <cfqueryparam cfsqltype="cf_sql_varchar" value="Resident" />
			</cfquery>
		<cfelseif variables.instanceQry.instance_name eq "Training Agreements">
			<cfquery name="getStudentTraining" dbtype="query">
				select *
				from getStudentTraining
				where
					TRAINING_LEVEL_DESC = <cfqueryparam cfsqltype="cf_sql_varchar" value="Clinical Fellow" />
			</cfquery>
		</cfif>*/

		savecontent variable="letterContent" {
			WriteOutput("<tags>");

			for (var j = 1; j <= ListLen(arguments.tagList); j++) {
				switch (UCase(ListGetAt(arguments.tagList, j))) {
					case "HEADER_INFO":
						var clientInfo = variables.moduleConfig.getSetting('CLIENTINFO');
						WriteOutput('
							<tag code="HEADER_INFO">
								<data>
									<row id="1">
										<column name="file_path">#XmlFormat("file:///#ExpandPath('/medsis')#/includes/#application.cbcontroller.getSetting('clientInfo').LOGO.BW#")#</column>
										<column name="file_url">#XmlFormat("#Replace(application.cbcontroller.getSetting('applicationBaseURL'),'https','http')#/includes/#application.cbcontroller.getSetting('clientInfo').LOGO.BW#")#</column>
										<column name="client_name">#application.cbcontroller.getSetting('clientInfo').NAME#</column>
										<column name="faculty">#XmlFormat("#clientInfo.FACULTY#")#</column>
										<column name="office">#XmlFormat("#clientInfo.OFFICE#")#</column>
										<column name="address">#XmlFormat("#clientInfo.ADDRESS#")#</column>
										<column name="phone">#XmlFormat("#clientInfo.PHONE#")#</column>
										<column name="ext">#XmlFormat("#clientInfo.EXT#")#</column>
										<column name="fax">#XmlFormat("#clientInfo.FAX#")#</column>
										<column name="website">#XmlFormat("#clientInfo.WEBSITE#")#</column>
									</row>
								</data>
							</tag>
						');
						break;

					case "OPHRDC_NUMBER":
						if (not isDefined("getStudentInfo")) {
							var getStudentInfo = arguments.loaDataProvider.getStudentInfo(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="OPHRDC_NUMBER">
								<data>#getStudentInfo.OPHRDC#</data>
							</tag>
						');
						break;

					case "DOCUMENT_DATE":
						WriteOutput('
							<tag code="DOCUMENT_DATE">
								<data>#DateFormat(now(),"mmmm dd, yyyy")#</data>
							</tag>
						');
						break;

					case "TRAINEE_NUMBER":
						if (not isDefined("getStudentInfo")) {
							var getStudentInfo = arguments.loaDataProvider.getStudentInfo(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="TRAINEE_NUMBER">
								<data>#getStudentInfo.student_number#</data>
							</tag>
						');
						break;

					case "TRAINEE_NAME":
						if (not isDefined("getStudentInfo")) {
							var getStudentInfo = arguments.loaDataProvider.getStudentInfo(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="TRAINEE_NAME">
								<data>#getStudentInfo.first_name# #getStudentInfo.last_name#</data>
							</tag>
						');
						break;

					case "TRAINEE_PICTURE":
						var StdRegModuleConfig = application.ModuleManager.getModuleConfig("STUDENT_REGISTRATION");
						if (not isDefined("getStudentInfo")) {
							var getStudentInfo = arguments.loaDataProvider.getStudentInfo(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="TRAINEE_PICTURE">
								<data>#application.cbcontroller.getSetting('modulesApplicationURL')##StdRegModuleConfig.getModuleInfo().RELATIVE_PATH#/#StdRegModuleConfig.getSetting("PICTURE_FOLDER")#/#getStudentInfo.PICTURE#</data>
							</tag>
						');
						break;

					case "TRAINEE_MINC":
						if (not isDefined("getStudentInfo")) {
							var getStudentInfo = arguments.loaDataProvider.getStudentInfo(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="TRAINEE_MINC">
								<data>#getStudentInfo.MINC#</data>
							</tag>
						');
						break;

					case "TRAINEE_MAILING_PHONE":
						if (not isDefined("getStudentInfo")) {
							var getStudentInfo = arguments.loaDataProvider.getStudentInfo(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="TRAINEE_MAILING_PHONE">
								<data>#getStudentInfo.MAILING_PHONE#</data>
							</tag>
						');
						break;

					case "TRAINEE_EMAIL":
						if (not isDefined("getStudentPersonalInfo")) {
							var getStudentPersonalInfo = arguments.loaDataProvider.getStudentPersonalInfo(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="TRAINEE_EMAIL">
								<data>#getStudentPersonalInfo.EMAIL_DEFAULT#</data>
							</tag>
						');
						break;

					case "TRAINEE_SCHOOL":
						if (not isDefined("getStudentTraining")) {
							var getStudentTraining = arguments.loaDataProvider.getStudentTraining(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						}

						WriteOutput('
							<tag code="TRAINEE_SCHOOL">
								<data>#getStudentTraining.UNIVERSITY_DESC#</data>
							</tag>
						');
						break;

					case "TRAINEE_ADDRESS":
						if (not isDefined("getStudentInfo")) {
							var getStudentInfo = arguments.loaDataProvider.getStudentInfo(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="TRAINEE_ADDRESS">
								<data>
									#replace(getStudentInfo.ADDRESS,"&","&amp;","all")#');
									if (getStudentInfo.ADDRESS2 != "") {
										WriteOutput("&lt;br/&gt;#replace(getStudentInfo.ADDRESS2,"&","&amp;","all")#");
									}

									WriteOutput("&lt;br/&gt;#replace(getStudentInfo.CITY,"&","&amp;","all")#");
									if (getStudentInfo.PROVINCE_DESC != "") {
										if (getStudentInfo.CITY != "") {
											WriteOutput(", ");
										}
										WriteOutput("#getStudentInfo.PROVINCE_DESC#");
									}

									if (getStudentInfo.POSTAL_CODE != "") {
										if (getStudentInfo.PROVINCE_DESC != "" && getStudentInfo.CITY != "") {
											WriteOutput(", ");
										}
										WriteOutput("#getStudentInfo.POSTAL_CODE#");
									}

									if (getStudentInfo.COUNTRY_DESC != "") {
										WriteOutput("&lt;br/&gt;#getStudentInfo.COUNTRY_DESC#");
									}
								WriteOutput('</data>
							</tag>
						');
						break;

					case "PROGRAM_DESC":
						if (not isDefined("getStudentTraining")) {
							var getStudentTraining = arguments.loaDataProvider.getStudentTraining(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						}

						WriteOutput('
							<tag code="PROGRAM_DESC">
								<data>');
									if (getStudentTraining.base_program_desc neq "") {
										WriteOutput(replace(getStudentTraining.base_program_desc,"&","&amp;","all"));
									} else {
										WriteOutput(replace(getStudentTraining.program_desc,"&","&amp;","all"));
									}
								WriteOutput('</data>
							</tag>
						');
						break;

					case "DEPARTMENT_DESC":
						if (not isDefined("getStudentTraining")) {
							var getStudentTraining = arguments.loaDataProvider.getStudentTraining(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						}

						WriteOutput('
							<tag code="DEPARTMENT_DESC">
								<data>#replace(getStudentTraining.department_desc,"&","&amp;","all")#</data>
							</tag>
						');
						break;

					case "TRAINEE_PROFILE":
						var getMedicalDegree = arguments.loaDataProvider.getMedicalDegree(contextStruct["STUDENT_ID"]);
						var getStudentCPSO = arguments.loaDataProvider.getStudentCPSO(contextStruct["STUDENT_ID"]);

						WriteOutput('
							<tag code="TRAINEE_PROFILE">
								<data>
									<row id="1">
										<column name="medical_school">#replace(getMedicalDegree.university_desc,"&","&amp;","all")#, #getMedicalDegree.country_desc#</column>
										<column name="graduation_year">#getMedicalDegree.degree_year#</column>');
										if (getStudentCPSO.recordcount) {
											for (var i = getStudentCPSO.recordcount; i <= getStudentCPSO.recordcount; i++) {
												WriteOutput('<column name="cpso_num">#getStudentCPSO['cpso_num'][i]#</column>');
												WriteOutput('<column name="cpso_type">#getStudentCPSO['cpso_type_desc'][i]#</column>');
											}
										} else {
											WriteOutput('<column name="cpso_num"></column>');
											WriteOutput('<column name="cpso_type"></column>');
										}
									WriteOutput('</row>
								</data>
							</tag>
						');
						break;

					case "TRAINING_LINE":
						if (not isDefined("getStudentTraining")) {
							var getStudentTraining = arguments.loaDataProvider.getStudentTraining(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						}

						var nr_star = 0;
						WriteOutput('
							<tag code="TRAINING_LINE">
								<data>');
									for (var i = 1; i <= getStudentTraining.recordcount; i++) {
										WriteOutput('<row id="#i#">');
											WriteOutput('<column name="training_cd">#getStudentTraining["training_cd"][i]#</column>');
											WriteOutput('<column name="program_desc">#replace(getStudentTraining["base_program_desc"][i],"&","&amp;","all")#</column>');
											WriteOutput('<column name="training_level_desc">');
												WriteOutput("#getStudentTraining['training_level_desc'][i]#");
												if (nr_star == 1 && !ListFindNoCase("ASSESSMENT VERIFICATION PERIOD,PRE ENTRY ASSESSMENT PROGRAM",getStudentTraining["training_status_desc"][i])) {
													WriteOutput("*");
													nr_star = 0;
												}
												if (ListFindNoCase("ASSESSMENT VERIFICATION PERIOD,PRE ENTRY ASSESSMENT PROGRAM",getStudentTraining["training_status_desc"][i])) {
													nr_star = 1;
												}
											WriteOutput("</column>");
											WriteOutput('
											<column name="funding_source">#replace(getStudentTraining["funding_source_desc"][i],"&","&amp;","all")#</column>
											<column name="training_status_desc">#getStudentTraining["training_status_desc"][i]#</column>
											<column name="time">#getStudentTraining["fte_amt"][i]#</column>
											<column name="group">#getStudentTraining["funding_category_no"][i]#</column>
											<column name="pool">#getStudentTraining["pool"][i]#</column>
											<column name="start_dt">#DateFormat(getStudentTraining["start_dt"][i], "dd-MMM-yyyy")#</column>
											<column name="end_dt">#DateFormat(getStudentTraining["end_dt"][i], "dd-MMM-yyyy")#</column>
											<column name="trainee_type">#getStudentTraining["student_type_desc"][i]#</column>
											');
										WriteOutput("</row>");
									}
								WriteOutput("</data>
							</tag>
						");
						break;

					case "TRAINING_LINE_EXTENDED":
						var getStudentTrainingGrouped = arguments.loaDataProvider.getStudentTrainingGrouped(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						WriteOutput('
							<tag code="TRAINING_LINE_EXTENDED">
								<data>');
									for (var i = 1; i <= getStudentTrainingGrouped.recordcount; i++) {
										WriteOutput('<row id="#i#">');
											WriteOutput('
											<column name="program_desc">#replace(getStudentTrainingGrouped["program_desc"][i],"&","&amp;","all")#</column>
											<column name="training_level_desc">#replace(getStudentTrainingGrouped["training_level_desc"][i],"&","&amp;","all")#</column>
											<column name="start_dt">#DateFormat(getStudentTrainingGrouped["min_start_dt"][i], "mmmm d, yyyy")#</column>
											<column name="end_dt">#DateFormat(getStudentTrainingGrouped["max_end_dt"][i], "mmmm d, yyyy")#</column>
											');
										WriteOutput("</row>");
									}
								WriteOutput("</data>
							</tag>
						");
						break;

					case "TRAINING_DETAIL":
						if (not isDefined("getStudentTraining")) {
							var getStudentTraining = arguments.loaDataProvider.getStudentTraining(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						}

						var nr_star = 0;
						WriteOutput('
							<tag code="TRAINING_DETAIL">
								<data>');
									for (var i = 1; i <= getStudentTraining.recordcount; i++) {
										WriteOutput('<row id="#i#">');
											WriteOutput('<column name="training_cd">#getStudentTraining["training_cd"][i]#</column>');
											if (getStudentTraining["base_program_desc"][i] neq "") {
												WriteOutput('<column name="program_desc">#replace(getStudentTraining["base_program_desc"][i],"&","&amp;","all")#</column>');
											} else {
												WriteOutput('<column name="program_desc">#replace(getStudentTraining["program_desc"][i],"&","&amp;","all")#</column>');
											}
											WriteOutput('<column name="training_level_desc">');
												WriteOutput("#getStudentTraining["training_level_desc"][i]#");
												if (nr_star == 1 && !ListFindNoCase("ASSESSMENT VERIFICATION PERIOD,PRE ENTRY ASSESSMENT PROGRAM",getStudentTraining["training_status_desc"][i])) {
													WriteOutput("*");
													nr_star = 0;
												}
												if (ListFindNoCase("ASSESSMENT VERIFICATION PERIOD,PRE ENTRY ASSESSMENT PROGRAM",getStudentTraining["training_status_desc"][i])) {
													nr_star = 1;
												}
											WriteOutput("</column>");
											WriteOutput('
											<column name="funding_source">#replace(getStudentTraining["funding_source_desc"][i],"&","&amp;","all")#</column>
											<column name="training_status_desc">#getStudentTraining["training_status_desc"][i]#</column>
											<column name="time">#getStudentTraining["fte_amt"][i]#</column>
											<column name="group">#getStudentTraining["funding_category_no"][i]#</column>
											<column name="pool">#getStudentTraining["pool"][i]#</column>
											<column name="start_dt">#DateFormat(getStudentTraining["start_dt"][i], "dd-MMM-yyyy")#</column>
											<column name="end_dt">#DateFormat(getStudentTraining["end_dt"][i], "dd-MMM-yyyy")#</column>
											<column name="trainee_type">#getStudentTraining["student_type_desc"][i]#</column>
											');
										WriteOutput("</row>");
									}
								WriteOutput("</data>
							</tag>
						");
						break;

					case "TRAINING_SESSION":
						var trainingSessionQry = arguments.loaDataProvider.getTrainingSession(contextStruct["TRAINING_SESSION_CD"]);
						WriteOutput('
							<tag code="TRAINING_SESSION">
								<data>');
									if (getStudentTraining.recordcount > 0) {
										WriteOutput("#trainingSessionQry.START_YEAR# - #trainingSessionQry.END_YEAR#");
									}
								WriteOutput('</data>
							</tag>
						');
						break;

					case "TRAINING_START_DATE":
						if (not isDefined("getStudentTraining")) {
							var getStudentTraining = arguments.loaDataProvider.getStudentTraining(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						}

						WriteOutput('
							<tag code="TRAINING_START_DATE">
								<data>');
									if (getStudentTraining.recordcount > 0) {
										WriteOutput("#DateFormat(getStudentTraining["start_dt"][1], "dd-MMM-yyyy")#");
									}
								WriteOutput('</data>
							</tag>
						');
						break;

					case "TRAINING_END_DATE":
						if (not isDefined("getStudentTraining")) {
							var getStudentTraining = arguments.loaDataProvider.getStudentTraining(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						}

						WriteOutput('
							<tag code="TRAINING_END_DATE">
								<data>');
									if (getStudentTraining.recordcount > 0) {
										WriteOutput('#DateFormat(getStudentTraining["end_dt"][getStudentTraining.recordcount], "dd-MMM-yyyy")#');
									}
								WriteOutput("</data>
							</tag>
						");
						break;

					case "REG_FEE":
						var regFee = arguments.loaDataProvider.getFee(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);

						WriteOutput('
							<tag code="REG_FEE">
								<data>#regFee#</data>
							</tag>
						');
						break;

					case "NETWORK_ACCESS_ID":
						if (not isDefined("getComputerAccess")) {
							var getComputerAccess = arguments.loaDataProvider.getComputerAccess(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="NETWORK_ACCESS_ID">
								<data>#getComputerAccess.ID#</data>
							</tag>
						');
						break;

					case "NETWORK_PASSWORD":
						if (not isDefined("getComputerAccess")) {
							var getComputerAccess = arguments.loaDataProvider.getComputerAccess(contextStruct["STUDENT_ID"]);
						}

						WriteOutput('
							<tag code="NETWORK_PASSWORD">
								<data>#getComputerAccess.PASSWORD#</data>
							</tag>
						');
						break;

					case "REQUIREMENT_DETAIL":
						var getRequirementDetail = arguments.LoaDataProvider.getRequirementDetail(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"]);
						var getStudentReqDocuments = arguments.LoaDataProvider.getStudentReqDocuments(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"], "CPSO_LICENSE,CMPA_LICENSE,WORK_PERMIT");
						var availibleDocuments = ValueList(getStudentReqDocuments.TEMPLATE_CODE);

						var nr_star = 0;
						WriteOutput('
							<tag code="REQUIREMENT_DETAIL">
								<data>
									<row id="1">');
										if (ListFindNoCase(availibleDocuments, "CPSO_LICENSE")) {
											WriteOutput('<column name="cpso_num">#getRequirementDetail.CPSO_NUM#</column>');
											if (Trim(getRequirementDetail.CPSO_EXP_DT) NEQ "") {
												WriteOutput('<column name="cpso_expiry_dt">#DateFormat(getRequirementDetail.CPSO_EXP_DT, "dd-MMM-yyyy")#</column>');
											} else {
												WriteOutput('<column name="cpso_expiry_dt">N/A</column>');
											}
										}
										if (ListFindNoCase(availibleDocuments, "CMPA_LICENSE")) {
											WriteOutput('<column name="cmpa_num">#getRequirementDetail.CMPA_NUM#</column>');
											WriteOutput('<column name="cmpa_expiry_dt">#DateFormat(getRequirementDetail.CMPA_EXP_DT, "dd-MMM-yyyy")#</column>');
										}
										if (Trim(getRequirementDetail.MASK_TYPE_DESC) NEQ "") {
											WriteOutput('<column name="mask_type_desc">#getRequirementDetail.MASK_TYPE_DESC#</column>');
										} else {
											WriteOutput('<column name="mask_type_desc">N/A</column>');
										}
										if (Trim(getRequirementDetail.MASK_EXPIRY_DT) NEQ "") {
											WriteOutput('<column name="mask_expiry_dt">#DateFormat(getRequirementDetail.MASK_EXPIRY_DT, "dd-MMM-yyyy")#</column>');
										} else {
											WriteOutput('<column name="mask_expiry_dt">N/A</column>');
										}
										if (Trim(getRequirementDetail.SCRUB_SIZE) NEQ "") {
											WriteOutput('<column name="scrub_size">#getRequirementDetail.SCRUB_SIZE#</column>');
										} else {
											WriteOutput('<column name="scrub_size">N/A</column>');
										}
										if (ListFindNoCase(availibleDocuments, "WORK_PERMIT")) {
											WriteOutput('<column name="visa_expiry_dt">#DateFormat(getRequirementDetail.VISA_EXPIRY_DATE, "dd-MMM-yyyy")#</column>');
										}
									WriteOutput('</row>
								</data>
							</tag>
						');
						break;

					case "ROTATION_DETAIL":
						var getStudentRotation = arguments.loaDataProvider.getStudentRotation(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"], contextStruct["STD_ROTATION_ID"]);

						var nr_star = 0;
						WriteOutput('
							<tag code="ROTATION_DETAIL">
								<data>');
									for (var i = 1; i <= getStudentRotation.recordcount; i++) {
										WriteOutput('<row id="#i#">');
											WriteOutput('
											<column name="rotation_desc">#getStudentRotation["ROTATION_NAME"][i]#</column>
											<column name="rotation_program_desc">#getStudentRotation["PROGRAM_NAME"][i]#</column>
											<column name="location_desc">#getStudentRotation["HOSPITAL_NAME"][i]#</column>
											<column name="start_dt">#DateFormat(getStudentRotation["START_DT"][i], "dd-MMM-yyyy")#</column>
											<column name="end_dt">#DateFormat(getStudentRotation["END_DT"][i], "dd-MMM-yyyy")#</column>
											<column name="supervisor_names">#getStudentRotation["SUPERVISOR_NAMES"][i]#</column>
											<column name="rotation_type">#getStudentRotation["ROTATION_TYPE"][i]#</column>
											<column name="service_format">#getStudentRotation["SERVICE_FORMAT"][i]#</column>
											<column name="service">#getStudentRotation["SERVICE"][i]#</column>
											');
										WriteOutput("</row>");
									}
								WriteOutput("</data>
							</tag>
						");
						break;

					case "MIN_REQ_EXPIRY_DATE":
						var getStudentReqDocuments = arguments.LoaDataProvider.getStdReqDocMinExpiry(contextStruct["STUDENT_ID"], contextStruct["TRAINING_SESSION_CD"], "CPSO_LICENSE,CMPA_LICENSE,WORK_PERMIT,IMMUNIZATION_FORM,EMODULES,PAYMENT,LOA,HAL,REGISTRATION_FORM");

						WriteOutput('
							<tag code="MIN_REQ_EXPIRY_DATE">
								<data>#DateFormat(getStudentReqDocuments.MIN_REQ_EXPIRY_DATE, "dd-MMM-yyyy")#</data>
							</tag>
						');
						break;
				}
			}

			WriteOutput("</tags>");
		}

		return trim(letterContent);
	}

	private void function saveMetadata(required numeric docSnapshotCD, required string contextKey) {
		var contextStruct = decodeContextualKey(arguments.contextKey);
		var keyList = StructKeyList(contextStruct);
		var docSnapshotMetadataVO = variables.dbFactory.create("DocSnapshotMetadataVO");
		var docSnapshotMetadataDAO = variables.dbFactory.create("DocSnapshotMetadataDAO");

		for (var i = 1; i <= ListLen(keyList); i++) {
			DocSnapshotMetadataVO.init();
			DocSnapshotMetadataVO.setDOC_SNAPSHOT_CD(arguments.docSnapshotCD);
			DocSnapshotMetadataVO.setFIELD_NAME(ListGetAt(keyList, i));
			DocSnapshotMetadataVO.setFIELD_VALUE(contextStruct[ListGetAt(keyList, i)]);

			docSnapshotMetadataDAO.add(DocSnapshotMetadataVO);
		}
	}

	private numeric function saveDocument(required numeric userID, required numeric docLetterTemplateCD, required numeric docTypeCD, required numeric docLetterTypeCD, required string contextKey, numeric loaChangeTypeCD = -1) {
		var DocLetterTemplateGTW = variables.dbFactory.create("DocLetterTemplateGTW");
		var docTemplateQry = DocLetterTemplateGTW.getByPK(arguments.docLetterTemplateCD);

		var docSnapshotBaseVO = variables.dbFactory.create("DOCSnapshotBaseVO").init();
		var docSnapshotBaseDAO = variables.dbFactory.create("DocSnapshotBaseDAO");
		/* var docLetterDataDAO = variables.dbFactory.create("DocLetterDataDAO"); */
		var docSnapshotDetailDAO = variables.dbFactory.create("DocSnapshotDetailDAO");
		var docSnapshotCD = "";
		var docLetterDataCD = "";

		var loaDataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.LoaDataProvider").init(arguments.userID);

		docSnapshotBaseVO.setINSTANCE_ID(docTemplateQry.INSTANCE_ID);
		docSnapshotBaseVO.setDOC_DT(now());
		docSnapshotBaseVO.setIS_CURRENT("Y");
		docSnapshotBaseVO.setUSER_ID(arguments.userID);
		// problem here
		docSnapshotBaseVO.setDOCUMENT_DESC(generateDocumentDescription(loaDataProvider, arguments.contextKey));
		docSnapshotBaseVO.setTEMPLATE_BODY(docTemplateQry.LETTER_TEMPLATE_BODY);
		docSnapshotBaseVO.setCREATION_ID(variables.userID);
		docSnapshotBaseVO.setCREATION_DT(now());
		docSnapshotCD = docSnapshotBaseDAO.add(docSnapshotBaseVO);

		/* create loa letter data */
		/* Was moved to Agreement Letter Sign Off
		var docLetterDataVO = variables.dbFactory.create("LOALetterDataVO").init();
		docLetterDataVO.setXML_LETTER_CONTENT(generateLetterContent(loaDataProvider, arguments.contextKey, docTemplateQry.TAGS));
		docLetterDataVO.setCREATION_DT(now());
		docLetterDataCD = docLetterDataDAO.add(docLetterDataVO); */

		/* create loa snapshot detail */
		var docSnapshotDetailVO = variables.dbFactory.create("DOCSnapshotDetailVO").init();
		docSnapshotDetailVO.setDOC_SNAPSHOT_CD(docSnapshotCD);
		docSnapshotDetailVO.setDOC_LETTER_TEMPLATE_CD(arguments.docLetterTemplateCD);
		/* docSnapshotDetailVO.setDOC_LETTER_DATA_CD(docLetterDataCD); */

		docSnapshotDetailVO.setDOC_TYPE_CD(arguments.docTypeCD);
		docSnapshotDetailVO.setDOC_LETTER_TYPE_CD(arguments.docLetterTypeCD);
		docSnapshotDetailVO.setCREATION_DT(now());

		// this should be metadata; saving for compatibility issues
		var contextStruct = decodeContextualKey(arguments.contextKey);
		if (StructKeyExists(contextStruct, "STUDENT_ID")) {
			docSnapshotDetailVO.setSTUDENT_ID(contextStruct["STUDENT_ID"]);
			var loaDataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.LoaDataProvider").init(arguments.userID);
			docSnapshotDetailVO.setTRAINEE_TYPE_CD(loaDataProvider.getStudentInfo(contextStruct["STUDENT_ID"]).STUDENT_TYPE_CD);
		}
		if (StructKeyExists(contextStruct, "TRAINING_SESSION_CD")) {
			docSnapshotDetailVO.setTRAINING_SESSION_CD(contextStruct["TRAINING_SESSION_CD"]);
		}

		if (arguments.loaChangeTypeCd > 0) {
			docSnapshotDetailVO.setDOC_CHANGE_TYPE_CD(arguments.loaChangeTypeCd);
		}

		docSnapshotDetailDAO.add(docSnapshotDetailVO);

		return docSnapshotCD;
	}

	public boolean function calculateUserDocument(required numeric userID, required numeric docLetterTemplateCD, required numeric docTypeCD, required string contextualKey, string docTypeList = "") transactional {
		var argStruct = {
			"userID" = arguments.userID,
			"docLetterTemplateCD" = arguments.docLetterTemplateCD,
			"docTypeCD" = arguments.docTypeCD,
			"contextualKey" = arguments.contextualKey
		};

		/* based on context keys, get metadata; if there is metadata, we can't generate a new original */
		var docSnapshotGtw = variables.dbFactory.create("DocSnapshotGtw");
		var docSnapshotMetadataQry = docSnapshotGtw.getMetadataInfo(arguments.userID, arguments.docLetterTemplateCD, arguments.contextualKey);

		if (! docSnapshotMetadataQry.recordCount) {
			// original
			generateOriginalDocument(argumentCollection = argStruct);
			return true;
		} else {
			//return generateRevisedDocument(argumentCollection = argStruct);
			return false;
		}
	}

	public void function generateOriginalDocument(required numeric userID, required numeric docLetterTemplateCD, required numeric docTypeCD, required string contextualKey) {
		var docLetterTypeGTW = variables.dbFactory.create("DocLetterTypeGTW");
		var docLetterTypeCD = docLetterTypeGTW.getByCode("ORIGINAL").DOC_LETTER_TYPE_CD;

		var docSnapshotCD = saveDocument(arguments.userID, arguments.docLetterTemplateCD, arguments.docTypeCD, docLetterTypeCD, arguments.contextualKey);
		saveMetadata(docSnapshotCD, arguments.contextualKey);
	}

	private string function compareXML(required any currentTagXMLDoc, required any snapshotTagXMLDoc) {
		var nodeArray = "";
		var elem = "";
		var selectedElements = XmlSearch(arguments.currentTagXMLDoc, "//tag");
		var tagsWithChanges = "";

		for (var elem in selectedElements) {
			var nodeData = elem.XmlChildren;
			var snapshotElem = XmlSearch(arguments.snapshotTagXMLDoc, "//tag[ @code = '#elem.XmlAttributes["code"]#' ]");

			if (ArrayLen(elem.XmlChildren[1].XmlChildren)) {
				// rows
				var rowData = elem.XmlChildren[1].XmlChildren;
				var snapshotRowData = snapshotElem[1].XmlChildren[1].XmlChildren;

				if (ArrayLen(rowData) != ArrayLen(snapshotRowData)) {
					tagsWithChanges = ListAppend(tagsWithChanges, elem.XmlAttributes["code"]);
					return false;
				}

				for (var rowIdx = 1; rowIdx <= ArrayLen(rowData); rowIdx++) {
					var columnData = rowData[rowIdx].XmlChildren;
					var snapshotColumnData = snapshotRowData[rowIdx].XmlChildren;

					if (ArrayLen(columnData) != ArrayLen(snapshotColumnData)) {
						tagsWithChanges = ListAppend(tagsWithChanges, elem.XmlAttributes["code"]);
						return false;
					}

					for (var colIdx = 1; colIdx <= ArrayLen(columnData); colIdx++) {
						if (columnData[colIdx].XmlText != snapshotColumnData[colIdx].XmlText) {
							tagsWithChanges = ListAppend(tagsWithChanges, elem.XmlAttributes["code"]);
							return false;
						}
					}
				}
			} else {
				// single data
				if (snapshotElem[1].XmlChildren[1].XmlText != elem.XmlChildren[1].XmlText) {
					tagsWithChanges = ListAppend(tagsWithChanges, elem.XmlAttributes["code"]);
				}
			}
		}

		return tagsWithChanges;
	}

	private string function calculateChangeType(required numeric userID, required numeric docLetterTemplateCD, required string contextualKey) {
		/* get current snapshot */
		var docSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW");
		var docInstanceGtw = variables.dbFactory.create("docInstanceGtw");
		var contextStruct = decodeContextualKey(arguments.contextualKey);
		var changeTypeCode = "";

		var argStruct = {
			"userID" = arguments.userID,
			"docLetterTemplateCD" = arguments.docLetterTemplateCD
		};

		if (StructKeyExists(contextStruct, "TRAINING_SESSION_CD")) {
			argStruct["trainingSessionCD"] = contextStruct["TRAINING_SESSION_CD"];
		}

		var currentDocQry = docSnapshotDetailGTW.getUserCurrentLOA(argumentCollection=argStruct);

		/* template is different, it's a correction always */
		var snapshotBaseQry = variables.dbFactory.create("DocSnapshotBaseGTW").getByPK(currentDocQry.DOC_SNAPSHOT_CD);
		var snapshotBody = snapshotBaseQry.TEMPLATE_BODY;
		var templateBody = variables.dbFactory.create("DocLetterTemplateGTW").getByPK(arguments.docLetterTemplateCD).LETTER_TEMPLATE_BODY;

		var tagList = docInstanceGtw.getByPK(snapshotBaseQry.INSTANCE_ID).TAGS;

		/* revision vs correction - DOC_REVISION_TAGS */
		var revisionTags = docInstanceGtw.getTagsByChangeType(snapshotBaseQry.INSTANCE_ID, "REVISION").REVISION_TAGS;
		var correctionTags = docInstanceGtw.getTagsByChangeType(snapshotBaseQry.INSTANCE_ID, "CORRECTION").REVISION_TAGS;

		var currentTagContent = XMLParse(generateLetterContent(arguments.userID, arguments.contextualKey, tagList));
		var snapshotTagContent = XMLParse(variables.dbFactory.create("DocLetterDataGTW").getByPK(currentDocQry.DOC_LETTER_DATA_CD).XML_LETTER_CONTENT);
		var tagsWithChanges = compareXML(currentTagContent, snapshotTagContent);

		var StringUtils = CreateObject("component", "sis_core.model.util.StringUtils").init();
		var DocSettings = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocSettings").init(snapshotBaseQry.INSTANCE_ID);
		var docSettingsQry = DocSettings.getSettings();

		if ( ListLen(StringUtils.listDiff(revisionTags, tagsWithChanges)) != ListLen(revisionTags) ) {
			// revision tag changed

			changeTypeCode = "REVISION";
		} else if (
			( ListLen(StringUtils.listDiff(correctionTags, tagsWithChanges)) != ListLen(correctionTags) || ( snapshotBody != "" && snapshotBody != templateBody ) ) &&
			docSettingsQry.GENERATE_CORRECTION == 'Y'
		) {
			// correction tag changed or template changed

			changeTypeCode = "CORRECTION";
		}

		return changeTypeCode;
	}

	public boolean function generateRevisedDocument(required numeric userID, required numeric docLetterTemplateCD, required numeric docTypeCD, required string contextualKey) {
		var docLetterTypeGTW = variables.dbFactory.create("DocLetterTypeGTW");
		var docLetterTypeCD = docLetterTypeGTW.getByCode("REVISED").DOC_LETTER_TYPE_CD;

		var changeType = calculateChangeType(arguments.userID, arguments.docLetterTemplateCD, arguments.contextualKey);

		if (changeType != "") {
			var changeTypeCD = variables.dbFactory.create("LoaChangeTypeGTW").getByCode(changeType).DOC_CHANGE_TYPE_CD;
			var docSnapshotCD = saveDocument(arguments.userID, arguments.docLetterTemplateCD, arguments.docTypeCD, docLetterTypeCD, arguments.contextualKey, changeTypeCD);
			saveMetadata(docSnapshotCD, arguments.contextualKey);

			/* factor in instance settings; publish confirmed */
			try {
				var docSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW");
				var currentDocQry = docSnapshotDetailGTW.getUserCurrentLOA(argumentCollection=argStruct);

				if (currentDocQry.recordCount) {
					var snapshotBaseQry = variables.dbFactory.create("DocSnapshotBaseGTW").getByPK(currentDocQry.DOC_SNAPSHOT_CD);
					var DocSettings = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocSettings").init(snapshotBaseQry.INSTANCE_ID);
					var docSettingsQry = DocSettings.getSettings();

					if ( ( ( changeType eq "REVISION" AND docSettingsQry.AUTO_PUBLISH_REVISION eq "Y")
						OR (changeType eq "CORRECTION" AND docSettingsQry.AUTO_PUBLISH_CORRECTION eq "Y")) AND snapshotBaseQry.DOC_SIGN_STATUS neq "OUTSTANDING" ) {
							var generatedDocQry = docSnapshotGtw.getGeneratedLOAByTemplate(arguments.docLetterTemplateCD, arguments.userID, arguments.trainingSessionCD);
							if (generatedDocQry.recordcount EQ 1) {
								if (docSettingsQry.OVERWRITE_CURRENT eq "Y")
									publishConfirmed(self.docLetterTemplateCD, generatedDocQry.doc_snapshot_cd, 'Y');
								else
									publishConfirmed(self.docLetterTemplateCD, generatedDocQry.doc_snapshot_cd);
							}
					}
				}
			} catch(any e) {

			}

			return true;
		}

		return false;
		/*<cfif arguments.instanceId eq 2>
			<cfquery name="getStudentTraining" dbtype="query">
				select *
				from getStudentTraining
				where
					funding_type_desc = <cfqueryparam cfsqltype="cf_sql_varchar" value="OMH" /> and
					training_level_type_desc = <cfqueryparam cfsqltype="cf_sql_varchar" value="Resident" />
			</cfquery>
		</cfif>

		// check if instance is HAL TO-DO
		<cfif currentDocQry.recordcount eq 1 and  arguments.instanceId eq 2>
			<cfset getCurrentLOATraining = DOCSnapshot.getChanges(currentDocQry.doc_snapshot_cd)>
			<cfquery name="getCurrentLOATraining" dbtype="query">
				select *
				from getCurrentLOATraining
				where doc_snapshot_cd = <cfqueryparam  cfsqltype="cf_sql_numeric" value="#currentDocQry.doc_snapshot_cd#" />
			</cfquery>
			<cfset currentLOATrainingAgg = DocHelper.aggTrainingLines(getCurrentLOATraining)>
			<cfset stdTrainingAgg = DocHelper.aggTrainingLines(getStudentTraining)>
		</cfif>*/

		/* //check if instance is HAL TO-DO
		<cfif arguments.instanceId eq 2 and generate eq true and currentDocQry.recordcount eq 1 and loaChangeTypeQry.DOC_CHANGE_CODE eq "REVISION" and currentLOATrainingAgg eq stdTrainingAgg>
			<cfset generate = false>
			<cfset cntNoChanges = cntNoChanges + 1>
		</cfif>*/
	}

	public string function deleteUnconfirmed(required numeric docLetterTemplateCD, numeric trainingSessionCD = -1, string userIDList = "") {
		var docSnapshotGTW = variables.dbFactory.create("DocSnapshotGTW");
		var affectedStudents = docSnapshotGTW.deleteNotConfirmedByTemplate(arguments.docLetterTemplateCD, arguments.trainingSessionCD, arguments.userIDList);

		return affectedStudents;
	}

	public void function publishConfirmed(required numeric docLetterTemplateCD, required string snapshotCDList, string overwriteFlag = "N") {
		var docSnapshotGTW = variables.dbFactory.create("DocSnapshotGTW");

		docSnapshotGtw.publishConfirmedByTemplate(arguments.docLetterTemplateCD, arguments.snapshotCDList, variables.userID, arguments.overwriteFlag);
	}

	private struct function processTemplate(required numeric docLetterTemplateCD, required numeric trainingSessionCD, required string docTypeList, required string refersTo, required string userCDList) {
		var docTypeGTW = variables.dbFactory.create("DocLetterMapGTW");
		var LogicalCondition = variables.wirebox.getInstance('LogicalCondition@CORE');
		var QueryBuilder = variables.wirebox.getInstance('QueryBuilder@CORE');

		var groupQry = "";
		var userQry = "";
		var userList = "";
		var populationCondition = "";
		var contextKeys = "";
		var prefixedContextKeys = "";
		var unprefixedContextKeys = "";
		var resultStruct = {};

		/* use arguments as params for the condition */
		var condParams = {};

		if (arguments.trainingSessionCD neq -1) {
			condParams["TRAINING_SESSION_CD"] = arguments.trainingSessionCD;
		}

		var self = arguments;
		var lockName = "";

		/* get template groups */
		groupQry = docTypeGTW.getByTemplateCD(arguments.docLetterTemplateCD);

		prefixedContextKeys = "";
		unprefixedContextKeys = "";
		for (var k = 1; k <= ListLen(arguments.refersTo); k++) {
			var entity = ListGetAt(arguments.refersTo, k);
			var entityObj = QueryBuilder.getEntity(entity);
			contextKeys = entityObj.getKeyColumns();
			for (var kk = 1; kk <= ListLen(contextKeys); kk++) {
				prefixedContextKeys = ListAppend(prefixedContextKeys, "#entity#." & ListGetAt(contextKeys, kk));
				unprefixedContextKeys = ListAppend(unprefixedContextKeys, ListGetAt(contextKeys, kk));
			}
		}

		unprefixedContextKeys = ListRemoveDuplicates(unprefixedContextKeys);
		resultStruct["TOTAL_USERS"] = 0;
		resultStruct["CNT_GEN"] = 0;
		resultStruct["CNT_NOT_ELIG"] = 0;

		deleteUnconfirmed(arguments.docLetterTemplateCD, arguments.trainingSessionCD, arguments.userCDList);

		for (var j = 1; j <= groupQry.recordcount; j++) {
			populationCondition = LogicalCondition.getCondition(groupQry["RESTRICTION_CONDITION_ID"][j]).getCONDITION_TEXT();
			/* get condition keys from the refers_to */
			populationCondition = joinConditionWithRefersTo(populationCondition, prefixedContextKeys);
			/* if we have user id list as argument */
			if (arguments.userCDList neq "") {
				populationCondition &= " AND Trainee.USER_ID IN (" & arguments.userCDList & ")";
			}

			/* get user mass */
			StructDelete(condParams, "USER_ID");
			userQry = LogicalCondition.previewCondition(populationCondition, condParams);

			userList = ValueList(userQry.USER_ID);
			userList = ListRemoveDuplicates(userList);
			resultStruct["TOTAL_USERS"] += ListLen(userList);

			for (var user_cd in userList) {
				/* TODO: find a quicker solution (QoQ is even slower) */
				condParams["USER_ID"] = user_cd;
				userQry = LogicalCondition.previewCondition(populationCondition, condParams);

				/* encode contextual key */
				var contextualKey = "";
				var idx = 0;
				for (var key in unprefixedContextKeys) {
					idx++;
					contextualKey &= "#key#=#userQry[key][userQry.currentRow]#";
					if (idx != ListLen(unprefixedContextKeys)) {
						contextualKey &= "+";
					}
				}
				/* calculate user document */
				var lockName = '#user_cd#_#arguments.docLetterTemplateCD#';
				lock name=lockName type="exclusive" timeout="1000" {
					if ( calculateUserDocument(user_cd, arguments.docLetterTemplateCD, groupQry["DOC_TYPE_CD"][j], contextualKey, arguments.docTypeList) ) {
						resultStruct["CNT_GEN"]++;
					} else {
						resultStruct["CNT_NOT_ELIG"]++;
					}
				}
			}
		}
		return resultStruct;
	}

	public void function run(required numeric trainingSessionCD, string docTypeList = "", string docLetterTemplateCDList = "", string userCDList = "", boolean bLogProcess = true) {
		/* get templates */
		var docLetterTemplateGtw = variables.dbFactory.create("DocLetterTemplateGTW");
		var templateQry = docLetterTemplateGtw.getByCDList(arguments.docLetterTemplateCDList,true);

		var operationList = "";

		operationList = variables.DocProcess.setOperationList(variables.steps);
		title = "Generate letters";

		if (arguments.bLogProcess) {
			var mainProcessID = variables.DocProcess.startprocess("#title#","Start processing");
		}
		var docProcessId = "";
		var resStruct = "";

		for (var i = 1; i <= templateQry.recordcount; i++) {
			try {
				if (variables.DocProcess.isProcessActive(templateQry["DOC_LETTER_TEMPLATE_CD"][i]))
					break;

				/*if ( ListFind(operationList, "PROCESS_INSTANCE") ) {
					try {
						variables.DocProcess.calculateInstance(arguments.trainingSessionCD, templateQry["DOC_LETTER_TEMPLATE_CD"][i]);
					} catch (any e) {
						variables.DocProcess.logProcessProgress(mainProcessID, "Calculating process instance error -- #e.detail#::#e.message#","ERROR");
						variables.DocProcess.endProcess(mainProcessID, "Processing finished with errors");
						exit;
					}
				}*/

				if (arguments.bLogProcess) {
					docProcessId = variables.DocProcess.startprocess("Generate letters for #templateQry["LETTER_TEMPLATE_NAME"][i]# - Session: #arguments.trainingSessionCD#", "Start generating", mainProcessID);
				}

				if (templateQry.STATUS[i] eq "ACTIVE") {
					resStruct = processTemplate(templateQry["DOC_LETTER_TEMPLATE_CD"][i], arguments.trainingSessionCD, arguments.docTypeList, templateQry["REFERS_TO"][i], arguments.userCDList);
					if (arguments.bLogProcess) {
						variables.DocProcess.logProcessProgress(docProcessId, "User(s): #resStruct.TOTAL_USERS#", "PROGRESS");
						variables.DocProcess.logProcessProgress(docProcessId, "Generated: #resStruct.CNT_GEN#; Not Eligible: #resStruct.CNT_NOT_ELIG#","PROGRESS");
						variables.DocProcess.logProcessProgress(docProcessId, "End Session: #arguments.trainingSessionCD# - #templateQry["LETTER_TEMPLATE_NAME"][i]#", "PROGRESS");
						variables.DocProcess.endProcess(docProcessId, "#templateQry["LETTER_TEMPLATE_NAME"][i]# letters for session #arguments.trainingSessionCD# successfully generated");
					}
				} else {
					var deletedTemplates = deleteUnconfirmed(templateQry["DOC_LETTER_TEMPLATE_CD"][i], arguments.trainingSessionCD, arguments.userCDList);
					if (arguments.bLogProcess) {
						variables.DocProcess.logProcessProgress(docProcessId, "Inactive template; Deleted: #ListLen(deletedTemplates)#","PROGRESS");
						variables.DocProcess.logProcessProgress(docProcessId, "End Session: #arguments.trainingSessionCD# - #templateQry["LETTER_TEMPLATE_NAME"][i]#", "PROGRESS");
						variables.DocProcess.endProcess(docProcessId, "#templateQry["LETTER_TEMPLATE_NAME"][i]# letters for session #arguments.trainingSessionCD# successfully generated");
					}
				}
			} catch (any e) {
				if (arguments.bLogProcess) {
					variables.DocProcess.logProcessProgress(docProcessId, "Process template error -- #e.detail#::#e.message#", "ERROR");
					variables.DocProcess.endProcess(docProcessId, "#templateQry["LETTER_TEMPLATE_NAME"][i]# letters for session #arguments.trainingSessionCD# finished with errors");
				}
			}
		}
		if (arguments.bLogProcess) {
			variables.DocProcess.endProcess(mainProcessID, "Generate Letter finished.");
		}
	}
}