<cfcomponent displayname="DocHelper" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocHelper">
		<cfargument name="instanceID" type="numeric" required="false" default="1">
		<cfset super.init() />
		<cfset variables.instance_id = arguments.instanceID>
		<cfreturn this/>
	</cffunction>

	<cffunction name="getLOALetterTypes" access="public" output="false" returntype="query">
		<cfset var DocLetterTypeGTW = variables.dbFactory.create("DocLetterTypeGTW") />
		<cfset var DocLetterTypeQry = DocLetterTypeGTW.getAll() />
		<cfreturn DocLetterTypeQry />
	</cffunction>

	<cffunction name="getLOATraineeTypes" access="public" output="false" returntype="query">
		<cfset var externalService = createObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.ExternalService").init() />
		<cfset var qry = externalService.getTraineeTypes() />
		<cfreturn qry />
	</cffunction>

	<cffunction name="getLOATraineeStatus" access="public" output="false" returntype="query">
		<cfset var externalService = createObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.ExternalService").init() />
		<cfset var qry = externalService.getTraineeStatus() />
		<cfreturn qry />
	</cffunction>

	<cffunction name="getLOAEmailType" access="public" output="false" returntype="query">
		<cfset var LoaEmailTypeGTW = variables.dbFactory.create("LoaEmailTypeGTW") />
		<cfset var LoaEmailTypeQry = LoaEmailTypeGTW.getAll() />
		<cfreturn LoaEmailTypeQry />
	</cffunction>

	<cffunction name="getDocType" access="public" output="false" returntype="query">
		<cfset var DocTypeGTW = variables.dbFactory.create("DocTypeGTW", true) />
		<cfset var DocTypeQry = DocTypeGTW.getAll() />
		<cfreturn DocTypeQry />
	</cffunction>

	<cffunction name="getDocTemplates" access="public" output="false" returntype="query">
		<cfset var DocLetterTemplateGTW = variables.dbFactory.create("DocLetterTemplateGTW", true) />
		<cfset var DocLetterTemplateQry = DocLetterTemplateGTW.getAll("", true) />
		<cfreturn DocLetterTemplateQry />
	</cffunction>

	<cffunction name="getSettings" access="public" output="false" returntype="query">
	<cfscript>
		var docSettingsGTW = variables.dbFactory.create("DocSettingsGTW");
		return docSettingsGTW.getAll(variables.instance_id);
	</cfscript>

	</cffunction>

	<cffunction name="getDocReportTrainees" returntype="query" access="public" output="false" hint="">
		<cfargument name="filterVO" type="LoaFilterFVO" required="true">
		<cfset var DocSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW") />
		<cfset var TraineQry = DocSnapshotDetailGTW.getDocReportTrainees(variables.instance_id,arguments.filterVO) />
		<cfreturn TraineQry />
    </cffunction>

	<cffunction name="getDocGeneratedTrainees" returntype="query" access="public" output="false" hint="">
	<cfargument name="filterVO" type="LoaFilterFVO" required="true">
		<cfset var DocSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW") />
		<cfset var TraineQry = DocSnapshotDetailGTW.getDocGeneratedTrainees(variables.instance_id,arguments.filterVO) />
		<cfreturn TraineQry />
    </cffunction>

    <cffunction name="getAllTrainees" returntype="query" access="public" output="false" hint="">
    	<cfargument name="stdName" type="string" required="true">

		<cfset var ExternalService = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.ExternalService" ).init() />
		<cfreturn ExternalService.getAllStudents4Auto(arguments.stdName) />
    </cffunction>

	<cffunction name="getStudentName" returntype="query" access="public" output="false" hint="">
		<cfargument name="student_id" type="numeric" required="true">
		<cfset var externalService = createObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.ExternalService").init() />
		<cfset var qry = externalService.getStudentbyID(arguments.student_id) />
		<cfreturn qry />
    </cffunction>

	<cffunction name="getTrainingSessions" access="public" returntype="query">
		<cfset var ExternalService = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.ExternalService" ).init() />
		<cfreturn ExternalService.getTrainingSessions() />
	</cffunction>

	<cffunction name="getTrainingLevels" access="public" returntype="query">
		<cfset var ExternalService = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.ExternalService" ).init() />
		<cfreturn ExternalService.getTrainingLevels() />
	</cffunction>

	<cffunction name="getCurrentTrainingSession" access="public" returntype="query">
		<cfset var ExternalService = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.ExternalService" ).init() />
		<cfreturn ExternalService.getCurrentTrainingSession() />
	</cffunction>

	<cffunction name="getTrainingSessions4Confirm" access="public" returntype="query">
		<cfset var docSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW") />
		<cfreturn docSnapshotDetailGTW.getTrainingSessions4Confirm(variables.instance_id) />
	</cffunction>

	<cffunction name="getTrainingSessions4Report" access="public" returntype="query">
		<cfset var docSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW") />
		<cfreturn docSnapshotDetailGTW.getTrainingSessions4Report() />
	</cffunction>

	<cffunction name="getDepartment4Report" access="public" returntype="query">
		<cfargument name="trainingSessionCD" type="any" required="false" default="">
		<cfset var docSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW",false) />
		<cfreturn docSnapshotDetailGTW.getDepartment4Report(arguments.trainingSessionCD) />
	</cffunction>

	<cffunction name="getProgramByDepartment4Report" access="public" returntype="query">
		<cfargument name="departmentCD" type="any" required="false" default="">
		<cfargument name="trainingSessionCD" type="any" required="false" default="">
		<cfset var docSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW") />
		<cfreturn docSnapshotDetailGTW.getProgramByDepartment4Report(arguments.departmentCD, arguments.trainingSessionCD) />
	</cffunction>

	<cffunction name="getDepartments" access="public" output="false" returntype="query">
		<cfset var externalService = createObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.ExternalService").init() />
		<cfset var qry = externalService.getDepartments() />
		<cfreturn qry />
	</cffunction>

	<cffunction name="getProgramsByDepartment" access="public" output="false" returntype="query">
		<cfargument name="departmentCd" type="any" required="true">
		<cfset var externalService = createObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.ExternalService").init() />
		<cfset var qry = "" />

		<cfif not isNumeric(arguments.departmentCd)>
			<cfset arguments.departmentCd = -1 />
		</cfif>
		<cfset qry = externalService.getProgramsByDepartment(arguments.departmentCd) />
		<cfreturn qry />
	</cffunction>

	<cffunction name="getLoaChangeType" access="public" output="false" returntype="query">
		<cfset var LOAChangeTypeGtw = variables.dbFactory.create("LOAChangeTypeGtw") />
		<cfset var ChangeTypeQry = LOAChangeTypeGtw.getAll() />
		<cfreturn ChangeTypeQry />
	</cffunction>

	<cffunction name="getInstance" access="public" output="false" returntype="query">
		<cfset var DocInstances = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocInstances").init()>
		<cfset var LOAInstanceQry = DocInstances.getInstanceInfo(variables.instance_id)>

		<cfreturn LOAInstanceQry />
	</cffunction>

	<cffunction name="aggTrainingLines" access="public" output="false" returntype="string">
		<cfargument name="trainingData" type="query" required="true"/>

		<cfset var trainingPeriods = ArrayNew(1)>
		<cfset var currentTrainingEndDt = "">
		<cfset var currentTrainingLevel = "">
		<cfset var output = "">

		<cfloop query="arguments.trainingData">
			<cfif currentTrainingEndDt eq "" or DateDiff("d",currentTrainingEndDt,arguments.trainingData.start_dt) neq 1 or arguments.trainingData.training_level_desc neq currentTrainingLevel>
				<cfset ArrayAppend(trainingPeriods,{training_level_desc=arguments.trainingData.training_level_desc,start_dt=arguments.trainingData.start_dt,end_dt=arguments.trainingData.end_dt})>
			<cfelse>
				<cfset trainingPeriods[ArrayLen(trainingPeriods)]["end_dt"] = arguments.trainingData.end_dt>
			</cfif>
			<cfset currentTrainingEndDt = arguments.trainingData.end_dt>
			<cfset currentTrainingLevel = arguments.trainingData.training_level_desc>
		</cfloop>
		<cfsavecontent variable="output">
		<cfoutput>
		<cfloop from="1" to="#ArrayLen(trainingPeriods)#" index="idx">
			<cfif idx neq 1> and </cfif>
			from <strong>#trainingPeriods[idx]["start_dt"]#</strong> to <strong>#trainingPeriods[idx]["end_dt"]#</strong> as a <strong>#trainingPeriods[idx]["training_level_desc"]#</strong>
		</cfloop>
		</cfoutput>
		</cfsavecontent>
		<cfreturn output>
	</cffunction>
</cfcomponent>