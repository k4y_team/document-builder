<cfcomponent displayname="DocInstances" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocInstances">
		<cfset super.init()>
		<cfreturn this/>
	</cffunction>

	<cffunction name="getInstances" access="public" returntype="any" output="false">
		<cfargument name="returnType" type="string" required="false" default="query">
		<cfargument name="columnList" type="string" required="false" default="*">
		<cfargument name="isUsed" type="any" required="false" default="false">
		<cfargument name="applyRestrictions" type="any" required="false" default="true">


		<cfset var docInstanceGtw = variables.dbDocumentFactory.create("docInstanceGtw", true) />
		<cfset var documentInstancesQry = docInstanceGtw.getAll(arguments.isUsed,arguments.applyRestrictions) />

		<cfif arguments.returnType EQ 'json'>
			<cfset var instanceStruct = {}/>
			<cfset var instanceArray  = []/>
			<cfset instanceArray = QueryToArray(documentInstancesQry, arguments.columnList)/>
			<cfset instanceStruct.success = true/>
			<cfset instanceStruct.total = documentInstancesQry.recordCount/>
			<cfset instanceStruct.data = instanceArray/>
			<cfreturn SerializeJson(instanceStruct)/>
		</cfif>
		<cfreturn documentInstancesQry />
	</cffunction>

	<cffunction name="getInstanceInfo" access="public" output="false" returntype="query">
		<cfargument name="instanceID" type="numeric" required="true">

		<cfset var docInstanceGtw = variables.dbDocumentFactory.create("docInstanceGtw") />
		<cfset var instanceQry = docInstanceGtw.getByPK(arguments.instanceID) />
		<cfreturn instanceQry />
	</cffunction>

	<cffunction name="getByCode" access="public" output="false" returntype="query">
		<cfargument name="instanceCode" type="string" required="true">

		<cfset var docInstanceGtw = variables.dbDocumentFactory.create("docInstanceGtw", true) />

		<cfset var instanceQry = docInstanceGtw.getByCode(arguments.instanceCode) />
		<cfreturn instanceQry />
	</cffunction>

	<cffunction name="getActions" access="public" output="false" returntype="query">
		<cfscript>
			var DocLetterActionGTW = variables.dbDocumentFactory.create("DocLetterActionGTW");
			return DocLetterActionGTW.getAll();
		</cfscript>
	</cffunction>

	<cffunction name="getInstanceConfig" access="public" output="false" returntype="struct">
		<cfargument name="instanceID" type="numeric" required="true">

		<cfset instanceConfig = {}>
		<cfset var docInstanceGtw = variables.dbDocumentFactory.create("docInstanceGtw") />
		<cfset var instanceQry = docInstanceGtw.getByPK(arguments.instanceID) />

		<cfif instanceQry.INSTANCE_CONFIG NEQ "">
			<cfset var instanceConfig = DeserializeJSON(instanceQry.INSTANCE_CONFIG) />
		</cfif>
		<cfreturn instanceConfig>
	</cffunction>
</cfcomponent>