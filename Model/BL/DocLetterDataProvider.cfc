<cfcomponent displayname="DocLetterDataProvider" output="false" extends="ServiceBase">
	<cfproperty name="data" type="xml" >
	<cfproperty name="docSnapshotCD" type="numeric" >

	<cffunction name="init" access="public" output="false" returntype="DocLetterDataProvider">
		<cfargument name="instanceID" type="numeric" required="true">
		<cfargument name="data" type="xml" required="true">
		<cfargument name="docSnapshotCD" type="numeric" required="true">
		<cfargument name="DOC_LETTER_TEMPLATE_CD" type="numeric" required="false">
		<cfscript>
			super.init();
			var docType = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocType").init(arguments.instanceID);
			variables.instance_id = arguments.instanceID;
			variables.data = arguments.data;
			variables.docSnapshotCD = arguments.docSnapshotCD;
			variables.tags = docType.getTags();
			variables.medsis_date_format = request.cbConfigSettings.formatStruct.dateFormat;
			if (arguments.docSnapshotCD EQ "-1") {
				variables.doc_letter_template_cd = arguments.DOC_LETTER_TEMPLATE_CD;
			}
			return this;
		</cfscript>
	</cffunction>

	<cffunction name="getTagSimple" access="public" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfscript>
			var d = XmlSearch(variables.data, "/tags/tag[@code='#arguments.code#']/data");
			var tag = getTag(arguments.code);
			var misc = "";
			var val = "";

			if(tag.TAG_TYPE eq "SIMPLE"){
				switch(tag.COLLECTION){
					case "MISC":
						misc = getMisc();
					break;
					case "SIGNATURE":
						misc = getSignatures();
					break;
					case "LOA":
						misc = getLOA();
					break;
					case "INPUT_SIGNATURES":
						misc = getInputSignatures();
					break;
					default:
						if(ArrayLen(d) gt 0){
							return d[1].XmlText;
						}
					break;
				}
			}

			if(isDefined("misc.#arguments.code#")) {
				val = misc[arguments.code];

				return val;
			}
			return "";
		</cfscript>
	</cffunction>

	<cffunction name="getTagComplex" access="public" output="false" returntype="query">
		<cfargument name="code" type="string" required="true">
		<cfscript>
			var rows = XmlSearch(variables.data, "/tags/tag[@code='#arguments.code#']/data/row");
			var tag = getTag(arguments.code);
			var aLen =  ArrayLen(rows);
			var tagQ = QueryNew("");
			var columnList = "";

			if(tag.TAG_TYPE neq "SIMPLE"  and aLen gt 0){
				for(i=1; i lte aLen; i++){
					columns = xmlSearch( rows[i], "column" );
					if(i eq 1){
					/*first build query with columns*/
						for(j = 1; j lte ArrayLen(columns); j++){
							/*make column list from xml data*/
							columnList = ListAppend(columnList, columns[j].XmlAttributes["name"]);
						}
						tagQ = QueryNew(columnList);
					}
					QueryAddRow(tagQ);
					for(j = 1; j lte ArrayLen(columns); j++){
						QuerySetCell(tagQ, columns[j].XmlAttributes["name"], columns[j].XmlText);
					}
				}
			}

			return tagQ;
		</cfscript>
	</cffunction>

	<cffunction name="getTag" access="private" output="false" returnType="query">
		<cfargument name="code" type="string" required="true">
		<cfset var tagQ = "" />

		<cfquery name="tagQ" dbtype="query">
			SELECT * FROM variables.tags WHERE TAG_CODE = '#arguments.code#'
		</cfquery>
		<cfreturn tagQ />
	</cffunction>

	<cffunction name="getMisc" access="private" output="false" returnType="query">
		<cfscript>
			var qry = QueryNew("CURRENT_DATE");
			var cnt = 0;
			QueryAddRow(qry); cnt++;
			QuerySetCell(qry, "CURRENT_DATE", DateFormat(Now(), variables.medsis_date_format));
			return qry;
		</cfscript>
	</cffunction>

	<cffunction name="getSignatures" access="private" output="false" returnType="query">
		<cfscript>
			if (variables.docSnapshotCD EQ "-1") {
				var DocLetterTemplateGTW = variables.dbFactory.create("DocLetterTemplateGTW");
				var qry = DocLetterTemplateGTW.getTemplateSignature(variables.instance_id, variables.doc_letter_template_cd);
			} else {
				var docSnapshotBaseGTW = variables.dbFactory.create("DocSnapshotBaseGTW");
				var qry = docSnapshotBaseGTW.getSignatures(variables.instance_id, variables.docSnapshotCD);
			}
			return qry;
		</cfscript>
	</cffunction>

	<cffunction name="getInputSignatures" access="private" output="false" returnType="query">
		<cfscript>
			var qry = QueryNew("INPUT_SIGNATURE,INPUT_DATE");

			if (variables.docSnapshotCD EQ "-1") {
				QueryAddRow(qry);
				var DocLetterTemplateGTW = variables.dbFactory.create("DocLetterTemplateGTW");
				var templateQry = DocLetterTemplateGTW.getByTemplateCD(variables.doc_letter_template_cd);
				if (templateQry.ACTION_CODE EQ "AGREE") {
					QuerySetCell(qry, "INPUT_SIGNATURE", "");
				} else {
					QuerySetCell(qry, "INPUT_SIGNATURE", "Jon Doe");
				}
				QuerySetCell(qry, "INPUT_DATE", DateFormat(Now(), variables.medsis_date_format));
			} else {
				var snapshotQry = variables.dbFactory.create("DocSnapshotBaseGTW").getByPK(variables.docSnapshotCD);
				var docInstanceGtw = variables.dbFactory.create("docInstanceGtw");
				var LOAInstanceQry = docInstanceGtw.getByPK(variables.instance_id);

				QueryAddRow(qry);
				if (snapshotQry.DOC_SIGNATURE eq "" AND LOAInstanceQry.ACTION_CODE NEQ "AGREE") {
					var UserService = variables.wirebox.getInstance("API@UserDirectory").getUserService(application.cbController.getSetting('ud_instance_id'));
					var User = UserService.getUserInfo(snapshotQry.USER_ID);
					QuerySetCell(qry, "INPUT_SIGNATURE", User.getFIRST_NAME() & " " & User.getLAST_NAME());
				} else {
					QuerySetCell(qry, "INPUT_SIGNATURE", snapshotQry.DOC_SIGNATURE);
				}
				if (snapshotQry.DOC_SUBMITTED_DT eq "") {
					QuerySetCell(qry, "INPUT_DATE", DateFormat(Now(), variables.medsis_date_format));
				} else {
					QuerySetCell(qry, "INPUT_DATE", DateFormat(snapshotQry.DOC_SUBMITTED_DT, variables.medsis_date_format));
				}
			}
			return qry;
		</cfscript>
	</cffunction>

	<cffunction name="getLOA" access="private" output="false" returnType="query">
		<cfscript>
			var docSnapshotDetailGTW = variables.dbFactory.create("DocSnapshotDetailGTW");
			var qry = docSnapshotDetailGTW.getSnapshots(variables.docSnapshotCD);

			return qry;
		</cfscript>
	</cffunction>

</cfcomponent>