<cfcomponent displayname="DocLetterMap" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocLetterMap">
		<cfargument name="instanceId" type="numeric" required="false" default="1" />
		<cfscript>
			super.init();
			variables.instanceId = arguments.instanceId;
			variables.ExternalService = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.ExternalService" ).init();
			return this;
		</cfscript>
	</cffunction>

	<cffunction name="getAll" access="public" output="false" returntype="query">
		<cfargument name="searchFilter" type="any" required="false">
		<cfargument name="sortDataVO" type="any" required="false">

		<cfset docLetterMapGTW = variables.dbDocumentFactory.create("DocLetterMapGTW", true) />
		<cfset docLetterMapQry = docLetterMapGTW.getAll(searchFilter, sortDataVO) />
		<cfreturn docLetterMapQry>
	</cffunction>

	<cffunction name="getLOAMapByDesc" access="public" output="false" returntype="query">
        <cfargument name="LOAMapDesc" type="string" required="true">
        <cfset docLetterMapGTW = variables.dbDocumentFactory.create("DocLetterMapGTW") />
        <cfset docLetterMapQry = docLetterMapGTW.getByDesc(variables.instanceID, arguments.LOAMapDesc) />
        <cfreturn docLetterMapQry>
    </cffunction>

	<cffunction name="getSnapshotByDocType" access="public" output="false" returntype="query">
		<cfargument name="instance_id" type="numeric" required="true">
		<cfargument name="doc_type_cd" type="numeric" required="true">
		<cfargument name="doc_letter_template_cd" type="numeric" required="true">

		<cfset docSnapshotDetailGTW = variables.dbDocumentFactory.create("DocSnapshotDetailGTW") />
		<cfset docSnapshotDetailQry = docSnapshotDetailGTW.getSnapshotByDocType(arguments.instance_id, arguments.doc_type_cd, arguments.doc_letter_template_cd) />
		<cfreturn docSnapshotDetailQry>
	</cffunction>

	<cffunction name="getLoaMapHistoryByDocTypeCD" access="public" output="false" returntype="query">
        <cfargument name="doc_type_cd" type="any" required="true">
        <cfset DocLetterMapHistoryGTW = variables.dbDocumentFactory.create("DocLetterMapHistoryGTW") />
        <cfset docLetterMapHistoryQry = DocLetterMapHistoryGTW.getByDOC_TYPE_CD(arguments.doc_type_cd) />
        <cfreturn docLetterMapHistoryQry>
    </cffunction>

	<cffunction name="getDocLetterMap" access="public" output="false" returntype="DocLetterMapFVO">
		<cfargument name="docTypeCD" type="numeric" required="true">
		<cfscript>
			var docLetterMapFVO = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocLetterMapFVO").init();
			var letterMapQry = getTemplate(arguments.docTypeCD);
			docLetterMapFVO.initFromQuery(LetterMapQry);

			var LogicalCondition = variables.wirebox.getInstance('LogicalCondition@CORE');
			docLetterMapFVO.setRESTRICTION_CONDITION(LogicalCondition.getCondition(letterMapQry.RESTRICTION_CONDITION_ID));

			return docLetterMapFVO;
		</cfscript>
	</cffunction>

	<cffunction name="getTemplate" access="public" output="false" returntype="query">
		<cfargument name="docTypeCD" type="numeric" required="true">
		<cfscript>
			var DocLetterMapGTW = variables.dbDocumentFactory.create("DocLetterMapGTW");
			return DocLetterMapGTW.getByTypeCD(arguments.docTypeCD);
		</cfscript>
	</cffunction>

	<cffunction name="populate" access="public" output="false" returntype="DocLetterMapFVO">
		<cfargument name="data" type="struct" required="true">

		<cfset var DocLetterMapFVO = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocLetterMapFVO").init() />
		<cfset var LogicalCondition = variables.wirebox.getInstance('LogicalCondition@CORE') />
		<cfset var LogicalConditionData = LogicalCondition.getCondition() />
		<cfset DocLetterMapFVO.initFromStruct(arguments.data) />
		<cfset LogicalConditionData.initFromStruct(arguments.data, 'restriction_condition') />
		<cfset DocLetterMapFVO.initFromStruct(arguments.data) />
		<cfset DocLetterMapFVO.setRESTRICTION_CONDITION(LogicalConditionData) />

		<cfreturn DocLetterMapFVO>
	</cffunction>

	<cffunction name="save" returntype="boolean" access="public" output="false" hint="">
        <cfargument name="data" type="DocLetterMapFVO" required="true"/>

        <cfif not validate(arguments.data)>
            <cfreturn false />
        </cfif>
		<cfset var LogicalCondition = variables.wirebox.getInstance('LogicalCondition@CORE') />
		<cfset var restrictionConditionId = LogicalCondition.saveCondition(arguments.data.getRESTRICTION_CONDITION()) />

        <cftransaction action="begin">
	        <cftry>
		        <cfset var docLetterMapDAO = variables.dbDocumentFactory.create("DocLetterMapDAO") />
				<cfset var DocLetterMapHistoryDAO = variables.dbDocumentFactory.create("DocLetterMapHistoryDAO") />
				<cfset var docTypeDAO = variables.dbDocumentFactory.create("DocTypeDAO") />
				<cfset var docTypeVO = "" />
		        <cfset var DocLetterMapHistoryVO = "">
				<cfset var DocLetterMapVO = "" />

					<cfif arguments.data.getDOC_TYPE_CD() eq "">
						<cfset docTypeVO = variables.dbDocumentFactory.create("DocTypeVO") />
					<cfelse>
						<cfset docTypeVO = docTypeDAO.read(arguments.data.getDOC_TYPE_CD()) />
					</cfif>
					<cfset docTypeCd = docTypeVO.getDOC_TYPE_CD() />
					<cfset docTypeVO.setINSTANCE_ID(arguments.data.getINSTANCE_ID()) />
					<cfset docTypeVO.setTYPE_DESC(arguments.data.getTYPE_DESC()) />
					<cfset docTypeVO.setTYPE_CODE(UCase(Replace(arguments.data.getTYPE_DESC(), " ", "_", "all"))) />
					<cfset docTypeVO.setRESTRICTION_CONDITION_ID(restrictionConditionId)>

					<cfif docTypeCd eq "">
						<cfset docTypeVO.setCREATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id) />
						<cfset docTypeVO.setCREATION_DT(Now()) />
						<cfset docTypeCd = docTypeDAO.add(docTypeVO) />
					<cfelse>
						<cfset docTypeVO.setMODIFICATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id) />
						<cfset docTypeVO.setMODIFICATION_DT(Now()) />
						<cfset docTypeDAO.update(docTypeVO) />
					</cfif>

					<!--- <cfset DocLetterMapVO = docLetterMapDAO.read(docTypeCd) /> --->

					<!--- save history --->
					<!--- <cfif arguments.data.getDOC_LETTER_TEMPLATE_CD() NEQ DocLetterMapVO.getDOC_LETTER_TEMPLATE_CD() >
											<cfloop list="#arguments.data.getDOC_LETTER_TEMPLATE_CD()#" index="templateCD">
								           		<cfset DocLetterMapHistoryVO = variables.dbDocumentFactory.create("DocLetterMapHistoryVO").init() />
												<cfset DocLetterMapHistoryVO.setDOC_TYPE_CD(arguments.data.getDOC_TYPE_CD() ) />
								            	<cfset DocLetterMapHistoryVO.setDOC_TEMPLATE_CD(templateCD) />
												<cfset DocLetterMapHistoryVO.setCREATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)/>
								            	<cfset DocLetterMapHistoryVO.setCREATION_DT(Now())/>
								            	<cfset DocLetterMapHistoryDAO.add(DocLetterMapHistoryVO) />
								            </cfloop>
										</cfif> --->

					<cfset DocLetterMapDAO.delete(docTypeCd) />

					<cfloop list="#arguments.data.getDOC_LETTER_TEMPLATE_CD()#" index="templateCD">
						<cfset DocLetterMapVO = variables.dbDocumentFactory.create("DocLetterMapVO").init() />

						<cfif DocLetterMapVO.getDOC_TYPE_CD() EQ "">
							<cfset DocLetterMapVO.setDOC_TYPE_CD(docTypeCd) />
						</cfif>

						<cfset DocLetterMapVO.setDOC_LETTER_TEMPLATE_CD(templateCD) />
	            		<cfset DocLetterMapVO.setMODIFICATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)/>
	            		<cfset DocLetterMapVO.setMODIFICATION_DT(Now())/>
						<cfset docLetterMapDAO.add(DocLetterMapVO) />
					</cfloop>
				<cfset variables.MsgBox.success("Category successfully saved.") />
	            <cftransaction action="commit"/>
		        <cfcatch>
					<cftransaction action="rollback"/>
						<cfdump var="#cfcatch#" /><cfabort />
					<cfset variables.MsgBox.error("An error occured while saving category.") />
		            <cfreturn false />
		        </cfcatch>
	        </cftry>
        </cftransaction>
		<!--- <cfset LogicalCondition.bindHandler(restrictionConditionId, {code = "GENERATE_LETTERS", params = {}}) /> --->
        <cfreturn true />
    </cffunction>

	<cffunction name="delete" returntype="boolean" access="public" output="false" hint="">
        <cfargument name="docTypeCd" type="numeric" required="true"/>
		<cfset var LogicalCondition = variables.wirebox.getInstance('LogicalCondition@CORE') />
        <cfset var docLetterMapDAO = variables.dbDocumentFactory.create("DocLetterMapDAO") />
		<cfset var DocLetterMapHistoryDAO = variables.dbDocumentFactory.create("DocLetterMapHistoryDAO") />
		<cfset var LoaEmailTemplateMapDAO = variables.dbDocumentFactory.create("LoaEmailTemplateMapDAO") />
		<cfset var docTypeDAO = variables.dbDocumentFactory.create("DocTypeDAO") />

		<cfset var DocLetterMapData = getDocLetterMap(arguments.docTypeCd) />
		<cfif DocLetterMapData.getRESTRICTION_CONDITION().getCONDITION_ID() NEQ "">
			<!--- <cfset LogicalCondition.unBindHandler(DocLetterMapData.getRESTRICTION_CONDITION().getCONDITION_ID()) /> --->
			<cfset LogicalCondition.deleteCondition(DocLetterMapData.getRESTRICTION_CONDITION().getCONDITION_ID()) />
		</cfif>
        <cftransaction action="begin">
	        <cftry>
				<cfset docLetterMapDAO.delete(arguments.docTypeCd) />
            	<cfset DocLetterMapHistoryDAO.deleteByDocType(arguments.docTypeCd) />
				<cfset LoaEmailTemplateMapDAO.deleteByDocType(arguments.docTypeCd) />
				<cfset docTypeDAO.delete(arguments.docTypeCd) />

				<cfset variables.MsgBox.success("Category successfully deleted.") />
	            <cftransaction action="commit"/>
		        <cfcatch>
					<cftransaction action="rollback"/>
					<cfset variables.MsgBox.error("Could not delete category.") />
		            <cfreturn false />
		        </cfcatch>
	        </cftry>
        </cftransaction>
        <cfreturn true />
    </cffunction>

	<cffunction name="validate" access="private" returntype="boolean" output="false">
        <cfargument name="data" type="DocLetterMapFVO" required="true"/>
        <cfset var valid = true />
		<cfset var docLetterMapQry = getLOAMapByDesc( Trim(arguments.data.getTYPE_DESC()) ) />

		<cfif not Len( arguments.data.getINSTANCE_ID() )>
			<cfset variables.MsgBox.error("Letter is required.","instance_id") />
            <cfset valid = false />
		 </cfif>
		<cfif not Len( arguments.data.getTYPE_DESC() )>
			<cfset variables.MsgBox.error("Name is required.","type_desc") />
            <cfset valid = false />
		 </cfif>

		 <!--- check if Training Category exists --->
        <cfif (docLetterMapQry.recordCount eq 1 and docLetterMapQry.doc_type_cd neq arguments.data.getDOC_TYPE_CD()) or docLetterMapQry.recordCount gt 1>
			<cfset variables.MsgBox.error("Name is already used by a different group.") />
            <cfset valid = false />
        </cfif>

		<cfreturn valid>
	</cffunction>

	<cffunction name="generateRestrictions" access="public" returntype="void" output="false">
		<cfargument name="letterTemplateCD" type="string" required="false" default="*">

		<cfset var DocTypeGTW = variables.dbDocumentFactory.create('DocTypeGTW')>
		<cfset var qDocType = DocTypeGTW.getAllWithMap(variables.instanceId,arguments.letterTemplateCD)>
		<cfset var LogicalCondition = variables.wirebox.getInstance('LogicalCondition@CORE')>

		<cfset var DocTypeRules = StructNew()>
		<cfloop query="qDocType">
			<cfset var ruleString = LogicalCondition.getCondition(qDocType.RESTRICTION_CONDITION_ID).getCONDITION_TEXT() />
			<cfset DocTypeRules[qDocType.DOC_TYPE_CD] = ruleString>
		</cfloop>
		<cfset request.DOC_TYPE_RULES = DocTypeRules>
	</cffunction>

	<cffunction name="getValidationReport" access="public" returntype="query" output="false">
		<cfargument name="filterData" type="sis_core.model.SearchFilter" required="true">

		<cfset generateRestrictions()>
		<cfset var qMappingValidation = variables.ExternalService.getForLoaMappingValidation(request.DOC_TYPE_RULES, arguments.filterData)>

		<cfreturn qMappingValidation>
	</cffunction>

</cfcomponent>