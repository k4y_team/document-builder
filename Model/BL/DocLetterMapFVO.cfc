<cfcomponent name="DocLetterMapFVO" displayname="DocLetterMapFVO" extends="sis_core.model.ModelBase" output="false">
	<cfproperty name="DOC_LETTER_TEMPLATE_CD" type="string" />
	<cfproperty name="DOC_TYPE_CD" type="string" />
	<cfproperty name="LETTER_TEMPLATE_NAME" type="string" />
	<cfproperty name="TYPE_DESC" type="string" />
	<cfproperty name="STATUS" type="string" />
	<cfproperty name="RESTRICTION_CONDITION" type="any" hint="LogicalConditionData" />
	<cfproperty name="INSTANCE_ID" type="string" />

    <cffunction name="init" access="public" returntype="DocLetterMapFVO" output="false" displayname="init" hint="I initialize a DocLetterMapFVO">
        <cfset super.init()>
		<cfset variables.DOC_LETTER_TEMPLATE_CD = "" />
		<cfset variables.DOC_TYPE_CD = "" />
		<cfset variables.LETTER_TEMPLATE_NAME = "" />
		<cfset variables.TYPE_DESC = "" />
		<cfset variables.STATUS = "" />
		<cfset variables.RESTRICTION_CONDITION= "" />
		<cfset variables.INSTANCE_ID = "" />
        <cfreturn this />
    </cffunction>

    <cffunction name="setDOC_LETTER_TEMPLATE_CD" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.DOC_LETTER_TEMPLATE_CD = arguments.val />
    </cffunction>
    <cffunction name="getDOC_LETTER_TEMPLATE_CD" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_LETTER_TEMPLATE_CD />
    </cffunction>

    <cffunction name="setDOC_TYPE_CD" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.DOC_TYPE_CD = arguments.val />
    </cffunction>
    <cffunction name="getDOC_TYPE_CD" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_TYPE_CD />
    </cffunction>

    <cffunction name="setLETTER_TEMPLATE_NAME" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.LETTER_TEMPLATE_NAME = arguments.val />
    </cffunction>
    <cffunction name="getLETTER_TEMPLATE_NAME" access="public" returntype="any" output="false">
        <cfreturn variables.LETTER_TEMPLATE_NAME />
    </cffunction>

    <cffunction name="setTYPE_DESC" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.TYPE_DESC = arguments.val />
    </cffunction>
    <cffunction name="getTYPE_DESC" access="public" returntype="any" output="false">
        <cfreturn variables.TYPE_DESC />
    </cffunction>

    <cffunction name="setSTATUS" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.STATUS = arguments.val />
    </cffunction>
    <cffunction name="getSTATUS" access="public" returntype="any" output="false">
        <cfreturn variables.STATUS />
    </cffunction>

	<cffunction name="setINSTANCE_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.INSTANCE_ID = arguments.val />
    </cffunction>
    <cffunction name="getINSTANCE_ID" access="public" returntype="any" output="false">
        <cfreturn variables.INSTANCE_ID />
    </cffunction>

    <cffunction name="setRESTRICTION_CONDITION" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.RESTRICTION_CONDITION = arguments.val />
    </cffunction>
    <cffunction name="getRESTRICTION_CONDITION" access="public" returntype="any" output="false">
        <cfreturn variables.RESTRICTION_CONDITION />
    </cffunction>

    <cffunction name="initFromQuery" access="public" returntype="DocLetterMapFVO" output="false">
        <cfargument name="queryData" type="query" required="true" />

        <cfset super.initFromQuery(arguments.queryData) />

        <cfset var templateList = "" />
        <cfloop query="arguments.queryData">
            <cfset templateList = ListAppend(templateList, arguments.queryData.DOC_LETTER_TEMPLATE_CD) />
        </cfloop>

        <cfset setDOC_LETTER_TEMPLATE_CD(templateList) />
        <cfreturn this />

    </cffunction>

</cfcomponent>