<cfcomponent name="DocLetterTemplateFVO" displayname="DocLetterTemplateFVO" extends="sis_core.model.ModelBase" output="false">
    <cfproperty name="DOC_LETTER_TEMPLATE_CD" type="string" />
    <cfproperty name="LETTER_TEMPLATE_CODE" type="string" />
    <cfproperty name="LETTER_TEMPLATE_NAME" type="string" />
    <cfproperty name="HTML_LETTER_TEMPLATE_BODY" type="string" />
    <cfproperty name="INSTRUCTIONS" type="string" />
    <cfproperty name="STATUS" type="string" />
	<cfproperty name="DOC_SIGNATURE_CD" type="string" />
	<cfproperty name="DOC_SIGNATURE_FILE_NAME" type="string" />
	<cfproperty name="INSTANCE_ID" type="string" />

    <cffunction name="init" access="public" returntype="DocLetterTemplateFVO" output="false" displayname="init" hint="I initialize a DocLetterTemplateFVO">
        <cfset super.init()>
		<cfset variables.DOC_LETTER_TEMPLATE_CD = "" />
		<cfset variables.LETTER_TEMPLATE_CODE = "" />
		<cfset variables.LETTER_TEMPLATE_NAME = "" />
		<cfset variables.HTML_LETTER_TEMPLATE_BODY = "" />
        <cfset variables.INSTRUCTIONS = "" />
		<cfset variables.STATUS = "" />
		<cfset variables.DOC_SIGNATURE_CD = "" />
		<cfset variables.DOC_SIGNATURE_FILE_NAME = "" />
		<cfset variables.INSTANCE_ID = "" />

        <cfreturn this />
    </cffunction>

    <cffunction name="setDOC_LETTER_TEMPLATE_CD" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.DOC_LETTER_TEMPLATE_CD = arguments.val />
    </cffunction>
    <cffunction name="getDOC_LETTER_TEMPLATE_CD" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_LETTER_TEMPLATE_CD />
    </cffunction>

    <cffunction name="setLETTER_TEMPLATE_CODE" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.LETTER_TEMPLATE_CODE = arguments.val />
    </cffunction>
    <cffunction name="getLETTER_TEMPLATE_CODE" access="public" returntype="any" output="false">
        <cfreturn variables.LETTER_TEMPLATE_CODE />
    </cffunction>

    <cffunction name="setLETTER_TEMPLATE_NAME" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.LETTER_TEMPLATE_NAME = arguments.val />
    </cffunction>
    <cffunction name="getLETTER_TEMPLATE_NAME" access="public" returntype="any" output="false">
        <cfreturn variables.LETTER_TEMPLATE_NAME />
    </cffunction>

    <cffunction name="setHTML_LETTER_TEMPLATE_BODY" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.HTML_LETTER_TEMPLATE_BODY = arguments.val />
    </cffunction>
    <cffunction name="getHTML_LETTER_TEMPLATE_BODY" access="public" returntype="any" output="false">
        <cfreturn variables.HTML_LETTER_TEMPLATE_BODY />
    </cffunction>

    <cffunction name="setINSTRUCTIONS" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.INSTRUCTIONS = arguments.val />
    </cffunction>
    <cffunction name="getINSTRUCTIONS" access="public" returntype="any" output="false">
        <cfreturn variables.INSTRUCTIONS />
    </cffunction>

    <cffunction name="setSTATUS" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.STATUS = arguments.val />
    </cffunction>
    <cffunction name="getSTATUS" access="public" returntype="any" output="false">
        <cfreturn variables.STATUS />
    </cffunction>

    <cffunction name="setDOC_SIGNATURE_CD" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.DOC_SIGNATURE_CD = arguments.val />
    </cffunction>
    <cffunction name="getDOC_SIGNATURE_CD" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_SIGNATURE_CD />
    </cffunction>

    <cffunction name="setDOC_SIGNATURE_FILE_NAME" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.DOC_SIGNATURE_FILE_NAME = arguments.val />
    </cffunction>
    <cffunction name="getDOC_SIGNATURE_FILE_NAME" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_SIGNATURE_FILE_NAME />
    </cffunction>

	<cffunction name="setINSTANCE_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />
        <cfset variables.INSTANCE_ID = arguments.val />
    </cffunction>
    <cffunction name="getINSTANCE_ID" access="public" returntype="any" output="false">
        <cfreturn variables.INSTANCE_ID />
    </cffunction>

</cfcomponent>