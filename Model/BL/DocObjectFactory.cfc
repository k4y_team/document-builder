<cfcomponent displayname="DocObjectFactory" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocObjectFactory">
		<cfset variables.moduleConfig = application.moduleManager.getModuleConfig('DOCUMENT_BUILDER_MODULE') />
		<cfreturn this>
	</cffunction>

	<cffunction name="create" access="public" output="false" returntype="any">
		<cfargument name="objectName" type="string" required="true">

		<cfif variables.moduleConfig.isSettingDefined(arguments.objectName)>
			<cfset var LoaObjPath = variables.moduleConfig.getSetting(arguments.objectName)>
			<cfset var loaObj = CreateObject("component","#LoaObjPath#")>
		<cfelse>
			<cfset var loaObj = CreateObject("component","#arguments.objectName#")>
		</cfif>
		<cfreturn loaObj />
	</cffunction>
</cfcomponent>