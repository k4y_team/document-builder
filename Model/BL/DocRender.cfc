<cfcomponent displayname="DocRender" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocRender">
		<cfargument name="instanceID" type="numeric" required="true">
		<cfargument name="renderTypeCode" type="string" required="false" default="HTML" hint="HTML/PDF">

		<cfset super.init()>
		<cfset variables.instance_id = arguments.instanceID>

		<cfset DocHelper = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocHelper").init(variables.instance_id) />
		<cfset instanceQry = DocHelper.getInstance()>
		<cfset moduleConfig = application.ModuleManager.getModuleConfig("DOCUMENT_BUILDER_MODULE")>
		<cfset variables.clientInfo = moduleConfig.getSetting('CLIENTINFO')>
		<cfset variables.renderTypeCode = arguments.renderTypeCode>
		<cfreturn this>
	</cffunction>

	<cffunction name="renderLOA" access="public" output="false" returntype="string">
		<cfargument name="docSnapshotCD" type="numeric" required="true">
		<cfargument name="userID" type="numeric" required="false" default="-1">
		<cfscript>
			var docSnapshotDetailGTW = variables.dbDocumentFactory.create("DocSnapshotDetailGTW");
			var docSnapshotDetailQry = docSnapshotDetailGTW.getByPk(arguments.docSnapshotCD);
			var xmlContent = "";

			/*get snapshot xml data*/
			if (docSnapshotDetailQry.DOC_LETTER_DATA_CD NEQ "") {
				var docLetterDataGTW = variables.dbDocumentFactory.create("DocLetterDataGTW");
				var docLetterDataQry = docLetterDataGTW.getByPk(docSnapshotDetailQry.DOC_LETTER_DATA_CD);
				xmlContent = docLetterDataQry.XML_LETTER_CONTENT;
			} else {
				var DocLetterTemplateGTW = variables.dbDocumentFactory.create("DocLetterTemplateGTW");
				var contextKey = "STUDENT_ID=" & docSnapshotDetailQry.STUDENT_ID & "+TRAINING_SESSION_CD=" & docSnapshotDetailQry.TRAINING_SESSION_CD;
				var docTemplateQry = DocLetterTemplateGTW.getByPK(docSnapshotDetailQry.DOC_LETTER_TEMPLATE_CD);
				var LoaDataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.LoaDataProvider").init();
                if (arguments.userID gt 0) {
                    var DocGenerate = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocGenerate").init(arguments.userID);
                } else {
                    var DocGenerate = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocGenerate").init(variables.SessionStorage.getVar('LoggedInUser').user_id);
                }

				xmlContent = DocGenerate.generateLetterContent(LoaDataProvider, contextKey, docTemplateQry.TAGS);
			}

			var xmlData = "";
			var docTypeRender = "";
			if(IsXML(xmlContent)){
				/*render xml data*/
				xmlData = XmlParse(xmlContent);
				docTypeRender = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocTypeRender").init(variables.instance_id, docSnapshotDetailQry.DOC_TYPE_CD, docSnapshotDetailQry.DOC_LETTER_TEMPLATE_CD, variables.renderTypeCode);
				return docTypeRender.render(xmlData, arguments.docSnapshotCD);
			}else{
				return "";
			}
		</cfscript>
	</cffunction>

	<cffunction name="renderTesLOA" access="public" output="false" returntype="string">
		<cfargument name="docSnapshotCD" type="numeric" required="true">
		<cfargument name="userID" type="numeric" required="false" default="-1">

		<cfscript>

			var snapshotQry = variables.dbDocumentFactory.create('DocSnapshotBaseGTW').getByPK(arguments.docSnapshotCD);


			var xmlContent = "";


			var dummyContent=generatedummyLetterContent();

			var xmlData = "";
			var docTypeRender = "";
			try{
			 	if(IsXML(dummyContent)){
					/*render xml data*/
					xmlData = XmlParse(dummyContent);
					docTypeRender = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocTypeRender").init(variables.instance_id, 37, snapshotQry.DOC_LETTER_TEMPLATE_CD, variables.renderTypeCode);
					return docTypeRender.render(xmlData, arguments.docSnapshotCD);
				}else{
					return "";
				}
			  }
            catch (any ex){
            	WriteLog (text="[#ex.type#] #ex.message# for document: #arguments.docSnapshotCD#", type="Error",file="documentbuilder.log")
				return renderDocumentError();
            }

		</cfscript>
	</cffunction>

	<cffunction name="dummyLOA" access="public" output="true">
		<cfargument name="DOC_LETTER_TEMPLATE_CD" type="numeric" required="true">
		<cfscript>
	        var xmlData = "";
	        var dummyContent=generatedummyLetterContent();
			if(IsXML(dummyContent)){
				/*render xml data*/
				xmlData = XmlParse(dummyContent);
				docTypeRender = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocTypeRender").init(variables.instance_id, -1, arguments.DOC_LETTER_TEMPLATE_CD, variables.renderTypeCode);
				return docTypeRender.render(xmlData,-1,arguments.DOC_LETTER_TEMPLATE_CD);
			} else {
				return "";
			}
		</cfscript>
	</cffunction>

	<cffunction name="renderDocumentError" access="private" output="false" returntype="string">

		<cfset var content = "" />

		<cfsavecontent variable="content">
			<cfoutput>
				<div style="text-align:center;font-size:20px;color:red;"><br /><br />
					<strong>Document content could not be rendered!</strong><br />&nbsp;
				</div>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="generateDummyLetterContent" access="private" output="false" returntype="string">
		<cfoutput>
			<cfsavecontent variable="letterContent">
				<tags>
					<tag code="HEADER_INFO">
						<data>
							<row id="1">
								<column name="file_path">#XmlFormat("file:///#ExpandPath('/medsis')#/includes/#application.cbcontroller.getSetting('clientInfo').LOGO.BW#")#</column>
								<column name="file_url">#XmlFormat("#application.cbcontroller.getSetting('applicationBaseURL')#/includes/#application.cbcontroller.getSetting('clientInfo').LOGO.BW#")#</column>
								<column name="client_name">#application.cbcontroller.getSetting('clientInfo').NAME#</column>
								<column name="faculty">#XmlFormat("#clientInfo.FACULTY#")#</column>
								<column name="office">#XmlFormat("#clientInfo.OFFICE#")#</column>
								<column name="address">#XmlFormat("#clientInfo.ADDRESS#")#</column>
								<column name="phone">#XmlFormat("#clientInfo.PHONE#")#</column>
								<column name="ext">#XmlFormat("#clientInfo.EXT#")#</column>
								<column name="fax">#XmlFormat("#clientInfo.FAX#")#</column>
								<column name="website">#XmlFormat("#clientInfo.WEBSITE#")#</column>
								<column name="email">postgd@mcmaster.ca</column>
							</row>
						</data>
					</tag>
					<tag code="HEADER_INFO_GRAD_STUDIES">
						<data>
							<row id="1">
								<column name="image_name">#XmlFormat("#application.cbcontroller.getSetting('applicationBaseURL')#/includes/img/GRAD_STUDIES_TES_LOGO.png")#</column>
								<column name="client_name">#application.cbcontroller.getSetting('clientInfo').NAME#</column>
							</row>
						</data>
					</tag>
					<tag code="HEADER_INFO_MIDWIFERY">
						<data>
							<row id="1">
								<column name="image_name">#XmlFormat("#application.cbcontroller.getSetting('applicationBaseURL')#/includes/img/MIDWIFERY_TES_HEADER.png")#</column>
								<column name="client_name">#application.cbcontroller.getSetting('clientInfo').NAME#</column>
							</row>
						</data>
					</tag>
					<tag code="HEADER_INFO_PT_TES">
						<data>
							<row id="1">
								<column name="image_name">#XmlFormat("#application.cbcontroller.getSetting('applicationBaseURL')#/includes/img/PT_TES_HEADER.png")#</column>
								<column name="client_name">#application.cbcontroller.getSetting('clientInfo').NAME#</column>
							</row>
						</data>
					</tag>
					<tag code="HEADER_INFO_OT_TES">
						<data>
							<row id="1">
								<column name="image_name">#XmlFormat("#application.cbcontroller.getSetting('applicationBaseURL')#/includes/img/OT_TES_HEADER.png")#</column>
								<column name="client_name">#application.cbcontroller.getSetting('clientInfo').NAME#</column>
							</row>
						</data>
					</tag>
					<tag code="HEADER_INFO_NURSING">
						<data>
							<row id="1">
								<column name="image_name">#XmlFormat("#application.cbcontroller.getSetting('applicationBaseURL')#/includes/img/NURSING_TES_HEADER.png")#</column>
								<column name="client_name">#application.cbcontroller.getSetting('clientInfo').NAME#</column>
							</row>
						</data>
					</tag>
					<tag code="OPHRDC_NUMBER">
						<data>20004934</data>
					</tag>
					<tag code="DOCUMENT_DATE">
						<data>#DateFormat(now(),"mmmm dd, yyyy")#</data>
					</tag>
					<tag code="TRAINEE_NUMBER">
						<data>11120001</data>
					</tag>
					<tag code="TRAINEE_NAME">
						<data>Jon Doe</data>
					</tag>
					<tag code="TRAINEE_PICTURE">
						<data>#application.cbcontroller.getSetting("applicationBaseURL")#includes/images/no_photo.png</data>
					</tag>
					<tag code="TRAINEE_MINC">
						<data>CAMD-1234-5678</data>
					</tag>
					<tag code="TRAINEE_MAILING_PHONE">
						<data>100-000-0000</data>
					</tag>
					<tag code="TRAINEE_ADDRESS">
						<data>
							1111&lt;br/&gt;
							1111 HARBOUR ST&lt;br/&gt;
							TORONTO, Ontario, M543J 1Z1&lt;br/&gt;
							Canada
						</data>
					</tag>
					<tag code="PROGRAM_DESC">
						<data>Otolaryngology</data>
					</tag>
					<tag code="DEPARTMENT_DESC">
						<data>Otolaryngology</data>
					</tag>
					<tag code="TRAINEE_PROFILE">
						<data>
							<row id="1">
								<column name="medical_school">Northeastern Ohio University, UNITED STATES</column>
								<column name="graduation_year">2000</column>
								<column name="cpso_num">11111111</column>
								<column name="cpso_type">Postgraduate Education</column>
							</row>
						</data>
					</tag>
					<tag code="TRAINING_LINE">
						<data>
							<row id="1">
								<column name="program_desc">Otolaryngology</column>
								<column name="training_level_desc">
									Postgrad Year 3
								</column>
								<column name="funding_source">Ont Min Hlth- Ministry Funded</column>
								<column name="training_status_desc">Active</column>
								<!--- <column name="time">1.00</column> --->
								<column name="group">1</column>
								<column name="pool">A</column>
								<column name="start_dt">01-Jul-#DateFormat(Now(), 'YYYY')#</column>
								<column name="end_dt">30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</column>
								<column name="trainee_type">Trainee</column>
							</row>
						</data>
					</tag>
					<tag code="TRAINING_LINE_EXTENDED">
						<data>
							<row id="1">
								<column name="program_desc">Otolaryngology</column>
								<column name="training_level_desc">Postgrad Year 3</column>
								<column name="start_dt">July 1, #DateFormat(Now(), 'YYYY')#</column>
								<column name="end_dt">June 30, #DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</column>
							</row>
						</data>
					</tag>
					<tag code="TRAINING_DETAIL">
						<data>
							<row id="1">
								<column name="program_desc">Otolaryngology</column>
								<column name="training_level_desc">
									Postgrad Year 3
								</column>
								<column name="funding_source">Ont Min Hlth- Ministry Funded</column>
								<column name="training_status_desc">Active</column>
								<!--- <column name="time">1.00</column> --->
								<column name="group">1</column>
								<column name="pool">A</column>
								<column name="start_dt">01-Jul-#DateFormat(Now(), 'YYYY')#</column>
								<column name="end_dt">30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</column>
								<column name="trainee_type">Trainee</column>
							</row>
						</data>
					</tag>
					<tag code="TRAINING_SESSION">
						<data>#DateFormat(Now(), 'YYYY')# - #DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</data>
					</tag>
					<tag code="TRAINING_START_DATE">
						<data>01-Jul-#DateFormat(Now(), 'YYYY')#</data>
					</tag>
					<tag code="TRAINING_END_DATE">
						<data>30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</data>
					</tag>
					<tag code="NETWORK_ACCESS_ID">
						<data>123</data>
					</tag>
					<tag code="NETWORK_PASSWORD">
						<data>456</data>
					</tag>
					<tag code="REG_FEE">
						<data>1111</data>
					</tag>
					<tag code="TRAINEE_EMAIL">
						<data>john@doe.com</data>
					</tag>
					<tag code="TRAINEE_SCHOOL">
						<data>Home School</data>
					</tag>
					<tag code="REQUIREMENT_DETAIL">
						<data>
							<row id="1">
								<column name="cpso_num">105895</column>
								<column name="cpso_expiry_dt">30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</column>
								<column name="cmpa_num">200112969</column>
								<column name="cmpa_expiry_dt">30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</column>
								<column name="mask_type_desc">3M 8210 - regular</column>
								<column name="mask_expiry_dt">30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</column>
								<column name="scrub_size">L</column>
								<column name="visa_expiry_dt">30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</column>
							</row>
						</data>
					</tag>
					<tag code="ROTATION_DETAIL">
						<data>
							<row id="1">
								<column name="rotation_desc">MRI</column>
								<column name="rotation_program_desc">Diagnostic Radiology</column>
								<column name="location_desc">McMaster Children's Hospital</column>
								<column name="start_dt">01-Jul-#DateFormat(Now(), 'YYYY')#</column>
								<column name="end_dt">30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</column>
								<column name="supervisor_names">Adams, Amy;Doe, Jane</column>
                                <column name="rotation_type">Single-Site</column>
                                <column name="service_format">Core</column>
                                <column name="service">Diagnostic Radiology</column>
							</row>
						</data>
					</tag>
					<tag code="MIN_REQ_EXPIRY_DATE">
						<data>30-Jun-#DateFormat(DateAdd('m', 12, Now()), 'YYYY')#</data>
					</tag>
				</tags>
			</cfsavecontent>
		</cfoutput>
		<cfreturn trim(letterContent)>
	</cffunction>

	<cffunction name="generatedummyEmail" access="private" output="false" returntype="string">
		<cfoutput>
			<cfsavecontent variable="letterContent">
				<tags>
					<tag code="user_name">
						<data>Jon Doe</data>
					</tag>
					<tag code="login_info">
						<data>PIN: <b>94543</b><br/><br/>If you forgot your password, please click on the "Forgot your pin and/or password?" link found on the Login page. You will be prompted to enter your email address. The system will send your pin and password to the email address on record. Please ensure you use the same email address used in previous communications with the PGME Office.</data>
					</tag>
				</tags>
			</cfsavecontent>
		</cfoutput>
		<cfreturn trim(letterContent)>
	</cffunction>

	<cffunction name="dummyEmail" access="public" output="true">
		<cfargument name="DOC_Email_TEMPLATE_CD" type="numeric" required="true">
		<cfscript>
	        var xmlData = "";
	        var dummyContent=generatedummyEmail();
			if(IsXML(dummyContent)){
				/*render xml data*/
				xmlData = XmlParse(dummyContent);
				docTypeRender = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.LoaEmailTemplate").init();
				return docTypeRender.renderEmail(xmlData,arguments.DOC_EMAIL_TEMPLATE_CD);
			}else{
				return "";
			}
		</cfscript>
	</cffunction>

</cfcomponent>