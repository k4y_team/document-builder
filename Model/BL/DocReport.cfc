<cfcomponent displayname="DocReport" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocReport">
		<cfset super.init() />

		<cfreturn this/>
	</cffunction>


	<cffunction name="getDocuments" access="public" output="false" returntype="query">
		<cfargument name="filterVO" type="sis_core.model.SearchFilter" required="false" />

		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceConfig = documentInstances.getInstanceConfig(arguments.filterVO.getFILTER_DATA_STRUCT().DOC_LETTER_INSTANCE_CD) />
		<cfset providerEntity = queryBuilder.GETENTITY(instanceConfig.CONFIG_DATA_PROVIDER_ENTITY) />
		<cfset EntityRestrictions =  wirebox.getInstance(name='sis_core.model.blAutomation.EntityRestrictions')/>
		<cfset userSessionData = application.wirebox.getInstance(dsl='coldbox:plugin:SessionStorage' ).getVar("LoggedInUser")>
		<cfif StructKeyExists (arguments.filterVO.getFILTER_DATA_STRUCT(),"USER_ROLE_CODE") and arguments.filterVO.getFILTER_DATA_STRUCT().USER_ROLE_CODE EQ "ADMIN">
			<cfset programRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_PROGRAM',userId=userSessionData.USER_ID,securityCode="DOC_REPORT") />
			<cfset courseRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_COURSE',userId=userSessionData.USER_ID,securityCode="DOC_REPORT") />
			<cfset locationRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_LOCATION',userId=userSessionData.USER_ID,securityCode="DOC_REPORT") />
		<cfelse>
			<cfset programRestrictionSql = "*" />
			<cfset courseRestrictionSql = "*" />
			<cfset locationRestrictionSql = "*" />
		</cfif>

		<cfset reportQrySql = providerEntity.getSQL('REPORT_QRY') >
		<cfset orderbyList = "USER_NAME,DOC_SETUP_NAME" />
		<cfif (arguments.filterVO.getSORT_OPTIONS().getSortString_UC() NEQ "") >
 				<cfset orderbyList = "#arguments.filterVO.getSORT_OPTIONS().getSortString()# NULLS LAST,USER_NAME,DOC_SETUP_ID,TO_DATE(DOC_PUBLISHED_DT) DESC,DOC_SETUP_NAME" />
		</cfif>
		<cfset reportSql = "Select F.*, COUNT(distinct F.USER_NAME||'_'||F.DOC_SETUP_NAME) OVER() AS total_records, DENSE_RANK() OVER  (order by #orderbyList#) AS ORDER_IDX  from (#reportQrySQl#) F  ORDER BY USER_NAME,DOC_SETUP_ID,TO_DATE(DOC_PUBLISHED_DT) DESC">
		<cfif arguments.filterVO.getTABLE_PAGE().GETEND_ROW() gt 0>
			<cfset reportSql = "SELECT J.*  FROM (#reportSql#) J WHERE ORDER_IDX >= #arguments.filterVO.getTABLE_PAGE().GETSTART_ROW()# AND ORDER_IDX <= #arguments.filterVO.getTABLE_PAGE().GETEND_ROW()# ORDER BY ORDER_IDX ASC">
		</cfif>
		<cfset data = DeserializeJson(arguments.filterVO.getFILTER_DATA_STRUCT().params)>
		<cfif programRestrictionSql NEQ "*">
			<cfset data.PROGRAM_RESTRICTIONS_LIST = listToArray(programRestrictionSql,",",false,true)>
		</cfif>
		<cfif courseRestrictionSql NEQ "*">
			<cfset data.COURSE_RESTRICTIONS_LIST = listToArray(courseRestrictionSql,",",false,true)>
		</cfif>
		<cfif locationRestrictionSql NEQ "*">
			<cfset data.LOCATION_RESTRICTIONS_LIST = listToArray(locationRestrictionSql,",",false,true)>
		</cfif>
		<cfset structAppend(data,arguments.filterVO.getFILTER_DATA_STRUCT(),false)>
		<cfset documentReportQry = queryBuilder.runQuery(sql=reportSql,datasource=providerEntity.GETENTITYDATASOURCE(),params=data)/>

		<cfreturn documentReportQry />
	</cffunction>

	<cffunction name="getProcessDocuments" access="public" output="false" returntype="query">
		<cfargument name="data" type="struct" required="false" />
        <cfset arguments.data.SETUP_ID = arguments.data.DOC_SETUP_ID>
		<cfset arguments.data.DOC_LETTER_INSTANCE_CD = arguments.data.instance_id>
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceConfig = documentInstances.getInstanceConfig(arguments.data.INSTANCE_ID) />
		<cfset providerEntity = queryBuilder.GETENTITY(instanceConfig.CONFIG_DATA_PROVIDER_ENTITY) />
		<cfset reportQrySql = providerEntity.getSQL('REPORT_QRY') >
        <cfset documentReportQry = queryBuilder.runQuery(sql=reportQrySql,datasource=providerEntity.GETENTITYDATASOURCE(),params=arguments.data)/>
		<cfreturn documentReportQry />
	</cffunction>

	<cffunction name="saveDocChanges" access="public" output="false" returntype="any">
		<cfargument name="params" type="any" required="false">

		<cfquery name="insertMetadata" datasource="#variables.dbDocumentFactory.getDatasource()#">
			MERGE INTO DOC_SNAPSHOT_CHANGES D USING
				( SELECT
				    #arguments.params.DOC_SNAPSHOT_CD# AS DOC_SNAPSHOT_CD,
				    '#arguments.params.KEY#' AS KEY,
				    '#arguments.params.VALUE#' AS VALUE
				FROM
				    DUAL
				)
				S ON (
					D.DOC_SNAPSHOT_CD = S.DOC_SNAPSHOT_CD AND D.KEY=S.KEY
				) WHEN NOT MATCHED THEN INSERT ( D.DOC_SNAPSHOT_CHANGES_CD,D.DOC_SNAPSHOT_CD,D.KEY,D.VALUE )
				VALUES ( DOC_SNAPSHOT_CHANGES_SEQ.NEXTVAL,S.DOC_SNAPSHOT_CD,S.KEY,S.VALUE )
				WHEN MATCHED THEN UPDATE
					SET D.VALUE = S.VALUE
		</cfquery>
	</cffunction>

	<cffunction name="getDocumentsForPrint" access="public" returntype="query">
		<cfargument name="filterVO" type="sis_core.model.SearchFilter" required="false" />

		<cfset data = arguments.filterVO.getFILTER_DATA_STRUCT()>
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
 		<cfset sql ="SELECT
					    EP.EVALUATION_PERIOD,
					    A.DOC_SNAPSHOT_CD,
					    D.LAST_NAME||', '||D.FIRST_NAME AS USER_NAME,
					    S.DOC_SETUP_NAME AS REPORT_NAME
					FROM
					    DOC_SNAPSHOT_BASE A,
					    DOC_SETUP S,
					    {fbx_datasource}.EVAL_SUPERVISOR D,
					    {fbx_datasource}.EVAL_SUPERVISOR_USER E,
					    DOC_SNAPSHOT_EVAL_PERIOD EP
					WHERE
					        A.DOC_SNAPSHOT_CD = EP.DOC_SNAPSHOT_CD(+)
					    AND A.DOC_SETUP_ID = S.DOC_SETUP_ID
					    AND D.SUPERVISOR_ID = E.SUPERVISOR_ID
					    AND A.USER_ID = E.USER_ID
					    {% if (DATA.DOC_SNAPSHOT_CD is defined) and (DATA.DOC_SNAPSHOT_CD !='')%}
					     	AND  ({{macros.listSplit('A.DOC_SNAPSHOT_CD',DATA.DOC_SNAPSHOT_CD,500)}})
					    {% endif %}
					ORDER BY USER_NAME"
					    >
		<cfset documentsQry = queryBuilder.runQuery(sql=sql,datasource = variables.datasource ,params=data)/>
		<cfreturn documentsQry />
	</cffunction>

	<cffunction name="geteligibleUsersQry" access="public" returntype="query">
		<cfargument name="data" type="struct" default="#StructNew()#">
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
 		<cfset sql ="SELECT	A.*
					FROM
    				doc_setup_eligible_users A
						where 1=1
						{% if (DATA.DOC_SETUP_ELIGIBLE_USER_ID is defined) and (DATA.DOC_SETUP_ELIGIBLE_USER_ID !='')%}
							AND  ({{macros.listSplit('A.doc_setup_eligible_user_id',DATA.DOC_SETUP_ELIGIBLE_USER_ID,500)}})
					    {% endif %}
					    {% if (DATA.DOC_SETUP_ID is defined) and (DATA.DOC_SETUP_ID !='')%}
					    	AND  ({{macros.listSplit('A.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					    {% endif %}
					    {% if (DATA.USER_ID is defined) and (DATA.USER_ID !='')%}
					     	AND  ({{macros.listSplit('A.USER_ID',DATA.USER_ID,500)}})
					    {% endif %}"
					    >
		<cfset eligibleUsersQry = queryBuilder.runQuery(sql=sql,datasource = variables.datasource ,params=arguments.data)/>
		<cfreturn eligibleUsersQry />
	</cffunction>
</cfcomponent>