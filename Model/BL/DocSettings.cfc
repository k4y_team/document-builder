<cfcomponent displayname="DocSettings" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocSettings">
		<cfargument name="instanceID" type="numeric" required="false" default="0"/>
		<cfscript>
			super.init();
    		var docHelper = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocHelper").init(arguments.instanceID);
			variables.instance_id = arguments.instanceID;
			variables.instanceQry = DOCHelper.getInstance();
			return this;
		</cfscript>
	</cffunction>

	<cffunction name="getSettings" access="public" output="false" returntype="query">
		<cfscript>
			var docSettingsGTW = variables.dbDocumentFactory.create("DocSettingsGTW");
			var settingsQry = docSettingsGTW.getAll(variables.instance_id);
			return settingsQry;
		</cfscript>
	</cffunction>

	<cffunction name="save" returntype="boolean" access="public" output="false" hint="">
		<cfargument name="data" type="DocSettingsFVO" required="true"/>
		<cfset var success = true />

		<cfif validate(arguments.data)>
			<cftransaction action="begin">
		        <cftry>
					<cfset var DocSettingsDAO = variables.dbDocumentFactory.create("DocSettingsDAO") />
			        <cfset var DocSettingsGTW = variables.dbDocumentFactory.create("DocSettingsGTW") />
			        <cfset var InstanceSettingsQry = DocSettingsGTW.getByPK(variables.instance_id) />
					<cfset var DocSettingsVO = variables.dbDocumentFactory.create("DocSettingsVO") />

					<cfset DocSettingsVO = variables.populator.populateFromObject(DocSettingsVO, arguments.data, 'dbName')/>
					<cfset DocSettingsVO.setSIGN_IMAGES_PATH('Files/Signatures')>
					<cfif variables.instanceQry.INSTANCE_NAME EQ "HAL">
						<cfset DocSettingsVO.setSIGN_IMAGES_PATH('Files/Signatures/HAL')>
					</cfif>
					<cfif InstanceSettingsQry.recordCount EQ 0>
						<cfset DocSettingsDAO.add(DocSettingsVO) />
					<cfelse>
			            <cfset DocSettingsVO.setMODIFICATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)/>
			            <cfset DocSettingsVO.setMODIFICATION_DT(Now())/>
						<cfset DocSettingsDAO.update(DocSettingsVO) />
					</cfif>

					<cfset variables.MsgBox.success("Settings successfully saved.")>
		            <cftransaction action="commit"/>
			        <cfcatch>
						<cftransaction action="rollback"/>
						<cfset variables.MsgBox.error("Could Not saved settings.")>
			            <cfreturn false />
			        </cfcatch>
		        </cftry>
			</cftransaction>
		<cfelse>
			<cfset success = false />
		</cfif>

		<cfreturn success />
	</cffunction>

	<cffunction name="validate" access="private" returntype="boolean" output="false">
		<cfargument name="data" type="DocSettingsFVO" required="true"/>

		<cfset var success = true />
		<cfif variables.instanceQry.ACTION_CODE EQ 'AGREE'>
			<cfif arguments.data.getDOC_DUE_DAYS() eq "">
				<cfset success = false />
				<cfset variables.MsgBox.error("#variables.instanceQry.INSTANCE_NAME# sign-off due is required.", "DOC_DUE_DAYS") />
			<cfelseif not isNumeric(arguments.data.getDOC_DUE_DAYS()) or arguments.data.getDOC_DUE_DAYS() lte 0>
				<cfset success = false />
				<cfset variables.MsgBox.error("#variables.instanceQry.INSTANCE_NAME# sign-off due must be positive numeric.", "DOC_DUE_DAYS") />
			</cfif>
		</cfif>

		<cfreturn success />
	</cffunction>

</cfcomponent>