<cfcomponent name="DocSettingsFVO" displayname="DocSettingsFVO" extends="sis_core.model.ModelBase" output="false">
	<cfproperty name="DOC_AVAILABLE_DAY" type="string" />
	<cfproperty name="DOC_AVAILABLE_MONTH" type="string" />
	<cfproperty name="DOC_DUE_DAYS" type="string" />
	<cfproperty name="GENERATE_CORRECTION" type="string" />
	<cfproperty name="AUTO_PUBLISH_ORIGINAL" type="string" />
	<cfproperty name="AUTO_PUBLISH_CORRECTION" type="string" />
	<cfproperty name="AUTO_PUBLISH_REVISION" type="string" />
	<cfproperty name="OVERWRITE_CURRENT" type="string" />
	<cfproperty name="MODIFICATION_DT" type="date" />
	<cfproperty name="MODIFICATION_ID" type="string" />
	<cfproperty name="DOC_AVAILABLE_DT" type="date">
	<cfproperty name="INSTANCE_ID" type="string" />
	<cfproperty name="ACTION_ID" type="string" />
	<cfproperty name="AUTO_GENERATE" type="string" />
	<cfproperty name="ENABLE_PUBLISH" type="string" />
	<cfproperty name="ENABLE_SEND_EMAIL" type="string" />
	<cfproperty name="ATTACH_USER_PICTURE" type="string" />

	<cffunction name="init" access="public" returntype="DocSettingsFVO" output="false" displayname="init" hint="I initialize a DocSettingsFVO">
		<cfset super.init()>
		<cfset variables.DOC_AVAILABLE_DAY = "" />
		<cfset variables.DOC_AVAILABLE_MONTH = "" />
		<cfset variables.DOC_DUE_DAYS = "" />
		<cfset variables.GENERATE_CORRECTION = "N" />
		<cfset variables.AUTO_PUBLISH_ORIGINAL = "N" />
		<cfset variables.AUTO_PUBLISH_CORRECTION = "N" />
		<cfset variables.AUTO_PUBLISH_REVISION = "N" />
		<cfset variables.OVERWRITE_CURRENT = "N" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.DOC_AVAILABLE_DT = "" />
		<cfset variables.INSTANCE_ID = "" />
		<cfset variables.ACTION_ID = "" />
		<cfset variables.AUTO_GENERATE = "N" />
		<cfset variables.ENABLE_PUBLISH = "N" />
		<cfset variables.ENABLE_SEND_EMAIL = "N" />
		<cfset variables.ATTACH_USER_PICTURE = "N" />

		<cfreturn this />
	</cffunction>

	<cffunction name="setDOC_AVAILABLE_DAY" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DOC_AVAILABLE_DAY = arguments.val />
	</cffunction>
	<cffunction name="getDOC_AVAILABLE_DAY" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_AVAILABLE_DAY />
	</cffunction>

	<cffunction name="setDOC_AVAILABLE_MONTH" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DOC_AVAILABLE_MONTH = arguments.val />
	</cffunction>
	<cffunction name="getDOC_AVAILABLE_MONTH" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_AVAILABLE_MONTH />
	</cffunction>

	<cffunction name="setDOC_DUE_DAYS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DOC_DUE_DAYS = arguments.val />
	</cffunction>
	<cffunction name="getDOC_DUE_DAYS" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_DUE_DAYS />
	</cffunction>

	 <cffunction name="setGENERATE_CORRECTION" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.GENERATE_CORRECTION = arguments.val />
	</cffunction>
	<cffunction name="getGENERATE_CORRECTION" access="public" returntype="any" output="false">
		<cfreturn variables.GENERATE_CORRECTION />
	</cffunction>

	 <cffunction name="setAUTO_PUBLISH_ORIGINAL" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.AUTO_PUBLISH_ORIGINAL = arguments.val />
	</cffunction>
	<cffunction name="getAUTO_PUBLISH_ORIGINAL" access="public" returntype="any" output="false">
		<cfreturn variables.AUTO_PUBLISH_ORIGINAL />
	</cffunction>

	 <cffunction name="setAUTO_PUBLISH_CORRECTION" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.AUTO_PUBLISH_CORRECTION = arguments.val />
	</cffunction>
	<cffunction name="getAUTO_PUBLISH_CORRECTION" access="public" returntype="any" output="false">
		<cfreturn variables.AUTO_PUBLISH_CORRECTION />
	</cffunction>

	 <cffunction name="setAUTO_PUBLISH_REVISION" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.AUTO_PUBLISH_REVISION = arguments.val />
	</cffunction>
	<cffunction name="getAUTO_PUBLISH_REVISION" access="public" returntype="any" output="false">
		<cfreturn variables.AUTO_PUBLISH_REVISION />
	</cffunction>

	 <cffunction name="setOVERWRITE_CURRENT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.OVERWRITE_CURRENT = arguments.val />
	</cffunction>
	<cffunction name="getOVERWRITE_CURRENT" access="public" returntype="any" output="false">
		<cfreturn variables.OVERWRITE_CURRENT />
	</cffunction>

	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.MODIFICATION_DT = arguments.val />
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>

	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.MODIFICATION_ID = arguments.val />
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>

	<cffunction name="setDOC_AVAILABLE_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DOC_AVAILABLE_DT = arguments.val />
	</cffunction>
	<cffunction name="getDOC_AVAILABLE_DT" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_AVAILABLE_DT />
	</cffunction>

	<cffunction name="setINSTANCE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INSTANCE_ID = arguments.val />
	</cffunction>
	<cffunction name="getINSTANCE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INSTANCE_ID />
	</cffunction>

	<cffunction name="setACTION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ACTION_ID = arguments.val />
	</cffunction>
	<cffunction name="getACTION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.ACTION_ID />
	</cffunction>

	<cffunction name="setAUTO_GENERATE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.AUTO_GENERATE = arguments.val />
	</cffunction>
	<cffunction name="getAUTO_GENERATE" access="public" returntype="any" output="false">
		<cfreturn variables.AUTO_GENERATE />
	</cffunction>

	<cffunction name="setENABLE_PUBLISH" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ENABLE_PUBLISH = arguments.val />
	</cffunction>
	<cffunction name="getENABLE_PUBLISH" access="public" returntype="any" output="false">
		<cfreturn variables.ENABLE_PUBLISH />
	</cffunction>

	<cffunction name="setENABLE_SEND_EMAIL" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ENABLE_SEND_EMAIL = arguments.val />
	</cffunction>
	<cffunction name="getENABLE_SEND_EMAIL" access="public" returntype="any" output="false">
		<cfreturn variables.ENABLE_SEND_EMAIL />
	</cffunction>

	<cffunction name="setATTACH_USER_PICTURE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ATTACH_USER_PICTURE = arguments.val />
	</cffunction>
	<cffunction name="getATTACH_USER_PICTURE" access="public" returntype="any" output="false">
		<cfreturn variables.ATTACH_USER_PICTURE />
	</cffunction>

</cfcomponent>