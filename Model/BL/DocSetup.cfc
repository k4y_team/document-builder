<cfcomponent name="DocSetup" displayname="DocSetup" output="false" extends="ServiceBase">

	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cffunction name="init" access="public" returntype="DocSetup">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>


	<cffunction name="getDocSetups" access="public" output="false" returntype="query">
		<cfargument name="searchFilter" type="sis_core.model.SearchFilter" required="false" />
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset data = DeserializeJson(arguments.searchFilter.getFILTER_DATA_STRUCT().params)>
		<cfset structAppend(data,arguments.searchFilter.getFILTER_DATA_STRUCT(),true)>
		<cfset EntityRestrictions =  wirebox.getInstance(name='sis_core.model.blAutomation.EntityRestrictions')/>
		<cfset userSessionData = application.wirebox.getInstance(dsl='coldbox:plugin:SessionStorage' ).getVar("LoggedInUser")>
		<cfset programRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_PROGRAM',userId=userSessionData.USER_ID,securityCode="DOCUMENT_SETUP") />
		<cfset courseRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_COURSE',userId=userSessionData.USER_ID,securityCode="DOCUMENT_SETUP") />
		<cfset instanceRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_INSTANCE',userId=userSessionData.USER_ID,securityCode="DOCUMENT_SETUP") />
		<cfif programRestrictionSql NEQ "*">
			<cfset data.PROGRAM_RESTRICTIONS_LIST = listToArray(programRestrictionSql,",",false,true)>
		</cfif>
		<cfif courseRestrictionSql NEQ "*">
			<cfset data.COURSE_RESTRICTIONS_LIST = listToArray(courseRestrictionSql,",",false,true)>
		</cfif>
		<cfif instanceRestrictionSql NEQ "*">
			<cfset data.INSTANCE_RESTRICTIONS_LIST = listToArray(instanceRestrictionSql,",",false,true)>
		</cfif>
		<cfset orderbyList = "DOC_SETUP_NAME,DOC_SETUP_ID" />
		<cfif (arguments.searchFilter.getSORT_OPTIONS().getSortString_UC() NEQ "") >
			<cfset orderbyList = "#arguments.searchFilter.getSORT_OPTIONS().getSortString()# NULLS LAST,DOC_SETUP_NAME,DOC_SETUP_ID" />
		</cfif>
		<cfset setupSql ="
				SELECT
					A.*,
					DENSE_RANK() OVER  (order by #orderbyList#) AS ORDER_IDX,
					COUNT(*) OVER() AS TOTAL_RECORDS
				FROM
					SETUPDETAIL_VIEW A
				WHERE 1=1
					{% if (DATA.DOC_LETTER_INSTANCE_CD is defined) and (DATA.DOC_LETTER_INSTANCE_CD !='')%}
					AND A.INSTANCE_ID = {{DATA.DOC_LETTER_INSTANCE_CD}}
					{% endif %}
					{% if (DATA.search_string is defined) and (DATA.search_string !='')%}
						AND (
						upper(A.DOC_SETUP_NAME) like '%{{DATA.search_string|upper}}%'
						OR upper(A.LETTER_TEMPLATE_NAME) like '%{{DATA.search_string|upper}}%'
						OR upper(A.INSTANCE_NAME) like '%{{DATA.search_string|upper}}%'
						OR upper(A.DETAILS) like '%{{DATA.search_string|upper}}%'
						)
					{% endif %}
					{% if DATA.INSTANCE_RESTRICTIONS_LIST is defined %}
						AND {{macros.whereClause('A.INSTANCE_ID',join(DATA.INSTANCE_RESTRICTIONS_LIST, '\',\''))}}
					{%else%}
						AND A.INSTANCE_ID =-1
					{% endif %}
					{% if DATA.PROGRAM_RESTRICTIONS_LIST is defined %}
						AND A.DOC_SETUP_ID in (SELECT a.DOC_SETUP_ID FROM tes_config a,tes_config_programs b
						WHERE	a.tes_config_id = b.tes_config_id(+) and (b.program_cd is null or {{macros.whereClause('b.program_cd',join(DATA.PROGRAM_RESTRICTIONS_LIST, '\',\''))}}))
					{% endif %}
					{% if DATA.COURSE_RESTRICTIONS_LIST is defined %}
						AND A.DOC_SETUP_ID in (SELECT a.DOC_SETUP_ID FROM tes_config a,UG_TES_CONFIG_COURSES b
					WHERE	a.tes_config_id = b.tes_config_id(+) and (b.course_cd is null or {{macros.whereClause('b.course_cd',join(DATA.COURSE_RESTRICTIONS_LIST, '\',\''))}}))
					{% endif %}
					{% if DATA.TEMPLATE_ID is defined %}
						AND {{macros.whereClause('A.DOC_LETTER_TEMPLATE_CD',join(DATA.TEMPLATE_ID, '\',\''))}}
					{% endif %}
					{% if DATA.SETUP_ID is defined %}
						AND {{macros.whereClause('A.DOC_SETUP_ID',join(DATA.SETUP_ID, '\',\''))}}
					{% endif %}
					{% if DATA.EVAL_FORM_CD is defined %}
						AND {{macros.whereClause('A.EVAL_FORM_CD',join(DATA.EVAL_FORM_CD, '\',\''))}}
					{% endif %}
					{% if DATA.PROGRAM_CD is defined %}
						AND A.DOC_SETUP_ID in (SELECT a.DOC_SETUP_ID FROM tes_config a,tes_config_programs b
						WHERE	a.tes_config_id = b.tes_config_id(+) and (b.program_cd is null or {{macros.whereClause('b.program_cd',join(DATA.PROGRAM_CD, '\',\''))}}))
					{% endif %}
					{% if DATA.COURSE_CD is defined %}
						AND A.DOC_SETUP_ID in (SELECT a.DOC_SETUP_ID FROM tes_config a,UG_TES_CONFIG_COURSES b
					WHERE	a.tes_config_id = b.tes_config_id(+) and (b.course_cd is null or {{macros.whereClause('b.course_cd',join(DATA.COURSE_CD, '\',\''))}}))
					{% endif %}
					ORDER BY A.DOC_SETUP_NAME ASC"
			>
		<cfif arguments.searchFilter.getTABLE_PAGE().GETEND_ROW() gt 0>
			<cfset setupSql = "SELECT J.*  FROM (#setupSql#) J WHERE ORDER_IDX >= #arguments.searchFilter.getTABLE_PAGE().GETSTART_ROW()# AND ORDER_IDX <= #arguments.searchFilter.getTABLE_PAGE().GETEND_ROW()# ORDER BY ORDER_IDX ASC">
		</cfif>
		<cfset DocReportQry = queryBuilder.runQuery(sql=setupSql,datasource = variables.datasource,params=data)/>
		<cfreturn DocReportQry />
	</cffunction>


	<cffunction name="getDocSetupByPk" access="public" output="false" returntype="query">
		<cfargument name="params" type="struct" required="false" default="#StructNew()#">
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset pgDSN = application.wirebox.getInstance(dsl="coldbox:setting:pg_datasource") />
		<cfset fbxDSN = application.wirebox.getInstance(dsl="coldbox:setting:fbx_datasource") />
		<cfset sql ="SELECT
			A.*,
			B.*,
			(SELECT LISTAGG(score_type_id,',') WITHIN GROUP (ORDER BY TES_CONFIG_ID) FROM tes_config_scores where TES_CONFIG_ID= B.TES_CONFIG_ID) AS score_type_id,
			(SELECT LISTAGG(COMPARISON_TYPE_ID,',') WITHIN GROUP (ORDER BY TES_CONFIG_ID) FROM TES_CONFIG_COMPARISONS where TES_CONFIG_ID= B.TES_CONFIG_ID) AS COMPARISON_TYPE_ID,
			(SELECT LISTAGG(A.PROGRAM_ID,',') WITHIN GROUP(ORDER BY M.TES_CONFIG_ID) FROM {pg_datasource}.RSPROGRAM_VIEW A, TES_CONFIG_PROGRAMS M WHERE A.PROGRAM_ID = M.PROGRAM_CD
			AND M.TES_CONFIG_ID=B.TES_CONFIG_ID) as PROGRAM_CD,
			(SELECT LISTAGG(TS.TRAINING_SESSION_CD,',') WITHIN GROUP(ORDER BY TS.TES_CONFIG_ID) FROM  TES_CONFIG_SESSIONS TS WHERE
			TS.TES_CONFIG_ID=B.TES_CONFIG_ID) as TRAINING_SESSION_CD,
			(SELECT LISTAGG(A.COURSE_CD,',') WITHIN GROUP(ORDER BY M.TES_CONFIG_ID) FROM {fbx_datasource}.COURSE A, UG_TES_CONFIG_COURSES M WHERE A.COURSE_CD = M.COURSE_CD
			AND M.TES_CONFIG_ID=B.TES_CONFIG_ID) as COURSE_CD
			FROM
			DOC_SETUP A,
			tes_CONFIG B
			WHERE A.DOC_SETUP_ID=B.DOC_SETUP_ID(+)
			{% if (DATA.DOC_SETUP_ID is defined) and (DATA.DOC_SETUP_ID !='')%}
			AND A.DOC_SETUP_ID  = {{DATA.DOC_SETUP_ID }}
			{% endif %}"
			>
		<cfset DocReportQry = queryBuilder.runQuery(sql=sql,datasource = variables.datasource ,params=arguments.params)/>
		<cfreturn DocReportQry />
	</cffunction>


	<cffunction name="getDocSetupByCode" access="public" output="false" returntype="query">
		<cfargument name="code" type="string" required="false" default="">
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset pgDSN = application.wirebox.getInstance(dsl="coldbox:setting:pg_datasource") />
		<cfset fbxDSN = application.wirebox.getInstance(dsl="coldbox:setting:fbx_datasource") />
		<cfset sql ="
			SELECT
			*
			FROM
			DOC_SETUP
			WHERE
			TRIM(DOC_SETUP_NAME)='#arguments.code#'
			">
		<cfset DocReportQry = queryBuilder.runQuery(sql=sql,datasource = variables.datasource)/>
		<cfreturn DocReportQry />
	</cffunction>

	<cfscript>
	public function save(struct data) {
		var validatorObj = application.wirebox.getInstance("EntityValidator@CORE");
		var MsgBox = application.wirebox.getInstance(dsl="coldbox:myplugin:MsgBox").init(application.cbController);
		var EntityRegistry = variables.wirebox.getInstance('EntityRegistry@CORE');
		var bValid = validatorObj.validateData(entityCode ='DOCUMENT_SETUP_FORM', data = arguments.data);
		if (bValid){
			var docInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init();
			var instanceConfig = docInstances.getInstanceConfig(arguments.data.INSTANCE_ID);
			var instanceInfoQry = docInstances.getInstanceInfo(arguments.data.instance_id);


			if(!StructIsEmpty(instanceConfig) and structKeyExists(instanceConfig,"SETUP_CONFIG_FORM")){
				var entity = EntityRegistry.getEntity(instanceConfig.SETUP_CONFIG_FORM);
				var dataEntityInstance = application.wirebox.getInstance(name=entity.ENTITY_PATH).init();
				var bValid = dataEntityInstance.validate(arguments.data);
			}
		}

		transaction {
 			try {
				if (bValid){
					var dbEntity = EntityRegistry.initDBEntity("DOC_SETUP");
					var bValid = dbEntity.save(arguments.data,{},{update_if_null_only = false});
				}

				if (bValid){


					if(!StructIsEmpty(instanceConfig) and structKeyExists(instanceConfig,"SETUP_CONFIG_FORM")){
						var bValid = EntityRegistry.save(entityCode="#instanceConfig.SETUP_CONFIG_FORM#", rbAlias=moduleConfig.getRBSettings().ALIAS,params=arguments.data);
					}
				}
			 transaction action="commit";
 			}
 			catch(any e) {
 				writedump(e);abort;


		  		transaction action="rollback";
		  		var bValid = false ;
			}
		}
		if (bValid){
			variables.MsgBox.success("Setup successfully saved." );
			var inform = variables.interceptor.inform("DOCUMENT_BUILDER:SETUP:AFTER:SAVE",{INSTANCE_ID=arguments.data.INSTANCE_ID,INSTANCE_CODE = instanceInfoQry.INSTANCE_CODE,DOC_SETUP_ID=arguments.data.DOC_SETUP_ID});

		}else {
			variables.MsgBox.error("Save setup failed." );
		}
		return bValid;
	}

	public function delete (struct data){
		var bValid = true ;
		var EntityRegistry = variables.wirebox.getInstance('EntityRegistry@CORE');
		var doaInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init();
		var instanceConfig = doaInstances.getInstanceConfig(arguments.data.INSTANCE_ID);
		if	(!StructIsEmpty(instanceConfig) and structKeyExists(instanceConfig,"SETUP_CONFIG_FORM")){
			var bValid = EntityRegistry.delete(entityCode="#instanceConfig.SETUP_CONFIG_FORM#", rbAlias=moduleConfig.getRBSettings().ALIAS,params=arguments.data);
		}
		if(bValid){
			var dbEntity = EntityRegistry.initDBEntity("DOC_SETUP");
			var bValid = dbEntity.delete(condition="DOC_SETUP_ID = #arguments.data.DOC_SETUP_ID#");
		}
		if (bValid){
			variables.MsgBox.success("Setup successfully deleted." );
		}else {
			variables.MsgBox.error("Delete setup failed." );
		}
		return bValid;
	}
	</cfscript>

</cfcomponent>
