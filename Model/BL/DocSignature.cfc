<cfcomponent displayname="DocSignature" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocSignature">
		<cfargument name="instanceID" type="numeric" required="false" default="0"/>
		<cfscript>
		super.init();
		variables.instance_id = arguments.instanceID;
		return this;
	</cfscript>
	</cffunction>

	<cffunction name="getSignaturesFolderPath" access="public" output="false" returntype="string">
		<cfset var DocSettingsObj = variables.dbDocumentFactory.create("DocSettingsGTW")>
		<cfset var docSettingsQry = DocSettingsObj.getAll(variables.instance_id)>
		<cfif Trim(docSettingsQry.SIGN_IMAGES_PATH) neq "">
			<cfset var docSignatureFolder = Trim(docSettingsQry.SIGN_IMAGES_PATH)>
		<cfelse>
			<cfset var docSignatureFolder = "Files/Signatures">
		</cfif>
		<cfreturn docSignatureFolder>
	</cffunction>

	<cffunction name="uploadSignature" access="public" returntype="boolean">
		<cfargument name="DocSignatureFVO" type="medsisOld.modules.DocumentBuilder.Model.BL.DocSignatureFVO" required="true">

		<cfset var success = validate(arguments.DocSignatureFVO, 'UPLOAD')>
		<cfif success>
			<cfset var signaturePath = variables.ModuleConfig.getModuleInfo().folder_path &"\" & getSignaturesFolderPath()>
			<cfset var fileUtils = CreateObject('component','sis_core.model.util.FileUtils') />
			<cfset var name=createUUID()>
			<cfif trim(arguments.DocSignatureFVO.getfile_name()) EQ "">
				<cfset success = true>
				<cfset variables.MsgBox.error("Could not upload the file.")>
			<cfelse>
				<cfset var FileManager = application.wirebox.getInstance("FileManager@CORE")>
				<cfset var fileDetails = FileManager.getFile(DocSignatureFVO.getFILE_NAME())>
				<cfset var fileName = fileDetails.name>

				<cfset FileManager.copyFile(fileDetails.path,"#signaturePath#/#name#_#fileName#")>

				<cfif fileDetails.filetype neq "image">
					<cfset success = false>
					<cfset variables.MsgBox.error('File must have one of the following formats: jpg,jpeg,gif,png.')>
				<cfelse>
					<!--- <cfset ImageService = CreateObject("component","sis_core.model.util.ImageService").init()>
					<cfset ImageService.resizeImgFile(srcPath="#signaturePath#/#cffile.CLIENTFILE#",destPath="#signaturePath#/#name#_#cffile.CLIENTFILE#")>
					<cfset ImageService.resizeImgFile(srcPath="#signaturePath#/#cffile.CLIENTFILE#",destPath="#signaturePath#/_small#name#_#cffile.CLIENTFILE#",width=64,height=64)>
					<cfset document=fileUtils.removeFile(filename="#signaturePath#/#cffile.CLIENTFILE#")> --->
					<cfset DocSignatureFVO.setFile_name("#name#_#fileName#")>
					<cfset succ=save(DocSignatureFVO)>
					<cfif succ>
						<cfset variables.MsgBox.success("Image successfully added.")>
						<cfset success = true>
					<cfelse>
						<cfset success =false>
					</cfif>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn success>
	</cffunction>

	<cffunction name="validate" access="public" returntype="boolean">
		<cfargument name="DocSignatureFVO" type="medsisOld.modules.DocumentBuilder.Model.BL.DocSignatureFVO" required="true">
		<cfargument name="action" type="string" required="true">

		<cfset var valid = true>
		<cfif Trim(arguments.DocSignatureFVO.getDISPLAY_NAME()) EQ "">
			<cfset variables.MsgBox.error('Display Name is required.', 'display_name')>
			<cfset valid = false>
		</cfif>
		<cfset DocSignatureGTW = variables.dbDocumentFactory.create('DocSignatureGTW')>
		<cfset qSignature = DocSignatureGTW.getByDisplayName(arguments.DocSignatureFVO.getDISPLAY_NAME(), variables.instance_id)>
		<cfif qSignature.recordCount GT 0>
			<cfset variables.MsgBox.error('Display Name is already used on a different signature.', 'display_name')>
			<cfset valid = false>
		</cfif>
		<cfif arguments.action EQ 'UPLOAD' AND Trim(arguments.DocSignatureFVO.getFILE_NAME()) EQ "">
			<cfset variables.MsgBox.error('File is required', 'file_name')>
			<cfset valid = false>
		</cfif>
		<cfreturn valid>
	</cffunction>

	<cffunction name="save" returntype="boolean" access="public" output="false" hint="">
		<cfargument name="data" type="DocSignatureFVO" required="true"/>
		<cfset var docSignatureDAO = variables.dbDocumentFactory.create("docSignatureDAO") />
		<cfset var docSignatureVO = "" />
		<cftransaction action="begin">
			<cftry>
				<!--- save --->
				<cfset docSignatureVO = variables.dbDocumentFactory.create("docSignatureVO").init() />
				<cfset docSignatureVO.setFILE_NAME(arguments.data.getFILE_NAME()) />
				<cfset docSignatureVO.setDISPLAY_NAME(arguments.data.getDISPLAY_NAME()) />
				<cfset docSignatureVO.setCREATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)/>
				<cfset docSignatureVO.setCREATION_DT(Now())/>
				<cfset signature=docSignatureDAO.add(docSignatureVO) />
				<cfset docSignatureVO.setDOC_SIGNATURE_CD(signature)/>
				<cftransaction action="commit"/>
				<cfcatch>
					<cftransaction action="rollback"/>
					<cfrethrow />
					<cfreturn false />
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn true />
	</cffunction>

	<cffunction name="updateSignature" returntype="boolean" access="public" output="false" hint="">
		<cfargument name="data" type="DocSignatureFVO" required="true"/>

		<cfset var success = validate(arguments.data, 'UPDATE')>
		<cfif success>
			<cfset var docSignatureDAO = variables.dbDocumentFactory.create("docSignatureDAO") />
			<cfset var docSignatureVO = "" />
			<cftransaction action="begin">
				<cftry>
					<cfset docSignatureVO = docSignatureDAO.read(arguments.data.getDOC_SIGNATURE_CD()) />
					<cfset docSignatureVO.setDISPLAY_NAME(arguments.data.getDISPLAY_NAME()) />
					<cfset docSignatureVO.setMODIFICATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)/>
					<cfset docSignatureVO.setMODIFICATION_DT(Now())/>
					<cfset signature=docSignatureDAO.update(docSignatureVO) />
					<cfset variables.MsgBox.success("Signature updated successfully.")>
					<cftransaction action="commit"/>
					<cfcatch>
						<cftransaction action="rollback"/>
						<cfset variables.MsgBox.error("Could not update Signature.")>
						<cfset success = false />
					</cfcatch>
				</cftry>
			</cftransaction>
		</cfif>
		<cfreturn success />
	</cffunction>

	<cffunction name="getUsedTemplates" access="public" output="false" returntype="DocLetterTemplateFVO">
		<cfargument name="Signature_CD" type="numeric" required="true">
		<cfscript>
		var docLetterTemplateFVO = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocLetterTemplateFVO").init();
		var tplQry = getTemplate(arguments.docLetterTemplateCD);
		var docHelper = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocHelper").init(variables.instance_id);
		var settings = DocHelper.getSettings();
		docLetterTemplateFVO.initFromQuery(tplQry);
		docLetterTemplateFVO.setHTML_LETTER_TEMPLATE_BODY(tplQry.LETTER_TEMPLATE_BODY);
		var docSignatureFolder = getSignaturesFolderPath();
		docLetterTemplateFVO.setDOC_SIGNATURE_FILE_NAME("#docSignatureFolder#/#docLetterTemplateFVO.getDOC_SIGNATURE_FILE_NAME()#");

		return docLetterTemplateFVO;
	</cfscript>
	</cffunction>

	<cffunction name="delete" returntype="boolean" access="public" output="false" hint="">
		<cfargument name="doc_signature_id" type="string" required="true"/>
		<cfset fileUtils = CreateObject('component','sis_core.model.util.FileUtils') />
		<cfset var signaturePath = variables.ModuleConfig.getModuleInfo().folder_path & "\" & getSignaturesFolderPath()>
		<cfset var DocSignatureDAO = variables.dbDocumentFactory.create("DocSignatureDAO") />
		<cfset Signature=DocSignatureDAO.read(arguments.doc_signature_id)>
		<cfset document=fileUtils.removeFile(filename="#signaturePath#/#Signature.getFILE_NAME()#")>
		<cfset document=fileUtils.removeFile(filename="#signaturePath#/_small#Signature.getFILE_NAME()#")>
		<cftransaction action="begin">
			<cftry>
				<!--- delete signature --->
				<cfset DocSignatureDAO.delete(arguments.doc_signature_id) />
				<cfset variables.MsgBox.success("Signature deleted successfully")>
				<cftransaction action="commit"/>
				<cfcatch type="database">
					<cftransaction action="rollback"/>
					<cfrethrow />
					<cfreturn false />
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn true />
	</cffunction>

</cfcomponent>