<cfcomponent name="DocSignatureFVO" displayname="Picture Form VO" extends="sis_core.model.ModelBase" output="false">

	<cfproperty name="DOC_SIGNATURE_CD" type="numeric">
	<cfproperty name="FILE_NAME" type="string">
	<cfproperty name="DISPLAY_NAME" type="string">
	<cfproperty name="PICTURE_PATH" type="string">
	<cfproperty name="NO_PICTURE_URL" type="string">
	<cfproperty name="IS_VALID_PICTURE" type="boolean">
	<cfproperty name="INSTANCE_ID" type="numeric">


	<cffunction name="init" access="public" returntype="DocSignatureFVO" output="false" displayname="" hint="">

		<cfset super.init()>

		<cfset variables.DOC_SIGNATURE_CD = "">
		<cfset variables.FILE_NAME = "">
		<cfset variables.DISPLAY_NAME = "">
		<cfset variables.PICTURE_PATH = "">
		<cfset variables.NO_PICTURE_URL = "">
		<cfset variables.IS_VALID_PICTURE = false>
		<cfset variables.INSTANCE_ID = "">

		<cfreturn this>
	</cffunction>
	
	<cffunction name="setDOC_SIGNATURE_CD" access="public" returntype="void">
		<cfargument name="val" type="any" required="true">
		<cfset variables.DOC_SIGNATURE_CD = val>
	</cffunction>

	<cffunction name="getDOC_SIGNATURE_CD" access="public" returntype="any">	
		<cfreturn variables.DOC_SIGNATURE_CD>
	</cffunction>	

	<cffunction name="setFILE_NAME" access="public" returntype="void">
		<cfargument name="val" type="string" required="true">
		<cfset variables.FILE_NAME = arguments.val>
	</cffunction>

	<cffunction name="getFILE_NAME" access="public" returntype="string">
		<cfreturn variables.FILE_NAME>
	</cffunction>	
	
	<cffunction name="setDISPLAY_NAME" access="public" returntype="void">
		<cfargument name="val" type="string" required="true">

		<cfset variables.DISPLAY_NAME = arguments.val>
	</cffunction>

	<cffunction name="getDISPLAY_NAME" access="public" returntype="string">
		<cfreturn variables.display_name>
	</cffunction>
	
	<cffunction name="setINSTANCE_ID" access="public" returntype="void">
		<cfargument name="val" type="any" required="true">
		<cfset variables.INSTANCE_ID = val>
	</cffunction>

	<cffunction name="getINSTANCE_ID" access="public" returntype="any">	
		<cfreturn variables.INSTANCE_ID>
	</cffunction>		
	
</cfcomponent>