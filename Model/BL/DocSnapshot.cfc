<cfcomponent displayname="DOCSnapshot" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DOCSnapshot">
		<cfset super.init() />
		<cfreturn this/>
	</cffunction>

	<cffunction name="get" access="public" output="false" returntype="query">
		<cfargument name="docSnapshotCd" type="numeric" required="true">
		<cfset var docSnapshotDetailGTW = variables.dbDocumentFactory.create('DocSnapshotDetailGTW')>
		<cfreturn docSnapshotDetailGTW.getSnapshots(arguments.docSnapshotCd) />
	</cffunction>

	<cffunction name="getByPk" access="public" output="false" returntype="query">
		<cfargument name="docSnapshotCd" type="numeric" required="true">
		<cfset var DocSnapshotBaseGTW = variables.dbDocumentFactory.create('DocSnapshotBaseGTW')>
		<cfreturn DocSnapshotBaseGTW.getByPk(arguments.docSnapshotCd) />
	</cffunction>

	<cffunction name="getHistory" access="public" output="false" returntype="query">
		<cfargument name="snapshotCd" type="numeric" required="true" />

		<cfset var LOAsObj = variables.dbFactory.create('DocSnapshotDetailGTW')>
		<cfset var LOAHistoryQry = LOAsObj.getHistory(variables.instance_id, arguments.snapshotCd)>

		<cfreturn LOAHistoryQry>
	</cffunction>

	<cffunction name="isSigned" access="public" output="false" returntype="boolean">
		<cfargument name="doc_snapshot_cd" type="numeric" required="true">
		<cfset var DocSnapshotBaseObj = variables.dbFactory.create('DocSnapshotBaseGTW')>
		<cfset var signFVO = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.SignFVO").init()>

		<cfset signFVO.setSTUDENT_ID(session.reg_student_id)>
		<cfset signFVO.setSIGNOFF_DT(DocSnapshotBaseObj.getByPk(doc_snapshot_cd).DOC_SUBMITTED_DT)>
		<cfset signFVO.setDOC_SNAPSHOT_CD(arguments.doc_snapshot_cd)>
		<cfif signFVO.getSIGNOFF_DT() neq ''>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>

	<cffunction name="traineeSignOff" access="public" output="false" returntype="boolean">
		<cfargument name="data" type="medsisOld.modules.DocumentBuilder.Model.BL.SignFVO" required="true" />
		<cfargument name="validationRequired" type="boolean" required="false" default="true" />

		<cfset var docSnapshotDetailGtw = variables.dbFactory.create("docSnapshotDetailGtw")>
		<cfset var docSnapshotDetailQry = "">
		<cfset var DOCSnapshotBaseDAO =variables.dbFactory.create("DOCSnapshotBaseDAO")>
		<cfset var DOCSnapshotBaseVO = variables.dbFactory.create("DOCSnapshotBaseVO").init()>

		<!--- validation --->
		<cfif arguments.validationRequired>
			<cfif not Len(Trim(arguments.data.getSIGNATURE()))>
				<cfset variables.MsgBox.error('Type your full name in signature box.', 'signature')>
				<cfreturn false />
			</cfif>
		</cfif>

		<cftransaction action="begin">
			<cftry>
				<cfset arguments.data.setSIGNOFF_DT(Now()) />

				<cfset DOCSnapshotBaseVO = DOCSnapshotBaseDAO.read(arguments.data.getDOC_SNAPSHOT_CD())>
				<cfset DOCSnapshotBaseVO.setDOC_SNAPSHOT_CD(arguments.data.getDOC_SNAPSHOT_CD())>
				<cfset DOCSnapshotBaseVO.setDOC_SUBMITTED_USER_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)>
				<cfset DOCSnapshotBaseVO.setDOC_SUBMITTED_DT(arguments.data.getSIGNOFF_DT())>
				<cfset DOCSnapshotBaseVO.setDOC_SIGNATURE(arguments.data.getSIGNATURE())>
				<cfset DOCSnapshotBaseDAO.update(DOCSnapshotBaseVO)>

				<!--- <cfset variables.MsgBox.success('#variables.instanceQry.INSTANCE_NAME# successfully signed!')> --->
				<cfset success = true>
				<cftransaction action = "commit"/>
				<cfcatch type="database">
					<cftransaction action = "rollback"/>
					<cfrethrow />
				</cfcatch>
			</cftry>
		</cftransaction>

		<!--- Attach Document --->
		<cfif success>
			<cfset var backwardCompatibility = ModuleConfig.getSetting("BACKWARD_COMPATIBILITY")>
			<cfset docSnapshotDetailQry = docSnapshotDetailGtw.getByPK(arguments.data.getDOC_SNAPSHOT_CD())>

			<cfif not backwardCompatibility>
				<cfset var DocLetterTemplateGTW = variables.dbFactory.create("DocLetterTemplateGTW")/>
				<cfset var docTemplateQry = DocLetterTemplateGTW.getByPK(docSnapshotDetailQry.DOC_LETTER_TEMPLATE_CD)/>
				<cfset var LoaDataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.LoaDataProvider").init()/>
				<cfset var contextKey = "STUDENT_ID=" & docSnapshotDetailQry.STUDENT_ID & "+TRAINING_SESSION_CD=" & docSnapshotDetailQry.TRAINING_SESSION_CD />
				<cfset var DocGenerate = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocGenerate").init(userID=variables.SessionStorage.getVar('LoggedInUser').user_id,instanceID=DOCSnapshotBaseVO.getINSTANCE_ID())>

				<cfset var DocLetterDataDAO = variables.dbFactory.create("DocLetterDataDAO")/>
				<cfset var LOALetterDataVO = variables.dbFactory.create("LOALetterDataVO").init()/>
				<cfset LOALetterDataVO.setXML_LETTER_CONTENT(DocGenerate.generateLetterContent(LoaDataProvider, contextKey, docTemplateQry.TAGS))/>
				<cfset LOALetterDataVO.setCREATION_DT(Now())/>
				<cfset docLetterDataCD = DocLetterDataDAO.add(LOALetterDataVO)/>

				<cfset DOCSnapshotBaseVO.setDOC_LETTER_DATA_CD(docLetterDataCD) />
			</cfif>

			<cftry>
				<cfset var fileCD = uploadPDF(arguments.data.getSTUDENT_ID(), arguments.data.getDOC_SNAPSHOT_CD())>
				<cfset DOCSnapshotBaseVO.setDOC_FILE_CD(fileCD)>
				<cfset DOCSnapshotBaseDAO.update(DOCSnapshotBaseVO)>
			<cfcatch>
				<!--- pdf file could not be generated --->
				<cflog file="letters" text="Letter PDF could not be generated for DOC_SNAPSHOT_CD=#arguments.data.getDOC_SNAPSHOT_CD()#">
			</cfcatch>
			</cftry>

			<cfset variables.Interceptor.inform("LOA:#instanceQry.instance_name#_DOCUMENT:AFTER:SIGN",{instance_id = DOCSnapshotBaseVO.getINSTANCE_ID(), letter_template_cd=docSnapshotDetailQry.doc_letter_template_cd, training_session_cd=docSnapshotDetailQry.training_session_cd, student_id=docSnapshotDetailQry.student_id})>
			<cfset variables.Interceptor.inform("LOA:AGREEMENT_LETTER:AFTER:SIGN",{instance_id=variables.instance_id,document_template_code="AGREEMENT_LETTER_#docSnapshotDetailQry.doc_letter_template_cd#",letter_template_cd=docSnapshotDetailQry.doc_letter_template_cd,student_id=docSnapshotDetailQry.student_id,training_session_cd=docSnapshotDetailQry.training_session_cd})>
		</cfif>
		<cfreturn success />
	</cffunction>

	<cffunction name="generatePDF" access="public" output="false" returntype="numeric">
		<cfargument name="docSnapshotCD" type="numeric" required="true">
		<cfargument name="bRegenerateLetterData" type="boolean" required="false" default="false">

        <cfset var DOCSnapshotBaseDAO = variables.dbFactory.create("DOCSnapshotBaseDAO")>
		<cfset var DOCSnapshotDetailGTW = variables.dbFactory.create("DOCSnapshotDetailGTW")>
		<cfset var DOCSnapshotDetailQry = DOCSnapshotDetailGTW.getByPK(arguments.docSnapshotCD)>

		<cfset var DOCSnapshotBaseVO = DOCSnapshotBaseDAO.read(arguments.docSnapshotCD)>

        <cfif arguments.bRegenerateLetterData>
            <cfset var DocLetterTemplateGTW = variables.dbFactory.create("DocLetterTemplateGTW")/>
            <cfset var docTemplateQry = DocLetterTemplateGTW.getByPK(DOCSnapshotDetailQry.DOC_LETTER_TEMPLATE_CD)/>
            <cfset var LoaDataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.LoaDataProvider").init()/>
            <cfset var contextKey = "STUDENT_ID=" & DOCSnapshotDetailQry.STUDENT_ID & "+TRAINING_SESSION_CD=" & DOCSnapshotDetailQry.TRAINING_SESSION_CD />

            <cfset var DocGenerate = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocGenerate").init(userID=DOCSnapshotBaseVO.getDOC_SUBMITTED_USER_ID(),instanceID=DOCSnapshotBaseVO.getINSTANCE_ID())>

            <cfset var DocLetterDataDAO = variables.dbFactory.create("DocLetterDataDAO")/>
            <cfset var LOALetterDataVO = variables.dbFactory.create("LOALetterDataVO").init()/>
            <cfset LOALetterDataVO.setXML_LETTER_CONTENT(DocGenerate.generateLetterContent(LoaDataProvider, contextKey, docTemplateQry.TAGS))/>
            <cfset LOALetterDataVO.setCREATION_DT(Now())/>
            <cfset var docLetterDataCD = DocLetterDataDAO.add(LOALetterDataVO)/>

            <cfset DOCSnapshotBaseVO.setDOC_LETTER_DATA_CD(docLetterDataCD) />
            <cfset DOCSnapshotBaseDAO.update(DOCSnapshotBaseVO)>
        </cfif>

		<cfset var fileCD = uploadPDF(DOCSnapshotDetailQry.STUDENT_ID, DOCSnapshotDetailQry.DOC_SNAPSHOT_CD, DOCSnapshotBaseVO.getDOC_SUBMITTED_USER_ID())>
        <cfif DOCSnapshotBaseVO.getDOC_FILE_CD() EQ "">
            <cfset DOCSnapshotBaseVO.setDOC_FILE_CD(fileCD)>
            <cfset DOCSnapshotBaseDAO.update(DOCSnapshotBaseVO)>
        </cfif>

		<cfreturn fileCD>
	</cffunction>

	<cffunction name="getChanges" access="public" output="false" returntype="query">
		<cfargument name="docSnapshotCD" type="numeric" required="true" />

		<cfset var DocSnapshotDetailGTW = variables.dbFactory.create('DocSnapshotDetailGTW')>
		<cfset var loaHistory = DocSnapshotDetailGTW.getHistoryChanges(variables.instance_id, arguments.docSnapshotCD) />

		<!--- TODO: Implement logic to detect differences --->

		<cfreturn loaHistory />
	</cffunction>

	<cffunction name="submitOnBehalf" access="public" output="false" returntype="boolean">
		<cfargument name="signFVO" type="medsisOld.modules.DocumentBuilder.Model.BL.SignFVO" required="true">

		<cfset var docSnapshotDetailGtw = variables.dbFactory.create("docSnapshotDetailGtw")>
		<cfset var docSnapshotBaseDAO = variables.dbFactory.create("DOCSnapshotBaseDAO")>
		<cfset var docLetterDataDAO = variables.dbFactory.create("DocLetterDataDAO")/>
		<cfset var docLetterDataVO = variables.dbFactory.create("LOALetterDataVO").init()/>
		<cfset var DocLetterTemplateGTW = variables.dbFactory.create("DocLetterTemplateGTW")/>
		<cfset var loaDataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.LoaDataProvider").init()/>
		<cfset var DocGenerate = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocGenerate").init(variables.SessionStorage.getVar('LoggedInUser').user_id)>
		<cfset var docTemplateQry = ""/>
		<cfset var docSnapshotDetailQry = "" />
		<cfset var docSnapshotBaseVO = "" />
		<cfset var contextKey = "" />

		<cfset bValid = true>
		<cfif Trim(arguments.signFVO.getSIGNOFF_DT()) EQ "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Please indicate the sign-off date.", "signoff_dt")>
		</cfif>
		<cfif Trim(arguments.signFVO.getDOC_PROOF_FILE_CD()) EQ "">
			<cfset bValid = false>
			<cfset variables.MsgBox.error("Please upload the proof of signature file.", "")>
		</cfif>
		<cfif NOT bValid>
			<cfreturn bValid>
		</cfif>

		<cftransaction action="begin">
			<cftry>
				<cfset var backwardCompatibility = ModuleConfig.getSetting("BACKWARD_COMPATIBILITY")>
				<cfset docSnapshotBaseVO = docSnapshotBaseDAO.read(arguments.signFVO.getDOC_SNAPSHOT_CD())/>
				<cfset docSnapshotDetailQry = docSnapshotDetailGtw.getByPK(arguments.signFVO.getDOC_SNAPSHOT_CD())>

				<cfif not backwardCompatibility>
					<cfset contextKey = "STUDENT_ID=" & docSnapshotDetailQry.STUDENT_ID & "+TRAINING_SESSION_CD=" & docSnapshotDetailQry.TRAINING_SESSION_CD />
					<cfset docTemplateQry = DocLetterTemplateGTW.getByPK(docSnapshotDetailQry.DOC_LETTER_TEMPLATE_CD)/>

					<cfset docLetterDataVO.setXML_LETTER_CONTENT(DocGenerate.generateLetterContent(loaDataProvider, contextKey, docTemplateQry.TAGS))/>
					<cfset docLetterDataVO.setCREATION_DT(Now())/>
					<cfset docLetterDataCD = docLetterDataDAO.add(docLetterDataVO)/>
					<cfset docSnapshotBaseVO.setDOC_LETTER_DATA_CD(docLetterDataCD)>
				</cfif>

				<cfset docSnapshotBaseVO.setDOC_SNAPSHOT_CD(arguments.signFVO.getDOC_SNAPSHOT_CD())>
				<cfset docSnapshotBaseVO.setDOC_SUBMITTED_USER_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)>
				<cfset docSnapshotBaseVO.setDOC_SUBMITTED_DT(arguments.signFVO.getSIGNOFF_DT())>
				<cfset docSnapshotBaseVO.setDOC_PROOF_FILE_CD(arguments.signFVO.getDOC_PROOF_FILE_CD())>

				<cfset docSnapshotBaseDAO.update(docSnapshotBaseVO)>

				<!--- <cfset variables.MsgBox.success('The sign-off proof was succesfully submitted.')> --->
				<cfset success = true>
				<cftransaction action = "commit"/>

				<cfset variables.Interceptor.inform("LOA:#instanceQry.instance_name#_DOCUMENT:AFTER:SIGN_ON_BEHALF",{instance_id = DocSnapshotBaseVO.getINSTANCE_ID(), letter_template_cd=docSnapshotDetailQry.doc_letter_template_cd,training_session_cd=docSnapshotDetailQry.training_session_cd, student_id=docSnapshotDetailQry.student_id})>
				<cfset variables.Interceptor.inform("LOA:AGREEMENT_LETTER:AFTER:SIGN_ON_BEHALF",{instance_id=variables.instance_id,letter_template_cd=docSnapshotDetailQry.doc_letter_template_cd,student_id=docSnapshotDetailQry.student_id,training_session_cd=docSnapshotDetailQry.training_session_cd})>

				<cfcatch type="database">
					<cftransaction action = "rollback"/>
					<cfrethrow />
					<cfset success = false />
				</cfcatch>
			</cftry>
			<cfreturn success />
		</cftransaction>
	</cffunction>

	<cffunction name="CreatePDFPreviews" access="public" output="false" returntype="void">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="false" />

		<cfset var DocZipVO = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocZipVO").init() />
	    <cfset var docHelper = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocHelper").init(variables.instance_id) />
		<cfset var DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init(variables.instance_id) />
		<cfset var DocReportQry = DocReport.getReport(LoaFilterSortVO)>
		<cfset var DocZip = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocZip').init()>

		<!---Here you can change print directory   --->
		<cfset DocZipVO.setPrintDIR(GetTempDirectory())/>
		<cfset DocZipVO.setZipFileName("printPDF_" & DateFormat(now(),'ddmmyyyy') & "_" & TimeFormat(now(),'HHmmss') & ".zip") />
		<cfset DocZipVO.setFolderStore(createUUID())/>

		<cfset printDir = DocZipVO.getPrintDIR()>
        <cfset folderStore = DocZipVO.getfolderStore()>

		<cfdirectory action="create" directory = "#printDir#/#folderStore#" >
    	<!---loop over selected snapshots and create pdf files  --->

		<cfloop query="DocReportQry">
			<cfset DocRenderer	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocRender').init(variables.instance_id, 'PDF')>
			<cfset renderedDoc 	= DocRenderer.renderLOA(doc_snapshot_cd)>
			<cfset PDFfileName = DocReportQry.instance_name & "_" & DocReportQry.TRAINEE_LNAME & "_" & DocReportQry.TRAINEE_FNAME & "_" & DocReportQry.OPHRDC>
			<cfinclude template="../../dsp_CreatePreviewPDF.cfm">
		</cfloop>
		<cfset zip = DocZip.generateZip(DocZipVO) />
	</cffunction>

	<cffunction name="CreateTesPDFPreviews" access="public" output="false" returntype="void">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="false" />

		<cfset var DocZipVO = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocZipVO").init() />
		<cfset var DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
		<cfset var DocReportQry = DocReport.getDocumentsForPrint(arguments.filter)>
		<cfset var DocZip = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocZip').init()>

		<!---Here you can change print directory   --->
		<cfset DocZipVO.setPrintDIR(GetTempDirectory())/>
		<cfset DocZipVO.setZipFileName("printPDF_" & DateFormat(now(),'ddmmyyyy') & "_" & TimeFormat(now(),'HHmmss') & ".zip") />
		<cfset DocZipVO.setFolderStore(createUUID())/>

		<cfset printDir = DocZipVO.getPrintDIR()>
        <cfset folderStore = DocZipVO.getfolderStore()>

		<cfdirectory action="create" directory = "#printDir#/#folderStore#" >
    	<!---loop over selected snapshots and create pdf files  --->
		<cfset var htmlToPDF = application.wirebox.getInstance("HTMLToPDF@CORE")>
		<cfloop query="DocReportQry">
			<cfif doc_snapshot_cd NEQ "">
				<cfset var generatePDFOptions = {}>
				<cfset filename="#DocReportQry.USER_NAME#_#DocReportQry.REPORT_NAME#(#DocReportQry.EVALUATION_PERIOD#)">
				<cfset PDFfileName = "#printDir#/#folderStore#/"&ReReplace(fileName,"[^a-zA-Z0-9-]","_","all")&".pdf">
				<cfset generatePDFOptions.fileName = PDFfileName>
				<cfset generatePDFOptions.orientation = "landscape" >
				<cfset var pdfContent = htmlToPDF.fromUrl("#application.cbController.getSetting('modulesApplicationURL')#/index.cfm?fuseaction=DocumentLetters.viewTesLetter&doc_snapshot_cd=#DocReportQry.DOC_SNAPSHOT_CD#&isDocument=1&pdf=true",generatePDFOptions,false)>
			</cfif>
		</cfloop>
		<cfset zip = DocZip.generateZip(DocZipVO) />
	</cffunction>

	<cffunction name="uploadPDF" access="private" output="false" returntype="any">
		<cfargument name="studentID" type="numeric" required="true">
		<cfargument name="docSnapshotCD" type="numeric" required="true">
		<cfargument name="userID" type="numeric" required="false" default="-1">

		<cfset var DocRenderer	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocRender').init(variables.instance_id, 'PDF')>
	    <cfset var ExternalService = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.ExternalService").init() />
		<cfset var renderedDoc 	= DocRenderer.renderLOA(arguments.docSnapshotCD,arguments.userID)>
		<cfset var studentInfo = ExternalService.getStudentByID(arguments.studentID) />

		<cfset var snapshotQry = variables.dbFactory.create('DocSnapshotDetailGTW').getByPK(arguments.docSnapshotCD) />

		<cfset var fileName = "LETTER_#snapshotQry.DOC_LETTER_TEMPLATE_CD#_#Trim(studentInfo.LAST_NAME)#_#Trim(studentInfo.FIRST_NAME)#_#Trim(studentInfo.OPHRDC)#.pdf" />
		<cfset fileName = Replace(fileName," ","_","all")>
		<cfset fileName = Replace(fileName,":","_","all")>

		<cfset var basePath = application.cbcontroller.getSetting('applicationConfiguration').storageFolder & "\">
		<cfset filePath = "#basePath#\tmp\#fileName#">

		<cfset var htmlToPDF = application.wirebox.getInstance("HTMLToPDF@CORE")>

		<cfset var generatePDFOptions = {}>
		<cfset generatePDFOptions.fileName = filePath>

		<cfset var pdfContent = htmlToPDF.fromUrl("#application.cbController.getSetting('modulesApplicationURL')#/index.cfm?fuseaction=DocumentLetters.viewLetter&instance_id=#variables.instance_id#&student_id=#arguments.studentID#&doc_snapshot_cd=#arguments.docSnapshotCD#&isDocument=1&pdf=true",generatePDFOptions,false)>

		<cfset var AttachmentManager = variables.wirebox.getInstance('AttachmentManager').init(basePath)>
		<cfset var AttachmentInfoVO = variables.wirebox.getInstance('AttachmentInfoVO').init()>
		<cfset AttachmentInfoVO.setFILE_CD(snapshotQry.DOC_FILE_CD)>
		<cfset AttachmentInfoVO.setFILE_NAME(fileName)>
		<cfset AttachmentInfoVO.setFILE_DESC(fileName)>

		<cfset ret = AttachmentManager.saveFile(AttachmentInfoVO)>
		<cfreturn ret.getFILE_CD() />
	</cffunction>


	<cffunction name="getAttachmentFiles" access="public" output="false" returntype="array">
		<cfargument name="studentID" type="numeric" required="true">
		<cfargument name="docSnapshotCD" type="numeric" required="true">

		<cfset var attachmentArr = ArrayNew(1) />
		<cfset var DocSettings = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSettings").init(variables.instance_id)>
		<cfset var DocSettingsQry = DocSettings.getSettings() />

		<!--- Attach PDF --->
		<cfset var pdfFileCD = uploadPDF(arguments.studentID, arguments.docSnapshotCD)>
		<cfset ArrayAppend(attachmentArr, pdfFileCD)/>

		<!--- Attach User Picture --->
		<cfif DocSettingsQry.ATTACH_USER_PICTURE EQ "Y">
			<cfset var LoaDataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.LoaDataProvider").init() />
			<cfset var studentQry = LoaDataProvider.getStudentInfo(arguments.studentID) />

			<cfset var StdRegModuleConfig = application.ModuleManager.getModuleConfig("STUDENT_REGISTRATION") />
			<cfset var fileName = studentQry.PICTURE/>
			<cfset var sourcePath = StdRegModuleConfig.getModuleInfo().folder_path & "\" & StdRegModuleConfig.getSetting("PICTURE_FOLDER") & "\" />

			<cfif fileExists(sourcePath & fileName)>
				<cfset var fileManager = application.wirebox.getInstance('FileManager@CORE').init()>
				<cfset var attachmentInfoVO = fileManager.createVO()>

				<cfset var friendlyFileName = studentQry.STUDENT_NUMBER & "_" & studentQry.LAST_NAME & "_" & studentQry.FIRST_NAME & "." & ListLast(fileName, ".")/>
				<cffile action="copy" source="#sourcePath##fileName#" destination="#fileManager.getRootPath()#\tmp\#fileName#">

				<cfset AttachmentInfoVO.setFILE_NAME(fileName) />
				<cfset AttachmentInfoVO.setFILE_DESC(friendlyFileName) />

				<cfset var ret = fileManager.saveFile(AttachmentInfoVO)>
				<cfset ArrayAppend(attachmentArr, ret.getFILE_CD())/>
			</cfif>
		</cfif>
		<cfreturn attachmentArr>
	</cffunction>

</cfcomponent>