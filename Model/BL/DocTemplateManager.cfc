<cfcomponent displayname="DocTemplateManager" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocTemplateManager">
		<cfargument name="instanceID" type="numeric" required="false" default="1"/>
		<cfscript>
			super.init();
			variables.instance_id = arguments.instanceID;
			return this;
		</cfscript>
	</cffunction>

	<cffunction name="getAllSignatures" access="public" output="false" returntype="struct">
		<cfset var DocSignatureGTW = variables.dbDocumentFactory.create("DocSignatureGTW")>
		<cfset var DocSignatureBL = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocSignature").init(variables.instance_id)>
		<cfset var signatureFolderPath = DocSignatureBL.getSignaturesFolderPath()>

		<cfset allSignatures = DocSignatureGTW.getAll()>
		<cfset CompleteSignatures = StructNew()>
		<cfset CompleteSignatures.filePath = signatureFolderPath>
		<cfset CompleteSignatures.fileNames = allSignatures>
		<cfreturn CompleteSignatures>
	</cffunction>

	<cffunction name="getAll" access="public" output="false" returntype="query">
		<cfargument name="searchFilter" type="any" required="false">
		<cfargument name="sortOptions" type="any" required="false">

		<cfset var DocLetterTemplateGTW = variables.dbDocumentFactory.create("DocLetterTemplateGTW", true) />
		<cfset var docLetterTemplateQry = DocLetterTemplateGTW.getTemplates(arguments.searchFilter, arguments.sortOptions) />
		<cfreturn docLetterTemplateQry>
	</cffunction>

	<cffunction name="getAllTemplates" access="public" output="false" returntype="query">
		<cfargument name="instanceId" type="string" required="false" default="" />

		<cfset var DocLetterTemplateGTW = variables.dbDocumentFactory.create("DocLetterTemplateGTW", true) />
		<cfset var docLetterTemplateQry = DocLetterTemplateGTW.getAll(arguments.instanceId, true) />
		<cfreturn docLetterTemplateQry>
	</cffunction>

	<cffunction name="get" access="public" output="false" returntype="docLetterTemplateFVO">
		<cfargument name="docLetterTemplateCD" type="numeric" required="true">
		<cfscript>
			var docLetterTemplateFVO = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.docLetterTemplateFVO").init();
			var tplQry = getTemplate(arguments.docLetterTemplateCD);
			var docSignatureBL = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.docSignature").init(variables.instance_id);
			var docSignatureFolder = docSignatureBL.getSignaturesFolderPath();

			docLetterTemplateFVO.initFromQuery(tplQry);
			docLetterTemplateFVO.setHTML_LETTER_TEMPLATE_BODY(tplQry.LETTER_TEMPLATE_BODY);
			docLetterTemplateFVO.setDOC_SIGNATURE_FILE_NAME("#docSignatureFolder#/#docLetterTemplateFVO.getDOC_SIGNATURE_FILE_NAME()#");

			return docLetterTemplateFVO;
		</cfscript>
	</cffunction>

	<cffunction name="getTemplate" access="public" output="false" returntype="query">
		<cfargument name="docLetterTemplateCD" type="numeric" required="true">
		<cfscript>
			var docLetterTemplateGTW = variables.dbDocumentFactory.create("DocLetterTemplateGTW");
			return docLetterTemplateGTW.getByTemplateCD(arguments.docLetterTemplateCD);
		</cfscript>
	</cffunction>

	<cffunction name="getTemplatebyDESC" access="public" output="false" returntype="query">
		<cfargument name="docLetterTemplateDESC" type="string" required="true">
		<cfscript>
			var docLetterTemplateGTW = variables.dbDocumentFactory.create("DocLetterTemplateGTW");
			return docLetterTemplateGTW.getByDescription(variables.instance_id,arguments.docLetterTemplateDESC);
		</cfscript>
	</cffunction>

	<cffunction name="save" returntype="boolean" access="public" output="false" hint="">
        <cfargument name="data" type="docLetterTemplateFVO" required="true"/>
        <cfset var docLetterTemplateDAO = variables.dbDocumentFactory.create("DocLetterTemplateDAO") />
        <cfset var docLetterTemplateVO = "" />
        <cfset var docLetterTemplateQry = "" />
		<cfset var docLetterTemplateSignatureDAO = variables.dbDocumentFactory.create("DocLetterTemplateSignatureDAO") />
		<cfset var docLetterTemplateSignatureVO = "" />
		<cfset var action = "">
		<cfset var name = "">
        <!--- validate data --->
        <cfif not validate(arguments.data)>
			<cfreturn false />
        </cfif>

        <!--- check if template already exists --->
		<cfif arguments.data.getDOC_LETTER_TEMPLATE_CD() neq "">
			<cfset docLetterTemplateVO = docLetterTemplateDAO.read( arguments.data.getDOC_LETTER_TEMPLATE_CD() ) />
			<cfset name = docLetterTemplateVO.getLETTER_TEMPLATE_NAME()>
		</cfif>
		<cfif name neq arguments.data.getLETTER_TEMPLATE_NAME()>
			<cfif getTemplatebyDESC(Trim(arguments.data.getLETTER_TEMPLATE_NAME())).recordcount gt 0>
           <cfset variables.MsgBox.error("A template with this name already exists." ) />
           <cfreturn false />
        </cfif>
		</cfif>
        <cftransaction action="begin">
        <cftry>
			<cfset body = arguments.data.getHTML_LETTER_TEMPLATE_BODY()>
			<cfset body = Replace(body, '=""', '="', 'all')>
			<cfset body = Replace(body, '"" ', '" ', 'all')>
			<cfset body = Replace(body, '"">', '">', 'all')>
			<cfset body = Replace(body, '####', '##', 'all')>
			<cfset docLetterTemplateVO = variables.dbDocumentFactory.create("docLetterTemplateVO") />
			<cfset docLetterTemplateVO = variables.populator.populateFromObject(docLetterTemplateVO, arguments.data) />
			<cfset docLetterTemplateVO.setLETTER_TEMPLATE_CODE(arguments.data.getLETTER_TEMPLATE_NAME()) />
			<cfset docLetterTemplateVO.setLETTER_TEMPLATE_BODY(body) />

			<cfif arguments.data.getDOC_LETTER_TEMPLATE_CD() EQ "">
				<!--- add --->
				<cfset docLetterTemplateVO.setCREATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)/>
				<cfset docLetterTemplateVO.setCREATION_DT(Now())/>

            	<cfset template_cd = docLetterTemplateDAO.add(docLetterTemplateVO) />
				<cfset arguments.data.setDOC_LETTER_TEMPLATE_CD(template_cd)/>
				<cfset action="add">
			<cfelse>
				<!--- update --->
            	<cfset docLetterTemplateVO.setMODIFICATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id)/>
            	<cfset docLetterTemplateVO.setMODIFICATION_DT(Now())/>
            	<cfset docLetterTemplateDAO.update(docLetterTemplateVO) />
				<cfset action = "update">
			</cfif>
			<!--- save signature --->
			<!--- first delete record --->
			<cfset docLetterTemplateSignatureDAO.delete(arguments.data.getDOC_LETTER_TEMPLATE_CD()) />
			<cfif Len(arguments.data.getDOC_SIGNATURE_CD())>
				<cfset docLetterTemplateSignatureVO = variables.dbDocumentFactory.create("DocLetterTemplateSignatureVO").init() />
				<cfset docLetterTemplateSignatureVO.setDOC_LETTER_TEMPLATE_CD(arguments.data.getDOC_LETTER_TEMPLATE_CD()) />
				<cfset docLetterTemplateSignatureVO.setDOC_SIGNATURE_CD(arguments.data.getDOC_SIGNATURE_CD()) />
				<cfset docLetterTemplateSignatureVO.setCREATION_ID(variables.SessionStorage.getVar('LoggedInUser').user_id) />
				<cfset docLetterTemplateSignatureVO.setCREATION_DT(Now())/>
				<!--- add record --->
				<cfset docLetterTemplateSignatureDAO.add(docLetterTemplateSignatureVO) />
			</cfif>
			<cfset  variables.MsgBox.success("Template successfully saved." ) />
            <cftransaction action="commit"/>
        <cfcatch>
            <cftransaction action="rollback"/>
			<cfrethrow />
            <cfreturn false />
        </cfcatch>
        </cftry>
        </cftransaction>

		<cfset var eventData = {
			"LETTER_TEMPLATE_CD" = arguments.data.getDOC_LETTER_TEMPLATE_CD(),
			"LETTER_TEMPLATE_NAME" = arguments.data.getLETTER_TEMPLATE_NAME(),
			"LETTER_TEMPLATE_STATUS" = arguments.data.getSTATUS()
		}>
		<cfset variables.interceptor.inform("LOA:LETTER_TEMPLATE:AFTER:#UCase(action)#",eventData)>
        <cfreturn true />
    </cffunction>

	<cffunction name="validate" access="private" returntype="boolean" output="false">
        <cfargument name="data" type="docLetterTemplateFVO" required="true"/>
        <cfset var valid = true />
		<cfif not Len(Trim(arguments.data.getLETTER_TEMPLATE_NAME()))>
			<cfset variables.MsgBox.error("Name is required.","LETTER_TEMPLATE_NAME") />
            <cfset valid = false />
		 </cfif>
		 <cfif not Len(arguments.data.getINSTANCE_ID())>
			<cfset variables.MsgBox.error("Letter is required.","instance_id") />
            <cfset valid = false />
		 </cfif>
		<cfset intentText = Trim(arguments.data.getHTML_LETTER_TEMPLATE_BODY()) />
		<cfset intentText = Replace(intentText, "&nbsp;", "", "all") />
		<cfset intentText = REReplaceNoCase(intentText,"<[^>]*>","","ALL")>
		<cfset intentText = Trim(intentText) />
		<cfif Len(intentText) eq 0 >
			<cfset variables.MsgBox.error("Body is required.","HTML_LETTER_TEMPLATE_BODY") />
           	<cfset valid = false />
		</cfif>
        <cfreturn valid />
    </cffunction>

	<cffunction name="deleteDocTemplate" returntype="boolean" access="public" output="false" hint="">
        <cfargument name="data" type="docLetterTemplateFVO" required="true"/>
		<cfset var docLetterTemplateGTW = variables.dbDocumentFactory.create("DocLetterTemplateGTW") />
        <cfset var docLetterTemplateDAO = variables.dbDocumentFactory.create("docLetterTemplateDAO") />
		<cfset var docLetterTemplateSignatureDAO = variables.dbDocumentFactory.create("DocLetterTemplateSignatureDAO") />
		<cfset var docLetterTemplateSignatureVO = "" />

        <!--- validate data --->

        <cftransaction action="begin">
        <cftry>
			<cfif arguments.data.getDOC_LETTER_TEMPLATE_CD() NEQ "">
				<!--- check if template assigned to category --->
				<cfset docLetterTemplateQry = docLetterTemplateGTW.getSetupsByTemplateCD(arguments.data.getDOC_LETTER_TEMPLATE_CD())/>
                <cfif docLetterTemplateQry.recordCount eq 0>
				    <!--- delete --->
					<cfset docLetterTemplateSignatureDAO.delete(arguments.data.getDOC_LETTER_TEMPLATE_CD()) />
					<cfset docLetterTemplateDAO.deleteDocTemplate(arguments.data.getDOC_LETTER_TEMPLATE_CD()) />
				<cfelse>
					<cfset variables.MsgBox.success("Template cannot be deleted, in use by setup#(docLetterTemplateQry.recordCount eq 1 ? '' : 's')#: <strong>" & ValueList(docLetterTemplateQry.DOC_SETUP_NAME, ", ") & "</strong>." ) />
					<cfreturn false />
				</cfif>
			</cfif>
				<cfset variables.MsgBox.success("Template successfully deleted" ) />
			<cftransaction action="commit"/>
        <cfcatch>
			 <cfset variables.MsgBox.error("Error in deleting template" ) />
             <cftransaction action="rollback"/>
             <cfreturn false />
			 <cfrethrow />
        </cfcatch>
        </cftry>
        </cftransaction>

		<cfset variables.interceptor.inform("LOA:LETTER_TEMPLATE:AFTER:DELETE",{letter_template_cd=arguments.data.getDOC_LETTER_TEMPLATE_CD()})>
        <cfreturn true />
    </cffunction>
</cfcomponent>