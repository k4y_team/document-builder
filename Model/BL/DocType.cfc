<cfcomponent displayname="DocType" output="false" extends="ServiceBase">
	<cfproperty name="instanceId" type="numeric" >

	<cffunction name="init" access="public" output="false" returntype="DocType">
		<cfargument name="instanceId" type="any" required="false" default=""/>
		<cfset variables.instanceId = arguments.instanceId />
		<cfset super.init() />
		<cfreturn this/>
	</cffunction>

	<cffunction name="getTags" access="public" output="false" returntype="query">
		<cfif variables.instanceId NEQ "">
			<cfset var docInstanceGtw = variables.dbDocumentFactory.create("docInstanceGtw") />
			<cfset var LOAInstanceQry = docInstanceGtw.getByPK(variables.instanceId) />
		</cfif>

		<cfset var tagsQry = QueryNew("TAG_CD,TAG_CODE,TAG_TYPE,TAG_DATA_TYPE,COLLECTION") />
		<cfscript>
			var cnt = 0;
			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "HEADER_INFO");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "HEADER_INFO_GRAD_STUDIES");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "HEADER_INFO_MIDWIFERY");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "HEADER_INFO_NURSING");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "HEADER_INFO_PT_TES");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "HEADER_INFO_OT_TES");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "CURRENT_DATE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "DATE");
			QuerySetCell(tagsQry, "COLLECTION", "MISC");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "NETWORK_ACCESS_ID");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "NETWORK_PASSWORD");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "OPHRDC_NUMBER");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_ID");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_MINC");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "LOGS");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_NUMBER");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_NAME");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_PICTURE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_ADDRESS");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_MAILING_PHONE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "LOGS");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINING_LINE");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINING_LINE_EXTENDED");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINING_DETAIL");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINING_SESSION");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINING_START_DATE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINING_END_DATE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINING_PROFILE");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "REG_FEE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "PROGRAM_DESC");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "DEPARTMENT_DESC");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "DOCUMENT_DATE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_DETAIL");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_SCHOOL");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "TRAINEE_EMAIL");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "LETTER_TYPE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "LOA");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "MIN_REQ_EXPIRY_DATE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "LOGS");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "REQUIREMENT_DETAIL");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "LOGS");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "ROTATION_DETAIL");
			QuerySetCell(tagsQry, "TAG_TYPE", "TABLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "QUERY");
			QuerySetCell(tagsQry, "COLLECTION", "LOGS");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "SIGNATURE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "SIGNATURE");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "SIGNATURE_PROOF");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "SIGNATURE");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "SIGNATURE_PROOF_DATE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "SIGNATURE");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "SIGNATURE_TYPE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "SIGNATURE");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "INPUT_SIGNATURE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "INPUT_SIGNATURES");

			QueryAddRow(tagsQry); cnt++;
			QuerySetCell(tagsQry, "TAG_CD", cnt);
			QuerySetCell(tagsQry, "TAG_CODE", "INPUT_DATE");
			QuerySetCell(tagsQry, "TAG_TYPE", "SIMPLE");
			QuerySetCell(tagsQry, "TAG_DATA_TYPE", "STRING");
			QuerySetCell(tagsQry, "COLLECTION", "INPUT_SIGNATURES");
		</cfscript>
		<cfif variables.instanceId NEQ "" AND LOAInstanceQry.TAGS NEQ "">
			<cfset availableTags = LOAInstanceQry.TAGS />
			<cfset availableTags = ListAppend(availableTags, "INPUT_SIGNATURE") />
			<cfset availableTags = ListAppend(availableTags, "INPUT_DATE") />
			<cfset availableTags = ListRemoveDuplicates(availableTags) />
			<cfquery dbtype="query" name="tagsQry">
				SELECT * FROM tagsQry WHERE TAG_CODE IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#availableTags#" list="true" />)
			</cfquery>
		</cfif>
		<cfreturn tagsQry />
	</cffunction>

	<cffunction name="renderExtraFilters" access="public" output="false" returntype="string">
		<cfargument name="student_id" type="numeric" required="true"/>
		<cfargument name="training_session_cd" type="numeric" required="true"/>

		<cfset var formHtml = "" />
		<cfset var docInstanceGtw = variables.dbDocumentFactory.create("docInstanceGtw") />
		<cfset var LOAInstanceQry = docInstanceGtw.getByPK(variables.instanceId) />

		<cfif LOAInstanceQry.ENTITY_CODE NEQ "">
			<cfset var EntityRegistry = variables.wirebox.getInstance('EntityRegistry@CORE') />
			<cfset formHtml = EntityRegistry.renderEntityForm(entityCode=LOAInstanceQry.ENTITY_CODE, params=arguments, values=arguments) />
		</cfif>
		<cfreturn formHtml/>
	</cffunction>
</cfcomponent>