<cfcomponent displayname="DocTypeRender" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DocTypeRender">
		<cfargument name="instanceID" type="numeric" required="true">
		<cfargument name="DOCTYPECD" type="any" required="true" default="">
		<cfargument name="DOC_LETTER_TEMPLATE_CD" type="numeric" required="false">
		<cfargument name="renderTypeCode" type="string" required="false" default="HTML" hint="HTML/PDF">
		<cfscript>
			super.init();
			/*if use dummy data*/
			variables.instance_id = arguments.instanceID;
			if (arguments.DOCTYPECD EQ "-1") {
				var docTemplateManager = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init();
				var tplFVO = "";
				tplFVO = docTemplateManager.get(arguments.DOC_LETTER_TEMPLATE_CD);
				variables.template = tplFVO.getHTML_LETTER_TEMPLATE_BODY();
			} else {
				/*get template body*/
				var docTemplateManager = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init();
				var tplFVO = "";
				if(Len(arguments.DOC_LETTER_TEMPLATE_CD)){
					tplFVO = docTemplateManager.get(arguments.DOC_LETTER_TEMPLATE_CD);
					variables.template = tplFVO.getHTML_LETTER_TEMPLATE_BODY();
				} else {
					variables.template = "TEMPLATE NOT FOUND (#arguments.docTypeCD#)";
				}
			}
			/*tag regexp*/
			variables.tagFilter = "\{([a-zA-Z0-9_]*)\}";
			variables.renderTypeCode = arguments.renderTypeCode;
			variables.docHelper = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocHelper").init(variables.instance_id);
			variables.instanceQry = variables.docHelper.getInstance();
			return this;
		</cfscript>
	</cffunction>

	<cffunction name="render" access="public" output="false" returntype="string">
		<cfargument name="data" type="xml" required="true">
		<cfargument name="docSnapshotCD" type="numeric" required="true">
		<cfargument name="DOC_LETTER_TEMPLATE_CD" type="numeric" required="false">

		<cfscript>
			if (arguments.docSnapshotCD EQ "-1") {
				var dataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider").init(variables.instance_id, arguments.data, arguments.docSnapshotCD, arguments.DOC_LETTER_TEMPLATE_CD);
			} else {
				var dataProvider = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider").init(variables.instance_id, arguments.data, arguments.docSnapshotCD);
			}

			var snapshotQry = variables.dbDocumentFactory.create('DocSnapshotBaseGTW').getByPK(arguments.docSnapshotCD);


			var snapshotDataQry = variables.dbDocumentFactory.create('DocSnapshotDetailGTW').getDataByPK(arguments.docSnapshotCD);
			var snapshotChangesQry = variables.dbDocumentFactory.create('DocSnapshotDetailGTW').getChangesByPK(arguments.docSnapshotCD);
			var templateContent = "";

			if (snapshotQry.TEMPLATE_BODY eq "") {
				templateContent = variables.template;
			} else {
				templateContent = snapshotQry.TEMPLATE_BODY;
			}
			var queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder");
			var queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils");
			var params = {};
			params.DOC_SETUP_ID = snapshotQry.DOC_SETUP_ID;
			var dataStruct = {};
	        var userSessionData = sessionStorage.getVar( 'LoggedInUser' );
			var UserDirectoryAPI = application.wirebox.getInstance(name="", dsl="API@UserDirectory");
			var	SecurityService = UserDirectoryAPI.getUserSecurity(1, userSessionData.user_id);

			if (snapshotQry.DOC_PUBLISHED_DT eq "" and SecurityService.checkAccess('REMOVE_COMMENTS')){
				dataStruct.READONLY = false;
			}else{
				dataStruct.READONLY = true;
			}
			var documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init();
			var instanceConfig = documentInstances.getInstanceConfig(variables.instance_id);
			if (arguments.docSnapshotCD EQ "-1") {
				dataStruct.READONLY = true;
				params.IS_DUMMY = true;
				if(!StructIsEmpty(instanceConfig)){
		            var dataProviderEntitiesList = ListAppend(instanceConfig.CONFIG_DATA_PROVIDER_ENTITY,instanceConfig.DOCUMENT_DATA_PROVIDER_ENTITY) ;
					for (entity in dataProviderEntitiesList){
						var providerEntity = QueryBuilder.GETENTITY(entity);
						for (property in providerEntity.GETPROPERTIESASLIST()){
							dataStruct["#property#"] = queryUtils.queryToArray(queryBuilder.runQuery(sql=providerEntity.getSQL('#property#'),datasource=providerEntity.GETENTITYDATASOURCE(),params=params));
						}
					}
				}
			}


			if(!StructIsEmpty(instanceConfig)){


			var dataProviderEntitiesList = instanceConfig.CONFIG_DATA_PROVIDER_ENTITY;
			for (entity in dataProviderEntitiesList){
				var providerEntity = QueryBuilder.GETENTITY(entity);
					for (property in providerEntity.GETPROPERTIESASLIST()){
							dataStruct["#property#"] = queryUtils.queryToArray(queryBuilder.runQuery(sql=providerEntity.getSQL('#property#'),datasource=providerEntity.GETENTITYDATASOURCE(),params=params));
					}
			}

			for (record in queryUtils.queryToArray(snapshotDataQry)){
				dataStruct["#record.key#"] = DeserializeJson(record.VALUE);
			}
			for (record in queryUtils.queryToArray(snapshotChangesQry)){
				dataStruct["#record.key#_CHANGES"] = DeserializeJson(record.VALUE);
			}



			dataStruct.RENDER_TYPE  = variables.renderTypeCode ;
			dataStruct.DOC_SNAPSHOT_CD =  arguments.docSnapshotCD;


			var templateRenderer = QueryBuilder.GETENTITY(instanceConfig.DOCUMENT_RENDERER_ENTITY);
				}else{
				templateRenderer="";
				}

			return processTemplate(templateContent,templateRenderer,dataStruct,dataProvider);
		</cfscript>
	</cffunction>

	<cffunction name="processTemplate" access="private" output="false" returntype="string">
		<cfargument name="templateContent" type="any" required="true">
		<cfargument name="templateRenderer" type="any" required="true">
		<cfargument name="dataStruct" type="any" required="true">
		<cfargument name="dataProvider" type="any" required="false">

		<cfscript>

			var start = 1;

			var parseResult = "TEST";
			var matchStruct = "";
			var match = "";
			var tagCode = "";
			var parseResult = "";
			var iterIdx = 0;
			while(start neq 0){
				iterIdx = iterIdx + 1;
				if (iterIdx gt 50) {
					throw(message="Letter rendering: Too many recursive iterations!");
				}
				/*match tag using tagFilter regexp*/
				 matchStruct =  REFindNoCase(variables.tagFilter,templateContent, 0, true);

				 if(matchStruct.pos[1] gt 0){
				 	/*we found a tag*/
				 	match = Mid(templateContent, matchStruct.pos[1], matchStruct.len[1]);
				 	/*remove {} from tag*/
				 	tagCode = Replace(Replace(match, "{", ""), "}", "");
				 	/*get rendered tag*/
				 	parseResult = renderTagTes(tagCode, templateRenderer,dataStruct);
					if (isDefined('arguments.dataProvider')){
						parseResult1 = renderTag(tagCode, dataProvider);
					}else{
						parseResult1 ="";
					}
				 	if (parseResult1 EQ ""){
				 	/*replace match tag with rendered value*/
				 	templateContent = Replace(templateContent, match, parseResult);
				 } else {
				 		templateContent = Replace(templateContent, match, parseResult1);
				 	}
				 } else {
				 	start = 0;
				 }
			}
			return templateContent;
		</cfscript>

	</cffunction>

	<cffunction name="renderTagTes" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="templateRenderer" type="any" required="true">
		<cfargument name="dataStruct" type="any" required="true">


		<cfset templateEngine = application.wirebox.getInstance("TemplateEngine@CORE") />
		<cfset queryTemplate = templateEngine.createQueryTemplate() />
		<cfscript>
			try {
				queryTemplate.setTemplate(arguments.templateRenderer.getTemplate(arguments.code));
				return templateEngine.render(queryTemplate,arguments.dataStruct);
			} catch (any e) {
				return "";
			}
		</cfscript>
	</cffunction>

	<cffunction name="renderSetupDocument" access="public" output="false" returntype="string">
		<cfargument name="params" type="any" required="true">
		<cfscript>
			var DocSetup = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSetup" ).init();
			var docSetupQry = DocSetup.getDocSetupByPk(arguments.params);
			var queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder");
			var queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils");
			var documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init();
			var docTemplateManager = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init();
			var tplFVO = docTemplateManager.get(docSetupQry.DOC_LETTER_TEMPLATE_CD);
			var templateContent = tplFVO.getHTML_LETTER_TEMPLATE_BODY();
			var instanceConfig = documentInstances.getInstanceConfig(docSetupQry.instance_id);
			var dataStruct = {};
			var Start = GetTickCount();
			if(!StructIsEmpty(instanceConfig)){
				var dataProviderEntitiesList = instanceConfig.CONFIG_DATA_PROVIDER_ENTITY;
				for (entity in dataProviderEntitiesList){
					var providerEntity = QueryBuilder.GETENTITY(entity);
					for (property in providerEntity.GETPROPERTIESASLIST()){
						var Start1 = GetTickCount();
						dataStruct["#property#"] = queryUtils.queryToArray(queryBuilder.runQuery(sql=providerEntity.getSQL('#property#'),datasource=providerEntity.GETENTITYDATASOURCE(),params=params));
						var end1 = GetTickCount();
						var total1=end1-start1;
						WriteLog(type="Error", file="performance.log", text="#total1#__#property#");
					}
				}
			}
			var end = GetTickCount();
			var total=end-start;
			WriteLog(type="Error", file="performance.log", text="#total#");
			var templateRenderer = QueryBuilder.GETENTITY(instanceConfig.DOCUMENT_RENDERER_ENTITY);
			return processTemplate(templateContent,templateRenderer,dataStruct);
		</cfscript>

	</cffunction>
	<cffunction name="renderTag" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfscript>
			try {
				switch(arguments.code){
					case "TRAINEE_PROFILE":
						return renderTraineeProfile(arguments.code, arguments.dp);
					break;
					case "TRAINING_LINE":
						return renderTrainingLine(arguments.code, arguments.dp);
					break;
					case "TRAINING_LINE_EXTENDED":
						return renderTrainingLineExtended(arguments.code, arguments.dp);
					break;
					case "TRAINING_DETAIL":
						return renderTrainingDetail(arguments.code, arguments.dp);
					break;
					case "HEADER_INFO":
						return renderHeaderInfo(arguments.code, arguments.dp);
					break;
					case "HEADER_INFO_GRAD_STUDIES":
						return renderHeaderInfoCustom(arguments.code, arguments.dp);
					break;
					case "HEADER_INFO_MIDWIFERY":
						return renderHeaderInfoCustom(arguments.code, arguments.dp);
					break;
					case "HEADER_INFO_NURSING":
						return renderHeaderInfoCustom(arguments.code, arguments.dp);
					break;
					case "HEADER_INFO_PT_TES":
						return renderHeaderInfoCustom(arguments.code, arguments.dp);
					break;
					case "HEADER_INFO_OT_TES":
						return renderHeaderInfoCustom(arguments.code, arguments.dp);
					break;
					case "REQUIREMENT_DETAIL":
						return renderRequirementDetail(arguments.code, arguments.dp);
					break;
					case "ROTATION_DETAIL":
						return renderRotationDetail(arguments.code, arguments.dp);
					break;
					case "INPUT_SIGNATURE":
						return renderSignatureInput(arguments.code, arguments.dp);
					break;
					case "INPUT_DATE":
						return renderSignatureDate(arguments.code, arguments.dp);
					break;
					case "TRAINEE_PICTURE":
						return renderTraineePicture(arguments.code, arguments.dp);
					break;
					default:
						/*simple tags*/
						return arguments.dp.getTagSimple(arguments.code);
					break;
				}
			} catch (any e) {
				return "";
			}
		</cfscript>
	</cffunction>

	<cffunction name="renderTraineeProfile" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfset var tagData = arguments.dp.getTagComplex(arguments.code) />
		<cfset var content = "" />
		<cfsavecontent variable="content">
			<cfoutput>
				<table class="tables" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td width="50%"><div class="cap">Name of Medical School/University of Graduation:</div><cfif Len(tagData.MEDICAL_SCHOOL)>#tagData.MEDICAL_SCHOOL#<cfelse><br /></cfif></td>
				 		<td width="50%"><div class="cap">Year of graduation from Medical School:</div><cfif Len(tagData.GRADUATION_YEAR)>#tagData.GRADUATION_YEAR#<cfelse><br /></cfif></td>
					</tr>
					<tr>
						<td><div class="cap">CPSO Registration Number:</div><cfif Len(tagData.cpso_num)>#tagData.cpso_num#<cfelse><div>&nbsp;</div></cfif></td>
						<td><div class="cap">CPSO Certificate Class:</div><cfif Len(tagData.cpso_type)>#tagData.cpso_type#<cfelse><div>&nbsp;</div></cfif></td>
					</tr>
				</tbody>
				</table>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderSignatureInput" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">

		<cfset var tagData = arguments.dp.getTagSimple(arguments.code) />
		<cfset var content = "" />

		<cfsavecontent variable="content">
			<cfoutput>
				<cfif variables.renderTypeCode EQ "HTML">
					<span style="vertical-align:middle;display:inline-block;min-height:20px !important;width:70px">Signature:</span>
					<cfif tagData eq "">
					<div style="display: inline-block; width: 250px;min-height:20px !important;">
						<input type="text" name="signature" class="signatureInput"  style="text-align: center; min-height: 20px !important; margin-bottom: 0px;" />
					</div>
					<cfelse>
					<div style="border-bottom: 1px solid ##000; background: ##F5F175; text-align: center; min-height: 20px !important; display: inline-block; width: 250px;padding-top: 5px;">
						#tagData#
					</div>
					</cfif>
				<cfelseif variables.renderTypeCode EQ "PDF">
					<span style="vertical-align:middle;display:inline-block;min-height:20px !important;width:70px;float:left;">Signature:</span>
					<div style="border-bottom: 1px solid ##000; width: 250px; text-align: center; height: 20px; margin-left:10px;display:inline-block;float:left;">
						<strong>#tagData#</strong>
					</div>
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderSignatureDate" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">

		<cfset var tagData = arguments.dp.getTagSimple(arguments.code) />
		<cfset var content = "" />

		<cfsavecontent variable="content">
			<cfoutput>
				<cfif variables.renderTypeCode EQ "HTML">
					<span style="vertical-align:middle;display:inline-block;min-height:20px !important;">Date:</span>
					<div style="border-bottom: 1px solid ##000; background: ##F5F175; text-align: center; width:100px; margin-left: 10px; min-height: 20px !important; display: inline-block;padding-top:5px;">
						<strong>
							#tagData#
						</strong>
					</div>
				<cfelseif variables.renderTypeCode EQ "PDF">
					<span style="vertical-align:middle;display:inline-block;min-height:20px !important;">Date:</span>
					<div style="border-bottom: 1px solid ##000; text-align: center; float: right; height: 20px; width:100px; margin-left:10px;">
						<strong>#tagData#</strong>
					</div>
				</cfif>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderTrainingLine" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfset var tagData = arguments.dp.getTagComplex(arguments.code) />
		<cfset var content = "" />

		<cfsavecontent variable="content">
			<cfoutput>
				#variables.docHelper.aggTrainingLines(tagData)#
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderTrainingLineExtended" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfset var tagData = arguments.dp.getTagComplex(arguments.code) />
		<cfset var content = "" />

		<cfsavecontent variable="content">
			<cfoutput>
				<ul>
					<cfloop query="tagData">
						<li>Registered in the program of <strong>#tagData.PROGRAM_DESC#</strong> at the training level of <strong>#tagData.TRAINING_LEVEL_DESC#</strong> from <strong>#tagData.START_DT#</strong> to <strong>#tagData.END_DT#</strong>.</li>
					</cfloop>
				</ul>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderTrainingDetail" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfset var tagData = arguments.dp.getTagComplex(arguments.code) />
		<cfset var content = "" />

		<cfsavecontent variable="content">
			<cfoutput>
				<table class="tables" cellspacing="0" cellpadding="0">
					<thead>
						<th>Program</th>
						<th>Training Level</th>
						<th>Source of Funding</th>
						<th>Status</th>
						<!--- <th>Time</th> --->
						<th>Group</th>
						<th>Pool</th>
						<th style="text-align: center">Term</th>
					</thead>
					<tbody>
						<cfloop query="tagData">
							<tr>
								<td>
									#tagData.PROGRAM_DESC#&nbsp;
									<cfif tagData.TRAINEE_TYPE neq "TRAINEE">- #tagData.TRAINEE_TYPE#</cfif>
								</td>
								<td>#tagData.TRAINING_LEVEL_DESC#&nbsp;</td>
								<td>#tagData.FUNDING_SOURCE#&nbsp;</td>
								<td>#tagData.TRAINING_STATUS_DESC#&nbsp;</td>
								<!--- <td>#tagData.TIME#&nbsp;</td> --->
								<td>#tagData.GROUP#&nbsp;</td>
								<td>#tagData.POOL#&nbsp;</td>
								<td class="nowrap" style="text-align: center"><cfif Len(tagData.START_DT) and Len(tagData.END_DT)>#tagData.START_DT#<br />#tagData.END_DT#<cfelse>&nbsp;</cfif></td>
							</tr>
						</cfloop>
					</tbody>
				</table>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderHeaderInfo" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfset var tagData = arguments.dp.getTagComplex(arguments.code) />
		<cfset var content = "" />
		<cfsavecontent variable="content">
			<cfoutput>
				<div class="letterOfApp" style="border-bottom: 1px solid black; padding-bottom:5px;">
					<cfif variables.INSTANCEQRY.INSTANCE_CODE EQ "UG_TES_REPORT">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td style="width:250px;text-align:center;">
									<img src="#application.cbcontroller.getSetting('applicationBaseURL')#/includes/img/McMaster_School_of_Medicine_logo.jpg" style="width:580px;" alt="#tagData.client_name# Logo">
								</td>
							</tr>
						</table>
					<cfelse>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td style="width:150px;">
									<!--- <cfif variables.renderTypeCode EQ "PDF">
										<img src="#tagData.file_path#" alt="#tagData.client_name# Logo">
									<cfelseif variables.renderTypeCode EQ "HTML"> --->
										<img src="#tagData.file_url#" alt="#tagData.client_name# Logo">
									<!--- </cfif> --->
								</td>
								<td  style="border-left:1px solid black;width:50px;">
								</td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td style="vertical-align: top;font-size: 20px;">
												<br />
												<!---#tagData.OFFICE#--->
												<div style="font-size: 25px;">Postgraduate </div></br>
												<div style="font-size: 25px;margin-top:-10px;">Medical Education</div>
											</td>
											<td width="30%" style="vertical-align: top;">
												#tagData.FACULTY#<br />
												MDCL 3101A<br>1280 Main Street West<br>Hamilton, Ontario<br>Canada L8S 4K1
											</td>
											<td width="30%" style="vertical-align: top;">
												</br>
												<cfif Trim(tagData.PHONE) neq "">Phone: #tagData.PHONE#
												<cfif trim(tagData.EXT) neq ""><br />Ext: #tagData.EXT# or 22118</cfif></cfif>
												<cfif Trim(tagData.FAX) neq ""><br />Fax: #tagData.FAX# </cfif>
												<!---<cfif StructKeyExists(tagData,"WEBSITE") and Trim(tagData.WEBSITE) neq ""><br />#tagData.WEBSITE#</cfif>--->
												<cfif StructKeyExists(tagData,"EMAIL") and Trim(tagData.EMAIL) neq ""><br />Email: <u>#tagData.EMAIL#<u></cfif>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</cfif>
				</div>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>
	<cffunction name="renderHeaderInfoCustom" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfset var tagData = arguments.dp.getTagComplex(arguments.code) />

		<cfset var content = "" />
		<cfsavecontent variable="content">
			<cfoutput>
				<div class="letterOfApp" style="border-bottom: 1px solid black; padding-bottom:5px;">
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td style="width:250px;text-align:center;">
									<img src="#tagData.image_name#" style="width:580px;" alt="#tagData.client_name# Logo">
								</td>
							</tr>
						</table>
				</div>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderRequirementDetail" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfset var tagData = arguments.dp.getTagComplex(arguments.code) />
		<cfset var content = "" />

		<cfsavecontent variable="content">
			<cfoutput>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<cfif StructKeyExists(tagData, "CPSO_NUM")>
							<tr>
								<td width="130">CPSO:</td>
								<td widht="200">#tagData.CPSO_NUM#</td>
								<td width="60">Expiry:</td>
								<td>#tagData.CPSO_EXPIRY_DT#</td>
							</tr>
						</cfif>
						<cfif StructKeyExists(tagData, "CMPA_NUM")>
							<tr>
								<td>CMPA:</td>
								<td>#tagData.CMPA_NUM#</td>
								<td>Expiry:</td>
								<td>#tagData.CMPA_EXPIRY_DT#</td>
							</tr>
						</cfif>
						<tr>
							<td>N95 Mask Fit:</td>
							<td>#tagData.MASK_TYPE_DESC#</td>
							<td>Expiry:</td>
							<td>#tagData.MASK_EXPIRY_DT#</td>
						</tr>
						<tr>
							<td>Scrub Size:</td>
							<td colspan="3">#tagData.SCRUB_SIZE#</td>
						</tr>
						<cfif StructKeyExists(tagData, "VISA_EXPIRY_DT")>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td>Work Permit:</td>
								<td colspan="3">Valid until #tagData.VISA_EXPIRY_DT# and covers our affiliated teaching site and Pediatric Complex Care rotation sites.</td>
							</tr>
						</cfif>
					</tbody>
				</table>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderRotationDetail" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">
		<cfset var tagData = arguments.dp.getTagComplex(arguments.code) />
		<cfset var content = "" />
        <cfset var rotationExtraData = "" />

		<cfsavecontent variable="content">
			<cfoutput>
				<table class="tables" cellspacing="0" cellpadding="0">
					<thead>
						<th>Rotation</th>
						<th>Location</th>
						<th>Rotation Dates</th>
						<th>Supervisor(s)</th>
					</thead>
					<tbody>
						<cfloop query="tagData">
                            <cfset rotationExtraData = "" />
                            <cfif Trim(tagData.ROTATION_TYPE) NEQ "">
                                <cfset rotationExtraData = ListAppend(rotationExtraData, "Type: #tagData.ROTATION_TYPE#") />
                            </cfif>
                            <cfif Trim(tagData.SERVICE_FORMAT) NEQ "">
                                <cfset rotationExtraData = ListAppend(rotationExtraData, "Format: #tagData.SERVICE_FORMAT#") />
                            </cfif>
							<tr>
								<td>
                                    #tagData.ROTATION_DESC#&nbsp;[#tagData.ROTATION_PROGRAM_DESC#]
                                    <cfif tagData.SERVICE NEQ "">
                                        <br>Service: #tagData.SERVICE#
                                    </cfif>
                                    <cfif ListLen(rotationExtraData)>
                                        <br>#ReplaceNoCase(rotationExtraData, ",", ", ")#
                                    </cfif>
                                </td>
								<td>#tagData.LOCATION_DESC#&nbsp;</td>
								<td class="nowrap" style="text-align: center"><cfif Len(tagData.START_DT) and Len(tagData.END_DT)>#tagData.START_DT#<br />#tagData.END_DT#<cfelse>&nbsp;</cfif></td>
								<td>
									<cfset supIdx = 1/>
									<cfloop list="#tagData.SUPERVISOR_NAMES#" index="supervisorName" delimiters=";">
										<cfif supIdx GT 1></br></cfif>
										#supervisorName#&nbsp;
										<cfset supIdx++/>
									</cfloop>
								</td>
							</tr>
						</cfloop>
					</tbody>
				</table>
			</cfoutput>
		</cfsavecontent>

		<cfreturn content />
	</cffunction>

	<cffunction name="renderTraineePicture" access="private" output="false" returntype="string">
		<cfargument name="code" type="string" required="true">
		<cfargument name="dp" type="medsisOld.modules.DocumentBuilder.Model.BL.DocLetterDataProvider" required="true">

		<cfset var tagData = arguments.dp.getTagSimple(arguments.code) />
		<cfset var content = "" />
		<cfsavecontent variable="content">
			<cfoutput>
				<div class="user-picture">
					<img alt="" src="#tagData#" style="width: 130px;"/>
				</div>
			</cfoutput>
		</cfsavecontent>
		<cfreturn content />
	</cffunction>

</cfcomponent>