<cfcomponent name="DocZipVO" displayname="DocZipVO" extends="sis_core.Model.ModelBase">
    <cfproperty name="PrintDIR" type="string">
	<cfproperty name="folderStore" type="string">
	<cfproperty name="ZipFileName" type="string">
	
	<cffunction name="init" access="public" returntype="DocZipVO" output="false" displayname="init" hint="I initialize a DocZipVO">
  		<cfset super.init() />
  		<cfset variables.PrintDIR = "" />
		<cfset variables.folderStore = "" />
		<cfreturn this />
	</cffunction>	
	<cffunction name="setPrintDIR" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true">
		<cfset variables.PrintDIR = arguments.val />
	</cffunction>
	
	<cffunction name="getPrintDIR" access="public" returntype="any" output="false">
		<cfreturn variables.PrintDIR />
	</cffunction>
	
	<cffunction name="setfolderStore" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true">
		<cfset variables.folderStore = arguments.val />
	</cffunction>
	
	<cffunction name="getfolderStore" access="public" returntype="any" output="false">
		<cfreturn variables.folderStore />
	</cffunction>		
	<cffunction name="setZipFileName" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true">
		<cfset variables.ZipFileName = arguments.val />
	</cffunction>
	
	<cffunction name="getZipFileName" access="public" returntype="any" output="false">
		<cfreturn variables.ZipFileName />
	</cffunction>		
	
	
	
</cfcomponent>