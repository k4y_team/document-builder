<cfcomponent displayname="DOCZip" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="DOCZip">
		<cfset super.init() />
		<cfreturn this/>
	</cffunction>

    <cffunction name="generateZip" access="public" output="false" returntype="void">
		<cfargument name="DocZipVO" type="DocZipVO" required="true" />

		<cfset var zipFile = DocZipVO.getZipFileName()>
		<cfset var printDir= DocZipVO.getPrintDIR()/>
		<cfset var folderStore = arguments.DocZipVO.getfolderStore()>
		<cfdirectory action="list" directory="#printDir#/#folderStore#" name="pdfFiles">
		<cftry>
			<cfif pdfFiles.RecordCount GT 0>
				<cfzip action="zip"
					source="#printDir#/#folderStore#"
					file="#printDir#/#zipFile#"
					overwrite="yes" />
			<cfelse>
				<cfthrow message="No files generated." errorcode="">
			</cfif>
			<!---delete files from tmp  --->
			<cfdirectory action = "delete" directory = "#printDir#/#folderStore#" recurse="true">
			<cfset file_name = zipFile>
			<cfset folder_name =printDir>
	        <cfset getfile="#FOLDER_NAME#/#FILE_NAME#">
	        <cfset file_size="#getFileInfo(getfile).size#">
	        <cffile action="readBinary" file="#getfile#" variable="serve_file">
			<cfheader name="Set-Cookie" value="print-letters=true; path=/" />
	        <cfheader name="Content-Disposition" value="attachment;filename=#FILE_NAME#">
	        <cfheader name="Content-Length" value="#file_size#">
	        <cfcontent type="application/zip, application/x-zip, application/x-zip-compressed, application/octet-stream, application/x-compress, application/x-compressed, multipart/x-zip" reset="true" variable="#toBinary(serve_file)#">
			<cfcatch>
				<cfset msg = ArrayNew(1)>
				<cfset placeholder = StructNew()>
				<cfset placeholder.type = "error">
				<cfset placeholder.message = 'An error occurred while processing your request.'>
				<cfset ArrayAppend(msg, placeholder)>
				<cfheader name="Set-Cookie" value="print-letters=false; path=/" />
				<cfcontent type="text/html; charset=utf-8" variable="#ToBinary(ToBase64(SerializeJSON(msg)))#"><cfabort>
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>