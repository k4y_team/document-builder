<cfcomponent name="DocumentTasks" displayname="DocumentTasks" output="false" extends="ServiceBase">


	<cffunction name="init" access="public" returntype="DocumentTasks">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>


	<cffunction name="generate" access="public" returntype="struct">
		<cfargument name="eventdata" type="struct" required="false" default="#StructNew()#">
		<cfset var response = StructNew()>
		<cfif StructKeyExists(arguments.eventdata,"instance_id")>
			<cfset var _instanceIDs = arguments.eventData.instance_id>
		<cfelse>
			<cfset var _instanceIDs = "*">
		</cfif>
		<cfif StructKeyExists(arguments.eventdata,"student_id")>
			<cfset var _studentIDs = arguments.eventData.student_id>
		<cfelse>
			<cfset var _studentIDs = "">
		</cfif>
		<cfif StructKeyExists(arguments.eventdata,"training_session_cd")>
			<cfset var _trainingSessionCDs = arguments.eventData.training_session_cd>
		<cfelse>
			<cfset var _trainingSessionCDs = "*">
		</cfif>
		<cfif StructKeyExists(arguments.eventdata,"on_demand")>
			<cfset var _onDemand = arguments.eventData.on_demand>
		<cfelse>
			<cfset var _onDemand = false>
		</cfif>
		<cfset var doaInstances = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocInstances").init()>
		<cfif _instanceIDs eq "*">
			<cfset var documentInstancesQry = doaInstances.getInstances()>
		<cfelse>
			<cfset var documentInstancesQry = doaInstances.getInstanceInfo(_instanceIDs)>
		</cfif>
		<cfloop query="documentInstancesQry">
			<cfif documentInstancesQry.AUTO_GENERATE EQ 'Y' OR _onDemand>
				<cfset var data = StructNew()>
				<cfset StructInsert(data,"instance_id",documentInstancesQry.instance_id)>
				<cfset var docObjectFactory = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.docObjectFactory").init()>
				<cfset var docAdmin = docObjectFactory.create("DOC_ADMIN_COMPONENT").init(argumentCollection=data)>
				<cfset docAdmin.generateOnDemand(stdId=_studentIDs, trSessCD=_trainingSessionCDs, onDemand=_onDemand)>
			</cfif>
		</cfloop>
		<cfset response.status = "SUCCESS">
		<cfreturn response>
	</cffunction>


	<cffunction name="generateLetters" access="public" returntype="struct">
		<cfargument name="data" type="struct" default="#StructNew()#">
		<cfargument name="onDemand" type="boolean" default="false">
		<cfset var result = StructNew()>
		<cfset result.status = "SUCCESS">
		<cfset var groupsQry = "">
		<cfset templatesPackageSize = 20>
		<cfset var coreDSN = application.wirebox.getInstance(dsl="coldbox:setting:datasource")>
		<cfset var DataSourceUtil = application.wirebox.getInstance(name="sis_core.model.util.datasource")>
		<cfset var coreDSNUser = DataSourceUtil.getUserName(coreDSN)>
		<cfset var LogicalCondition = variables.wirebox.getInstance("LogicalCondition@CORE")>
		<cfset var QueryBuilder = variables.wirebox.getInstance("QueryBuilder@CORE")>
		<cfset var queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils")>
		<cfset var multipleTemplatesSql = "">
		<cfset var templateCdsPackage = "">
		<cfset var templatesPackage = "">
		<cfset var okTemplates = "">
		<cfset var failTemplates = "">
		<cfset var contextKeys = "Trainee.STUDENT_ID,Trainee.USER_ID,TraineeRegistration.TRAINING_SESSION_CD">
		<cfset var templatesData = StructNew()>
		<cfset var templateData = StructNew()>
		<cfset var templateCondition = "">
		<cfset var userID = "">
		<cfset var instanceResList = "">
		<cfif variables.sessionStorage.exists("LoggedInUser")>
			<cfset userID = variables.sessionStorage.getVar("LoggedInUser").USER_ID>
			<cfset entityRestrictions = application.wirebox.getInstance(name="EntityRestrictions@CORE")>
			<cfset instanceResList = entityRestrictions.getUserRestrictions(restrictionType = 'Letter Instances', userId = userID)>
		</cfif>
		<cfquery name="groupsQry" datasource="#variables.dbFactory.getDatasource()#">
			select a.*, c.doc_type_cd, d.condition_text, nvl(e.auto_generate, 'N') as auto_generate
			from doc_letter_template a,
				 doc_letter_map b,
				 doc_type c,
				 #coreDSNUser#.logical_condition d,
				 doc_settings e
			where
				a.status = 'ACTIVE' and
				a.doc_letter_template_cd = b.doc_letter_template_cd and
				a.instance_id = e.instance_id (+) and
				b.doc_type_cd = c.doc_type_cd and
				c.restriction_condition_id = d.condition_id (+)
				<cfif StructKeyExists(arguments.data,"doc_template_cd") and Trim(arguments.data.doc_template_cd) neq "">
					and a.doc_letter_template_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.data.doc_template_cd#">)
				</cfif>
				<cfif StructKeyExists(arguments.data,"doc_template_code") and Trim(arguments.data.doc_template_code) neq "">
					and a.doc_letter_template_code in (<cfqueryparam cfsqltype="cf_sql_varchar" list="true" value="#arguments.data.doc_template_code#">)
				</cfif>
				<cfif StructKeyExists(arguments.data,"instance_id") and Trim(arguments.data.instance_id) neq "">
					and a.instance_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.data.instance_id#">)
				</cfif>
				<cfif instanceResList neq "" and instanceResList neq "*">
					AND A.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
				</cfif>
			order by
				a.doc_letter_template_cd
		</cfquery>
		<cfset var processID = CreateUUID()>
		<cfset var updateDataQry = "">
		<cfset var currentTemplateCD = groupsQry.doc_letter_template_cd>
		<cfloop query="groupsQry">
			<cfif uCase(groupsQry.AUTO_GENERATE) EQ "Y" OR arguments.onDemand>
				<cfif currentTemplateCD neq groupsQry.doc_letter_template_cd>
					<cfset templatesData[currentTemplateCD] = templateData>
					<cfset var templateData = StructNew()>
					<cfset currentTemplateCD = groupsQry.doc_letter_template_cd>
					<cfset var templateCondition = "">
				</cfif>
				<cfsavecontent variable="templateCondition">
					<cfoutput>#templateCondition#<cfif templateCondition neq ""> OR </cfif><cfif Trim(groupsQry.condition_text) neq "">(#groupsQry.condition_text#)<cfelse>(Trainee.STUDENT_ID IS NOT NULL)</cfif></cfoutput>
				</cfsavecontent>
				<cfset templateData["template_name"] = groupsQry.letter_template_name>
				<cfset templateData["instance_id"] = groupsQry.instance_id>
				<cfset templateData["template_condition"] = templateCondition>
				<cfset templateData["auto_generate"] = groupsQry.auto_generate>
			</cfif>
		</cfloop>
		<cfif uCase(templateData["auto_generate"]) EQ "Y" OR arguments.onDemand>
			<cfset templatesData[currentTemplateCD] = templateData>
		</cfif>
		<cfset var templateCds = StructKeyList(templatesData)>
		<cfquery name="updateDataQry" datasource="#variables.dbFactory.getDatasource()#">
			update doc_snapshot_base
			set
				process_id = '#processID#',
				is_processing = 'Y'
			where
				process_id is null and
				doc_published_dt is null
				<cfif Trim(templateCds) neq "">
					and doc_letter_template_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#templateCds#">)
				</cfif>
				<cfif StructKeyExists(arguments.data, 'user_id') and Trim(arguments.data.user_id) neq "">
					and #queryUtils.buildInSql(arguments.data.user_id, 'USER_ID')#
				</cfif>
				<cfif StructKeyExists(arguments.data, 'student_id') and Trim(arguments.data.student_id) neq "">
					and user_id in (select user_id from student_user where #queryUtils.buildInSql(arguments.data.student_id, 'STUDENT_ID')#)
				</cfif>
				<cfif StructKeyExists(arguments.data, 'training_session_cd') and Trim(arguments.data.training_session_cd) neq "">
					and doc_snapshot_cd in (
						select doc_snapshot_cd
						from doc_snapshot_metadata
						where
							field_value in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.data.training_session_cd#">) and
							field_name = 'TRAINING_SESSION_CD'
					)
				</cfif>
				<cfif instanceResList neq "" and instanceResList neq "*">
					AND INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
				</cfif>
		</cfquery>
		<cfset var templateCD = "">
		<cfset var templateIdx = 0>
		<cfloop list="#templateCds#" index="templateCD">
			<cfset templateIdx = templateIdx + 1>
			<!--- build metadata context sql --->
			<cfset var contextSql = "">
			<cfset var metadataSql = "">
			<cfset var documentDescSql = "'Session: ' || TRAINING_SESSION">
			<cfloop from="1" to="#ListLen(contextKeys)#" index="j">
				<cfset contextSql &= " '" & ListGetAt(ListGetAt(contextKeys, j),2,'.') & "' || " & ListGetAt(ListGetAt(contextKeys, j),2,'.') &  " ||">
				<cfset metadataSql &= " '" & ListGetAt(ListGetAt(contextKeys, j),2,'.') & "' || '=' || " & ListGetAt(ListGetAt(contextKeys, j),2,'.') &  " || ';' ||">
			</cfloop>
			<cfif contextSql NEQ "">
				<cfset contextSql = Left(contextSql, Len(contextSql) - 3)>
			</cfif>
			<cfif metadataSql NEQ "">
				<cfset metadataSql = Left(metadataSql, Len(metadataSql) - 10)>
			</cfif>
			<cfset templateCdsPackage = ListAppend(templateCdsPackage,templateCD)>
			<cfset templatesPackage = ListAppend(templatesPackage,templatesData[templateCD]['template_name'])>
			<cfset templateCondition = "(#templatesData[templateCD]['template_condition']#) AND Trainee.STUDENT_ID IS NOT NULL AND TraineeRegistration.TRAINING_SESSION_CD IS NOT NULL" />
			<cfset var sql = LogicalCondition.getSQL(templateCondition, "TraineeRegistration.TRAINING_SESSION,#contextKeys#", false) />
			<cfsavecontent variable="multipleTemplatesSql">
				<cfoutput>
					#multipleTemplatesSql#
					select x.*,
						   #templatesData[templateCD]['instance_id']# as instance_id,
						   #templateCD# as doc_letter_template_cd,
						   #contextSql# as context_id,
						   #metadataSql# as metadata,
						   #documentDescSql# as document_desc
					from (#PreserveSingleQuotes(sql)#) x
					<cfif not(templateIdx mod templatesPackageSize eq 0 or templateIdx eq ListLen(templateCds))>
					union
					</cfif>
				</cfoutput>
			</cfsavecontent>
			<cfif templateIdx mod templatesPackageSize eq 0 or templateIdx eq ListLen(templateCds)>
				<cfset var multipleTemplatesQry = QueryBuilder.getQuery(multipleTemplatesSql,arguments.data)>
				<cfset multipleTemplatesSql = "">
				<cftry>
					<cfquery name="insertQry" datasource="#variables.dbFactory.getDatasource()#">
					merge into doc_snapshot_base x
					using (
						select *
						from (#PreservesingleQuotes(multipleTemplatesQry)#)
					) y
					on (x.context_id = y.context_id and x.doc_letter_template_cd = y.doc_letter_template_cd)
					when matched then
						update set
							x.is_processing = null
						where
							x.process_id = '#processID#' and
							x.is_processing = 'Y'
					when not matched then
						insert (X.DOC_SNAPSHOT_CD,X.INSTANCE_ID,X.USER_ID,X.DOCUMENT_DESC,X.DOC_LETTER_TEMPLATE_CD, X.DOC_LETTER_TYPE_CD, X.DOC_DT, X.CONTEXT_ID,X.METADATA,X.PROCESS_ID,X.CREATION_ID,X.CREATION_DT)
						values (DOC_SNAPSHOT_BASE_SEQ.NEXTVAL,Y.INSTANCE_ID,Y.USER_ID,Y.DOCUMENT_DESC,Y.DOC_LETTER_TEMPLATE_CD,1,SYSDATE,Y.CONTEXT_ID,Y.METADATA,'#processID#','#userID#',SYSDATE)
				</cfquery>
					<cfset okTemplates = ListAppend(okTemplates,templateCdsPackage)>
					<cfset templateCdsPackage = "">
					<cfset templatesPackage = "">
					<cfcatch>
						<cfset failTemplates = ListAppend(failTemplates,templatesPackage)>
						<cfset templateCdsPackage = "">
						<cfset templatesPackage = "">
					</cfcatch>
				</cftry>
			</cfif>
		</cfloop>
		<cfif ListLen(okTemplates)>
			<cfset var delete1Qry = "">
			<cfquery name="delete1Qry" datasource="#variables.dbFactory.getDatasource()#">
				delete
				from doc_snapshot_metadata
				where
					doc_snapshot_cd in (
					select doc_snapshot_cd
					from doc_snapshot_base
					where
						process_id = '#processID#' and
						is_processing = 'Y' and
						doc_letter_template_cd in (#okTemplates#))
			</cfquery>
			<cfset var delete2Qry = "">
			<cfquery name="delete2Qry" datasource="#variables.dbFactory.getDatasource()#">
				delete
				from doc_letter_data
				where
					doc_letter_data_cd in (
					select doc_letter_data_cd
					from doc_snapshot_base
					where
						process_id = '#processID#' and
						is_processing = 'Y' and
						doc_letter_template_cd in (#okTemplates#))
			</cfquery>
			<cfset var delete3Qry = "">
			<cfquery name="delete3Qry" datasource="#variables.dbFactory.getDatasource()#">
				delete
				from doc_snapshot_base
				where
					process_id = '#processID#' and
					is_processing = 'Y' and
					doc_letter_template_cd in (#okTemplates#)
			</cfquery>
			<cfset var insertMetadata = "">
			<cfquery name="insertMetadata" datasource="#variables.dbFactory.getDatasource()#">
				merge into doc_snapshot_metadata x
				using (
				select
				  t.doc_snapshot_cd,
				  regexp_substr(trim(regexp_substr(t.metadata, '[^;]+', 1, levels.column_value)), '[^=]+', 1, 1) AS KEY,
				  regexp_substr(trim(regexp_substr(t.metadata, '[^;]+', 1, levels.column_value)), '[^=]+', 1, 2) AS VALUE
				from
				  (
				    select a.doc_snapshot_cd,a.metadata from doc_snapshot_base a, doc_snapshot_metadata b where a.doc_letter_template_cd in (#okTemplates#) and a.metadata is not null and a.doc_snapshot_cd = b.doc_snapshot_cd (+) and b.doc_snapshot_cd is null
				  ) t,
				  table(cast(multiset(select level from dual connect by level <= length (regexp_replace(t.metadata, '[^;]+'))  + 1) as sys.OdciNumberList)) levels
				) y
			    on (y.doc_snapshot_cd=x.doc_snapshot_cd)
			    when not matched then
			      insert (X.DOC_SNAPSHOT_CD,X.FIELD_NAME,X.FIELD_VALUE)
				  values (
				  	y.doc_snapshot_cd,
				  	y.key,
				  	y.value)
			</cfquery>
		</cfif>
		<cfset var getLettersQry = "">
		<cfquery name="getLettersQry" datasource="#variables.dbFactory.getDatasource()#">
			select *
			from doc_snapshot_base
			where
				process_id = '#processID#'
		</cfquery>
		<cfset var updateProcessQry = "">
		<cfquery name="updateProcessQry" datasource="#variables.dbFactory.getDatasource()#">
			update doc_snapshot_base
			set process_id = null,
				is_processing = null
			where
				process_id = '#processID#'
		</cfquery>
		<cfset var loaDSNUser = DataSourceUtil.getUserName(variables.dbFactory.getDatasource())>
		<cfstoredproc procedure="REFRESH_MATERIALIZED_VIEW.RefreshMaterializedViews" datasource="#coreDSN#">
			<cfprocparam cfsqltype="cf_sql_varchar" value="#loaDSNUser#.DOC_SNAPSHOT_DETAIL">
		</cfstoredproc>
		<cfset result.DOC_SNAPSHOT_CD = ValueList(getLettersQry.doc_snapshot_cd)>
		<cfset var lettersStruct = StructNew()>
		<cfloop query="getLettersQry">
			<cfif !StructKeyExists(lettersStruct,getLettersQry.instance_id)>
				<cfset lettersStruct[getLettersQry.instance_id] = ''>
			</cfif>
			<cfset lettersStruct[getLettersQry.instance_id] = ListAppend(lettersStruct[getLettersQry.instance_id],getLettersQry.doc_snapshot_cd)>
		</cfloop>
		<cfset var instance_id = ''>
		<cfset var docObjectFactory = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.docObjectFactory").init()>
		<cfset var filter = application.wirebox.getInstance("SearchFilter")>
		<cfset var docAdmin = docObjectFactory.create("DOC_ADMIN_COMPONENT").init(argumentCollection=arguments.data)>
		<cfloop collection="#lettersStruct#" item="instance_id" >
			<cfset var DocSettings = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocSettings").init(instance_id)>
			<cfset var docSettingsQry = DocSettings.getSettings()>
			<cfif docSettingsQry.AUTO_PUBLISH_ORIGINAL eq 'Y'>
				<cfset filter.setFILTER_ITEM('DOC_SNAPSHOT_CD', lettersStruct[instance_id])>
				<cfset docAdmin.publish(filter,true)>
			</cfif>
		</cfloop>
		<cfif ListLen(failTemplates) gt 0>
			<cfset result.STATUS = "ERROR">
			<cfset result.STATUS_DETAILS = "Failed to generate letters for the following templates: #failTemplates#">
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="generateMissingLetterPDFs" access="public" returntype="struct">
		<cfargument name="data" type="struct" required="true">
		<cfset var result = StructNew()>
		<cfset var docSnapshot = CreateObject("component","medsisOld.modules.AgreementLetters.Model.Bl.DocSnapshot").init(5)>
		<cfset var datasource = application.wirebox.getInstance(dsl="coldbox:setting:pg_datasource")>
		<cfquery name="getIncompleteLettersQry" datasource="#datasource#">
			select *
			from doc_snapshot_base
			where
				doc_file_cd is null and
				doc_submitted_dt is not null
		</cfquery>
		<cfif getIncompleteLettersQry.recordcount gt 0>
			<cfset generatedFiles = "">
			<cfset failedSnapshots = "">
			<cfloop query="getIncompleteLettersQry">
				<cftry>
					<cfset fileCD = docSnapshot.generatePDF(getIncompleteLettersQry.doc_snapshot_cd)>
					<cfset generatedFiles = ListAppend(generatedFiles,fileCD)>
					<cfcatch>
						<cfset failedSnapshots = ListAppend(failedSnapshots,getIncompleteLettersQry.doc_snapshot_cd)>
					</cfcatch>
				</cftry>
			</cfloop>
		</cfif>
		<cfif ListLen(failedSnapshots) gt 0>
			<cfset result.status = "ERROR">
			<cfset result.status_details = "<div class='msg-error'>Could not generate PDFs for the following letters: #failedSnapshots#.</div>">
		<cfelse>
			<cfset result.status = "SUCCESS">
			<cfset result.status_details = "<div class='msg-success'>#getIncompleteLettersQry.recordcount# PDF files generated.</div>">
		</cfif>
		<cfreturn result>
	</cffunction>


	<cffunction name="generateTes" access="public" returntype="struct">
		<cfargument name="data" type="struct" default="#StructNew()#">
		<cfif variables.sessionStorage.exists("LoggedInUser")>
			<cfset userID = variables.sessionStorage.getVar("LoggedInUser").USER_ID>
		<cfelse>
			<cfset userID = "1">
		</cfif>
		<cfset DocProcess = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocProcess" ).init(userID)/>
		<cfset docInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceInfoQry = docInstances.getInstanceInfo(arguments.data.instance_id)>
		<cfset  stringUtil = CreateObject("component","sis_core.model.util.stringUtils").init()>
		<cfif NOT isDefined("arguments.data.DOC_SETUP_ELIGIBLE_USER_ID")>
			<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
			<!--- Get only Ready documents---->
			<cfset arguments.data.SETUP_STATUS_ID = 2 >
			<cfset DocReportQry = DocReport.getProcessDocuments(arguments.data)>
			<cfset arguments.data.DOC_SETUP_ELIGIBLE_USER_ID = ListRemoveDuplicates(ValueList(DocReportQry.DOC_SETUP_ELIGIBLE_USER_ID))>
		</cfif>
		<cfset  splittedList =  stringUtil.listSplit(arguments.data.DOC_SETUP_ELIGIBLE_USER_ID,30) />
		<cfset var result = StructNew()>
		<cfset result.status = "SUCCESS">
		<cfif !DocProcess.isProcessActive(arguments.data.instance_id)>
			<cfset mainProcessID = DocProcess.startprocess("Generate #instanceInfoQry.instance_name# Documents","Start processing",-1,arguments.data.instance_id)/>
			<cfloop list="#splittedList#" index="idx" delimiters="/">
				<cfset dataStruct = Duplicate(arguments.data) >
				<cfset dataStruct.DOC_SETUP_ELIGIBLE_USER_ID = idx>
				<cfset dataStruct.processID = mainProcessID >
				<cftransaction action="begin">
					<cftry>
						<cfset instanceConfig = docInstances.getInstanceConfig(dataStruct.instance_id) />
						<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
						<cfset queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils")>
						<cfset dataProviderEntity = queryBuilder.getEntity(instanceConfig.DOCUMENT_DATA_PROVIDER_ENTITY) />
						<cfset configProviderEntity = queryBuilder.getEntity(instanceConfig.CONFIG_DATA_PROVIDER_ENTITY) />
						<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
						<cfset eligibleUsersQry = DocReport.geteligibleUsersQry(dataStruct)>
						<cfset dataStruct.DOC_SETUP_ID = ListRemoveDuplicates(ValueList(eligibleUsersQry.DOC_SETUP_ID))/>
						<cfset dataStruct.USER_ID = ListRemoveDuplicates(ValueList(eligibleUsersQry.USER_ID))>
						<cfset dataStruct.DOC_SETUP_ELIGIBLE_USER_ID = ListRemoveDuplicates(ValueList(eligibleUsersQry.DOC_SETUP_ELIGIBLE_USER_ID))>
						<cfset entityRegistry = application.wirebox.getInstance("EntityRegistry@CORE") />
						<cfset entity = entityRegistry.getEntity('#instanceConfig.CONFIG_DATA_PROVIDER_ENTITY#') >
						<cfset dataEntityInstance = application.wirebox.getInstance(name=entity.ENTITY_PATH).init() >
						<cfset dataEntityInstance.preProcessSql(dataStruct) />
						<cfset docProcessId = DocProcess.startprocess("Generate Snapshots","Start generating",mainProcessID)>
						<cfset  stringUtil = CreateObject("component","sis_core.model.util.stringUtils").init()>
						<cfset  setupList =  stringUtil.listSplit(dataStruct.doc_setup_id) />
						<cfset  userList =  stringUtil.listSplit(dataStruct.user_id) />
						<cfquery name="insertSnapshot" datasource="#variables.dbDocumentFactory.getDatasource()#">
								MERGE INTO doc_snapshot_base d USING
								( SELECT
								    b.*,
								    a.instance_id,
								    a.DOC_LETTER_TEMPLATE_CD
								FROM
								    doc_setup a,
								    doc_setup_eligible_users b,
								    (#configProviderEntity.getSQL('UNPUBLISHEDEVALSQRY')#) c
								WHERE
								    a.doc_setup_id = b.doc_setup_id
								    and b.user_id = c.user_id
								    and b.doc_setup_id = c.doc_setup_id
								    <cfif dataStruct.doc_setup_id neq "*">
										AND	(1 = 0
										<cfloop list="#setupList#" index="idx" delimiters="/">
										or a.doc_setup_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#idx#">)
										</cfloop>)
									</cfif>
									 <cfif dataStruct.user_id neq "*">
										AND	(1 = 0
										<cfloop list="#userList#" index="idx" delimiters="/">
										or b.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#idx#">)
										</cfloop>)
									</cfif>
								)
								s ON (
								    d.doc_setup_id = s.doc_setup_id AND d.user_id = s.user_id and d.process_id ='#dataStruct.processID#'
								) WHEN NOT MATCHED THEN
								INSERT (
										d.doc_snapshot_cd,
										d.doc_setup_id,
										d.user_id,
										d.instance_id,
										d.process_id,
										d.creation_dt,
										d.creation_id,
										d.DOC_LETTER_TEMPLATE_CD,
										d.IS_CURRENT
										)
								VALUES (
									doc_snapshot_base_seq.NEXTVAL,
									s.doc_setup_id,
									s.user_id,
									s.instance_id,
									'#dataStruct.processID#',
									sysdate,
									'#userID#',
									s.DOC_LETTER_TEMPLATE_CD,
									'Y')
							</cfquery>
						<cfset DocProcess.endProcess(docProcessId,"End generating") />
						<cfloop list="#dataProviderEntity.GETPROPERTIESASLIST()#" index="property">
							<cfset docProcessId = DocProcess.startprocess("Generate #property#","Start generating",mainProcessID)>
							<cfset qrySql = "Select * from (" & dataProviderEntity.getSQL('#property#') & ") where 1=0">
							<cfset Qry = queryBuilder.runQuery(sql=qrySql,datasource=dataProviderEntity.GETENTITYDATASOURCE(),params=dataStruct)>
							<cfset columnList = ArrayToList(Qry.getColumnList())>
							<cfset listPos = ListFind(columnList,'DOC_SNAPSHOT_CD',",")>
							<cfif listPos>
								<cfset columnList = ListDeleteAt( columnList, listPos ,",") />
							</cfif>
							<cfset sql ="Select '[' "/>
							<cfset sql= sql & " ||dbms_xmlgen.convert(RTRIM(XMLAGG(XMLELEMENT(E, '{">
							<cfset loopCount = 0>
							<cfloop index="i" list="#columnList#">
								<cfset  sql = sql & '"#i#":"''||#i#||''"'/>
								<cfset loopCount=loopCount+1>
								<cfif (loopCount lt ListLen(columnList))>
									<cfset  sql = sql & ',' />
								</cfif>
							</cfloop>
							<cfset sql = sql & "}',',').EXTRACT('//text()') ORDER BY order_nr asc,doc_snapshot_cd DESC NULLS LAST).getclobval(),','),1) ||']' AS VALUE,'#property#' as key, doc_snapshot_cd from ("/>
							<cfset newSql = sql & 'SELECT ROWNUM AS ORDER_NR ,Z.* FROM ('& dataProviderEntity.getSQL('#property#') & ') Z)	group by doc_snapshot_cd'>
							<cfset resultsql= queryBuilder.getQuery(sql=newSql,datasource = dataProviderEntity.GETENTITYDATASOURCE(),params=dataStruct)>
							<cfquery name="insertMetadata" datasource="#variables.dbDocumentFactory.getDatasource()#">
									merge into DOC_SNAPSHOT_DATA x
									using (
									Select
									g.* from (
									select
									  (select doc_snapshot_cd from doc_snapshot_base where user_id||'_'||doc_setup_id||'_'||process_id = j.doc_snapshot_cd) as doc_snapshot_cd,
									  j.key AS KEY,
									  j.value AS VALUE,
									  j.value as TEMP_VALUE
									from
										(#preserveSingleQuotes(resultsql)#) j) g where g.doc_snapshot_cd is not null
									) y
								    on (y.doc_snapshot_cd=x.doc_snapshot_cd and UPPER(y.KEY)=UPPER(x.KEY))
								    when not matched then
								      insert (
								      		X.DOC_SNAPSHOT_DATA_CD,
								      		X.DOC_SNAPSHOT_CD,
								      		X.KEY,
								      		X.VALUE,
								      		x.temp_value,
								      		x.creation_dt)
									  values (
									    DOC_SNAPSHOT_DATA_SEQ.nextval,
									  	y.doc_snapshot_cd,
									  	y.key,
									  	y.value,
									  	y.temp_value,
									  	SYSDATE)
								  	WHEN MATCHED THEN UPDATE
								  		SET x.temp_value = y.temp_value,
								  			x.modification_dt = sysdate
								</cfquery>
							<cfset DocProcess.endProcess(docProcessId,"End generating") />
						</cfloop>
						<cfset docProcessId = DocProcess.startprocess("Populate Used Evaluations","Start",mainProcessID)>
						<cfquery name="mergeEvaluations" datasource="#variables.dbDocumentFactory.getDatasource()#">
							merge into DOC_SNAPSHOT_EVALUATIONS d USING(
							SELECT REGEXP_SUBSTR( EVALUATION_LIST, '[^,]+', 1, EVALUATIONS.COLUMN_VALUE ) AS EVALUATION_ID,DOC_SNAPSHOT_CD,USER_ID
							FROM
							  (SELECT A.DOC_SNAPSHOT_CD,
							    A.USER_ID,
							    (SELECT LISTAGG( EVALUATION_ID, ',' ) WITHIN GROUP(
							    ORDER BY A.DOC_SNAPSHOT_CD) AS EVALUATION_LIST
							    FROM TABLE ( PLJSON_TABLE.JSON_TABLE( D.VALUE, PLJSON_VARRAY('[*].EVALUATION_ID'), PLJSON_VARRAY('EVALUATION_ID'), TABLE_MODE => 'NESTED' ) )
							    ) EVALUATION_LIST
							  FROM DOC_SNAPSHOT_BASE A,
							    DOC_SNAPSHOT_DATA D
							  WHERE A.DOC_SNAPSHOT_CD = D.DOC_SNAPSHOT_CD
							  AND D.KEY               = 'EVALUATION_LIST'
							  AND A.process_id = '#dataStruct.processID#'
							  ),
							  TABLE ( CAST(MULTISET
							  (SELECT LEVEL
							  FROM DUAL
							    CONNECT BY LEVEL <= LENGTH(REGEXP_REPLACE( EVALUATION_LIST, '[^,]+' ) ) + 1
							  ) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
							  ) S on(D.DOC_SNAPSHOT_CD=S.DOC_SNAPSHOT_CD AND D.EVALUATION_ID=S.EVALUATION_ID)
							  WHEN NOT MATCHED THEN
										INSERT (D.DOC_SNAPSHOT_CD,
												D.USER_ID,
												D.EVALUATION_ID
												)
										VALUES (s.DOC_SNAPSHOT_CD,
												S.USER_ID,
												S.EVALUATION_ID
									)
				  		</cfquery>
						<cfset dataEntityInstance.postProcessSql(dataStruct) />
						<cfset DocProcess.endProcess(docProcessId,'End')>
						<cftransaction action="commit" />
						<cfcatch>
							<cftransaction action="rollback" />
							<cfset var result = {STATUS = "ERROR"}>
						</cfcatch>
					</cftry>
				</cftransaction>
			</cfloop>
			<cfset inform = variables.interceptor.inform("DOCUMENT_BUILDER:SETUP:AFTER:GENERATE",{INSTANCE_CODE = instanceInfoQry.INSTANCE_CODE}) />
			<cfset DocProcess.endProcess(mainProcessID,'Generate Documents successfully completed #result.status#')>
		</cfif>

		<cfreturn result>
	</cffunction>


	<cffunction name="calculateEligibleUsers" access="public" returntype="struct">
		<cfargument name="data" type="struct" default="#StructNew()#">
		<cfset doaInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceConfig = doaInstances.getInstanceConfig(arguments.data.instance_id) />
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset dataProviderEntity = queryBuilder.getEntity(instanceConfig.CONFIG_DATA_PROVIDER_ENTITY) />
		<cfset eligibleUsersSql = queryBuilder.getQuery(sql=dataProviderEntity.getSQL('ELIGIBLEUSERSQRY'),datasource=dataProviderEntity.GETENTITYDATASOURCE(),params=arguments.data)>
		<cfset var result = StructNew()>
		<cftransaction action="begin">
			<cftry>
				<!---merge eligible users--->
				<cfquery name="insertEligibleUsers" datasource="#variables.dbDocumentFactory.getDatasource()#">
					MERGE INTO DOC_SETUP_ELIGIBLE_USERS D USING (
						#preserveSingleQuotes(eligibleUsersSql)#
					) S ON (D.DOC_SETUP_ID = S.DOC_SETUP_ID AND D.USER_ID = S.USER_ID)
					WHEN NOT MATCHED THEN
						INSERT (D.DOC_SETUP_ELIGIBLE_USER_ID,
								D.USER_ID,
								D.DOC_SETUP_ID
								)
						VALUES (DOC_SETUP_ELIGIBLE_USER_SEQ.NEXTVAL,
								S.USER_ID,
								S.DOC_SETUP_ID
					)
				</cfquery>
				<cfset var result = {STATUS = "SUCCESS"}>
				<cftransaction action="commit" />
				<cfcatch>
					<cftransaction action="rollback" />
					<cfset var result = {STATUS = "ERROR"}>
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfreturn result>
	</cffunction>


	<cffunction name="deleteDocuments" access="public" returntype="struct">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="false" />
		<cfset var DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
		<cfset var DocReportQry = DocReport.getDocumentsForPrint(arguments.filter)>
		<cfset docInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceInfoQry = docInstances.getInstanceInfo(arguments.filter.getFILTER_DATA_STRUCT().DOC_LETTER_INSTANCE_CD)>
		<cftransaction action="begin">
			<cftry>
				<cfloop query="DocReportQry">
					<cfquery name="deleteQry1" datasource="#variables.dbDocumentFactory.getDatasource()#">
						delete from doc_snapshot_evaluations where DOC_SNAPSHOT_CD in (#DocReportQry.DOC_SNAPSHOT_CD#)
					</cfquery>
					<cfquery name="deleteQry2" datasource="#variables.dbDocumentFactory.getDatasource()#">
						delete from DOC_SNAPSHOT_DETAIL where DOC_SNAPSHOT_CD in (#DocReportQry.DOC_SNAPSHOT_CD#)

					</cfquery>
					<cfquery name="deleteQry3" datasource="#variables.dbDocumentFactory.getDatasource()#">
						delete from DOC_SNAPSHOT_Data where DOC_SNAPSHOT_CD in (#DocReportQry.DOC_SNAPSHOT_CD#)

					</cfquery>
					<cfquery name="deleteQry4" datasource="#variables.dbDocumentFactory.getDatasource()#">
						delete from DOC_SNAPSHOT_EVAL_PERIOD where DOC_SNAPSHOT_CD in (#DocReportQry.DOC_SNAPSHOT_CD#)

					</cfquery>
					<cfquery name="deleteQry5" datasource="#variables.dbDocumentFactory.getDatasource()#">
						delete from DOC_SNAPSHOT_CHANGES where DOC_SNAPSHOT_CD in (#DocReportQry.DOC_SNAPSHOT_CD#)
					</cfquery>
					<cfquery name="deleteQry6" datasource="#variables.dbDocumentFactory.getDatasource()#">
						delete from DOC_SNAPSHOT_BASE where DOC_SNAPSHOT_CD in (#DocReportQry.DOC_SNAPSHOT_CD#)
					</cfquery>
				</cfloop>
				<cfset var result = {STATUS = "SUCCESS"}>
				<cftransaction action="commit" />
				<cfcatch>
					<cftransaction action="rollback" />
					<cfset var result = {STATUS = "ERROR"}>
				</cfcatch>
			</cftry>
		</cftransaction>
		<cfif instanceInfoQry.INSTANCE_CODE EQ "PG_TES_REPORT">
			<cfstoredproc procedure="REFRESH_MATERIALIZED_VIEW.RefreshMaterializedViews" datasource="#variables.dbDocumentFactory.getDatasource()#">
				<cfprocparam cfsqltype="cf_sql_varchar" value="USER_UNPUBLISHED_EVALUATIONS" />
			</cfstoredproc>
		</cfif>
		<cfif instanceInfoQry.INSTANCE_CODE EQ "UG_TES_REPORT">
			<cfstoredproc procedure="REFRESH_MATERIALIZED_VIEW.RefreshMaterializedViews" datasource="#variables.dbDocumentFactory.getDatasource()#">
				<cfprocparam cfsqltype="cf_sql_varchar" value="UG_USER_UNPUBLISHED_EVALS" />
			</cfstoredproc>
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction name="publish" access="public" output="false" returntype="struct">
       <cfargument name="data" type="struct" default="#StructNew()#">

		<cfset var success = true />
		<cfset var docSnapshotsQry = "">
		<cfif variables.sessionStorage.exists("LoggedInUser")>
			<cfset userID = variables.sessionStorage.getVar("LoggedInUser").USER_ID>
		<cfelse>
			<cfset userID = "1">
		</cfif>

		<cfif NOT isDefined("arguments.data.DOC_SNAPSHOT_CD")>
			<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
			<cfset DocReportQry = DocReport.getProcessDocuments(arguments.data)>
			<cfset docSnapshotCdList = ListRemoveDuplicates(ValueList(DocReportQry.DOC_SNAPSHOT_CD))>
		<cfelse>
			<cfset var docSnapshotCdList = arguments.data.DOC_SNAPSHOT_CD>
		</cfif>
		<cfset docSnapshotCdList = ListAppend(docSnapshotCdList,-1) >
		<cfset docInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceInfoQry = docInstances.getInstanceInfo(arguments.data.instance_id)>

		<cfset var updateQry = "">

		<cfset var queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils")>
		<cfquery name="updateQry" datasource="#variables.dbDocumentFactory.getDatasource()#">
			update
				doc_snapshot_base
			set
				is_current = 'Y',
				doc_published_dt = sysdate,
				doc_published_user_id = '#userID#',
				doc_confirmed_dt = sysdate,
				doc_confirmed_user_id = '#userID#'
			where
				doc_published_dt is null and
				#queryUtils.buildInSql(docSnapshotCdList, "DOC_SNAPSHOT_CD")#
		</cfquery>

		<cfif success>
			<cfset variables.MsgBox.success("All selected documents successfully published.")>
			<cfset variables.Interceptor.inform("DOCUMENT_BUILDER:DOCUMENT:AFTER:PUBLISH",{INSTANCE_CODE = instanceInfoQry.INSTANCE_CODE})>
		<cfelse>
			<cfset variables.MsgBox.error("Publishing selected documents failed.")>
		</cfif>

		<cfset var result = StructNew()>
		<cfset result.status = "SUCCESS">
		<cfreturn result>

	</cffunction>

	<cffunction name="mapSetupQuestions" access="public" returntype="struct">
		<cfargument name="data" type="struct" default="#StructNew()#">

		<cfset datasource = application.cbcontroller.getsetting("cbx_datasource")>
		<cfset fbx_datasource = application.cbcontroller.getsetting("fbx_datasource")>
		<cfset DataSourceUtil = application.wirebox.getInstance(name="sis_core.model.util.datasource") />
		<cfset fbxDSN = DataSourceUtil.getUserName(fbx_datasource) />

		<cfset formList = "MD Program Teacher Evaluation - Clinical,MD Program Teacher Evaluation - Presenter,MD Program Teacher Evaluation - Small">

		<cfloop list="#formList#" index="idx" delimiters = ",">
			    <!--- Step1 Identify the DOC_SETUP_ID,eval_form_cd and TES_CONFIG_ID for course --->
				<cfquery name="step1Qry" datasource="#datasource#">
					WITH PARENT_COURSES AS (
					SELECT
					    C.COURSE_CD,
					    C.COURSE_NAME,
					    C.COURSE_NAME_SHORT,
					    P.COURSE_CD AS PARENT_COURSE_CD,
					    P.COURSE_NAME AS PARENT_COURSE_NAME,
					    P.COURSE_NAME_SHORT AS PARENT_COURSE_NAME_SHORT
					FROM
					    #fbxDSN#.course c
					    left join #fbxDSN#.COURSE_HIERARCHY H on(c.course_cd = h.course_cd)
					    left join #fbxDSN#.COURSE P on(h.parent_course_cd = p.course_cd)
					    )
					SELECT
					    A.DOC_SETUP_ID,
					    A.TES_CONFIG_ID,
					    B.COURSE_CD,
					    C.PARENT_COURSE_NAME,
					    C.COURSE_NAME AS CHILD_COURSES,
					    C.COURSE_CD AS CHILD_COURSE_CD,
					    A.EVAL_FORM_CD,
					    EF.EVAL_FORM_DESC
					FROM
					    TES_CONFIG A,
					    UG_TES_CONFIG_COURSES B,
					    PARENT_COURSES C,
			            #fbxDSN#.EVAL_FORM EF
					WHERE
					    A.INSTANCE_ID = #arguments.data.INSTANCE_ID#
					    <cfif isDefined("arguments.data.DOC_SETUP_ID") and arguments.data.DOC_SETUP_ID NEQ "">
					    	AND A.DOC_SETUP_ID = #arguments.data.DOC_SETUP_ID#
					    </cfif>
					    AND B.TES_CONFIG_ID=A.TES_CONFIG_ID
					    AND B.COURSE_CD=C.PARENT_COURSE_CD
					    AND A.EVAL_FORM_CD = EF.EVAL_FORM_CD
					    AND EF.EVAL_FORM_DESC LIKE '%#idx#%'
				</cfquery>

				<cfloop query="step1Qry">
						<cfquery name="step2Qry" datasource="#fbx_datasource#">
							SELECT
								STRING_AGG(EF.EVAL_FORM_CD) AS EVAL_FORM_CD,
								C.COURSE_CD,
								C.COURSE_NAME
							FROM
								EVAL_FORM EF,
								COURSE C
							WHERE
									C.COURSE_CD=EF.COURSE_CD
								AND C.COURSE_CD IN (#step1Qry.child_course_cd#)
								AND EF.EVAL_FORM_DESC LIKE '%#idx#%'
							GROUP BY C.COURSE_CD,
									 C.COURSE_NAME
						</cfquery>

						<cfquery name="step3Qry" datasource="#datasource#">
							MERGE INTO tes_config_questions d USING (
								SELECT
								    #step1Qry.TES_CONFIG_ID# AS TES_CONFIG_ID, --from step 1
								    #step1Qry.DOC_SETUP_ID# AS DOC_SETUP_ID, --from step 1
								    A.QUESTION_CD AS QUESTION_ID,
								    A.QUESTION_CD AS TES_CONFIG_QUESTION_ID
								FROM
								    #fbxDSN#.EVAL_QUESTION A
								WHERE
							        EVAL_FORM_CD IN (
							           #step1Qry.EVAL_FORM_CD#
							        )
							    AND
							        QUESTION_TYPE_CD IN (1,8)
								)
								S ON (
							   		D.TES_CONFIG_ID = S.TES_CONFIG_ID
									AND D.QUESTION_ID = S.QUESTION_ID)
								WHEN NOT MATCHED
									THEN INSERT ( D.DOC_SETUP_ID,D.TES_CONFIG_ID,D.QUESTION_ID,D.TES_CONFIG_QUESTION_ID)
									VALUES ( S.DOC_SETUP_ID,S.TES_CONFIG_ID,S.QUESTION_ID,S.TES_CONFIG_QUESTION_ID)
						</cfquery>


						<cfquery name="step4Qry" datasource="#datasource#">
							MERGE INTO TES_CONFIG_QUESTION_MAPPINGS D USING
							(
								SELECT
								    #step1Qry.TES_CONFIG_ID# AS TES_CONFIG_ID, --from step 1
								    #step1Qry.DOC_SETUP_ID# AS DOCUMENT_SETUP_ID, --from step 1
								    A.QUESTION_CD AS QUESTION_ID,
								    A.QUESTION_DESC,
								    A.EVAL_FORM_CD,
								    (
								        SELECT
								            QUESTION_CD
								        FROM
								            #fbxDSN#.EVAL_QUESTION
								        WHERE
								                QUESTION_DESC_SHORT = A.QUESTION_DESC_SHORT
								            AND
								                EVAL_FORM_CD = #step1Qry.EVAL_FORM_CD#
								    ) AS TES_CONFIG_QUESTION_ID
								FROM
								    #fbxDSN#.EVAL_QUESTION A
								WHERE
								        EVAL_FORM_CD IN (#step2Qry.eval_form_cd#)
								    AND
								        QUESTION_TYPE_CD IN(1,8)
						     )
							S ON (
							    D.TES_CONFIG_ID = S.TES_CONFIG_ID
							AND
							    D.QUESTION_ID = S.QUESTION_ID
							) WHEN NOT MATCHED THEN INSERT ( D.DOCUMENT_SETUP_ID,D.TES_CONFIG_ID,D.QUESTION_ID,D.EVAL_FORM_CD,D.TES_CONFIG_QUESTION_ID)
							VALUES (S.DOCUMENT_SETUP_ID,S.TES_CONFIG_ID,S.QUESTION_ID,S.EVAL_FORM_CD,S.TES_CONFIG_QUESTION_ID)
						</cfquery>
				</cfloop>


		</cfloop>

		<cfset var result = StructNew()>
		<cfreturn result>
	</cffunction>

</cfcomponent>
