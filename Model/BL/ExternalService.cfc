<cfcomponent displayname="ExternalService" output="false" extends="ServiceBase">

	<cffunction name="init" access="public" output="false" returntype="ExternalService">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>

	<cffunction name="getTrainingSessions" access="public" output="false" returntype="query">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfset var TrainingSessionQry = Training.getTrainingSessions() />
		<cfreturn TrainingSessionQry />
	</cffunction>

	<cffunction name="getStdTrainingSessions" access="public" output="false" returntype="query">
		<cfargument name="studentID" type="numeric" required="true">

		<cfset var StudentHelper = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfset var TrainingSessionQry = StudentHelper.getStdTrainingSessions(arguments.studentID) />
		<cfreturn TrainingSessionQry />
	</cffunction>

	<cffunction name="getTrainingLevels" access="public" output="false" returntype="query">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfset var TrainingLevelsQry = Training.getTrainingLevels() />
		<cfreturn TrainingLevelsQry />
	</cffunction>

	<cffunction name="getTrainingSessionByCD" access="public" output="false" returntype="query">
		<cfargument name="trainingSessionCd" type="numeric" required="true">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfset var TrainingSessionQry = Training.getTrainingSessionAsQuery(arguments.trainingSessionCd) />
		<cfreturn TrainingSessionQry />
	</cffunction>

	<cffunction name="getCurrentTrainingSession" access="public" output="false" returntype="query">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfset var TrainingSessionQry = Training.getCurrentTrainingSession() />
		<cfreturn TrainingSessionQry />
	</cffunction>

	<cffunction name="getDefaultTrainingSession" access="public" output="false" returntype="query">
		<cfset var StudentHelper = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfset var TrainingSessionQry = StudentHelper.getDefaultTrainingSession() />
		<cfreturn TrainingSessionQry />
	</cffunction>

	<cffunction name="getTrainingSessionByStartYear" access="public" output="false" returntype="query">
		<cfargument name="startYear" type="date" required="true">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfset var TrainingSessionQry = Training.getTrainingSessionByStartYear(arguments.startYear) />
		<cfreturn TrainingSessionQry />
	</cffunction>

	<cffunction name="checkRecordsInSession" access="public" output="false" returntype="boolean">
		<cfargument name="trainingSessionCD" type="numeric" required="true">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfreturn Training.checkRecordsInSession(arguments.trainingSessionCD) />
	</cffunction>

	<cffunction name="getStudentByID" access="public" output="false" returntype="query">
		<cfargument name="studentID" type="numeric" required="true">
		<cfset var Student = variables.wirebox.getInstance("Student@TraineeRegistration") />
		<cfreturn Student.getByID(arguments.studentID) />
	</cffunction>

	<cffunction name="getStudentPersonalInfo" access="public" output="false" returntype="query">
		<cfargument name="studentID" type="numeric" required="true">
		<cfset var Student = variables.wirebox.getInstance("Student@TraineeRegistration") />
		<cfreturn Student.getStdPersonalInfo(arguments.studentID) />
	</cffunction>

	<cffunction name="getAllStudents4Auto" access="public" output="false" returntype="query">
		<cfargument name="stdName" type="string" required="true">
		<cfset var Student = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfreturn Student.getAllStudents4Auto(arguments.stdName) />
	</cffunction>

	<cffunction name="getStudentTrainingInfo" access="public" output="false" returntype="query">
		<cfargument name="studentID" type="numeric" required="true">
		<cfargument name="trainingSessionCD" type="numeric" required="true">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfreturn Training.getStudentTrainingInfo(arguments.studentID, arguments.trainingSessionCD) />
	</cffunction>

	<cffunction name="getStudentCPSOInfo" access="public" output="false" returntype="query">
		<cfargument name="studentID" type="numeric" required="true">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfreturn Training.getStudentCPSOInfo(arguments.studentID) />
	</cffunction>

	<cffunction name="getRequirementDetail" access="public" output="false" returntype="Query">
		<cfargument name="studentID" type="Numeric" required="true" />
		<cfargument name="trSessionCD" type="Numeric" required="true" />

		<cfset var Student = variables.wirebox.getInstance("Student@TraineeRegistration") />
		<cfreturn Student.getRequirementDetail(arguments.studentID, arguments.trSessionCD) />
	</cffunction>

	<cffunction name="getStdMedicalDegree" access="public" output="false" returntype="query">
		<cfargument name="studentID" type="numeric" required="true">
		<cfset var Degree = variables.wirebox.getInstance("Degree@TraineeRegistration") />
		<cfreturn Degree.getStdMedicalDegree(arguments.studentID) />
	</cffunction>

	<cffunction name="getStdComputerAccess" access="public" output="false" returntype="query">
		<cfargument name="studentID" type="numeric" required="true">

        <cfset var StdComputerAccess = variables.wirebox.getInstance("StdComputerAccess@TraineeRegistration").init() />
        <cfset var computerAccessQry = StdComputerAccess.getComputerAccessByStdId(arguments.studentID) />

        <cfreturn computerAccessQry />
	</cffunction>

	<cffunction name="getStdRegFee" access="public" output="false" returntype="numeric">
		<cfargument name="studentID" type="numeric" required="true">
		<cfargument name="trainingSessionCD" type="numeric" required="true">

		<cfset var StudentHelper = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfreturn StudentHelper.getStdRegFee(arguments.studentID, arguments.trainingSessionCD) />
	</cffunction>

	<cffunction name="getDepartments" access="public" returntype="query" output="false">
		<cfset var obj = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfset var qry = obj.getDepartments("",false) />
		<cfreturn qry />
	</cffunction>

	<cffunction name="getTraineeTypes" access="public" returntype="query" output="false">
		<cfset var obj = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfset var qry = obj.getTraineeTypes() />
		<cfreturn qry />
	</cffunction>

	<cffunction name="getTraineeStatus" access="public" returntype="query" output="false">
		<cfset var obj = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfset var qry = obj.getTraineeStatus() />
		<cfreturn qry />
	</cffunction>

	<cffunction name="getProgramsByDepartment" access="public" returntype="query" output="false">
		<cfargument name="departmentCd" type="numeric" required="true">
		<cfset var obj = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfset var qry = obj.getProgramByDepartment(arguments.departmentCd, false) />
		<cfreturn qry />
	</cffunction>

	<cffunction name="getCPSOType" access="public" output="false" returntype="query">
		<cfset var obj = variables.wirebox.getInstance("StudentHelper@TraineeRegistration") />
		<cfset var qry= obj.getCPSOTypes() />
		<cfreturn qry>
	</cffunction>

	<cffunction name="getForLoaMappingValidation" access="public" output="false" returntype="query">
		<cfargument name="ruleStruct" type="struct" required="true">
		<cfargument name="filterData" type="sis_core.model.SearchFilter" required="true">
		<cfset var Training = variables.wirebox.getInstance("Training@TraineeRegistration") />
		<cfset var TrainingQry = Training.getForLoaMappingValidation(arguments.ruleStruct, arguments.filterData) />
		<cfreturn TrainingQry />
	</cffunction>

</cfcomponent>