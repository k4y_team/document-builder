<cfcomponent name="Restrictions" displayName="Restrictions" output="false">
	<cfproperty name="DATASOURCE" inject="coldbox:setting:datasource" scope="variables">

	<cfscript>
		public Restrictions function init( ) {
			return this;
		}


		public string function  getProgramsSQL (any userId = ""){
			var Sql = "";
			var processedSql = "";

			savecontent variable="processedSql" {
					WriteOutput("SELECT
	                				Y.USER_ID,
	                				Y.ROLE_ID,
	                				NVL(LISTAGG(A.REFERENCE_ID,',') WITHIN GROUP (ORDER BY REFERENCE_ID),'*') AS ACCESS_LIST
								FROM
					                  {cbx_datasource}.UD_USER X,
					                  {cbx_datasource}.UD_USER_ROLE Y,
					                  {cbx_datasource}.UD_ROLE Z,
									  {cbx_datasource}.UD_ACCESS_NODE A ,
									  {cbx_datasource}.UD_USER_ROLE_ACCESS B
								WHERE
						              X.USER_ID = Y.USER_ID AND
						              Y.ROLE_ID = Z.ROLE_ID AND
						              Z.IS_SYSTEM = 'N' AND
						              Y.USER_ID = B.USER_ID (+) AND
						              Y.ROLE_ID = B.ROLE_ID (+) AND
									  B.ACCESS_NODE_ID = A.ACCESS_NODE_ID (+) AND
									  UPPER(A.LABEL (+)) = '#UCase("DB PROGRAM")#'
									");
					if(arguments.userId != "") {
						WriteOutput(' AND X.USER_ID = #arguments.userId# ');
					}
					WriteOutput("
             				GROUP BY
              				 	 Y.USER_ID,
              				 	 Y.ROLE_ID

					");
				}
				return processedSql;

			}
		public string function  getCoursesSQL (any userId = ""){
			var Sql = "";
			var processedSql = "";

			savecontent variable="processedSql" {
					WriteOutput("SELECT
	                				Y.USER_ID,
	                				Y.ROLE_ID,
	                				NVL(LISTAGG(A.REFERENCE_ID,',') WITHIN GROUP (ORDER BY REFERENCE_ID),'*') AS ACCESS_LIST
								FROM
					                  {cbx_datasource}.UD_USER X,
					                  {cbx_datasource}.UD_USER_ROLE Y,
					                  {cbx_datasource}.UD_ROLE Z,
									  {cbx_datasource}.UD_ACCESS_NODE A ,
									  {cbx_datasource}.UD_USER_ROLE_ACCESS B
								WHERE
						              X.USER_ID = Y.USER_ID AND
						              Y.ROLE_ID = Z.ROLE_ID AND
						              Z.IS_SYSTEM = 'N' AND
						              Y.USER_ID = B.USER_ID (+) AND
						              Y.ROLE_ID = B.ROLE_ID (+) AND
									  B.ACCESS_NODE_ID = A.ACCESS_NODE_ID (+) AND
									  UPPER(A.LABEL (+)) = '#UCase("DB COURSE")#'
									");
					if(arguments.userId != "") {
						WriteOutput(' AND X.USER_ID = #arguments.userId# ');
					}
					WriteOutput("
             				GROUP BY
              				 	 Y.USER_ID,
              				 	 Y.ROLE_ID

					");
				}
				return processedSql;

			}
	 public string function  getLocationsSQL (any userId = ""){
		var Sql = "";
		var processedSql = "";

		savecontent variable="processedSql" {
				WriteOutput("SELECT
                				Y.USER_ID,
                				Y.ROLE_ID,
                				NVL(LISTAGG(A.REFERENCE_ID,',') WITHIN GROUP (ORDER BY REFERENCE_ID),'*') AS ACCESS_LIST
							FROM
				                  {cbx_datasource}.UD_USER X,
				                  {cbx_datasource}.UD_USER_ROLE Y,
				                  {cbx_datasource}.UD_ROLE Z,
								  {cbx_datasource}.UD_ACCESS_NODE A ,
								  {cbx_datasource}.UD_USER_ROLE_ACCESS B
							WHERE
					              X.USER_ID = Y.USER_ID AND
					              Y.ROLE_ID = Z.ROLE_ID AND
					              Z.IS_SYSTEM = 'N' AND
					              Y.USER_ID = B.USER_ID (+) AND
					              Y.ROLE_ID = B.ROLE_ID (+) AND
								  B.ACCESS_NODE_ID = A.ACCESS_NODE_ID (+) AND
								  UPPER(A.LABEL (+)) = '#UCase("DB LOCATION")#'
								");
				if(arguments.userId != "") {
					WriteOutput(' AND X.USER_ID = #arguments.userId# ');
				}
				WriteOutput("
            				GROUP BY
             				 	 Y.USER_ID,
             				 	 Y.ROLE_ID

				");
			}
			return processedSql;

		}


		public string function getDocumentSetupsSQL (any userId = ""){
			var Sql = "";
			var programSql = "";
			var courseSql = "";
			var previewCondition = "1=1";
	   		var LogicalCondition = application.wirebox.getInstance('LogicalCondition@CORE');
 			var queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder");
 			var EntityRestrictions =  application.wirebox.getInstance(name='sis_core.model.blAutomation.EntityRestrictions');

			var programRestrictionsList = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_PROGRAM',userId=arguments.userId,securityCode="DOCUMENT_BUILDER_MODULE");
			var courseRestrictionsList = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_COURSE',userId=arguments.userId,securityCode="DOCUMENT_BUILDER_MODULE");

			if (programRestrictionsList NEQ "*"){
				var previewCondition = previewCondition & " AND X.DOC_SETUP_ID IN (SELECT A.DOC_SETUP_ID
							FROM TES_CONFIG A,
								 TES_CONFIG_PROGRAMS B
							WHERE	A.TES_CONFIG_ID = B.TES_CONFIG_ID(+)
							AND (B.PROGRAM_CD IS NULL OR
							B.PROGRAM_CD IN (#programRestrictionsList#)))";

			}
			if (courseRestrictionsList NEQ "*"){
				var previewCondition = previewCondition & " AND X.DOC_SETUP_ID IN (SELECT A.DOC_SETUP_ID
							FROM TES_CONFIG A,
								 UG_TES_CONFIG_COURSES B
							WHERE	A.TES_CONFIG_ID = B.TES_CONFIG_ID(+)
							AND (B.COURSE_CD IS NULL OR
							B.COURSE_CD IN (#courseRestrictionsList#)))";

			}
			var conditionQry = "
		    	SELECT
		    		DISTINCT X.DOC_SETUP_ID as ACCESS_LIST,
		    		#arguments.userId# as USER_ID
		    	FROM
		    		DOC_SETUP X
		    	WHERE
		    		#previewCondition#";
			return conditionQry;
			}

			public string function  getInstancesSQL (any userId = ""){
				var Sql = "";
				var processedSql = "";

				savecontent variable="processedSql" {
						WriteOutput("SELECT
		                				Y.USER_ID,
		                				Y.ROLE_ID,
		                				NVL(LISTAGG(A.REFERENCE_ID,',') WITHIN GROUP (ORDER BY REFERENCE_ID),'*') AS ACCESS_LIST
									FROM
						                  {cbx_datasource}.UD_USER X,
						                  {cbx_datasource}.UD_USER_ROLE Y,
						                  {cbx_datasource}.UD_ROLE Z,
										  {cbx_datasource}.UD_ACCESS_NODE A ,
										  {cbx_datasource}.UD_USER_ROLE_ACCESS B
									WHERE
							              X.USER_ID = Y.USER_ID AND
							              Y.ROLE_ID = Z.ROLE_ID AND
							              Z.IS_SYSTEM = 'N' AND
							              Y.USER_ID = B.USER_ID (+) AND
							              Y.ROLE_ID = B.ROLE_ID (+) AND
										  B.ACCESS_NODE_ID = A.ACCESS_NODE_ID (+) AND
										  UPPER(A.LABEL (+)) = '#UCase("DB INSTANCE")#'
										");
						if(arguments.userId != "") {
							WriteOutput(' AND X.USER_ID = #arguments.userId# ');
						}
						WriteOutput("
	             				GROUP BY
	              				 	 Y.USER_ID,
	              				 	 Y.ROLE_ID

						");
					}
					return processedSql;
			}


	</cfscript>

</cfcomponent>