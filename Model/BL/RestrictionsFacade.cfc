﻿component displayname="RestrictionsFacade" accessors="true" {
	property name="userSecurity";
	property name="userDirectoryAPI" inject="id:API@UserDirectory";
	property name="QueryHelper" inject="coldbox:plugin:QueryHelper";
	property name="udInstanceId";

	/**
	 *@hint Constructor
	 */
	function init(required any udInstanceId, any userSecurity) {
		variables.udInstanceId = arguments.udInstanceId;
		variables.userSecurity = arguments.userSecurity;
		return this;
	}

	any function getDataRestrictions(required string restrictionNodeCode = "", string label = "*", string bReturnType = "query") {
		var nodes = variables.userSecurity.getChildNodes(arguments.restrictionNodeCode);
		if (filterByAccessNode()) {
			// search roles that have the current access node enabled
			var roles = getUserRolesByAccessNode( getCurrentAccessNodeCode() );

			if (StructCount(roles)) {

				// if any of the roles has no data restrictions
				if (anyRoleWithoutDataRestrictions( roles, code )) {
					nodes = QueryNew("access_node_id,access_node_name,access_node_code,parent_node_id,reference_id,access_type_id,access_type_name,access_type_code,level,label");
				}
				// children of node with a specific code
				var gatheredNodes = gatherDataAccessNodes(roles, code);
				if (ListLen(gatheredNodes)) {
					nodes = variables.QueryHelper.filterQuery( nodes, "ACCESS_NODE_ID", gatheredNodes, "CF_SQL_DECIMAL", true );
				} else {
					nodes = QueryNew("access_node_id,access_node_name,access_node_code,parent_node_id,reference_id,access_type_id,access_type_name,access_type_code,level,label");
				}
			}
		}

		if (arguments.label neq "*") {
			nodes = variables.QueryHelper.filterQuery( nodes, "LABEL", arguments.label, "CF_SQL_VARCHAR");
		}

		if (arguments.bReturnType eq "query") {
			return nodes;
		} else {
			return ValueList(nodes.reference_id);
		}
	}

	/**
	 *@hint Determine if data restrictions need to be filtered by a security access node.
	 */
	boolean function filterByAccessNode() {
		return ListLen( getCurrentAccessNodeCode() );
	}

	/**
	 *@hint Read current security access node.
	 *@note Current access node represents the current module's access node in User Directory. See UserDirectoryInterceptor.cfc
	 */
	string function getCurrentAccessNodeCode() {
		var requestService = application.wirebox.getInstance(dsl="coldbox:RequestService");
		return requestService.requestCapture().getValue(name="accessNodeCode", defaultValue="", private=true);
	}

	/**
	 *@hint Given a security access node code, determine the user roles that have it enabled.
	 *@returns Struct of queries with user access nodes indexed by role_id.
	 */
	struct function getUserRolesByAccessNode(required string code) {
		var roles = {};
		var user  = userDirectoryAPI.getUserService(udInstanceId).getUserInfo( sessionManager.getVar('userid', 0) );

		// Note: getAccess() returns a struct of queries with user access nodes indexed by role_id
		var accessNodesByRole = user.getAccess();
		for (var roleIndex in accessNodesByRole) {
			if (variables.QueryHelper.filterQuery( accessNodesByRole[roleIndex], "ACCESS_NODE_CODE", code, "CF_SQL_VARCHAR" ).recordCount > 0) {
				StructInsert(roles,roleIndex,accessNodesByRole[roleIndex]);
			}
		}
		return roles;
	}

	/**
	 *@hint Check if any of the roles has data restrictions.
	 */
	boolean function anyRoleWithoutDataRestrictions(required struct roles, string code = "") {
		var DATA_PARENT_NODE_ID = 2;

		for (var roleIndex in roles) {
			// examples: TRAINING_LOCATIONS, RESIDENCY_PROGRAMS etc.
			var dataRestrictionParents = variables.QueryHelper.filterQuery( roles[roleIndex], "PARENT_NODE_ID", DATA_PARENT_NODE_ID, "CF_SQL_DECIMAL" );

			if (dataRestrictionParents.recordCount == 0) {
				return true;
			}

			// a specific code is requested
			if (ListLen(arguments.code) && (variables.QeuryHelper.filterQuery( dataRestrictionParents, "ACCESS_NODE_CODE", arguments.code, "CF_SQL_VARCHAR" ).recordCount == 0) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 *@hint From the roles, gather all the data access node identifiers children of a given data access node code.
	 *@returns Comma separated list of access node identifiers.
	 */
	string function gatherDataAccessNodes(required struct roles, required string code) {
		var nodesList = "";

		// TODO: traverse full data restrictions tree (more the 2 levels)
		for (var roleIndex in roles) {
			var parentAccessNode = variables.QueryHelper.filterQuery( roles[roleIndex], "ACCESS_NODE_CODE", code, "CF_SQL_VARCHAR" );
			if (parentAccessNode.recordCount > 0) {
				var dataAccessNodes  = variables.QueryHelper.filterQuery( roles[roleIndex], "PARENT_NODE_ID", parentAccessNode.ACCESS_NODE_ID, "CF_SQL_DECIMAL" );
				// TODO: check for duplicates
				nodesList = ListAppend(nodesList, ValueList(dataAccessNodes.ACCESS_NODE_ID));
			}
		}
		return nodesList;
	}


}