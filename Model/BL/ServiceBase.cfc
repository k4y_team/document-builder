<cfcomponent displayname="ServiceBase" name="ServiceBase" output="false">

	<cffunction name="init" access="public" returntype="ServiceBase">
		<cfset variables.wirebox = application.wirebox>
		<cfset pg_datasource = variables.wirebox.getInstance(dsl="coldbox:setting:pg_datasource")>
		<cfset variables.dbFactory = variables.wirebox.getInstance(name="medsisOld.modules.DocumentBuilder.Model.BL.dbFactory",initArguments={datasource=pg_datasource})>
		<cfset datasource = variables.wirebox.getInstance(dsl="coldbox:setting:datasource")>
		<cfset variables.dbDocumentFactory = variables.wirebox.getInstance(name="medsisOld.modules.DocumentBuilder.Model.BL.dbFactory",initArguments={datasource=datasource})>
		<cfset variables.populator = variables.wirebox.getInstance("populator@medsis")>
		<cfset variables.MsgBox = variables.wirebox.getInstance(dsl="coldbox:myplugin:MsgBox")>
		<cfset variables.Notificator = variables.wirebox.getInstance(dsl="coldbox:myplugin:Notificator")>
		<cfset variables.logger = variables.wirebox.getInstance(dsl="logbox:logger:LOALogger")>
		<cfset variables.SessionStorage = application.cbcontroller.getPlugin('SessionStorage')>
		<cfset variables.ModuleConfig = application.ModuleManager.getModuleConfig("DOCUMENT_BUILDER_MODULE")>
		<cfset variables.Interceptor = application.wirebox.getInstance("Interceptor@CORE")>
		<cfreturn this />
	</cffunction>

	<cffunction access="public" name="queryToArray" returntype="array">
		<cfargument name="qry" type="query" required="true">
		<cfargument name="columnList" type="string" required="false" default="*">

		<cfset myarray = ArrayNew(1)>
		<cfif arguments.columnList eq "" or arguments.columnList eq "*">
			<cfset qryColumns = arguments.qry.ColumnList>
		<cfelse>
			<cfset qryColumns = arguments.columnList>
		</cfif>
		<cfloop query="arguments.qry">
			<cfset tmp = {}>
			<cfloop list="#qryColumns#" index="column">
				<cfset tmp[uCase(column)] = arguments.qry[column][CurrentRow]>
			</cfloop>
		    <cfset ArrayAppend(myarray, tmp)>
		</cfloop>

		<cfreturn myarray>
	</cffunction>

</cfcomponent>