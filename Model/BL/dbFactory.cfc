<cfcomponent displayname="dbFactory" output="false">
	<cfproperty name="wirebox" inject="wirebox" scope="variables" />
	<cfproperty name="datasource" type="string" default="medsis"/>
	<cfproperty name="memory" inject="coldbox:myPlugin:Memory"/>

	<cffunction name="init" access="public" returntype="any">
		<cfargument name="datasource" type="string">
		<cfset variables.instance.datasource = arguments.datasource />
		<cfreturn this />
	</cffunction>

	<cffunction name="getDatasource" access="public" returntype="string">
		<cfreturn variables.instance.datasource />
	</cffunction>

	<cffunction name="create" access="public" returntype="any">
		<cfargument name="name" type="string" required="true">
		<cfargument name="bLoadRestrictions" type="boolean" required="false" default="false">

		<cfset var sessionStorage = variables.wirebox.getInstance(dsl = "coldbox:plugin:SessionStorage" )>
		<cfset result = CreateObject("component","medsisOld.modules.DocumentBuilder.model.db.#arguments.name#")>
		<cfif FindNoCase("DAO", arguments.name) OR FindNoCase("GTW", arguments.name)>
			<cfif sessionStorage.exists('LoggedInUser') and arguments.bLoadRestrictions>
				<cfset var userSessionData = sessionStorage.getVar( 'LoggedInUser' )>
				<cfset var UserSecurity = wirebox.getInstance( "API@UserDirectory" ).getUserSecurity( application.cbController.getSetting( 'ud_instance_id' ),userSessionData.user_id )>

				<cfset var restrictions = variables.wirebox.getInstance(name="medsisOld.modules.DocumentBuilder.Model.BL.RestrictionsFacade",initArguments={udInstanceId=application.cbController.getSetting('ud_instance_id'),userSecurity=UserSecurity})>
			</cfif>

			<cftry>
				<cfif isDefined("restrictions")>
					<cfset result.init(getDatasource(), restrictions) />
				<cfelse>
					<cfset result.init(getDatasource()) />
				</cfif>
			<cfcatch>
				<cfdump var="#cfcatch#">
				<cfabort>
			</cfcatch>
			</cftry>
		<cfelseif Right(arguments.name, 2) EQ "VO">
			<cftry>
				<cfset result.init()>
				<cfcatch></cfcatch>
			</cftry>
		</cfif>
		<cfreturn result />
	</cffunction>

</cfcomponent>