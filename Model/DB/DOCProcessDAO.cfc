<cfcomponent name="DOCProcessDAO" displayname="DOCProcessDAO" hint="I abstract data access for DOC_PROCESS" extends="dbObject">
	<cffunction name="read" returntype="DOCProcessVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="processID" type="numeric" required="true" />

		<cfset var docProcessVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				PROCESS_ID,
				PROCESS_NAME,
				LOG_DETAIL,
				PROCESS_STATUS,
				PARENT_PROCESS_ID,
				START_DT,
				END_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INSTANCE_ID
			FROM DOC_PROCESS
			WHERE
				PROCESS_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.processID#"/>
		</cfquery>
		<cfset docProcessVO = CreateObject("component","DOCProcessVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset docProcessVO.setPROCESS_ID(getQry.PROCESS_ID) />
			<cfset docProcessVO.setPROCESS_NAME(getQry.PROCESS_NAME) />
			<cfset docProcessVO.setLOG_DETAIL(getQry.LOG_DETAIL) />
			<cfset docProcessVO.setPROCESS_STATUS(getQry.PROCESS_STATUS) />
			<cfset docProcessVO.setPARENT_PROCESS_ID(getQry.PARENT_PROCESS_ID) />
			<cfset docProcessVO.setSTART_DT(getQry.START_DT) />
			<cfset docProcessVO.setEND_DT(getQry.END_DT) />
			<cfset docProcessVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset docProcessVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset docProcessVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset docProcessVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset docProcessVO.setINSTANCE_ID(getQry.INSTANCE_ID) />
		</cfif>

		<cfreturn docProcessVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DOCProcessVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">

		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT DOC_PROCESS_SEQ.nextval AS nextval
			FROM dual
		</cfquery>

		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO DOC_PROCESS (
				PROCESS_ID ,PROCESS_NAME ,LOG_DETAIL ,PROCESS_STATUS ,PARENT_PROCESS_ID ,START_DT ,END_DT ,CREATION_ID ,CREATION_DT, INSTANCE_ID)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getPROCESS_NAME()#" maxLength="200" null="#iif((arguments.bean.getPROCESS_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getLOG_DETAIL()#" maxLength="4000" null="#iif((arguments.bean.getLOG_DETAIL() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getPROCESS_STATUS()#" maxLength="100" null="#iif((arguments.bean.getPROCESS_STATUS() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getPARENT_PROCESS_ID()#" maxLength="22" null="#iif((arguments.bean.getPARENT_PROCESS_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getSTART_DT()#" maxLength="26" null="#iif((arguments.bean.getSTART_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getEND_DT()#" maxLength="26" null="#iif((arguments.bean.getEND_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINSTANCE_ID()#" maxLength="22" null="#iif((arguments.bean.getINSTANCE_ID() eq ''),de("true"), de("false"))#" />)
		</cfquery>

		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DOCProcessVO" required="true" />
		<cfset var updateQry = "" />

		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	DOC_PROCESS
			SET
				PROCESS_NAME = <cfqueryparam value="#arguments.bean.getPROCESS_NAME()#" cfsqltype="cf_sql_varchar" maxLength="200" null="#iif((arguments.bean.getPROCESS_NAME() eq ''),de("true"), de("false"))#" />,
				LOG_DETAIL = <cfqueryparam value="#arguments.bean.getLOG_DETAIL()#" cfsqltype="cf_sql_varchar" maxLength="4000" null="#iif((arguments.bean.getLOG_DETAIL() eq ''),de("true"), de("false"))#" />,
				PROCESS_STATUS = <cfqueryparam value="#arguments.bean.getPROCESS_STATUS()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getPROCESS_STATUS() eq ''),de("true"), de("false"))#" />,
				PARENT_PROCESS_ID = <cfqueryparam value="#arguments.bean.getPARENT_PROCESS_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getPARENT_PROCESS_ID() eq ''),de("true"), de("false"))#" />,
				START_DT = <cfqueryparam value="#arguments.bean.getSTART_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getSTART_DT() eq ''),de("true"), de("false"))#" />,
				END_DT = <cfqueryparam value="#arguments.bean.getEND_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getEND_DT() eq ''),de("true"), de("false"))#" />,
				INSTANCE_ID = <cfqueryparam value="#arguments.bean.getINSTANCE_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getINSTANCE_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />
			WHERE
				PROCESS_ID = <cfqueryparam value="#arguments.bean.getPROCESS_ID()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="processID" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	DOC_PROCESS
			WHERE
				PROCESS_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.processID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>
