<cfcomponent name="DOCProcessGtw" displayname="DOCProcessGtw" output="false" hint="" extends="dbObject">

	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				PROCESS_ID,
				PROCESS_NAME,
				LOG_DETAIL,
				PROCESS_STATUS,
				PARENT_PROCESS_ID,
				START_DT,
				END_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_PROCESS
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="processID" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				PROCESS_ID,
				PROCESS_NAME,
				LOG_DETAIL,
				PROCESS_STATUS,
				PARENT_PROCESS_ID,
				START_DT,
				END_DT,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_PROCESS
			WHERE
				PROCESS_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.processID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="isProcessActive" access="public" returntype="boolean" output="false" hint="">
		<cfargument name="instanceID" type="any" required="false" default="" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT a.process_id
			FROM doc_process a
			WHERE
				<cfif arguments.instanceID neq "">
 				a.instance_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" /> AND
				</cfif>
				upper(a.process_status) = 'PROGRESS' AND
				a.parent_process_id = -1 AND
				a.start_dt > sysdate - 1
		</cfquery>

		<cfif getQry.recordCount gt 0>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>
	</cffunction>

	<cffunction name="getProcessLog" access="public" returntype="query" output="false" hint="">
		<cfargument name="processId" type="numeric" required="true">

		<cfset var getQry = ""/>
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT a.process_id, a.process_name, a.start_dt, a.end_dt, a.log_detail
			FROM
				doc_process a
			WHERE
				a.process_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.processID#" />
			UNION
			SELECT a.process_id, a.process_name, a.start_dt, a.end_dt, a.log_detail
			FROM
				doc_process a
			WHERE
				a.parent_process_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.processID#" />
				<cfif instanceResList neq "" and instanceResList neq "*">
					AND A.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
				</cfif>
			ORDER BY start_dt DESC, process_id DESC
		</cfquery>

		<cfreturn getQry>
	</cffunction>

	<cffunction name="getAllLogs" access="public" returntype="query" output="false" hint="">
		<cfargument name="instanceID" type="any" required="false" default="">

		<cfset var getQry = ""/>
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>
		<cfquery name="getQry" maxrows="10" datasource="#getDatasource()#">
			SELECT a.process_id, a.process_name, a.start_dt, a.end_dt, a.process_status,
				b.last_name || ', ' || b.first_name AS createdBy
			FROM
				doc_process a,
			 	UD_USER b
			WHERE
				<cfif arguments.instanceID neq "">
				a.instance_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" /> AND
				</cfif>
				a.creation_id = b.user_id (+)
				<cfif instanceResList neq "" and instanceResList neq "*">
					AND A.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
				</cfif>
				AND a.parent_process_id = -1
			ORDER BY a.start_dt DESC
		</cfquery>

		<cfreturn getQry>
	</cffunction>

	<cffunction name="getLastProcess" access="public" returntype="query" output="false" hint="">
		<cfargument name="instanceID" type="any" required="false" default="" />
		<cfset var getQry = ""/>

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT max(x.process_id) AS process_id
			FROM doc_process x
			WHERE
				<cfif arguments.instanceID neq "">
			    x.instance_id = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" /> AND
			    </cfif>
				x.parent_process_id = -1 AND
				x.process_status = 'COMPLETED'
		</cfquery>

		<cfreturn getByPK(Val(getQry.process_id)) />
	</cffunction>

</cfcomponent>