<cfcomponent name="DOCProcessVO" displayname="DOCProcessVO" extends="getSetObjectValue" output="false">
		<cfproperty name="PROCESS_ID" type="numeric" />
		<cfproperty name="PROCESS_NAME" type="string" />
		<cfproperty name="LOG_DETAIL" type="string" />
		<cfproperty name="PROCESS_STATUS" type="string" />
		<cfproperty name="PARENT_PROCESS_ID" type="numeric" />
		<cfproperty name="START_DT" type="date" />
		<cfproperty name="END_DT" type="date" />
		<cfproperty name="CREATION_ID" type="numeric" />
		<cfproperty name="CREATION_DT" type="date" />
		<cfproperty name="MODIFICATION_ID" type="numeric" />
		<cfproperty name="MODIFICATION_DT" type="date" />
		<cfproperty name="INSTANCE_ID" type="numeric" />

	<cffunction name="init" access="public" returntype="DOCProcessVO" output="false" displayname="init" hint="I initialize a DOCProcess">
		<cfset super.init() />
		<cfset variables.PROCESS_ID = "" />
		<cfset variables.PROCESS_NAME = "" />
		<cfset variables.LOG_DETAIL = "" />
		<cfset variables.PROCESS_STATUS = "" />
		<cfset variables.PARENT_PROCESS_ID = "" />
		<cfset variables.START_DT = "" />
		<cfset variables.END_DT = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.INSTANCE_ID = "" />

		<cfreturn this />
 	</cffunction>

	<cffunction name="setPROCESS_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.PROCESS_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getPROCESS_ID" access="public" returntype="any" output="false">
		<cfreturn variables.PROCESS_ID />
	</cffunction>
	<cffunction name="setPROCESS_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.PROCESS_NAME = arguments.val />
	</cffunction>
	<cffunction name="getPROCESS_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.PROCESS_NAME />
	</cffunction>
	<cffunction name="setLOG_DETAIL" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.LOG_DETAIL = arguments.val />
	</cffunction>
	<cffunction name="getLOG_DETAIL" access="public" returntype="any" output="false">
		<cfreturn variables.LOG_DETAIL />
	</cffunction>
	<cffunction name="setPROCESS_STATUS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.PROCESS_STATUS = val/>
	</cffunction>
	<cffunction name="getPROCESS_STATUS" access="public" returntype="any" output="false">
		<cfreturn variables.PROCESS_STATUS />
	</cffunction>
	<cffunction name="setPARENT_PROCESS_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.PARENT_PROCESS_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getPARENT_PROCESS_ID" access="public" returntype="any" output="false">
		<cfreturn variables.PARENT_PROCESS_ID />
	</cffunction>
	<cffunction name="setSTART_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.START_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getSTART_DT" access="public" returntype="any" output="false">
		<cfreturn variables.START_DT />
	</cffunction>
	<cffunction name="setEND_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.END_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getEND_DT" access="public" returntype="any" output="false">
		<cfreturn variables.END_DT />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>

	<cffunction name="setINSTANCE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.INSTANCE_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getINSTANCE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INSTANCE_ID />
	</cffunction>
</cfcomponent>
