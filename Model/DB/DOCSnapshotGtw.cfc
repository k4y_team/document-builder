<cfcomponent name="DOCSnapshotGtw" displayname="DOCSnapshotGtw" extends="dbObject">

	<cffunction name="getByFilter" access="public" output="false" returntype="query">
		<cfargument name="filter" type="struct" required="false" default="#StructNew()#">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT x.*
			FROM
				doc_snapshot_base x,
				doc_snapshot_detail y
			WHERE
				x.doc_snapshot_cd = y.doc_snapshot_cd
				<cfif StructKeyExists(arguments.filter,"instance_id") and arguments.filter.instance_id neq "*">
					AND x.instance_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.filter.instance_id#">)
				</cfif>
				<cfif StructKeyExists(arguments.filter,"training_session_cd") and arguments.filter.training_session_cd neq "*">
					AND y.training_session_cd in (<cfqueryparam cfsqltype="cf_sql_numeric"list="true"  value="#arguments.filter.training_session_cd#">)
				</cfif>
				<cfif StructKeyExists(arguments.filter,"letter_template_cd") and arguments.filter.letter_template_cd neq "*">
					AND y.doc_letter_template_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.filter.letter_template_cd#">)
				</cfif>
				<cfif StructKeyExists(arguments.filter,"user_id") and arguments.filter.user_id neq "*">
					AND x.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.filter.user_id#">)
				</cfif>
				<cfif StructKeyExists(arguments.filter,"letter_confirmed") and not arguments.filter.letter_confirmed>
					AND x.doc_confirmed_dt is null
				</cfif>
		</cfquery>
		<cfreturn getQry />
	</cffunction>

	<cffunction name="getDOCProcessTrainees" access="public" output="false" returntype="query">
		<cfargument name="instanceName" type="string" required="true">
		<cfargument name="trSessCD" type="numeric" required="true">
		<cfargument name="docType" type="string" required="true">
		<cfargument name="stdID" type="numeric" required="false" default="0">
		<cfargument name="userID" type="string" required="false" default="*">
		<!--- ORIGINAL - trainees without LOA; REVISED -  trainees with LOA; --->

		<cfset var getQry = ""/>
		<cfswitch expression="#arguments.docType#">
			<!--- trainees without LOA --->
		<cfcase value="ORIGINAL">
				<cfquery name="getQry" datasource="#getDatasource()#">
					SELECT
						a.student_id,
						d.user_id,
						a.training_session_cd,
						a.student_type_cd as trainee_type_cd,
						(SELECT
							DOC_LETTER_TYPE_CD
						FROM
							DOC_LETTER_TYPE
						WHERE
							upper(LETTER_TYPE_CODE) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#uCase(arguments.docType)#" />) AS doc_letter_type_cd,
						null AS doc_change_type_cd
					FROM
						std_registration a,
						registration_status c,
						student_user d
					WHERE
						<cfif Val(arguments.stdID) GT 0>
							a.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
						</cfif>
						a.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
						a.registration_status_cd = c.registration_status_cd AND
						c.registration_status_code != <cfqueryparam cfsqltype="cf_sql_varchar" value="WITHDRAWN"> AND
						a.student_id = d.student_id AND
						a.student_type_cd IS NOT null AND
						<cfif arguments.userID neq "*">
							d.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">) AND
						</cfif>
						EXISTS (SELECT 1
						        FROM
						        	std_training x,
						        	funding_source y,
						        	funding_type z,
						        	training_level t,
									training_level_type w
								WHERE
								    x.funding_source_cd = y.funding_source_cd (+) AND
								    y.funding_type_cd = z.funding_type_cd (+) AND
								    x.training_level_cd = t.training_level_cd AND
									t.training_level_type_cd = w.training_level_type_cd AND
								    <cfif arguments.instanceName eq "HAL">
								    	z.funding_type_desc = 'OMH' AND
								    	w.training_level_type_desc = 'Resident' AND
								    </cfif>
									x.student_id = a.student_id AND
									x.training_session_cd = a.training_session_cd) AND
						NOT EXISTS (SELECT 1
									FROM
										doc_snapshot_base x,
										doc_snapshot_detail y,
										doc_instance z
									WHERE
										x.instance_id = z.instance_id and
										upper(z.instance_name) = <cfqueryparam cfsqltype="cf_sql_varchar" value="#uCase(arguments.instanceName)#"> AND
										x.doc_snapshot_cd = y.doc_snapshot_cd AND
										y.student_id = a.student_id AND
										y.training_session_cd = a.training_session_cd)
				</cfquery>
			</cfcase>
			<!--- trainees with LOA --->
			<cfcase value="REVISED">
				<cfquery name="getQry" datasource="#getDatasource()#">
					WITH
					sq_generated_loa AS (
						SELECT b.student_id, b.training_session_cd, b.doc_letter_data_cd,a.instance_id
						FROM
							doc_snapshot_base a,
							doc_snapshot_detail b,
							doc_instance c,
							student_user d
						WHERE
							a.instance_id = c.instance_id and
							upper(c.instance_name) = <cfqueryparam cfsqltype="cf_sql_varchar" value="#uCase(arguments.instanceName)#"> AND
							a.doc_snapshot_cd = b.doc_snapshot_cd AND
							a.doc_published_dt IS null AND
							<cfif Val(arguments.stdID) GT 0>
								b.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
							</cfif>
							b.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
							b.student_id = d.student_id
							<cfif arguments.userID neq "*">
								AND d.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">)
							</cfif>),
					sq_current_published_loa AS (
						SELECT b.student_id, b.training_session_cd, b.doc_letter_data_cd, a.instance_id
						FROM
							doc_snapshot_base a,
							doc_snapshot_detail b,
							doc_instance c,
							student_user d
						WHERE
							a.instance_id = c.instance_id and
							upper(c.instance_name) = <cfqueryparam cfsqltype="cf_sql_varchar" value="#uCase(arguments.instanceName)#"> AND
							a.doc_snapshot_cd = b.doc_snapshot_cd AND
							a.doc_published_dt IS NOT null AND
							a.is_current = 'Y' AND
							<cfif Val(arguments.stdID) GT 0>
								b.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
							</cfif>
							b.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
							b.student_id = d.student_id
							<cfif arguments.userID neq "*">
								AND d.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">)
							</cfif>),
					sq_data_loa AS (
						SELECT distinct a.student_id, a.training_session_cd
							<cfif arguments.instanceName eq "LOA">
								,nvl(b.program_cd,0) AS program_cd
								,nvl(b.subprogram_cd,0) AS subprogram_cd
								,nvl(b.training_status_cd,0) AS training_status_cd
								,nvl(b.funding_source_cd,0) AS funding_source_cd
								,nvl(b.pool,'0') AS pool
								,nvl(b.student_type_cd,0) AS student_type_cd
							</cfif>
							,nvl(b.training_level_cd,0) AS training_level_cd
							,to_number(to_char(b.start_dt,'yyyymmdd')) AS start_dt
							,to_number(to_char(b.end_dt,'yyyymmdd')) AS end_dt
						FROM
							sq_current_published_loa a,
							doc_data_change b
						WHERE
							a.doc_letter_data_cd = b.doc_letter_data_cd),
					sq_orig_data_loa AS (
						SELECT distinct a.student_id, a.training_session_cd
							<cfif arguments.instanceName eq "LOA">
								,nvl(b.base_program_cd,nvl(b.program_cd,0)) AS program_cd
								,nvl(b.subprogram_cd,0) AS subprogram_cd
								,nvl(b.training_status_cd,0) AS training_status_cd
								,nvl(b.funding_source_cd,0) AS funding_source_cd
								,nvl(p.pool_desc,'0') AS pool
								,nvl(b.student_type_cd,0) AS student_type_cd
							</cfif>
							,nvl(b.training_level_cd,0) AS training_level_cd
							,to_number(to_char(b.start_dt,'yyyymmdd')) AS start_dt
							,to_number(to_char(b.end_dt,'yyyymmdd')) AS end_dt
						FROM
							std_registration a,
							std_training b,
							student_user d,
							pool p,
						    funding_source fs,
						    funding_type ft,
						    training_level t,
							training_level_type w
						WHERE
							a.student_id = b.student_id AND
							a.training_session_cd = b.training_session_cd AND
							b.pool_cd = p.pool_cd (+) AND
							<cfif Val(arguments.stdID) GT 0>
								a.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
							</cfif>
							b.funding_source_cd = fs.funding_source_cd (+) AND
							fs.funding_type_cd = ft.funding_type_cd (+) AND
							b.training_level_cd = t.training_level_cd AND
							t.training_level_type_cd = w.training_level_type_cd AND
							<cfif arguments.instanceName neq "LOA">
								 ft.funding_type_desc = 'OMH' AND
								 w.training_level_type_desc = 'Resident' AND
						    </cfif>
							a.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
							a.student_id = d.student_id AND
							<cfif arguments.userID neq "*">
								d.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">) AND
							</cfif>
							EXISTS (SELECT 1 FROM sq_current_published_loa x
									WHERE
										x.student_id = a.student_id AND
										x.training_session_cd = a.training_session_cd) <!--- AND
							NOT EXISTS (SELECT 1 FROM sq_generated_loa x
										WHERE
											x.student_id = a.student_id AND
											x.training_session_cd = a.training_session_cd) --->),
					<cfif arguments.instanceName eq "LOA">
					sq_std_degree AS (
						SELECT a.student_id, b.university_cd,  upper(c.university_desc) as university_desc, b.degree_year, c.country_cd AS univ_country_cd, upper(d.country_desc) as univ_country_desc
						FROM
							std_registration a,
							std_degree b,
							university c,
							country d,
							student_user e
						WHERE
							a.student_id = b.student_id AND
							b.university_cd = c.university_cd AND
							<cfif Val(arguments.stdID) GT 0>
								a.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
							</cfif>
							a.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
							b.degree_cd IS NOT null AND
							c.country_cd = d.country_cd (+) AND
							upper(b.is_prime) = 'Y' AND
							a.student_id = e.student_id
							<cfif arguments.userID neq "*">
								AND e.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">)
							</cfif>),
					sq_std_cpso AS (
						SELECT distinct a.student_id, b.cpso_num, b.cpso_type_cd, cpso.cpso_type_desc
						FROM
							std_registration a,
							std_cpso_current_vw b,
							cpso_type cpso,
							student_user d
						WHERE
							a.student_id = b.student_id (+)
							and cpso.cpso_type_cd(+) = b.cpso_type_cd
							<cfif Val(arguments.stdID) GT 0>
								and a.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#">
							</cfif>
							and a.student_id = d.student_id
							<cfif arguments.userID neq "*">
								and d.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">)
							</cfif>
					),
					sq_std_contact AS (
						SELECT
							a.student_id
							, a.address1 as address
							, a.address2 as address2
							, a.city
							, b.country_desc as country
							, c.province_desc as province
							, a.postal_code
						FROM
							std_contact a,
							country b,
							province c,
							contact_type d,
							student_user e
						WHERE
							<cfif Val(arguments.stdID) GT 0>
								a.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
							</cfif>
							a.country_cd = b.country_cd (+) and
							a.province_cd = c.province_cd (+) and
							a.contact_type_cd = d.contact_type_cd (+) and
							upper(d.contact_type_desc) = 'CURRENT' and
							a.student_id = e.student_id
							<cfif arguments.userID neq "*">
								and e.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">)
							</cfif>
					),
					</cfif>
					sq_profile_loa AS (
						SELECT DISTINCT a.student_id, a.training_session_cd,
							trim(upper(b.first_name)) AS first_name,
							trim(upper(b.last_name)) AS last_name,
							trim(upper(b.student_number)) as student_number,
							trim(upper(b.ophrdc_number)) as ophrdc_number
							<cfif arguments.instanceName eq "LOA">
								,b.university_cd
								,b.univ_country_cd
								,b.degree_year
								,nvl(b.cpso_num,'0') AS cpso_num
								,nvl(b.cpso_type_cd,0) AS cpso_type_cd
								,b.address
								,b.address2
								,b.city
								,b.province
								,b.country
								,b.postal_code
							</cfif>
						FROM
							sq_current_published_loa a,
							doc_profile_change b
						WHERE
							a.doc_letter_data_cd = b.doc_letter_data_cd),
					sq_orig_profile_loa AS (
						SELECT DISTINCT a.student_id, a.training_session_cd,
							trim(upper(b.first_name)) AS first_name,
							trim(upper(b.last_name)) AS last_name,
							trim(upper(b.student_number)) as student_number,
							trim(upper(f.ophrdc)) as ophrdc_number
							<cfif arguments.instanceName eq "LOA">
								,c.university_cd
								,c.univ_country_cd
								,c.degree_year
								,nvl(d.cpso_num,'0') AS cpso_num
								,nvl(d.cpso_type_cd,0) AS cpso_type_cd
								,e.address
								,e.address2
								,e.city
								,e.province
								,e.country
								,e.postal_code
							</cfif>
						FROM
							std_registration a,
							student b,
							student_pgme f,
							student_user g
							<cfif arguments.instanceName eq "LOA">
								,sq_std_degree c
								,sq_std_cpso d
								,sq_std_contact e
							</cfif>
						WHERE
							a.student_id = b.student_id AND
							a.student_id = f.student_id AND
							<cfif arguments.instanceName eq "LOA">
								a.student_id = c.student_id AND
								a.student_id = d.student_id(+) AND
								a.student_id = e.student_id(+) AND
							</cfif>
							<cfif Val(arguments.stdID) GT 0>
								a.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
							</cfif>
							a.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
							a.student_id = g.student_id AND
							<cfif arguments.userID neq "*">
								g.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">) AND
							</cfif>
							EXISTS (SELECT 1 FROM sq_current_published_loa x
									WHERE
										x.student_id = a.student_id AND
										x.training_session_cd = a.training_session_cd) AND
							NOT EXISTS (SELECT 1 FROM sq_generated_loa x
										WHERE
											x.student_id = a.student_id AND
											x.training_session_cd = a.training_session_cd)),
					sq_changed_data_loa AS (
						SELECT student_id, training_session_cd, training_level_cd, start_dt, end_dt
							<cfif arguments.instanceName eq "LOA">
							,program_cd
							,subprogram_cd
							,training_status_cd
							,funding_source_cd
							,pool
							,student_type_cd
							</cfif>
						FROM
							(SELECT a.*, 1 AS src1, 0 AS src2 FROM sq_data_loa a
							 UNION ALL
							 SELECT b.*, 0 AS src1, 1 AS src2 FROM sq_orig_data_loa b)
						GROUP BY
							student_id, training_session_cd, training_level_cd, start_dt, end_dt
							<cfif arguments.instanceName eq "LOA">
							,program_cd
							,subprogram_cd
							,training_status_cd
							,funding_source_cd
							,pool
							,student_type_cd
							</cfif>
						HAVING sum(src1) <> sum(src2)),
					sq_changed_profile_loa AS (
						SELECT
						student_id, training_session_cd, first_name, last_name
						<cfif arguments.instanceName eq "LOA">
							,university_cd
							,univ_country_cd
							,degree_year
							,cpso_num
							,cpso_type_cd
							,address
							,address2
							,city
							,province
							,country
							,postal_code
						</cfif>
						FROM
							(SELECT a.*, 1 AS src1, 0 AS src2 FROM sq_profile_loa a
							 UNION ALL
							 SELECT b.*, 0 AS src1, 1 AS src2 FROM sq_orig_profile_loa b)
						GROUP BY
						student_id, training_session_cd, first_name, last_name
						<cfif arguments.instanceName eq "LOA">
							,university_cd
							,univ_country_cd
							,degree_year
							,cpso_num
							,cpso_type_cd
							,address
							,address2
							,city
							,province
							,country
							,postal_code
						</cfif>
						HAVING sum(src1) <> sum(src2)),
					sq_changed_data_population AS (
						SELECT distinct x.student_id, x.training_session_cd FROM sq_changed_data_loa x),
					sq_changed_profile_population AS (
						SELECT distinct x.student_id, x.training_session_cd FROM sq_changed_profile_loa x)
					SELECT a.student_id, d.user_id, a.training_session_cd, a.student_type_cd as trainee_type_cd, 'training change' as change_type,
						(SELECT
							DOC_LETTER_TYPE_CD
						FROM DOC_LETTER_TYPE
						WHERE
							upper(LETTER_TYPE_CODE) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#uCase(arguments.docType)#" />) AS doc_letter_type_cd,
						(SELECT
							DOC_CHANGE_TYPE_CD
						FROM DOC_CHANGE_TYPE
						WHERE
							upper(DOC_CHANGE_CODE) = 'REVISION') AS doc_change_type_cd
					FROM
						std_registration a,
						sq_changed_data_population b,
						registration_status c,
						student_user d
					WHERE
						a.student_id = b.student_id AND
						a.training_session_cd = b.training_session_cd AND
						<cfif Val(arguments.stdID) GT 0>
							a.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
						</cfif>
						a.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
						a.registration_status_cd = c.registration_status_cd AND
						c.registration_status_code != <cfqueryparam cfsqltype="cf_sql_varchar" value="WITHDRAWN"> AND
						a.student_type_cd IS NOT NULL AND
						a.student_id = d.student_id
						<cfif arguments.userID neq "*">
							AND d.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">)
						</cfif>
					UNION
					SELECT a.student_id, d.user_id, a.training_session_cd, a.student_type_cd as trainee_type_cd, 'profile change' as change_type,
						(SELECT
							DOC_LETTER_TYPE_CD
						FROM DOC_LETTER_TYPE
						WHERE
							upper(LETTER_TYPE_CODE) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#uCase(arguments.docType)#" />) AS doc_letter_type_cd,
						(SELECT
							DOC_CHANGE_TYPE_CD
						FROM DOC_CHANGE_TYPE
						WHERE
							upper(DOC_CHANGE_CODE) = 'CORRECTION') AS doc_change_type_cd
					FROM
						std_registration a,
						sq_changed_profile_population b,
						registration_status c,
						student_user d
					WHERE
						a.student_id = b.student_id AND
						a.training_session_cd = b.training_session_cd AND
						<cfif Val(arguments.stdID) GT 0>
							a.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
						</cfif>
						a.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
						a.registration_status_cd = c.registration_status_cd AND
						c.registration_status_code != <cfqueryparam cfsqltype="cf_sql_varchar" value="WITHDRAWN"> AND
						a.student_type_cd IS NOT null AND
						a.student_id = d.student_id AND
						<cfif arguments.userID neq "*">
							d.user_id in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userID#">) AND
						</cfif>
						NOT EXISTS (SELECT 1 FROM sq_changed_data_population x
									WHERE
										x.student_id = a.student_id AND
										x.training_session_cd = a.training_session_cd)
				</cfquery>
			</cfcase>
		</cfswitch>
		<cfreturn getQry />
	</cffunction>

    <cffunction name="deleteNotConfirmed" access="public" output="false" returntype="string">
		<cfargument name="instanceID" type="numeric" required="false" default="0">
		<cfargument name="trainingSessionCD" type="numeric" required="true">
		<cfargument name="snapshotCD" type="string" required="false" default="*">

        <cfset var delQry = "">
		<cfset var selqry = "">
		<cfset var studentIDsList = "">

		<cfset var stringUtil = CreateObject("component","sis_core.model.util.stringUtils").init()>
        <cfset var splittedList =  stringUtil.listSplit(arguments.snapshotCD) />

		<cfquery name="selQry" datasource="#getDatasource()#">
			SELECT DISTINCT y.student_id
			FROM
				doc_snapshot_base x,
				doc_snapshot_detail y
			WHERE
				x.instance_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#"> AND
				x.doc_snapshot_cd = y.doc_snapshot_cd AND
				y.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#"> AND
				<cfif arguments.snapshotCD neq "*">
					(1 = 0
					 <cfloop list="#splittedList#" index="idx" delimiters="/">
						or x.doc_snapshot_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#idx#">)
					 </cfloop>
					) AND
				</cfif>
				x.doc_confirmed_dt IS null
		</cfquery>
		<cfset studentIDsList = ValueList(selQry.student_id)>

		<!--- delete doc_SNAPSHOT_METADATA --->
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE
			FROM doc_snapshot_metadata
			WHERE doc_snapshot_cd in (
				SELECT x.doc_snapshot_cd
				FROM
					doc_snapshot_base x,
					doc_snapshot_detail y
				WHERE
					x.instance_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#"> AND
					x.doc_snapshot_cd = y.doc_snapshot_cd AND
					y.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#"> AND
					<cfif arguments.snapshotCD neq "*">
						(1 = 0
						<cfloop list="#splittedList#" index="idx" delimiters="/">
						or x.doc_snapshot_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#idx#">)
						</cfloop>) AND
					</cfif>
					x.doc_confirmed_dt IS NULL)
		</cfquery>

		<!--- delete doc_profile_change --->
		<cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM doc_profile_change a
            WHERE
				EXISTS (SELECT 1
						FROM
							doc_snapshot_base x,
							doc_snapshot_detail y
						WHERE
							x.instance_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#"> AND
							x.doc_snapshot_cd = y.doc_snapshot_cd AND
							y.doc_letter_data_cd = a.doc_letter_data_cd AND
							y.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#"> AND
							<cfif arguments.snapshotCD neq "*">
								(1 = 0
								<cfloop list="#splittedList#" index="idx" delimiters="/">
								or x.doc_snapshot_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#idx#">)
								</cfloop>) AND
							</cfif>
							x.doc_confirmed_dt IS null)
        </cfquery>

		<!--- delete doc_data_change --->
        <cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM doc_data_change a
            WHERE
				EXISTS (SELECT 1
						FROM
							doc_snapshot_base x,
							doc_snapshot_detail y
						WHERE
							x.instance_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#"> AND
							x.doc_snapshot_cd = y.doc_snapshot_cd AND
							y.doc_letter_data_cd = a.doc_letter_data_cd AND
							y.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#"> AND
							<cfif arguments.snapshotCD neq "*">
								(1 = 0
								<cfloop list="#splittedList#" index="idx" delimiters="/">
									or x.doc_snapshot_cd IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#idx#">)
								</cfloop>) AND
							</cfif>
							x.doc_confirmed_dt IS null)
        </cfquery>

		<!--- delete doc_snapshot_detail --->
        <cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM doc_snapshot_detail a
            WHERE
				a.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#"> AND
				<cfif arguments.snapshotCD neq "*">
					(1 = 0
					<cfloop list="#splittedList#" index="idx" delimiters="/">
						or a.doc_snapshot_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#idx#">)
					</cfloop>) AND
				</cfif>
				EXISTS (SELECT 1 FROM doc_snapshot_base x
						WHERE
							x.instance_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#"> AND
							x.doc_snapshot_cd = a.doc_snapshot_cd AND
							x.doc_confirmed_dt IS null)
        </cfquery>

		<!--- delete doc_letter_data --->
        <cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM doc_letter_data
            WHERE
				doc_letter_data_cd not in (
					select distinct doc_letter_data_cd
					from doc_data_change
				) and
				doc_letter_data_cd not in (
					select distinct doc_letter_data_cd
					from doc_profile_change
				) and
		        doc_letter_data_cd not in (
		          select distinct doc_letter_data_cd
		          from doc_snapshot_detail
        		)
        </cfquery>

		<!--- delete doc_snapshot_base --->
        <cfquery name="delQry" datasource="#getDatasource()#">
			delete
			from doc_snapshot_base
			where
				<cfif arguments.snapshotCD neq "*">
					(1 = 0
					<cfloop list="#splittedList#" index="idx" delimiters="/">
						or doc_snapshot_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#idx#">)
					</cfloop>)
				<cfelse>
				doc_snapshot_cd not in (
				  select doc_snapshot_cd
				  from doc_snapshot_detail
				)
				</cfif>
        </cfquery>
		<cfreturn studentIDsList>
    </cffunction>

    <cffunction name="deleteNotConfirmedByTemplate" access="public" output="false" returntype="string">
		<cfargument name="docLetterTemplateCD" type="numeric" required="true">
		<cfargument name="trainingSessionCD" type="numeric" required="false" default="-1">
		<cfargument name="userIDList" type="string" required="false" default="">

        <cfset var delQry = "">
		<cfset var selqry = "">
		<cfset var studentIDsList = "">

		<cfset var arrUsers = ArrayNew(1)>
		<cfset var qCount = ListLen(arguments.userIDList)>
		<cfif qCount gt 1000>
		 	<cfset var qList = ''>
		 	<cfloop from="1" to="#qCount#" index="i">
				<cfset qList = ListAppend(qList, ListGetAt(arguments.userIDList, i))>
			 	<cfif i mod 1000 eq 0>
				 	<cfset ArrayAppend(arrUsers, qList)>
				 	<cfset qList = ''>
				</cfif>
			</cfloop>
			<cfset ArrayAppend(arrUsers, qList)>
		</cfif>

		<cfquery name="selQry" datasource="#getDatasource()#">
			SELECT DISTINCT y.student_id
			FROM
				doc_snapshot_base x,
				doc_snapshot_detail y
			WHERE
				y.doc_letter_template_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#">
				AND x.doc_snapshot_cd = y.doc_snapshot_cd
				<cfif arguments.trainingSessionCD GT 0>
				AND y.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#">
				</cfif>

				<cfif ArrayLen(arrUsers) gt 0>
					AND ((1 = 0)
					<cfloop from="1" to="#ArrayLen(arrUsers)#" index="i">
						OR x.user_id IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrUsers[i]#" null="0" />)
					</cfloop>
					)
				<cfelseif arguments.userIDList neq "">
					AND x.user_id IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userIDList#">)
				</cfif>
				<!--- <cfif arguments.userIDList NEQ "">
									x.user_id IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userIDList#">) AND
								</cfif> --->
				AND x.doc_confirmed_dt IS null
		</cfquery>
		<cfset studentIDsList = ValueList(selQry.student_id)>

		<!--- delete doc_profile_change --->
		<cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM doc_profile_change a
            WHERE
				EXISTS (SELECT 1
						FROM
							doc_snapshot_base x,
							doc_snapshot_detail y
						WHERE
							y.doc_letter_template_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#">
							AND x.doc_snapshot_cd = y.doc_snapshot_cd
							AND y.doc_letter_data_cd = a.doc_letter_data_cd
							<cfif arguments.trainingSessionCD GT 0>
								AND y.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#">
							</cfif>

							<cfif ArrayLen(arrUsers) gt 0>
								AND ((1 = 0)
								<cfloop from="1" to="#ArrayLen(arrUsers)#" index="i">
									OR x.user_id IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrUsers[i]#" null="0" />)
								</cfloop>
								)
							<cfelseif arguments.userIDList neq "">
								AND x.user_id IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userIDList#">)
							</cfif>


							<!--- <cfif arguments.userIDList NEQ "">
															x.user_id IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userIDList#">) AND
														</cfif> --->
							AND x.doc_confirmed_dt IS null)
        </cfquery>

		<!--- delete doc_data_change --->
        <cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM doc_data_change a
            WHERE
				EXISTS (SELECT 1
						FROM
							doc_snapshot_base x,
							doc_snapshot_detail y
						WHERE
							y.doc_letter_template_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#">
							AND x.doc_snapshot_cd = y.doc_snapshot_cd
							AND y.doc_letter_data_cd = a.doc_letter_data_cd
							<cfif arguments.trainingSessionCD GT 0>
							AND y.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#">
							</cfif>

							<cfif ArrayLen(arrUsers) gt 0>
								AND ((1 = 0)
								<cfloop from="1" to="#ArrayLen(arrUsers)#" index="i">
									OR x.user_id IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrUsers[i]#" null="0" />)
								</cfloop>
								)
							<cfelseif arguments.userIDList neq "">
								AND x.user_id IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userIDList#">)
							</cfif>

							<!--- <cfif arguments.userIDList NEQ "">
															x.user_id IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userIDList#">) AND
														</cfif> --->
							AND x.doc_confirmed_dt IS null)
        </cfquery>

		<!--- delete doc_snapshot_detail --->

        <cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM doc_snapshot_detail a
            WHERE
            	1 = 1 AND
            	<cfif arguments.trainingSessionCD GT 0>
				a.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#"> AND
				</cfif>
				EXISTS (SELECT 1
						FROM
							doc_snapshot_base x,
							doc_snapshot_detail y
						WHERE
							y.doc_letter_template_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#">
							<cfif ArrayLen(arrUsers) gt 0>
								AND ((1 = 0)
								<cfloop from="1" to="#ArrayLen(arrUsers)#" index="i">
									OR x.user_id IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrUsers[i]#" null="0" />)
								</cfloop>
								)
							<cfelseif arguments.userIDList neq "">
								AND x.user_id IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userIDList#">)
							</cfif>

							<!--- <cfif arguments.userIDList NEQ "">
															x.user_id IN (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.userIDList#">) AND
														</cfif> --->
							AND x.doc_snapshot_cd = y.doc_snapshot_cd
							AND x.doc_snapshot_cd = a.doc_snapshot_cd
							AND x.doc_confirmed_dt IS null)
        </cfquery>

		<!--- delete doc_letter_data --->
        <cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM doc_letter_data
            WHERE
				doc_letter_data_cd not in (
					select distinct doc_letter_data_cd
					from doc_data_change
				) and
				doc_letter_data_cd not in (
					select distinct doc_letter_data_cd
					from doc_profile_change
				) and
		        doc_letter_data_cd not in (
		          select distinct doc_letter_data_cd
		          from doc_snapshot_detail
        		)
        </cfquery>

		<!--- delete doc_snapshot_base --->
        <cfquery name="delQry" datasource="#getDatasource()#">
			delete
			from doc_snapshot_base
			where
				doc_snapshot_cd not in (
				  select doc_snapshot_cd
				  from doc_snapshot_detail
				)
        </cfquery>

        <!--- delete doc_snapshot_metadata --->
        <cfquery name="delQry" datasource="#getDatasource()#">
			delete
			from doc_snapshot_metadata
			where
				doc_snapshot_cd not in (
				  select doc_snapshot_cd
				  from doc_snapshot_detail
				)
        </cfquery>

		<cfreturn studentIDsList>
    </cffunction>

    <cffunction name="publishConfirmed" access="public" output="false" returntype="void">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="snapshotCDList" type="string" required="true">
		<cfargument name="userId" type="numeric" required="true" />
		<cfargument name="overwriteFlag" type="string" required="false" default="N">

		<cfset var arrSnapshots = ArrayNew(1)>
		<cfset var qCount = ListLen(arguments.snapshotCDList)>
		<cfif qCount gt 1000>
		 	<cfset var qList = ''>
		 	<cfloop from="1" to="#qCount#" index="i">
				<cfset qList = ListAppend(qList, ListGetAt(arguments.snapshotCDList, i))>
			 	<cfif i mod 1000 eq 0>
				 	<cfset ArrayAppend(arrSnapshots, qList)>
				 	<cfset qList = ''>
				</cfif>
			</cfloop>
			<cfset ArrayAppend(arrSnapshots, qList)>
		</cfif>

		<!--- update existing --->

		<cfif arguments.overwriteFlag EQ 'Y'>
			<cfset var updateQry = "">
			<cfset var updateDetailQry = "">
			<cfset var delQry = "">

	        <cfquery name="updateQry" datasource="#getDatasource()#">
				MERGE INTO DOC_SNAPSHOT_DETAIL LSD
				USING
					(SELECT DISTINCT
						CUR_LOA.DOC_SNAPSHOT_CD AS CUR_DOC_SNAPSHOT_CD,
						PUB_LOA.DOC_TYPE_CD AS PUB_DOC_TYPE_CD,
						PUB_LOA.TRAINEE_TYPE_CD AS PUB_TRAINEE_TYPE_CD,
						PUB_LOA.DOC_LETTER_DATA_CD AS PUB_DOC_LETTER_DATA_CD,
						PUB_LOA.DOC_LETTER_TEMPLATE_CD AS PUB_DOC_LETTER_TEMPLATE_CD
					FROM
						DOC_INFO_VW CUR_LOA,
						DOC_INFO_VW PUB_LOA
					WHERE
						CUR_LOA.IS_CURRENT = 'Y'
						AND UPPER(CUR_LOA.DOC_SIGN_STATUS) = <cfqueryparam cfsqltype="cf_sql_varchar" value="OUTSTANDING">
						AND CUR_LOA.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#">
						AND CUR_LOA.STUDENT_ID = PUB_LOA.STUDENT_ID
						AND CUR_LOA.TRAINING_SESSION_CD = PUB_LOA.TRAINING_SESSION_CD
						AND CUR_LOA.DOC_SNAPSHOT_CD != PUB_LOA.DOC_SNAPSHOT_CD
						<cfif ArrayLen(arrSnapshots) gt 0>
							AND ((1 = 0)
							<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
								OR PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
							</cfloop>
							)
						<cfelse>
							AND PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
						</cfif>) NLSD
				ON
					(LSD.DOC_SNAPSHOT_CD = NLSD.CUR_DOC_SNAPSHOT_CD)
				WHEN MATCHED THEN
				UPDATE SET
					DOC_TYPE_CD = PUB_DOC_TYPE_CD,
					TRAINEE_TYPE_CD = PUB_TRAINEE_TYPE_CD,
					DOC_LETTER_DATA_CD = PUB_DOC_LETTER_DATA_CD,
					DOC_LETTER_TEMPLATE_CD = PUB_DOC_LETTER_TEMPLATE_CD
	        </cfquery>

			<!--- delete doc_letter_data --->
	        <!--- <cfquery name="updateDetailQry" datasource="#getDatasource()#">
	            UPDATE DOC_SNAPSHOT_DETAIL SET
					DOC_TYPE_CD = NULL,
					TRAINEE_TYPE_CD = NULL,
					DOC_LETTER_DATA_CD = NULL,
					DOC_LETTER_TEMPLATE_CD = NULL
	            WHERE
					<cfif ArrayLen(arrSnapshots) gt 0>
						(1 = 0)
						<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
							OR DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
						</cfloop>
					<cfelse>
						DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
					</cfif>
	        </cfquery> --->

			<!--- delete doc_snapshot_detail --->


	        <cfquery name="delQry" datasource="#getDatasource()#">
	            DELETE FROM DOC_SNAPSHOT_DETAIL A
	            WHERE
					<cfif ArrayLen(arrSnapshots) gt 0>
						((1 = 0)
						<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
							OR A.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
						</cfloop>
						) AND
					<cfelse>
						A.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />) AND
					</cfif>
					A.DOC_SNAPSHOT_CD IN (
						SELECT DISTINCT
							PUB_LOA.DOC_SNAPSHOT_CD
						FROM
							DOC_INFO_VW CUR_LOA,
							DOC_INFO_VW PUB_LOA
						WHERE
							CUR_LOA.IS_CURRENT = 'Y'
							AND UPPER(CUR_LOA.DOC_SIGN_STATUS) = <cfqueryparam cfsqltype="cf_sql_varchar" value="OUTSTANDING">
							AND CUR_LOA.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#">
							AND CUR_LOA.STUDENT_ID = PUB_LOA.STUDENT_ID
							AND CUR_LOA.TRAINING_SESSION_CD = PUB_LOA.TRAINING_SESSION_CD
							AND CUR_LOA.DOC_SNAPSHOT_CD != PUB_LOA.DOC_SNAPSHOT_CD
							<cfif ArrayLen(arrSnapshots) gt 0>
								AND ((1 = 0)
								<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
									OR PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
								</cfloop>
								)
							<cfelse>
								AND PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
							</cfif>
					) AND
					(SELECT
							COUNT(B.DOC_SNAPSHOT_CD)
					 FROM
					 		DOC_SNAPSHOT_DETAIL B,
			                DOC_SNAPSHOT_BASE C
			         WHERE
			         		B.STUDENT_ID = A.STUDENT_ID
			         AND
			         		B.DOC_SNAPSHOT_CD = C.DOC_SNAPSHOT_CD
			         AND
			         		B.TRAINING_SESSION_CD = A.TRAINING_SESSION_CD
			         AND
			         		C.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#"> ) > 1
	        </cfquery>

			<!--- delete doc_snapshot_base --->
	        <cfquery name="delQry" datasource="#getDatasource()#">
				DELETE FROM DOC_SNAPSHOT_BASE A
				WHERE
					<cfif ArrayLen(arrSnapshots) gt 0>
						((1 = 0)
						<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
							OR A.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
						</cfloop>
						) AND
					<cfelse>
						A.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />) AND
					</cfif>
					A.DOC_SNAPSHOT_CD IN (
						SELECT DISTINCT
							PUB_LOA.DOC_SNAPSHOT_CD
						FROM
							DOC_INFO_VW CUR_LOA,
							DOC_INFO_VW PUB_LOA
						WHERE
							CUR_LOA.IS_CURRENT = 'Y'
							AND UPPER(CUR_LOA.DOC_SIGN_STATUS) = <cfqueryparam cfsqltype="cf_sql_varchar" value="OUTSTANDING">
							AND CUR_LOA.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#">
							AND CUR_LOA.STUDENT_ID = PUB_LOA.STUDENT_ID
							AND CUR_LOA.TRAINING_SESSION_CD = PUB_LOA.TRAINING_SESSION_CD
							AND CUR_LOA.DOC_SNAPSHOT_CD != PUB_LOA.DOC_SNAPSHOT_CD
							<cfif ArrayLen(arrSnapshots) gt 0>
								AND ((1 = 0)
								<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
									OR PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
								</cfloop>
								)
							<cfelse>
								AND PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
							</cfif>
					) AND
					(SELECT
			          		COUNT(B.DOC_SNAPSHOT_CD)
					 FROM
					 		DOC_SNAPSHOT_DETAIL B,
			                DOC_SNAPSHOT_BASE C
					 WHERE
			             	B.STUDENT_ID = (SELECT D.STUDENT_ID FROM DOC_SNAPSHOT_DETAIL D WHERE  D.DOC_SNAPSHOT_CD =  A.DOC_SNAPSHOT_CD AND D.TRAINING_SESSION_CD = B.TRAINING_SESSION_CD)
		             AND
			                C.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
		             AND
			                C.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#">) > 1

	        </cfquery>
		</cfif>

			<!--- publish --->
	        <cfquery name="updateQry" datasource="#getDatasource()#">
				UPDATE DOC_SNAPSHOT_BASE X
				SET
					DOC_PUBLISHED_USER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userId#">,
					DOC_PUBLISHED_DT = SYSDATE,
					DOC_CONFIRMED_USER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userId#">,
					DOC_CONFIRMED_DT = SYSDATE,
					IS_CURRENT = 'Y',
					MODIFICATION_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userId#">,
					MODIFICATION_DT = SYSDATE
				WHERE
					X.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#">
					AND X.DOC_PUBLISHED_DT IS NULL
					<cfif ArrayLen(arrSnapshots) gt 0>
						AND ((1 = 0)
						<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
							OR X.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
						</cfloop>
						)
					<cfelse>
						AND X.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
					</cfif>
					<cfif arguments.overwriteFlag EQ 'Y'>
					AND
						X.DOC_PUBLISHED_USER_ID IS NULL
					AND
						X.DOC_PUBLISHED_DT IS NULL
					</cfif>
	        </cfquery>
    </cffunction>

     <cffunction name="publishConfirmedByTemplate" access="public" output="false" returntype="void">
		<cfargument name="docLetterTemplateCD" type="numeric" required="true" />
		<cfargument name="snapshotCDList" type="string" required="true">
		<cfargument name="userId" type="numeric" required="true" />
		<cfargument name="overwriteFlag" type="string" required="false" default="N">

		<cfset var arrSnapshots = ArrayNew(1)>
		<cfset var qCount = ListLen(arguments.snapshotCDList)>
		<cfif qCount gt 1000>
		 	<cfset var qList = ''>
		 	<cfloop from="1" to="#qCount#" index="i">
				<cfset qList = ListAppend(qList, ListGetAt(arguments.snapshotCDList, i))>
			 	<cfif i mod 1000 eq 0>
				 	<cfset ArrayAppend(arrSnapshots, qList)>
				 	<cfset qList = ''>
				</cfif>
			</cfloop>
			<cfset ArrayAppend(arrSnapshots, qList)>
		</cfif>

		<!--- update existing --->
		<cfif arguments.overwriteFlag EQ 'Y'>
			<cfset var updateQry = "">
			<cfset var updateDetailQry = "">
			<cfset var delQry = "">

	        <cfquery name="updateQry" datasource="#getDatasource()#">
				MERGE INTO DOC_SNAPSHOT_DETAIL LSD
				USING
					(SELECT DISTINCT
						CUR_LOA.DOC_SNAPSHOT_CD AS CUR_DOC_SNAPSHOT_CD,
						PUB_LOA.DOC_TYPE_CD AS PUB_DOC_TYPE_CD,
						PUB_LOA.TRAINEE_TYPE_CD AS PUB_TRAINEE_TYPE_CD,
						PUB_LOA.DOC_LETTER_DATA_CD AS PUB_DOC_LETTER_DATA_CD,
						PUB_LOA.DOC_LETTER_TEMPLATE_CD AS PUB_DOC_LETTER_TEMPLATE_CD
					FROM
						DOC_INFO_VW CUR_LOA,
						DOC_INFO_VW PUB_LOA
					WHERE
						CUR_LOA.IS_CURRENT = 'Y'
						AND UPPER(CUR_LOA.DOC_SIGN_STATUS) = <cfqueryparam cfsqltype="cf_sql_varchar" value="OUTSTANDING">
						AND CUR_LOA.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#">
						AND CUR_LOA.STUDENT_ID = PUB_LOA.STUDENT_ID
						AND CUR_LOA.TRAINING_SESSION_CD = PUB_LOA.TRAINING_SESSION_CD
						AND CUR_LOA.DOC_SNAPSHOT_CD != PUB_LOA.DOC_SNAPSHOT_CD
						<cfif ArrayLen(arrSnapshots) gt 0>
							AND ((1 = 0)
							<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
								OR PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
							</cfloop>
							)
						<cfelse>
							AND PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
						</cfif>) NLSD
				ON
					(LSD.DOC_SNAPSHOT_CD = NLSD.CUR_DOC_SNAPSHOT_CD)
				WHEN MATCHED THEN
				UPDATE SET
					DOC_TYPE_CD = PUB_DOC_TYPE_CD,
					TRAINEE_TYPE_CD = PUB_TRAINEE_TYPE_CD,
					DOC_LETTER_DATA_CD = PUB_DOC_LETTER_DATA_CD,
					DOC_LETTER_TEMPLATE_CD = PUB_DOC_LETTER_TEMPLATE_CD
	        </cfquery>

			<!--- delete doc_letter_data --->
	        <!--- <cfquery name="updateDetailQry" datasource="#getDatasource()#">
	            UPDATE DOC_SNAPSHOT_DETAIL SET
					DOC_TYPE_CD = NULL,
					TRAINEE_TYPE_CD = NULL,
					DOC_LETTER_DATA_CD = NULL,
					DOC_LETTER_TEMPLATE_CD = NULL
	            WHERE
					<cfif ArrayLen(arrSnapshots) gt 0>
						(1 = 0)
						<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
							OR DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
						</cfloop>
					<cfelse>
						DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
					</cfif>
	        </cfquery> --->

			<!--- delete doc_snapshot_detail --->


	        <cfquery name="delQry" datasource="#getDatasource()#">
	            DELETE FROM DOC_SNAPSHOT_DETAIL A
	            WHERE
					<cfif ArrayLen(arrSnapshots) gt 0>
						((1 = 0)
						<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
							OR A.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
						</cfloop>
						) AND
					<cfelse>
						A.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />) AND
					</cfif>
					A.DOC_SNAPSHOT_CD IN (
						SELECT DISTINCT
							PUB_LOA.DOC_SNAPSHOT_CD
						FROM
							DOC_INFO_VW CUR_LOA,
							DOC_INFO_VW PUB_LOA
						WHERE
							CUR_LOA.IS_CURRENT = 'Y'
							AND UPPER(CUR_LOA.DOC_SIGN_STATUS) = <cfqueryparam cfsqltype="cf_sql_varchar" value="OUTSTANDING">
							AND CUR_LOA.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#">
							AND CUR_LOA.STUDENT_ID = PUB_LOA.STUDENT_ID
							AND CUR_LOA.TRAINING_SESSION_CD = PUB_LOA.TRAINING_SESSION_CD
							AND CUR_LOA.DOC_SNAPSHOT_CD != PUB_LOA.DOC_SNAPSHOT_CD
							<cfif ArrayLen(arrSnapshots) gt 0>
								AND ((1 = 0)
								<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
									OR PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
								</cfloop>
								)
							<cfelse>
								AND PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
							</cfif>
					) AND
					(SELECT
							COUNT(B.DOC_SNAPSHOT_CD)
					 FROM
					 		DOC_SNAPSHOT_DETAIL B,
			                DOC_SNAPSHOT_BASE C
			         WHERE
			         		B.STUDENT_ID = A.STUDENT_ID
			         AND
			         		B.DOC_SNAPSHOT_CD = C.DOC_SNAPSHOT_CD
			         AND
			         		B.TRAINING_SESSION_CD = A.TRAINING_SESSION_CD
			         AND
			         		B.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#"> ) > 1
	        </cfquery>

			<!--- delete doc_snapshot_base --->
	        <cfquery name="delQry" datasource="#getDatasource()#">
				DELETE FROM DOC_SNAPSHOT_BASE A
				WHERE
					<cfif ArrayLen(arrSnapshots) gt 0>
						((1 = 0)
						<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
							OR A.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
						</cfloop>
						) AND
					<cfelse>
						A.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />) AND
					</cfif>
					A.DOC_SNAPSHOT_CD IN (
						SELECT DISTINCT
							PUB_LOA.DOC_SNAPSHOT_CD
						FROM
							DOC_INFO_VW CUR_LOA,
							DOC_INFO_VW PUB_LOA
						WHERE
							CUR_LOA.IS_CURRENT = 'Y'
							AND UPPER(CUR_LOA.DOC_SIGN_STATUS) = <cfqueryparam cfsqltype="cf_sql_varchar" value="OUTSTANDING">
							AND CUR_LOA.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#">
							AND CUR_LOA.STUDENT_ID = PUB_LOA.STUDENT_ID
							AND CUR_LOA.TRAINING_SESSION_CD = PUB_LOA.TRAINING_SESSION_CD
							AND CUR_LOA.DOC_SNAPSHOT_CD != PUB_LOA.DOC_SNAPSHOT_CD
							<cfif ArrayLen(arrSnapshots) gt 0>
								AND ((1 = 0)
								<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
									OR PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
								</cfloop>
								)
							<cfelse>
								AND PUB_LOA.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
							</cfif>
					) AND
					(SELECT
			          		COUNT(B.DOC_SNAPSHOT_CD)
					 FROM
					 		DOC_SNAPSHOT_DETAIL B,
			                DOC_SNAPSHOT_BASE C
					 WHERE
			             	B.STUDENT_ID = (SELECT D.STUDENT_ID FROM DOC_SNAPSHOT_DETAIL D WHERE  D.DOC_SNAPSHOT_CD =  A.DOC_SNAPSHOT_CD AND D.TRAINING_SESSION_CD = B.TRAINING_SESSION_CD)
		             AND
			                C.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
		             AND
			                B.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#">) > 1

	        </cfquery>
		</cfif>
			<!--- publish --->
	        <cfquery name="updateQry" datasource="#getDatasource()#">
				UPDATE DOC_SNAPSHOT_BASE X
				SET
					DOC_PUBLISHED_USER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userId#">,
					DOC_PUBLISHED_DT = SYSDATE,
					DOC_CONFIRMED_USER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userId#">,
					DOC_CONFIRMED_DT = SYSDATE,
					IS_CURRENT = 'Y',
					MODIFICATION_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userId#">,
					MODIFICATION_DT = SYSDATE
				WHERE
					X.DOC_PUBLISHED_DT IS NULL
					<cfif ArrayLen(arrSnapshots) gt 0>
						AND ((1 = 0)
						<cfloop from="1" to="#ArrayLen(arrSnapshots)#" index="i">
							OR X.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSnapshots[i]#" null="0" />)
						</cfloop>
						)
					<cfelse>
						AND X.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCDList#" list="true" />)
					</cfif>
					<cfif arguments.overwriteFlag EQ 'Y'>
					AND
						X.DOC_PUBLISHED_USER_ID IS NULL
					AND
						X.DOC_PUBLISHED_DT IS NULL
					</cfif>
	        </cfquery>
    </cffunction>

	<cffunction name="publishConfirmedbySession" access="public" output="false" returntype="string">
		<cfargument name="trainingSessionCD" type="numeric" required="true" />
		<cfargument name="userId" type="numeric" required="true" />
		<cfargument name="snapshotCD" type="numeric" required="false" default="0">

        <cfset var updateQry = "">
		<!--- get students for publish --->
        <cfquery name="getQry" datasource="#getDatasource()#">
			SELECT DISTINCT
				a.doc_snapshot_cd,
				b.student_id
			FROM
				doc_snapshot_base a,
				doc_snapshot_detail b
			WHERE
				<cfif Val(arguments.snapshotCD) GT 0>
					a.doc_snapshot_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.snapshotCD#"> AND
				</cfif>
				a.doc_snapshot_cd = b.doc_snapshot_cd AND
				a.doc_published_dt IS NULL AND
				b.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#">
		</cfquery>
		<!--- publish --->
        <cfquery name="updateQry" datasource="#getDatasource()#">
			UPDATE doc_snapshot_base x
			SET
				doc_published_user_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userId#">,
				doc_published_dt = SYSDATE,
				is_current = 'Y',
				modification_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userId#">,
				modification_dt = SYSDATE
			WHERE
				<cfif Val(arguments.snapshotCD) GT 0>
					x.doc_snapshot_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.snapshotCD#"> AND
				</cfif>
				x.doc_published_dt IS NULL AND
				x.doc_snapshot_cd in (select doc_snapshot_cd from doc_snapshot_detail where doc_snapshot_cd = x.doc_snapshot_cd and training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCD#">)
        </cfquery>
		<cfreturn ValueList(getQry.student_id)>
    </cffunction>

	<cffunction name="getTrainees4Notification" access="public" output="false" returntype="query">
		<cfargument name="filterData" type="sis_core.model.SearchFilter" required="true" />
		<cfset var getQry = ""/>

		<cfquery name="getQry" datasource="#getDatasource()#">
		    WITH X AS
			(SELECT
				A.DOC_SNAPSHOT_CD,
				B.DOC_DT,
				B.TYPE_CODE,
				B.DOC_LETTER_TYPE_CD,
				D.EMAIL_DT,
				D.LAST_REMINDER_DT,
				D.CNT_NOTIFICATION,
				G.EMAIL,
				C.STUDENT_NUMBER,
				'Dr. ' || INITCAP(C.FIRST_NAME || ' ' || C.LAST_NAME) AS USER_NAME,
				E.NOTI_FREQUENCY,
				E.NOTIF_MAX_REMINDERS,
				F.DOC_DUE_DAYS,
				G.USER_ID,
				G.PIN AS USER_PIN,
				DECODE((SELECT DISTINCT USER_ID FROM UD_LOGIN_HISTORY WHERE USER_ID = G.USER_ID AND FAILED_LOGIN = 'N'),G.USER_ID,'Y','N') AS LOGGED_IN_BEFORE,
				G.PASSWORD AS USER_PASSWD,
				B.DOC_SIGN_STATUS,
				B.LETTER_TYPE_CODE
			FROM
				DOC_CURRENT_VW A,
				DOC_INFO_VW B,
				STUDENT C,
				DOC_SNAPSHOT_NOTIFICATION D,
				DOC_EMAIL_SETTINGS E,
				DOC_SETTINGS F,
				UD_USER G,
		        STD_REGISTRATION H,
		        REGISTRATION_STATUS I,
				STUDENT_USER J
			WHERE
				A.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filterData.getFILTER_ITEM('INSTANCE_ID')#"> AND
				A.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.filterData.getFILTER_ITEM('TRAINING_SESSION_CD')#"> AND
				A.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD AND
				B.DOC_SUBMITTED_DT IS NULL AND
				B.STUDENT_ID = C.STUDENT_ID AND
				A.DOC_SNAPSHOT_CD = D.DOC_SNAPSHOT_CD (+) AND
				TO_CHAR(SYSDATE,'YYYYMMDD') >= TO_CHAR(B.DOC_DT,'YYYYMMDD') AND
				C.STUDENT_ID = J.STUDENT_ID AND
				J.USER_ID = G.USER_ID AND
				H.STUDENT_ID = C.STUDENT_ID AND
		        H.TRAINING_SESSION_CD = A.TRAINING_SESSION_CD AND
		        I.REGISTRATION_STATUS_CD = H.REGISTRATION_STATUS_CD AND
		        A.INSTANCE_ID = F.INSTANCE_ID AND
		        A.INSTANCE_ID = E.INSTANCE_ID
		    )
		    SELECT DISTINCT X.*
		    FROM X
			WHERE
				<cfif arguments.filterData.getFILTER_ITEM("EMAIL_TYPE_CODE") eq "INVITATION">
					X.EMAIL_DT IS NULL
				<cfelseif arguments.filterData.getFILTER_ITEM("EMAIL_TYPE_CODE") eq "REMINDER">
					X.EMAIL_DT IS NOT NULL AND
					NVL(X.CNT_NOTIFICATION,0) < X.NOTIF_MAX_REMINDERS AND
					TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD'),'YYYYMMDD') - TO_DATE(TO_CHAR(NVL(X.LAST_REMINDER_DT,X.EMAIL_DT),'YYYYMMDD'),'YYYYMMDD') >= X.NOTI_FREQUENCY AND
					TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD'),'YYYYMMDD') - TO_DATE(TO_CHAR(X.EMAIL_DT,'YYYYMMDD'),'YYYYMMDD') <= NVL(X.DOC_DUE_DAYS,0)
				<cfelseif arguments.filterData.getFILTER_ITEM("EMAIL_TYPE_CODE") eq "EXPIRED">
					X.EMAIL_DT IS NOT NULL AND
					NVL(X.CNT_NOTIFICATION,0) < X.NOTIF_MAX_REMINDERS AND
					TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD'),'YYYYMMDD') - TO_DATE(TO_CHAR(NVL(X.LAST_REMINDER_DT,X.EMAIL_DT),'YYYYMMDD'),'YYYYMMDD') >= X.NOTI_FREQUENCY AND
					TO_DATE(TO_CHAR(SYSDATE,'YYYYMMDD'),'YYYYMMDD') - TO_DATE(TO_CHAR(X.EMAIL_DT,'YYYYMMDD'),'YYYYMMDD') > NVL(X.DOC_DUE_DAYS,0)
				<cfelse>
					1 = 0
				</cfif>
				<cfif arguments.filterData.getFILTER_ITEM("TYPE_CODE") NEQ "">
					AND UPPER(X.TYPE_CODE) = <cfqueryparam cfsqltype="cf_sql_varchar" value="#uCase(arguments.filterData.getFILTER_ITEM('TYPE_CODE'))#">
				</cfif>
				<cfif arguments.filterData.getFILTER_ITEM("LETTER_TYPE_CODE") NEQ "">
					AND UPPER(X.LETTER_TYPE_CODE) = <cfqueryparam cfsqltype="cf_sql_varchar" value="#uCase(arguments.filterData.getFILTER_ITEM('LETTER_TYPE_CODE'))#">
				</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getGeneratedLOA" access="public" output="false" returntype="query">
		<cfargument name="instanceID" type="numeric" required="true">
		<cfargument name="stdID" type="numeric" required="true">
		<cfargument name="trSessCD" type="numeric" required="true">
		<cfargument name="letterTemplateCDs" type="string" required="false" default="*">

		<cfset var getQry = ""/>

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT a.*, b.DOC_LETTER_TEMPLATE_CD
			FROM
				doc_snapshot_base a,
				doc_snapshot_detail b
			WHERE
				a.instance_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#"> AND
				a.doc_snapshot_cd = b.doc_snapshot_cd AND
				<cfif arguments.stdID neq -1>
					b.student_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.stdID#"> AND
				</cfif>
				b.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
				a.doc_published_dt IS null
				<cfif arguments.letterTemplateCDs neq "*">
					and b.doc_letter_template_cd in (<cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.letterTemplateCDs#">)
				</cfif>
		</cfquery>

		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getGeneratedLOAByTemplate" access="public" output="false" returntype="query">
		<cfargument name="docLetterTemplateCD" type="numeric" required="true">
		<cfargument name="userID" type="numeric" required="true">
		<cfargument name="trSessCD" type="numeric" required="true">

		<cfset var getQry = ""/>

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT a.*
			FROM
				doc_snapshot_base a,
				doc_snapshot_detail b
			WHERE
				b.doc_letter_template_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#"> AND
				a.doc_snapshot_cd = b.doc_snapshot_cd AND
				<cfif arguments.userID neq -1>
					a.user_id = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userID#"> AND
				</cfif>
				b.training_session_cd = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trSessCD#"> AND
				a.doc_published_dt IS null
		</cfquery>

		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getRealSQL" returntype="string" output="true" description="Return the query original SQL with parameters expanded;" hint="you can pass either the query object or the result object">
	    <cfargument name="qryResult" type="any">
	    <!---alow either query object or result object--->
	    <cfif arguments.qryResult.getClass().getName() eq "coldfusion.runtime.Struct" and StructKeyExists(arguments.qryResult, "sql")>
	        <cfset var realSQL = arguments.qryResult.sql>
	        <cfset var sqlParameters =   arguments.qryResult.sqlParameters>
	    <cfelseif arguments.qryResult.getClass().getName() eq "coldfusion.sql.QueryTable">
	        <cfset var metaData = arguments.qryResult.getMetaData().getExtendedMetaData()>
	        <cfset var realSQL = metaData.sql>
	        <cfif StructKeyExists(metaData,'sqlParameters')>
	            <cfset var sqlParameters = metaData.sqlParameters>
	        <cfelse>
	            <cfset var sqlParameters = []>
	        </cfif>
	    <cfelse>
	        <cfthrow message="Could not parse the query">
	    </cfif>

	    <cfset var dtRegExp = "\'(\{ts\s\'.*\'\})\'">
	    <cfset var OleDateTime = CreateObject("java","coldfusion.runtime.OleDateTime")>
	    <cfloop array="#sqlParameters#" index="a">
	        <cfscript>
	            if (NOT isNumeric(a)) {
	                a = "'#a#'";
	            }
	            var regInfo = ReFindNoCase(dtRegExp,a,1,true);
	            if(ArrayLen(regInfo["len"]) eq 2 and regInfo["len"][2] gt 0) {
	                var dateString = Mid(a,regInfo["pos"][2],regInfo["len"][2]);
	                var dateParam = OleDateTime.parseCrazyDate(dateString);//static method
	                if (IsDate(dateParam)) {
	                    a= "to_date('#DateFormat(dateParam,"DD-MMM-YYYY")#')";
	                }
	            }
	            realSQL = Replace(realSQL, "?", a);
	        </cfscript>
	    </cfloop>
	    <cfreturn realSQL>
	</cffunction>

	<cffunction name="getMetadataInfo" access="public" output="false" returntype="query">
		<cfargument name="userID" type="numeric" required="true" />
		<cfargument name="docLetterTemplateCD" type="numeric" required="true" />
		<cfargument name="contextualKey" type="string" required="true" hint="Primary key of the entity, must be passed in as KEY1=VAL1+KEY2=VAL2+..." />
		<cfargument name="additionalFields" type="string" required="false" default="" hint="Additional fields that are not part of the primary key, defined as a simple list" />

		<cfset var getQry = ""/>
		<cfset var cnt = 0 />

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				X.*,
			  	Y.LETTER_TYPE_DESC,
			  	Y.DOC_LETTER_TYPE_CD,
			  	Y.DOC_TYPE_CD,
			  	Y.DOC_LETTER_DATA_CD,
			  	Y.DOC_LETTER_TEMPLATE_CD,
			  	Y.DOC_SIGNATURE,
			  	Y.DOC_DT,
			  	Y.DOC_SUBMITTED_DT,
			  	Y.LAST_REMINDER_DT,
			  	Y.DOC_DUE_DATE,
			  	Y.CNT_NOTIFICATION,
			  	Y.DOC_FILE_CD,
			  	Y.DOC_PROOF_FILE_CD,
			  	Y.DOC_PUBLISHED_DT,
			  	Y.DOC_CONFIRMED_DT,
			  	Y.DOC_SIGN_STATUS,
			  	Y.TYPE_DESC,
			  	Y.TEMPLATE_REVISED_DATE
			FROM (
				SELECT * FROM
				TABLE( PIVOT(  '
					SELECT *
					FROM DOC_SNAPSHOT_METADATA WHERE DOC_SNAPSHOT_CD IN (
					<cfloop list="#arguments.contextualKey#" index="key" delimiters="+">
					<cfset cnt++ />
					SELECT DOC_SNAPSHOT_CD FROM DOC_SNAPSHOT_METADATA WHERE
   						FIELD_NAME = ''#UCase(ListFirst(key, '='))#'' AND FIELD_VALUE = ''#UCase(ListLast(key, '='))#''
   					<cfif cnt neq ListLen(arguments.contextualKey, "+")>
   					INTERSECT
   					</cfif>
					</cfloop> )
				' ) )
			) X,
			DOC_INFO_VW Y,
			DOC_SNAPSHOT_BASE Z
			WHERE
				X.DOC_SNAPSHOT_CD = Y.DOC_SNAPSHOT_CD
				AND Y.DOC_SNAPSHOT_CD = Z.DOC_SNAPSHOT_CD
				AND Y.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#" />
				AND Z.USER_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.userID#" />
		</cfquery>

		<cfreturn getQry />
	</cffunction>
</cfcomponent>
