<cfcomponent name="DocInstanceGTW" displayname="DocInstanceGTW" output="false" hint="" extends="dbObject">

	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfargument name="isUsed" type="any" required="false" default="false">
		<cfargument name="applyRestrictions" type="any" required="false" default="true">

		<cfset var getQry = ""/>
		<cfset var instanceResList = "-1">

		<cfif arguments.applyRestrictions>
			<cfset EntityRestrictions =  application.wirebox.getInstance(name='sis_core.model.blAutomation.EntityRestrictions')/>
			<cfset userSessionData = application.wirebox.getInstance(dsl='coldbox:plugin:SessionStorage' ).getVar("LoggedInUser")>
		 	<cfset instanceRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_INSTANCE',userId=userSessionData.USER_ID,securityCode="DOCUMENT_BUILDER_MODULE") />
			<cfif instanceRestrictionSql NEQ "*">
				<cfset instanceResList = ListAppend(instanceResList,instanceRestrictionSql)>
			</cfif>
		<cfelse>
				<cfset instanceResList = "" />
		</cfif>

		<!---
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>
		--->
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				LI.INSTANCE_ID,
				LI.INSTANCE_NAME,
				LI.INSTANCE_CODE,
				LI.ACCESS_NODE,
				LI.TEMPLATE_ENTITIES,
				NVL2(LI.TEMPLATE_ENTITIES, 'Y', 'N') AS HAS_ENTITIES,
				LI.RESTRICTION_NODES,
				LI.HEADER_CONTENT,
				LI.REFERS_TO,
				LI.TAGS,
				NVL(LS.AUTO_GENERATE, 'N') AS AUTO_GENERATE,
				NVL(LS.ENABLE_PUBLISH, 'N') AS ENABLE_PUBLISH,
				NVL(LS.ATTACH_USER_PICTURE, 'N') AS ATTACH_USER_PICTURE,
				ENTITY_CODE
			FROM
				DOC_INSTANCE LI,
				DOC_SETTINGS LS
			WHERE
				LI.INSTANCE_ID = LS.INSTANCE_ID (+)
				<cfif arguments.isUsed>
					AND EXISTS (SELECT 1 FROM DOC_SETUP S,DOC_SETUP_ELIGIBLE_USERS EU WHERE S.DOC_SETUP_ID = EU.DOC_SETUP_ID  AND S.INSTANCE_ID = LI.INSTANCE_ID)
				</cfif>
				<cfif instanceResList neq "" and instanceResList neq "*">
					AND LI.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
				</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				LI.INSTANCE_ID,
				LI.INSTANCE_NAME,
				LI.INSTANCE_CODE,
				LI.ACCESS_NODE,
				LI.TEMPLATE_ENTITIES,
				LI.INSTANCE_CONFIG,
				NVL2(LI.TEMPLATE_ENTITIES, 'Y', 'N') AS HAS_ENTITIES,
				LI.RESTRICTION_NODES,
				LI.HEADER_CONTENT,
				LI.REFERS_TO,
				LI.TAGS,
				LI.ENTITY_CODE,
				LLA.ACTION_CODE,
				NVL(LS.AUTO_GENERATE, 'N') AS AUTO_GENERATE,
				NVL(LS.ENABLE_PUBLISH, 'N') AS ENABLE_PUBLISH,
				NVL(LS.ATTACH_USER_PICTURE, 'N') AS ATTACH_USER_PICTURE
			FROM
				DOC_INSTANCE LI,
				DOC_SETTINGS LS,
				DOC_LETTER_ACTION LLA
			WHERE
				LI.INSTANCE_ID = LS.INSTANCE_ID (+)
				AND LS.ACTION_ID = LLA.ACTION_ID (+)
				AND LI.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByCode" access="public" returntype="query" output="false" displayname="getByCode" hint="">
		<cfargument name="instanceCode" type="string" required="true" />

		<cfset var getQry = ""/>
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				LI.INSTANCE_ID,
				LI.INSTANCE_NAME,
				LI.INSTANCE_CODE,
				LI.ACCESS_NODE,
				LI.TEMPLATE_ENTITIES,
				NVL2(LI.TEMPLATE_ENTITIES, 'Y', 'N') AS HAS_ENTITIES,
				LI.RESTRICTION_NODES,
				LI.HEADER_CONTENT,
				LI.REFERS_TO,
				LI.TAGS,
				LI.ENTITY_CODE,
				NVL(LS.AUTO_GENERATE, 'N') AS AUTO_GENERATE,
				NVL(LS.ENABLE_PUBLISH, 'N') AS ENABLE_PUBLISH,
				NVL(LS.ATTACH_USER_PICTURE, 'N') AS ATTACH_USER_PICTURE
			FROM
				DOC_INSTANCE LI,
				DOC_SETTINGS LS
			WHERE
				LI.INSTANCE_ID = LS.INSTANCE_ID (+)
				AND UPPER(LI.INSTANCE_CODE) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#uCase(arguments.instanceCode)#" />
				<cfif instanceResList neq "" and instanceResList neq "*">
					AND LI.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
				</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByIdList" access="public" returntype="query" output="false" displayname="getByIdList" hint="">
		<cfargument name="instanceID" type="string" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				LI.INSTANCE_ID,
				LI.INSTANCE_NAME,
				LI.INSTANCE_CODE,
				LI.ACCESS_NODE,
				LI.TEMPLATE_ENTITIES,
				NVL2(LI.TEMPLATE_ENTITIES, 'Y', 'N') AS HAS_ENTITIES,
				LI.RESTRICTION_NODES,
				LI.HEADER_CONTENT,
				LI.REFERS_TO,
				LI.TAGS,
				LI.ENTITY_CODE,
				NVL(LS.AUTO_GENERATE, 'N') AS AUTO_GENERATE,
				NVL(LS.ENABLE_PUBLISH, 'N') AS ENABLE_PUBLISH,
				NVL(LS.ATTACH_USER_PICTURE, 'N') AS ATTACH_USER_PICTURE
			FROM
				DOC_INSTANCE LI,
				DOC_SETTINGS LS
			WHERE
				LI.INSTANCE_ID = LS.INSTANCE_ID (+)
				<cfif arguments.instanceID neq "*" AND arguments.instanceID neq "" AND arguments.instanceID GT 0>
					AND LI.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" list="true" value="#arguments.instanceID#" />)
				</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getTagsByChangeType" access="public" returntype="query" output="false" displayname="getTagsByChangeType" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="changeTypeCode" type="string" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				A.*,
				B.TAGS AS REVISION_TAGS,
				C.DOC_CHANGE_CODE
			FROM
				DOC_INSTANCE A,
				DOC_REVISION_TAGS B,
				DOC_CHANGE_TYPE C
			WHERE
				A.INSTANCE_ID = B.INSTANCE_ID
				AND B.DOC_CHANGE_TYPE_CD = C.DOC_CHANGE_TYPE_CD
				AND A.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#" />
				AND C.DOC_CHANGE_CODE = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.changeTypeCode#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

</cfcomponent>