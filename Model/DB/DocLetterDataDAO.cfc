<cfcomponent name="DocLetterDataDAO" displayname="DocLetterDataDAO" hint="I abstract data access for DOC_LETTER_DATA" extends="dbObject">
	<cffunction name="read" returntype="DocLetterDataVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="docLetterDataCD" type="numeric" required="true" />

		<cfset var DocLetterDataVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_LETTER_DATA_CD,
				XML_LETTER_CONTENT,
				CREATION_DT
			FROM DOC_LETTER_DATA
			WHERE
				DOC_LETTER_DATA_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docLetterDataCD#"/>
		</cfquery>
		<cfset DocLetterDataVO = CreateObject("component","DocLetterDataVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset DocLetterDataVO.setDOC_LETTER_DATA_CD(getQry.DOC_LETTER_DATA_CD) />
			<cfset DocLetterDataVO.setXML_LETTER_CONTENT(getQry.XML_LETTER_CONTENT) />
			<cfset DocLetterDataVO.setCREATION_DT(getQry.CREATION_DT) />
		</cfif>

		<cfreturn DocLetterDataVO />
	</cffunction>

	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterDataVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">

		<cfquery name="seqQry" datasource="#getDatasource()#">
			SELECT DOC_LETTER_DATA_SEQ.nextval AS nextval
			FROM dual
		</cfquery>

		<cfquery name="addQry" datasource="#getDatasource()#">
			INSERT INTO DOC_LETTER_DATA (
				DOC_LETTER_DATA_CD ,XML_LETTER_CONTENT ,CREATION_DT)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_clob" value="#arguments.bean.getXML_LETTER_CONTENT()#" null="#iif((arguments.bean.getXML_LETTER_CONTENT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />)
		</cfquery>

		<cfreturn seqQry.nextval />
	</cffunction>

	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterDataVO" required="true" />
		<cfset var updateQry = "" />

		<cfquery name="updateQry" datasource="#getDatasource()#">
			UPDATE	DOC_LETTER_DATA
			SET
				XML_LETTER_CONTENT = <cfqueryparam value="#arguments.bean.getXML_LETTER_CONTENT()#" cfsqltype="cf_sql_clob" null="#iif((arguments.bean.getXML_LETTER_CONTENT() eq ''),de("true"), de("false"))#" />
			WHERE
				DOC_LETTER_DATA_CD = <cfqueryparam value="#arguments.bean.getDOC_LETTER_DATA_CD()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="docLetterDataCD" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE FROM	DOC_LETTER_DATA
			WHERE
				DOC_LETTER_DATA_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterDataCD#"/>
		</cfquery>
	</cffunction>

</cfcomponent>