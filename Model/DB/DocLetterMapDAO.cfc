<cfcomponent name="DocLetterMapDAO" displayname="DocLetterMapDAO" hint="I abstract data access for DOC_LETTER_MAP" extends="dbObject">
	<cffunction name="read" returntype="DocLetterMapVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="docTypeCD" type="numeric" required="true" />
		<cfset var DocLetterMapVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_TYPE_CD,
				DOC_LETTER_TEMPLATE_CD,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_LETTER_MAP
			WHERE
				DOC_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docTypeCD#"/>
		</cfquery>
		<cfset DocLetterMapVO = CreateObject("component","DocLetterMapVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset DocLetterMapVO.setDOC_TYPE_CD(getQry.DOC_TYPE_CD) />
			<cfset DocLetterMapVO.setDOC_LETTER_TEMPLATE_CD(getQry.DOC_LETTER_TEMPLATE_CD) />
			<cfset DocLetterMapVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset DocLetterMapVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset DocLetterMapVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset DocLetterMapVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
		</cfif>
		<cfreturn DocLetterMapVO />
	</cffunction>

	<cffunction name="add" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterMapVO" required="true" />
		<cfset var addQry = "">
		<cfquery name="addQry" datasource="#getDatasource()#">
			INSERT INTO DOC_LETTER_MAP (
				DOC_TYPE_CD ,DOC_LETTER_TEMPLATE_CD ,CREATION_ID ,CREATION_DT)
			VALUES (<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_TYPE_CD()#" maxLength="22" null="false" />,
					<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_LETTER_TEMPLATE_CD()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />)
		</cfquery>
	</cffunction>

	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterMapVO" required="true" />
		<cfset var updateQry = "" />
		<cfquery name="updateQry" datasource="#getDatasource()#">
			UPDATE	DOC_LETTER_MAP
			SET
				DOC_LETTER_TEMPLATE_CD = <cfqueryparam value="#arguments.bean.getDOC_LETTER_TEMPLATE_CD()#" cfsqltype="cf_sql_numeric" maxLength="22" null="false" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />
			WHERE
				DOC_TYPE_CD = <cfqueryparam value="#arguments.bean.getDOC_TYPE_CD()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="docTypeCD" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE FROM	DOC_LETTER_MAP
			WHERE
				DOC_TYPE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docTypeCD#"/>
		</cfquery>
	</cffunction>

</cfcomponent>
