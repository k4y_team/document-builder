<cfcomponent name="DocLetterMapGTW" displayname="DocLetterMapGTW" output="false" hint="" extends="dbObject">

	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="false">
		<cfargument name="data" type="sis_core.model.SortOptions" required="true"/>

		<cfset pgDSN = application.wirebox.getInstance(dsl="coldbox:setting:pg_datasource") />

		<cfset var getQry = ""/>
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				*
			FROM
				(SELECT
					Z.*
					<cfif arguments.filter.getTABLE_PAGE().getRECORDS_PER_PAGE() GT 0 AND arguments.filter.getTABLE_PAGE().getPAGE_NO()>
						,ROWNUM AS MYROWNUM
					</cfif>
				FROM
					(SELECT
						Y.*
						<cfif arguments.filter.getTABLE_PAGE().getRECORDS_PER_PAGE() GT 0 AND arguments.filter.getTABLE_PAGE().getPAGE_NO()>
							,CEIL(Y.TOTAL_RECORDS / #arguments.filter.getTABLE_PAGE().getRECORDS_PER_PAGE()#) AS TOTAL_PAGES
						</cfif>
					FROM
						(SELECT
							XX.DOC_TYPE_CD,
							XX.CREATION_ID,
							XX.CREATION_DT,
							XX.MODIFICATION_ID,
							XX.MODIFICATION_DT,
							XX.TYPE_DESC,
							COUNT(*) OVER() AS TOTAL_RECORDS,
							XX.INSTANCE_ID,
							XX.INSTANCE_NAME,
							STRING_AGG(XX.LETTER_TEMPLATE_NAME) AS TEMPLATE_LIST
						FROM
							(SELECT
								C.DOC_TYPE_CD,
								A.DOC_LETTER_TEMPLATE_CD,
								A.CREATION_ID,
								A.CREATION_DT,
								A.MODIFICATION_ID,
								A.MODIFICATION_DT,
								B.LETTER_TEMPLATE_NAME,
								C.TYPE_DESC,
								D.DESCRIPTION,
								C.INSTANCE_ID,
								LI.INSTANCE_NAME
							FROM
								DOC_LETTER_MAP A,
								DOC_LETTER_TEMPLATE B,
								DOC_TYPE C,
								(SELECT
									LSD.*,
									TS.TRAINING_SESSION_NAME AS DESCRIPTION
								FROM
									 #pgDSN#.TRAINING_SESSION TS,
									(SELECT
										X.DOC_LETTER_TEMPLATE_CD,
										MAX(X.TRAINING_SESSION_CD) AS TRAINING_SESSION_CD
									FROM
										DOC_SNAPSHOT_DETAIL X
									GROUP BY
										X.DOC_LETTER_TEMPLATE_CD) LSD
								WHERE
									TS.TRAINING_SESSION_CD=LSD.TRAINING_SESSION_CD) D,
								DOC_INSTANCE LI
							WHERE
								<cfif arguments.filter.getFILTER_ITEM("LETTERTYPE") neq "">
									C.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM("LETTERTYPE")#" /> AND
								</cfif>
								C.INSTANCE_ID = LI.INSTANCE_ID AND
								<cfif arguments.filter.getFILTER_ITEM("SEARCH_STRING") neq "">
								(
									UPPER(C.TYPE_DESC) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#UCase(arguments.filter.getFILTER_ITEM("SEARCH_STRING"))#%" /> OR
									UPPER(B.LETTER_TEMPLATE_NAME) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#UCase(arguments.filter.getFILTER_ITEM("SEARCH_STRING"))#%" />
								) AND
								</cfif>
								B.DOC_LETTER_TEMPLATE_CD=D.DOC_LETTER_TEMPLATE_CD(+)
								AND A.DOC_LETTER_TEMPLATE_CD=B.DOC_LETTER_TEMPLATE_CD(+)
								AND C.DOC_TYPE_CD = A.DOC_TYPE_CD(+)
								<cfif instanceResList neq "" and instanceResList neq "*">
									AND B.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
								</cfif>
							) XX
						GROUP BY
							DOC_TYPE_CD,
							CREATION_ID,
							CREATION_DT,
							MODIFICATION_ID,
							MODIFICATION_DT,
							TYPE_DESC,
							INSTANCE_ID,
							INSTANCE_NAME
						) Y
					ORDER BY
					<cfif arguments.filter.getSORT_OPTIONS().getField() neq "">
						#arguments.filter.getSORT_OPTIONS().getSortString_UC()#
					<cfelse>
						UPPER(TYPE_DESC)
					</cfif>
				) Z
			) W
			<cfif arguments.filter.getTABLE_PAGE().getRECORDS_PER_PAGE() gt 0 and arguments.filter.getTABLE_PAGE().getPAGE_NO() gt 0>
				WHERE W.MYROWNUM BETWEEN #arguments.filter.getTABLE_PAGE().getSTART_ROW()# AND #arguments.filter.getTABLE_PAGE().getEND_ROW()#
			</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="docTypeCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_TYPE_CD,
				DOC_LETTER_TEMPLATE_CD,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_LETTER_MAP
			WHERE
				DOC_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docTypeCD#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByDesc" access="public" returntype="query" output="false" displayname="getByDesc" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="docTypeDesc" type="string" required="true" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
			  LM.*, LT.*
			FROM
			  DOC_LETTER_MAP LM,
			  DOC_TYPE LT
			WHERE
			  LM.DOC_TYPE_CD = LT.DOC_TYPE_CD
			  AND UPPER(LT.TYPE_DESC) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#UCase(arguments.docTypeDesc)#" />
			  AND LT.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
		</cfquery>

		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByTypeCD" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="docTypeCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				C.DOC_TYPE_CD,
				A.DOC_LETTER_TEMPLATE_CD,
				A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT,
				B.LETTER_TEMPLATE_NAME,
				C.TYPE_DESC,
				C.RESTRICTION_CONDITION_ID,
				C.INSTANCE_ID
			FROM DOC_LETTER_MAP A,
				 DOC_LETTER_TEMPLATE B,
				 DOC_TYPE C
			WHERE
					A.DOC_LETTER_TEMPLATE_CD=B.DOC_LETTER_TEMPLATE_CD(+)
				AND C.DOC_TYPE_CD = A.DOC_TYPE_CD(+)
				AND C.DOC_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docTypeCD#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByTemplateCD" access="public" returntype="query" output="false" displayname="getByTemplateCDList" hint="">
		<cfargument name="docLetterTemplateCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				C.DOC_TYPE_CD,
				A.DOC_LETTER_TEMPLATE_CD,
				A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT,
				B.LETTER_TEMPLATE_NAME,
				C.TYPE_DESC,
				C.RESTRICTION_CONDITION_ID,
				C.INSTANCE_ID
			FROM DOC_LETTER_MAP A,
				 DOC_LETTER_TEMPLATE B,
				 DOC_TYPE C
			WHERE
				A.DOC_LETTER_TEMPLATE_CD=B.DOC_LETTER_TEMPLATE_CD(+)
				AND C.DOC_TYPE_CD = A.DOC_TYPE_CD(+)
				AND A.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docLetterTemplateCD#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

</cfcomponent>