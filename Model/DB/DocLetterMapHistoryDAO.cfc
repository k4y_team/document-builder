<cfcomponent name="DocLetterMapHistoryDAO" displayname="DocLetterMapHistoryDAO" hint="I abstract data access for DOC_LETTER_MAP_HISTORY" extends="dbObject">
	<cffunction name="read" returntype="DocLetterMapHistoryVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="docLetterMapHistoryCD" type="numeric" required="true" />

		<cfset var DocLetterMapHistoryVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_LETTER_MAP_HISTORY_CD,
				DOC_TYPE_CD,
				DOC_TEMPLATE_CD,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_LETTER_MAP_HISTORY
			WHERE
				DOC_LETTER_MAP_HISTORY_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docLetterMapHistoryCD#"/>
		</cfquery>
		<cfset DocLetterMapHistoryVO = CreateObject("component","DocLetterMapHistoryVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset DocLetterMapHistoryVO.setDOC_LETTER_MAP_HISTORY_CD(getQry.DOC_LETTER_MAP_HISTORY_CD) />
			<cfset DocLetterMapHistoryVO.setDOC_TYPE_CD(getQry.DOC_TYPE_CD) />
			<cfset DocLetterMapHistoryVO.setDOC_TEMPLATE_CD(getQry.DOC_TEMPLATE_CD) />
			<cfset DocLetterMapHistoryVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset DocLetterMapHistoryVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset DocLetterMapHistoryVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset DocLetterMapHistoryVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
		</cfif>

		<cfreturn DocLetterMapHistoryVO />
	</cffunction>

	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterMapHistoryVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">

		<cfquery name="seqQry" datasource="#getDatasource()#">
			SELECT DOC_LETTER_MAP_HISTORY_SEQ.nextval AS nextval
			FROM dual
		</cfquery>

		<cfquery name="addQry" datasource="#getDatasource()#">
			INSERT INTO DOC_LETTER_MAP_HISTORY (
				DOC_LETTER_MAP_HISTORY_CD ,DOC_TYPE_CD ,DOC_TEMPLATE_CD ,CREATION_ID ,CREATION_DT)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_TYPE_CD()#" maxLength="22" null="#iif((arguments.bean.getDOC_TYPE_CD() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_TEMPLATE_CD()#" maxLength="22" null="#iif((arguments.bean.getDOC_TEMPLATE_CD() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />)
		</cfquery>

		<cfreturn seqQry.nextval />
	</cffunction>

	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterMapHistoryVO" required="true" />
		<cfset var updateQry = "" />

		<cfquery name="updateQry" datasource="#getDatasource()#">
			UPDATE	DOC_LETTER_MAP_HISTORY
			SET
				DOC_TYPE_CD = <cfqueryparam value="#arguments.bean.getDOC_TYPE_CD()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDOC_TYPE_CD() eq ''),de("true"), de("false"))#" />,
				DOC_TEMPLATE_CD = <cfqueryparam value="#arguments.bean.getDOC_TEMPLATE_CD()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDOC_TEMPLATE_CD() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />
			WHERE
				DOC_LETTER_MAP_HISTORY_CD = <cfqueryparam value="#arguments.bean.getDOC_LETTER_MAP_HISTORY_CD()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="DOC_TYPE_CD" type="numeric" required="true" />
		<cfargument name="DOC_TEMPLATE_CD" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE FROM	DOC_LETTER_MAP_HISTORY
			WHERE
				DOC_TYPE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.DOC_TYPE_CD#"/>
			AND DOC_TEMPLATE_CD=<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.DOC_TEMPLATE_CD#"/>
		</cfquery>
	</cffunction>

	<cffunction name="deleteByDocType" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="DOC_TYPE_CD" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE FROM	DOC_LETTER_MAP_HISTORY
			WHERE
				DOC_TYPE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.DOC_TYPE_CD#"/>
		</cfquery>
	</cffunction>

</cfcomponent>