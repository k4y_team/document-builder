<cfcomponent name="DocLetterMapHistoryGTW" displayname="DocLetterMapHistoryGTW" output="false" hint="" extends="dbObject">
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_LETTER_MAP_HISTORY_CD,
				DOC_TYPE_CD,
				DOC_TEMPLATE_CD,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_LETTER_MAP_HISTORY
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="docLetterMapHistoryCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_LETTER_MAP_HISTORY_CD,
				DOC_TYPE_CD,
				DOC_TEMPLATE_CD,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_LETTER_MAP_HISTORY
			WHERE
				DOC_LETTER_MAP_HISTORY_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docLetterMapHistoryCD#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getByDOC_TYPE_CD" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="DOC_TYPE_CD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				distinct(LMH.DOC_TEMPLATE_CD),
				LLT.LETTER_Template_NAME,
				LSD.DESCRIPTION,
				U.LAST_NAME||', '||U.FIRST_NAME as UsedBy,
				LMH.CREATION_DT
					FROM DOC_LETTER_MAP_HISTORY LMH,
        			 DOC_LETTER_TEMPLATE LLT,
        			 UD_USER U,
					(
						SELECT
							DISTINCT(LSD.TRAINING_SESSION_CD) ,
							DOC_LETTER_TEMPLATE_CD,TS.TRAINING_SESSION_NAME AS DESCRIPTION
        				FROM DOC_SNAPSHOT_DETAIL LSD,
        					 TRAINING_SESSION TS
                		WHERE TS.TRAINING_SESSION_CD = LSD.TRAINING_SESSION_CD
                	)LSD
  			 	WHERE LSD.DOC_LETTER_TEMPLATE_CD=LLT.DOC_LETTER_TEMPLATE_CD
         			AND LLT.DOC_LETTER_TEMPLATE_CD=LMH.DOC_TEMPLATE_CD
         			AND LLT.MODIFICATION_ID=U.USER_ID
         			AND LMH.DOC_TYPE_CD= <CFQUERYPARAM CFSQLTYPE="CF_SQL_NUMERIC" VALUE="#ARGUMENTS.DOC_TYPE_CD#" />
         			ORDER BY LMH.CREATION_DT DESC
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
</cfcomponent>
