<cfcomponent name="DocLetterMapVO" displayname="DocLetterMapVO" extends="getSetObjectValue" output="false">
		<cfproperty name="DOC_TYPE_CD" type="numeric" />
		<cfproperty name="DOC_LETTER_TEMPLATE_CD" type="numeric" />
		<cfproperty name="CREATION_ID" type="numeric" />
		<cfproperty name="CREATION_DT" type="date" />
		<cfproperty name="MODIFICATION_ID" type="numeric" />
		<cfproperty name="MODIFICATION_DT" type="date" />

	<cffunction name="init" access="public" returntype="DocLetterMapVO" output="false" displayname="init" hint="I initialize a DocLetterMap">
		<cfset super.init() />
		<cfset variables.DOC_TYPE_CD = "" />
		<cfset variables.DOC_LETTER_TEMPLATE_CD = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />

		<cfreturn this />
 	</cffunction>

	<cffunction name="setDOC_TYPE_CD" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.DOC_TYPE_CD = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getDOC_TYPE_CD" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_TYPE_CD />
	</cffunction>
	<cffunction name="setDOC_LETTER_TEMPLATE_CD" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.DOC_LETTER_TEMPLATE_CD = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getDOC_LETTER_TEMPLATE_CD" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_LETTER_TEMPLATE_CD />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
</cfcomponent>
