<cfcomponent name="DocLetterTemplateDAO" displayname="DocLetterTemplateDAO" hint="I abstract data access for DOC_LETTER_TEMPLATE" extends="dbObject">
	<cffunction name="read" returntype="DocLetterTemplateVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="docLetterTemplateCD" type="numeric" required="true" />

		<cfset var DocLetterTemplateVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_LETTER_TEMPLATE_CD,
				LETTER_TEMPLATE_CODE,
				LETTER_TEMPLATE_NAME,
				LETTER_TEMPLATE_BODY,
				INSTRUCTIONS,
				STATUS,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INSTANCE_ID
			FROM DOC_LETTER_TEMPLATE
			WHERE
				DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docLetterTemplateCD#"/>
		</cfquery>
		<cfset DocLetterTemplateVO = CreateObject("component","DocLetterTemplateVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset DocLetterTemplateVO.setDOC_LETTER_TEMPLATE_CD(getQry.DOC_LETTER_TEMPLATE_CD) />
			<cfset DocLetterTemplateVO.setLETTER_TEMPLATE_CODE(getQry.LETTER_TEMPLATE_CODE) />
			<cfset DocLetterTemplateVO.setLETTER_TEMPLATE_NAME(getQry.LETTER_TEMPLATE_NAME) />
			<cfset DocLetterTemplateVO.setLETTER_TEMPLATE_BODY(getQry.LETTER_TEMPLATE_BODY) />
			<cfset DocLetterTemplateVO.setINSTRUCTIONS(getQry.INSTRUCTIONS) />
			<cfset DocLetterTemplateVO.setSTATUS(getQry.STATUS) />
			<cfset DocLetterTemplateVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset DocLetterTemplateVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset DocLetterTemplateVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset DocLetterTemplateVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset DocLetterTemplateVO.setINSTANCE_ID(getQry.INSTANCE_ID) />
		</cfif>

		<cfreturn DocLetterTemplateVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterTemplateVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">

		<cfquery name="seqQry" datasource="#getDatasource()#">
			SELECT DOC_LETTER_TEMPLATE_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		<cfquery name="addQry" datasource="#getDatasource()#">
			INSERT INTO DOC_LETTER_TEMPLATE (
				DOC_LETTER_TEMPLATE_CD ,LETTER_TEMPLATE_CODE ,LETTER_TEMPLATE_NAME ,LETTER_TEMPLATE_BODY ,INSTRUCTIONS ,STATUS ,CREATION_ID ,CREATION_DT,INSTANCE_ID)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getLETTER_TEMPLATE_CODE()#" maxLength="100" null="#iif((arguments.bean.getLETTER_TEMPLATE_CODE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getLETTER_TEMPLATE_NAME()#" maxLength="250" null="#iif((arguments.bean.getLETTER_TEMPLATE_NAME() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_clob" value="#arguments.bean.getLETTER_TEMPLATE_BODY()#" null="#iif((arguments.bean.getLETTER_TEMPLATE_BODY() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getINSTRUCTIONS()#" maxLength="4000" null="#iif((arguments.bean.getINSTRUCTIONS() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getSTATUS()#" maxLength="100" null="#iif((arguments.bean.getSTATUS() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINSTANCE_ID()#" maxLength="22" null="#iif((arguments.bean.getINSTANCE_ID() eq ''),de("true"), de("false"))#" />)
		</cfquery>

		<cfreturn seqQry.nextval />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterTemplateVO" required="true" />
		<cfset var updateQry = "" />

		<cfquery name="updateQry" datasource="#getDatasource()#">
			UPDATE	DOC_LETTER_TEMPLATE
			SET
				LETTER_TEMPLATE_CODE = <cfqueryparam value="#arguments.bean.getLETTER_TEMPLATE_CODE()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getLETTER_TEMPLATE_CODE() eq ''),de("true"), de("false"))#" />,
				LETTER_TEMPLATE_NAME = <cfqueryparam value="#arguments.bean.getLETTER_TEMPLATE_NAME()#" cfsqltype="cf_sql_varchar" maxLength="250" null="#iif((arguments.bean.getLETTER_TEMPLATE_NAME() eq ''),de("true"), de("false"))#" />,
				LETTER_TEMPLATE_BODY = <cfqueryparam value="#arguments.bean.getLETTER_TEMPLATE_BODY()#" cfsqltype="cf_sql_clob" null="#iif((arguments.bean.getLETTER_TEMPLATE_BODY() eq ''),de("true"), de("false"))#" />,
				INSTRUCTIONS = <cfqueryparam value="#arguments.bean.getINSTRUCTIONS()#" cfsqltype="cf_sql_varchar" maxLength="4000" null="#iif((arguments.bean.getINSTRUCTIONS() eq ''),de("true"), de("false"))#" />,
				STATUS = <cfqueryparam value="#arguments.bean.getSTATUS()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getSTATUS() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINSTANCE_ID()#" maxLength="22" null="#iif((arguments.bean.getINSTANCE_ID() eq ''),de("true"), de("false"))#" />
			WHERE
				DOC_LETTER_TEMPLATE_CD = <cfqueryparam value="#arguments.bean.getDOC_LETTER_TEMPLATE_CD()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>

	<cffunction name="deleteDocTemplate" returntype="void" access="public" output="false" hint="CRUD method" >
        <cfargument name="DOC_LETTER_TEMPLATE_CD" type="numeric" required="true" />

        <cfset var delQry1 = "">
        <cfset var delQry2 = "">
        <cfset var delQry3 = "">

        <cfquery name="delQry1" datasource="#getDatasource()#">
            DELETE FROM DOC_LETTER_MAP
            WHERE
                DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.DOC_LETTER_TEMPLATE_CD#"/>
        </cfquery>

        <cfquery name="delQry2" datasource="#getDatasource()#">
            DELETE FROM DOC_SNAPSHOT_DETAIL
            WHERE
                DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.DOC_LETTER_TEMPLATE_CD#"/>
        </cfquery>

        <cfquery name="delQry3" datasource="#getDatasource()#">
            DELETE FROM DOC_LETTER_TEMPLATE
            WHERE
                DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.DOC_LETTER_TEMPLATE_CD#"/>
        </cfquery>
    </cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="DOC_LETTER_TEMPLATE_CD" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE FROM	DOC_LETTER_TEMPLATE
			WHERE
				DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.DOC_LETTER_TEMPLATE_CD#"/>
		</cfquery>
	</cffunction>
</cfcomponent>
