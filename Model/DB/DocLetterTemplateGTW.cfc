<cfcomponent name="DocLetterTemplateGTW" displayname="DocLetterTemplateGTW" output="false" hint="" extends="dbObject">

	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfargument name="instanceID" type="string" required="true">
		<cfargument name="all" type="boolean" required="false" default="false">

		<cfset var getQry = ""/>
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_LETTER_TEMPLATE_CD,
				LETTER_TEMPLATE_CODE,
				LETTER_TEMPLATE_NAME,
				LETTER_TEMPLATE_BODY,
				INSTRUCTIONS,
				STATUS,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_LETTER_TEMPLATE
			WHERE
				1 = 1
				<cfif arguments.instanceId neq "">
					AND INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
				</cfif>
				<cfif not arguments.all>
					AND STATUS='ACTIVE'
				</cfif>
				<cfif instanceResList neq "" and instanceResList neq "*">
					AND INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
				</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="doaLetterTemplateCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				A.DOC_LETTER_TEMPLATE_CD,
				A.LETTER_TEMPLATE_CODE,
				A.LETTER_TEMPLATE_NAME,
				A.LETTER_TEMPLATE_BODY,
				A.INSTRUCTIONS,
				A.STATUS,
				A.INSTANCE_ID,
				A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT,
				B.TAGS
			FROM
				DOC_LETTER_TEMPLATE A,
				DOC_INSTANCE B
			WHERE
				A.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.doaLetterTemplateCD#" />
				AND A.INSTANCE_ID = B.INSTANCE_ID
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByTemplateCD" access="public" returntype="query" output="false" displayname="getByTemplateCD" hint="">
		<cfargument name="doaLetterTemplateCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				A.DOC_LETTER_TEMPLATE_CD,
				A.LETTER_TEMPLATE_CODE,
				A.LETTER_TEMPLATE_NAME,
				A.LETTER_TEMPLATE_BODY,
				A.INSTRUCTIONS,
				F.ACTION_NAME,
				F.ACTION_CODE,
				A.STATUS,
				A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT,
				A.INSTANCE_ID,
				D.DOC_SIGNATURE_CD,
				E.FILE_NAME AS DOC_SIGNATURE_FILE_NAME,
				NVL(B.ENABLE_PUBLISH, 'N') AS ENABLE_PUBLISH,
				NVL(B.ENABLE_SEND_EMAIL, 'N') AS ENABLE_SEND_EMAIL
			FROM
				DOC_LETTER_TEMPLATE A,
				DOC_SETTINGS B,
				DOC_LETTER_TEMPLATE_SIGNATURE D,
				DOC_SIGNATURE E,
				DOC_LETTER_ACTION F
			WHERE
				A.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.doaLetterTemplateCD#" />
				AND D.DOC_LETTER_TEMPLATE_CD(+) = A.DOC_LETTER_TEMPLATE_CD
				AND E.DOC_SIGNATURE_CD(+) = D.DOC_SIGNATURE_CD
				AND A.INSTANCE_ID = B.INSTANCE_ID (+)
				AND B.ACTION_ID = F.ACTION_ID (+)
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getTypesByTemplateCD" access="public" returntype="query" output="false" displayname="getTypesByTemplateCD" hint="">
        <cfargument name="doaLetterTemplateCD" type="numeric" required="true" />
        <cfset var getQry = ""/>
        <cfquery name="getQry" datasource="#getDatasource()#">
            SELECT
			    C.DOC_TYPE_CD,
                C.TYPE_CODE,
                C.TYPE_DESC
            FROM
                DOC_LETTER_TEMPLATE A,
                DOC_LETTER_MAP B,
                DOC_TYPE C
            WHERE
                A.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.doaLetterTemplateCD#" /> AND
                B.DOC_LETTER_TEMPLATE_CD(+) = A.DOC_LETTER_TEMPLATE_CD AND
                C.DOC_TYPE_CD = B.DOC_TYPE_CD
        </cfquery>
        <cfreturn getQry/>
    </cffunction>

	<cffunction name="getSetupsByTemplateCD" access="public" returntype="query" output="false" displayname="getTypesByTemplateCD" hint="">
        <cfargument name="doaLetterTemplateCD" type="numeric" required="true" />
        <cfset var getQry = ""/>
        <cfquery name="getQry" datasource="#getDatasource()#">
            SELECT
			    B.*
            FROM
                DOC_LETTER_TEMPLATE A,
                DOC_SETUP B
            WHERE
                A.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.doaLetterTemplateCD#" /> AND
                B.DOC_LETTER_TEMPLATE_CD = A.DOC_LETTER_TEMPLATE_CD
        </cfquery>
        <cfreturn getQry/>
    </cffunction>

	<cffunction name="getTemplates" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfargument name="filter" type="sis_core.model.SearchFilter" required="false">
	    <cfargument name="data" type="sis_core.model.SortOptions" required="true"/>

		<cfset DataSourceUtil = application.wirebox.getInstance(name="sis_core.model.util.datasource")/>
		<cfset pgDSN = application.wirebox.getInstance(dsl="coldbox:setting:pg_datasource") />
		<cfset pgUserDSNUser = DataSourceUtil.getUserName(pgDSN)/>

		<cfset var getQry = ""/>
		<cfset var instanceResList = "-1">

		<cfset EntityRestrictions =  application.wirebox.getInstance(name='sis_core.model.blAutomation.EntityRestrictions')/>
		<cfset userSessionData = application.wirebox.getInstance(dsl='coldbox:plugin:SessionStorage' ).getVar("LoggedInUser")>
	 	<cfset instanceRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_INSTANCE',userId=userSessionData.USER_ID,securityCode="DOCUMENT_SETUP") />
		<cfif instanceRestrictionSql NEQ "*">
			<cfset instanceResList = ListAppend(instanceResList,instanceRestrictionSql)>
		</cfif>

		<cfquery name="getQry" datasource="#getDatasource()#">
		 SELECT *
             FROM (
                SELECT
                    Z.*
                    <cfif arguments.filter.getTABLE_PAGE().getRECORDS_PER_PAGE() GT 0 AND arguments.filter.getTABLE_PAGE().getPAGE_NO()>
		    		,ROWNUM AS MYROWNUM
			    	</cfif>
                FROM
                    (SELECT
                        Y.*
						<cfif arguments.filter.getTABLE_PAGE().getRECORDS_PER_PAGE() GT 0 AND arguments.filter.getTABLE_PAGE().getPAGE_NO()>
							,CEIL(Y.TOTAL_RECORDS / #arguments.filter.getTABLE_PAGE().getRECORDS_PER_PAGE()#) AS TOTAL_PAGES
						</cfif>
                    FROM
						(
							SELECT
								LLT.DOC_LETTER_TEMPLATE_CD,
								LLT.LETTER_TEMPLATE_CODE,
					        	LLT.LETTER_TEMPLATE_NAME,
					        	LLT.LETTER_TEMPLATE_BODY,
					        	LLT.INSTRUCTIONS,
					        	LLT.STATUS,
					        	LLT.CREATION_ID,
					        	LLT.CREATION_DT,
					        	LLT.MODIFICATION_ID,
					        	LLT.MODIFICATION_DT,
								A.DESCRIPTION,
								U.LAST_NAME||', '||U.FIRST_NAME as UsedBy,
								NVL(LLT.MODIFICATION_DT, LLT.CREATION_DT) AS LAST_MODIFIED,
								COUNT(*) OVER() AS TOTAL_RECORDS,
								LLT.INSTANCE_ID,
								LI.INSTANCE_NAME
							FROM DOC_LETTER_TEMPLATE LLT,
							      UD_USER U,
							  (SELECT  LSD.*,
							  		   TS.TRAINING_SESSION_NAME as DESCRIPTION
							  	FROM #pgUserDSNUser#.TRAINING_SESSION TS,
					     			(
									 SELECT X.DOC_LETTER_TEMPLATE_CD,
											MAX(X.TRAINING_SESSION_CD) AS TRAINING_SESSION_CD
										FROM DOC_SNAPSHOT_DETAIL X
										GROUP BY X.DOC_LETTER_TEMPLATE_CD
										)LSD
							   WHERE
							   TS.TRAINING_SESSION_CD=LSD.TRAINING_SESSION_CD
							   )A,
							   DOC_INSTANCE LI
							WHERE
							<cfif arguments.filter.getFILTER_ITEM("LETTERTYPE") neq "">
								LLT.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filter.getFILTER_ITEM("LETTERTYPE")#" /> AND
							</cfif>

							LLT.INSTANCE_ID = LI.INSTANCE_ID AND
							<cfif arguments.filter.getFILTER_ITEM("SEARCH_STRING") neq "">
								UPPER(LLT.LETTER_TEMPLATE_NAME) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#UCase(arguments.filter.getFILTER_ITEM("SEARCH_STRING"))#%" /> AND
							</cfif>
							LLT.DOC_LETTER_TEMPLATE_CD = A.DOC_LETTER_TEMPLATE_CD(+) AND
							COALESCE(LLT.MODIFICATION_ID,LLT.CREATION_ID)=U.USER_ID(+)
							<cfif instanceResList neq "" and instanceResList neq "*">
								AND LLT.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
							</cfif>
					)Y
					ORDER BY
					<cfif arguments.filter.getSORT_OPTIONS().getField() neq "">
						#arguments.filter.getSORT_OPTIONS().getSortString_UC()#
					<cfelse>
						upper(LETTER_TEMPLATE_NAME)
					</cfif>
            	) Z
			) W
            <cfif arguments.filter.getTABLE_PAGE().getRECORDS_PER_PAGE() gt 0 and arguments.filter.getTABLE_PAGE().getPAGE_NO() gt 0>
				WHERE W.MYROWNUM BETWEEN #arguments.filter.getTABLE_PAGE().getSTART_ROW()# AND #arguments.filter.getTABLE_PAGE().getEND_ROW()#
			</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getTemplateSignature" access="public" returntype="query" output="false" displayname="getTemplateSignature" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="doaLetterTemplateCD" type="numeric" required="true" />
		<cfset var getQry = ""/>

		<cfset var loaSignatureBL = CreateObject("component", "LOA.Model.BL.loaSignature").init(arguments.instanceID)>
		<cfset var loaSignatureFolder = loaSignatureBL.getSignaturesFolderPath()>

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				'<img width="150" src="#application.cbcontroller.getSetting("modulesApplicationURL")#\modules\AgreementLetters\' || '#loaSignatureFolder#' || '/' || D.FILE_NAME || '"/>' AS SIGNATURE
			FROM
				DOC_LETTER_TEMPLATE_SIGNATURE C,
				DOC_SIGNATURE D
			WHERE
				C.DOC_LETTER_TEMPLATE_CD(+) = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.doaLetterTemplateCD#" /> AND
				D.DOC_SIGNATURE_CD(+) = C.DOC_SIGNATURE_CD
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByDescription" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="doaLetterTemplateDESC" type="string" required="true" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				A.DOC_LETTER_TEMPLATE_CD,
				A.LETTER_TEMPLATE_CODE,
				A.LETTER_TEMPLATE_NAME,
				A.LETTER_TEMPLATE_BODY,
				A.INSTRUCTIONS,
				A.STATUS,
				A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT
			FROM
				DOC_LETTER_TEMPLATE A
			WHERE
				A.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" /> AND
				UPPER(A.LETTER_TEMPLATE_NAME) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#UCase(arguments.doaLetterTemplateDESC)#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByCDList" access="public" returntype="query" output="false" displayname="getByCDList" hint="">
		<cfargument name="idList" type="string" required="true" />
		<cfargument name="all" type="boolean" required="false" default="false" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				A.DOC_LETTER_TEMPLATE_CD,
				A.LETTER_TEMPLATE_CODE,
				A.LETTER_TEMPLATE_NAME,
				A.LETTER_TEMPLATE_BODY,
				A.INSTRUCTIONS,
				A.STATUS,
				A.INSTANCE_ID,
				A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT,
				B.REFERS_TO
			FROM DOC_LETTER_TEMPLATE A, DOC_INSTANCE B
			WHERE
				A.INSTANCE_ID = B.INSTANCE_ID
				<cfif not arguments.all>
					AND A.STATUS='ACTIVE'
				</cfif>
				<cfif Len(arguments.idList)>
					AND A.DOC_LETTER_TEMPLATE_CD IN ( <cfqueryparam cfsqltype="cf_sql_numeric" list="true" value="#arguments.idList#" /> )
				</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
</cfcomponent>