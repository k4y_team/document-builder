<cfcomponent name="DocLetterTemplateSignatureDAO" displayname="DocLetterTemplateSignatureDAO" hint="I abstract data access for DOC_LETTER_TEMPLATE_SIGNATURE" extends="dbObject">
	<cffunction name="read" returntype="DocLetterTemplateSignatureVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="docLetterTemplateCD" type="numeric" required="true" />

		<cfset var DocLetterTemplateSignatureVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_LETTER_TEMPLATE_CD,
				DOC_SIGNATURE_CD,
				TAG_CODE,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_LETTER_TEMPLATE_SIGNATURE
			WHERE
				DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docLetterTemplateCD#"/>
		</cfquery>
		<cfset DocLetterTemplateSignatureVO = CreateObject("component","DocLetterTemplateSignatureVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset DocLetterTemplateSignatureVO.setDOC_LETTER_TEMPLATE_CD(getQry.DOC_LETTER_TEMPLATE_CD) />
			<cfset DocLetterTemplateSignatureVO.setDOC_SIGNATURE_CD(getQry.DOC_SIGNATURE_CD) />
			<cfset DocLetterTemplateSignatureVO.setTAG_CODE(getQry.TAG_CODE) />
			<cfset DocLetterTemplateSignatureVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset DocLetterTemplateSignatureVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset DocLetterTemplateSignatureVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset DocLetterTemplateSignatureVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
		</cfif>

		<cfreturn DocLetterTemplateSignatureVO />
	</cffunction>
	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterTemplateSignatureVO" required="true" />
		<cfset var addQry = "">

		<cfquery name="addQry" datasource="#getDatasource()#">
			INSERT INTO DOC_LETTER_TEMPLATE_SIGNATURE (
				DOC_LETTER_TEMPLATE_CD ,DOC_SIGNATURE_CD ,TAG_CODE ,CREATION_ID ,CREATION_DT)
			VALUES (<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_LETTER_TEMPLATE_CD()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_SIGNATURE_CD()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getTAG_CODE()#" maxLength="100" null="#iif((arguments.bean.getTAG_CODE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />)
		</cfquery>

		<cfreturn arguments.bean.getDOC_LETTER_TEMPLATE_CD() />
	</cffunction>
	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocLetterTemplateSignatureVO" required="true" />
		<cfset var updateQry = "" />

		<cfquery name="updateQry" datasource="#getDatasource()#">
			UPDATE	DOC_LETTER_TEMPLATE_SIGNATURE
			SET
				DOC_SIGNATURE_CD = <cfqueryparam value="#arguments.bean.getDOC_SIGNATURE_CD()#" cfsqltype="cf_sql_numeric" maxLength="22" null="false" />,
				TAG_CODE = <cfqueryparam value="#arguments.bean.getTAG_CODE()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getTAG_CODE() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />
			WHERE
				DOC_LETTER_TEMPLATE_CD = <cfqueryparam value="#arguments.bean.getDOC_LETTER_TEMPLATE_CD()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>
	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="docLetterTemplateCD" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE FROM	DOC_LETTER_TEMPLATE_SIGNATURE
			WHERE
				DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docLetterTemplateCD#"/>
		</cfquery>
	</cffunction>
</cfcomponent>
