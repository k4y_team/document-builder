<cfcomponent name="DocLetterTemplateSignatureVO" displayname="DocLetterTemplateSignatureVO" extends="getSetObjectValue" output="false">
		<cfproperty name="DOC_LETTER_TEMPLATE_CD" type="numeric" />
		<cfproperty name="DOC_SIGNATURE_CD" type="numeric" />
		<cfproperty name="TAG_CODE" type="string" />
		<cfproperty name="CREATION_ID" type="numeric" />
		<cfproperty name="CREATION_DT" type="date" />
		<cfproperty name="MODIFICATION_ID" type="numeric" />
		<cfproperty name="MODIFICATION_DT" type="date" />
		
	<cffunction name="init" access="public" returntype="DocLetterTemplateSignatureVO" output="false" displayname="init" hint="I initialize a DocLetterTemplateSignature">
		<cfset super.init() />
		<cfset variables.DOC_LETTER_TEMPLATE_CD = "" />
		<cfset variables.DOC_SIGNATURE_CD = "" />
		<cfset variables.TAG_CODE = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setDOC_LETTER_TEMPLATE_CD" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.DOC_LETTER_TEMPLATE_CD = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getDOC_LETTER_TEMPLATE_CD" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_LETTER_TEMPLATE_CD />
	</cffunction>
	<cffunction name="setDOC_SIGNATURE_CD" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.DOC_SIGNATURE_CD = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getDOC_SIGNATURE_CD" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_SIGNATURE_CD />
	</cffunction>
	<cffunction name="setTAG_CODE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.TAG_CODE = arguments.val />
	</cffunction>
	<cffunction name="getTAG_CODE" access="public" returntype="any" output="false">
		<cfreturn variables.TAG_CODE />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
</cfcomponent>
