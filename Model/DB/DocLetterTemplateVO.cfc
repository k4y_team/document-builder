<cfcomponent name="DocLetterTemplateVO" displayname="DocLetterTemplateVO" extends="getSetObjectValue" output="false">
		<cfproperty name="DOC_LETTER_TEMPLATE_CD" type="numeric" />
		<cfproperty name="LETTER_TEMPLATE_CODE" type="string" />
		<cfproperty name="LETTER_TEMPLATE_NAME" type="string" />
		<cfproperty name="LETTER_TEMPLATE_BODY" type="string" />
		<cfproperty name="INSTRUCTIONS" type="string" />
		<cfproperty name="STATUS" type="string" />
		<cfproperty name="CREATION_ID" type="numeric" />
		<cfproperty name="CREATION_DT" type="date" />
		<cfproperty name="MODIFICATION_ID" type="numeric" />
		<cfproperty name="MODIFICATION_DT" type="date" />
		<cfproperty name="INSTANCE_ID" type="numeric" />

	<cffunction name="init" access="public" returntype="DocLetterTemplateVO" output="false" displayname="init" hint="I initialize a DocLetterTemplate">
		<cfset super.init() />
		<cfset variables.DOC_LETTER_TEMPLATE_CD = "" />
		<cfset variables.LETTER_TEMPLATE_CODE = "" />
		<cfset variables.LETTER_TEMPLATE_NAME = "" />
		<cfset variables.LETTER_TEMPLATE_BODY = "" />
		<cfset variables.INSTRUCTIONS = "" />
		<cfset variables.STATUS = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.INSTANCE_ID = "" />

		<cfreturn this />
 	</cffunction>

	<cffunction name="setDOC_LETTER_TEMPLATE_CD" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.DOC_LETTER_TEMPLATE_CD = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getDOC_LETTER_TEMPLATE_CD" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_LETTER_TEMPLATE_CD />
	</cffunction>
	<cffunction name="setLETTER_TEMPLATE_CODE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.LETTER_TEMPLATE_CODE = arguments.val />
	</cffunction>
	<cffunction name="getLETTER_TEMPLATE_CODE" access="public" returntype="any" output="false">
		<cfreturn variables.LETTER_TEMPLATE_CODE />
	</cffunction>
	<cffunction name="setLETTER_TEMPLATE_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.LETTER_TEMPLATE_NAME = arguments.val />
	</cffunction>
	<cffunction name="getLETTER_TEMPLATE_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.LETTER_TEMPLATE_NAME />
	</cffunction>
	<cffunction name="setLETTER_TEMPLATE_BODY" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.LETTER_TEMPLATE_BODY = arguments.val />
	</cffunction>
	<cffunction name="getLETTER_TEMPLATE_BODY" access="public" returntype="any" output="false">
		<cfreturn variables.LETTER_TEMPLATE_BODY />
	</cffunction>
	<cffunction name="setINSTRUCTIONS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.INSTRUCTIONS = arguments.val />
	</cffunction>
	<cffunction name="getINSTRUCTIONS" access="public" returntype="any" output="false">
		<cfreturn variables.INSTRUCTIONS />
	</cffunction>
	<cffunction name="setSTATUS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.STATUS = arguments.val />
	</cffunction>
	<cffunction name="getSTATUS" access="public" returntype="any" output="false">
		<cfreturn variables.STATUS />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
	<cffunction name="setINSTANCE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.INSTANCE_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getINSTANCE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INSTANCE_ID />
	</cffunction>
</cfcomponent>
