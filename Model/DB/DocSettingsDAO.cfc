<cfcomponent name="DocSettingsDAO" displayname="DocSettingsDAO" hint="I abstract data access for DOC_SETTINGS" extends="dbObject">

	<cffunction name="read" returntype="DocSettingsVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="instanceID" type="numeric" required="true" />

		<cfset var DocSettingsVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				INSTANCE_ID,
				DOC_AVAILABLE_DAY,
				DOC_AVAILABLE_MONTH,
				DOC_DUE_DAYS,
				DOC_REPO_PATH,
				SIGN_IMAGES_PATH,
				MODIFICATION_ID,
				MODIFICATION_DT,
				GENERATE_CORRECTION,
				AUTO_PUBLISH_ORIGINAL,
				AUTO_PUBLISH_CORRECTION,
				AUTO_PUBLISH_REVISION,
				OVERWRITE_CURRENT,
				ACTION_ID,
				AUTO_GENERATE,
				ENABLE_PUBLISH,
				ENABLE_SEND_EMAIL,
				ATTACH_USER_PICTURE
			FROM DOC_SETTINGS
			WHERE
				INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#"/>
		</cfquery>

		<cfset DocSettingsVO = CreateObject("component","DocSettingsVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset DocSettingsVO.setINSTANCE_ID(getQry.INSTANCE_ID) />
			<cfset DocSettingsVO.setDOC_AVAILABLE_DAY(getQry.DOC_AVAILABLE_DAY) />
			<cfset DocSettingsVO.setDOC_AVAILABLE_MONTH(getQry.DOC_AVAILABLE_MONTH) />
			<cfset DocSettingsVO.setDOC_DUE_DAYS(getQry.DOC_DUE_DAYS) />
			<cfset DocSettingsVO.setDOC_REPO_PATH(getQry.DOC_REPO_PATH) />
			<cfset DocSettingsVO.setSIGN_IMAGES_PATH(getQry.SIGN_IMAGES_PATH) />
			<cfset DocSettingsVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset DocSettingsVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
			<cfset DocSettingsVO.setGENERATE_CORRECTION(getQry.GENERATE_CORRECTION) />
			<cfset DocSettingsVO.setAUTO_PUBLISH_ORIGINAL(getQry.AUTO_PUBLISH_ORIGINAL) />
			<cfset DocSettingsVO.setAUTO_PUBLISH_CORRECTION(getQry.AUTO_PUBLISH_CORRECTION) />
			<cfset DocSettingsVO.setAUTO_PUBLISH_REVISION(getQry.AUTO_PUBLISH_REVISION) />
			<cfset DocSettingsVO.setOVERWRITE_CURRENT(getQry.OVERWRITE_CURRENT) />
			<cfset DocSettingsVO.setACTION_ID(getQry.ACTION_ID) />
			<cfset DocSettingsVO.setAUTO_GENERATE(getQry.AUTO_GENERATE) />
			<cfset DocSettingsVO.setENABLE_PUBLISH(getQry.ENABLE_PUBLISH) />
			<cfset DocSettingsVO.setENABLE_SEND_EMAIL(getQry.ENABLE_SEND_EMAIL) />
			<cfset DocSettingsVO.setATTACH_USER_PICTURE(getQry.ATTACH_USER_PICTURE) />
		</cfif>

		<cfreturn DocSettingsVO />
	</cffunction>

	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocSettingsVO" required="true" />
		<cfset var addQry = "">

		<cfquery name="addQry" datasource="#getDatasource()#">
			INSERT INTO DOC_SETTINGS (INSTANCE_ID ,DOC_AVAILABLE_DAY ,DOC_AVAILABLE_MONTH ,DOC_DUE_DAYS ,DOC_REPO_PATH ,SIGN_IMAGES_PATH ,GENERATE_CORRECTION ,AUTO_PUBLISH_ORIGINAL ,AUTO_PUBLISH_CORRECTION ,AUTO_PUBLISH_REVISION ,OVERWRITE_CURRENT ,ACTION_ID, AUTO_GENERATE, ENABLE_PUBLISH, ENABLE_SEND_EMAIL, ATTACH_USER_PICTURE)
			VALUES (<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINSTANCE_ID()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getDOC_AVAILABLE_DAY()#" maxLength="25" null="#iif((arguments.bean.getDOC_AVAILABLE_DAY() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getDOC_AVAILABLE_MONTH()#" maxLength="25" null="#iif((arguments.bean.getDOC_AVAILABLE_MONTH() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_DUE_DAYS()#" maxLength="22" null="#iif((arguments.bean.getDOC_DUE_DAYS() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getDOC_REPO_PATH()#" maxLength="1000" null="#iif((arguments.bean.getDOC_REPO_PATH() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getSIGN_IMAGES_PATH()#" maxLength="1000" null="#iif((arguments.bean.getSIGN_IMAGES_PATH() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getGENERATE_CORRECTION()#" maxLength="20" null="#iif((arguments.bean.getGENERATE_CORRECTION() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getAUTO_PUBLISH_ORIGINAL()#" maxLength="20" null="#iif((arguments.bean.getAUTO_PUBLISH_ORIGINAL() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getAUTO_PUBLISH_CORRECTION()#" maxLength="20" null="#iif((arguments.bean.getAUTO_PUBLISH_CORRECTION() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getAUTO_PUBLISH_REVISION()#" maxLength="20" null="#iif((arguments.bean.getAUTO_PUBLISH_REVISION() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getOVERWRITE_CURRENT()#" maxLength="20" null="#iif((arguments.bean.getOVERWRITE_CURRENT() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getACTION_ID()#" maxLength="22" null="#iif((arguments.bean.getACTION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getAUTO_GENERATE()#" maxLength="1" null="#iif((arguments.bean.getAUTO_GENERATE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getENABLE_PUBLISH()#" maxLength="1" null="#iif((arguments.bean.getENABLE_PUBLISH() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getENABLE_SEND_EMAIL()#" maxLength="1" null="#iif((arguments.bean.getENABLE_SEND_EMAIL() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getATTACH_USER_PICTURE()#" maxLength="1" null="#iif((arguments.bean.getATTACH_USER_PICTURE() eq ''),de("true"), de("false"))#" />)
		</cfquery>

		<cfreturn 0 />
	</cffunction>


	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocSettingsVO" required="true" />
		<cfset var updateQry = "" />

		<cfquery name="updateQry" datasource="#getDatasource()#">
			UPDATE  DOC_SETTINGS
			SET
				DOC_AVAILABLE_DAY = <cfqueryparam value="#arguments.bean.getDOC_AVAILABLE_DAY()#" cfsqltype="cf_sql_varchar" maxLength="25" null="#iif((arguments.bean.getDOC_AVAILABLE_DAY() eq ''),de("true"), de("false"))#" />,
				DOC_AVAILABLE_MONTH = <cfqueryparam value="#arguments.bean.getDOC_AVAILABLE_MONTH()#" cfsqltype="cf_sql_varchar" maxLength="25" null="#iif((arguments.bean.getDOC_AVAILABLE_MONTH() eq ''),de("true"), de("false"))#" />,
				DOC_DUE_DAYS = <cfqueryparam value="#arguments.bean.getDOC_DUE_DAYS()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDOC_DUE_DAYS() eq ''),de("true"), de("false"))#" />,
				DOC_REPO_PATH = <cfqueryparam value="#arguments.bean.getDOC_REPO_PATH()#" cfsqltype="cf_sql_varchar" maxLength="1000" null="#iif((arguments.bean.getDOC_REPO_PATH() eq ''),de("true"), de("false"))#" />,
				SIGN_IMAGES_PATH = <cfqueryparam value="#arguments.bean.getSIGN_IMAGES_PATH()#" cfsqltype="cf_sql_varchar" maxLength="1000" null="#iif((arguments.bean.getSIGN_IMAGES_PATH() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				GENERATE_CORRECTION = <cfqueryparam value="#arguments.bean.getGENERATE_CORRECTION()#" cfsqltype="cf_sql_varchar" maxLength="20" null="#iif((arguments.bean.getGENERATE_CORRECTION() eq ''),de("true"), de("false"))#" />,
				AUTO_PUBLISH_ORIGINAL = <cfqueryparam value="#arguments.bean.getAUTO_PUBLISH_ORIGINAL()#" cfsqltype="cf_sql_varchar" maxLength="20" null="#iif((arguments.bean.getAUTO_PUBLISH_ORIGINAL() eq ''),de("true"), de("false"))#" />,
				AUTO_PUBLISH_CORRECTION = <cfqueryparam value="#arguments.bean.getAUTO_PUBLISH_CORRECTION()#" cfsqltype="cf_sql_varchar" maxLength="20" null="#iif((arguments.bean.getAUTO_PUBLISH_CORRECTION() eq ''),de("true"), de("false"))#" />,
				AUTO_PUBLISH_REVISION = <cfqueryparam value="#arguments.bean.getAUTO_PUBLISH_REVISION()#" cfsqltype="cf_sql_varchar" maxLength="20" null="#iif((arguments.bean.getAUTO_PUBLISH_REVISION() eq ''),de("true"), de("false"))#" />,
				OVERWRITE_CURRENT = <cfqueryparam value="#arguments.bean.getOVERWRITE_CURRENT()#" cfsqltype="cf_sql_varchar" maxLength="20" null="#iif((arguments.bean.getOVERWRITE_CURRENT() eq ''),de("true"), de("false"))#" />,
				ACTION_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getACTION_ID()#" null="#iif((arguments.bean.getACTION_ID() eq ''),de("true"), de("false"))#" />,
				AUTO_GENERATE = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getAUTO_GENERATE()#" maxLength="1" null="#iif((arguments.bean.getAUTO_GENERATE() eq ''),de("true"), de("false"))#" />,
				ENABLE_PUBLISH = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getENABLE_PUBLISH()#" maxLength="1" null="#iif((arguments.bean.getENABLE_PUBLISH() eq ''),de("true"), de("false"))#" />,
				ENABLE_SEND_EMAIL = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getENABLE_SEND_EMAIL()#" maxLength="1" null="#iif((arguments.bean.getENABLE_SEND_EMAIL() eq ''),de("true"), de("false"))#" />,
				ATTACH_USER_PICTURE = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getATTACH_USER_PICTURE()#" maxLength="1" null="#iif((arguments.bean.getATTACH_USER_PICTURE() eq ''),de("true"), de("false"))#" />
			WHERE
				INSTANCE_ID = <cfqueryparam value="#arguments.bean.getINSTANCE_ID()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="instanceID" type="numeric" required="true" />

		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE FROM DOC_SETTINGS
			WHERE
				INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#"/>
		</cfquery>
	</cffunction>
</cfcomponent>