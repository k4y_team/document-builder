<cfcomponent name="DocSettingsGTW" displayname="DocSettingsGTW" output="false" hint="" extends="dbObject">
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfargument name="instanceID" type="numeric" required="true">

		<cfset DataSourceUtil = application.wirebox.getInstance(name="sis_core.model.util.datasource")/>
		<cfset pgDSN = application.wirebox.getInstance(dsl="coldbox:setting:pg_datasource") />
		<cfset pgUserDSNUser = DataSourceUtil.getUserName(pgDSN)/>

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				LS.DOC_DUE_DAYS,
				LS.DOC_REPO_PATH,
				LS.SIGN_IMAGES_PATH,
				LS.GENERATE_CORRECTION,
				LS.AUTO_PUBLISH_ORIGINAL,
				LS.AUTO_PUBLISH_CORRECTION,
				LS.AUTO_PUBLISH_REVISION,
				LS.OVERWRITE_CURRENT,
				LS.MODIFICATION_ID,
				LS.MODIFICATION_DT,
				LS.DOC_AVAILABLE_DAY,
				LS.DOC_AVAILABLE_MONTH,
				LS.ACTION_ID,
				NVL(LS.AUTO_GENERATE, 'N') AS AUTO_GENERATE,
				NVL(LS.ENABLE_PUBLISH, 'N') AS ENABLE_PUBLISH,
				NVL(LS.ATTACH_USER_PICTURE, 'N') AS ATTACH_USER_PICTURE,
				NVL(LS.ENABLE_SEND_EMAIL, 'N') AS ENABLE_SEND_EMAIL,
				U.LAST_NAME||', '||U.FIRST_NAME as USER_NAME
			FROM
				DOC_SETTINGS LS,
				#pgUserDSNUser#.UD_USER U
			WHERE
				LS.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
				AND LS.MODIFICATION_ID = U.USER_ID (+)
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfargument name="instanceID" type="numeric" required="true">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				LS.DOC_DUE_DAYS,
				LS.DOC_REPO_PATH,
				LS.SIGN_IMAGES_PATH,
				NVL(LS.GENERATE_CORRECTION,'N'),
				NVL(LS.AUTO_PUBLISH_ORIGINAL,'N'),
				NVL(LS.AUTO_PUBLISH_CORRECTION,'N'),
				NVL(LS.AUTO_PUBLISH_REVISION,'N'),
				NVL(LS.OVERWRITE_CURRENT,'N'),
				LS.MODIFICATION_ID,
				LS.MODIFICATION_DT,
				LS.DOC_AVAILABLE_DAY,
				LS.DOC_AVAILABLE_MONTH,
				LS.ACTION_ID,
				NVL(LS.AUTO_GENERATE, 'N') AS AUTO_GENERATE,
				NVL(LS.ENABLE_PUBLISH, 'N') AS ENABLE_PUBLISH,
				NVL(LS.ATTACH_USER_PICTURE, 'N') AS ATTACH_USER_PICTURE,
				NVL(LS.ENABLE_SEND_EMAIL, 'N') AS ENABLE_SEND_EMAIL
			FROM DOC_SETTINGS LS
			WHERE
				LS.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

</cfcomponent>