<cfcomponent name="DocSettingsVO" displayname="DocSettingsVO" extends="getSetObjectValue" output="false">
	<cfproperty name="INSTANCE_ID" type="numeric" />
	<cfproperty name="DOC_AVAILABLE_DAY" type="string" />
	<cfproperty name="DOC_AVAILABLE_MONTH" type="string" />
	<cfproperty name="DOC_DUE_DAYS" type="numeric" />
	<cfproperty name="DOC_REPO_PATH" type="string" />
	<cfproperty name="SIGN_IMAGES_PATH" type="string" />
	<cfproperty name="MODIFICATION_ID" type="numeric" />
	<cfproperty name="MODIFICATION_DT" type="date" />
	<cfproperty name="GENERATE_CORRECTION" type="string" />
	<cfproperty name="AUTO_PUBLISH_ORIGINAL" type="string" />
	<cfproperty name="AUTO_PUBLISH_CORRECTION" type="string" />
	<cfproperty name="AUTO_PUBLISH_REVISION" type="string" />
	<cfproperty name="OVERWRITE_CURRENT" type="string" />
	<cfproperty name="ACTION_ID" type="numeric" />
	<cfproperty name="AUTO_GENERATE" type="string" />
	<cfproperty name="ENABLE_PUBLISH" type="string" />
	<cfproperty name="ENABLE_SEND_EMAIL" type="string" />
	<cfproperty name="ATTACH_USER_PICTURE" type="string" />

	<cffunction name="init" access="public" returntype="DocSettingsVO" output="false" displayname="init" hint="I initialize a DocSettings">
		<cfset variables.INSTANCE_ID = "" />
		<cfset variables.DOC_AVAILABLE_DAY = "" />
		<cfset variables.DOC_AVAILABLE_MONTH = "" />
		<cfset variables.DOC_DUE_DAYS = "" />
		<cfset variables.DOC_REPO_PATH = "" />
		<cfset variables.SIGN_IMAGES_PATH = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		<cfset variables.GENERATE_CORRECTION = "" />
		<cfset variables.AUTO_PUBLISH_ORIGINAL = "" />
		<cfset variables.AUTO_PUBLISH_CORRECTION = "" />
		<cfset variables.AUTO_PUBLISH_REVISION = "" />
		<cfset variables.OVERWRITE_CURRENT = "" />
		<cfset variables.ACTION_ID = "" />
		<cfset variables.AUTO_GENERATE = "" />
		<cfset variables.ENABLE_PUBLISH = "" />
		<cfset variables.ENABLE_SEND_EMAIL = "" />
		<cfset variables.ATTACH_USER_PICTURE = "" />

		<cfreturn this />
	</cffunction>

	<cffunction name="setINSTANCE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.INSTANCE_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getINSTANCE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INSTANCE_ID />
	</cffunction>

	<cffunction name="setDOC_AVAILABLE_DAY" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.DOC_AVAILABLE_DAY = arguments.val />
	</cffunction>
	<cffunction name="getDOC_AVAILABLE_DAY" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_AVAILABLE_DAY />
	</cffunction>

	<cffunction name="setDOC_AVAILABLE_MONTH" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.DOC_AVAILABLE_MONTH = arguments.val />
	</cffunction>
	<cffunction name="getDOC_AVAILABLE_MONTH" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_AVAILABLE_MONTH />
	</cffunction>

	<cffunction name="setDOC_DUE_DAYS" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.DOC_DUE_DAYS = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getDOC_DUE_DAYS" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_DUE_DAYS />
	</cffunction>

	<cffunction name="setDOC_REPO_PATH" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.DOC_REPO_PATH = arguments.val />
	</cffunction>
	<cffunction name="getDOC_REPO_PATH" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_REPO_PATH />
	</cffunction>

	<cffunction name="setSIGN_IMAGES_PATH" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.SIGN_IMAGES_PATH = arguments.val />
	</cffunction>
	<cffunction name="getSIGN_IMAGES_PATH" access="public" returntype="any" output="false">
		<cfreturn variables.SIGN_IMAGES_PATH />
	</cffunction>

	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.MODIFICATION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>

	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfif isDate(arguments.val) OR arguments.val EQ ''>
			<cfset variables.MODIFICATION_DT = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid date!"/>
		</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>

	<cffunction name="setGENERATE_CORRECTION" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.GENERATE_CORRECTION = arguments.val />
	</cffunction>
	<cffunction name="getGENERATE_CORRECTION" access="public" returntype="any" output="false">
		<cfreturn variables.GENERATE_CORRECTION />
	</cffunction>

	<cffunction name="setAUTO_PUBLISH_ORIGINAL" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.AUTO_PUBLISH_ORIGINAL = arguments.val />
	</cffunction>
	<cffunction name="getAUTO_PUBLISH_ORIGINAL" access="public" returntype="any" output="false">
		<cfreturn variables.AUTO_PUBLISH_ORIGINAL />
	</cffunction>

	<cffunction name="setAUTO_PUBLISH_CORRECTION" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.AUTO_PUBLISH_CORRECTION = arguments.val />
	</cffunction>
	<cffunction name="getAUTO_PUBLISH_CORRECTION" access="public" returntype="any" output="false">
		<cfreturn variables.AUTO_PUBLISH_CORRECTION />
	</cffunction>

	<cffunction name="setAUTO_PUBLISH_REVISION" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.AUTO_PUBLISH_REVISION = arguments.val />
	</cffunction>
	<cffunction name="getAUTO_PUBLISH_REVISION" access="public" returntype="any" output="false">
		<cfreturn variables.AUTO_PUBLISH_REVISION />
	</cffunction>

	<cffunction name="setOVERWRITE_CURRENT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />

		<cfset variables.OVERWRITE_CURRENT = arguments.val />
	</cffunction>
	<cffunction name="getOVERWRITE_CURRENT" access="public" returntype="any" output="false">
		<cfreturn variables.OVERWRITE_CURRENT />
	</cffunction>

	<cffunction name="setACTION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.ACTION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getACTION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.ACTION_ID />
	</cffunction>

	<cffunction name="setAUTO_GENERATE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.AUTO_GENERATE = arguments.val />
	</cffunction>
	<cffunction name="getAUTO_GENERATE" access="public" returntype="any" output="false">
		<cfreturn variables.AUTO_GENERATE />
	</cffunction>

	<cffunction name="setENABLE_PUBLISH" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ENABLE_PUBLISH = arguments.val />
	</cffunction>
	<cffunction name="getENABLE_PUBLISH" access="public" returntype="any" output="false">
		<cfreturn variables.ENABLE_PUBLISH />
	</cffunction>

	<cffunction name="setENABLE_SEND_EMAIL" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ENABLE_SEND_EMAIL = arguments.val />
	</cffunction>
	<cffunction name="getENABLE_SEND_EMAIL" access="public" returntype="any" output="false">
		<cfreturn variables.ENABLE_SEND_EMAIL />
	</cffunction>

	<cffunction name="setATTACH_USER_PICTURE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.ATTACH_USER_PICTURE = arguments.val />
	</cffunction>
	<cffunction name="getATTACH_USER_PICTURE" access="public" returntype="any" output="false">
		<cfreturn variables.ATTACH_USER_PICTURE />
	</cffunction>

</cfcomponent>