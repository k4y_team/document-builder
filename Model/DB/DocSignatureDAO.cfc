<cfcomponent name="DocSignatureDAO" displayname="DocSignatureDAO" hint="I abstract data access for DOC_SIGNATURE" extends="dbObject">
	<cffunction name="read" returntype="docSignatureVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="docSignatureCD" type="numeric" required="true" />
		<cfset var DocSignatureVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_SIGNATURE_CD,
				SIGNATURE_CODE,
				FILE_NAME,
				DISPLAY_NAME,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_SIGNATURE
			WHERE
				DOC_SIGNATURE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSignatureCD#"/>
		</cfquery>
		<cfset DocSignatureVO = CreateObject("component","docSignatureVO").init()>

			<cfif getQry.recordCount gt 0>
			<cfset DocSignatureVO.setDOC_SIGNATURE_CD(getQry.DOC_SIGNATURE_CD) />
			<cfset DocSignatureVO.setSIGNATURE_CODE(getQry.SIGNATURE_CODE) />
			<cfset DocSignatureVO.setFILE_NAME(getQry.FILE_NAME) />
			<cfset DocSignatureVO.setDISPLAY_NAME(getQry.DISPLAY_NAME) />
			<cfset DocSignatureVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset DocSignatureVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset DocSignatureVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset DocSignatureVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
		</cfif>
		<cfreturn DocSignatureVO />
	</cffunction>

	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="docSignatureVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">

		<cfquery name="seqQry" datasource="#getDatasource()#">
			SELECT DOC_SIGNATURE_SEQ.nextval AS nextval
			FROM dual
		</cfquery>
		<cfquery name="addQry" datasource="#getDatasource()#">
			INSERT INTO DOC_SIGNATURE (
				DOC_SIGNATURE_CD ,SIGNATURE_CODE ,FILE_NAME ,DISPLAY_NAME ,CREATION_ID ,CREATION_DT )
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getSIGNATURE_CODE()#" maxLength="100" null="#iif((arguments.bean.getSIGNATURE_CODE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getFILE_NAME()#" maxLength="250" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getDISPLAY_NAME()#" maxLength="250" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />)
		</cfquery>

		<cfreturn seqQry.nextval />
	</cffunction>

	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="docSignatureVO" required="true" />
		<cfset var updateQry = "" />

		<cfquery name="updateQry" datasource="#getDatasource()#">
			UPDATE	DOC_SIGNATURE
			SET
				SIGNATURE_CODE = <cfqueryparam value="#arguments.bean.getSIGNATURE_CODE()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getSIGNATURE_CODE() eq ''),de("true"), de("false"))#" />,
				FILE_NAME = <cfqueryparam value="#arguments.bean.getFILE_NAME()#" cfsqltype="cf_sql_varchar" maxLength="250" null="false" />,
				DISPLAY_NAME = <cfqueryparam value="#arguments.bean.getDISPLAY_NAME()#" cfsqltype="cf_sql_varchar" maxLength="250" null="false" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />
			WHERE
				DOC_SIGNATURE_CD = <cfqueryparam value="#arguments.bean.getDOC_SIGNATURE_CD()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="docSignatureCD" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#">
			DELETE FROM	DOC_SIGNATURE
			WHERE
				DOC_SIGNATURE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docSignatureCD#"/>
		</cfquery>
	</cffunction>

</cfcomponent>