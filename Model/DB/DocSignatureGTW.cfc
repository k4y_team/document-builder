<cfcomponent name="DocSignatureGTW" displayname="DocSignatureGTW" output="false" hint="" extends="dbObject">
    <cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
        <cfquery name="getQry" datasource="#getDatasource()#">
         SELECT
  			SIG.*,
			NULL as LETTER_TEMPLATE_NAME,
			U.FIRST_NAME ||', '||U.LAST_NAME as UsedBy
		 FROM
			DOC_SIGNATURE SIG,
			UD_USER U
		 WHERE
			 SIG.CREATION_ID=U.USER_ID(+)
			ORDER BY SIG.FILE_NAME
        </cfquery>

		<cfreturn getQry/>
    </cffunction>

    <cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
        <cfargument name="docSignatureCD" type="numeric" required="true" />

        <cfset var getQry = ""/>
        <cfquery name="getQry" datasource="#getDatasource()#">
            SELECT
                DOC_SIGNATURE_CD,
                SIGNATURE_CODE,
                FILE_NAME,
                DISPLAY_NAME,
                CREATION_ID,
                CREATION_DT,
                MODIFICATION_ID,
                MODIFICATION_DT
            FROM DOC_SIGNATURE
            WHERE
                DOC_SIGNATURE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSignatureCD#" />
        </cfquery>
        <cfreturn getQry/>
    </cffunction>

	<cffunction name="getByDisplayName" access="public" returntype="query" output="false" displayname="getByDisplayName" hint="">
        <cfargument name="DisplayName" type="string" required="true" />

        <cfset var getQry = ""/>
        <cfquery name="getQry" datasource="#getDatasource()#">
            SELECT
                DOC_SIGNATURE_CD,
                SIGNATURE_CODE,
                FILE_NAME,
                DISPLAY_NAME,
                CREATION_ID,
                CREATION_DT,
                MODIFICATION_ID,
                MODIFICATION_DT
            FROM DOC_SIGNATURE
            WHERE
                DISPLAY_NAME = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.DisplayName#" />
        </cfquery>
        <cfreturn getQry/>
    </cffunction>

</cfcomponent>