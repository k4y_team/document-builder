<cfcomponent name="DocSignatureVO" displayname="DocSignatureVO" extends="getSetObjectValue" output="false">
	<cfproperty name="DOC_SIGNATURE_CD" type="numeric" />
	<cfproperty name="SIGNATURE_CODE" type="string" />
	<cfproperty name="FILE_NAME" type="string" />
	<cfproperty name="DISPLAY_NAME" type="string" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="MODIFICATION_ID" type="numeric" />
	<cfproperty name="MODIFICATION_DT" type="date" />
		
	<cffunction name="init" access="public" returntype="DocSignatureVO" output="false" displayname="init" hint="I initialize a DocSignature">
		<cfset super.init() />
		<cfset variables.DOC_SIGNATURE_CD = "" />
		<cfset variables.SIGNATURE_CODE = "" />
		<cfset variables.FILE_NAME = "" />
		<cfset variables.DISPLAY_NAME = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />
		
		<cfreturn this />
 	</cffunction>
	
	<cffunction name="setDOC_SIGNATURE_CD" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.DOC_SIGNATURE_CD = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getDOC_SIGNATURE_CD" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_SIGNATURE_CD />
	</cffunction>
	<cffunction name="setSIGNATURE_CODE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.SIGNATURE_CODE = arguments.val />
	</cffunction>
	<cffunction name="getSIGNATURE_CODE" access="public" returntype="any" output="false">
		<cfreturn variables.SIGNATURE_CODE />
	</cffunction>
	<cffunction name="setFILE_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.FILE_NAME = arguments.val />
	</cffunction>
	<cffunction name="getFILE_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.FILE_NAME />
	</cffunction>
	<cffunction name="setDISPLAY_NAME" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.DISPLAY_NAME = arguments.val />
	</cffunction>
	<cffunction name="getDISPLAY_NAME" access="public" returntype="any" output="false">
		<cfreturn variables.DISPLAY_NAME />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
</cfcomponent>
