<cfcomponent name="DocSnapshotBaseDAO" displayname="DocSnapshotBaseDAO" hint="I abstract data access for DOC_SNAPSHOT_BASE" extends="dbObject">
    <cffunction name="read" returntype="DocSnapshotBaseVO" access="public" output="false" hint="CRUD method" >
        <cfargument name="docSnapshotCD" type="numeric" required="true" />

        <cfset var DocSnapshotBaseVO = "" />
        <cfset var getQry = "" />
        <cfquery name="getQry" datasource="#getDatasource()#">
            SELECT
                *
            FROM DOC_SNAPSHOT_BASE
            WHERE
                DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#"/>
        </cfquery>

        <cfset DocSnapshotBaseVO = CreateObject("component","DocSnapshotBaseVO").init()>
        <cfif getQry.recordCount gt 0>
            <cfset DocSnapshotBaseVO.setDOC_SNAPSHOT_CD(getQry.DOC_SNAPSHOT_CD) />
            <cfset DocSnapshotBaseVO.setDOC_CONFIRMED_USER_ID(getQry.DOC_CONFIRMED_USER_ID) />
            <cfset DocSnapshotBaseVO.setDOC_CONFIRMED_DT(getQry.DOC_CONFIRMED_DT) />
            <cfset DocSnapshotBaseVO.setDOC_PUBLISHED_USER_ID(getQry.DOC_PUBLISHED_USER_ID) />
            <cfset DocSnapshotBaseVO.setDOC_PUBLISHED_DT(getQry.DOC_PUBLISHED_DT) />
            <cfset DocSnapshotBaseVO.setDOC_DT(getQry.DOC_DT) />
            <cfset DocSnapshotBaseVO.setIS_CURRENT(getQry.IS_CURRENT) />
            <cfset DocSnapshotBaseVO.setDOC_SUBMITTED_USER_ID(getQry.DOC_SUBMITTED_USER_ID) />
            <cfset DocSnapshotBaseVO.setDOC_SUBMITTED_DT(getQry.DOC_SUBMITTED_DT) />
            <cfset DocSnapshotBaseVO.setDOC_SIGNATURE(getQry.DOC_SIGNATURE) />
            <cfset DocSnapshotBaseVO.setCREATION_ID(getQry.CREATION_ID) />
            <cfset DocSnapshotBaseVO.setCREATION_DT(getQry.CREATION_DT) />
            <cfset DocSnapshotBaseVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
            <cfset DocSnapshotBaseVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
            <cfset DocSnapshotBaseVO.setINSTANCE_ID(getQry.INSTANCE_ID) />
            <cfset DocSnapshotBaseVO.setDOC_FILE_CD(getQry.DOC_FILE_CD) />
            <cfset DocSnapshotBaseVO.setDOC_PROOF_FILE_CD(getQry.DOC_PROOF_FILE_CD) />
            <cfset DocSnapshotBaseVO.setUSER_ID(getQry.USER_ID) />
            <cfset DocSnapshotBaseVO.setDOCUMENT_DESC(getQry.DOCUMENT_DESC) />
            <cfset DocSnapshotBaseVO.setTEMPLATE_BODY(getQry.TEMPLATE_BODY) />
        </cfif>

        <cfreturn DocSnapshotBaseVO />
    </cffunction>

    <cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
        <cfargument name="bean" type="DocSnapshotBaseVO" required="true" />
        <cfset var addQry = "">
        <cfset var seqQry = "">

        <cfquery name="seqQry" datasource="#getDatasource()#">
            SELECT DOC_SNAPSHOT_BASE_SEQ.nextval AS nextval
            FROM dual
        </cfquery>

        <cfquery name="addQry" datasource="#getDatasource()#">
            INSERT INTO DOC_SNAPSHOT_BASE (
                DOC_SNAPSHOT_CD ,INSTANCE_ID ,DOC_CONFIRMED_USER_ID ,DOC_CONFIRMED_DT ,DOC_PUBLISHED_USER_ID ,DOC_PUBLISHED_DT ,DOC_DT ,IS_CURRENT ,DOC_SUBMITTED_USER_ID ,DOC_SUBMITTED_DT ,DOC_SIGNATURE ,CREATION_ID ,CREATION_DT, DOC_FILE_CD, DOC_PROOF_FILE_CD, USER_ID, DOCUMENT_DESC, TEMPLATE_BODY)
            VALUES (#seqQry.nextval#
                    ,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINSTANCE_ID()#" maxLength="22" />
                    ,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_CONFIRMED_USER_ID()#" maxLength="22" null="#iif((arguments.bean.getDOC_CONFIRMED_USER_ID() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getDOC_CONFIRMED_DT()#" maxLength="26" null="#iif((arguments.bean.getDOC_CONFIRMED_DT() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_PUBLISHED_USER_ID()#" maxLength="22" null="#iif((arguments.bean.getDOC_PUBLISHED_USER_ID() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getDOC_PUBLISHED_DT()#" maxLength="26" null="#iif((arguments.bean.getDOC_PUBLISHED_DT() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getDOC_DT()#" maxLength="26" null="#iif((arguments.bean.getDOC_DT() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_char" value="#arguments.bean.getIS_CURRENT()#" maxLength="1" null="#iif((arguments.bean.getIS_CURRENT() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_SUBMITTED_USER_ID()#" maxLength="22" null="#iif((arguments.bean.getDOC_SUBMITTED_USER_ID() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getDOC_SUBMITTED_DT()#" maxLength="26" null="#iif((arguments.bean.getDOC_SUBMITTED_DT() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getDOC_SIGNATURE()#" maxLength="200" null="#iif((arguments.bean.getDOC_SIGNATURE() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_FILE_CD()#" maxLength="22" null="#iif((arguments.bean.getDOC_FILE_CD() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getDOC_PROOF_FILE_CD()#" maxLength="22" null="#iif((arguments.bean.getDOC_PROOF_FILE_CD() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getUSER_ID()#" maxLength="22" null="#iif((arguments.bean.getUSER_ID() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getDOCUMENT_DESC()#" maxLength="4000" null="#iif((arguments.bean.getDOCUMENT_DESC() eq ''),de("true"), de("false"))#" />
                    ,<cfqueryparam cfsqltype="cf_sql_clob" value="#arguments.bean.getTEMPLATE_BODY()#" null="#iif((arguments.bean.getTEMPLATE_BODY() eq ''),de("true"), de("false"))#" />)
        </cfquery>

        <cfreturn seqQry.nextval />
    </cffunction>

    <cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
        <cfargument name="bean" type="DocSnapshotBaseVO" required="true" />
        <cfset var updateQry = "" />

        <cfquery name="updateQry" datasource="#getDatasource()#">
            UPDATE DOC_SNAPSHOT_BASE
            SET
                DOC_CONFIRMED_USER_ID = <cfqueryparam value="#arguments.bean.getDOC_CONFIRMED_USER_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDOC_CONFIRMED_USER_ID() eq ''),de("true"), de("false"))#" />,
                DOC_CONFIRMED_DT = <cfqueryparam value="#arguments.bean.getDOC_CONFIRMED_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getDOC_CONFIRMED_DT() eq ''),de("true"), de("false"))#" />,
                DOC_PUBLISHED_USER_ID = <cfqueryparam value="#arguments.bean.getDOC_PUBLISHED_USER_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDOC_PUBLISHED_USER_ID() eq ''),de("true"), de("false"))#" />,
                DOC_PUBLISHED_DT = <cfqueryparam value="#arguments.bean.getDOC_PUBLISHED_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getDOC_PUBLISHED_DT() eq ''),de("true"), de("false"))#" />,
                DOC_DT = <cfqueryparam value="#arguments.bean.getDOC_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getDOC_DT() eq ''),de("true"), de("false"))#" />,
                IS_CURRENT = <cfqueryparam value="#arguments.bean.getIS_CURRENT()#" cfsqltype="cf_sql_char" maxLength="1" null="#iif((arguments.bean.getIS_CURRENT() eq ''),de("true"), de("false"))#" />,
                DOC_SUBMITTED_USER_ID = <cfqueryparam value="#arguments.bean.getDOC_SUBMITTED_USER_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDOC_SUBMITTED_USER_ID() eq ''),de("true"), de("false"))#" />,
                DOC_SUBMITTED_DT = <cfqueryparam value="#arguments.bean.getDOC_SUBMITTED_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getDOC_SUBMITTED_DT() eq ''),de("true"), de("false"))#" />,
                DOC_SIGNATURE = <cfqueryparam value="#arguments.bean.getDOC_SIGNATURE()#" cfsqltype="cf_sql_varchar" maxLength="200" null="#iif((arguments.bean.getDOC_SIGNATURE() eq ''),de("true"), de("false"))#" />,
                MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
                MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />,
				DOC_FILE_CD = <cfqueryparam value="#arguments.bean.getDOC_FILE_CD()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDOC_FILE_CD() eq ''),de("true"), de("false"))#" />,
                DOC_PROOF_FILE_CD = <cfqueryparam value="#arguments.bean.getDOC_PROOF_FILE_CD()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getDOC_PROOF_FILE_CD() eq ''),de("true"), de("false"))#" />,
                USER_ID = <cfqueryparam value="#arguments.bean.getUSER_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getUSER_ID() eq ''),de("true"), de("false"))#" />,
                DOCUMENT_DESC = <cfqueryparam value="#arguments.bean.getDOCUMENT_DESC()#" cfsqltype="cf_sql_varchar" maxLength="4000" null="#iif((arguments.bean.getDOCUMENT_DESC() eq ''),de("true"), de("false"))#" />,
                TEMPLATE_BODY = <cfqueryparam value="#arguments.bean.getTEMPLATE_BODY()#" cfsqltype="cf_sql_clob" null="#iif((arguments.bean.getTEMPLATE_BODY() eq ''),de("true"), de("false"))#" />
                
                <cfif arguments.bean.getDOC_LETTER_DATA_CD() NEQ "">
	               , DOC_LETTER_DATA_CD = <cfqueryparam value="#arguments.bean.getDOC_LETTER_DATA_CD()#" cfsqltype="cf_sql_numeric" null="#iif((arguments.bean.getDOC_LETTER_DATA_CD() eq ''),de("true"), de("false"))#" />
                </cfif>
            WHERE
                DOC_SNAPSHOT_CD = <cfqueryparam value="#arguments.bean.getDOC_SNAPSHOT_CD()#" cfsqltype="CF_SQL_NUMERIC" />
        </cfquery>
	</cffunction>

    <cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
        <cfargument name="docSnapshotCD" type="numeric" required="true" />

        <cfset var delQry = "">
        <cfquery name="delQry" datasource="#getDatasource()#">
            DELETE FROM DOC_SNAPSHOT_BASE
            WHERE
                DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docSnapshotCD#"/>
        </cfquery>
    </cffunction>

</cfcomponent>