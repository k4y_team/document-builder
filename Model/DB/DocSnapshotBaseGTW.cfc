<cfcomponent name="DocSnapshotBaseGTW" displayname="DocSnapshotBaseGTW" output="false" hint="" extends="dbObject">
	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_SNAPSHOT_CD,
				DOC_CONFIRMED_USER_ID,
				DOC_CONFIRMED_DT,
				DOC_PUBLISHED_USER_ID,
				DOC_PUBLISHED_DT,
				DOC_DT,
				IS_CURRENT,
				DOC_SUBMITTED_USER_ID,
				DOC_SUBMITTED_DT,
				DOC_SIGNATURE,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INSTANCE_ID,
				DOC_PROOF_FILE_CD,
				DOC_FILE_CD,
			FROM DOC_SNAPSHOT_BASE
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="docSnapshotCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_SNAPSHOT_CD,
				INSTANCE_ID,
				DOC_CONFIRMED_USER_ID,
				DOC_CONFIRMED_DT,
				DOC_PUBLISHED_USER_ID,
				DOC_PUBLISHED_DT,
				DOC_DT,
				IS_CURRENT,
				DOC_SUBMITTED_USER_ID,
				DOC_SUBMITTED_DT,
				DOC_SIGNATURE,
				DOC_FILE_CD,
				DOC_PROOF_FILE_CD,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				USER_ID,
				DOCUMENT_DESC,
				TEMPLATE_BODY,
				PROCESS_ID,
				IS_PROCESSING,
				METADATA,
				DOC_LETTER_TEMPLATE_CD,
				DOC_LETTER_TYPE_CD,
				DOC_LETTER_DATA_CD,
				DOC_CHANGE_TYPE_CD,
				DOC_SETUP_ID
			FROM DOC_SNAPSHOT_BASE
			WHERE
				DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getSignatures" access="public" returntype="query" output="false" displayname="getSignatures" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="docSnapshotCD" type="numeric" required="true" />

		<cfset var getQry = ""/>
		<cfset var docSignatureBL = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.docSignature").init(arguments.instanceID)>
		<cfset var docSignatureFolder = docSignatureBL.getSignaturesFolderPath()>

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				A.DOC_SNAPSHOT_CD,
				TO_CHAR(A.DOC_SUBMITTED_DT,'dd-Mon-yyyy') AS SIGNATURE_PROOF_DATE,
				NVL(A.DOC_SIGNATURE, NVL2(A.DOC_PROOF_FILE_CD, 'Submitted on behalf', '')) AS SIGNATURE_PROOF,
				'<img width="150" src="#application.cbcontroller.getSetting("modulesApplicationURL")#/modules/AgreementLetters/' || '#docSignatureFolder#' || '/' || D.FILE_NAME || '"/>' AS SIGNATURE,
				NVL2(A.DOC_SIGNATURE, '(Digital Signature)', '') AS SIGNATURE_TYPE
			FROM
				DOC_SNAPSHOT_BASE A,
				DOC_SNAPSHOT_DETAIL B,
				DOC_LETTER_TEMPLATE_SIGNATURE C,
				DOC_SIGNATURE D
			WHERE
				A.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#" /> AND
				B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD AND
				C.DOC_LETTER_TEMPLATE_CD(+) = B.DOC_LETTER_TEMPLATE_CD AND
				D.DOC_SIGNATURE_CD(+) = C.DOC_SIGNATURE_CD
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

</cfcomponent>