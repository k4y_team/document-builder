<cfcomponent name="DocSnapshotBaseVO" displayname="DocSnapshotBaseVO" extends="getSetObjectValue" output="false">
	<cfproperty name="DOC_SNAPSHOT_CD" type="numeric" />
	<cfproperty name="DOC_CONFIRMED_USER_ID" type="numeric" />
	<cfproperty name="DOC_CONFIRMED_DT" type="date" />
	<cfproperty name="DOC_PUBLISHED_USER_ID" type="numeric" />
	<cfproperty name="DOC_PUBLISHED_DT" type="date" />
	<cfproperty name="DOC_DT" type="date" />
	<cfproperty name="IS_CURRENT" type="string" />
	<cfproperty name="DOC_SUBMITTED_USER_ID" type="numeric" />
	<cfproperty name="DOC_SUBMITTED_DT" type="date" />
	<cfproperty name="DOC_SIGNATURE" type="string" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="MODIFICATION_ID" type="numeric" />
	<cfproperty name="MODIFICATION_DT" type="date" />
	<cfproperty name="INSTANCE_ID" type="numeric" />
	<cfproperty name="DOC_FILE_CD" type="numeric" />
	<cfproperty name="DOC_PROOF_FILE_CD" type="numeric" />
    <cfproperty name="USER_ID" type="numeric" />
    <cfproperty name="DOCUMENT_DESC" type="string" />
    <cfproperty name="TEMPLATE_BODY" type="string" />
    <cfproperty name="DOC_LETTER_DATA_CD" type="numeric" />

    <cffunction name="init" access="public" returntype="DocSnapshotBaseVO" output="false" displayname="init" hint="I initialize a DocSnapshotBase">
        <cfset super.init() />
        <cfset variables.DOC_SNAPSHOT_CD = "" />
        <cfset variables.DOC_CONFIRMED_USER_ID = "" />
        <cfset variables.DOC_CONFIRMED_DT = "" />
        <cfset variables.DOC_PUBLISHED_USER_ID = "" />
        <cfset variables.DOC_PUBLISHED_DT = "" />
        <cfset variables.DOC_DT = "" />
        <cfset variables.IS_CURRENT = "" />
        <cfset variables.DOC_SUBMITTED_USER_ID = "" />
        <cfset variables.DOC_SUBMITTED_DT = "" />
        <cfset variables.DOC_SIGNATURE = "" />
        <cfset variables.CREATION_ID = "" />
        <cfset variables.CREATION_DT = "" />
        <cfset variables.MODIFICATION_ID = "" />
        <cfset variables.MODIFICATION_DT = "" />
        <cfset variables.INSTANCE_ID = "" />
        <cfset variables.DOC_FILE_CD = "" />
        <cfset variables.DOC_PROOF_FILE_CD = "" />
        <cfset variables.USER_ID = "" />
        <cfset variables.DOCUMENT_DESC = "" />
        <cfset variables.TEMPLATE_BODY = "" />
        <cfset variables.DOC_LETTER_DATA_CD = "" />

        <cfreturn this />
    </cffunction>

    <cffunction name="setDOC_SNAPSHOT_CD" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
                <cfset variables.DOC_SNAPSHOT_CD = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid numeric!"/>
            </cfif>
    </cffunction>
    <cffunction name="getDOC_SNAPSHOT_CD" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_SNAPSHOT_CD />
    </cffunction>

    <cffunction name="setDOC_CONFIRMED_USER_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
                <cfset variables.DOC_CONFIRMED_USER_ID = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid numeric!"/>
            </cfif>
    </cffunction>
    <cffunction name="getDOC_CONFIRMED_USER_ID" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_CONFIRMED_USER_ID />
    </cffunction>

    <cffunction name="setDOC_CONFIRMED_DT" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isDate(arguments.val) OR arguments.val EQ ''>
                <cfset variables.DOC_CONFIRMED_DT = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid date!"/>
            </cfif>
    </cffunction>
    <cffunction name="getDOC_CONFIRMED_DT" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_CONFIRMED_DT />
    </cffunction>

    <cffunction name="setDOC_PUBLISHED_USER_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
                <cfset variables.DOC_PUBLISHED_USER_ID = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid numeric!"/>
            </cfif>
    </cffunction>
    <cffunction name="getDOC_PUBLISHED_USER_ID" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_PUBLISHED_USER_ID />
    </cffunction>

    <cffunction name="setDOC_PUBLISHED_DT" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isDate(arguments.val) OR arguments.val EQ ''>
                <cfset variables.DOC_PUBLISHED_DT = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid date!"/>
            </cfif>
    </cffunction>
    <cffunction name="getDOC_PUBLISHED_DT" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_PUBLISHED_DT />
    </cffunction>

    <cffunction name="setDOC_DT" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isDate(arguments.val) OR arguments.val EQ ''>
                <cfset variables.DOC_DT = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid date!"/>
            </cfif>
    </cffunction>
    <cffunction name="getDOC_DT" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_DT />
    </cffunction>

    <cffunction name="setIS_CURRENT" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfset variables.IS_CURRENT = arguments.val />
    </cffunction>
    <cffunction name="getIS_CURRENT" access="public" returntype="any" output="false">
        <cfreturn variables.IS_CURRENT />
    </cffunction>

    <cffunction name="setDOC_SUBMITTED_USER_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
                <cfset variables.DOC_SUBMITTED_USER_ID = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid numeric!"/>
            </cfif>
    </cffunction>
    <cffunction name="getDOC_SUBMITTED_USER_ID" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_SUBMITTED_USER_ID />
    </cffunction>

    <cffunction name="setDOC_SUBMITTED_DT" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isDate(arguments.val) OR arguments.val EQ ''>
                <cfset variables.DOC_SUBMITTED_DT = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid date!"/>
            </cfif>
    </cffunction>
    <cffunction name="getDOC_SUBMITTED_DT" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_SUBMITTED_DT />
    </cffunction>

    <cffunction name="setDOC_SIGNATURE" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfset variables.DOC_SIGNATURE = arguments.val />
    </cffunction>
    <cffunction name="getDOC_SIGNATURE" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_SIGNATURE />
    </cffunction>

    <cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
                <cfset variables.CREATION_ID = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid numeric!"/>
            </cfif>
    </cffunction>
    <cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
        <cfreturn variables.CREATION_ID />
    </cffunction>

    <cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isDate(arguments.val) OR arguments.val EQ ''>
                <cfset variables.CREATION_DT = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid date!"/>
            </cfif>
    </cffunction>
    <cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
        <cfreturn variables.CREATION_DT />
    </cffunction>

    <cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
                <cfset variables.MODIFICATION_ID = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid numeric!"/>
            </cfif>
    </cffunction>
    <cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
        <cfreturn variables.MODIFICATION_ID />
    </cffunction>

    <cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isDate(arguments.val) OR arguments.val EQ ''>
                <cfset variables.MODIFICATION_DT = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid date!"/>
            </cfif>
    </cffunction>
    <cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
        <cfreturn variables.MODIFICATION_DT />
    </cffunction>

    <cffunction name="setINSTANCE_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
                <cfset variables.INSTANCE_ID = val/>
            <cfelse>
                <cfthrow message="'#arguments.val#' is not a valid numeric!"/>
            </cfif>
    </cffunction>
    <cffunction name="getINSTANCE_ID" access="public" returntype="any" output="false">
        <cfreturn variables.INSTANCE_ID />
    </cffunction>

    <cffunction name="setDOC_FILE_CD" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.DOC_FILE_CD = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
    </cffunction>
    <cffunction name="getDOC_FILE_CD" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_FILE_CD />
    </cffunction>

    <cffunction name="setDOC_PROOF_FILE_CD" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.DOC_PROOF_FILE_CD = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
    </cffunction>
    <cffunction name="getDOC_PROOF_FILE_CD" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_PROOF_FILE_CD />
    </cffunction>

    <cffunction name="setUSER_ID" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
            <cfset variables.USER_ID = val/>
        <cfelse>
            <cfthrow message="'#arguments.val#' is not a valid numeric!"/>
        </cfif>
    </cffunction>
    <cffunction name="getUSER_ID" access="public" returntype="any" output="false">
        <cfreturn variables.USER_ID />
    </cffunction>

     <cffunction name="setDOCUMENT_DESC" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfset variables.DOCUMENT_DESC = arguments.val />
    </cffunction>
    <cffunction name="getDOCUMENT_DESC" access="public" returntype="any" output="false">
        <cfreturn variables.DOCUMENT_DESC />
    </cffunction>

    <cffunction name="setTEMPLATE_BODY" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfset variables.TEMPLATE_BODY = arguments.val />
    </cffunction>
    <cffunction name="getTEMPLATE_BODY" access="public" returntype="any" output="false">
        <cfreturn variables.TEMPLATE_BODY />
    </cffunction>

    <cffunction name="setDOC_LETTER_DATA_CD" access="public" returntype="void" output="false">
        <cfargument name="val" type="any" required="true" />

        <cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.DOC_LETTER_DATA_CD = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
    </cffunction>
    <cffunction name="getDOC_LETTER_DATA_CD" access="public" returntype="any" output="false">
        <cfreturn variables.DOC_LETTER_DATA_CD />
    </cffunction>

</cfcomponent>