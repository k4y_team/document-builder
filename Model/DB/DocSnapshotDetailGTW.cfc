<cfcomponent name="DocSnapshotDetailGTW" displayname="DocSnapshotDetailGTW" output="false" hint="" extends="dbObject">

	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_LETTER_TYPE_CD,
				DOC_TYPE_CD,
				STUDENT_ID,
				TRAINING_SESSION_CD,
				TRAINEE_TYPE_CD,
				DOC_LETTER_DATA_CD,
				DOC_SNAPSHOT_CD,
				DOC_LETTER_TEMPLATE_CD,
				DOC_CHANGE_TYPE_CD,
				CREATION_DT
			FROM DOC_SNAPSHOT_DETAIL
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="docSnapshotCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				A.DOC_LETTER_TYPE_CD,
				A.DOC_TYPE_CD,
				A.STUDENT_ID,
				A.TRAINING_SESSION_CD,
				A.TRAINEE_TYPE_CD,
				A.DOC_LETTER_DATA_CD,
				A.DOC_SNAPSHOT_CD,
				A.DOC_LETTER_TEMPLATE_CD,
				A.DOC_CHANGE_TYPE_CD,
				B.RESTRICTION_CONDITION_ID,
				A.CREATION_DT,
                C.DOC_FILE_CD
			FROM
				DOC_SNAPSHOT_DETAIL A,
				DOC_TYPE B,
				DOC_SNAPSHOT_BASE C
			WHERE
				A.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#" />
                AND A.DOC_SNAPSHOT_CD = C.DOC_SNAPSHOT_CD (+)
				AND A.DOC_TYPE_CD = B.DOC_TYPE_CD (+)
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getDataByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="docSnapshotCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_SNAPSHOT_DATA_CD,
				DOC_SNAPSHOT_CD,
				KEY,
				VALUE
			FROM
				DOC_SNAPSHOT_DATA A

			WHERE
				A.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>
	<cffunction name="getChangesByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="docSnapshotCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				* FROM
			DOC_SNAPSHOT_CHANGES A
				WHERE
					A.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getSnapshots" access="public" returntype="query" output="false" displayname="getSnapshots" hint="">
		<cfargument name="docSnapshotList" type="string" required="false" default=""/>
		<cfargument name="instanceID" type="numeric" required="false" default="-1">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				SB.DOC_SNAPSHOT_CD,
				SB.DOC_CONFIRMED_USER_ID,
				SB.DOC_CONFIRMED_DT,
				SB.DOC_PUBLISHED_USER_ID,
				SB.DOC_PUBLISHED_DT,
				SB.DOC_DT,
				SB.IS_CURRENT,
				SB.DOC_SUBMITTED_USER_ID,
				SB.DOC_SUBMITTED_DT,
				SB.DOC_SIGNATURE,
				SB.DOC_FILE_CD,
				SB.DOC_PROOF_FILE_CD,
				SD.STUDENT_ID,
				SD.TRAINING_SESSION_CD,
				SD.DOC_TYPE_CD,
				SD.DOC_LETTER_TEMPLATE_CD,
				SD.DOC_LETTER_TYPE_CD,
				SD.DOC_CHANGE_TYPE_CD,
				SD.TRAINEE_TYPE_CD,
				SD.DOC_LETTER_DATA_CD,
				SU.LAST_NAME || ',' || SU.FIRST_NAME AS SUBMITTED_BY,
				<!--- DOC_LETTER_TYPE used for render templates --->
				DECODE(LT.LETTER_TYPE_CODE, 'REVISED', LT.LETTER_TYPE_DESC, NULL) AS LETTER_TYPE,
				S.FIRST_NAME AS STUDENT_FNAME,
				S.LAST_NAME AS STUDENT_LNAME
			FROM
				DOC_SNAPSHOT_BASE SB,
			    DOC_SNAPSHOT_DETAIL SD,
			    DOC_LETTER_TYPE LT,
			    UD_USER SU,
			    STUDENT S
			WHERE
				SB.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" list="true" value="#arguments.docSnapshotList#" />)
				<cfif arguments.instanceID neq "-1">
					AND SB.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_INTEGER"  value="#arguments.instanceID#" />
				</cfif>
				AND SB.DOC_SNAPSHOT_CD = SD.DOC_SNAPSHOT_CD
				AND SB.DOC_SUBMITTED_USER_ID = SU.USER_ID (+)
				AND LT.DOC_LETTER_TYPE_CD = SD.DOC_LETTER_TYPE_CD
				AND SD.STUDENT_ID = S.STUDENT_ID
		</cfquery>

		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getAllByStudentID" access="public" returntype="query" output="false" displayname="getAllByStudentID" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="studentId" type="numeric" required="true" />

		<cfset var loaQuery = "" />

		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
				B.DOC_SNAPSHOT_CD,
				B.START_YEAR,
				B.END_YEAR,
				B.DOC_DT,
				B.DOC_DUE_DATE,
				B.LETTER_TYPE_DESC,
				B.DOC_SIGNATURE,
				B.DOC_SUBMITTED_USER_ID,
				B.DOC_SUBMITTED_DT,
				B.DOC_FILE_CD,
				B.DOC_PROOF_FILE_CD,
				B.TRAINING_SESSION_CD
			FROM
				DOC_CURRENT_VW A,
				DOC_INFO_VW B
			WHERE
				A.STUDENT_ID 	= <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentId#" />
				AND B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
				AND B.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
			ORDER BY
				START_YEAR DESC
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getSnapshotByDocType" access="public" returntype="query" output="false" displayname="getSnapshotByDocType" hint="">
        <cfargument name="instanceID" type="numeric" required="true" />
        <cfargument name="docTypeCd" type="numeric" required="true" />
		<cfargument name="docLetterTemplateCd" type="numeric" required="true" />

        <cfset var loaQuery = "" />
        <cfquery name="loaQuery" datasource="#getDatasource()#">
            SELECT
			    B.*
			FROM
			    DOC_SNAPSHOT_DETAIL A,
			    DOC_INFO_VW B
			WHERE
			    B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
			    AND A.DOC_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docTypeCd#" />
				AND A.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docLetterTemplateCd#" />
			    AND B.INSTANCE_ID =  <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
        </cfquery>
        <cfreturn loaQuery />
    </cffunction>

	<cffunction name="getGenerated" access="public" returntype="query" output="false" displayname="getGenerated" hint="">
		<cfargument name="instanceID" type="numeric" required="true"/>
		<cfargument name="trainingSessionCd" type="numeric" required="false" default="-1"/>
		<cfargument name="studentId" type="numeric" required="true" />

		<cfset var loaQuery = "" />

		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
				B.DOC_SNAPSHOT_CD,
				B.START_YEAR,
				B.END_YEAR,
				B.DOC_DT,
				B.DOC_DUE_DATE,
				B.LETTER_TYPE_DESC,
				B.DOC_SIGNATURE,
				B.DOC_SUBMITTED_USER_ID,
				B.DOC_SUBMITTED_DT,
				B.DOC_FILE_CD,
				B.DOC_PROOF_FILE_CD,
				B.TRAINING_SESSION_CD,
				DECODE(SU.USER_ID,NULL,'System',SU.LAST_NAME || ',' || SU.FIRST_NAME) AS GENERATED_BY,
				B.GENERATED_DT,
				B.TYPE_DESC
			FROM
				DOC_GENERATED_VW A,
				DOC_INFO_VW B,
				UD_USER SU
			WHERE
				A.STUDENT_ID 	= <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentId#" />
				<cfif arguments.trainingSessionCd neq -1>
				AND A.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.trainingSessionCd#" />
				</cfif>
				AND B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
				AND B.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
				AND B.GENERATED_USER_ID = SU.USER_ID (+)
			ORDER BY
				START_YEAR DESC
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getCurrentLOA" access="public" returntype="query" output="false" displayname="getCurrentLOA" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="trainingSessionCd" type="numeric" required="true" />
		<cfargument name="studentId" type="numeric" required="true" />
		<cfargument name="letterTemplateCD" type="numeric" required="false" default="-1" />

		<cfset var loaQuery = "" />

		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
				B.TYPE_DESC AS DOC_TEMPLATE,
				B.LETTER_TYPE_DESC AS DOC_TYPE,
				B.DOC_LETTER_TYPE_CD,
				B.EMAIL_DT,
				B.DOC_TYPE_CD,
				B.STUDENT_ID,
				B.TRAINING_SESSION_CD,
				B.START_YEAR,
				B.END_YEAR,
				B.TRAINEE_TYPE_CD,
				B.DOC_LETTER_DATA_CD,
				B.DOC_SNAPSHOT_CD,
				B.DOC_LETTER_TEMPLATE_CD,
				B.DOC_SIGNATURE,
				B.DOC_DT,
				B.DOC_SUBMITTED_DT,
				B.LAST_REMINDER_DT,
				B.DOC_DUE_DATE,
				B.CNT_NOTIFICATION,
				B.DOC_FILE_CD,
				B.DOC_PROOF_FILE_CD,
				B.DOC_PUBLISHED_DT,
				B.DOC_CONFIRMED_DT,
				B.DOC_SIGN_STATUS,
				B.FIRST_NAME,
				B.LAST_NAME,
				B.TYPE_DESC,
				B.TEMPLATE_REVISED_DATE,
				B.ACTION_NAME,
				B.ACTION_CODE
			FROM
				DOC_CURRENT_VW A,
				DOC_INFO_VW B
			WHERE
				A.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.trainingSessionCd#" />
				AND A.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentID#" />
				AND B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
				AND A.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
				<cfif arguments.letterTemplateCD gt 0>
					AND B.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.letterTemplateCD#" />
				</cfif>
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getUserCurrentLOA" access="public" returntype="query" output="false" displayname="getUserCurrentLOA" hint="">
		<cfargument name="userID" type="numeric" required="true" />
		<cfargument name="docLetterTemplateCD" type="numeric" required="true" />
		<cfargument name="trainingSessionCd" type="numeric" required="false" default="-1" />

		<cfset var loaQuery = "" />

		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
			  B.TYPE_DESC 		AS DOC_TEMPLATE,
			  B.LETTER_TYPE_DESC	AS DOC_TYPE,
			  B.DOC_LETTER_TYPE_CD,
			  B.EMAIL_DT,
			  B.DOC_TYPE_CD,
			  B.STUDENT_ID,
			  B.TRAINING_SESSION_CD,
			  B.START_YEAR,
			  B.END_YEAR,
			  B.TRAINEE_TYPE_CD,
			  B.DOC_LETTER_DATA_CD,
			  B.DOC_SNAPSHOT_CD,
			  B.DOC_LETTER_TEMPLATE_CD,
			  B.DOC_SIGNATURE,
			  B.DOC_DT,
			  B.DOC_SUBMITTED_DT,
			  B.LAST_REMINDER_DT,
			  B.DOC_DUE_DATE,
			  B.CNT_NOTIFICATION,
			  B.DOC_FILE_CD,
			  B.DOC_PROOF_FILE_CD,
			  B.DOC_PUBLISHED_DT,
			  B.DOC_CONFIRMED_DT,
			  B.DOC_SIGN_STATUS,
			  B.FIRST_NAME,
			  B.LAST_NAME,
			  B.TYPE_DESC,
			  B.TEMPLATE_REVISED_DATE
			FROM
				DOC_CURRENT_VW A,
				DOC_INFO_VW B,
				DOC_SNAPSHOT_BASE C
			WHERE
				B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
				AND C.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
				AND C.USER_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.userID#" />
				AND B.DOC_LETTER_TEMPLATE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docLetterTemplateCD#" />
				<cfif arguments.trainingSessionCd neq -1>
				AND A.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.trainingSessionCd#" />
				</cfif>
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getStudentLOA" access="public" returntype="query" output="false" displayname="getStudentLOA" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="studentId" type="numeric" required="true" />
		<cfargument name="trainingSessionCd" type="numeric" required="false" />

		<cfset var loaQuery = "" />

		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
				B.TYPE_DESC 		AS DOC_TEMPLATE,
				B.LETTER_TYPE_DESC	AS DOC_TYPE,
				B.DOC_LETTER_TYPE_CD,
				B.EMAIL_DT,
				B.DOC_TYPE_CD,
				B.STUDENT_ID,
				B.TRAINING_SESSION_CD,
				B.START_YEAR,
				B.END_YEAR,
				B.TRAINEE_TYPE_CD,
				B.DOC_LETTER_DATA_CD,
				B.DOC_SNAPSHOT_CD,
				B.DOC_LETTER_TEMPLATE_CD,
				B.DOC_SIGNATURE,
				B.DOC_DT,
				B.DOC_SUBMITTED_DT,
				B.LAST_REMINDER_DT,
				B.DOC_DUE_DATE,
				B.CNT_NOTIFICATION,
				B.DOC_FILE_CD,
				B.DOC_PROOF_FILE_CD,
				B.DOC_PUBLISHED_DT,
				B.DOC_CONFIRMED_DT,
				B.DOC_SIGN_STATUS,
				B.FIRST_NAME,
				B.LAST_NAME,
				B.TYPE_DESC,
				B.TEMPLATE_REVISED_DATE
			FROM
				DOC_CURRENT_VW A,
				DOC_INFO_VW B
			WHERE
				A.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentId#" />
				<cfif isDefined("arguments.trainingSessionCd")>
					AND B.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.trainingSessionCd#" />
				</cfif>
				AND B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
				AND B.INSTANCE_ID = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.instanceID#" />
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getStdPublishedLOA" access="public" returntype="query" output="false" displayname="getStudentLOA" hint="">
		<cfargument name="studentId" type="numeric" required="true" />
		<cfargument name="docSnapshotCD" type="numeric" required="false" default="-1" />

		<cfset var loaQuery = "" />
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>
		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
				B.TYPE_DESC 		AS DOC_TEMPLATE,
				B.LETTER_TYPE_DESC	AS DOC_TYPE,
				B.DOC_LETTER_TYPE_CD,
				B.EMAIL_DT,
				B.DOC_TYPE_CD,
				B.STUDENT_ID,
				B.TRAINING_SESSION_CD,
				B.START_YEAR,
				B.END_YEAR,
				B.TRAINEE_TYPE_CD,
				B.DOC_LETTER_DATA_CD,
				B.DOC_SNAPSHOT_CD,
				B.DOC_LETTER_TEMPLATE_CD,
				B.LETTER_TEMPLATE_NAME,
				B.DOC_SIGNATURE,
				B.DOC_DT,
				B.DOC_SUBMITTED_DT,
				B.LAST_REMINDER_DT,
				B.DOC_DUE_DATE,
				B.CNT_NOTIFICATION,
				B.DOC_FILE_CD,
				B.DOC_PROOF_FILE_CD,
				B.DOC_PUBLISHED_DT,
				B.DOC_CONFIRMED_DT,
				B.DOC_SIGN_STATUS,
				B.FIRST_NAME,
				B.LAST_NAME,
				B.TYPE_DESC,
				B.TEMPLATE_REVISED_DATE,
				B.INSTANCE_ID,
				B.ACTION_NAME,
				B.ACTION_CODE,
				B.INSTRUCTIONS,
				B.ENABLE_PUBLISH,
				B.ENABLE_SEND_EMAIL,
				LSB.DOCUMENT_DESC
			FROM
				DOC_PUBLISHED_VW A,
				DOC_INFO_VW B,
				DOC_SNAPSHOT_BASE LSB
			WHERE
				A.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentId#" />
				AND B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
				AND LSB.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
				AND B.IS_CURRENT = 'Y'
				<cfif arguments.docSnapshotCD neq -1>
				AND B.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#" />
				</cfif>
				<cfif instanceResList neq "" and instanceResList neq "*">
					AND B.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
				</cfif>
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getStdUnpublishedLOA" access="public" returntype="query" output="false" displayname="getStdUnpublishedLOA" hint="">
		<cfargument name="studentId" type="numeric" required="true" />
		<cfargument name="onlyPublished" type="boolean" required="false" default="true" />
		<cfargument name="docSnapshotCD" type="numeric" required="false" default="-1" />
		<cfargument name="trainingSessionCD" type="numeric" required="false" default="-1" />

		<cfset var loaQuery = "" />
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>

		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
				A.*,
				NVL2(A.DOC_PUBLISHED_DT, 'Published', 'Not Published') AS PUBLISHED_STATUS,
				NVL2(A.DOC_SUBMITTED_DT, 'Signed', 'Not Signed') AS SIGNED_STATUS
			FROM (
				SELECT
					DECODE(LSB.MODIFICATION_ID, NULL, UC.LAST_NAME || ', ' || UC.FIRST_NAME, UM.LAST_NAME || ', ' || UM.FIRST_NAME) AS GENERATED_BY,
					DECODE(LSB.MODIFICATION_DT, NULL, LSB.CREATION_DT, LSB.MODIFICATION_DT) AS GENERATION_DT,
					B.TYPE_DESC 		AS DOC_TEMPLATE,
					B.LETTER_TYPE_DESC	AS DOC_TYPE,
					B.DOC_LETTER_TYPE_CD,
					B.EMAIL_DT,
					B.DOC_TYPE_CD,
					B.STUDENT_ID,
					B.TRAINING_SESSION_CD,
					B.START_YEAR,
					B.END_YEAR,
					B.TRAINEE_TYPE_CD,
					B.DOC_LETTER_DATA_CD,
					B.DOC_SNAPSHOT_CD,
					B.DOC_LETTER_TEMPLATE_CD,
					B.LETTER_TEMPLATE_NAME,
					B.DOC_SIGNATURE,
					B.DOC_DT,
					B.DOC_SUBMITTED_DT,
					B.LAST_REMINDER_DT,
					B.DOC_DUE_DATE,
					NVL(B.CNT_NOTIFICATION, 0) AS CNT_NOTIFICATION,
					B.DOC_FILE_CD,
					B.DOC_PROOF_FILE_CD,
					B.DOC_PUBLISHED_DT,
					B.DOC_CONFIRMED_DT,
					B.DOC_SIGN_STATUS,
					B.FIRST_NAME,
					B.LAST_NAME,
					B.TYPE_DESC,
					B.TEMPLATE_REVISED_DATE,
					B.INSTANCE_ID,
					B.ACTION_NAME,
					B.ACTION_CODE,
					B.ENABLE_PUBLISH,
					LSB.DOCUMENT_DESC,
					C.INSTANCE_NAME
				FROM
					DOC_SNAPSHOT_DETAIL A,
					DOC_INFO_VW B,
					DOC_SNAPSHOT_BASE LSB,
					DOC_INSTANCE C,
					(SELECT FIELD_VALUE AS TRAINING_SESSION_CD, DOC_SNAPSHOT_CD
					FROM DOC_SNAPSHOT_METADATA
					WHERE FIELD_NAME = 'TRAINING_SESSION_CD') D,
					UD_USER UC,
					UD_USER UM
				WHERE
					A.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentId#" />
					AND LSB.CREATION_ID = UC.USER_ID (+)
					AND LSB.MODIFICATION_ID = UM.USER_ID (+)
					AND B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
					AND LSB.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD
					AND B.INSTANCE_ID = C.INSTANCE_ID
					AND A.DOC_SNAPSHOT_CD = D.DOC_SNAPSHOT_CD (+)
					<cfif arguments.trainingSessionCD neq -1>
						AND D.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.trainingSessionCD#" />
					</cfif>
					<cfif arguments.docSnapshotCD neq -1>
						AND B.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#" />
					</cfif>
					<cfif instanceResList neq "" and instanceResList neq "*">
						AND C.INSTANCE_ID IN (<cfqueryparam value="#instanceResList#" cfsqltype="CF_SQL_NUMERIC" list="true"/>)
					</cfif>
				ORDER BY
					C.INSTANCE_ID ASC, DOCUMENT_DESC DESC, B.DOC_DT DESC
			) A
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getStdLOAInfo" access="public" returntype="query" output="false" displayname="getStudentLOA" hint="">
		<cfargument name="studentId" type="numeric" required="true" />
		<cfargument name="docSnapshotCD" type="numeric" required="true" />

		<cfset var loaQuery = "" />

		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
				B.TYPE_DESC 		AS DOC_TEMPLATE,
				B.LETTER_TYPE_DESC	AS DOC_TYPE,
				B.DOC_LETTER_TYPE_CD,
				B.EMAIL_DT,
				B.DOC_TYPE_CD,
				B.STUDENT_ID,
				B.TRAINING_SESSION_CD,
				B.START_YEAR,
				B.END_YEAR,
				B.TRAINEE_TYPE_CD,
				B.DOC_LETTER_DATA_CD,
				B.DOC_SNAPSHOT_CD,
				B.DOC_LETTER_TEMPLATE_CD,
				B.LETTER_TEMPLATE_NAME,
				B.DOC_SIGNATURE,
				B.DOC_DT,
				B.DOC_SUBMITTED_DT,
				B.LAST_REMINDER_DT,
				B.DOC_DUE_DATE,
				B.CNT_NOTIFICATION,
				B.DOC_FILE_CD,
				B.DOC_PROOF_FILE_CD,
				B.DOC_PUBLISHED_DT,
				B.DOC_CONFIRMED_DT,
				B.DOC_SIGN_STATUS,
				B.FIRST_NAME,
				B.LAST_NAME,
				B.TYPE_DESC,
				B.TEMPLATE_REVISED_DATE,
				B.INSTANCE_ID,
				B.ACTION_NAME,
				B.ACTION_CODE,
				B.INSTRUCTIONS,
				B.ENABLE_PUBLISH,
				B.ENABLE_SEND_EMAIL,
				LSB.DOCUMENT_DESC
			FROM
				DOC_INFO_VW B,
				DOC_SNAPSHOT_BASE LSB
			WHERE
				B.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentId#" />
				AND LSB.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
				AND B.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docSnapshotCD#" />
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getOutstanding" access="public" returntype="query" output="false" displayname="getOutstanding" hint="">
		<cfargument name="studentId" type="numeric" required="true" />

		<cfset var loaQuery = "" />
		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
              B.TRAINING_SESSION_CD,
              B.START_YEAR,
              B.END_YEAR,
              B.DOC_SNAPSHOT_CD,
              B.DOC_DT,
              B.DOC_DUE_DATE,
			  B.LETTER_TYPE_DESC,
			  B.LETTER_TYPE_CODE,
			  B.DOC_TYPE_CD,
			  'Y' AS ALLOW_DISPLAY
            FROM
                DOC_CURRENT_VW A,
                DOC_INFO_VW B
           WHERE
                A.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentId#" /> AND
                B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD AND
                B.DOC_SIGN_STATUS = 'OUTSTANDING'
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getSigned" access="public" returntype="query" output="false" displayname="getSigned" hint="">
		<cfargument name="studentId" type="numeric" required="true" />

		<cfset var loaQuery = "" />
		<cfquery name="loaQuery" datasource="#getDatasource()#">
			SELECT
              B.TRAINING_SESSION_CD,
              B.START_YEAR,
              B.END_YEAR,
              B.DOC_SNAPSHOT_CD,
              B.DOC_DT,
              B.DOC_DUE_DATE,
			  B.DOC_SIGN_STATUS,
			  B.DOC_TYPE_CD,
			  'Y' AS ALLOW_DISPLAY
            FROM
                DOC_CURRENT_VW A,
                DOC_INFO_VW B
           WHERE
                A.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.studentId#" /> AND
                B.DOC_SNAPSHOT_CD = A.DOC_SNAPSHOT_CD AND
                B.DOC_SIGN_STATUS <> 'OUTSTANDING'
		</cfquery>

		<cfreturn loaQuery />
	</cffunction>

	<cffunction name="getTraineePublished" access="public" returntype="query" output="false" displayname="getTraineePublished" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				A.DOC_SNAPSHOT_CD
			FROM DOC_PUBLISHED_VW A
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getHistory" access="public" returntype="query" output="false" displayname="getHistory" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="snapshotCd" type="numeric" required="true" />

		<cfset var getQry = ""/>

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				INFO.DOC_SNAPSHOT_CD,
				INFO.DOC_DT,
				INFO.LETTER_TYPE_DESC,
				INFO.DOC_SUBMITTED_DT,
				INFO.DOC_SUBMITTED_USER_ID,
				INFO.DOC_FILE_CD,
				INFO.DOC_PROOF_FILE_CD,
				INFO.EMAIL_DT,
				INFO.LAST_REMINDER_DT,
				INFO.CNT_NOTIFICATION,
				INFO.FIRST_NAME,
				INFO.LAST_NAME,
				INFO.DOC_SIGN_STATUS,
				INFO.PROGRAM_DESC,
				INFO.SUBPROGRAM_DESC,
				INFO.TRAINING_STATUS_DESC,
				INFO.TRAINING_LEVEL_DESC,
				INFO.EMPLOYMENT_TYPE_DESC,
				INFO.START_DT,
				INFO.END_DT,
				INFO.DOC_CONFIRMED_DT,
				INFO.LETTER_TYPE_DESC,
				INFO.TYPE_DESC,
				A.DOC_CONFIRMED_USER_ID,
				A.DOC_CHANGE_DESC,
				A.DOC_SIGN_STATUS,
				INFO.LETTER_TYPE_CODE,
				S1.FIRST_NAME || DECODE(S1.FIRST_NAME, NULL, NULL, DECODE(S1.LAST_NAME, NULL, NULL, ', ')) || S1.LAST_NAME AS CONFIRMED_BY,
				S2.FIRST_NAME || DECODE(S2.FIRST_NAME, NULL, NULL, DECODE(S2.LAST_NAME, NULL, NULL, ', ')) || S2.LAST_NAME AS SUBMITED_BY
			FROM
				DOC_INFO_VW A,
				DOC_PUBLISHED_VW B,
				DOC_INFO_VW INFO,
				UD_USER S1,
				UD_USER S2
			WHERE
				A.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCd#" />
				AND B.TRAINING_SESSION_CD = A.TRAINING_SESSION_CD
				AND B.STUDENT_ID = A.STUDENT_ID
				AND B.INSTANCE_ID = A.INSTANCE_ID
				AND INFO.INSTANCE_ID = B.INSTANCE_ID
				AND INFO.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
				AND INFO.DOC_CONFIRMED_USER_ID = S1.USER_ID(+)
				AND INFO.DOC_SUBMITTED_USER_ID = S2.USER_ID(+)
		  	ORDER BY INFO.DOC_DT DESC, A.START_DT ASC, A.END_DT ASC
		</cfquery>

		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getHistoryChanges" access="public" returntype="query" output="false" displayname="getHistoryChanges" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="snapshotCd" type="numeric" required="true" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				C.DOC_SNAPSHOT_CD,
				F.DOC_DT,
				E.PROGRAM_DESC,
				E.SUBPROGRAM_DESC,
				E.TRAINING_STATUS_DESC,
				E.TRAINING_LEVEL_DESC,
				E.STUDENT_TYPE_CD,
				E.STUDENT_TYPE_DESC,
				E.EMPLOYMENT_TYPE_DESC,
				E.START_DT,
				E.END_DT,
				H.DOC_CHANGE_DESC,
				G.LETTER_TYPE_DESC,
				D.LAST_NAME || ', ' || D.FIRST_NAME AS TRAINEE_NAME,
				D.STUDENT_NUMBER,
				D.OPHRDC_NUMBER,
				D.ADDRESS,
				D.ADDRESS2,
				D.CITY,
				D.PROVINCE,
				D.COUNTRY,
				D.POSTAL_CODE,
				D.CPSO_NUM,
				D.CPSO_TYPE_DESC,
				D.UNIVERSITY_DESC,
				D.UNIV_COUNTRY_DESC,
				D.DEGREE_YEAR,
				E.FUNDING_SOURCE_DESC,
				E.Pool,
				F.DOC_FILE_CD,
				F.DOC_PROOF_FILE_CD,
				F.DOC_SUBMITTED_DT,
				CASE
			      WHEN F.DOC_SUBMITTED_USER_ID IS NOT NULL
			      AND F.DOC_SUBMITTED_DT       IS NOT NULL
			      AND F.DOC_PROOF_FILE_CD    IS NULL
			      THEN 'SIGNED_BY_TRAINEE'
			      WHEN F.DOC_SUBMITTED_USER_ID IS NOT NULL
			      AND F.DOC_SUBMITTED_DT       IS NOT NULL
			      AND F.DOC_PROOF_FILE_CD    IS NOT NULL
			      THEN 'SIGNED_BY_STAFF'
			      ELSE 'OUTSTANDING'
    			END AS DOC_SIGN_STATUS
			FROM
				DOC_INFO_VW A,
				DOC_PUBLISHED_VW B,
				DOC_SNAPSHOT_DETAIL C,
				DOC_PROFILE_CHANGE D,
				DOC_DATA_CHANGE E,
				DOC_SNAPSHOT_BASE F,
				DOC_LETTER_TYPE G,
				DOC_CHANGE_TYPE H
			WHERE
				A.DOC_SNAPSHOT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.snapshotCd#" />
				AND B.TRAINING_SESSION_CD = A.TRAINING_SESSION_CD
				AND B.STUDENT_ID = A.STUDENT_ID
				AND B.INSTANCE_ID = A.INSTANCE_ID
				AND C.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
				AND C.DOC_LETTER_DATA_CD = D.DOC_LETTER_DATA_CD
				AND C.DOC_LETTER_DATA_CD = E.DOC_LETTER_DATA_CD
				AND C.DOC_SNAPSHOT_CD = F.DOC_SNAPSHOT_CD
				AND C.DOC_LETTER_TYPE_CD = G.DOC_LETTER_TYPE_CD (+)
				AND C.DOC_CHANGE_TYPE_CD = H.DOC_CHANGE_TYPE_CD (+)
		  	ORDER BY F.DOC_DT DESC, C.DOC_SNAPSHOT_CD DESC, E.START_DT ASC, E.END_DT ASC
		</cfquery>

		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getDocReportTrainees" access="public" returntype="query" output="false" displayname="getTrainee" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />
		<cfargument name="filterVO" type="medsisOld.modules.DocumentBuilder.Model.BL.LoaFilterFVO" required="true">
		<cfset var getQry = ""/>

		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
			 DISTINCT
				STUDENT_ID,
				FIRST_NAME,
				LAST_NAME,
				LAST_NAME || ', ' || FIRST_NAME AS TRAINEE_NAME
	        FROM
	        	DOC_INFO_VW
		    WHERE
		    		DOC_CONFIRMED_USER_ID IS NOT NULL
				    AND DOC_CONFIRMED_DT  IS NOT NULL
				<cfif arguments.filterVO.getTRAINING_SESSION_CD() NEQ "">
					AND TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterVO.getTRAINING_SESSION_CD()#" />
				</cfif>
				<cfif Len(arguments.filterVO.getTERM())>
				AND (
					UPPER(LAST_NAME) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#UCase(arguments.filterVO.getTERM())#%" /> OR
					UPPER(FIRST_NAME) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#UCase(arguments.filterVO.getTERM())#%" />
				)
				</cfif>

	        ORDER BY UPPER(LAST_NAME), UPPER(FIRST_NAME)
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getDocGeneratedTrainees" access="public" returntype="query" output="false" displayname="getDocGeneratedTrainees" hint="">
		<cfargument name="instanceID" type="numeric" required="true">
		<cfargument name="filterVO" type="medsisOld.modules.DocumentBuilder.Model.BL.LoaFilterFVO" required="true">
		<cfset var getQry = ""/>

		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
			 DISTINCT
				STUDENT_ID,
				FIRST_NAME,
				LAST_NAME,
				LAST_NAME || ', ' || FIRST_NAME AS TRAINEE_NAME
	        FROM
	        	DOC_INFO_VW V
		    WHERE
	    		<!--- V.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" /> AND	 --->
				V.DOC_PUBLISHED_USER_ID IS NULL
			    AND V.DOC_PUBLISHED_DT IS NULL
			<cfif arguments.filterVO.getTRAINING_SESSION_CD() NEQ "">
				AND TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterVO.getTRAINING_SESSION_CD()#" />
			</cfif>
			<cfif Len(arguments.filterVO.getSTUDENT_NAME())>
				AND (
					LOWER(TRIM(LAST_NAME)) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="#LCase(arguments.filterVO.getSTUDENT_NAME())#%"> OR
					LOWER(TRIM(FIRST_NAME)) LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="#LCase(arguments.filterVO.getSTUDENT_NAME())#%">
				)
			</cfif>
	        ORDER BY UPPER(LAST_NAME), UPPER(FIRST_NAME)
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getDocReport" access="public" returntype="query" output="false" displayname="GetDocReport" hint="">
		<cfargument name="filterData" type="sis_core.model.SearchFilter" required="true">

		<cfset var getQry = ""/>
		<cfset var restrictionsList = "">
		<cfif restrictionsLoaded()>
			<cfset restrictionsList = getRestrictions().getDataRestrictions('PG_DEPARTMENTS_PROGRAMS','PG Program','list')>
		</cfif>
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>

		<cfquery name="getDocReportQry" datasource="#getDatasource()#" >
			SELECT
				Z.*
			FROM (
				SELECT
					Y.*,
					ROWNUM AS MY_ROWNUM
				FROM(
					SELECT
						X.*,
						COUNT(X.DOC_SNAPSHOT_CD) OVER() AS TOTAL_RECORDS
					FROM (
						SELECT
							A.DOC_SNAPSHOT_CD,
							A.START_YEAR || ' - ' || A.END_YEAR AS TRAINING_SESSION,
							A.STUDENT_ID AS TRAINEE_ID,
							A.LAST_NAME || ', ' || A.FIRST_NAME AS TRAINEE_FULL_NAME,
							A.LAST_NAME AS TRAINEE_LNAME,
							A.FIRST_NAME AS TRAINEE_FNAME,
							A.DEPARTMENT_DESC ||' / '|| A.PROGRAM_DESC AS DEPARTMENT_PROGRAM,
							A.TYPE_DESC AS DOC_TEMPLATE,
							A.LETTER_TYPE_DESC AS DOC_TYPE,
							A.TRAINEE_TYPE_DESC AS TRAINEE_TYPE,
							A.EMAIL_DT,
							A.DOC_DUE_DATE,
							A.IS_CURRENT,
							A.DOC_SUBMITTED_DT,
							A.DOC_SUBMITTED_USER_ID,
							A.DOC_FILE_CD,
							A.DOC_PROOF_FILE_CD,
							A.lOA_SIGN_STATUS,
							A.DOC_CHANGE_DESC,
							A.LETTER_TEMPLATE_NAME,
							A.LETTER_TYPE_CODE,
							A.STUDENT_NUMBER,
							A.STUDENT_ID,
							C.OPHRDC,
							A.INSTANCE_ID,
							I.INSTANCE_NAME
						FROM
							(SELECT
								DOC_SNAPSHOT_CD,
								START_YEAR,
								END_YEAR,
								STUDENT_ID,
								LAST_NAME,
								FIRST_NAME,
								DEPARTMENT_DESC,
								PROGRAM_DESC,
								TYPE_DESC,
								LETTER_TYPE_DESC,
								TRAINEE_TYPE_DESC,
								EMAIL_DT,
								DOC_DUE_DATE,
								IS_CURRENT,
								DOC_SUBMITTED_DT,
								DOC_SUBMITTED_USER_ID,
								DOC_FILE_CD,
								DOC_PROOF_FILE_CD,
								DOC_SIGN_STATUS,
								DOC_CHANGE_DESC,
								LETTER_TEMPLATE_NAME,
								LETTER_TYPE_CODE,
								STUDENT_NUMBER,
								INSTANCE_ID,
								DOC_LETTER_TEMPLATE_CD,
								REGISTRATION_STATUS_DESC,
								DOC_TYPE_CD,
								DOC_CHANGE_TYPE_CD,
								DOC_LETTER_TYPE_CD,
								DEPARTMENT_CD,
								PROGRAM_CD,
								TRAINEE_TYPE_CD,
								TRAINING_SESSION_CD
							FROM
								DOC_INFO_VW
							WHERE
								ENABLE_PUBLISH = 'Y'
								AND DOC_CONFIRMED_DT IS NOT NULL
								AND IS_CURRENT = 'Y'
							UNION
							SELECT
								DOC_SNAPSHOT_CD,
								START_YEAR,
								END_YEAR,
								STUDENT_ID,
								LAST_NAME,
								FIRST_NAME,
								DEPARTMENT_DESC,
								PROGRAM_DESC,
								TYPE_DESC,
								LETTER_TYPE_DESC,
								TRAINEE_TYPE_DESC,
								EMAIL_DT,
								DOC_DUE_DATE,
								IS_CURRENT,
								DOC_SUBMITTED_DT,
								DOC_SUBMITTED_USER_ID,
								DOC_FILE_CD,
								DOC_PROOF_FILE_CD,
								DOC_SIGN_STATUS,
								DOC_CHANGE_DESC,
								LETTER_TEMPLATE_NAME,
								LETTER_TYPE_CODE,
								STUDENT_NUMBER,
								INSTANCE_ID,
								DOC_LETTER_TEMPLATE_CD,
								REGISTRATION_STATUS_DESC,
								DOC_TYPE_CD,
								DOC_CHANGE_TYPE_CD,
								DOC_LETTER_TYPE_CD,
								DEPARTMENT_CD,
								PROGRAM_CD,
								TRAINEE_TYPE_CD,
								TRAINING_SESSION_CD
							FROM
								DOC_INFO_VW
							WHERE
								ENABLE_PUBLISH = 'N') A,
							DOC_INSTANCE I,
							STUDENT_PGME C
							<cfif Len(arguments.filterData.getFILTER_ITEM("STATUS"))>
								<cfif arguments.filterData.getFILTER_ITEM("STATUS") EQ "PUBLISHED">
									,DOC_PUBLISHED_VW B
								<cfelse>
									,DOC_GENERATED_VW B
								</cfif>
							</cfif>
						WHERE
							<cfif arguments.filterData.getFILTER_ITEM("LETTERTYPE") neq "">
							    A.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("LETTERTYPE")#" /> AND
						    </cfif>
						    A.INSTANCE_ID = I.INSTANCE_ID AND
						    A.STUDENT_ID = C.STUDENT_ID
							<cfif Len(arguments.filterData.getFILTER_ITEM("DOC_SNAPSHOT_CD")) and arguments.filterData.getFILTER_ITEM("DOC_SELECTED") neq "Y">
								AND A.DOC_SNAPSHOT_CD IN(<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arguments.filterData.getFILTER_ITEM("DOC_SNAPSHOT_CD")#" />)
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("TRAINING_SESSION_CD") NEQ "">
								AND A.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("TRAINING_SESSION_CD")#" />
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("DOC_TYPE_CD") NEQ "">
								AND A.DOC_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DOC_TYPE_CD")#" />
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("DOC_LETTER_TEMPLATE_CD") NEQ "">
								AND A.DOC_LETTER_TEMPLATE_CD IN (<cfqueryparam list="true" cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DOC_LETTER_TEMPLATE_CD")#" />)
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("DOC_CHANGE_TYPE_CD") NEQ "">
								AND DOC_CHANGE_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DOC_CHANGE_TYPE_CD")#" />
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("DOC_LETTER_TYPE_CD") NEQ "">
								AND A.DOC_LETTER_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DOC_LETTER_TYPE_CD")#" />
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("DEPARTMENT_CD") NEQ "">
								AND A.DEPARTMENT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DEPARTMENT_cD")#" />
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("PROGRAM_CD") NEQ "">
								AND A.PROGRAM_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("PROGRAM_CD")#" list="true" />)
							</cfif>
							<cfif ListLen(restrictionsList) gt 0>
								AND A.PROGRAM_CD IN (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" list="true" value="#restrictionsList#" />)
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("TRAINEE_TYPE_CD") NEQ "">
								AND A.TRAINEE_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("TRAINEE_TYPE_CD")#" />
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("STUDENT_ID") NEQ "">
								AND A.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("STUDENT_ID")#" />
							</cfif>
							<cfif Len(arguments.filterData.getFILTER_ITEM("STATUS"))>
								AND A.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("SIGNOFF") NEQ "">
								<cfif arguments.filterData.getFILTER_ITEM("SIGNOFF") EQ "TRAINEE_SIGNED">
									AND A.DOC_SUBMITTED_DT 	IS NOT NULL
									AND A.DOC_PROOF_FILE_CD IS NULL
								</cfif>
								<cfif arguments.filterData.getFILTER_ITEM("SIGNOFF") EQ "STAFF_SIGNED">
									AND A.DOC_SUBMITTED_DT 	IS NOT NULL
									AND A.DOC_PROOF_FILE_CD IS NOT NULL
								</cfif>
								<cfif arguments.filterData.getFILTER_ITEM("SIGNOFF") EQ "NOT_SIGNED">
									AND A.DOC_SUBMITTED_DT 	IS NULL
								</cfif>
			 				</cfif>
			 				<cfif arguments.filterData.getFILTER_ITEM("STATUS") neq "PUBLISHED">
							 	AND UPPER(A.REGISTRATION_STATUS_DESC) <> 'WITHDRAWN'
							 </cfif>
							<cfif instanceResList neq "" and instanceResList neq "*">
								AND I.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
							</cfif>
						) X
					ORDER BY
						<cfif arguments.filterData.getSORT_OPTIONS().getField() neq "">
							#arguments.filterData.getSORT_OPTIONS().getSortString_UC()# NULLS LAST
						<cfelse>
							UPPER(TRAINEE_FULL_NAME)
						</cfif>
					) Y
				) Z
			<cfif arguments.filterData.getTABLE_PAGE().getRECORDS_PER_PAGE() gt 0 and arguments.filterData.getTABLE_PAGE().getPAGE_NO() gt 0 and arguments.filterData.getFILTER_ITEM("EXPORT") neq "1">
				WHERE Z.MY_ROWNUM BETWEEN #arguments.filterData.getTABLE_PAGE().getSTART_ROW()# AND #arguments.filterData.getTABLE_PAGE().getEND_ROW()#
			</cfif>
		</cfquery>

		<cfreturn getDocReportQry/>
	</cffunction>

	<cffunction name="getLoaConfirm" access="public" returntype="query" output="false" displayname="GetDocReport" hint="">
		<cfargument name="filterData" type="sis_core.model.SearchFilter" required="true">
		<cfargument name="instanceID" type="numeric" required="false" default="-1">

		<cfset var getDocReportQry = ""/>
		<cfset var restrictionsList = "">
		<cfif restrictionsLoaded()>
			<cfset restrictionsList = getRestrictions().getDataRestrictions('PG_DEPARTMENTS_PROGRAMS','PG Program','list')>
		</cfif>
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>

		<cfset var arrSelected = ArrayNew(1)>
		<cfset var qSCount = ListLen(arguments.filterData.getFILTER_ITEM("SELECTED_SNAPSHOTS"))>
		<cfif qSCount gt 1000>
		 	<cfset var qSList = ''>
		 	<cfloop from="1" to="#qSCount#" index="i">
				<cfset qSList = ListAppend(qSList, ListGetAt(arguments.filterData.getFILTER_ITEM("SELECTED_SNAPSHOTS"), i))>
			 	<cfif i mod 1000 eq 0>
				 	<cfset ArrayAppend(arrSelected, qSList)>
				 	<cfset qSList = ''>
				</cfif>
			</cfloop>
			<cfset ArrayAppend(arrSelected, qSList)>
		</cfif>

		<cfset var arrExcluded = ArrayNew(1)>
		<cfset var qECount = ListLen(arguments.filterData.getFILTER_ITEM("EXCLUDED_SNAPSHOTS"))>
		<cfif qECount gt 1000>
		 	<cfset var qEList = ''>
		 	<cfloop from="1" to="#qECount#" index="i">
				<cfset qEList = ListAppend(qEList, ListGetAt(arguments.filterData.getFILTER_ITEM("EXCLUDED_SNAPSHOTS"), i))>
			 	<cfif i mod 1000 eq 0>
				 	<cfset ArrayAppend(arrExcluded, qEList)>
				 	<cfset qEList = ''>
				</cfif>
			</cfloop>
			<cfset ArrayAppend(arrExcluded, qEList)>
		</cfif>

		<cfquery name="getDocReportQry" datasource="#getDatasource()#">
		SELECT
			Z.*
		FROM (
			SELECT
				Y.*,
				ROWNUM AS MY_ROWNUM
			FROM(
				SELECT
					X.*,
					COUNT(X.DOC_SNAPSHOT_CD) OVER() AS REC_COUNT
				FROM(
					SELECT DISTINCT
					  INF.DOC_SNAPSHOT_CD,
					  INF.START_YEAR || ' - ' || INF.END_YEAR AS TRAINING_SESSION,
					  INF.STUDENT_ID AS TRAINEE_ID,
					  INF.LAST_NAME || ', ' || INF.FIRST_NAME AS TRAINEE_FULL_NAME,
					  INF.TYPE_DESC AS DOC_TEMPLATE,
					  INF.LETTER_TYPE_DESC AS DOC_TYPE,
					  INF.TRAINEE_TYPE_DESC AS TRAINEE_TYPE,
					  INF.EMAIL_DT,
					  INF.STUDENT_ID,
					  INF.DOC_DUE_DATE,
					  INF.IS_CURRENT,
					  INF.DOC_SUBMITTED_DT,
					  INF.DOC_SUBMITTED_USER_ID,
					  INF.DOC_FILE_CD,
					  INF.DOC_PROOF_FILE_CD,
					  INF.DEPARTMENT_DESC AS DEPARTMENT,
					  INF.PROGRAM_DESC AS PROGRAM,
					  DECODE(SU.USER_ID,NULL,'',SU.LAST_NAME || ',' || SU.FIRST_NAME) AS ConfirmedBy,
					  INF.DOC_CONFIRMED_DT,
					  INF.DOC_CHANGE_DESC,
					  INF.LETTER_TYPE_CODE,
					  INF.STUDENT_NUMBER,
					  INF.LETTER_TEMPLATE_NAME,
					  INF.INSTANCE_ID,
					  LI.INSTANCE_NAME
					FROM
						DOC_INFO_VW INF,
					  	UD_USER SU,
					  	DOC_INSTANCE LI
					WHERE
						<cfif arguments.instanceID neq "-1">
							INF.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" /> AND
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("LETTERTYPE") neq "">
						    INF.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("LETTERTYPE")#" /> AND
					    </cfif>
						INF.INSTANCE_ID = LI.INSTANCE_ID AND
						INF.DOC_PUBLISHED_DT IS NULL
						AND INF.DOC_CONFIRMED_USER_ID 	= SU.USER_ID (+)
						<cfif arguments.filterData.getFILTER_ITEM("ONLY_CONFIRMED") EQ "Y">
							AND INF.DOC_CONFIRMED_USER_ID IS NOT NULL
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM('EXCLUDED_SNAPSHOTS') NEQ "">
							<cfif ArrayLen(arrSelected) gt 0>
								AND ((1 = 0)
								<cfloop from="1" to="#ArrayLen(arrSelected)#" index="i">
									OR INF.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrSelected[i]#" null="0" />)
								</cfloop>
								)
							<cfelse>
								AND INF.DOC_SNAPSHOT_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM('SELECTED_SNAPSHOTS')#" list="true" />)
							</cfif>
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM('EXCLUDED_SNAPSHOTS') NEQ "">
							<cfif ArrayLen(arrExcluded) gt 0>
								AND ((1 = 1)
								<cfloop from="1" to="#ArrayLen(arrExcluded)#" index="i">
									AND INF.DOC_SNAPSHOT_CD NOT IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arrExcluded[i]#" null="0" />)
								</cfloop>
								)
							<cfelse>
								AND INF.DOC_SNAPSHOT_CD NOT IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM('EXCLUDED_SNAPSHOTS')#" list="true" />)
							</cfif>
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("TRAINING_SESSION_CD") NEQ "">
							AND TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("TRAINING_SESSION_CD")#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("DOC_TYPE_CD") NEQ "">
							AND DOC_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DOC_TYPE_CD")#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("DOC_LETTER_TEMPLATE_CD") NEQ "">
							AND INF.DOC_LETTER_TEMPLATE_CD IN (<cfqueryparam list="true" cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DOC_LETTER_TEMPLATE_CD")#" />)
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("DOC_TYPE_CHANGE_CD") NEQ "">
							AND DOC_CHANGE_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DOC_TYPE_CHANGE_CD")#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("DOC_LETTER_TYPE_CD") NEQ "">
							AND DOC_LETTER_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DOC_LETTER_TYPE_CD")#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("DEPARTMENT_CD") NEQ "">
							AND INF.DEPARTMENT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("DEPARTMENT_CD")#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("PROGRAM_CD") NEQ ""
							AND arguments.filterData.getFILTER_ITEM("PROGRAM_CD") NEQ "-1">
							AND INF.PROGRAM_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("PROGRAM_CD")#" list="true" />)
						</cfif>
						<cfif ListLen(restrictionsList) gt 0>
							AND INF.PROGRAM_CD IN (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" list="true" value="#restrictionsList#" />)
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("TRAINEE_TYPE_CD") NEQ "">
							AND TRAINEE_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("TRAINEE_TYPE_CD")#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("STUDENT_ID") NEQ "">
							AND INF.STUDENT_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("STUDENT_ID")#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("USER_ID") NEQ "">
							AND INF.USER_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM("USER_ID")#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("TRAINING_LEVEL_CD") NEQ "">
							AND INF.TRAINING_LEVEL_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM('TRAINING_LEVEL_CD')#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("TRAINEE_STATUS_CD") NEQ "">
							AND INF.TRAINEE_STATUS_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getFILTER_ITEM('TRAINEE_STATUS_CD')#" />
						</cfif>
						<cfif arguments.filterData.getFILTER_ITEM("STATUS") NEQ "">
							<cfif arguments.filterData.getFILTER_ITEM("STATUS") EQ "NOT_CONFIRMED">
								AND DOC_CONFIRMED_DT IS NULL
							</cfif>
							<cfif arguments.filterData.getFILTER_ITEM("STATUS") EQ "CONFIRMED">
								AND DOC_CONFIRMED_DT IS NOT NULL
							</cfif>
						</cfif>
						<cfif instanceResList neq "" and instanceResList neq "*">
							AND LI.INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
						</cfif>
						) X
					<cfif arguments.filterData.getSORT_OPTIONS().getField() NEQ ''>
	                    ORDER BY #arguments.filterData.getSORT_OPTIONS().getSortString_UC()#
	                </cfif>
					) Y
				) Z
				<cfif arguments.filterData.getTABLE_PAGE().getRECORDS_PER_PAGE() NEQ 0>
				WHERE
					Z.MY_ROWNUM >= <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getTABLE_PAGE().getSTART_ROW()#" />
					AND Z.MY_ROWNUM <= <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.filterData.getTABLE_PAGE().getEND_ROW()#" />
				</cfif>

			</cfquery>
		<cfreturn getDocReportQry/>
	</cffunction>

	<cffunction name="getTrainingSessions4Confirm" access="public" returntype="query" output="false" displayname="getTrainingSessions4Confirm" hint="">
		<cfargument name="instanceID" type="numeric" required="true" />

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				a.training_session_cd, to_char(a.start_dt, 'YYYY') as start_year, to_char(a.end_dt, 'YYYY') as end_year,
				(a.TRAINING_SESSION_NAME) AS description
			FROM
				TRAINING_SESSION A
			WHERE
				to_char(a.start_dt, 'YYYY') >= 2014


			ORDER BY START_DT DESC
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getTrainingSessions4Report" access="public" returntype="query" output="false" displayname="getTrainingSessions4Report" hint="">
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				A.TRAINING_SESSION_CD,
				TO_CHAR(A.START_DT, 'YYYY') AS START_YEAR,
				TO_CHAR(A.END_DT, 'YYYY') AS END_YEAR,
				A.TRAINING_SESSION_NAME,
				A.TRAINING_SESSION_NAME AS DESCRIPTION
			FROM
				TRAINING_SESSION A
			WHERE
				A.TRAINING_SESSION_CD IN (SELECT DISTINCT TRAINING_SESSION_CD FROM DOC_INFO_VW V WHERE V.DOC_CONFIRMED_USER_ID IS NOT NULL AND V.DOC_CONFIRMED_DT IS NOT NULL)
			ORDER BY START_YEAR DESC
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getProgramByDepartment4Report" access="public" returntype="query" output="false" displayname="getTrainingSessions4Report" hint="">
		<cfargument name="departmentCD" type="any" required="false" value="" />
		<cfargument name="trainingSessionCD" type="any" required="false" value="" />

		<cfset var getQry = ""/>
		<cfset var restrictionsList = "">
		<cfif restrictionsLoaded()>
			<cfset restrictionsList = getRestrictions().getDataRestrictions('PG_DEPARTMENTS_PROGRAMS','PG Program','list')>
		</cfif>

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT DISTINCT
				A.PROGRAM_CD,
				A.PROGRAM_DESC,
				A.STATUS
			FROM
				PROGRAM A,
				(SELECT DISTINCT
					V.PROGRAM_CD,
					V.INSTANCE_ID
				FROM
					DOC_INFO_VW V
				WHERE
					1 =1
					<cfif arguments.departmentCD NEQ "">
						AND V.DEPARTMENT_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.departmentCD#" />
					</cfif>
					<cfif arguments.trainingSessionCD NEQ "">
						AND V.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.trainingSessionCD#" />
					</cfif>
				) B
			WHERE
				A.PROGRAM_CD = B.PROGRAM_CD
				<cfif ListLen(restrictionsList) gt 0>
					AND A.PROGRAM_CD IN (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" list="true" value="#restrictionsList#" />)
				</cfif>
			ORDER BY A.PROGRAM_DESC
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getDepartment4Report" access="public" returntype="query" output="false" displayname="getTrainingSessions4Report" hint="">
		<cfargument name="trainingSessionCD" type="any" required="false" value="" />

		<cfset var getQry = ""/>
		<cfset var restrictionsList = "">
		<cfif restrictionsLoaded()>
			<cfset restrictionsList = getRestrictions().getDataRestrictions('PG_DEPARTMENTS_PROGRAMS','PG Department','list')>
		</cfif>

		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT DISTINCT
				A.DEPARTMENT_CD,
				A.DEPARTMENT_DESC,
				A.STATUS
			FROM
				DEPARTMENT A,
				(SELECT DISTINCT
					V.DEPARTMENT_CD,
					V.INSTANCE_ID
				FROM
					DOC_INFO_VW V
				WHERE
					1 = 1
					<cfif arguments.trainingSessionCD NEQ "">
						AND V.TRAINING_SESSION_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.trainingSessionCD#" />
					</cfif>
				) B
			WHERE
				A.DEPARTMENT_CD = B.DEPARTMENT_CD
				<cfif ListLen(restrictionsList) gt 0>
					AND A.DEPARTMENT_CD IN (<cfqueryparam cfsqltype="CF_SQL_VARCHAR" list="true" value="#restrictionsList#" />)
				</cfif>
			ORDER BY A.DEPARTMENT_DESC
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

</cfcomponent>