<cfcomponent name="DocTypeDAO" displayname="DocTypeDAO" hint="I abstract data access for DOC_TYPE" extends="dbObject">
	<cffunction name="read" returntype="DocTypeVO" access="public" output="false" hint="CRUD method" >
		<cfargument name="docTypeCD" type="numeric" required="true" />

		<cfset var DocTypeVO = "" />
		<cfset var getQry = "" />
		<cfquery name="getQry" datasource="#getDatasource()#" >
			SELECT
				DOC_TYPE_CD,
				INSTANCE_ID,
				TYPE_CODE,
				TYPE_DESC,
				RESTRICTION_CONDITION_ID,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_TYPE
			WHERE
				DOC_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docTypeCD#"/>
		</cfquery>
		<cfset DocTypeVO = CreateObject("component","DocTypeVO").init()>
		<cfif getQry.recordCount gt 0>
			<cfset DocTypeVO.setDOC_TYPE_CD(getQry.DOC_TYPE_CD) />
			<cfset DocTypeVO.setINSTANCE_ID(getQry.INSTANCE_ID) />
			<cfset DocTypeVO.setTYPE_CODE(getQry.TYPE_CODE) />
			<cfset DocTypeVO.setTYPE_DESC(getQry.TYPE_DESC) />
			<cfset DocTypeVO.setRESTRICTION_CONDITION_ID(getQry.RESTRICTION_CONDITION_ID) />
			<cfset DocTypeVO.setCREATION_ID(getQry.CREATION_ID) />
			<cfset DocTypeVO.setCREATION_DT(getQry.CREATION_DT) />
			<cfset DocTypeVO.setMODIFICATION_ID(getQry.MODIFICATION_ID) />
			<cfset DocTypeVO.setMODIFICATION_DT(getQry.MODIFICATION_DT) />
		</cfif>

		<cfreturn DocTypeVO />
	</cffunction>

	<cffunction name="add" returntype="numeric" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocTypeVO" required="true" />
		<cfset var addQry = "">
		<cfset var seqQry = "">

		<cfquery name="seqQry" datasource="#getDatasource()#" >
			SELECT DOC_TYPE_SEQ.nextval AS nextval
			FROM dual
		</cfquery>

		<cfquery name="addQry" datasource="#getDatasource()#" >
			INSERT INTO DOC_TYPE (
				DOC_TYPE_CD ,INSTANCE_ID ,TYPE_CODE ,TYPE_DESC ,RESTRICTION_CONDITION_ID ,CREATION_ID ,CREATION_DT)
			VALUES (#seqQry.nextval#
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getINSTANCE_ID()#" maxLength="22" null="false" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getTYPE_CODE()#" maxLength="100" null="#iif((arguments.bean.getTYPE_CODE() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.bean.getTYPE_DESC()#" maxLength="1000" null="#iif((arguments.bean.getTYPE_DESC() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getRESTRICTION_CONDITION_ID()#" maxLength="22" null="#iif((arguments.bean.getRESTRICTION_CONDITION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.bean.getCREATION_ID()#" maxLength="22" null="#iif((arguments.bean.getCREATION_ID() eq ''),de("true"), de("false"))#" />
					,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#arguments.bean.getCREATION_DT()#" maxLength="26" null="#iif((arguments.bean.getCREATION_DT() eq ''),de("true"), de("false"))#" />)
		</cfquery>

		<cfreturn seqQry.nextval />
	</cffunction>

	<cffunction name="update" returntype="void" access="public" output="false" hint="CRUD method">
		<cfargument name="bean" type="DocTypeVO" required="true" />
		<cfset var updateQry = "" />

		<cfquery name="updateQry" datasource="#getDatasource()#" >
			UPDATE	DOC_TYPE
			SET
				INSTANCE_ID = <cfqueryparam value="#arguments.bean.getINSTANCE_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="false" />,
				TYPE_CODE = <cfqueryparam value="#arguments.bean.getTYPE_CODE()#" cfsqltype="cf_sql_varchar" maxLength="100" null="#iif((arguments.bean.getTYPE_CODE() eq ''),de("true"), de("false"))#" />,
				TYPE_DESC = <cfqueryparam value="#arguments.bean.getTYPE_DESC()#" cfsqltype="cf_sql_varchar" maxLength="1000" null="#iif((arguments.bean.getTYPE_DESC() eq ''),de("true"), de("false"))#" />,
				RESTRICTION_CONDITION_ID = <cfqueryparam value="#arguments.bean.getRESTRICTION_CONDITION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getRESTRICTION_CONDITION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_ID = <cfqueryparam value="#arguments.bean.getMODIFICATION_ID()#" cfsqltype="cf_sql_numeric" maxLength="22" null="#iif((arguments.bean.getMODIFICATION_ID() eq ''),de("true"), de("false"))#" />,
				MODIFICATION_DT = <cfqueryparam value="#arguments.bean.getMODIFICATION_DT()#" cfsqltype="cf_sql_timestamp" maxLength="26" null="#iif((arguments.bean.getMODIFICATION_DT() eq ''),de("true"), de("false"))#" />
			WHERE
				DOC_TYPE_CD = <cfqueryparam value="#arguments.bean.getDOC_TYPE_CD()#" cfsqltype="CF_SQL_NUMERIC" />
		</cfquery>
	</cffunction>

	<cffunction name="delete" returntype="void" access="public" output="false" hint="CRUD method" >
		<cfargument name="docTypeCD" type="numeric" required="true" />
		<cfset var delQry = "">
		<cfquery name="delQry" datasource="#getDatasource()#" >
			DELETE FROM	DOC_TYPE
			WHERE
				DOC_TYPE_CD = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.docTypeCD#"/>
		</cfquery>
	</cffunction>

</cfcomponent>