<cfcomponent name="DocTypeGTW" displayname="DocTypeGTW" output="false" hint="" extends="dbObject">

	<cffunction name="getAll" access="public" returntype="query" output="false" displayname="getAll" hint="">		<cfset var getQry = ""/>
		<cfset var getQry = ""/>
		<cfset var instanceResList = "">
		<cfif restrictionsLoaded()>
			<cfset instanceResList = getRestrictions().getDataRestrictions('LETTER_INSTANCE_RESTRICTIONS', 'Letter Instances', 'list')>
		</cfif>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_TYPE_CD,
				TYPE_CODE,
				TYPE_DESC,
				RESTRICTION_CONDITION_ID,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT,
				INSTANCE_ID
			FROM DOC_TYPE
			<cfif instanceResList neq "" and instanceResList neq "*">
			WHERE
				INSTANCE_ID IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#instanceResList#" list="true" />)
			</cfif>
			ORDER BY
				UPPER(TYPE_DESC)
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByPK" access="public" returntype="query" output="false" displayname="getByPK" hint="">
		<cfargument name="docTypeCD" type="numeric" required="true" />
		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_TYPE_CD,
				TYPE_CODE,
				TYPE_DESC,
				RESTRICTION_CONDITION_ID,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_TYPE
			WHERE
				DOC_TYPE_CD = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.docTypeCD#" />
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getByCode" access="public" output="false" returntype="query">
		<cfargument name="colCode" type="string" required="true">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				DOC_TYPE_CD,
				TYPE_CODE,
				TYPE_DESC,
				RESTRICTION_CONDITION_ID,
				CREATION_ID,
				CREATION_DT,
				MODIFICATION_ID,
				MODIFICATION_DT
			FROM DOC_TYPE
			WHERE
				upper(TYPE_CODE) = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#uCase(arguments.colCode)#" />
		</cfquery>

		<cfreturn getQry/>
	</cffunction>

	<cffunction name="getAllWithMap" access="public" returntype="query" output="false" displayname="getAllWithMap" hint="">
		<cfargument name="instanceID" type="numeric" required="true">
		<cfargument name="letterTemplateCD" type="string" required="false" default="*">

		<cfset var getQry = ""/>
		<cfquery name="getQry" datasource="#getDatasource()#">
			SELECT
				LT.DOC_TYPE_CD,
				LT.TYPE_CODE,
				LT.TYPE_DESC,
				LT.RESTRICTION_CONDITION_ID
			FROM
				DOC_TYPE LT,
				DOC_LETTER_MAP LLM,
				DOC_LETTER_TEMPLATE LLT
			WHERE
				LT.INSTANCE_ID = <cfqueryparam cfsqltype="CF_SQL_NUMERIC" value="#arguments.instanceID#" />
				AND LT.DOC_TYPE_CD = LLM.DOC_TYPE_CD
				AND LLM.DOC_LETTER_TEMPLATE_CD = LLT.DOC_LETTER_TEMPLATE_CD
				AND UPPER(LLT.STATUS) = 'ACTIVE'
				<cfif arguments.letterTemplateCD neq "*">
					AND LLT.LETTER_TEMPLATE_CD IN (<cfqueryparam cfsqltype="CF_SQL_NUMERIC" list="true" value="#arguments.letterTemplateCD#" />)
				</cfif>
		</cfquery>
		<cfreturn getQry/>
	</cffunction>

</cfcomponent>