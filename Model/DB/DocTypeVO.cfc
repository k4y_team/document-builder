<cfcomponent name="DocTypeVO" displayname="DocTypeVO" extends="getSetObjectValue" output="false">
	<cfproperty name="DOC_TYPE_CD" type="numeric" />
	<cfproperty name="INSTANCE_ID" type="numeric" />
	<cfproperty name="TYPE_CODE" type="string" />
	<cfproperty name="TYPE_DESC" type="string" />
	<cfproperty name="RESTRICTION_CONDITION_ID" type="string" />
	<cfproperty name="CREATION_ID" type="numeric" />
	<cfproperty name="CREATION_DT" type="date" />
	<cfproperty name="MODIFICATION_ID" type="numeric" />
	<cfproperty name="MODIFICATION_DT" type="date" />

	<cffunction name="init" access="public" returntype="DocTypeVO" output="false" displayname="init" hint="I initialize a DocType">
		<cfset variables.DOC_TYPE_CD = "" />
		<cfset variables.INSTANCE_ID = "" />
		<cfset variables.TYPE_CODE = "" />
		<cfset variables.TYPE_DESC = "" />
		<cfset variables.RESTRICTION_CONDITION_ID = "" />
		<cfset variables.CREATION_ID = "" />
		<cfset variables.CREATION_DT = "" />
		<cfset variables.MODIFICATION_ID = "" />
		<cfset variables.MODIFICATION_DT = "" />

		<cfreturn this />
 	</cffunction>

	<cffunction name="setDOC_TYPE_CD" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.DOC_TYPE_CD = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getDOC_TYPE_CD" access="public" returntype="any" output="false">
		<cfreturn variables.DOC_TYPE_CD />
	</cffunction>
	<cffunction name="setINSTANCE_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.INSTANCE_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getINSTANCE_ID" access="public" returntype="any" output="false">
		<cfreturn variables.INSTANCE_ID />
	</cffunction>
	<cffunction name="setTYPE_CODE" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.TYPE_CODE = arguments.val />
	</cffunction>
	<cffunction name="getTYPE_CODE" access="public" returntype="any" output="false">
		<cfreturn variables.TYPE_CODE />
	</cffunction>
	<cffunction name="setTYPE_DESC" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfset variables.TYPE_DESC = arguments.val />
	</cffunction>
	<cffunction name="getTYPE_DESC" access="public" returntype="any" output="false">
		<cfreturn variables.TYPE_DESC />
	</cffunction>
	<cffunction name="setRESTRICTION_CONDITION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
			<cfset variables.RESTRICTION_CONDITION_ID = val/>
		<cfelse>
			<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
		</cfif>
	</cffunction>
	<cffunction name="getRESTRICTION_CONDITION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.RESTRICTION_CONDITION_ID />
	</cffunction>
	<cffunction name="setCREATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_ID />
	</cffunction>
	<cffunction name="setCREATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.CREATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getCREATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.CREATION_DT />
	</cffunction>
	<cffunction name="setMODIFICATION_ID" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isNumeric(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_ID = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid numeric!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_ID" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_ID />
	</cffunction>
	<cffunction name="setMODIFICATION_DT" access="public" returntype="void" output="false">
		<cfargument name="val" type="any" required="true" />
		<cfif isDate(arguments.val) OR arguments.val EQ ''>
				<cfset variables.MODIFICATION_DT = val/>
			<cfelse>
				<cfthrow message="'#arguments.val#' is not a valid date!"/>
			</cfif>
	</cffunction>
	<cffunction name="getMODIFICATION_DT" access="public" returntype="any" output="false">
		<cfreturn variables.MODIFICATION_DT />
	</cffunction>
</cfcomponent>
