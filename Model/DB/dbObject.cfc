<cfcomponent name="dbObject" displayname="dbObject">
	<cfproperty name="datasource" type="string"/>
	<cfproperty name="restrictions" type="any"/>

	<cffunction name="init" access="public" returntype="dbObject" output="false" hint="dbObject Constructor.">
		<cfargument name="datasource" type="string" required="true">
		<cfargument name="restrictions" type="any" required="false" default="">

		<cfset variables.instance.datasource = arguments.datasource />
		<cfset variables.instance.restrictions = arguments.restrictions />

		<cfreturn this />
	</cffunction>

	<cffunction name="getDatasource" access="public" returntype="string">
		<cfreturn variables.instance.datasource />
	</cffunction>

	<cffunction name="getRestrictions" access="public" returntype="any">
		<cfreturn variables.instance.restrictions />
	</cffunction>

	<cffunction name="restrictionsLoaded" access="public" returntype="any">
		<cfif not isObject(variables.instance.restrictions)>
			<cfreturn false>
		<cfelse>
			<cfreturn true>
		</cfif>
	</cffunction>
</cfcomponent>