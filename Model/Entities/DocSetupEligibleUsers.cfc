<cfcomponent name="DocSetupEligibleUsers" code="QB_DOC_SETUP_ELIGIBLE_USERS" displayName="DocSetupEligibleUsers" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="DOC_SETUP_ID" label="ID" type="numeric" key="true">
	<cfproperty name="USER_NAME" label="Supervisor" type="string" id="USER_ID">
	<cfproperty name="DOC_SETUP_NAME" label="Setup Name" type="string" id="DOC_SETUP_ID">
	<cfproperty name="INSTANCE_ID" label="Instance ID" type="string">

	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="UD_USER",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"}
		]>
		 <cfreturn grants>
	</cffunction>

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
			SELECT
			    A.USER_ID,
			    A.DOC_SETUP_ELIGIBLE_USER_ID,
			    D.LAST_NAME||', '||D.FIRST_NAME AS USER_NAME,
			    B.DOC_SETUP_NAME,
			    B.DOC_SETUP_ID,
			    B.INSTANCE_ID,
			    C.INSTANCE_CODE
			 FROM
			    DOC_SETUP_ELIGIBLE_USERS A,
			    DOC_SETUP B,
			    DOC_INSTANCE C,
			    {fbx_datasource}.EVAL_SUPERVISOR D,
			    {fbx_datasource}.EVAL_SUPERVISOR_USER E
			WHERE A.DOC_SETUP_ID = B.DOC_SETUP_ID
			AND B.INSTANCE_ID = C.INSTANCE_ID
			AND D.SUPERVISOR_ID = E.SUPERVISOR_ID
			AND A.USER_ID = E.USER_ID
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>

