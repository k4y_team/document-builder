<cfcomponent name="DocSetupForm" code="DOCUMENT_SETUP_FORM" displayName="DocSetupForm" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables" />

	<cfproperty name="DOC_SETUP_ID"  type="numeric" key="true">
	<cfproperty name="INSTANCE_NAME"  type="string" label="Document Type" id="instance_id" validations="required">
   	<cfproperty name="TEMPLATE_NAME"  type="string" label="Document Template" id="DOC_LETTER_TEMPLATE_CD" validations="required">
	<cfproperty name="GROUP_NAME"  type="string" label=" User Group" id="DOC_TYPE_CD">

	<cfproperty name="DOC_SETUP_NAME"  type="string" label="Setup Name"  validations="required,max_length(200)">



	<cfscript>

	public DocSetupForm function init(numeric DOC_SETUP_ID) {
		variables.args = arguments;
		super.init();
		return this;
	}

	public struct function getValidationRules() {
		return {};
	}

	public string function getSQL() {

		var sql = "
		   	SELECT * FROM DOC_SETUP
           ";
		return sql;
	}

	public string function getDataProvider_INSTANCE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.INSTANCE_ID as idField,
                        A.INSTANCE_NAME as descField,
                        A.INSTANCE_CODE
                    FROM
                       DOC_INSTANCE A
                       WHERE 1=1
                         {% if DATA.INSTANCE_RESTRICTIONS_LIST is defined %}
							AND {{macros.whereClause('A.INSTANCE_ID',join(DATA.INSTANCE_RESTRICTIONS_LIST, '\',\''))}}
						{%else%}
							AND A.INSTANCE_ID =-1
		            	{% endif %}
                    ORDER BY UPPER(A.INSTANCE_NAME)";
        return _sql;
    }

	public string function getDataProvider_GROUP_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.DOC_TYPE_CD as idField,
                        A.TYPE_DESC as descField
                    FROM
                       DOC_TYPE A
                    WHERE instance_id=<instance_id>
                    ORDER BY UPPER(A.TYPE_DESC)";
        return _sql;
    }
    public string function getDataProvider_TEMPLATE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.DOC_LETTER_TEMPLATE_CD as idField,
                        A.LETTER_TEMPLATE_NAME as descField
                    FROM
                       DOC_LETTER_TEMPLATE A
                    WHERE  instance_id=<instance_id>
                    ORDER BY UPPER(A.LETTER_TEMPLATE_NAME)";
        return _sql;
    }

	  public struct function getRenderInfo() {
            /* Constants */
            var RENDER_TYPE = "renderType"; var NAME = "name"; var MULTI_SELECT = "multiSelect"; var READ_ONLY = "readOnly";
            var PLACEHOLDER = "placeholder"; var rtDEFAULT = "default"; var DISABLED = "DISABLED"; var addForm = "addFormHtml";
            var mobileaddForm = "addFormMobile";
            /* Variables */
            var fieldDataDefault = super.getRenderInfo();
            fieldDataDefault["default"]["DOC_SETUP_ID"]["renderType"] = "hidden";
            fieldDataDefault["default"]["INSTANCE_NAME"]["renderType"] = "select";
            fieldDataDefault["default"]["INSTANCE_NAME"]["METADATAFIELDS"] = "INSTANCE_CODE";
            fieldDataDefault["default"]["INSTANCE_NAME"]["onChangeFunction"] = "onInstanceChange()";
			fieldDataDefault["default"]["GROUP_NAME"][RENDER_TYPE] = "multiSelect";
            fieldDataDefault["default"]["TEMPLATE_NAME"]["dependencyFields"] = [{"PARAM_NAME" = "instance_id", "FIELD_ID" = "instance_id"}];
            fieldDataDefault["default"]["GROUP_NAME"]["dependencyFields"] = [{"PARAM_NAME" = "instance_id", "FIELD_ID" = "instance_id"}];
            return fieldDataDefault;
        }

	</cfscript>



	<cffunction name="getcolumnProperties" access="public" returntype="string">
		<cfset var columnProperties = "DOC_SETUP_ID,DOC_SETUP_NAME,INSTANCE_NAME,TEMPLATE_NAME,GROUP_NAME" />
		<cfreturn columnProperties >
	</cffunction>

	<cffunction name="getTemplates" access="public" returntype="struct" >
		<cfset var result = {} />
		<cfset var formHtml = "">

		<cfset var columnProperties = getcolumnProperties() />
		<cfsavecontent variable="formHtml">
			<cfoutput>
				<cfloop list="#columnProperties#" index="property">
					<% if (typeof fields["#property#"] != "undefined"){ %>
						<div class="row-form clearfix no-border" id="#property#" <cfif "#property#" EQ "GROUP_NAME">style="display:none;"</cfif>>
							<div class="span2"><%= fields["#property#"].formLabel %></div>
							<div class="span3" id="">
								<%= fields["#property#"].input %>
							</div>
						</div>
					<% } %>
				</cfloop>
				<script type="text/javascript">
				 	function onInstanceChange(){
				 		var instanceId = getMultiselectValues('instance_id', 'values').join(',');
				 		var selectedMetadataFields = getMultiselectValues('instance_id', 'metadataFields');
				 		$.grep(selectedMetadataFields, function(obj) {
        					if (typeof(obj.INSTANCE_CODE) != 'undefined') {
        						if (['PG_TES_REPORT','UG_TES_REPORT'].indexOf(obj.INSTANCE_CODE) < 0){
									$("##GROUP_NAME").show();
								}else{
									$("##GROUP_NAME").hide();
								}
        					}
  						});
					    $("##configForm").html("");
						if (instanceId >0){
								$.ajax({
							    	url: '#application.cbController.getSetting("modulesApplicationURL")#index.cfm?fuseaction=ReportSetup.getInstanceForm',
							      	data: $('##templatesfrmEdit').serialize(),
							      	type: 'POST',
							      	dataType:'html',
							       	}).done(function (result) {
							            $("##configForm").html(result);
							       });
						}
				 	}

				</script>
			</cfoutput>
		</cfsavecontent>
		<cfset result["default"] = formHtml />
		<cfreturn result>
	</cffunction>

</cfcomponent>