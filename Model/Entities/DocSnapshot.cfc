<cfcomponent name="DocSnapshot" code="QB_DOC_SNAPSHOT" displayName="DocSnapshot" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="USER_ID" type="numeric" key="true">
	<cfproperty name="DOC_SNAPSHOT_CD" label="ID" type="numeric" key="true">
	<cfproperty name="PUBLISHED_STATUS" label="Status" type="string" id="PUBLISHED_STATUS_ID">
	<cfproperty name="INSTANCE_CODE" label="Status" type="string" id="INSTANCE_ID" dataProvider="SELECT A.INSTANCE_ID as idField, A.INSTANCE_CODE as descField FROM DOC_INSTANCE A">
	<cfproperty name="USER_NAME" id="USER_ID" type="string" label="lbl.SUPERVISOR_NAME" />


	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="UD_USER",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"}
		]>
		 <cfreturn grants>
	</cffunction>

	<cfscript>
		public string function getDataProvider_PUBLISHED_STATUS() {
        	var _sql = "SELECT
				    	*
					FROM
				    (
				        SELECT
				            1 AS idfield,
				            'Published' AS descfield,
				            1 AS order_nr
				        FROM
				            dual
				        UNION
				        SELECT
				            2 AS idfield,
				            'Not Published' AS descfield,
				            2 AS order_nr
				        	FROM
            	dual
    			)
				ORDER BY order_nr";
        	return _sql;
   	 }
	</cfscript>

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
				SELECT
					A.DOC_SNAPSHOT_CD,
					A.DOC_CONFIRMED_USER_ID,
					A.DOC_CONFIRMED_DT,
					A.DOC_PUBLISHED_USER_ID,
					A.DOC_PUBLISHED_DT,
					NVL2(A.DOC_PUBLISHED_DT, 1 , 2) AS PUBLISHED_STATUS_ID,
					NVL2(A.DOC_PUBLISHED_DT,'Published','Not Published') AS PUBLISHED_STATUS,
					A.DOC_DT,
					A.IS_CURRENT,
					A.DOC_SUBMITTED_USER_ID,
					A.DOC_SUBMITTED_DT,
					A.DOC_SIGNATURE,
					A.CREATION_ID,
					A.CREATION_DT,
					A.MODIFICATION_ID,
					A.MODIFICATION_DT,
					A.INSTANCE_ID,
					A.DOC_PROOF_FILE_CD,
					A.DOC_FILE_CD,
					A.USER_ID,
					B.INSTANCE_CODE,
					U.LAST_NAME||', '||U.FIRST_NAME AS USER_NAME,
					EPD.EVALUATION_PERIOD,
					C.DOC_SETUP_NAME
				FROM DOC_SNAPSHOT_BASE A,
					 DOC_INSTANCE B,
					 DOC_SETUP C,
					 {pg_datasource}.UD_USER U,
					 DOC_SNAPSHOT_EVAL_PERIOD EPD
				WHERE A.INSTANCE_ID = B.INSTANCE_ID
					AND U.USER_ID = A.USER_ID
					AND A.DOC_SNAPSHOT_CD = EPD.DOC_SNAPSHOT_CD
					AND A.DOC_SETUP_ID = C.DOC_SETUP_ID
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>

