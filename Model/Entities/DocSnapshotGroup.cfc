<cfcomponent name="DocSnapshotGroup" code="QB_DOC_SNAPSHOT_GROUP" displayName="DocSnapshotGroup" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="USER_ID" type="numeric" key="true">
	<cfproperty name="DOC_PUBLISHED_DT" type="date" key="true">
	<cfproperty name="PUBLISHED_STATUS" label="Status" type="string" id="PUBLISHED_STATUS_ID">
	<cfproperty name="INSTANCE_CODE" label="Status" type="string" id="INSTANCE_ID" dataProvider="SELECT A.INSTANCE_ID as idField, A.INSTANCE_CODE as descField FROM DOC_INSTANCE A">
	<cfproperty name="USER_NAME" id="USER_ID" type="string" label="lbl.SUPERVISOR_NAME" />


	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="UD_USER",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"}
		]>
		 <cfreturn grants>
	</cffunction>

	<cfscript>
		public string function getDataProvider_PUBLISHED_STATUS() {
        	var _sql = "SELECT
				    	*
					FROM
				    (
				        SELECT
				            1 AS idfield,
				            'Published' AS descfield,
				            1 AS order_nr
				        FROM
				            dual
				        UNION
				        SELECT
				            2 AS idfield,
				            'Not Published' AS descfield,
				            2 AS order_nr
				        	FROM
            	dual
    			)
				ORDER BY order_nr";
        	return _sql;
   	 }
	</cfscript>

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
				SELECT
                    LISTAGG(EPD.EVALUATION_PERIOD,', ') WITHIN GROUP(ORDER BY A.USER_ID) AS EVALUATION_PERIOD,
					LISTAGG(C.DOC_SETUP_NAME,', ') WITHIN GROUP(ORDER BY A.USER_ID) AS DOC_SETUP_NAME,
                    NVL2(TRUNC(A.DOC_PUBLISHED_DT),'Published','Not Published') AS PUBLISHED_STATUS,
					NVL2(TRUNC(A.DOC_PUBLISHED_DT), 1 , 2) AS PUBLISHED_STATUS_ID,
                    A.INSTANCE_ID,
                    A.USER_ID,
                    TRUNC(A.DOC_PUBLISHED_DT) as DOC_PUBLISHED_DT,
                    B.INSTANCE_NAME,
                    B.INSTANCE_CODE
				FROM DOC_SNAPSHOT_BASE A,
					 DOC_INSTANCE B,
					 DOC_SETUP C,
					 {pg_datasource}.UD_USER U,
					 DOC_SNAPSHOT_EVAL_PERIOD EPD
				WHERE A.INSTANCE_ID = B.INSTANCE_ID
					AND U.USER_ID = A.USER_ID
					AND A.DOC_SNAPSHOT_CD = EPD.DOC_SNAPSHOT_CD
					AND A.DOC_SETUP_ID = C.DOC_SETUP_ID
                    GROUP BY
                        A.USER_ID,
                        TRUNC(A.DOC_PUBLISHED_DT),
                        A.INSTANCE_ID,
                        B.INSTANCE_NAME,
                        B.INSTANCE_CODE
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>

