<cfcomponent name="DocumentTemplate" code="QB_DOCUMENT_TEMPLATE" displayName="DocumentTemplate" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="LETTER_TEMPLATE_CD" label="ID" type="numeric" key="true">
	<cfproperty name="LETTER_TEMPLATE_CODE" label="Name" type="string" key="true">

	<cfproperty name="LETTER_TEMPLATE_NAME" label="Name" type="string">
	<cfproperty name="INSTANCE_ID" label="Instance ID" type="string">

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
				SELECT
					DOC_LETTER_TEMPLATE_CD AS LETTER_TEMPLATE_CD,
					LETTER_TEMPLATE_NAME,
					LETTER_TEMPLATE_CODE,
					INSTANCE_ID
				FROM
					DOC_LETTER_TEMPLATE
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>