<cfcomponent name="LetterInstance" code="QB_LETTER_INSTANCE" displayName="LetterInstance" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="INSTANCE_ID" label="ID" type="numeric" key="true">
	<cfproperty name="INSTANCE_NAME" label="Name" type="string">

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
				SELECT
					INSTANCE_ID,
					INSTANCE_NAME
				FROM
					DOC_INSTANCE
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>