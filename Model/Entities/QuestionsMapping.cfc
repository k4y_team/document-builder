<cfcomponent name="QuestionsMapping" code="QB_QUESTIONS_MAPPING" displayName="QuestionsMapping"  dbViewName="QUESTIONS_MAPPING_VIEW" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">



	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_QUESTION",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"}

		]>
		<cfreturn grants>
	</cffunction>

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
				  WITH MAPPING_QUESTIONS AS (
				    SELECT
				        Q.TES_CONFIG_ID,
				        Q.DOC_SETUP_ID,
				        Q.QUESTION_ID AS GROUP_QUESTION_ID,
				        NVL(
				            M.QUESTION_ID,
				            Q.QUESTION_ID
				        ) AS QUESTION_ID
				    FROM
				        TES_CONFIG_QUESTIONS Q,
				        TES_CONFIG_QUESTION_MAPPINGS M
				    WHERE
				            M.TES_CONFIG_QUESTION_ID (+) = Q.TES_CONFIG_QUESTION_ID
				),FORM_QUESTIONS AS (
				    SELECT
				        EQ.QUESTION_DESC,
				        C.TES_CONFIG_ID,
				        C.DOC_SETUP_ID,
				        EQ.QUESTION_CD AS QUESTION_ID,
				        NVL(EQ.MAIN_QUESTION_CD,EQ.QUESTION_CD) AS GROUP_QUESTION_ID
				    FROM
				        TES_CONFIG C,
				        {eval_form_datasource}.EVAL_QUESTION EQ
				    WHERE
				            C.EVAL_FORM_CD = EQ.EVAL_FORM_CD
				        AND
				            EQ.QUESTION_TYPE_CD IN (1,8)
				        AND NVL(EQ.IS_DELETED,'N')!='Y'
				),ALL_QUESTIONS AS (
				    SELECT
				        A.QUESTION_DESC,
				        A.TES_CONFIG_ID,
				        A.DOC_SETUP_ID,
				        NVL(B.GROUP_QUESTION_ID,A.GROUP_QUESTION_ID) AS GROUP_QUESTION_ID,
				        NVL(B.QUESTION_ID,A.QUESTION_ID) AS QUESTION_ID,
				        DECODE(
				            B.GROUP_QUESTION_ID,
				            A.QUESTION_ID,
				            '1',
				            0
				        ) AS MATCH,
				        SUM(
				            DECODE(
				                B.QUESTION_ID,
				                NULL,
				                0,
				                1
				            )
				        ) OVER(PARTITION BY
				            A.TES_CONFIG_ID
				        ) AS COUNTPERCONFIG
				    FROM
				        FORM_QUESTIONS A,
				        MAPPING_QUESTIONS B
				    WHERE
				        A.QUESTION_ID = B.GROUP_QUESTION_ID (+)
				     AND   A.DOC_SETUP_ID = B.DOC_SETUP_ID (+)
				    GROUP BY
				        B.QUESTION_ID,
				        A.QUESTION_ID,
				        A.TES_CONFIG_ID,
				        A.DOC_SETUP_ID,
				        A.GROUP_QUESTION_ID,
				        B.GROUP_QUESTION_ID,
				        A.QUESTION_DESC
				) SELECT
				    DOC_SETUP_ID,
				    TES_CONFIG_ID,
				    GROUP_QUESTION_ID,
				    QUESTION_ID
				FROM
				    ALL_QUESTIONS
				WHERE
				        COUNTPERCONFIG > 0
				    AND
				        MATCH = 1
				UNION ALL
				SELECT
				    DOC_SETUP_ID,
				    TES_CONFIG_ID,
				    GROUP_QUESTION_ID,
				    QUESTION_ID
				FROM
				    ALL_QUESTIONS
				WHERE
				    COUNTPERCONFIG = 0
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>

