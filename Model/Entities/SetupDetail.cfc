<cfcomponent name="SetupDetail" code="DOC_SETUP_DETAIL" displayName="SetupDetail" dbMaterializedView="false" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables" />

	<cfproperty name="TEMPLATE_ID" type="numeric" key="true">
	<cfproperty name="DOC_SETUP_ID" type="numeric" key="true">


	<cfscript>

	public SetupDetail function init(numeric TRAINEE_ID) {
		variables.args = arguments;
		super.init();
		return this;
	}

	public struct function getValidationRules() {
		return {};
	}

	function getGrants() {
		 var grants = [{OBJECT="EVAL_FORM",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
					   {OBJECT="RSPROGRAM_VIEW",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"},
					   {OBJECT="EVAL_FORM",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
					   {OBJECT="COURSE",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}];
		return grants;
	}

	public string function getSQL() {
		var sql = "
			WITH DETAILS AS (SELECT
				    D.DOC_SETUP_ID,
				    D.ACTIVITY_END_DT,
				    D.GENERATE_SCHEDULE_ID,
        			D.PUBLISH_SCHEDULE_ID,
        			D.EVAL_FORM_CD,
				    'Eval Form: <b>'||E.EVAL_FORM_DESC||'</b>'||'</br>Program: <b>'|| NVL((SELECT
									    LISTAGG(A.PROGRAM_NAME,', ') WITHIN GROUP(ORDER BY M.TES_CONFIG_ID)
									FROM
									    {pg_datasource}.RSPROGRAM_VIEW A,
									    TES_CONFIG_PROGRAMS M
									WHERE

									        A.PROGRAM_ID = M.PROGRAM_CD
									        AND M.TES_CONFIG_ID=D.TES_CONFIG_ID),'All Programs')||'</b>' as Details
				FROM
				    TES_CONFIG D,
				    {eval_form_datasource}.EVAL_FORM E,
	                DOC_SETUP S,
	                DOC_INSTANCE I
				WHERE
					D.EVAL_FORM_CD = E.EVAL_FORM_CD
					AND S.DOC_SETUP_ID = D.DOC_SETUP_ID
					AND S.INSTANCE_ID = I.INSTANCE_ID
					AND I.INSTANCE_CODE ='PG_TES_REPORT'

			UNION
    			SELECT
				    D.DOC_SETUP_ID,
				    D.ACTIVITY_END_DT,
				    D.GENERATE_SCHEDULE_ID,
        			D.PUBLISH_SCHEDULE_ID,
        			D.EVAL_FORM_CD,
				    'Eval Form: <b>'||E.EVAL_FORM_DESC ||' ['||E.EVAL_FORM_CD||']' ||'</b>'||'</br>Course: <b>'|| NVL((SELECT
									    LISTAGG(A.COURSE_NAME,', ') WITHIN GROUP(ORDER BY M.TES_CONFIG_ID)
									FROM
									    {fbx_datasource}.COURSE A,
									    UG_TES_CONFIG_COURSES M
									WHERE

									        A.COURSE_CD = M.COURSE_CD
									        AND M.TES_CONFIG_ID=D.TES_CONFIG_ID),'All Courses')||'</b>' as Details
				FROM
				    TES_CONFIG D,
				    {fbx_datasource}.EVAL_FORM E,
	                DOC_SETUP S,
	                DOC_INSTANCE I
				WHERE  D.EVAL_FORM_CD = E.EVAL_FORM_CD
				    AND S.DOC_SETUP_ID = D.DOC_SETUP_ID
					AND S.INSTANCE_ID = I.INSTANCE_ID
					AND I.INSTANCE_CODE ='UG_TES_REPORT'
				)
		   	SELECT
		   		A.DOC_SETUP_ID,
		   		A.INSTANCE_ID,
		   		A.DOC_LETTER_TEMPLATE_CD,
		   		A.DOC_LETTER_TEMPLATE_CD AS TEMPLATE_ID,
		   		A.DOC_SETUP_ID AS SETUP_ID,
		   		A.DOC_TYPE_CD,
		   		A.DOC_SETUP_NAME,
		   		A.CREATION_ID,
				A.CREATION_DT,
				A.MODIFICATION_ID,
				A.MODIFICATION_DT,
		   		B.INSTANCE_NAME,
				C.LETTER_TEMPLATE_NAME,
		   		D.DETAILS,
		   		D.EVAL_FORM_CD,
		   		D.ACTIVITY_END_DT as ACTIVITY_END_DT,
		   		(SELECT
		   			NVL(MIN(SCH.NEXT_TRIGGER_DT),MAX(SCH.LAST_TRIGGER_DT))
				FROM
				    interceptor_event_binding IEB,
				    INTERCEPTOR_EVENT IE,
				    INTERCEPTOR_SCHEDULE SCH
				WHERE
				   IEB.handler_config_cd = d.PUBLISH_SCHEDULE_ID
				   AND IE.EVENT_NAME = IEB.EVENT_PATTERN
				   AND SCH.SCHEDULE_NAME = 'PUBLISH_DOCUMENTS'
				   AND SCH.EVENT_CD(+) = IE.EVENT_CD) as PUBLISH_DT,
				(SELECT
		   			NVL(MIN(SCH.NEXT_TRIGGER_DT),MAX(SCH.LAST_TRIGGER_DT))
				FROM
				    INTERCEPTOR_EVENT_BINDING IEB,
				    INTERCEPTOR_EVENT IE,
				    INTERCEPTOR_SCHEDULE SCH
				WHERE
				   IEB.HANDLER_CONFIG_CD = D.GENERATE_SCHEDULE_ID
				   AND IE.EVENT_NAME = IEB.EVENT_PATTERN
				   AND SCH.SCHEDULE_NAME = 'GENERATE_DOCUMENTS'
				   AND SCH.EVENT_CD(+) = IE.EVENT_CD) AS GENERATE_DT
		   	FROM
		   		DOC_SETUP A,
		   		DOC_INSTANCE B,
				DOC_LETTER_TEMPLATE C,
		   		DETAILS D
		   	WHERE A.INSTANCE_ID = B.INSTANCE_ID
				AND A.DOC_LETTER_TEMPLATE_CD = C.DOC_LETTER_TEMPLATE_CD
		   		AND A.DOC_SETUP_ID = D.DOC_SETUP_ID(+)
           ";
		return sql;
	}

	</cfscript>
</cfcomponent>