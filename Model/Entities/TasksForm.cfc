<cfcomponent name="TasksForm" displayname = "TasksForm" output="false" extends="sis_core.model.blAutomation.EntityBase" accessors="true" code="TASKS_FORM">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="INSTANCE_NAME" id="INSTANCE_ID" type="string" label="Document Type" >
	<cfproperty name="SETUP_NAME" id="DOC_SETUP_ID" type="string" label="Setup" >
	<cfproperty name="USER_NAME" id="USER_ID" type="string" label="Supervisor" >
	<cfscript>

	public TasksForm function init() {
		variables.args = arguments;
		super.init();
		return this;
	}



	public string function getDataProvider_INSTANCE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.INSTANCE_ID as idField,
                        A.INSTANCE_NAME as descField,
                        A.INSTANCE_CODE
                    FROM
                       DOC_INSTANCE A
                    ORDER BY UPPER(A.INSTANCE_NAME)";
        return _sql;
    }
    public string function getDataProvider_SETUP_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
         				DISTINCT
                        A.DOC_SETUP_ID as idField,
                        A.DOC_SETUP_NAME as descField
                    FROM
                       DOCSETUPELIGIBLEUSERS_VIEW A
                    WHERE INSTANCE_ID = <instance_id>
                    ORDER BY UPPER(A.DOC_SETUP_NAME)";
        return _sql;
    }
    public string function getDataProvider_USER_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
        				DISTINCT
                        A.USER_ID as idField,
                        A.USER_NAME as descField
                    FROM
                       DOCSETUPELIGIBLEUSERS_VIEW A
                    ORDER BY UPPER(A.USER_NAME)";
        return _sql;
    }


	  public struct function getRenderInfo() {
            /* Constants */
            var RENDER_TYPE = "renderType"; var NAME = "name"; var MULTI_SELECT = "multiSelect"; var READ_ONLY = "readOnly";
            var PLACEHOLDER = "placeholder"; var rtDEFAULT = "default"; var DISABLED = "DISABLED"; var addForm = "addFormHtml";
            var mobileaddForm = "addFormMobile";
            /* Variables */
            var fieldDataDefault = super.getRenderInfo();
            fieldDataDefault["default"]["INSTANCE_NAME"]["METADATAFIELDS"] = "INSTANCE_CODE";
			fieldDataDefault["default"]["SETUP_NAME"][RENDER_TYPE] = "multiSelect";
			fieldDataDefault[rtDEFAULT]["SETUP_NAME"]["dependencyFields"] = [{"PARAM_NAME" = "instance_id", "FIELD_ID" = "instance_id"}];

			fieldDataDefault["default"]["USER_NAME"][RENDER_TYPE] = "multiSelect";


            return fieldDataDefault;
        }

	</cfscript>



 	<cffunction name="getcolumnProperties" access="public" returntype="string">
		<cfset var columnProperties = "INSTANCE_NAME,SETUP_NAME,USER_NAME" />
		<cfreturn columnProperties >
	</cffunction>

	<cffunction name="getTemplates" access="public" returntype="struct" >
		<cfset var result = {} />
		<cfset var formHtml = "">

		<cfset var columnProperties = getcolumnProperties() />
		<cfsavecontent variable="formHtml">
			<cfoutput>
				<cfloop list="#columnProperties#" index="property">
					<% if (typeof fields["#property#"] != "undefined"){ %>
						<div class="row-form clearfix no-border" id="#property#" <cfif "#property#" EQ "GROUP_NAME">style="display:none;"</cfif>>
							<div class="span2"><%= fields["#property#"].formLabel %></div>
							<div class="span3" id="">
								<%= fields["#property#"].input %>
							</div>
						</div>
					<% } %>
				</cfloop>
			</cfoutput>
		</cfsavecontent>
		<cfset result["default"] = formHtml />
		<cfreturn result>
	</cffunction>

</cfcomponent>