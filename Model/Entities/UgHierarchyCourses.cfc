<cfcomponent name="UgHierarchyCourses" code="QB_UG_HIERARCHY_CONFIG_COURSES" displayName="UgHierarchyCourses"  dbViewName="UG_HIERARCHY_CONFIG_COURSES" dbMaterializedView="true"  output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="COURSE_HIERARCHY",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
		]>
		<cfreturn grants>
	</cffunction>

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
				SELECT
				    TES_CONFIG_COURSE_ID,
				    TES_CONFIG_ID,
				    COURSE_CD
				FROM
				    UG_TES_CONFIG_COURSES
				UNION
				SELECT
				    UC.TES_CONFIG_COURSE_ID,
				    UC.TES_CONFIG_ID,
				    H.COURSE_CD
				FROM
				    UG_TES_CONFIG_COURSES UC
				    JOIN {fbx_datasource}.COURSE_HIERARCHY H ON (UC.COURSE_CD=H.PARENT_COURSE_CD)
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>

