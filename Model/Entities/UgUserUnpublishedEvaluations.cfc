<cfcomponent name="UgUserUnpublishedEvaluations" code="QB_UGUNPUBLISHED_USERS" displayName="UgUserUnpublishedEvaluations"  dbViewName="UG_USER_UNPUBLISHED_EVALS" dbMaterializedView="true" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="DOC_SETUP_ID" label="ID" type="numeric" key="true">
	<cfproperty name="USER_NAME" label="Supervisor" type="string" id="USER_ID">
	<cfproperty name="DOC_SETUP_NAME" label="Setup Name" type="string" id="DOC_SETUP_ID">
	<cfproperty name="INSTANCE_ID" label="Instance ID" type="string">

	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_QUESTION",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="S4Y_USER_EVAL",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="S4Y_USER_ANSWER",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="EVAL_SUPERVISOR",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="QUESTION_TYPE",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
		]>
		<cfreturn grants>
	</cffunction>

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
			  WITH USED_EVALUATIONS AS (
            			SELECT
						    A.DOC_SNAPSHOT_CD,
						    A.USER_ID,
						    A.EVALUATION_ID AS USER_EVAL_CD
						FROM
						    DOC_SNAPSHOT_EVALUATIONS A,
						    DOC_SNAPSHOT_BASE B,
						    DOC_INSTANCE C
						WHERE
						        A.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
						    AND
						        B.INSTANCE_ID = C.INSTANCE_ID
						    AND C.INSTANCE_CODE='UG_TES_REPORT'
				),EVALUATIONS AS (
				    SELECT
				        DISTINCT
				        D.USER_ID,
			            D.INSTANCE_ID,
			            A.USER_EVAL_CD,
			            D.DOC_SETUP_ID,
			            A.USER_ID AS EVALUATOR_USER_ID,
			            TC.MIN_EVALS
				     FROM  {fbx_datasource}.S4Y_USER_EVAL  A
   					 	JOIN  {fbx_datasource}.S4Y_EVAL_SCHEDULE B  ON(A.EVAL_SCHEDULE_CD = B.EVAL_SCHEDULE_CD)
   						JOIN DOCSETUPELIGIBLEUSERS_VIEW D ON(D.USER_ID=A.ADDRESSEE_ID AND D.INSTANCE_CODE  ='UG_TES_REPORT' )
   						JOIN TES_CONFIG TC ON(TC.DOC_SETUP_ID = D.DOC_SETUP_ID AND TRUNC(B.END_DT) <= TRUNC(NVL(TC.ACTIVITY_END_DT,B.END_DT)))
   						JOIN UG_HIERARCHY_CONFIG_COURSES TCC ON(TCC.COURSE_CD=B.COURSE_CD AND TCC.TES_CONFIG_ID = TC.TES_CONFIG_ID)
   						LEFT JOIN USED_EVALUATIONS UE ON (A.USER_EVAL_CD = UE.USER_EVAL_CD)
   						JOIN (select MAIN_FORM_CD as FORM_CD,EVAL_FORM_CD FROM {fbx_datasource}.EVAL_FORM  UNION  SELECT EVAL_FORM_CD AS FORM_CD,EVAL_FORM_CD FROM {fbx_datasource}.EVAL_FORM )FR ON (A.EVAL_FORM_CD = FR.EVAL_FORM_CD)
   						JOIN (SELECT TCQ.MAPPING_EVAL_FORM_CD AS EVAL_FORM_CD,TCQ.TES_CONFIG_ID FROM UG_QUESTIONS_MAPPING_VIEW TCQ UNION ALL SELECT TCQ.EVAL_FORM_CD,TCQ.TES_CONFIG_ID FROM UG_QUESTIONS_MAPPING_VIEW TCQ) FORMS ON(FR.FORM_CD=FORMS.EVAL_FORM_CD and FORMS.TES_CONFIG_ID=tc.TES_CONFIG_ID)
  				WHERE  1=1
					AND UE.USER_EVAL_CD IS NULL
				    AND NOT EXISTS (
						SELECT
   							1
							FROM
							    {fbx_datasource}.QUESTION_TYPE QT
							    JOIN {fbx_datasource}.EVAL_QUESTION EQ ON (QT.QUESTION_TYPE_CD  = EQ.QUESTION_TYPE_CD)
							    JOIN {fbx_datasource}.S4Y_USER_ANSWER UA ON (UA.QUESTION_CD = EQ.QUESTION_CD)
							WHERE
							    UPPER(QT.QUESTION_TYPE_DESC) = 'FORM DISABLER'
							    AND UA.USER_EVAL_CD = A.USER_EVAL_CD
							    AND SAFE_TO_NUMBER(SUBSTR(UA.ANSWER_TEXT,1,1))=0
					)
 					AND A.COMPLETE_DT IS NOT NULL
  					AND
			            1 = (
			                CASE
			                    WHEN NOT
			                        EXISTS (
			                            SELECT
			                                1
			                            FROM
			                                tes_config_sessions tcs
			                            WHERE
			                                tc.tes_config_id = tcs.tes_config_id
			                        )
			                    THEN 1
			                    WHEN EXISTS (
			                        SELECT
			                            1
			                        FROM
			                            tes_config_sessions tcs
			                        WHERE
			                                tc.tes_config_id = tcs.tes_config_id
			                            AND
			                                tcs.training_session_cd = b.training_session_cd
			                    ) THEN 1
			                    ELSE -1
			                END
			            )
					)
				SELECT
				    USER_ID,
				    DOC_SETUP_ID,
				    COUNT(USER_EVAL_CD) AS UNPUBLISHED_EVALUATIONS,
					COUNT(DISTINCT EVALUATOR_USER_ID) AS DISTINCT_EVALUATORS,
				    CASE WHEN COUNT(DISTINCT EVALUATOR_USER_ID)>=MIN_EVALS
				    THEN '2'
				    ELSE '1'
				    END AS Setup_Status_id,
				    INSTANCE_ID
				FROM
				    EVALUATIONS
				GROUP BY
				    INSTANCE_ID,
				    USER_ID,
				    DOC_SETUP_ID,
				    MIN_EVALS
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>