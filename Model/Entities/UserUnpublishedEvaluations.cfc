<cfcomponent name="UserUnpublishedEvaluations" code="QB_UNPUBLISHED_USERS" displayName="UserUnpublishedEvaluations"  dbViewName="USER_UNPUBLISHED_EVALUATIONS" dbMaterializedView="true" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="DOC_SETUP_ID" label="ID" type="numeric" key="true">
	<cfproperty name="USER_NAME" label="Supervisor" type="string" id="USER_ID">
	<cfproperty name="DOC_SETUP_NAME" label="Setup Name" type="string" id="DOC_SETUP_ID">
	<cfproperty name="INSTANCE_ID" label="Instance ID" type="string">

	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_QUESTION",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="USER_FORM_ANSWER",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="ESCH_EVALUATION",OBJECT_DATASOURCE="{eval_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="UD_USER",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"}
		]>
		<cfreturn grants>
	</cffunction>

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
				WITH USED_EVALUATIONS AS (
				    SELECT
						    A.DOC_SNAPSHOT_CD,
						    A.USER_ID,
						    A.EVALUATION_ID AS EVALUATION_ID
						FROM
						    DOC_SNAPSHOT_EVALUATIONS A,
						    DOC_SNAPSHOT_BASE B,
						    DOC_INSTANCE C
						WHERE
						        A.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
						    AND
						        B.INSTANCE_ID = C.INSTANCE_ID
						    AND B.INSTANCE_ID = 5

					),MAPPING_QUESTIONS AS (
					    SELECT
					       DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM QUESTIONS_MAPPING_VIEW
					),
				  ESCHACTIVITIES AS (
				    SELECT
				        A.PROGRAM_ID AS PROGRAM_CD,
				        B.ACTIVITY_ID
				    FROM
				        {eval_datasource}.ESCHROTATION_VIEW A,
				        {eval_datasource}.ESCH_ACTIVITY B
				    WHERE
				        A.EXTERNAL_ID = B.EXTERNAL_ID
				    UNION
				    SELECT
				        A.EVENT_PROGRAM_ID AS PROGRAM_CD,
				        B.ACTIVITY_ID
				    FROM
				        {eval_datasource}.ESCHEVENT_VIEW A,
				        {eval_datasource}.ESCH_ACTIVITY B
				    WHERE
				        A.EXTERNAL_ID = B.EXTERNAL_ID
				    ),
					EVALS AS (
						SELECT * FROM
					   		(
					            SELECT
					                EVALUATION_ID
					            FROM
					                {eval_datasource}.ESCH_EVALUATION EV
					            MINUS
					            SELECT
					                TO_NUMBER(EVALUATION_ID)
					            FROM
					                USED_EVALUATIONS
					        )
					),
					EVALUATIONS AS (
					    SELECT
					        EV.EVALUATEE_USER_ID as USER_ID,
					        LS.INSTANCE_ID,
					        EV.EVALUATION_ID,
					        LS.DOC_SETUP_ID,
					        EV.EVALUATOR_USER_ID,
					        T.MIN_EVALS
					    FROM
					        {eval_form_datasource}.EVAL_QUESTION A,
					        {eval_form_datasource}.USER_FORM_ANSWER UA,
					        {eval_datasource}.ESCH_EVALUATION EV,
					        {eval_datasource}.ESCH_EVAL_DETAILS B,
					        {eval_datasource}.ESCH_ACTIVITY EA,
							{pg_datasource}.TRAINING_SESSION SESS,
							ESCHACTIVITIES D,
							TES_CONFIG_PROGRAMS C,
							TES_CONFIG T,
					        MAPPING_QUESTIONS TCQ,
					        DOC_SETUP LS,
					        EVALS
					    WHERE
					            A.QUESTION_TYPE_CD = 1
					        AND
					            UA.USER_FORM_CD = EV.USER_FORM_ID
					        AND
					            UA.QUESTION_CD = A.QUESTION_CD
					        AND

					            TCQ.DOC_SETUP_ID = LS.DOC_SETUP_ID
					        AND EV.IS_DELETED IS NULL
					        AND EV.EVAL_STATUS_ID = 3
					        AND (
					                NVL(
					                    A.MAIN_QUESTION_CD,
					                    A.QUESTION_CD
					                ) = TCQ.QUESTION_ID
					            OR
					                NVL(
					                    A.MAIN_QUESTION_CD,
					                    A.QUESTION_CD
					                ) = TCQ.GROUP_QUESTION_ID
					        ) AND EV.EVALUATION_ID = EVALS.EVALUATION_ID
					        AND EV.EVAL_DETAILS_ID = B.EVAL_DETAILS_ID
						    AND B.START_DT BETWEEN SESS.START_DT AND SESS.END_DT
						    AND
						     1 = (
						          CASE
						              WHEN NOT EXISTS (
						                  SELECT
						                      1
						                  FROM
						                      TES_CONFIG_SESSIONS TCS,
						                      TES_CONFIG TC
						                  WHERE
						                      TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
						                   AND TC.DOC_SETUP_ID= LS.DOC_SETUP_ID

						              ) THEN 1
						              WHEN EXISTS (
						                  SELECT
						                      1
						                  FROM
						                      TES_CONFIG_SESSIONS TCS,
						                      TES_CONFIG TC
						                  WHERE
						                      TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
								    		  AND TC.DOC_SETUP_ID= LS.DOC_SETUP_ID
						                      AND TCS.TRAINING_SESSION_CD = SESS.TRAINING_SESSION_CD
						              ) THEN 1
						              ELSE -1
						          END
						      )
						       AND (D.PROGRAM_CD = C.PROGRAM_CD)
						       AND	B.ACTIVITY_ID = D.ACTIVITY_ID
						       AND	B.ACTIVITY_ID = EA.ACTIVITY_ID
						       AND TRUNC(EA.END_DT) <= TRUNC(NVL(T.ACTIVITY_END_DT,EA.END_DT))
						       AND C.TES_CONFIG_ID = T.TES_CONFIG_ID
						       AND T.DOC_SETUP_ID = LS.DOC_SETUP_ID
						       AND
							        (
							            SELECT
							                SUM(SAFE_TO_NUMBER(NVL(
							                    ANSWER_TEXT,
							                    0
							                ) ) )
							            FROM
							                {eval_form_datasource}.USER_FORM_ANSWER
							            WHERE
							                USER_FORM_CD = EV.USER_FORM_ID
							        ) > 0
					) SELECT
					    USER_ID,
					    DOC_SETUP_ID,
					    COUNT(DISTINCT EVALUATION_ID) AS UNPUBLISHED_EVALUATIONS,
					    COUNT(DISTINCT EVALUATOR_USER_ID) AS DISTINCT_EVALUATORS,
					    CASE WHEN COUNT(DISTINCT EVALUATOR_USER_ID)>=MIN_EVALS
					    THEN '2'
					    ELSE '1'
					    END AS Setup_Status_id,
					    INSTANCE_ID,
					    MIN_EVALS
					FROM
					    EVALUATIONS
					GROUP BY
					    INSTANCE_ID,
					    USER_ID,
					    DOC_SETUP_ID,
					    MIN_EVALS

			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>

