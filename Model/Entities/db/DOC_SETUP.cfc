<cfcomponent name="DOC_SETUP" displayname="DOC_SETUP" code="DOC_SETUP" output="false" table="DOC_SETUP" extends="sis_core.model.blAutomation.EntityBase" accessors="true" >

	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="DOC_SETUP_ID" type="numeric" label="Tes config Id" key="true" seq="DOC_SETUP_SEQ" >
	<cfproperty name="INSTANCE_ID" type="numeric" label="Instance CD" validations="required">
	<cfproperty name="DOC_LETTER_TEMPLATE_CD" type="numeric" label="Item Group CD" validations="required">
	<cfproperty name="DOC_TYPE_CD"  type="numeric" label="Loa Type CD">
	<cfproperty name="DOC_SETUP_NAME"  type="string" label="Loa Setup Name" validations="required,max_length(200)" >
	<cfproperty name="CREATION_ID" type="numeric" label="Creation ID">
	<cfproperty name="MODIFICATION_ID" type="numeric" label="Modification ID">
	<cfproperty name="CREATION_DT" type="date" label="Creation Dt">
	<cfproperty name="MODIFICATION_DT" type="date" label="Modification Dt">

	<cffunction name="init" access="public" output="false" returntype="DOC_SETUP">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>

</cfcomponent>
