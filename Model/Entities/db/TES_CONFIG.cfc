<cfcomponent name="TES_CONFIG" displayname="TES_CONFIG" code="TES_CONFIG" output="false" table="TES_CONFIG" extends="sis_core.model.blAutomation.EntityBase" accessors="true" >

	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="TES_CONFIG_ID" type="numeric" label="Tes config Id" key="true" seq="DOC_CONFIG_SEQ" >
	<cfproperty name="DOC_SETUP_ID" type="numeric" label="Item Group CD" key="true">
	<cfproperty name="INSTANCE_ID" type="numeric" label="Instance CD" validations="required">
	<cfproperty name="EVAL_FORM_CD" type="numeric" label="Item Group CD" validations="required">
	<cfproperty name="SHOW_COMMENTS"  type="boolean" label="Show Comments" >
	<cfproperty name="ACTIVITY_END_DT"  type="date" label="Activity End Date" >
	<cfproperty name="MIN_EVALS"  type="string" label="Minimum Number of Completed Forms" validations="required,numeric,interval(1;100)">
	<cfproperty name="SCORE_BASE"  type="string" label="Score base">
	<cfproperty name="CREATION_ID" type="numeric" label="Creation ID">
	<cfproperty name="MODIFICATION_ID" type="numeric" label="Modification ID">
	<cfproperty name="CREATION_DT" type="date" label="Creation Dt">
	<cfproperty name="MODIFICATION_DT" type="date" label="Modification Dt">
	<cfproperty name="GENERATE_SCHEDULE_ID" type="numeric" label="">
	<cfproperty name="PUBLISH_SCHEDULE_ID" type="numeric" label="">

	<cffunction name="init" access="public" output="false" returntype="TES_CONFIG">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>

</cfcomponent>
