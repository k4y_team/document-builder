<cfcomponent name="TES_CONFIG_PROGRAMS" displayname="TES_CONFIG_PROGRAMS" code="TES_CONFIG_PROGRAMS" output="false" table="TES_CONFIG_PROGRAMS" extends="sis_core.model.blAutomation.EntityBase" accessors="true" >

	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="TES_CONFIG_PROGRAM_ID" type="numeric" label="Tes config Id" key="true" seq="TES_CONFIG_PROGRAMS_SEQ" >
	<cfproperty name="TES_CONFIG_ID" type="numeric" label="Tes config Id"  key="true" validations="required">
	<cfproperty name="PROGRAM_CD" type="string" label="Program CD" key="true" validations="required">


	<cffunction name="init" access="public" output="false" returntype="TES_CONFIG_PROGRAMS">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>

</cfcomponent>
