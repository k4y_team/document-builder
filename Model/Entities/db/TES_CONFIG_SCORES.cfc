<cfcomponent name="TES_CONFIG_SCORES" displayname="TES_CONFIG_SCORES" code="TES_CONFIG_SCORES" output="false" table="TES_CONFIG_SCORES" extends="sis_core.model.blAutomation.EntityBase" accessors="true" >

	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="TES_CONFIG_ID" type="numeric" label="Tes config Id" key="true" >
	<cfproperty name="DOC_SETUP_ID" type="numeric" label="Item Group CD"  key="true" validations="required">
	<cfproperty name="SCORE_TYPE_ID" type="numeric" label="Item Group CD" key="true" validations="required">

	<cffunction name="init" access="public" output="false" returntype="TES_CONFIG_SCORES">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>

</cfcomponent>
