<cfcomponent name="TES_CONFIG_SESSIONS" displayname="TES_CONFIG_SESSIONS" code="TES_CONFIG_SESSIONS" output="false" table="TES_CONFIG_SESSIONS" extends="sis_core.model.blAutomation.EntityBase" accessors="true" >

	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="TES_CONFIG_SESSION_ID" type="numeric" label="Tes config sessionId" key="true" seq="TES_CONFIG_SESSIONS_SEQ" >
	<cfproperty name="TES_CONFIG_ID" type="numeric" label="Tes config Id"  key="true" validations="required">
	<cfproperty name="TRAINING_SESSION_CD" type="string" label="Training Session CD" key="true" validations="required">


	<cffunction name="init" access="public" output="false" returntype="TES_CONFIG_SESSIONS">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>

</cfcomponent>
