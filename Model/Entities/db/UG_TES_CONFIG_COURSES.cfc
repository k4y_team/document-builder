<cfcomponent name="UG_TES_CONFIG_COURSES" displayname="UG_TES_CONFIG_COURSES" code="UG_TES_CONFIG_COURSES" output="false" table="UG_TES_CONFIG_COURSES" extends="sis_core.model.blAutomation.EntityBase" accessors="true" >

	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="TES_CONFIG_COURSE_ID" type="numeric" label="Tes config Id" key="true" seq="UG_TES_CONFIG_COURSES_SEQ" >
	<cfproperty name="TES_CONFIG_ID" type="numeric" label="Tes config Id"  key="true" validations="required">
	<cfproperty name="COURSE_CD" type="string" label="Course CD" key="true" validations="required">


	<cffunction name="init" access="public" output="false" returntype="UG_TES_CONFIG_COURSES">
		<cfset super.init()>
		<cfreturn this>
	</cffunction>

</cfcomponent>
