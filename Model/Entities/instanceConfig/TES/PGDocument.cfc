<cfcomponent name="PGDocument" code="QB_PG_DOCUMENT" displayName="PGDocument" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables">

	<cfproperty name="DOC_SNAPSHOT_CD" type="numeric" key="true">
	<cfproperty name="PROGRAM_CD" type="string">
	<cfproperty name="PUBLISHED_STATUS" label="Status" type="string" id="PUBLISHED_STATUS_ID">
	<cfproperty name="GENERATED_DATE" type="date" label="Generate Date">

	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="UD_USER",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"}
		]>
		 <cfreturn grants>
	</cffunction>

	<cfscript>
		public string function getDataProvider_PUBLISHED_STATUS() {
        	var _sql = "SELECT
				    	*
					FROM
				    (
				        SELECT
				            1 AS idfield,
				            'Published' AS descfield,
				            1 AS order_nr
				        FROM
				            dual
				        UNION
				        SELECT
				            2 AS idfield,
				            'Not Published' AS descfield,
				            2 AS order_nr
				        	FROM
            	dual
    			)
				ORDER BY order_nr";
        	return _sql;
   	 }
	</cfscript>

	<cffunction name="getSQL" access="public" returntype="String">
		<cfset var sql = "">
		<cfsavecontent variable="sql">
			<cfoutput>
				SELECT
				    A.DOC_SNAPSHOT_CD,
				    C.PROGRAM_CD,
				    NVL2(A.DOC_PUBLISHED_DT, 1 , 2) AS PUBLISHED_STATUS_ID,
					NVL2(A.DOC_PUBLISHED_DT,'Published','Not Published') AS PUBLISHED_STATUS,
					U.LAST_NAME||', '||U.FIRST_NAME AS USER_NAME,
					RP.PROGRAM_NAME,
					A.CREATION_DT as GENERATED_DATE,
					S.PUBLISH_DT
				FROM
				    DOC_SNAPSHOT_BASE A,
				    TES_CONFIG B,
				    TES_CONFIG_PROGRAMS C,
				    {pg_datasource}.UD_USER U,
				    {pg_datasource}.RSPROGRAM_VIEW RP,
                    SETUPDETAIL_VIEW S
				WHERE
				        A.DOC_SETUP_ID = B.DOC_SETUP_ID
				    AND
				        B.TES_CONFIG_ID = C.TES_CONFIG_ID
				    AND
				    	U.USER_ID = A.USER_ID
				    AND
				    	C.PROGRAM_CD = RP.PROGRAM_ID
                    AND
						A.DOC_SETUP_ID = S.DOC_SETUP_ID
			</cfoutput>
		</cfsavecontent>
		<cfreturn sql>
	</cffunction>
</cfcomponent>

