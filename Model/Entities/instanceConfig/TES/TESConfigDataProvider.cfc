<cfcomponent name="TESConfigDataProvider" code="QB_TES_CONFIG_DATA_PROVIDER" displayName="TESConfigDataProvider" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" label="" inject="coldbox:setting:datasource" scope="variables"/>

	<cfproperty name="COMPARISONS_DEF"  type="string">

	<cfscript>

	public TESConfigDataProvider function init() {
		variables.args = arguments;


		super.init();
		return this;
	}

	</cfscript>

	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_QUESTION",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="USER_FORM",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="RATING_SCALE_GROUP",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="RATING_SCALE",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="ESCH_EVALUATION",OBJECT_DATASOURCE="{eval_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="ESCH_EVAL_DETAILS",OBJECT_DATASOURCE="{eval_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="ESCH_ACTIVITY",OBJECT_DATASOURCE="{eval_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="ESCHROTATION_VIEW",OBJECT_DATASOURCE="{eval_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="ESCHEVENT_VIEW",OBJECT_DATASOURCE="{eval_datasource}",GRANT_TYPES="SELECT"}]>,
			{OBJECT="EVAL_SUPERVISOR_APPOINTMENT",OBJECT_DATASOURCE="{fbx_datsource}",GRANT_TYPES="SELECT"}]>

		<cfreturn grants>
	</cffunction>


	<cffunction name="getSQLS" access="public" output="true" returntype="struct">
		<cfset var sqls = StructNew()>
		<cfset ratingGroupsSql =''>
		<cfset questionDefinitionsSql = ''>
		<cfset ratingScalesSql = ''>
		<cfset scoresSql = ''>
		<cfset comparisonsSql = ''>

		<cfsavecontent variable ="ratingGroupsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
						{% set data =[{rating_group_cd:1,rating_group_desc:'Rating Descriptin'}]%}
						{{macros.SQLFromArray(data)}}
				{%else %}
						SELECT DISTINCT
					    C.RATING_GROUP_CD,
					    C.RATING_GROUP_DESC
					FROM
					    {eval_form_datasource}.EVAL_QUESTION A,
					    {eval_form_datasource}.RATING_SCALE_GROUP C,
					    (SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            SAFE_TO_NUMBER(RATING_VALUE) AS RATING_VALUE,
					            WEIGHT,
					            COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            0 AS RATING_NR,
					            0 AS RATING_VALUE,
					            0 AS WEIGHT,
					            '' AS COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE_GROUP) d,
					    TES_CONFIG TC,
					    QUESTIONS_MAPPING_VIEW TCQ
					WHERE
					        A.QUESTION_TYPE_CD = 1
					    AND
					        A.RATING_GROUP_CD = C.RATING_GROUP_CD
					    AND
					        C.RATING_GROUP_CD = D.RATING_GROUP_CD
					    AND TC.EVAL_FORM_CD = A.EVAL_FORM_CD
					    AND TC.TES_CONFIG_ID = TCQ.TES_CONFIG_ID
					    AND A.QUESTION_CD = TCQ.GROUP_QUESTION_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					   ORDER BY  RATING_GROUP_CD ASC
				 {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="questionDefinitionsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,question_desc:'Overall Teaching Effectiveness: Overall for this clinical experience, what is your opinion of the effectiveness of the instructor?',RATING_GROUP_CD:1},
								  {question_cd:2,question_desc:'Evaluation (if applicable): A variety of observation methods were utilized including case discussion and direct observation. Evaluation was fair, well constructed and provided in a timely manner. Feedback was received and was constructive and informative.',RATING_GROUP_CD:1}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT DISTINCT
					    a.question_cd,
					    a.question_desc,
					    A.RATING_GROUP_CD,
					    A.QUESTION_NR,
					    G.GROUP_NR
					FROM
					    {eval_form_datasource}.eval_question a,
					    {eval_form_datasource}.eval_group g,
						QUESTIONS_MAPPING_VIEW TCQ
					WHERE
					        a.question_type_cd = 1
					    AND A.GROUP_CD = G.GROUP_CD
					    AND A.QUESTION_CD = TCQ.GROUP_QUESTION_ID
					  	{% if DATA.DOC_SETUP_ID is defined %}
							AND TCQ.DOC_SETUP_ID  in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					    ORDER BY G.GROUP_NR ASC,A.QUESTION_NR ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="ratingScalesSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{rating_cd:1,rating_desc:'Poor',rating_nr:1,rating_value:1,weight:1,rating_group_cd:1},
								  {rating_cd:2,rating_desc:'Fair',rating_nr:2,rating_value:2,weight:2,rating_group_cd:1},
								  {rating_cd:3,rating_desc:'Good',rating_nr:3,rating_value:3,weight:3,rating_group_cd:1},
								  {rating_cd:4,rating_desc:'Satisfactory',rating_nr:4,rating_value:4,weight:4,rating_group_cd:1},
								  {rating_cd:5,rating_desc:'Excellent',rating_nr:5,rating_value:5,weight:5,rating_group_cd:1}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT DISTINCT
						    D.RATING_CD,
						    D.RATING_DESC,
						    D.RATING_NR,
						    D.RATING_VALUE,
						    D.WEIGHT,
						    D.COLOR,
						    C.RATING_GROUP_CD,
						    C.RATING_GROUP_DESC
						FROM
						    {eval_form_datasource}.EVAL_QUESTION A,
						    {eval_form_datasource}.RATING_SCALE_GROUP C,
						   	(SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            RATING_VALUE,
					            WEIGHT,
					            COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            NULL AS RATING_NR,
					            '' AS RATING_VALUE,
					            0 AS WEIGHT,
					            '' AS COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE_GROUP) d,
					    	TES_CONFIG TC
						WHERE
						        A.QUESTION_TYPE_CD = 1
						    AND
						        A.EVAL_FORM_CD = TC.EVAL_FORM_CD
						    AND
						        A.RATING_GROUP_CD = C.RATING_GROUP_CD
						    AND
						        C.RATING_GROUP_CD = D.RATING_GROUP_CD
						  	{% if DATA.DOC_SETUP_ID is defined %}
								AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    	{% endif %}
						   ORDER BY  RATING_NR ASC,RATING_GROUP_CD ASC,D.WEIGHT ASC
			 {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="scoresSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{score_type_id:1,score_type_desc:'Mean Value',score_type_code:'MEAN_VALUE'},
								  {score_type_id:2,score_type_desc:'Median Value',score_type_code:'MEDIAN_VALUE'},
								  {score_type_id:3,score_type_desc:'Standard Deviation Value',score_type_code:'SDV'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
					    SC.SCORE_TYPE_ID,
					    SC.SCORE_TYPE_DESC,
					    SC.SCORE_TYPE_CODE
					FROM
					    TES_SCORE SC,
					    TES_CONFIG_SCORES TCS,
					    TES_CONFIG TC
					WHERE
					        SC.SCORE_TYPE_ID = TCS.SCORE_TYPE_ID
					    AND
					        TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					    order by SCORE_TYPE_ID ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="comparisonsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{comparison_type_id:1,comparison_type_desc:'All Teachers',comparison_type_code:'ALL_TEACHERS'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
					    TC.COMPARISON_TYPE_ID,
					    TC.COMPARISON_TYPE_DESC,
					    TC.COMPARISON_TYPE_CODE
					FROM
					    TES_COMPARISON TC,
					    TES_CONFIG_COMPARISONS TCC,
					    TES_CONFIG TC
					WHERE
					       TC.COMPARISON_TYPE_ID =TCC.COMPARISON_TYPE_ID
					    AND
					        TC.TES_CONFIG_ID = TCC.TES_CONFIG_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND	TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="reportsSql">
			<cfoutput>
				SELECT XX.* FROM (
					WITH DOCUMENT_ELIGIBLE_USERS AS (
						SELECT DISTINCT
		                	A.USER_ID,
		                	A.USER_NAME,
                        	B.DOC_SETUP_ID,
                       		B.DOC_SETUP_NAME,
							A.DOC_SETUP_ELIGIBLE_USER_ID
		            FROM
		                DOCSETUPELIGIBLEUSERS_VIEW A,
		                DOC_SETUP B,
		                UD_USER U
		            WHERE
		                    A.DOC_SETUP_ID = B.DOC_SETUP_ID
		                AND
		                    A.USER_ID = U.USER_ID
		                {% if DATA.DOC_LETTER_INSTANCE_CD is defined %}
		                 	AND   B.INSTANCE_ID = {{ DATA.DOC_LETTER_INSTANCE_CD}}
		                {% endif %}
		                {% if DATA.SUPERVISOR_ID is defined %}
							AND	{{macros.whereClause('A.USER_ID',join(DATA.SUPERVISOR_ID, '\',\''))}}
		                {% endif %}
		                {% if DATA.LOCATION_RESTRICTIONS_LIST is defined %}
			             AND EXISTS ( SELECT 1
								FROM
								    {fbx_datasource}.EVAL_SUPERVISOR_USER ESU,
								    {fbx_datasource}.SUPERVISOR_APPOINTMENT SA,
								    {fbx_datasource}.APPOINTMENT_TYPE AT,
								    {fbx_datasource}.ORGANIZATION H
								WHERE
								        ESU.USER_ID = A.USER_ID
								    AND
								        ESU.SUPERVISOR_ID = SA.SUPERVISOR_ID
								    AND
								        SA.APPOINTMENT_GROUP = 2
								    AND
								    	AT.APPOINTMENT_TYPE_CD in (1,2)
								    AND
								        SA.APPOINTMENT_TYPE_CD = AT.APPOINTMENT_TYPE_CD
								    AND
								        SA.ORG_CD = H.ORG_CD
								    AND	{{macros.whereClause('SA.ORG_CD',join(DATA.LOCATION_RESTRICTIONS_LIST, '\',\''))}}
						)
			            {% endif %}
		                {% if DATA.LOCATION_ID is defined %}
		             	   AND EXISTS ( SELECT 1
								FROM
								    {fbx_datasource}.EVAL_SUPERVISOR_USER ESU,
								    {fbx_datasource}.SUPERVISOR_APPOINTMENT SA,
								    {fbx_datasource}.APPOINTMENT_TYPE AT,
								    {fbx_datasource}.ORGANIZATION H
								WHERE
								        ESU.USER_ID = A.USER_ID
								    AND
								        ESU.SUPERVISOR_ID = SA.SUPERVISOR_ID
								    AND
								        SA.APPOINTMENT_GROUP = 2
								    AND
								    	AT.APPOINTMENT_TYPE_CD in (1,2)
								    AND
								        SA.APPOINTMENT_TYPE_CD = AT.APPOINTMENT_TYPE_CD
								    AND
								        SA.ORG_CD = H.ORG_CD
								    AND	{{macros.whereClause('SA.ORG_CD',join(DATA.LOCATION_ID, '\',\''))}}
								    {% if DATA.LOCATION_TYPE_ID is defined %}
								    	AND	{{macros.whereClause('AT.APPOINTMENT_TYPE_CD',join(DATA.LOCATION_TYPE_ID, '\',\''))}}
								    {% endif %}
						)
		             	{% endif %}
		             	{% if DATA.LOCATION_TYPE_ID is defined %}
		             	   AND EXISTS ( SELECT 1
								FROM
								    {fbx_datasource}.EVAL_SUPERVISOR_USER ESU,
								    {fbx_datasource}.SUPERVISOR_APPOINTMENT SA,
								    {fbx_datasource}.APPOINTMENT_TYPE AT,
								    {fbx_datasource}.ORGANIZATION H
								WHERE
								        ESU.USER_ID = A.USER_ID
								    AND
								        ESU.SUPERVISOR_ID = SA.SUPERVISOR_ID
								    AND
								        SA.APPOINTMENT_GROUP = 2
								    AND
								        SA.APPOINTMENT_TYPE_CD = AT.APPOINTMENT_TYPE_CD
								    AND
								        SA.ORG_CD = H.ORG_CD
								    AND	{{macros.whereClause('AT.APPOINTMENT_TYPE_CD',join(DATA.LOCATION_TYPE_ID, '\',\''))}}
								    {% if DATA.LOCATION_ID is defined %}
								    	AND	{{macros.whereClause('SA.ORG_CD',join(DATA.LOCATION_ID, '\',\''))}}
								    {% endif %}
						)
		             	{% endif %}
		                {% if DATA.SETUP_ID is defined %}
							AND	{{macros.whereClause('A.DOC_SETUP_ID',join(DATA.SETUP_ID, '\',\''))}}
		                {% endif %}
		                {% if DATA.TEMPLATE_ID is defined %}
							AND	{{macros.whereClause('B.DOC_LETTER_TEMPLATE_CD',join(DATA.TEMPLATE_ID, '\',\''))}}
		                {% endif %}
					),SNAPSHOTS  AS (
					     SELECT
					        A.USER_ID,
					        A.DOC_SNAPSHOT_CD,
					        A.DOC_PUBLISHED_USER_ID,
					        A.DOC_PUBLISHED_DT,
					        'Y' as ENABLE_PUBLISH,
					        NVL2(A.DOC_PUBLISHED_DT,2,1) AS PUBLISHED_STATUS_ID,
					        A.CREATION_DT,
					        A.INSTANCE_ID,
					        S.DOC_SETUP_ID,
					        S.DOC_SETUP_NAME AS REPORT_NAME,
					        T.LETTER_TEMPLATE_NAME,
					        T.DOC_LETTER_TEMPLATE_CD,
					        T.INSTRUCTIONS,
					        NVL2(P.USER_ID,P.FIRST_NAME|| ' '|| P.LAST_NAME,'') AS PUBLISHED_BY,
					        NVL2(G.USER_ID,G.FIRST_NAME|| ' '|| G.LAST_NAME,'') AS GENERATED_BY
					    FROM
					        DOC_SNAPSHOT_BASE A,
					        DOC_LETTER_TEMPLATE T,
					        DOC_SETUP S,
					        UD_USER P,
					        UD_USER G
					    WHERE
					            A.DOC_SETUP_ID = S.DOC_SETUP_ID
					        AND A.DOC_LETTER_TEMPLATE_CD = T.DOC_LETTER_TEMPLATE_CD
					        {% if DATA.DOC_LETTER_INSTANCE_CD is defined %}
		                 		AND   A.INSTANCE_ID = {{ DATA.DOC_LETTER_INSTANCE_CD}}
		                	{% endif %}
		                	{% if DATA.SETUP_ID is defined %}
								AND	{{macros.whereClause('A.DOC_SETUP_ID',join(DATA.SETUP_ID, '\',\''))}}
		                	{% endif %}
					        AND A.DOC_PUBLISHED_USER_ID =P.USER_ID(+)
					        AND A.CREATION_ID = G.USER_ID(+)
					),UNPUBLISHED AS (
						SELECT
					    	*
					    FROM
					    	USER_UNPUBLISHED_EVALUATIONS
					    WHERE 1=1
					       	{% if DATA.INSTANCE_ID is defined %}
		                 		AND   INSTANCE_ID = {{ DATA.INSTANCE_ID}}
		                	{% endif %}
		                	{% if DATA.SUPERVISOR_ID is defined %}
								AND	{{macros.whereClause('USER_ID',join(DATA.SUPERVISOR_ID, '\',\''))}}
		                	{% endif %}
					)
					SELECT
					    Z.USER_ID,
					    Z.USER_NAME,
					    Z.DOC_SETUP_NAME,
					    Z.DOC_SETUP_ID,
					    Z.DOC_SETUP_ELIGIBLE_USER_ID,
					    Y.INSTANCE_ID,
					    Y.DOC_SNAPSHOT_CD,
					    Y.LETTER_TEMPLATE_NAME,
					    Y.DOC_LETTER_TEMPLATE_CD,
					    Y.INSTRUCTIONS,
					    Y.ENABLE_PUBLISH,
					    Y.DOC_PUBLISHED_DT,
					    Y.PUBLISHED_STATUS_ID,
					    NVL2(Y.DOC_PUBLISHED_DT, 'Published', 'Not Published') AS PUBLISHED_STATUS,
					    TO_CHAR(Y.CREATION_DT,'dd-Mon-yyyy') as GENERATE_DT,
					    EPD.EVALUATION_PERIOD,
					    A.Details || '</br> Evaluation Period: <b>' ||EPD.EVALUATION_PERIOD||'</b>' as Details,
					    Y.DOC_PUBLISHED_USER_ID,
					    Y.PUBLISHED_BY,
					    Y.GENERATED_BY,
					    Y.REPORT_NAME,
					    DECODE(
					        COUNT(
					            Y.DOC_SNAPSHOT_CD
					        ) OVER(PARTITION BY
					            Y.USER_ID, Y.DOC_SETUP_ID
					        ),
					        0,
					        0,
					        COUNT(
					            Y.DOC_SNAPSHOT_CD
					        ) OVER(PARTITION BY
					            Y.USER_ID, Y.DOC_SETUP_ID
					        )
					    ) HISTORICAL_TES_REPORTS,
					    NVL(U.UNPUBLISHED_EVALUATIONS,0) AS UNPUBLISHED_EVALUATIONS,
					    NVL(U.DISTINCT_EVALUATORS,0) AS DISTINCT_EVALUATORS,
					    DECODE(U.SETUP_STATUS_ID,'2','Ready','Not Ready') as SETUP_STATUS,
					    NVL(U.SETUP_STATUS_ID,'1') as SETUP_STATUS_ID
					FROM
					    DOCUMENT_ELIGIBLE_USERS Z,
					    SNAPSHOTS Y,
					    UNPUBLISHED U,
					    SETUPDETAIL_VIEW A,
					    DOC_SNAPSHOT_EVAL_PERIOD EPD
					WHERE  Z.USER_ID = Y.USER_ID(+)
					AND Z.DOC_SETUP_ID = Y.DOC_SETUP_ID (+)
					AND Z.DOC_SETUP_ID = A.DOC_SETUP_ID (+)
					AND Z.USER_ID = U.USER_ID(+)
					AND Z.DOC_SETUP_ID = U.DOC_SETUP_ID (+)
					AND Y.DOC_SNAPSHOT_CD = EPD.DOC_SNAPSHOT_CD(+)

		            {% if DATA.SETUP_STATUS_ID is defined %}
						AND	{{macros.whereClause('NVL(U.SETUP_STATUS_ID,1)',join(DATA.SETUP_STATUS_ID, '\',\''))}}
		            {% endif %}
		            {% if DATA.PROGRAM_CD is defined %}
		             AND Z .DOC_SETUP_ID in (SELECT a.DOC_SETUP_ID FROM tes_config a,tes_config_programs b
							WHERE	a.tes_config_id = b.tes_config_id(+) and (b.program_cd is null or {{macros.whereClause('b.program_cd',join(DATA.PROGRAM_CD, '\',\''))}}))
		             {% endif %}
		            {% if DATA.PROGRAM_RESTRICTIONS_LIST is defined %}
		             AND Z .DOC_SETUP_ID in (SELECT a.DOC_SETUP_ID FROM tes_config a,tes_config_programs b
							WHERE	a.tes_config_id = b.tes_config_id(+) and (b.program_cd is null or {{macros.whereClause('b.program_cd',join(DATA.PROGRAM_RESTRICTIONS_LIST, '\',\''))}}))
		            {% endif %}
		            {% if (DATA.DOC_SNAPSHOT_CD is defined) and (DATA.DOC_SNAPSHOT_CD !='') %}
		            	AND  ({{macros.listSplit('Y.DOC_SNAPSHOT_CD',DATA.DOC_SNAPSHOT_CD,500)}})
		            {% endif %}
		            {% if (DATA.USER_ID is defined) and (DATA.USER_ID !='') %}
		            	AND  Y.USER_ID in ({{ DATA.USER_ID}})
		            {% endif %}
		            ORDER BY  Z.DOC_SETUP_NAME ASC,Y.DOC_PUBLISHED_DT DESC NULLS FIRST
		            ) XX WHERE 1=1
		            {% if DATA.STATUS is defined %}
						AND	{{macros.whereClause('XX.PUBLISHED_STATUS_ID',join(DATA.STATUS, '\',\''))}}
		            {% endif %}
		            {% if DATA.PUBLISHED_DT is defined %}
		            	{% if DATA.PUBLISHED_DT.TYPE =='equal' %}
							AND	{{macros.whereClause("TO_CHAR(XX.DOC_PUBLISHED_DT,'MM-DD-YYYY')",join(DATA.PUBLISHED_DT.VALUE, '\',\''))}}
						{% endif %}
						{% if DATA.PUBLISHED_DT.TYPE =='between' %}
							AND	{{macros.betweenClause("trunc(XX.DOC_PUBLISHED_DT)",DATA.PUBLISHED_DT.VALUE)}}
						{% endif %}
		            {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="eligibleUsersSql">
			<cfoutput>
				WITH ESCHACTIVITIES AS (
				    SELECT
				        A.PROGRAM_ID AS PROGRAM_CD,
				        B.ACTIVITY_ID
				    FROM
				        {eval_datasource}.ESCHROTATION_VIEW A,
				        {eval_datasource}.ESCH_ACTIVITY B
				    WHERE
				        A.EXTERNAL_ID = B.EXTERNAL_ID
				    UNION
				    SELECT
				        A.EVENT_PROGRAM_ID AS PROGRAM_CD,
				        B.ACTIVITY_ID
				    FROM
				        {eval_datasource}.ESCHEVENT_VIEW A,
				        {eval_datasource}.ESCH_ACTIVITY B
				    WHERE
				        A.EXTERNAL_ID = B.EXTERNAL_ID
				) SELECT DISTINCT
				    A.DOC_SETUP_ID,
				    E.EVALUATEE_USER_ID AS USER_ID
				FROM
				    DOC_SETUP A,
				    TES_CONFIG B,
				    TES_CONFIG_PROGRAMS C,
				    ESCHACTIVITIES D,
				    (SELECT
                		EV.*
				            FROM
				                {eval_datasource}.ESCH_EVALUATION EV,
				                {eval_datasource}.ESCH_EVAL_DETAILS B,
								{pg_datasource}.TRAINING_SESSION SESS
							WHERE
						        EV.EVAL_DETAILS_ID = B.EVAL_DETAILS_ID
						    AND
						        B.START_DT BETWEEN SESS.START_DT AND SESS.END_DT
						    AND
						     1 = (
						          CASE
						              WHEN NOT EXISTS (
						                  SELECT
						                      1
						                  FROM
						                      TES_CONFIG_SESSIONS TCS,
						                      TES_CONFIG TC
						                  WHERE
						                      TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
						                  {% if DATA.DOC_SETUP_ID is defined %}
								    		AND TC.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                				  {% endif %}
						              ) THEN 1
						              WHEN EXISTS (
						                  SELECT
						                      1
						                  FROM
						                      TES_CONFIG_SESSIONS TCS,
						                      TES_CONFIG TC
						                  WHERE
						                      TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
						                      {% if DATA.DOC_SETUP_ID is defined %}
								    			AND TC.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                					  {% endif %}
						                      AND TCS.TRAINING_SESSION_CD = SESS.TRAINING_SESSION_CD
						              ) THEN 1
						              ELSE -1
						          END
						      ))E,
				    {eval_datasource}.ESCH_EVAL_DETAILS F,
				    {eval_datasource}.ESCH_ACTIVITY EA
				WHERE
				        A.DOC_SETUP_ID = B.DOC_SETUP_ID
				    {% if DATA.DOC_SETUP_ID is defined %}
						AND A.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		            {% endif %}
				    AND B.TES_CONFIG_ID = C.TES_CONFIG_ID(+)
				    AND
				        (D.PROGRAM_CD = C.PROGRAM_CD)
				    AND
				        F.ACTIVITY_ID = D.ACTIVITY_ID
				     AND
				        F.ACTIVITY_ID = EA.ACTIVITY_ID
				    AND
				        E.EVAL_DETAILS_ID = F.EVAL_DETAILS_ID
				    AND E.EVAL_STATUS_ID = 3
				    AND TRUNC(EA.END_DT) <= TRUNC(NVL(B.ACTIVITY_END_DT,EA.END_DT))
				    AND E.IS_DELETED IS NULL
				    AND F.EVAL_FORM_ID IN (select eval_form_cd from {eval_form_datasource}.eval_form where eval_form_cd = b.eval_form_cd or main_form_cd = b.eval_form_cd
				     UNION ALL
				     SELECT
            			eval_form_cd
				        FROM
				            {eval_form_datasource}.eval_form
				        WHERE
				            eval_form_cd in (select eval_form_cd from tes_config_question_mappings where document_setup_id = A.DOC_SETUP_ID)
				            OR main_form_cd  in (select eval_form_cd from tes_config_question_mappings where document_setup_id = A.DOC_SETUP_ID)
				    )
				    AND E.EVALUATEE_USER_ID IS NOT NULL
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="unpublishedEvalsSql">
			<cfoutput>
				  SELECT
		            D.*,
		            C.USER_ID
		        FROM
		            USER_UNPUBLISHED_EVALUATIONS C,
		            TES_CONFIG D
		        WHERE
		                C.DOC_SETUP_ID = D.DOC_SETUP_ID
		            AND
		                D.MIN_EVALS <= C.DISTINCT_EVALUATORS
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="reportQuestionsSql">
			<cfoutput>
				SELECT
				     DOC_SETUP_ID,
				     TCQ.GROUP_QUESTION_ID AS REPORT_QUESTION_CD,
				     A.QUESTION_CD AS EVAL_QUESTION_CD
				FROM
				    QUESTIONS_MAPPING_VIEW TCQ,
				    {eval_form_datasource}.EVAL_QUESTION A
				WHERE
					{% if DATA.DOC_SETUP_ID is defined %}
						 TCQ.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}}) AND
		            {% endif %}
				     (
				            NVL(
				                A.MAIN_QUESTION_CD,
				                A.QUESTION_CD
				            ) = TCQ.QUESTION_ID
				        OR
				            NVL(
				                A.MAIN_QUESTION_CD,
				                A.QUESTION_CD
				            ) = TCQ.GROUP_QUESTION_ID)
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="docsetupSql">
			<cfoutput>
				WITH USED_EVALUATIONS AS (
				    SELECT
						    A.DOC_SNAPSHOT_CD,
						    A.USER_ID,
						    A.EVALUATION_ID AS EVALUATION_ID
						FROM
						    DOC_SNAPSHOT_EVALUATIONS A,
						    DOC_SNAPSHOT_BASE B,
						    DOC_INSTANCE C
						WHERE
						        A.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
						    AND
						        B.INSTANCE_ID = C.INSTANCE_ID
						     AND B.INSTANCE_ID = 5
					),MAPPING_QUESTIONS AS (
					    SELECT
					       DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM QUESTIONS_MAPPING_VIEW
					),
				  ESCHACTIVITIES AS (
				    SELECT
				        A.PROGRAM_ID AS PROGRAM_CD,
				        B.ACTIVITY_ID
				    FROM
				        {eval_datasource}.ESCHROTATION_VIEW A,
				        {eval_datasource}.ESCH_ACTIVITY B
				    WHERE
				        A.EXTERNAL_ID = B.EXTERNAL_ID
				    UNION
				    SELECT
				        A.EVENT_PROGRAM_ID AS PROGRAM_CD,
				        B.ACTIVITY_ID
				    FROM
				        {eval_datasource}.ESCHEVENT_VIEW A,
				        {eval_datasource}.ESCH_ACTIVITY B
				    WHERE
				        A.EXTERNAL_ID = B.EXTERNAL_ID
				    ),
					EVALS AS (
						SELECT * FROM
					   		(
					            SELECT
					                EVALUATION_ID
					            FROM
					                {eval_datasource}.ESCH_EVALUATION EV
					            MINUS
					            SELECT
					                TO_NUMBER(EVALUATION_ID)
					            FROM
					                USED_EVALUATIONS
					        )
					),
					EVALUATIONS AS (
					    SELECT
					        EU.USER_ID,
					        LS.INSTANCE_ID,
					        EV.EVALUATION_ID,
					        LS.DOC_SETUP_ID,
					        EV.EVALUATOR_USER_ID,
					        SESS.TRAINING_SESSION_CD
					    FROM
					        {eval_form_datasource}.EVAL_QUESTION A,
					        {eval_form_datasource}.USER_FORM_ANSWER UA,
					        {eval_datasource}.ESCH_EVALUATION EV,
					        {eval_datasource}.ESCH_EVAL_DETAILS B,
					        {eval_datasource}.ESCH_ACTIVITY EA,
							{pg_datasource}.TRAINING_SESSION SESS,
							ESCHACTIVITIES D,
							TES_CONFIG_PROGRAMS C,
							TES_CONFIG T,
					        MAPPING_QUESTIONS TCQ,
					        DOC_SETUP_ELIGIBLE_USERS EU,
					        DOC_SETUP LS,
					        EVALS
					    WHERE
					            A.QUESTION_TYPE_CD = 1
					        AND
					            UA.USER_FORM_CD = EV.USER_FORM_ID
					        AND
					            UA.QUESTION_CD = A.QUESTION_CD
					        AND
					            TCQ.DOC_SETUP_ID = EU.DOC_SETUP_ID
					        AND
					            LS.DOC_SETUP_ID = EU.DOC_SETUP_ID
					        {% if DATA.DOC_SETUP_ID is defined %}
					        	AND  ({{macros.listSplit('LS.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
		            		{% endif %}
		            		{% if DATA.USER_ID is defined %}
		            			AND  ({{macros.listSplit('EU.USER_ID',DATA.USER_ID,500)}})
					    	{% endif %}
					        AND EV.IS_DELETED IS NULL
					        AND EV.EVAL_STATUS_ID = 3
					        AND (
					                NVL(
					                    A.MAIN_QUESTION_CD,
					                    A.QUESTION_CD
					                ) = TCQ.QUESTION_ID
					            OR
					                NVL(
					                    A.MAIN_QUESTION_CD,
					                    A.QUESTION_CD
					                ) = TCQ.GROUP_QUESTION_ID
					        ) AND
					            EV.EVALUATEE_USER_ID = EU.USER_ID
					        AND
					            EV.EVALUATION_ID = EVALS.EVALUATION_ID
					        AND EV.EVAL_DETAILS_ID = B.EVAL_DETAILS_ID
						    AND B.START_DT BETWEEN SESS.START_DT AND SESS.END_DT
						    AND
						     1 = (
						          CASE
						              WHEN NOT EXISTS (
						                  SELECT
						                      1
						                  FROM
						                      TES_CONFIG_SESSIONS TCS,
						                      TES_CONFIG TC
						                  WHERE
						                      TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
						                   AND TC.DOC_SETUP_ID= LS.DOC_SETUP_ID

						              ) THEN 1
						              WHEN EXISTS (
						                  SELECT
						                      1
						                  FROM
						                      TES_CONFIG_SESSIONS TCS,
						                      TES_CONFIG TC
						                  WHERE
						                      TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
								    		  AND TC.DOC_SETUP_ID= LS.DOC_SETUP_ID
						                      AND TCS.TRAINING_SESSION_CD = SESS.TRAINING_SESSION_CD
						              ) THEN 1
						              ELSE -1
						          END
						      )
						       AND (D.PROGRAM_CD = C.PROGRAM_CD)
						       AND	B.ACTIVITY_ID = D.ACTIVITY_ID
						       AND EA.ACTIVITY_ID = D.ACTIVITY_ID
						       AND TRUNC(EA.END_DT) <= TRUNC(NVL(T.ACTIVITY_END_DT,EA.END_DT))
						       AND C.TES_CONFIG_ID = T.TES_CONFIG_ID
						       AND T.DOC_SETUP_ID = LS.DOC_SETUP_ID
						       AND
							        (
							            SELECT
							                SUM(SAFE_TO_NUMBER(NVL(
							                    ANSWER_TEXT,
							                    0
							                ) ) )
							            FROM
							                {eval_form_datasource}.USER_FORM_ANSWER
							            WHERE
							                USER_FORM_CD = EV.USER_FORM_ID
							        ) > 0
					    GROUP BY
					        LS.INSTANCE_ID,
					        EU.USER_ID,
					        EV.EVALUATION_ID,
					        LS.DOC_SETUP_ID,
					        EV.EVALUATOR_USER_ID,
					        SESS.TRAINING_SESSION_CD
					) SELECT
					   *
					FROM
					    EVALUATIONS

			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="comparisonSql">
			<cfoutput>
				WITH MAPPING_QUESTIONS AS (
					    SELECT
					       DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM QUESTIONS_MAPPING_VIEW
					),
				  ESCHACTIVITIES AS (
				    SELECT
				        A.PROGRAM_ID AS PROGRAM_CD,
				        B.ACTIVITY_ID
				    FROM
				        {eval_datasource}.ESCHROTATION_VIEW A,
				        {eval_datasource}.ESCH_ACTIVITY B
				    WHERE
				        A.EXTERNAL_ID = B.EXTERNAL_ID
				    UNION
				    SELECT
				        A.EVENT_PROGRAM_ID AS PROGRAM_CD,
				        B.ACTIVITY_ID
				    FROM
				        {eval_datasource}.ESCHEVENT_VIEW A,
				        {eval_datasource}.ESCH_ACTIVITY B
				    WHERE
				        A.EXTERNAL_ID = B.EXTERNAL_ID
				    ),
					EVALUATIONS AS (
					    SELECT
					        LS.INSTANCE_ID,
					        EV.EVALUATION_ID,
					        LS.DOC_SETUP_ID,
					        SESS.TRAINING_SESSION_CD
					    FROM
					        {eval_form_datasource}.EVAL_QUESTION A,
					        {eval_form_datasource}.USER_FORM_ANSWER UA,
					        {eval_datasource}.ESCH_EVALUATION EV,
					        {eval_datasource}.ESCH_EVAL_DETAILS B,
					        {eval_datasource}.ESCH_ACTIVITY EA,
							{pg_datasource}.TRAINING_SESSION SESS,
							ESCHACTIVITIES D,
							TES_CONFIG_PROGRAMS C,
							TES_CONFIG T,
					        MAPPING_QUESTIONS TCQ,
					        DOC_SETUP LS
					    WHERE
					            A.QUESTION_TYPE_CD = 1
					        AND
					            UA.USER_FORM_CD = EV.USER_FORM_ID
					        AND
					            UA.QUESTION_CD = A.QUESTION_CD
					        AND
					            TCQ.DOC_SETUP_ID = LS.DOC_SETUP_ID
					        {% if DATA.DOC_SETUP_ID is defined %}
								AND LS.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		            		{% endif %}

					        AND EV.IS_DELETED IS NULL
					        AND EV.EVAL_STATUS_ID = 3
					        AND (
					                NVL(
					                    A.MAIN_QUESTION_CD,
					                    A.QUESTION_CD
					                ) = TCQ.QUESTION_ID
					            OR
					                NVL(
					                    A.MAIN_QUESTION_CD,
					                    A.QUESTION_CD
					                ) = TCQ.GROUP_QUESTION_ID
					        ) AND

					         EV.EVAL_DETAILS_ID = B.EVAL_DETAILS_ID
						    AND B.START_DT BETWEEN SESS.START_DT AND SESS.END_DT
						    AND
						     1 = (
						          CASE
						              WHEN NOT EXISTS (
						                  SELECT
						                      1
						                  FROM
						                      TES_CONFIG_SESSIONS TCS,
						                      TES_CONFIG TC
						                  WHERE
						                      TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
						                   AND TC.DOC_SETUP_ID= LS.DOC_SETUP_ID

						              ) THEN 1
						              WHEN EXISTS (
						                  SELECT
						                      1
						                  FROM
						                      TES_CONFIG_SESSIONS TCS,
						                      TES_CONFIG TC
						                  WHERE
						                      TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
								    		  AND TC.DOC_SETUP_ID= LS.DOC_SETUP_ID
						                      AND TCS.TRAINING_SESSION_CD = SESS.TRAINING_SESSION_CD
						              ) THEN 1
						              ELSE -1
						          END
						      )
						       AND (D.PROGRAM_CD = C.PROGRAM_CD)
						       AND	B.ACTIVITY_ID = D.ACTIVITY_ID
						       AND	B.ACTIVITY_ID = EA.ACTIVITY_ID
						       AND TRUNC(EA.END_DT) <= TRUNC(NVL(T.ACTIVITY_END_DT,EA.END_DT))
						       AND C.TES_CONFIG_ID = T.TES_CONFIG_ID
						       AND T.DOC_SETUP_ID = LS.DOC_SETUP_ID
					    GROUP BY
					        LS.INSTANCE_ID,
					        EV.EVALUATION_ID,
					        LS.DOC_SETUP_ID,
					        SESS.TRAINING_SESSION_CD
					) SELECT
					   *
					FROM
					    EVALUATIONS

			</cfoutput>
		</cfsavecontent>
		<cfset sqls["RATING_GROUPS_DEF"] = ratingGroupsSql>
		<cfset sqls["QUESTIONS_DEF"] = questionDefinitionsSql>
		<cfset sqls["RATING_SCALE_DEF"]	= ratingScalesSql>
		<cfset sqls["SCORES_DEF"]	= scoresSql>
		<cfset sqls["COMPARISONS_DEF"]	= comparisonsSql>
		<cfset sqls["REPORT_QRY"]	= reportsSql>
		<cfset sqls["ELIGIBLEUSERSQRY"]	= eligibleUsersSql>
		<cfset sqls["UNPUBLISHEDEVALSQRY"]	= unpublishedEvalsSql>
		<cfset sqls["REPORTQUESTIONSQRY"]	= reportQuestionsSql>
		<cfset sqls["DOCSETUPQRY"]	= docsetupSql>
		<cfset sqls["COmparisonQRY"]	= comparisonSql>
		<cfreturn sqls>
	</cffunction>

	<cffunction name="preProcessSql" returntype="any" access="public">

	    <cfargument name="data" type="struct" default="#StructNew()#">

		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset configProviderEntity = queryBuilder.getEntity('TESConfigDataProvider') />
		<cfset reportQuestionsSql = queryBuilder.getQuery(sql=configProviderEntity.getSQL('REPORTQUESTIONSQRY'),datasource=variables.datasource,params=arguments.data)/>

		<!---Step 1 Insert Report Questions --->
		<cfquery name="prePrcesSQl" datasource="#variables.datasource#">
				MERGE INTO REPORT_QUESTIONS D USING (
					#reportQuestionsSql#
				    ) S ON (S.DOC_SETUP_ID=D.DOC_SETUP_ID
				    		AND D.REPORT_QUESTION_CD = S.REPORT_QUESTION_CD
				    		AND D.EVAL_QUESTION_CD = S.EVAL_QUESTION_CD
				    		)
				   WHEN NOT MATCHED THEN
						INSERT (D.doc_setup_id,
								D.report_question_cd,
								D.eval_question_cd
								)
						VALUES (s.doc_setup_id,
								S.report_question_cd,
								S.eval_question_cd
					)
			</cfquery>
		<cfset docSetupSql = queryBuilder.getQuery(sql=configProviderEntity.getSQL('DOCSETUPQRY'),datasource=variables.datasource,params=arguments.data)/>

	  <!---Step 2 Calculate Unpublished Evaluation Population --->
		<cfquery name="deleteSetupSql" datasource="#variables.datasource#">
		  	DELETE FROM DOC_SETUP_UNPUBLISHED_EVALS D WHERE D.PROCESS_ID ='#arguments.data.processID#'
		</cfquery>
		<cfquery name="docSetupSql" datasource="#variables.datasource#">
				MERGE INTO DOC_SETUP_UNPUBLISHED_EVALS D USING (
					#PreservesingleQuotes(docSetupSql)#
				    ) S ON ('#arguments.data.processID#' = D.PROCESS_ID)
				   WHEN NOT MATCHED THEN
						INSERT (D.doc_setup_id,
								D.EVALUATION_ID,
								D.TRAINING_SESSION_CD,
								D.USER_ID,
								D.INSTANCE_ID,
								D.PROCESS_ID
								)
						VALUES (s.doc_setup_id,
								s.EVALUATION_ID,
								S.TRAINING_SESSION_CD,
								s.USER_ID,
								s.INSTANCE_ID,
								'#arguments.data.processID#'
					)
		</cfquery>

	   <!---Step 3 Calculate Setup Eligible Evaluations Population --->

		<cfset comparisonSql = queryBuilder.getQuery(sql=configProviderEntity.getSQL('COmparisonQRY'),datasource=variables.datasource,params=arguments.data)/>

		<cfquery name="deleteEligibleEvalsSql" datasource="#variables.datasource#">
		  	DELETE FROM DOC_SETUP_ELIGIBLE_EVALS D WHERE D.PROCESS_ID ='#arguments.data.processID#'
		</cfquery>

		<cfquery name="prePrcesSQl" datasource="#variables.datasource#">
				MERGE INTO DOC_SETUP_ELIGIBLE_EVALS D USING (
					#PreservesingleQuotes(comparisonSql)#
				    ) S ON ('#arguments.data.processID#' = D.PROCESS_ID)
				   WHEN NOT MATCHED THEN
						INSERT (D.doc_setup_id,
								D.EVALUATION_ID,
								D.TRAINING_SESSION_CD,
								D.PROCESS_ID
								)
						VALUES (s.doc_setup_id,
								s.EVALUATION_ID,
								s.TRAINING_SESSION_CD,
								'#arguments.data.processID#'
					)
		</cfquery>


		<cfreturn true>
	</cffunction>

	<cffunction name="postProcessSql" returntype="any" access="public">
 		<cfargument name="data" type="struct" default="#StructNew()#">



		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />


		<cfquery name="mergeEvalPeriod" datasource="#variables.datasource#">
				MERGE INTO DOC_SNAPSHOT_EVAL_PERIOD D USING (
					SELECT
					    A.DOC_SNAPSHOT_CD,
					    A.DOC_SETUP_ID,
					    (
					        SELECT
					            MIN_START_DT || '/' || MAX_END_DT
					        FROM
					            TABLE ( pljson_table.json_table(
					                d.value,
					                pljson_varray(
					                    '[*].MIN_START_DT',
					                    '[*].MAX_END_DT'
					                ),
					                pljson_varray(
					                    'MIN_START_DT',
					                    'MAX_END_DT'
					                ),
					                table_mode   => 'NESTED'
					            ) )
					    ) EVALUATION_PERIOD
					FROM
					    DOC_SNAPSHOT_BASE A,
					    DOC_SNAPSHOT_DATA D
					WHERE
					        A.DOC_SNAPSHOT_CD = D.DOC_SNAPSHOT_CD
					    AND A.PROCESS_ID = '#arguments.data.processID#'
					    AND
					        D.KEY = 'TOTAL_SCORES'
				    ) S ON (S.DOC_SNAPSHOT_CD = D.DOC_SNAPSHOT_CD)
				   WHEN NOT MATCHED THEN
						INSERT (D.DOC_SNAPSHOT_CD,
								D.DOC_SETUP_ID,
								D.EVALUATION_PERIOD
								)
						VALUES (S.DOC_SNAPSHOT_CD,
								S.DOC_SETUP_ID,
								S.EVALUATION_PERIOD
					)
		</cfquery>

		<cfreturn true>
	</cffunction>

</cfcomponent>