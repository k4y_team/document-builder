<cfcomponent name="TESDataProvider" code="QB_TES_DATA_PROVIDER" displayName="TESDataProvider" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" label="" inject="coldbox:setting:datasource" scope="variables"/>
	<cfproperty name="EVALUATION_LIST"  type="string">
	<cfproperty name="QUESTION_RATINGS_COUNT"  type="string">
	<cfproperty name="QUESTION_SCORES"  type="string">
	<cfproperty name="QUESTION_COMPARISONS"  type="string">
	<cfproperty name="TOTAL_SCORES"  type="string">
	<cfproperty name="COMMENTS"  type="string">
	<cfproperty name="DOCUMENT_DETAILS"  type="string">
	<cfproperty name="QUESTIONS_DEF"  type="string">
    <cfproperty name="RATING_GROUPS_DEF" type="string">
    <cfproperty name="RATING_SCALE_DEF"  type="string">
	<cfproperty name="SCORES_DEF"  type="string">

	<cfscript>

	public TESDataProvider function init() {
		variables.args = arguments;
		super.init();
		return this;
	}


	</cfscript>
	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_QUESTION",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="USER_FORM",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="USER_FORM_ANSWER",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="RATING_SCALE_GROUP",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="RATING_SCALE",OBJECT_DATASOURCE="{eval_form_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="ESCH_EVALUATION",OBJECT_DATASOURCE="{eval_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="UD_USER",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="EVAL_SUPERVISOR_USER",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="DEPARTMENT",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="DIVISION",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="EVAL_SUPERVISOR_APPOINTMENT",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
		]>
		<cfreturn grants>
	</cffunction>

	<cffunction name="getSQLS" access="public" output="true" returntype="struct">
		<cfset var sqls = StructNew()>
		<cfset evalListSql = ''>
		<cfset ratingCountSql = ''>
		<cfset scoresCountSql = ''>
		<cfset totalscoresSql = ''>
		<cfset comparisonsSql = ''>
		<cfset commentsSql 	  = ''>
		<cfset supervisorSql  = ''>
		<cfset questionDefinitionsSql = ''>
		<cfset ratingGroupsSql =''>
		<cfset ratingScalesSql = ''>
		<cfset scoresSql = ''>
		<cfset comparisonsSql = ''>

		<cfsavecontent variable ="ratingScalesSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{rating_cd:1,rating_desc:'Poor',rating_nr:1,rating_value:1,weight:1,rating_group_cd:1},
								  {rating_cd:2,rating_desc:'Fair',rating_nr:2,rating_value:2,weight:2,rating_group_cd:1},
								  {rating_cd:3,rating_desc:'Good',rating_nr:3,rating_value:3,weight:3,rating_group_cd:1},
								  {rating_cd:4,rating_desc:'Satisfactory',rating_nr:4,rating_value:4,weight:4,rating_group_cd:1},
								  {rating_cd:5,rating_desc:'Excellent',rating_nr:5,rating_value:5,weight:5,rating_group_cd:1},
								  {rating_cd:1,rating_desc:'Poor',rating_nr:1,rating_value:1,weight:1,rating_group_cd:2},
								  {rating_cd:2,rating_desc:'Fair',rating_nr:2,rating_value:2,weight:2,rating_group_cd:2},
								  {rating_cd:3,rating_desc:'Good',rating_nr:3,rating_value:3,weight:3,rating_group_cd:2},
								  {rating_cd:4,rating_desc:'Satisfactory',rating_nr:4,rating_value:4,weight:4,rating_group_cd:2},
								  {rating_cd:5,rating_desc:'Excellent',rating_nr:5,rating_value:5,weight:5,rating_group_cd:2},
								  {rating_cd:6,rating_desc:'Very Good',rating_nr:6,rating_value:6,weight:6,rating_group_cd:2},
								  {rating_cd:5,rating_desc:'n/A',rating_nr:6,rating_value:6,weight:6,rating_group_cd:2}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT DISTINCT
						    D.RATING_CD,
						    D.RATING_DESC,
						    D.RATING_NR,
						    D.RATING_VALUE,
						    D.WEIGHT,
						    D.COLOR,
						    C.RATING_GROUP_CD,
						    C.RATING_GROUP_DESC,
						    EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
						FROM
						    {eval_form_datasource}.EVAL_QUESTION A,
						    {eval_form_datasource}.RATING_SCALE_GROUP C,
						   	(SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            RATING_VALUE,
					            WEIGHT,
					            COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            NULL AS RATING_NR,
					            '' AS RATING_VALUE,
					            0 AS WEIGHT,
					            '' AS COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE_GROUP) d,
					    	TES_CONFIG TC,
							DOC_SETUP_ELIGIBLE_USERS EU
						WHERE
						        A.QUESTION_TYPE_CD = 1
						    AND
						        A.EVAL_FORM_CD = TC.EVAL_FORM_CD
						    AND
						        A.RATING_GROUP_CD = C.RATING_GROUP_CD
						    AND
						        C.RATING_GROUP_CD = D.RATING_GROUP_CD
						    AND EU.DOC_SETUP_ID = TC.DOC_SETUP_ID
						  	{% if DATA.DOC_SETUP_ID is defined %}
								AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    	{% endif %}
						   ORDER BY  RATING_NR ASC,RATING_GROUP_CD ASC,D.WEIGHT ASC
			 {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="scoresSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{score_type_id:1,score_type_desc:'Mean Value',score_type_code:'MEAN_VALUE'},
								  {score_type_id:2,score_type_desc:'Median Value',score_type_code:'MEDIAN_VALUE'},
								  {score_type_id:3,score_type_desc:'Standard Deviation Value',score_type_code:'SDV'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
					    SC.SCORE_TYPE_ID,
					    SC.SCORE_TYPE_DESC,
					    SC.SCORE_TYPE_CODE,
					    EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
					FROM
					    TES_SCORE SC,
					    TES_CONFIG_SCORES TCS,
					    TES_CONFIG TC,
						DOC_SETUP_ELIGIBLE_USERS EU
					WHERE
					        SC.SCORE_TYPE_ID = TCS.SCORE_TYPE_ID
					    AND
					        TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
					    AND EU.DOC_SETUP_ID = TC.DOC_SETUP_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					    order by SCORE_TYPE_ID ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="comparisonsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{comparison_type_id:1,comparison_type_desc:'All Teachers',comparison_type_code:'ALL_TEACHERS'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
					    TC.COMPARISON_TYPE_ID,
					    TC.COMPARISON_TYPE_DESC,
					    TC.COMPARISON_TYPE_CODE,
					    EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
					FROM
					    TES_COMPARISON TC,
					    TES_CONFIG_COMPARISONS TCC,
					    TES_CONFIG TC,
						DOC_SETUP_ELIGIBLE_USERS EU
					WHERE
					       TC.COMPARISON_TYPE_ID =TCC.COMPARISON_TYPE_ID
					    AND
					        TC.TES_CONFIG_ID = TCC.TES_CONFIG_ID
					    AND EU.DOC_SETUP_ID = TC.DOC_SETUP_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND	TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
				{% endif %}
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable ="ratingGroupsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
						{% set data =[{rating_group_cd:1,rating_group_desc:'Rating Descriptin'},{rating_group_cd:2,rating_group_desc:'Rating Description'}]%}
						{{macros.SQLFromArray(data)}}
				{%else %}
						SELECT DISTINCT
					    C.RATING_GROUP_CD,
					    C.RATING_GROUP_DESC,
					    EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
					FROM
					    {eval_form_datasource}.EVAL_QUESTION A,
					    {eval_form_datasource}.RATING_SCALE_GROUP C,
					    (SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            RATING_VALUE,
					            WEIGHT,
					            COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            0 AS RATING_NR,
					            '0' AS RATING_VALUE,
					            0 AS WEIGHT,
					            '' AS COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE_GROUP) d,
					    TES_CONFIG TC,
					    QUESTIONS_MAPPING_VIEW TCQ,
						DOC_SETUP_ELIGIBLE_USERS EU
					WHERE
					        A.QUESTION_TYPE_CD = 1
					    AND
					        A.RATING_GROUP_CD = C.RATING_GROUP_CD
					    AND
					        C.RATING_GROUP_CD = D.RATING_GROUP_CD
					    AND TC.EVAL_FORM_CD = A.EVAL_FORM_CD
					    AND TC.TES_CONFIG_ID = TCQ.TES_CONFIG_ID
					    AND A.QUESTION_CD = TCQ.GROUP_QUESTION_ID
					    AND EU.DOC_SETUP_ID = TCQ.DOC_SETUP_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					   ORDER BY  RATING_GROUP_CD ASC
				 {% endif %}
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable ="questionDefinitionsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,question_desc:'Overall Teaching Effectiveness: Overall for this clinical experience, what is your opinion of the effectiveness of the instructor?',RATING_GROUP_CD:1},
								  {question_cd:2,question_desc:'Evaluation (if applicable): A variety of observation methods were utilized including case discussion and direct observation.',RATING_GROUP_CD:2}
								  								  ]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT DISTINCT
					    a.question_cd,
					    REPLACE(a.QUESTION_DESC,chr(34),chr(39)) as QUESTION_DESC,
					    A.RATING_GROUP_CD,
					    A.QUESTION_NR,
					    G.GROUP_NR,
					    EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
					FROM
					    {eval_form_datasource}.eval_question a,
					    {eval_form_datasource}.eval_group g,
						QUESTIONS_MAPPING_VIEW TCQ,
						DOC_SETUP_ELIGIBLE_USERS EU
					WHERE
					        a.question_type_cd = 1
					    AND A.GROUP_CD = G.GROUP_CD
					    AND EU.DOC_SETUP_ID = TCQ.DOC_SETUP_ID
					    AND A.QUESTION_CD = TCQ.GROUP_QUESTION_ID
					  	{% if DATA.DOC_SETUP_ID is defined %}
							AND TCQ.DOC_SETUP_ID  in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					    {% if DATA.USER_ID is defined %}
							AND  ({{macros.listSplit('EU.USER_ID',DATA.USER_ID,500)}})
					    {% endif %}
					    ORDER BY G.GROUP_NR ASC,A.QUESTION_NR ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable ="evalListSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,rating_cd:1,weight:1,group_count:3},
								  {question_cd:1,rating_cd:2,weight:2,group_count:0},
								  {question_cd:1,rating_cd:3,weight:3,group_count:2},
								  {question_cd:1,rating_cd:4,weight:4,group_count:1},
								  {question_cd:1,rating_cd:5,weight:5,group_count:0},
								  {question_cd:2,rating_cd:1,weight:1,group_count:0},
								  {question_cd:2,rating_cd:2,weight:2,group_count:1},
								  {question_cd:2,rating_cd:3,weight:3,group_count:2},
								  {question_cd:2,rating_cd:4,weight:4,group_count:3},
								  {question_cd:2,rating_cd:5,weight:5,group_count:2}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH USED_EVALUATIONS AS (
					 SELECT
						    A.DOC_SNAPSHOT_CD,
						    A.USER_ID,
						    A.EVALUATION_ID AS EVALUATION_ID
						FROM
						    DOC_SNAPSHOT_EVALUATIONS A,
						    DOC_SNAPSHOT_BASE B,
						    DOC_INSTANCE C
						WHERE
						        A.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
						    AND
						        B.INSTANCE_ID = C.INSTANCE_ID
						    AND B.INSTANCE_ID = 5

				),
				MAPPING_QUESTIONS AS (
					    SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM QUESTIONS_MAPPING_VIEW
							WHERE 1=1
								{% if DATA.DOC_SETUP_ID is defined %}
								    AND DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                		{% endif %}
					),
				EVALUATIONS AS (
				   SELECT
				        EV.EVALUATION_ID,
                        EV.EVALUATEE_USER_ID ||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS DOC_SNAPSHOT_CD
				    FROM
				        {eval_form_datasource}.EVAL_QUESTION A,
				        {eval_form_datasource}.USER_FORM B,
				        {eval_form_datasource}.USER_FORM_ANSWER UA,
				        {eval_form_datasource}.RATING_SCALE_GROUP C,
				        {eval_form_datasource}.RATING_SCALE D,
				        {eval_datasource}.ESCH_EVALUATION EV,
				        (SELECT
                			G.EVALUATION_ID,
							G.DOC_SETUP_ID,
							G.USER_ID
				            FROM
				          		DOC_SETUP_UNPUBLISHED_EVALS G
				          	WHERE 1=1
				          		AND G.PROCESS_ID = '{{DATA.PROCESSID}}'
	                      	{% if DATA.DOC_SETUP_ID is defined %}
	                      		AND  ({{macros.listSplit('G.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
             				{% endif %}
						    {% if DATA.USER_ID is defined %}
						    	AND  ({{macros.listSplit('G.USER_ID',DATA.USER_ID,500)}})
					    	{% endif %}
				             AND NOT EXISTS (SELECT 1 FROM USED_EVALUATIONS WHERE EVALUATION_ID = G.EVALUATION_ID )
				        ) EVALS,
				        MAPPING_QUESTIONS TCQ,
				        DOC_SETUP_ELIGIBLE_USERS EU,
				        TES_CONFIG TC,
        				USER_UNPUBLISHED_EVALUATIONS UUE
				    WHERE
				            A.QUESTION_TYPE_CD = 1
				        AND
				            A.EVAL_FORM_CD = B.EVAL_FORM_CD
				        AND
				            UA.USER_FORM_CD = B.USER_FORM_CD
				        AND
				            UA.QUESTION_CD = A.QUESTION_CD
				        AND
				            A.RATING_GROUP_CD = C.RATING_GROUP_CD
				        AND
				            C.RATING_GROUP_CD = D.RATING_GROUP_CD
				        AND B.USER_FORM_CD  =EV.USER_FORM_ID
					    AND (
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID
						        OR
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID
						    )

					    AND EV.EVALUATEE_USER_ID = EU.USER_ID
					    AND EV.EVALUATION_ID = EVALS.EVALUATION_ID
					    AND EU.DOC_SETUP_ID =EVALS.DOC_SETUP_ID
					    AND EU.USER_ID =EVALS.USER_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
					    	AND  ({{macros.listSplit('EU.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
		                {% endif %}
					    {% if DATA.USER_ID is defined %}
					    	AND  ({{macros.listSplit('EU.USER_ID',DATA.USER_ID,500)}})
					    {% endif %}
					    AND EV.EVALUATION_ID not in (select distinct evaluation_id from used_evaluations)
					    AND EU.USER_ID = UUE.USER_ID
					    AND EU.DOC_SETUP_ID = UUE.DOC_SETUP_ID
       					AND TC.DOC_SETUP_ID = EU.DOC_SETUP_ID
       					AND TC.MIN_EVALS <=UUE.DISTINCT_EVALUATORS
				    ORDER BY A.QUESTION_DESC
				)
				SELECT
					 evaluation_id,
                     doc_snapshot_cd
				FROM EVALUATIONS
					GROUP BY
						evaluation_id,
                    	doc_snapshot_cd
				   ORDER BY  evaluation_id
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="ratingCountSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,rating_cd:1,weight:1,group_count:3},
								  {question_cd:1,rating_cd:2,weight:2,group_count:0},
								  {question_cd:1,rating_cd:3,weight:3,group_count:2},
								  {question_cd:1,rating_cd:4,weight:4,group_count:1},
								  {question_cd:1,rating_cd:5,weight:5,group_count:5},
								  {question_cd:2,rating_cd:1,weight:1,group_count:0},
								  {question_cd:2,rating_cd:2,weight:2,group_count:1},
								  {question_cd:2,rating_cd:3,weight:3,group_count:2},
								  {question_cd:2,rating_cd:4,weight:4,group_count:3},
								  {question_cd:2,rating_cd:5,weight:5,group_count:2}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH EVALUATIONS AS (
					Select doc_setup_id,doc_snapshot_cd,evaluation_id from (
					select
					doc_snapshot_cd,
					doc_setup_id,
					REGEXP_SUBSTR(EVALUATION_LIST,'[^,]+',1,EVALUATIONS.COLUMN_VALUE) AS EVALUATION_ID FROM
					(select
					a.doc_snapshot_cd,
					a.doc_Setup_id,
					(select string_agg(evaluation_id) from table(pljson_table.json_table(
					                          d.value,
					                          pljson_varray('[*].EVALUATION_ID'),
					                          pljson_varray('EVALUATION_ID'),
					                          table_mode => 'nested'
					                        )) ) evaluation_list
					from
					doc_snapshot_base a,
					doc_snapshot_data d
					where 1 = 1
					AND  ({{macros.listSplit('A.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					and a.process_id='{{DATA.PROCESSID}}'
					AND D.KEY = 'EVALUATION_LIST'
					and a.doc_snapshot_cd=d.doc_snapshot_cd),
				    TABLE ( CAST(MULTISET(SELECT LEVEL FROM DUAL CONNECT BY LEVEL <= LENGTH(REGEXP_REPLACE(EVALUATION_LIST,'[^,]+') ) + 1) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
					)
					GROUP BY evaluation_id,doc_snapshot_cd,doc_setup_id
					),
					MAPPING_QUESTIONS AS (
					     SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM QUESTIONS_MAPPING_VIEW
								WHERE
								1 = 1
									AND  ({{macros.listSplit('DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					),
					 QUESTIONS AS (
				    SELECT
				        tcq.group_question_id as QUESTION_CD,
				        D.RATING_CD,
				        D.RATING_NR,
                        D.WEIGHT,
				        CASE WHEN UA.ANSWER_TEXT like D.RATING_CD THEN 1
				        ELSE 0 END AS ANSWERED,
				        ev.evaluatee_user_id ||'_'||EVL.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
				    FROM
				        {eval_form_datasource}.EVAL_QUESTION A,
				        {eval_form_datasource}.USER_FORM B,
				        {eval_form_datasource}.USER_FORM_ANSWER UA,
				        {eval_form_datasource}.RATING_SCALE_GROUP C,
				        (SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            RATING_VALUE,
					            WEIGHT,
					            COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            NULL AS RATING_NR,
					            '0' AS RATING_VALUE,
					            0 AS WEIGHT,
					            '' AS COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE_GROUP) d,
				        {eval_datasource}.esch_evaluation EV,
				        EVALUATIONS EVL,
    					mapping_questions tcq
				        WHERE
				            A.QUESTION_TYPE_CD = 1
				        AND
				            A.EVAL_FORM_CD = B.EVAL_FORM_CD
				        AND
				            UA.USER_FORM_CD = B.USER_FORM_CD
				        AND
				            UA.QUESTION_CD = A.QUESTION_CD
				        AND
				            A.RATING_GROUP_CD = C.RATING_GROUP_CD
				        AND
				            C.RATING_GROUP_CD = D.RATING_GROUP_CD
				        AND B.USER_FORM_CD    = EV.USER_FORM_ID
				        AND EV.EVALUATION_ID = EVL.EVALUATION_ID
				        {% if DATA.USER_ID is defined %}
							AND  ({{macros.listSplit('EV.EVALUATEE_USER_ID',DATA.USER_ID,500)}})
					    {% endif %}
				        AND TCQ.DOC_SETUP_ID =EVL.DOC_SETUP_ID
						AND (
						           NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID
						        OR
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID
						    )
				    ORDER BY A.QUESTION_DESC
				)
				SELECT
					QUESTION_CD,
					RATING_CD,
					RATING_NR,
                    WEIGHT,
					SUM(ANSWERED) AS GROUP_COUNT,
					doc_snapshot_cd
				FROM QUESTIONS
					GROUP BY
						QUESTION_CD,
						RATING_CD,
						RATING_NR,
                        WEIGHT,
						doc_snapshot_cd
				   ORDER BY  QUESTION_CD ASC,RATING_NR ASC,WEIGHT ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="scoresCountSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,MEAN_VALUE:3.12,MEDIAN_VALUE:3,SDV:0.86},
								  {question_cd:2,MEAN_VALUE:1,MEDIAN_VALUE:1,SDV:3}
								  ]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH EVALUATIONS AS (
					Select doc_snapshot_cd,doc_setup_id,evaluation_id from (
					select
					doc_snapshot_cd,
					doc_setup_id,
					REGEXP_SUBSTR(EVALUATION_LIST,'[^,]+',1,EVALUATIONS.COLUMN_VALUE) AS EVALUATION_ID FROM
					(select
					a.doc_snapshot_cd,
					a.doc_setup_id,
					(select string_agg(evaluation_id) from table(pljson_table.json_table(
					                          d.value,
					                          pljson_varray('[*].EVALUATION_ID'),
					                          pljson_varray('EVALUATION_ID'),
					                          table_mode => 'nested'
					                        )) ) evaluation_list
					from
					doc_snapshot_base a,
					doc_snapshot_data d
					where
					1 = 1
					AND  ({{macros.listSplit('A.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					and a.process_id='{{DATA.PROCESSID}}'
					AND D.KEY = 'EVALUATION_LIST'
					and a.doc_snapshot_cd=d.doc_snapshot_cd),
				    TABLE ( CAST(MULTISET(SELECT LEVEL FROM DUAL CONNECT BY LEVEL <= LENGTH(REGEXP_REPLACE(EVALUATION_LIST,'[^,]+') ) + 1) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
					)
					GROUP BY evaluation_id,doc_snapshot_cd,doc_setup_id
					),
					MAPPING_QUESTIONS AS (
					     SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM QUESTIONS_MAPPING_VIEW
								WHERE DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					),
				QUESTIONS AS (
				    SELECT
				        TCQ.GROUP_QUESTION_ID AS QUESTION_CD,
				        D.RATING_CD,
					    SAFE_TO_NUMBER(D.RATING_VALUE) AS RATING_VALUE,
				        D.WEIGHT,
				            CASE
				                WHEN UA.ANSWER_TEXT like D.RATING_CD THEN 1
				                ELSE 0
				            END
				        AS ANSWERED,
				        ev.evaluatee_user_id ||'_'||EVL.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
				    FROM
				        {eval_form_datasource}.EVAL_QUESTION A,
				        {eval_form_datasource}.USER_FORM B,
				        {eval_form_datasource}.USER_FORM_ANSWER UA,
				        {eval_form_datasource}.RATING_SCALE_GROUP C,
				        (SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            SAFE_TO_NUMBER(RATING_VALUE) AS RATING_VALUE,
					            WEIGHT,
					            COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            0 AS RATING_NR,
					            0 AS RATING_VALUE,
					            0 AS WEIGHT,
					            '' AS COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE_GROUP) d,
				        {eval_datasource}.esch_evaluation EV,
				        MAPPING_QUESTIONS TCQ,
				        EVALUATIONS EVL
				   WHERE
				            A.QUESTION_TYPE_CD = 1
				        AND
				            A.EVAL_FORM_CD = B.EVAL_FORM_CD
				        AND
				            UA.USER_FORM_CD = B.USER_FORM_CD
				        AND
				            UA.QUESTION_CD = A.QUESTION_CD
				        AND
				            A.RATING_GROUP_CD = C.RATING_GROUP_CD
				        AND
				            C.RATING_GROUP_CD = D.RATING_GROUP_CD
				        and rating_value > 0
				        AND B.USER_FORM_CD  =EV.USER_FORM_ID
				        AND EV.EVALUATION_ID = EVL.EVALUATION_ID
				        {% if DATA.USER_ID is defined %}
				 			AND  ({{macros.listSplit('EV.EVALUATEE_USER_ID',DATA.USER_ID,500)}})
						{% endif %}
						{% if DATA.DOC_SETUP_ID is defined %}
							AND  ({{macros.listSplit('EVL.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
						{% endif %}
				  		AND (NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID OR NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID)
				    ORDER BY A.QUESTION_DESC
				) SELECT
				    QUESTION_CD,
				    doc_snapshot_cd,
				    TO_CHAR(AVG(TO_NUMBER(RATING_VALUE)), '0.90') AS MEAN_VALUE,
				    MEDIAN(TO_NUMBER(RATING_VALUE)) AS MEDIAN_VALUE,
				    TO_CHAR(STDDEV(TO_NUMBER(RATING_VALUE)), '0.90') AS SDV
				FROM
				    QUESTIONS
				WHERE
				    ANSWERED = 1
				    AND safe_to_number(RATING_VALUE)>0
				GROUP BY
				doc_snapshot_cd,
				QUESTION_CD
			  {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="totalscoresSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{NR_EVALS:7,NR_STUDENTS:4,TES_SCORE:3.86,MIN_START_DT:'05-Mar-17',MAX_END_DT:'05-Jun-17',MIN_COMPLETION_DT:'05-Apr-17',MAX_COMPLETION_DT:'05-May-17'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH EVALUATIONS AS (
					Select doc_snapshot_cd,evaluation_id,doc_setup_id from (
					select
					doc_snapshot_cd,
					doc_setup_id,
					REGEXP_SUBSTR(EVALUATION_LIST,'[^,]+',1,EVALUATIONS.COLUMN_VALUE) AS EVALUATION_ID FROM
					(select
					a.doc_snapshot_cd,
					a.doc_setup_id,
					(select string_agg(evaluation_id) from table(pljson_table.json_table(
					                          d.value,
					                          pljson_varray('[*].EVALUATION_ID'),
					                          pljson_varray('EVALUATION_ID'),
					                          table_mode => 'nested'
					                        )) ) evaluation_list
					from
					doc_snapshot_base a,
					doc_snapshot_data d
					where a.doc_setup_id in ({{DATA.DOC_SETUP_ID}})
					and a.process_id='{{DATA.PROCESSID}}'
					and D.KEY='EVALUATION_LIST'
					and a.doc_snapshot_cd=d.doc_snapshot_cd),
				    TABLE ( CAST(MULTISET(SELECT LEVEL FROM DUAL CONNECT BY LEVEL <= LENGTH(REGEXP_REPLACE(EVALUATION_LIST,'[^,]+') ) + 1) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
					)
					GROUP BY evaluation_id,doc_snapshot_cd,doc_setup_id
					)
					,
					mapping_questions AS (
					      SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM QUESTIONS_MAPPING_VIEW
								WHERE
								1 = 1
								AND  ({{macros.listSplit('DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					),
				QUESTIONS AS (
				    SELECT
				        UA.USER_FORM_CD,
				        tcq.group_question_id as QUESTION_CD,
				        D.RATING_CD,
					    SAFE_TO_NUMBER(D.RATING_VALUE) AS RATING_VALUE,
				        EV.EVALUATOR_USER_ID as TRAINEE_ID,
				        EV.COMPLETION_DT,
        				ED.END_DT,
						ED.START_DT,
				        D.WEIGHT,
				            CASE
				                WHEN UA.ANSWER_TEXT like D.RATING_CD THEN 1
				                ELSE 0
				            END
				        AS ANSWERED,
				        ev.evaluatee_user_id ||'_'||EVL.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
				    FROM
				        {eval_form_datasource}.EVAL_QUESTION A,
				        {eval_form_datasource}.USER_FORM B,
				        {eval_form_datasource}.USER_FORM_ANSWER UA,
				        {eval_form_datasource}.RATING_SCALE_GROUP C,
				        (SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            SAFE_TO_NUMBER(RATING_VALUE) AS RATING_VALUE,
					            WEIGHT,
					            COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            0 AS RATING_NR,
					            0 AS RATING_VALUE,
					            0 AS WEIGHT,
					            '' AS COLOR
					        FROM
					            {eval_form_datasource}.RATING_SCALE_GROUP) d,
				        {eval_datasource}.esch_evaluation EV,
				        {eval_datasource}.ESCH_EVAL_DETAILS ed,
				        EVALUATIONS EVL,
    					mapping_questions tcq
				    WHERE
				            A.QUESTION_TYPE_CD = 1
				        AND
				            A.EVAL_FORM_CD = B.EVAL_FORM_CD
				        AND
				            UA.USER_FORM_CD = B.USER_FORM_CD
				        AND
				            UA.QUESTION_CD = A.QUESTION_CD
				        AND
				            A.RATING_GROUP_CD = C.RATING_GROUP_CD
				        AND
				            C.RATING_GROUP_CD = D.RATING_GROUP_CD
				        AND B.USER_FORM_CD  =EV.USER_FORM_ID
				        AND (
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = tcq.question_id
						        OR
						           NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = tcq.group_question_id
						    )
				        AND EV.EVALUATION_ID = EVL.EVALUATION_ID
				        AND EV.EVAL_DETAILS_ID = ED.EVAL_DETAILS_ID
					ORDER BY A.QUESTION_DESC
				) SELECT
				    COUNT( DISTINCT USER_FORM_CD) AS NR_EVALS,
				    COUNT( DISTINCT TRAINEE_ID) AS NR_STUDENTS,
				    TO_CHAR(MIN(START_DT),'dd-Mon-yyyy') AS MIN_START_DT,
				    TO_CHAR(MAX(END_DT),'dd-Mon-yyyy') AS MAX_END_DT,
				    TO_CHAR(MIN(COMPLETION_DT),'dd-Mon-yyyy') AS MIN_COMPLETION_DT,
 					TO_CHAR(MAX(COMPLETION_DT),'dd-Mon-yyyy') AS MAX_COMPLETION_DT,
				   	TO_CHAR(AVG(NULLIF(TO_NUMBER(RATING_VALUE),0)), '0.90') AS TES_SCORE,
				   	DOC_SNAPSHOT_CD
				FROM
				    QUESTIONS
				WHERE ANSWERED=1
				    AND safe_to_number(RATING_VALUE)>0
				group by
				DOC_SNAPSHOT_CD

				{% endif %}
		</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="comparisonsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,ALL_TEACHERS_MEAN_VALUE:3.12,ALL_TEACHERS_MEDIAN_VALUE:3,ALL_TEACHERS_SDV:0.86},
								  {question_cd:2,ALL_TEACHERS_MEAN_VALUE:1,ALL_TEACHERS_MEDIAN_VALUE:1,ALL_TEACHERS_SDV:3}
								  ]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH
				 ELIGIBLE_USERS AS (
				 	SELECT
				 		EU.DOC_SETUP_ID,
				 		EU.USER_ID
				 	FROM
				 		DOC_SETUP_ELIGIBLE_USERS EU
				 	WHERE 1=1
				 	{% if DATA.USER_ID is defined %}
				 		AND  ({{macros.listSplit('EU.USER_ID',DATA.USER_ID,500)}})
					{% endif %}
					{% if DATA.DOC_SETUP_ID is defined %}
						AND  ({{macros.listSplit('EU.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					{% endif %}
					)
				 ,QUESTIONS AS (
				    SELECT
				        TCQ.REPORT_QUESTION_CD AS QUESTION_CD,
				        D.RATING_CD,
					    SAFE_TO_NUMBER(D.RATING_VALUE) AS RATING_VALUE,
				        D.WEIGHT,
				            CASE
				                WHEN UA.ANSWER_TEXT like D.RATING_CD THEN 1
				                ELSE 0
				            END
				        AS ANSWERED,
				        EU.USER_ID,
               			EU.DOC_SETUP_ID
				    FROM
				        {eval_form_datasource}.EVAL_QUESTION A,
				        {eval_form_datasource}.USER_FORM B,
				        {eval_form_datasource}.USER_FORM_ANSWER UA,
				        {eval_form_datasource}.RATING_SCALE_GROUP C,
				        {eval_form_datasource}.RATING_SCALE D,
				        {eval_datasource}.esch_evaluation EV,
				        REPORT_QUESTIONS TCQ,
				        DOC_SETUP_ELIGIBLE_EVALS EVALS,
				        ELIGIBLE_USERS EU
				   WHERE
				   		A.QUESTION_TYPE_CD = 1
                	AND	A.EVAL_FORM_CD = B.EVAL_FORM_CD
                	AND	UA.USER_FORM_CD = B.USER_FORM_CD
                	AND	UA.QUESTION_CD = A.QUESTION_CD
                	AND	A.RATING_GROUP_CD = C.RATING_GROUP_CD
                	AND	C.RATING_GROUP_CD = D.RATING_GROUP_CD
                	AND	B.USER_FORM_CD = EV.USER_FORM_ID
               		AND A.QUESTION_CD = TCQ.EVAL_QUESTION_CD
				    AND EVALS.EVALUATION_ID = EV.EVALUATION_ID
				    AND EVALS.PROCESS_ID = '{{DATA.PROCESSID}}'
				    AND EVALS.TRAINING_SESSION_CD IN (select U.TRAINING_SESSION_CD from DOC_SETUP_UNPUBLISHED_EVALS U WHERE U.PROCESS_ID = '{{DATA.PROCESSID}}' AND U.user_id = eU.user_id)
					{% if DATA.DOC_SETUP_ID is defined %}
						AND  ({{macros.listSplit('TCQ.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					{% endif %}
				    ORDER BY A.QUESTION_DESC
					) SELECT
				    QUESTION_CD,
				    TO_CHAR(AVG(TO_NUMBER(RATING_VALUE)), '0.90') AS ALL_TEACHERS_MEAN_VALUE,
				    MEDIAN(TO_NUMBER(RATING_VALUE)) AS ALL_TEACHERS_MEDIAN_VALUE,
				    TO_CHAR(STDDEV(TO_NUMBER(RATING_VALUE)), '0.90') AS ALL_TEACHERS_SDV,
				    user_id||'_'||DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
				FROM
				    QUESTIONS
				WHERE
				    ANSWERED = 1
				    AND safe_to_number(RATING_VALUE)>0
				GROUP BY
				QUESTION_CD,
				USER_ID,
				DOC_SETUP_ID
				{% endif %}
		</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="commentsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,ANSWER_COMMENTS:' Nice overview with good interactivity and great discussion around writing orders clearly and avoiding mistakes. ',USER_ANSWER_CD:3},
								  {question_cd:2,ANSWER_COMMENTS:'Enjoyed working with Dr. Fudge.He had a wealth of knowledge and was also responsive to my questions.',USER_ANSWER_CD:3}
								  ]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH EVALUATIONS AS (
					Select doc_setup_id,doc_snapshot_cd,evaluation_id from (
					select
					doc_snapshot_cd,
					doc_setup_id,
					REGEXP_SUBSTR(EVALUATION_LIST,'[^,]+',1,EVALUATIONS.COLUMN_VALUE) AS EVALUATION_ID FROM
					(select
					a.doc_snapshot_cd,
					a.doc_Setup_id,
					(select string_agg(evaluation_id) from table(pljson_table.json_table(
					                          d.value,
					                          pljson_varray('[*].EVALUATION_ID'),
					                          pljson_varray('EVALUATION_ID'),
					                          table_mode => 'nested'
					                        )) ) evaluation_list
					from
					doc_snapshot_base a,
					doc_snapshot_data d
					where
					1 = 1
					AND  ({{macros.listSplit('A.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					and a.process_id='{{DATA.PROCESSID}}'
					AND D.KEY               = 'EVALUATION_LIST'
					and a.doc_snapshot_cd=d.doc_snapshot_cd),
				    TABLE ( CAST(MULTISET(SELECT LEVEL FROM DUAL CONNECT BY LEVEL <= LENGTH(REGEXP_REPLACE(EVALUATION_LIST,'[^,]+') ) + 1) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
					)
					GROUP BY evaluation_id,doc_snapshot_cd,doc_setup_id
					),
				mapping_questions AS (
				     SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM QUESTIONS_MAPPING_VIEW
								WHERE DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
				) , QUESTIONS AS (
				     SELECT
				        UA.USER_FORM_CD,
				        A.QUESTION_CD,
				        A.QUESTION_NR,
				        A.QUESTION_CODE,
						PLJSON_PRINTER.ESCAPESTRING(A.QUESTION_DESC) as QUESTION_DESC,
						PLJSON_PRINTER.ESCAPESTRING(A.QUESTION_DESC_SHORT) as QUESTION_DESC_SHORT,
						clobEscape(UA.ANSWER_TEXT) AS ANSWER_COMMENTS,
				        UA.USER_ANSWER_CD,
				        eU.user_id||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
				    FROM
				        {eval_form_datasource}.EVAL_QUESTION A,
				        {eval_form_datasource}.USER_FORM B,
				        {eval_form_datasource}.USER_FORM_ANSWER UA,
				        {eval_datasource}.esch_evaluation EV,
				        EVALUATIONS EVL,
				        MAPPING_QUESTIONS TCQ,
				        TES_CONFIG TC,
    					DOC_SETUP_ELIGIBLE_USERS EU
				    WHERE
				              A.QUESTION_TYPE_CD = 8
					    AND UA.USER_FORM_CD = B.USER_FORM_CD
					    AND UA.ANSWER_TEXT IS NOT NULL
					    AND B.USER_FORM_CD = EV.USER_FORM_ID
					    AND A.QUESTION_CD = UA.QUESTION_CD
					    AND TC.SHOW_COMMENTS ='Y'
					    AND (NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID OR NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID)
					    AND EU.DOC_SETUP_ID = TC.DOC_SETUP_ID
					    AND EU.DOC_SETUP_ID = TCQ.DOC_SETUP_ID
					    AND EV.EVALUATEE_USER_ID = EU.USER_ID
					    AND EV.EVALUATION_ID = EVL.EVALUATION_ID
					    AND EU.DOC_SETUP_ID = EVL.DOC_SETUP_ID
				  		{% if DATA.DOC_SETUP_ID is defined %}
							AND  ({{macros.listSplit('TC.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					    {% endif %}
				    ORDER BY A.QUESTION_DESC
				) SELECT
				    QUESTION_CD,
				    QUESTION_CODE,
					QUESTION_DESC,
					QUESTION_DESC_SHORT,
				    ANSWER_COMMENTS,
				    USER_ANSWER_CD,
				    doc_snapshot_cd
				FROM
				    QUESTIONS
				ORDER BY QUESTION_NR,QUESTION_DESC,QUESTION_DESC_SHORT,QUESTION_CD ASC
				{% endif %}
		</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="detailsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{USER_ID:1,SUPERVISOR_NAME:'Jhon Doe',DOC_SETUP_NAME:'PG Pediatrics TES - Presenter',APPOINTMENT:'Anesthesia'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
			 		SELECT
				 		EU.DOC_SETUP_ID,
				 		EU.USER_ID,
				 		U.LAST_NAME||', '||U.FIRST_NAME AS SUPERVISOR_NAME,
				 		EU.USER_ID ||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd,
				 		DS.DOC_SETUP_NAME,
				 		D.DEPARTMENT_DESC || DECODE(DIV.DIVISION_DESC,null,'','/'||DIV.DIVISION_DESC) as APPOINTMENT
				 	FROM
				 		DOC_SETUP_ELIGIBLE_USERS EU,
				 		{pg_datasource}.UD_USER U,
				 		{fbx_datasource}.EVAL_SUPERVISOR_USER EUS,
				 		{fbx_datasource}.EVAL_SUPERVISOR_APPOINTMENT AP,
				 		{fbx_datasource}.DEPARTMENT D,
				 		{fbx_datasource}.DIVISION DIV,
				 		DOC_SETUP DS
				 	WHERE 1=1
				 		AND EU.USER_ID = U.USER_ID
				 		AND EU.DOC_SETUP_ID = DS.DOC_SETUP_ID
				 		AND EU.USER_ID = EUS.USER_ID(+)
				 		AND EUS.SUPERVISOR_ID = AP.SUPERVISOR_ID(+)
				 		AND AP.DEPARTMENT_CD = D.DEPARTMENT_CD(+)
				 		AND AP.DIVISION_CD = DIV.DIVISION_CD(+)
				 	{% if DATA.USER_ID is defined %}
						AND  ({{macros.listSplit('EU.USER_ID',DATA.USER_ID,500)}})
					{% endif %}
					{% if DATA.DOC_SETUP_ID is defined %}
						AND  ({{macros.listSplit('EU.DOC_SETUP_ID',DATA.DOC_SETUP_ID,500)}})
					{% endif %}

				{% endif %}
		</cfoutput>
		</cfsavecontent>
		<cfset sqls["EVALUATION_LIST"] = evalListSql>
		<cfset sqls["QUESTION_RATINGS_COUNT"] = ratingCountSql>
		<cfset sqls["QUESTION_SCORES"] = scoresCountSql>
		<cfset sqls["TOTAL_SCORES"] = totalscoresSql>
		<cfset sqls["QUESTION_COMPARISONS"] = comparisonsSql>
		<cfset sqls["COMMENTS"] = commentsSql>
		<cfset sqls["DOCUMENT_DETAILS"] = detailsSql>
		<cfset sqls["QUESTIONS_DEF"] = questionDefinitionsSql>
		<cfset sqls["RATING_GROUPS_DEF"] = ratingGroupsSql>
		<cfset sqls["RATING_SCALE_DEF"]	= ratingScalesSql>
		<cfset sqls["SCORES_DEF"]	= scoresSql>
		<cfset sqls["COMPARISONS_DEF"]	= comparisonsSql>
		<cfreturn sqls>
	</cffunction>

</cfcomponent>