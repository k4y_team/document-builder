<cfcomponent name="TESRenderer" code="QB_TES_RENDERER" displayName="TESRenderer" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" label="" inject="coldbox:setting:datasource" scope="variables"/>

	<cfproperty name="NR_EVALS" type="string">
	<cfproperty name="NR_STUDENTS"  type="string">
	<cfproperty name="TES_SCORE"  type="string">
	<cfproperty name="MIN_COMPLETION_DT" type="string">
	<cfproperty name="MAX_COMPLETION_DT"  type="string">
	<cfproperty name="MIN_START_DT" type="string">
	<cfproperty name="MAX_END_DT"  type="string">
	<cfproperty name="RATING_SCALES"  type="string">
	<cfproperty name="EXTRA_SCORES"  type="string">
	<cfproperty name="COMMENTS" type="string">
	<cfproperty name="SUPERVISOR" type="string">
	<cfproperty name="DOCUMENT_NAME" type="string">
	<cfproperty name="APPOINTMENT" type="string">

	<cfscript>

	public TESRenderer function init() {
		variables.args = arguments;
		super.init();
		return this;
	}

	</cfscript>


	<cffunction name="getTemplates" access="public" returntype="struct" >
		<cfset var result = {} />
		<cfset var ratingScalesHtml = "">
		<cfset var extraScoresHtml = "">
		<cfset var evalsHtml = "">
		<cfset var studentsHtml = "">

		<cfsavecontent variable="ratingScalesHtml">
			<cfoutput>
				<style>
				table.ratingScales td {
  					border: 1px solid black;
					border-collapse: collapse;
				}
				table.ratingScales {
  					border: 1px solid black;
					border-collapse: collapse;
				}
				table.ratingScales th {
    				background-color: ##e0e0e0;
					font-weight: bold;
					border: 1px solid black;
				}

				.comments {
    				background-color:##e0e0e0;
					border-left: 3px solid ;
					padding:5px;
				}
				.instructions {
					font-style: italic;
    				font-weight: bold;
				}

				</style>
				<!---Display Header--->
				{% macro createHeader(ratingScales,scoreDefinitions,comparisonDefitions,ratingGroup) %}
				{% import _self as macros %}
					<tr class="dontsplit">
            			<th style="min-width:200px;" rowspan="2">Questions</th>
            			<th style="width:30%" colspan="{{ratingScales|length}}">Rating Scales</th>
						{% for score_def in scoreDefinitions %}
						{% if(comparisonDefitions|length > 0)%}
							<th  colspan="{{comparisonDefitions|length+1}}">
								{{score_def.SCORE_TYPE_DESC }}
							</th>
						{%else%}
							<th >{{score_def.SCORE_TYPE_DESC }}</th>
						{%endif%}
					{% endfor %}
        			</tr>
					<tr class="dontsplit">
						{% if(ratingScales|length > 0)%}
            			{% for rating in ratingScales %}
						<th style="min-width: 30px;padding-top: 5px;">
							{{rating.RATING_DESC }} </br>
							{{rating.RATING_VALUE }}
						</th>
						{% endfor %}
						{%else%}
							<th></th>
						{%endif%}
						{% for score_def in scoreDefinitions %}
							<th style="min-width: 50px;padding-top: 5px;"> This teacher</th>
							<!--- Comparisons--->
							{% if(comparisonDefitions|length > 0)%}
								{% for comparison_def in comparisonDefitions %}
									<th style="width: 50px;padding-top: 5px;">
										{{comparison_def.COMPARISON_TYPE_DESC }}
									</th>
								{% endfor %}
							{%endif%}
						{% endfor %}
					</tr>
				{% endmacro %}


				{% macro displayQuestionRow(question,ratingCounts) %}
				    	<td>{{question.QUESTION_DESC }}</td>
							{% for ratingCount in ratingCounts %}
								{% if(question.QUESTION_CD==ratingCount.QUESTION_CD)%}
									<td style="text-align:center;">{{ ratingCount.GROUP_COUNT }}</td>
								{%	endif%}
							{% endfor %}
				{% endmacro %}
				{% macro displayQuestionExtraScores(question,scoreDefinitions,scores,comparisonDefitions,comparisons) %}
				{% import _self as macros %}
							{% for scores in scores %}
								{% if(question.QUESTION_CD==scores.QUESTION_CD)%}
									{% for score_def in scoreDefinitions %}
										<td style="text-align:center;">{{scores[''~score_def.SCORE_TYPE_CODE]}}</td>
										{{macros.displayComparisons(question,score_def.SCORE_TYPE_CODE,comparisons,comparisonDefitions)}}
									{%endfor%}
								{%	endif%}
							{% endfor %}
				{% endmacro %}

				{% macro displayComparisons(question,scoreDescription,comparisons,comparisonDefitions) %}
					{% for comparison in comparisons%}
						{% if(comparison.QUESTION_CD==question.QUESTION_CD)%}
							{% for comparison_def in comparisonDefitions %}
								<td style="text-align:center;">{{comparison[comparison_def.COMPARISON_TYPE_CODE~'_'~scoreDescription]}}</td>
							{% endfor %}
						{%	endif%}
					{%endfor%}
				{% endmacro %}
				{% for ratingGroup in DATA.RATING_GROUPS_DEF%}
					{% set ratingScales = [] %}
					{% set questions = [] %}
					{% for scores in DATA.RATING_SCALE_DEF%}
						{% if(ratingGroup.RATING_GROUP_CD==scores.RATING_GROUP_CD)%}
 							{% set ratingScales = ratingScales|merge([scores]) %}
						{%endif%}
					{%endfor%}
					{% for question in DATA.QUESTIONS_DEF%}
						{% if(question.RATING_GROUP_CD==ratingGroup.RATING_GROUP_CD)%}
 							{% set questions = questions|merge([question]) %}
						{%endif%}
					{%endfor%}
				<table class="ratingScales" style="width:100%">
					<thead class="dontsplit ">
						{{macros.createHeader(ratingScales,DATA.SCORES_DEF,DATA.COMPARISONS_DEF,ratingGroup)}}
					</thead>
					<tbody>
						{% for question in questions %}
							<tr class="dontsplit">
								{{macros.displayQuestionRow(question,DATA.QUESTION_RATINGS_COUNT)}}
								{{macros.displayQuestionExtraScores(question,DATA.SCORES_DEF,DATA.QUESTION_SCORES,DATA.COMPARISONS_DEF,DATA.QUESTION_COMPARISONS)}}
							</tr>
						{% endfor %}
					</tbody>
				</table>
				{% endfor %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="extraScoresHtml">
			<cfoutput>
				<table class="ratingScales">
					{% for key,questions in DATA.QUESTIONS_DEF %}
							<tr>
								<!---Extra Values --->
								{% for key ,score in DATA.QUESTION_SCORES %}
									{% if(score.QUESTION_CD==questions.QUESTION_CD)%}
										{% for key,score_def in DATA.SCORES_DEF %}
											<td>{{score[''~score_def.SCORE_TYPE_CODE]}}</td>
											<!---Comparisons--->
											{% if(DATA.SHOWCOMAPRISONS)%}
												{% for key ,comparison in DATA.QUESTION_COMPARISONS %}
													{% if(comparison.QUESTION_CD==questions.QUESTION_CD)%}
														{% for key,comparison_def in DATA.COMPARISONS_DEF %}
															<td>{{comparison[comparison_def.COMPARISON_TYPE_CODE~'_'~score_def.SCORE_TYPE_CODE]}}</td>
														{% endfor %}
													{%	endif%}
												{%endfor%}
											{%endif%}
										{% endfor %}
									{%	endif%}
								{% endfor %}
							</tr>
					{% endfor %}
				</table>
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="evalsHtml">
			<cfoutput>
				{% for value in DATA.TOTAL_SCORES %}
				 		{{value.NR_EVALS}}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="supervisorHtml">
			<cfoutput>
				{% set user_id = "" %}
				{% for value in DATA.DOCUMENT_DETAILS%}
					{% if(user_id!= value.USER_ID)%}
						{{value.SUPERVISOR_NAME}}
					{%endif%}
					{% set user_id = value.USER_ID %}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="appointmentHtml">
			<cfoutput>
				{% for value in DATA.DOCUMENT_DETAILS%}
				 		{{value.APPOINTMENT}}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="documentHtml">
			<cfoutput>
				{% set setup_name = "" %}
				{% for value in DATA.DOCUMENT_DETAILS%}
				 	{% if(setup_name!= value.DOC_SETUP_NAME)%}
				 		{{value.DOC_SETUP_NAME}}
					{%endif%}
					{% set setup_name = value.DOC_SETUP_NAME %}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="studentsHtml">
			<cfoutput>
				{% for value in DATA.TOTAL_SCORES %}
				 		{{value.NR_STUDENTS}}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="commentsHtml">
			<cfoutput>
				{% macro displayComments (comments,comments_changes,readonly,docSnapshotCd) %}
					{%if not readonly %}
						<p class="instructions dontend">
						* Please uncheck the comments you want to be removed from the TES.
						</p>
					{%endif%}
							<table>
							<tbody>
				{% set ratingScales =[]%}
				{% set comment_desc = "" %}
				{% for comment in comments%}
					{% if(comment_desc!= comment.QUESTION_DESC)%}
						<tr>
							<td>
						<h3 >{{comment.QUESTION_DESC}}</h3>
							</td>
						</tr>
						{% set comment_desc = comment.QUESTION_DESC %}
					{%endif%}
				{%set hidden = false %}
						{% for changed_comment in comments_changes%}
							{% if(comment.USER_ANSWER_CD==changed_comment.USER_ANSWER_CD)%}
			 					{%set hidden = true %}
							{%endif%}
						{% endfor %}
					{%if readonly %}
						{% if not hidden %}
				 			<tr>
								<td class="comments">
									{{comment.ANSWER_COMMENTS}}
								</td>
							</tr>
							<tr>
								<td >
									</br>
								</td>
							</tr>
						{%endif%}
					{%else%}
				 			<tr>
								<td style="background-color:##e0e0e0;border-left: 3px solid ##a7b9b7;">
									<label class="checkbox smart-checkbox" style="padding-left:5px;"> <input type="checkbox" onclick="toggleCheckbox({{docSnapshotCd}});" 	{% if not hidden %} checked {%endif%} class="userAnswer" name="USER_ANSWER_CD" value="{{comment.USER_ANSWER_CD}}" />
									<span style="font-size:12px;">{{comment.ANSWER_COMMENTS}}</span>
									</label>
								</td>
							</tr>
							<tr>
								<td>
									</br>
								</td>
							</tr>

					{%endif%}
				{% endfor %}

					</tbody>
				</table>

				{% endmacro %}
				{% if(DATA.COMMENTS|length > 0)%}
					{{macros.displayComments(DATA.COMMENTS,DATA.COMEMNTS_CHANGES,DATA.READONLY,DATA.DOC_SNAPSHOT_CD)}}
				{%endif%}
				{%if DATA.RENDER_TYPE =='HTML'%}

				<script type="text/javascript">
					function toggleCheckbox(snapshotCd){
						var answers = new Array();
						$("input:checkbox[name='USER_ANSWER_CD']:not(:checked)").each(function(){
							 answers.push({[$(this).attr('name')]:$(this).attr('value')});
						});
						var postData = {'value': JSON.stringify(answers), 'key': 'COMEMNTS','DOC_SNAPSHOT_CD':snapshotCd};
						$.ajax({
							url: '#application.cbController.getSetting('modulesApplicationURL')#index.cfm?fuseaction=DocReport.saveDocChanges',
							data: postData,
							type: 'POST',
							dataType:'html',
							}).done(function (result) {
							    console.log(result);
							});
					}
				</script>
				{%endif%}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable="tesScoresHtml">
			<cfoutput>
				{% for value in DATA.TOTAL_SCORES %}
				 		{{value.TES_SCORE}}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable="minCompletionDt">
			<cfoutput>
				{% for value in DATA.TOTAL_SCORES %}
				 		{{value.MIN_COMPLETION_DT}}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable="maxCompletionDt">
			<cfoutput>
				{% for value in DATA.TOTAL_SCORES %}
				 		{{value.MAX_COMPLETION_DT}}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable="minStartDt">
			<cfoutput>
				{% for value in DATA.TOTAL_SCORES %}
				 		{{value.MIN_START_DT}}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable="maxEndDt">
			<cfoutput>
				{% for value in DATA.TOTAL_SCORES %}
				 		{{value.MAX_END_DT}}
				{% endfor %}
			</cfoutput>
		</cfsavecontent>
		<cfset result["RATING_SCALES"] = ratingScalesHtml />
		<cfset result["EXTRA_SCORES"] = extraScoresHtml />
		<cfset result["NR_EVALS"] = evalsHtml />
		<cfset result["NR_STUDENTS"] = studentsHtml />
		<cfset result["TES_SCORE"] = tesScoresHtml />
		<cfset result["COMMENTS"] = commentsHtml />
		<cfset result["DOCUMENT_NAME"] = documentHtml />
		<cfset result["SUPERVISOR_NAME"] = supervisorHtml />
		<cfset result["APPOINTMENT"] = appointmentHtml />
		<cfset result["MIN_COMPLETION_DT"] = minCompletionDt />
		<cfset result["MAX_COMPLETION_DT"] = maxCompletionDt />
		<cfset result["MIN_START_DT"] = minStartDt />
		<cfset result["MAX_END_DT"] = maxEndDt />
		<cfreturn result>
	</cffunction>

</cfcomponent>