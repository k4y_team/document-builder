<cfcomponent name="TesSetupFilter" code="QB_TES_SETUP_FILTER" displayName="TesSetupFilter" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" label="" inject="coldbox:setting:datasource" scope="variables"/>
	<cfproperty name="PROGRAM_NAME"  type="string" label="Program" restriction_type="DOCUMENT_BUILDER_PROGRAM" id="PROGRAM_CD">
	<cfproperty name="TEMPLATE_NAME"  type="string" label="Template Name" id="TEMPLATE_ID">
	<cfproperty name="SETUP_NAME"  type="string" label="Setup Name" id="SETUP_ID">
	<cfproperty name="EVAL_FORM_DESC"  type="string" label="Evaluation Form" id="EVAL_FORM_CD">


	<cfscript>

	public TesSetupFilter function init() {
		variables.args = arguments;
		super.init();
		return this;
	}

	function getGrants() {
		 var grants = [
					   {OBJECT="RSPROGRAM_VIEW",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"},
					   {OBJECT="SUPERVISOR_APPOINTMENT",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
					 ];
		return grants;
	}


    public string function getDataProvider_PROGRAM_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
        				DISTINCT
                        A.PROGRAM_ID as idField,
                        A.PROGRAM_NAME as descField
                    FROM
                       {pg_datasource}.RSPROGRAM_VIEW A
                    ORDER BY UPPER(A.PROGRAM_NAME)";
        return _sql;
    }
    public string function getDataProvider_TEMPLATE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.LETTER_TEMPLATE_CD as idField,
                        A.LETTER_TEMPLATE_NAME as descField
                    FROM
                       DocumentTemplate_VIEW A
                    WHERE INSTANCE_ID = 5
                    ORDER BY UPPER(A.LETTER_TEMPLATE_NAME)";
        return _sql;
    }
    public string function getDataProvider_SETUP_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
         				DISTINCT
                        A.DOC_SETUP_ID as idField,
                        A.DOC_SETUP_NAME as descField
                    FROM
                       DOCSETUPELIGIBLEUSERS_VIEW A
                    WHERE INSTANCE_ID = 5
                    ORDER BY UPPER(A.DOC_SETUP_NAME)";
        return _sql;
    }

    public string function getDataProvider_EVAL_FORM_DESC() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.EVAL_FORM_CD as idField,
                        A.EVAL_FORM_DESC as descField
                    FROM
                       {eval_form_datasource}.EVAL_FORM A
                       WHERE A.MAIN_FORM_CD IS NULL
                    ORDER BY UPPER(A.EVAL_FORM_DESC)";
        return _sql;
    }

    </cfscript>


</cfcomponent>