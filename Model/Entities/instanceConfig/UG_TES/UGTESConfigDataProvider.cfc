<cfcomponent name="UGTESConfigDataProvider" code="QB_UG_TES_CONFIG_DATA_PROVIDER" displayName="UGTESConfigDataProvider" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" label="" inject="coldbox:setting:datasource" scope="variables"/>

	<cfproperty name="COMPARISONS_DEF"  type="string">

	<cfscript>

	public UGTESConfigDataProvider function init() {
		variables.args = arguments;


		super.init();
		return this;
	}

	</cfscript>

	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
				{OBJECT="EVAL_FORM",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
			]>
		<cfreturn grants>
	</cffunction>


	<cffunction name="getSQLS" access="public" output="true" returntype="struct">
		<cfset var sqls = StructNew()>
		<cfset ratingGroupsSql =''>
		<cfset questionDefinitionsSql = ''>
		<cfset ratingScalesSql = ''>
		<cfset scoresSql = ''>
		<cfset comparisonsSql = ''>

		<cfsavecontent variable ="ratingGroupsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
						{% set data =[{rating_group_cd:1,rating_group_desc:'Rating Descriptin'}]%}
						{{macros.SQLFromArray(data)}}
				{%else %}
						SELECT DISTINCT
							    C.RATING_GROUP_CD,
							    C.RATING_GROUP_DESC
							FROM
							    {fbx_datasource}.EVAL_QUESTION A,
							    {fbx_datasource}.RATING_SCALE_GROUP C,
							    (
							        SELECT
							            RATING_CD,
							            RATING_DESC,
							            RATING_GROUP_CD,
							            RATING_NR,
							            RATING_VALUE,
							            WEIGHT
							        FROM
							            {fbx_datasource}.RATING_SCALE
							        UNION
							        SELECT
							            0 AS RATING_CD,
							            'N/A' AS RATING_DESC,
							            RATING_GROUP_CD AS RATING_GROUP_CD,
							            0 AS RATING_NR,
							            '0' AS RATING_VALUE,
							            0 AS WEIGHT
							        FROM
							            {fbx_datasource}.RATING_SCALE_GROUP
							    ) D,
							    TES_CONFIG TC
							WHERE
							        A.QUESTION_TYPE_CD = 1
							    AND
							        A.RATING_GROUP_CD = C.RATING_GROUP_CD
							    AND
							        C.RATING_GROUP_CD = D.RATING_GROUP_CD
							    AND
							        TC.EVAL_FORM_CD = A.EVAL_FORM_CD
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					   ORDER BY  RATING_GROUP_CD ASC
				 {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="questionDefinitionsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,question_desc:'Overall Teaching Effectiveness: Overall for this clinical experience, what is your opinion of the effectiveness of the instructor?',RATING_GROUP_CD:1},
								  {question_cd:2,question_desc:'Evaluation (if applicable): A variety of observation methods were utilized including case discussion and direct observation. Evaluation was fair, well constructed and provided in a timely manner. Feedback was received and was constructive and informative.',RATING_GROUP_CD:1}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
						DISTINCT
						B.RATING_GROUP_CD,
    					B.QUESTION_CD,
					    B.QUESTION_DESC,
					    B.QUESTION_TYPE_CD,
					    C.QUESTION_TYPE_DESC,
					    B.QUESTION_NR,
					    G.GROUP_NR
					FROM
					    {fbx_datasource}.EVAL_FORM A,
					    {fbx_datasource}.EVAL_QUESTION B,
					    {fbx_datasource}.eval_group g,
					    {fbx_datasource}.QUESTION_TYPE C,
					    UG_QUESTIONS_MAPPING_VIEW TCQ
					WHERE
					        B.QUESTION_CD = TCQ.GROUP_QUESTION_ID
    					AND
					        A.EVAL_FORM_CD = B.EVAL_FORM_CD
					    AND
					        B.QUESTION_TYPE_CD = C.QUESTION_TYPE_CD
					    AND
					        C.QUESTION_TYPE_DESC = 'Rating Scale'
					  	AND B.GROUP_CD = G.GROUP_CD
					  	{% if DATA.DOC_SETUP_ID is defined %}
							AND TCQ.DOC_SETUP_ID  in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					    ORDER BY G.GROUP_NR ASC,B.QUESTION_NR ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="ratingScalesSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{rating_cd:1,rating_desc:'Poor',rating_nr:1,rating_value:1,weight:1,rating_group_cd:1},
								  {rating_cd:2,rating_desc:'Fair',rating_nr:2,rating_value:2,weight:2,rating_group_cd:1},
								  {rating_cd:3,rating_desc:'Good',rating_nr:3,rating_value:3,weight:3,rating_group_cd:1},
								  {rating_cd:4,rating_desc:'Satisfactory',rating_nr:4,rating_value:4,weight:4,rating_group_cd:1},
								  {rating_cd:5,rating_desc:'Excellent',rating_nr:5,rating_value:5,weight:5,rating_group_cd:1}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT DISTINCT
					    D.RATING_CD,
					    D.RATING_DESC,
					    D.RATING_NR,
					    D.RATING_VALUE,
					    D.WEIGHT,
					    C.RATING_GROUP_CD,
					    C.RATING_GROUP_DESC
					FROM
					    {fbx_datasource}.EVAL_QUESTION A,
					    {fbx_datasource}.RATING_SCALE_GROUP C,
					    (
					        SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            RATING_VALUE,
					            WEIGHT
					        FROM
					            {fbx_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            NULL AS RATING_NR,
					            '' AS RATING_VALUE,
					            0 AS WEIGHT
					        FROM
					            {fbx_datasource}.RATING_SCALE_GROUP
					    ) D,
					    TES_CONFIG TC
					WHERE
					        A.QUESTION_TYPE_CD = 1
					    AND
					        A.EVAL_FORM_CD = TC.EVAL_FORM_CD
					    AND
					        A.RATING_GROUP_CD = C.RATING_GROUP_CD
					    AND
					        C.RATING_GROUP_CD = D.RATING_GROUP_CD
						  	{% if DATA.DOC_SETUP_ID is defined %}
								AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    	{% endif %}
						   ORDER BY  RATING_NR ASC,RATING_GROUP_CD ASC,D.WEIGHT ASC
			 {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="scoresSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{score_type_id:1,score_type_desc:'Mean Value',score_type_code:'MEAN_VALUE'},
								  {score_type_id:2,score_type_desc:'Median Value',score_type_code:'MEDIAN_VALUE'},
								  {score_type_id:3,score_type_desc:'Standard Deviation Value',score_type_code:'SDV'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
					    SC.SCORE_TYPE_ID,
					    SC.SCORE_TYPE_DESC,
					    SC.SCORE_TYPE_CODE
					FROM
					    TES_SCORE SC,
					    TES_CONFIG_SCORES TCS,
					    TES_CONFIG TC
					WHERE
					        SC.SCORE_TYPE_ID = TCS.SCORE_TYPE_ID
					    AND
					        TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					    order by SCORE_TYPE_ID ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="comparisonsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{comparison_type_id:1,comparison_type_desc:'All Teachers',comparison_type_code:'ALL_TEACHERS'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
					    TC.COMPARISON_TYPE_ID,
					    TC.COMPARISON_TYPE_DESC,
					    TC.COMPARISON_TYPE_CODE
					FROM
					    TES_COMPARISON TC,
					    TES_CONFIG_COMPARISONS TCC,
					    TES_CONFIG TC
					WHERE
					       TC.COMPARISON_TYPE_ID =TCC.COMPARISON_TYPE_ID
					    AND
					        TC.TES_CONFIG_ID = TCC.TES_CONFIG_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND	TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="reportsSql">
			<cfoutput>
				SELECT XX.* FROM (
					WITH DOCUMENT_ELIGIBLE_USERS AS (
						SELECT DISTINCT
		                	A.USER_ID,
		                	A.USER_NAME,
                        	B.DOC_SETUP_ID,
                       		B.DOC_SETUP_NAME,
							A.DOC_SETUP_ELIGIBLE_USER_ID
		            FROM
		                DocSetupEligibleUsers_VIEW A,
		                DOC_SETUP B,
		                UD_USER U
		            WHERE
		                    A.DOC_SETUP_ID = B.DOC_SETUP_ID
		                AND
		                    A.USER_ID = U.USER_ID
		                {% if DATA.DOC_LETTER_INSTANCE_CD is defined %}
		                 	AND   B.INSTANCE_ID = {{ DATA.DOC_LETTER_INSTANCE_CD}}
		                {% endif %}
		                {% if DATA.SUPERVISOR_ID is defined %}
							AND	{{macros.whereClause('A.USER_ID',join(DATA.SUPERVISOR_ID, '\',\''))}}
		                {% endif %}
		                {% if DATA.LOCATION_RESTRICTIONS_LIST is defined %}
			             AND EXISTS ( SELECT 1
								FROM
								    {fbx_datasource}.EVAL_SUPERVISOR_USER ESU,
								    {fbx_datasource}.SUPERVISOR_APPOINTMENT SA,
								    {fbx_datasource}.APPOINTMENT_TYPE AT,
								    {fbx_datasource}.ORGANIZATION H
								WHERE
								        ESU.USER_ID = A.USER_ID
								    AND
								        ESU.SUPERVISOR_ID = SA.SUPERVISOR_ID
								    AND
								        SA.APPOINTMENT_GROUP = 2
								    AND
								    	AT.APPOINTMENT_TYPE_CD in (1,2)
								    AND
								        SA.APPOINTMENT_TYPE_CD = AT.APPOINTMENT_TYPE_CD
								    AND
								        SA.ORG_CD = H.ORG_CD
								    AND	{{macros.whereClause('SA.ORG_CD',join(DATA.LOCATION_RESTRICTIONS_LIST, '\',\''))}}
						)
			            {% endif %}
		                {% if DATA.LOCATION_ID is defined %}
		             	   AND EXISTS ( SELECT 1
								FROM
								    {fbx_datasource}.EVAL_SUPERVISOR_USER ESU,
								    {fbx_datasource}.SUPERVISOR_APPOINTMENT SA,
								    {fbx_datasource}.APPOINTMENT_TYPE AT,
								    {fbx_datasource}.ORGANIZATION H
								WHERE
								        ESU.USER_ID = A.USER_ID
								    AND
								        ESU.SUPERVISOR_ID = SA.SUPERVISOR_ID
								    AND
								        SA.APPOINTMENT_GROUP = 2
								    AND
								    	AT.APPOINTMENT_TYPE_CD in (1,2)
								    AND
								        SA.APPOINTMENT_TYPE_CD = AT.APPOINTMENT_TYPE_CD
								    AND
								        SA.ORG_CD = H.ORG_CD
								    AND	{{macros.whereClause('SA.ORG_CD',join(DATA.LOCATION_ID, '\',\''))}}
								    {% if DATA.LOCATION_TYPE_ID is defined %}
								    	AND	{{macros.whereClause('AT.APPOINTMENT_TYPE_CD',join(DATA.LOCATION_TYPE_ID, '\',\''))}}
								    {% endif %}
						)
		             	{% endif %}
		             	{% if DATA.LOCATION_TYPE_ID is defined %}
		             	   AND EXISTS ( SELECT 1
								FROM
								    {fbx_datasource}.EVAL_SUPERVISOR_USER ESU,
								    {fbx_datasource}.SUPERVISOR_APPOINTMENT SA,
								    {fbx_datasource}.APPOINTMENT_TYPE AT,
								    {fbx_datasource}.ORGANIZATION H
								WHERE
								        ESU.USER_ID = A.USER_ID
								    AND
								        ESU.SUPERVISOR_ID = SA.SUPERVISOR_ID
								    AND
								        SA.APPOINTMENT_GROUP = 2
								    AND
								        SA.APPOINTMENT_TYPE_CD = AT.APPOINTMENT_TYPE_CD
								    AND
								        SA.ORG_CD = H.ORG_CD
								    AND	{{macros.whereClause('AT.APPOINTMENT_TYPE_CD',join(DATA.LOCATION_TYPE_ID, '\',\''))}}
								    {% if DATA.LOCATION_ID is defined %}
								    	AND	{{macros.whereClause('SA.ORG_CD',join(DATA.LOCATION_ID, '\',\''))}}
								    {% endif %}
						)
		             	{% endif %}
		                {% if DATA.SETUP_ID is defined %}
							AND	{{macros.whereClause('A.DOC_SETUP_ID',join(DATA.SETUP_ID, '\',\''))}}
		                {% endif %}
		                {% if DATA.TEMPLATE_ID is defined %}
							AND	{{macros.whereClause('B.DOC_LETTER_TEMPLATE_CD',join(DATA.TEMPLATE_ID, '\',\''))}}
		                {% endif %}
					),SNAPSHOTS  AS (
					     SELECT
					        A.USER_ID,
					        A.DOC_SNAPSHOT_CD,
					        A.DOC_PUBLISHED_USER_ID,
					        A.DOC_PUBLISHED_DT,
					        'Y' as ENABLE_PUBLISH,
					        NVL2(A.DOC_PUBLISHED_DT,2,1) AS PUBLISHED_STATUS_ID,
					        A.CREATION_DT,
					        A.INSTANCE_ID,
					        S.DOC_SETUP_ID,
					        S.DOC_SETUP_NAME AS REPORT_NAME,
					        T.LETTER_TEMPLATE_NAME,
					        T.DOC_LETTER_TEMPLATE_CD,
					        T.INSTRUCTIONS,
					        NVL2(P.USER_ID,P.FIRST_NAME|| ' '|| P.LAST_NAME,'') AS PUBLISHED_BY,
					        NVL2(G.USER_ID,G.FIRST_NAME|| ' '|| G.LAST_NAME,'') AS GENERATED_BY
					    FROM
					        DOC_SNAPSHOT_BASE A,
					        DOC_LETTER_TEMPLATE T,
					        DOC_SETUP S,
					        UD_USER P,
					        UD_USER G
					    WHERE
					            A.DOC_SETUP_ID = S.DOC_SETUP_ID
					        AND A.DOC_LETTER_TEMPLATE_CD = T.DOC_LETTER_TEMPLATE_CD
					        {% if DATA.DOC_LETTER_INSTANCE_CD is defined %}
		                 		AND   A.INSTANCE_ID = {{ DATA.DOC_LETTER_INSTANCE_CD}}
		                	{% endif %}
		                	{% if DATA.SETUP_ID is defined %}
								AND	{{macros.whereClause('A.DOC_SETUP_ID',join(DATA.SETUP_ID, '\',\''))}}
		                	{% endif %}
					        AND A.DOC_PUBLISHED_USER_ID =P.USER_ID(+)
					        AND A.CREATION_ID = G.USER_ID(+)
					),UNPUBLISHED AS (
						SELECT
					    	*
					    FROM
					    	UG_USER_UNPUBLISHED_EVALS
					    WHERE 1=1
					       	{% if DATA.INSTANCE_ID is defined %}
		                 		AND   INSTANCE_ID = {{ DATA.INSTANCE_ID}}
		                	{% endif %}
		                	{% if DATA.SUPERVISOR_ID is defined %}
								AND	{{macros.whereClause('USER_ID',join(DATA.SUPERVISOR_ID, '\',\''))}}
		                	{% endif %}

					)

					SELECT
					    DOCUMENT_ELIGIBLE_USERS.USER_ID,
					    DOCUMENT_ELIGIBLE_USERS.USER_NAME,
					    DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_NAME,
					    DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_ID,
					    DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_ELIGIBLE_USER_ID,
					    Y.INSTANCE_ID,
					    Y.DOC_SNAPSHOT_CD,
					    Y.LETTER_TEMPLATE_NAME,
					    Y.DOC_LETTER_TEMPLATE_CD,
					    Y.INSTRUCTIONS,
					    Y.ENABLE_PUBLISH,
					    Y.DOC_PUBLISHED_DT,
					    Y.PUBLISHED_STATUS_ID,
					    NVL2(Y.DOC_PUBLISHED_DT, 'Published', 'Not Published') AS PUBLISHED_STATUS,
					    TO_CHAR(Y.CREATION_DT,'dd-Mon-yyyy') as GENERATE_DT,
					    EPD.EVALUATION_PERIOD,
					    A.Details || '</br> Evaluation Period: <b>' ||EPD.EVALUATION_PERIOD||'</b>' as Details,
					    Y.DOC_PUBLISHED_USER_ID,
					    Y.PUBLISHED_BY,
					    Y.GENERATED_BY,
					    Y.REPORT_NAME,
					    DECODE(
					        COUNT(
					            Y.DOC_SNAPSHOT_CD
					        ) OVER(PARTITION BY
					            Y.USER_ID, Y.DOC_SETUP_ID
					        ),
					        0,
					        0,
					        COUNT(
					            Y.DOC_SNAPSHOT_CD
					        ) OVER(PARTITION BY
					            Y.USER_ID, Y.DOC_SETUP_ID
					        )
					    ) HISTORICAL_TES_REPORTS,
					    NVL(UNPUBLISHED.UNPUBLISHED_EVALUATIONS,0) AS UNPUBLISHED_EVALUATIONS,
					    NVL(UNPUBLISHED.DISTINCT_EVALUATORS,0) AS DISTINCT_EVALUATORS,
					    DECODE(UNPUBLISHED.SETUP_STATUS_ID,'2','Ready','Not Ready') as SETUP_STATUS,
					    NVL(UNPUBLISHED.SETUP_STATUS_ID,'1') as SETUP_STATUS_ID
					FROM
					    DOCUMENT_ELIGIBLE_USERS ,
					    SNAPSHOTS Y,
					    UNPUBLISHED,
					    SETUPDETAIL_VIEW A,
					    DOC_SNAPSHOT_EVAL_PERIOD EPD
					WHERE  DOCUMENT_ELIGIBLE_USERS.USER_ID = Y.USER_ID(+)
					AND DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_ID = Y.DOC_SETUP_ID (+)
					AND DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_ID = A.DOC_SETUP_ID (+)
					AND DOCUMENT_ELIGIBLE_USERS.USER_ID = UNPUBLISHED.USER_ID(+)
					AND DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_ID = UNPUBLISHED.DOC_SETUP_ID (+)
					AND Y.DOC_SNAPSHOT_CD = EPD.DOC_SNAPSHOT_CD(+)
		            {% if DATA.SETUP_STATUS_ID is defined %}
						AND	{{macros.whereClause('NVL(UNPUBLISHED.SETUP_STATUS_ID,1)',join(DATA.SETUP_STATUS_ID, '\',\''))}}
		            {% endif %}
		            {% if DATA.COURSE_CD is defined %}
		             AND DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_ID in (SELECT a.DOC_SETUP_ID FROM tes_config a,UG_HIERARCHY_CONFIG_COURSES b
							WHERE	a.tes_config_id = b.tes_config_id(+) and (b.course_cd is null or {{macros.whereClause('b.course_cd',join(DATA.COURSE_CD, '\',\''))}}))
		             {% endif %}
		             {% if DATA.COURSE_RESTRICTIONS_LIST is defined %}
		             AND DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_ID in (SELECT a.DOC_SETUP_ID FROM tes_config a,UG_HIERARCHY_CONFIG_COURSES b
							WHERE	a.tes_config_id = b.tes_config_id(+) and (b.course_cd is null or {{macros.whereClause('b.course_cd',join(DATA.COURSE_RESTRICTIONS_LIST, '\',\''))}}))
		            {% endif %}
		            {% if (DATA.DOC_SNAPSHOT_CD is defined) and (DATA.DOC_SNAPSHOT_CD !='') %}
		            	AND  Y.DOC_SNAPSHOT_CD in ({{ DATA.DOC_SNAPSHOT_CD}})
		            {% endif %}
		            {% if (DATA.USER_ID is defined) and (DATA.USER_ID !='') %}
		            	AND  Y.USER_ID in ({{ DATA.USER_ID}})
		            {% endif %}
		            ORDER BY DOCUMENT_ELIGIBLE_USERS.DOC_SETUP_NAME ASC, Y.DOC_PUBLISHED_DT DESC NULLS FIRST
		             ) XX WHERE 1=1
		            {% if DATA.STATUS is defined %}
						AND	{{macros.whereClause('XX.PUBLISHED_STATUS_ID',join(DATA.STATUS, '\',\''))}}
		            {% endif %}
		            {% if DATA.PUBLISHED_DT is defined %}
		            	{% if DATA.PUBLISHED_DT.TYPE =='equal' %}
							AND	{{macros.whereClause("TO_CHAR(XX.DOC_PUBLISHED_DT,'MM-DD-YYYY')",join(DATA.PUBLISHED_DT.VALUE, '\',\''))}}
						{% endif %}
						{% if DATA.PUBLISHED_DT.TYPE =='between' %}
							AND	{{macros.betweenClause("trunc(XX.DOC_PUBLISHED_DT)",DATA.PUBLISHED_DT.VALUE)}}
						{% endif %}
		            {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="eligibleUsersSql">
			<cfoutput>
				SELECT
				 	DISTINCT
				    	A.ADDRESSEE_ID AS USER_ID,
				    	D.DOC_SETUP_ID
				FROM
				    {fbx_datasource}.S4Y_EVAL_SCHEDULE B,
				    S4Y_USER_EVAL A,
				    (
				        SELECT DISTINCT
				            UE.USER_EVAL_CD,
				            UE.COMPLETE_DT
				        FROM
				            {fbx_datasource}.S4Y_USER_EVAL UE,
				            {fbx_datasource}.S4Y_USER_ANSWER D,
				            {fbx_datasource}.EVAL_QUESTION E
				        WHERE
				                UE.USER_EVAL_CD = D.USER_EVAL_CD
				            AND
				                D.QUESTION_CD = E.QUESTION_CD
				            AND (
				                E.QUESTION_TYPE_CD = 1
				            )
				    ) Q,
				    {fbx_datasource}.EVAL_SUPERVISOR S,
				    {fbx_datasource}.S_USER U,
				    TES_CONFIG TC,
				    UG_QUESTIONS_MAPPING_VIEW TCQ,
				    {fbx_datasource}.EVAL_QUESTION EQ,
				    UG_HIERARCHY_CONFIG_COURSES TCC,
				    DOC_SETUP D
				WHERE
				        A.EVAL_SCHEDULE_CD = B.EVAL_SCHEDULE_CD
			        AND
			        	A.EVAL_FORM_CD = EQ.EVAL_FORM_CD
			        AND (
			                NVL(
			                    EQ.MAIN_QUESTION_CD,
			                    EQ.QUESTION_CD
			                ) = TCQ.QUESTION_ID
			            OR
			                NVL(
			                    EQ.MAIN_QUESTION_CD,
			                    EQ.QUESTION_CD
			                ) = TCQ.GROUP_QUESTION_ID
			        )
			        {% if DATA.DOC_SETUP_ID is defined %}
						AND D.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		            {% endif %}
				    AND (
				        B.COURSE_CD = TCC.COURSE_CD
				    ) AND (
				        S.SUPERVISOR_ID = U.SUPERVISOR_ID
				    ) AND (
				        A.ADDRESSEE_ID = U.USER_ID
				    ) AND (
				        A.USER_EVAL_CD = Q.USER_EVAL_CD (+)
				    ) AND
				        TCC.TES_CONFIG_ID = TC.TES_CONFIG_ID
				    AND TC.DOC_SETUP_ID = D.DOC_SETUP_ID
				    AND TCQ.TES_CONFIG_ID = TC.TES_CONFIG_ID
				   	AND TRUNC(B.END_DT) <= TRUNC(NVL(TC.ACTIVITY_END_DT,B.END_DT))

			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="unpublishedEvalsSql">
			<cfoutput>
				SELECT
		            D.*,
		            C.USER_ID
		        FROM
		            UG_USER_UNPUBLISHED_EVALS C,
		            TES_CONFIG D
		        WHERE
		                C.DOC_SETUP_ID = D.DOC_SETUP_ID
		            AND
		                D.MIN_EVALS <= C.DISTINCT_EVALUATORS
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="reportQuestionsSql">
			<cfoutput>
				 SELECT
						    TC.DOC_SETUP_ID,
						    NVL(B.MAIN_QUESTION_CD,B.QUESTION_CD) AS REPORT_QUESTION_CD,
						    B.QUESTION_CD AS EVAL_QUESTION_CD
						    FROM
						         {fbx_datasource}.EVAL_FORM A,
						         {fbx_datasource}.EVAL_QUESTION B,
						         {fbx_datasource}.QUESTION_TYPE C,
						        TES_CONFIG TC
						    WHERE
						            A.EVAL_FORM_CD = TC.EVAL_FORM_CD
						        AND
						            A.EVAL_FORM_CD = B.EVAL_FORM_CD
						        AND
						            B.QUESTION_TYPE_CD = C.QUESTION_TYPE_CD
						        AND
						            C.QUESTION_TYPE_DESC = 'Rating Scale'
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable ="docsetupSql">
			<cfoutput>
				 WITH USED_EVALUATIONS AS (
            			SELECT
						    A.DOC_SNAPSHOT_CD,
						    A.USER_ID,
						    A.EVALUATION_ID AS USER_EVAL_CD
						FROM
						    DOC_SNAPSHOT_EVALUATIONS A,
						    DOC_SNAPSHOT_BASE B,
						    DOC_INSTANCE C
						WHERE
						        A.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
						    AND
						        B.INSTANCE_ID = C.INSTANCE_ID
						    AND C.INSTANCE_CODE='UG_TES_REPORT'
				),EVALUATIONS AS (
				    SELECT
				        DISTINCT
				        D.USER_ID,
			            D.INSTANCE_ID,
			            A.USER_EVAL_CD,
			            D.DOC_SETUP_ID,
			            A.USER_ID AS EVALUATOR_USER_ID,
			            B.TRAINING_SESSION_CD
				     FROM  {fbx_datasource}.S4Y_USER_EVAL  A
   					 	JOIN  {fbx_datasource}.S4Y_EVAL_SCHEDULE B  ON(A.EVAL_SCHEDULE_CD = B.EVAL_SCHEDULE_CD)
   						JOIN DOCSETUPELIGIBLEUSERS_VIEW D ON(D.USER_ID=A.ADDRESSEE_ID AND D.INSTANCE_CODE  ='UG_TES_REPORT' )
   						JOIN TES_CONFIG TC ON(TC.DOC_SETUP_ID = D.DOC_SETUP_ID AND TRUNC(B.END_DT) <= TRUNC(NVL(TC.ACTIVITY_END_DT,B.END_DT)))
   						JOIN UG_HIERARCHY_CONFIG_COURSES TCC ON(TCC.COURSE_CD=B.COURSE_CD AND TCC.TES_CONFIG_ID = TC.TES_CONFIG_ID)
   						LEFT JOIN USED_EVALUATIONS UE ON (A.USER_EVAL_CD = UE.USER_EVAL_CD)
   						JOIN (select MAIN_FORM_CD as FORM_CD,EVAL_FORM_CD FROM {fbx_datasource}.EVAL_FORM  UNION  SELECT EVAL_FORM_CD AS FORM_CD,EVAL_FORM_CD FROM {fbx_datasource}.EVAL_FORM )FR ON (A.EVAL_FORM_CD = FR.EVAL_FORM_CD)
   						JOIN (SELECT TCQ.MAPPING_EVAL_FORM_CD AS EVAL_FORM_CD,TCQ.TES_CONFIG_ID FROM UG_QUESTIONS_MAPPING_VIEW TCQ UNION ALL SELECT TCQ.EVAL_FORM_CD,TCQ.TES_CONFIG_ID FROM UG_QUESTIONS_MAPPING_VIEW TCQ) FORMS ON(FR.FORM_CD=FORMS.EVAL_FORM_CD and FORMS.TES_CONFIG_ID=TC.TES_CONFIG_ID)
  				WHERE  1=1
					AND UE.USER_EVAL_CD IS NULL
 					AND A.COMPLETE_DT IS NOT NULL
	 				{% if DATA.DOC_SETUP_ID is defined %}
						AND D.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		            {% endif %}
		            {% if DATA.USER_ID is defined %}
						AND D.USER_ID in ({{DATA.USER_ID}})
					{% endif %}
					AND NOT EXISTS (
						SELECT
   							1
							FROM
							    {fbx_datasource}.QUESTION_TYPE QT
							    JOIN {fbx_datasource}.EVAL_QUESTION EQ ON (QT.QUESTION_TYPE_CD  = EQ.QUESTION_TYPE_CD)
							    JOIN {fbx_datasource}.S4Y_USER_ANSWER UA ON (UA.QUESTION_CD = EQ.QUESTION_CD)
							WHERE
									UPPER(QT.QUESTION_TYPE_DESC) = 'FORM DISABLER'
							    AND UA.USER_EVAL_CD = A.USER_EVAL_CD
							    AND SAFE_TO_NUMBER(SUBSTR(UA.ANSWER_TEXT,1,1))=0
					)
  					AND
			            1 = (
			                CASE
			                    WHEN NOT
			                        EXISTS (
			                            SELECT
			                                1
			                            FROM
			                                tes_config_sessions tcs
			                            WHERE
			                                tc.tes_config_id = tcs.tes_config_id
			                        )
			                    THEN 1
			                    WHEN EXISTS (
			                        SELECT
			                            1
			                        FROM
			                            tes_config_sessions tcs
			                        WHERE
			                                tc.tes_config_id = tcs.tes_config_id
			                            AND
			                                tcs.training_session_cd = b.training_session_cd
			                    ) THEN 1
			                    ELSE -1
			                END
			            )
					)
				SELECT
				    USER_ID,
				    DOC_SETUP_ID,
				    USER_EVAL_CD,
				    INSTANCE_ID,
				    TRAINING_SESSION_CD
				FROM
				    EVALUATIONS
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="comparisonSql">
			<cfoutput>
				WITH EVALUATIONS AS (
				    SELECT
				        DISTINCT
				        D.USER_ID,
			            D.INSTANCE_ID,
			            A.USER_EVAL_CD,
			            D.DOC_SETUP_ID,
			            A.USER_ID AS EVALUATOR_USER_ID,
			            B.TRAINING_SESSION_CD
				     FROM  {fbx_datasource}.S4Y_USER_EVAL  A
   					 	JOIN  {fbx_datasource}.S4Y_EVAL_SCHEDULE B  ON(A.EVAL_SCHEDULE_CD = B.EVAL_SCHEDULE_CD)
   						JOIN DOCSETUPELIGIBLEUSERS_VIEW D ON(D.USER_ID=A.ADDRESSEE_ID AND D.INSTANCE_CODE  ='UG_TES_REPORT' )
   						JOIN TES_CONFIG TC ON(TC.DOC_SETUP_ID = D.DOC_SETUP_ID AND TRUNC(B.END_DT) <= TRUNC(NVL(TC.ACTIVITY_END_DT,B.END_DT)))
   						JOIN UG_HIERARCHY_CONFIG_COURSES TCC ON(TCC.COURSE_CD=B.COURSE_CD AND TCC.TES_CONFIG_ID = TC.TES_CONFIG_ID)
   						JOIN (select MAIN_FORM_CD as FORM_CD,EVAL_FORM_CD FROM {fbx_datasource}.EVAL_FORM  UNION  SELECT EVAL_FORM_CD AS FORM_CD,EVAL_FORM_CD FROM {fbx_datasource}.EVAL_FORM )FR ON (A.EVAL_FORM_CD = FR.EVAL_FORM_CD)
   						JOIN (SELECT TCQ.MAPPING_EVAL_FORM_CD AS EVAL_FORM_CD,TCQ.TES_CONFIG_ID FROM UG_QUESTIONS_MAPPING_VIEW TCQ UNION ALL SELECT TCQ.EVAL_FORM_CD,TCQ.TES_CONFIG_ID FROM UG_QUESTIONS_MAPPING_VIEW TCQ) FORMS ON(FR.FORM_CD=FORMS.EVAL_FORM_CD and FORMS.TES_CONFIG_ID=TC.TES_CONFIG_ID)
  				WHERE  1=1
  					{% if DATA.DOC_SETUP_ID is defined %}
						AND D.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		            {% endif %}
 					AND A.COMPLETE_DT IS NOT NULL
  					AND
			            1 = (
			                CASE
			                    WHEN NOT
			                        EXISTS (
			                            SELECT
			                                1
			                            FROM
			                                tes_config_sessions tcs
			                            WHERE
			                                tc.tes_config_id = tcs.tes_config_id
			                        )
			                    THEN 1
			                    WHEN EXISTS (
			                        SELECT
			                            1
			                        FROM
			                            tes_config_sessions tcs
			                        WHERE
			                                tc.tes_config_id = tcs.tes_config_id
			                            AND
			                                tcs.training_session_cd = b.training_session_cd
			                    ) THEN 1
			                    ELSE -1
			                END
			            )
					)
				SELECT
				    USER_ID,
				    DOC_SETUP_ID,
				    USER_EVAL_CD as EVALUATION_ID,
				    INSTANCE_ID,
				    TRAINING_SESSION_CD
				FROM
				    EVALUATIONS
			</cfoutput>
		</cfsavecontent>
		<cfset sqls["RATING_GROUPS_DEF"] = ratingGroupsSql>
		<cfset sqls["QUESTIONS_DEF"] = questionDefinitionsSql>
		<cfset sqls["RATING_SCALE_DEF"]	= ratingScalesSql>
		<cfset sqls["SCORES_DEF"]	= scoresSql>
		<cfset sqls["COMPARISONS_DEF"]	= comparisonsSql>
		<cfset sqls["REPORT_QRY"]	= reportsSql>
		<cfset sqls["ELIGIBLEUSERSQRY"]	= eligibleUsersSql>
		<cfset sqls["UNPUBLISHEDEVALSQRY"]	= unpublishedEvalsSql>
		<cfset sqls["REPORTQUESTIONSQRY"]	= reportQuestionsSql>
		<cfset sqls["DOCSETUPQRY"]	= docsetupSql>
		<cfset sqls["COmparisonQRY"]	= comparisonSql>

		<cfreturn sqls>
	</cffunction>


	<cffunction name="preProcessSql" returntype="any" access="public">

	    <cfargument name="data" type="struct" default="#StructNew()#">

		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset configProviderEntity = queryBuilder.getEntity('UGTESConfigDataProvider') />

		<!---Step 1 Insert Report Questions --->
		<cfquery name="prePrcesSQl" datasource="#variables.datasource#">
				MERGE INTO REPORT_QUESTIONS D USING (
					#configProviderEntity.getSQL('REPORTQUESTIONSQRY')#
				    ) S ON (S.DOC_SETUP_ID=D.DOC_SETUP_ID AND D.REPORT_QUESTION_CD = S.REPORT_QUESTION_CD AND D.EVAL_QUESTION_CD = S.EVAL_QUESTION_CD)
				   WHEN NOT MATCHED THEN
						INSERT (D.doc_setup_id,
								D.report_question_cd,
								D.eval_question_cd
								)
						VALUES (s.doc_setup_id,
								S.report_question_cd,
								S.eval_question_cd
					)
		</cfquery>

		<cfset docSetupSql = queryBuilder.getQuery(sql=configProviderEntity.getSQL('DOCSETUPQRY'),datasource=variables.datasource,params=arguments.data)/>

	  <!---Step 2 Calculate Unpublished Evaluation Population --->
	  	<cfquery name="deleteSetupSql" datasource="#variables.datasource#">
		  	DELETE FROM DOC_SETUP_UNPUBLISHED_EVALS D WHERE D.PROCESS_ID ='#arguments.data.processID#'
		</cfquery>

		<cfquery name="docSetupSql" datasource="#variables.datasource#">
				MERGE INTO DOC_SETUP_UNPUBLISHED_EVALS D USING (
					#PreservesingleQuotes(docSetupSql)#
				    ) S ON ('#arguments.data.processID#' = D.PROCESS_ID)
				   WHEN NOT MATCHED THEN
						INSERT (D.doc_setup_id,
								D.EVALUATION_ID,
								D.TRAINING_SESSION_CD,
								D.USER_ID,
								D.INSTANCE_ID,
								D.PROCESS_ID
								)
						VALUES (s.doc_setup_id,
								s.USER_EVAL_CD,
								S.TRAINING_SESSION_CD,
								s.USER_ID,
								s.INSTANCE_ID,
								'#arguments.data.processID#'
					)
		</cfquery>


	 <!---Step 3 Calculate Setup Eligible Evaluations Population --->

		<cfset comparisonSql = queryBuilder.getQuery(sql=configProviderEntity.getSQL('COmparisonQRY'),datasource=variables.datasource,params=arguments.data)/>

		<cfquery name="deleteEligibleEvalsSql" datasource="#variables.datasource#">
		  	DELETE FROM DOC_SETUP_ELIGIBLE_EVALS D WHERE D.PROCESS_ID ='#arguments.data.processID#'
		</cfquery>

		<cfquery name="prePrcesSQl" datasource="#variables.datasource#">
				MERGE INTO DOC_SETUP_ELIGIBLE_EVALS D USING (
					#PreservesingleQuotes(comparisonSql)#
				    ) S ON ('#arguments.data.processID#' = D.PROCESS_ID)
				   WHEN NOT MATCHED THEN
						INSERT (D.doc_setup_id,
								D.EVALUATION_ID,
								D.TRAINING_SESSION_CD,
								D.PROCESS_ID
								)
						VALUES (s.doc_setup_id,
								s.EVALUATION_ID,
								s.TRAINING_SESSION_CD,
								'#arguments.data.processID#'
					)
		</cfquery>



		<cfreturn true>
	</cffunction>

	<cffunction name="postProcessSql" returntype="any" access="public">
 		<cfargument name="data" type="struct" default="#StructNew()#">

		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />


		<cfquery name="mergeEvalPeriod" datasource="#variables.datasource#">
				MERGE INTO DOC_SNAPSHOT_EVAL_PERIOD D USING (
					SELECT
					    A.DOC_SNAPSHOT_CD,
					    A.DOC_SETUP_ID,
					    (
					        SELECT
					            MIN_START_DT || '/' || MAX_END_DT
					        FROM
					            TABLE ( pljson_table.json_table(
					                d.value,
					                pljson_varray(
					                    '[*].MIN_START_DT',
					                    '[*].MAX_END_DT'
					                ),
					                pljson_varray(
					                    'MIN_START_DT',
					                    'MAX_END_DT'
					                ),
					                table_mode   => 'NESTED'
					            ) )
					    ) EVALUATION_PERIOD
					FROM
					    DOC_SNAPSHOT_BASE A,
					    DOC_SNAPSHOT_DATA D
					WHERE
					        A.DOC_SNAPSHOT_CD = D.DOC_SNAPSHOT_CD
					    AND A.PROCESS_ID = '#arguments.data.processID#'
					    AND
					        D.KEY = 'TOTAL_SCORES'
				    ) S ON (S.DOC_SNAPSHOT_CD = D.DOC_SNAPSHOT_CD)
				   WHEN NOT MATCHED THEN
						INSERT (D.DOC_SNAPSHOT_CD,
								D.DOC_SETUP_ID,
								D.EVALUATION_PERIOD
								)
						VALUES (S.DOC_SNAPSHOT_CD,
								S.DOC_SETUP_ID,
								S.EVALUATION_PERIOD
					)
		</cfquery>

		<cfreturn true>
	</cffunction>


</cfcomponent>