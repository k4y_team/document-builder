<cfcomponent name="UGTesConfigForm" code="UG_TES_CONFIG_FORM" displayName="UGTesConfigForm" table="TES_CONFIG" output="false" extends="sis_core.model.blAutomation.EntityBase" accessors="true">

	<cfproperty name="datasource" inject="coldbox:setting:datasource" scope="variables" />

	<cfproperty name="TES_CONFIG_ID" type="numeric" key="true" >
	<cfproperty name="INSTANCE_NAME"  type="string" label="Document Type" id="INSTANCE_ID">
	<cfproperty name="EVAL_FORM_DESC"  type="string" label="Evaluation Form" id="EVAL_FORM_CD"  validations="required">
	<cfproperty name="SHOW_COMMENTS"  type="boolean" label="Show Comments" >
	<cfproperty name="MIN_EVALS"  type="string" label="Minimum Number of distinct evaluators"  validations="required">
	<cfproperty name="SCORE_TYPE_DESC"  type="string" label="Available Scores" id="SCORE_TYPE_ID">
	<cfproperty name="COMPARISON_TYPE_DESC"  type="string" label="Available Comparisons" id="COMPARISON_TYPE_ID">
	<cfproperty name="COURSE_DESC"  type="string" restriction_type="DOCUMENT_BUILDER_COURSE" label="Course" id="COURSE_CD">
	<cfproperty name="TRAINING_SESSION_NAME"  type="string" label="Academic Session" id="TRAINING_SESSION_CD">
	<cfproperty name="ACTIVITY_END_DT"  type="date" label="Activity End Date" >
	<cfproperty name="GENERATE_SCHEDULE_ID"  type="numeric" label="Auto-Generate" >
	<cfproperty name="PUBLISH_SCHEDULE_ID"  type="numeric" label="Auto-Publish" >


	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_FORM",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="TRAINING_SESSION",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
		]>
		<cfreturn grants>
	</cffunction>

	<cfscript>

	public UGTesConfigForm function init(numeric TRAINEE_ID) {
		variables.args = arguments;
		super.init();
		return this;
	}

	public struct function getValidationRules() {
		return {};
	}

	public string function getSQL() {
		var sql = "
		   	SELECT * FROM TES_CONFIG
           ";
		return sql;
	}

	public string function getDataProvider_INSTANCE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.INSTANCE_ID as idField,
                        A.INSTANCE_NAME as descField
                    FROM
                       DOC_INSTANCE A
                    ORDER BY UPPER(A.INSTANCE_NAME)";
        return _sql;
    }
    public string function getDataProvider_COURSE_DESC() {
        var _sql = "SELECT DISTINCT
					    C.COURSE_CD AS IDFIELD,
					    up.program_desc ||'-'|| '['||C.COURSE_CODE||'] '|| C.COURSE_NAME AS DESCFIELD,
					    C.COURSE_NAME,
					    c.year_level,
						up.program_desc,
						up.program_code
					FROM
					     {FBX_DATASOURCE}.COURSE C,
					     {FBX_DATASOURCE}.CURRICULUM R,
					     {FBX_DATASOURCE}.UNIV_MULTIPLE_PROGRAM UP,
						 {FBX_DATASOURCE}.UNIV_MULTIPLE_PROGRAM_COURSE UPC
					WHERE
					   UP.MULTIPLE_PROGRAM_CD = UPC.MULTIPLE_PROGRAM_CD AND
					   UPC.COURSE_CD = C.COURSE_CD AND
					   C.COURSE_CD = R.COURSE_CD
					ORDER BY UP.PROGRAM_CODE ASC, C.YEAR_LEVEL, C.COURSE_NAME ASC";
        return _sql;
    }

    public string function getDataProvider_SCORE_TYPE_DESC() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.SCORE_TYPE_ID as idField,
                        A.SCORE_TYPE_DESC as descField
                    FROM
                       TES_SCORE A
                    ORDER BY UPPER(A.SCORE_TYPE_DESC)";
        return _sql;
    }

     public string function getDataProvider_COMPARISON_TYPE_DESC() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.COMPARISON_TYPE_ID as idField,
                        A.COMPARISON_TYPE_DESC as descField
                    FROM
                       TES_COMPARISON A
                    ORDER BY UPPER(A.COMPARISON_TYPE_DESC)";
        return _sql;
    }

     public string function getDataProvider_EVAL_FORM_DESC() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.EVAL_FORM_CD as idField,
                        A.EVAL_FORM_DESC ||' ['||A.EVAL_FORM_CD||']' as descField
                    FROM
                       {fbx_datasource}.EVAL_FORM A
                       WHERE A.MAIN_FORM_CD IS NULL
                       AND A.COURSE_CD = <course_cd>
                    ORDER BY UPPER(A.EVAL_FORM_DESC)";
        return _sql;
    }



    public string function getDataProvider_TEMPLATE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.LETTER_TEMPLATE_CD as idField,
                        A.LETTER_TEMPLATE_NAME as descField
                    FROM
                       DocumentTemplate_VIEW A
                    ORDER BY UPPER(A.LETTER_TEMPLATE_NAME)";
        return _sql;
    }

  	public string function getDataProvider_TRAINING_SESSION_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.TRAINING_SESSION_CD as idField,
                        A.START_YEAR||' - '||A.END_YEAR   AS descfield
                     FROM
      					  {fbx_datasource}.TRAINING_SESSION A ORDER BY A.END_YEAR DESC";
        return _sql;
    }
    public string function getDataProvider_SHOW_COMMENTS() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
				    	*
					FROM
				    (
				        SELECT
				            'Y' AS idfield,
				            'Yes' AS descfield,
				            1 AS order_nr
				        FROM
				            dual
				        UNION
				        SELECT
				            'N' AS idfield,
				            'No' AS descfield,
				            2 AS order_nr
				        	FROM
            	dual
    			)
			ORDER BY order_nr";
        return _sql;
    }
    public string function getDataProvider_AUTO_PUBLISH() {
         var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
				    	*
					FROM
				    (
				        SELECT
				            'Y' AS idfield,
				            'Yes' AS descfield,
				            1 AS order_nr
				        FROM
				            dual
				        UNION
				        SELECT
				            'N' AS idfield,
				            'No' AS descfield,
				            2 AS order_nr
				        	FROM
            	dual
    			)
			ORDER BY order_nr";
        return _sql;
    }
    public string function getDataProvider_AUTO_GENERATE() {
         var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
				    	*
					FROM
				    (
				        SELECT
				            'Y' AS idfield,
				            'Yes' AS descfield,
				            1 AS order_nr
				        FROM
				            dual
				        UNION
				        SELECT
				            'N' AS idfield,
				            'No' AS descfield,
				            2 AS order_nr
				        	FROM
            	dual
    			)
			ORDER BY order_nr";
        return _sql;
    }

     public struct function getRenderInfo() {
            /* Constants */
            var RENDER_TYPE = "renderType"; var NAME = "name"; var MULTI_SELECT = "multiSelect"; var READ_ONLY = "readOnly";
            var PLACEHOLDER = "placeholder"; var rtDEFAULT = "default"; var DISABLED = "DISABLED"; var addForm = "addFormHtml";
            var mobileaddForm = "addFormMobile";
            /* Variables */
            var fieldDataDefault = super.getRenderInfo();
            fieldDataDefault[rtDEFAULT]["SHOW_COMMENTS"][RENDER_TYPE] = "TOGGLE";
            fieldDataDefault[rtDEFAULT]["SHOW_COMMENTS"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["AUTO_PUBLISH"][RENDER_TYPE] = "TOGGLE";
            fieldDataDefault[rtDEFAULT]["AUTO_PUBLISH"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["AUTO_GENERATE"][RENDER_TYPE] = "TOGGLE";
            fieldDataDefault[rtDEFAULT]["AUTO_GENERATE"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["SCORE_TYPE_DESC"][RENDER_TYPE] = "multiSelect";
            fieldDataDefault[rtDEFAULT]["SCORE_TYPE_DESC"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["TES_CONFIG_ID"][RENDER_TYPE] = "hidden";
            fieldDataDefault[rtDEFAULT]["GENERATE_SCHEDULE_ID"][RENDER_TYPE] = "SCHEDULE";
            fieldDataDefault[rtDEFAULT]["GENERATE_SCHEDULE_ID"]["SECURITY_CODE"] = "DOCUMENT_SETUP_DATES";
            fieldDataDefault[rtDEFAULT]["GENERATE_SCHEDULE_ID"]["params"] = {"EVENT_CODE":"GENERATE_DOCUMENTS","id":"generateSchedule","title":"Generate Schedule Details","fieldPrefix":"generate"};
            fieldDataDefault[rtDEFAULT]["PUBLISH_SCHEDULE_ID"][RENDER_TYPE] = "SCHEDULE";
            fieldDataDefault[rtDEFAULT]["PUBLISH_SCHEDULE_ID"]["SECURITY_CODE"] = "DOCUMENT_SETUP_DATES";
            fieldDataDefault[rtDEFAULT]["PUBLISH_SCHEDULE_ID"]["params"] = {"EVENT_CODE":"PUBLISH_DOCUMENTS","id":"publishSchedule","title":"Publish Schedule Details","fieldPrefix":"publish"};
            fieldDataDefault[rtDEFAULT]["PROGRAM_DESC"][RENDER_TYPE] = "select";
            fieldDataDefault[rtDEFAULT]["PROGRAM_DESC"]["naDesc"] = "All Programs";
            fieldDataDefault[rtDEFAULT]["PROGRAM_DESC"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["EVAL_FORM_DESC"][RENDER_TYPE] = "select";
            fieldDataDefault[rtDEFAULT]["EVAL_FORM_DESC"]["dependencyFields"] = [{"PARAM_NAME" = "course_cd", "FIELD_ID" = "course_cd"}];
            fieldDataDefault[rtDEFAULT]["EVAL_FORM_DESC"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["TRAINING_SESSION_NAME"][RENDER_TYPE] = "multiSelect";
            fieldDataDefault[rtDEFAULT]["TRAINING_SESSION_NAME"]["naDesc"] = "All Sessions";
            fieldDataDefault[rtDEFAULT]["MIN_EVALS"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["TRAINING_SESSION_NAME"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["COMPARISON_TYPE_DESC"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            fieldDataDefault[rtDEFAULT]["COURSE_DESC"]["SECURITY_CODE"] = "DOCUMENT_REPORT_SETUP";
            return fieldDataDefault;
        }


	</cfscript>

	<cffunction name="getcolumnProperties" access="public" returntype="string">
		<cfset var columnProperties = "TES_CONFIG_ID,TRAINING_SESSION_NAME,ACTIVITY_END_DT,COURSE_DESC,EVAL_FORM_DESC,SHOW_COMMENTS,GENERATE_SCHEDULE_ID,PUBLISH_SCHEDULE_ID,MIN_EVALS,SCORE_TYPE_DESC,COMPARISON_TYPE_DESC" />
		<cfreturn columnProperties >
	</cffunction>

	<cffunction name="getTemplates" access="public" returntype="struct" >
		<cfset var result = {} />
		<cfset var formHtml = "">

		<cfset var columnProperties = getcolumnProperties() />
		<cfsavecontent variable="formHtml">
			<cfoutput>
				 <style>
	                ##min_evals{
	                    width:50px;
	                }
	                ##scheduleBtn_generateSchedule,##scheduleBtn_publishSchedule {
		                float: left !important;
						padding-top: -8px;
						margin: -5px;
						margin-top: -5px;
						margin-right: -5px;
						margin-bottom: -5px;
						margin-left: -15px;
					}
            	</style>
			<h3 class="accordion-head">
				Configuration
			</h3>
				<cfloop list="#columnProperties#" index="property">
					<% if (typeof fields["#property#"] != "undefined"){ %>
						<div class="row-form clearfix no-border">
							<div class="span2"><%= fields["#property#"].formLabel %></div>
							<div class="span3">
								<%= fields["#property#"].input %>
							</div>
						</div>
					<% } %>
				</cfloop>
			</cfoutput>

		</cfsavecontent>
		<cfset result["default"] = formHtml />
		<cfreturn result>
	</cffunction>

	<cffunction name="save" access="public" output="false" returntype="boolean">
		<cfargument name="data" required="true" type="struct">

		<cfset var bValid = true>
		<cfset var bSuccess = true>
   		<cfset var validatorObj = application.wirebox.getInstance("EntityValidator@CORE")>
		<cfset EntityRegistry = variables.wirebox.getInstance('EntityRegistry@CORE') />
		<cfset dbEntity = EntityRegistry.initDBEntity("TES_CONFIG") />

     <cfscript>
     var renderInfo = this.getRenderInfo()['default'];
	 for (property in  renderInfo){
	 	field = renderInfo['#property#'];
        if (UCASE(field.renderType) eq "SCHEDULE"){
			saveScheduleFields(property,field,arguments.data);
        }
	 }
	</cfscript>



		<cfset var mappings = {}>
		<cfset bValid = validatorObj.validateData(entityCode = 'TES_CONFIG', data = arguments.data) >
		<cfif bValid>
			<cfset bValid = dbEntity.save(arguments.data,mappings,{update_if_null_only = false}) />
		</cfif>

		<cfif bValid>
			<cfquery name="deleteScores" datasource="#variables.datasource#">
				DELETE FROM TES_CONFIG_SCORES D WHERE D.DOC_SETUP_ID = '#arguments.data.DOC_SETUP_ID#'
			</cfquery>

			<cfset scoreList = arguments.data.SCORE_TYPE_ID>
			<cfloop  list="#scoreList#" index="scoreCd">
			<cfset arguments.data.SCORE_TYPE_ID =scoreCd >
			<cfset var mappings = {
					DOC_SETUP_ID={NAME="DOC_SETUP_ID"},
					TES_CONFIG_ID={NAME="TES_CONFIG_ID"},
					SCORE_TYPE_ID={NAME="SCORE_TYPE_ID"}
				}>

				<cfset dbScoreEntity = EntityRegistry.initDBEntity("TES_CONFIG_SCORES") />
				<cfset bValid = dbScoreEntity.save(arguments.data,mappings,{update_if_null_only = false}) />

			</cfloop>
		</cfif>

		<cfif bValid>
			<cfquery name="deleteComparisons" datasource="#variables.datasource#">
				 DELETE FROM TES_CONFIG_COMPARISONS D WHERE D.DOC_SETUP_ID = '#arguments.data.DOC_SETUP_ID#'
			</cfquery>
			<cfset comaprisonList = arguments.data.COMPARISON_TYPE_ID>
			<cfloop  list="#comaprisonList#" index="comparison_cd">
				<cfset arguments.data.COMPARISON_TYPE_ID = comparison_cd >
				<cfset var mappings = {
					DOC_SETUP_ID={NAME="DOC_SETUP_ID"},
					TES_CONFIG_ID={NAME="TES_CONFIG_ID"},
					COMPARISON_TYPE_ID={NAME="COMPARISON_TYPE_ID"}
					}>
					<cfset dbEntity = EntityRegistry.initDBEntity("TES_CONFIG_COMPARISONS") />
					<cfset bValid = dbEntity.save(arguments.data,mappings,{update_if_null_only = false}) />
			</cfloop>
		</cfif>
		<cfif bValid>
			<cfset courseList = arguments.data.COURSE_CD>
			<cfquery name="deletePrograms" datasource="#variables.datasource#">
				 DELETE FROM UG_TES_CONFIG_COURSES D WHERE D.TES_CONFIG_ID = '#arguments.data.TES_CONFIG_ID#'
			</cfquery>
			<cfloop  list="#courseList#" index="courseCD">
			<cfset arguments.data.COURSE_CD = courseCD >
			<cfset var mappings = {} />

				<cfset dbScoreEntity = EntityRegistry.initDBEntity("UG_TES_CONFIG_COURSES") />
				<cfset bValid = dbScoreEntity.save(arguments.data,mappings,{update_if_null_only = false}) />

			</cfloop>
		</cfif>
		<cfif bValid>
			<cfset sessionList = arguments.data.TRAINING_SESSION_CD>
			<cfquery name="deletePrograms" datasource="#variables.datasource#">
				 DELETE FROM TES_CONFIG_SESSIONS D WHERE D.TES_CONFIG_ID = '#arguments.data.TES_CONFIG_ID#'
			</cfquery>
			<cfloop  list="#sessionList#" index="sessionCd">
			<cfset arguments.data.TRAINING_SESSION_CD =sessionCd >
			<cfset var mappings = {} />
				<cfset dbScoreEntity = EntityRegistry.initDBEntity("TES_CONFIG_SESSIONS") />
				<cfset bValid = dbScoreEntity.save(arguments.data,mappings,{update_if_null_only = false}) />

			</cfloop>
		</cfif>

		<cfreturn bValid>
	</cffunction>
	<cffunction name="validate" access="public" output="false" returntype="boolean">
		<cfargument name="data" required="true" type="struct">
		<cfset var bValid = true>
			<cfset var validatorObj = application.wirebox.getInstance("EntityValidator@CORE")>
			<cfset bValid = validatorObj.validateData(entityCode = 'TES_CONFIG', data = arguments.data) >
		<cfreturn bValid>
	</cffunction>




	<cffunction name="delete" access="public" output="false" returntype="boolean">
		<cfargument name="data" required="true" type="struct">

		<cfset var bSuccess = true>
		<cfset var MsgBox = application.wirebox.getInstance(dsl="coldbox:myplugin:MsgBox").init(application.cbController)>

		<cfset var dbEntity = EntityRegistry.initDBEntity("TES_CONFIG")>
		<cfset bSuccess = dbEntity.delete(condition="DOC_SETUP_ID = #arguments.data.DOC_SETUP_ID#")>

		<cfif bSuccess>
		  	<cfset var dbEntity = EntityRegistry.initDBEntity("TES_CONFIG_SCORES")>
			<cfset bSuccess = dbEntity.delete(condition="DOC_SETUP_ID = #arguments.data.DOC_SETUP_ID#")>
			<cfset var dbEntity = EntityRegistry.initDBEntity("TES_CONFIG_COMPARISONS")>
			<cfset bSuccess = dbEntity.delete(condition="DOC_SETUP_ID = #arguments.data.DOC_SETUP_ID#")>
		</cfif>

		<cfreturn bSuccess>
	</cffunction>

	<cfscript>
		function saveScheduleFields (field_name,field,data){
			var params = arguments.field.params;
			var Interceptor = application.wirebox.getInstance('sis_core.model.blAutomation.Interceptor');
			var eventSchedule = application.wirebox.getInstance('sis_core.model.blAutomation.interceptorEventSchedule');
			var	interceptorGTW = application.wirebox.getInstance('sis_core.model.blAutomation.InterceptorGTW');

			/*delete*/
			if (isDefined('arguments.data.#params.fieldPrefix#schedule_data')){
				var scheduleData = DeserializeJSon(Evaluate('arguments.data.#params.fieldPrefix#schedule_data'));
				if (StructIsEmpty(scheduleData)){
					if(isDefined('#params.fieldPrefix#handler_config_schedule') and isNumeric(Evaluate('#params.fieldPrefix#handler_config_schedule'))){
						Interceptor.deleteHandlerConfig({"config_id" = Evaluate('#params.fieldPrefix#handler_config_schedule')});
						arguments.data['#field_name#'] ="";
					}
				}
			}
			/*save or update config*/
			var handlerDetails = InterceptorGTW.getHandlerByCode('#params.EVENT_CODE#');
			var configStruct = StructNew();
			configStruct.handlerCd = handlerDetails.handler_cd;
			if(isDefined('#params.fieldPrefix#handler_config_schedule') and isNumeric(Evaluate('#params.fieldPrefix#handler_config_schedule'))){
				configStruct.config_id = generatehandler_config_schedule;
			}
			configStruct.configData = '{"DOC_SETUP_ID":"#arguments.data.DOC_SETUP_ID#","instance_id":"#arguments.data.INSTANCE_ID#"}';
			configStruct.name = '#arguments.data.DOC_SETUP_NAME#_#arguments.data.DOC_SETUP_ID#';
			if (isDefined('arguments.data.#params.fieldPrefix#schedule_data')){
				var scheduleData = DeserializeJSon(Evaluate('arguments.data.#params.fieldPrefix#schedule_data'));
				if(!StructIsEmpty(scheduleData) && StructKeyExists(scheduleData, "scheduleDt")){
					if(StructKeyExists(scheduleData, 'valid') and  scheduleData.valid eq 'YES'){
						var configResult = interceptor.saveHandlerConfig(configStruct);
						scheduleData.parameters.handler_config_schedule = configResult.config_id;
						arguments.data['#field_name#'] = configResult.config_id;
						var eventResult = eventSchedule.saveSchedule(scheduleData);
					}
				}else{
					var eventResult = true;
				}
			}
		}
	</cfscript>

</cfcomponent>