<cfcomponent name="UG_TesReportFilter" code="QB_UG_TES_REPORT_FILTER" displayName="UG_TesReportFilter" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" label="" inject="coldbox:setting:datasource" scope="variables"/>
	<cfproperty name="SUPERVISOR_NAME"  type="string" label="Supervisor" id="SUPERVISOR_ID">
	<cfproperty name="TEACHING_LOCATION"  type="string" label="Teaching Location" id="LOCATION_ID" restriction_type="DOCUMENT_BUILDER_LOCATION">
	<cfproperty name="TEACHING_LOCATION_TYPE"  type="string" label="Teaching Location Type" id="LOCATION_TYPE_ID">
	<cfproperty name="COURSE_NAME"  type="string" label="Course" id="COURSE_CD" restriction_type="DOCUMENT_BUILDER_COURSE">
	<cfproperty name="TEMPLATE_NAME"  type="string" label="Template Name" id="TEMPLATE_ID">
	<cfproperty name="SETUP_STATUS"  type="string" label="Setup Status" id="SETUP_STATUS_ID">
	<cfproperty name="SETUP_NAME"  type="string" label="Setup Name" id="SETUP_ID">
	<cfproperty name="STATUS"  type="string" label="Status">
	<cfproperty name="PUBLISHED_DT"  type="date" label="Publish Date">


	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_FORM",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="SUPERVISOR_APPOINTMENT",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
		]>
		<cfreturn grants>
	</cffunction>

	<cfscript>

	public UG_TesReportFilter function init() {
		variables.args = arguments;
		super.init();
		return this;
	}


	public string function getDataProvider_SUPERVISOR_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
        				DISTINCT
                        A.USER_ID as idField,
                        A.USER_NAME as descField
                    FROM
                       DOCSETUPELIGIBLEUSERS_VIEW A
                    WHERE A.INSTANCE_CODE = 'UG_TES_REPORT'
                    ORDER BY UPPER(A.USER_NAME)";
        return _sql;
    }
	public string function getDataProvider_TEACHING_LOCATION() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT A.LOCATION_ID as idField,
        				   A.LOCATION_NAME as descField
						FROM  {pg_datasource}.RsLocation_VIEW A
							where EXISTS (SELECT 1 FROM  {fbx_datasource}.SUPERVISOR_APPOINTMENT B WHERE B.ORG_CD = a.location_id)
					ORDER BY UPPER(A.LOCATION_NAME)";
        return _sql;
    }
    public string function getDataProvider_TEACHING_LOCATION_TYPE() {
        var _sql = "SELECT 1 AS idField, 'Primary' AS descField FROM DUAL UNION SELECT 2 AS idField, 'Secondary' AS descField FROM DUAL";
        return _sql;
    }
    public string function getDataProvider_COURSE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
       var _sql = "SELECT DISTINCT
					    C.COURSE_CD as idField,
					    '['||C.COURSE_CODE||'] '|| C.COURSE_NAME as descField,
					    C.COURSE_NAME
					FROM
					     {fbx_datasource}.COURSE C,
					     {fbx_datasource}.CURRICULUM R
					WHERE
					    C.COURSE_CD = R.COURSE_CD
					ORDER BY C.COURSE_NAME ASC";
        return _sql;
    }
    public string function getDataProvider_TEMPLATE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.LETTER_TEMPLATE_CD as idField,
                        A.LETTER_TEMPLATE_NAME as descField
                    FROM
                       DocumentTemplate_VIEW A
                    WHERE INSTANCE_ID = 6
                    ORDER BY UPPER(A.LETTER_TEMPLATE_NAME)";
        return _sql;
    }
    public string function getDataProvider_SETUP_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
         				DISTINCT
                        A.DOC_SETUP_ID as idField,
                        A.DOC_SETUP_NAME as descField
                    FROM
                       DOCSETUPELIGIBLEUSERS_VIEW A
                    WHERE INSTANCE_ID = 6
                    ORDER BY UPPER(A.DOC_SETUP_NAME)";
        return _sql;
    }
    public string function getDataProvider_STATUS() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT 2 AS idField, 'Published' AS descField FROM DUAL UNION SELECT 1 AS idField, 'Not Published' AS descField FROM DUAL";
        return _sql;
    }
 	public string function getDataProvider_SETUP_STATUS() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT 2 AS idField, 'Ready' AS descField FROM DUAL UNION SELECT 1 AS idField, 'Not Ready' AS descField FROM DUAL";
        return _sql;
    }

    </cfscript>


</cfcomponent>