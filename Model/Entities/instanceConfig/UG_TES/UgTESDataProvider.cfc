<cfcomponent name="UgTESDataProvider" code="QB_UG_TES_DATA_PROVIDER" displayName="UgTESDataProvider" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" label="" inject="coldbox:setting:datasource" scope="variables"/>
	<cfproperty name="EVALUATION_LIST"  type="string">
	<cfproperty name="QUESTION_RATINGS_COUNT"  type="string">
	<cfproperty name="QUESTION_SCORES"  type="string">
	<cfproperty name="QUESTION_COMPARISONS"  type="string">
	<cfproperty name="TOTAL_SCORES"  type="string">
	<cfproperty name="COMMENTS"  type="string">
	<cfproperty name="DOCUMENT_DETAILS"  type="string">
	<cfproperty name="QUESTIONS_DEF"  type="string">
    <cfproperty name="RATING_GROUPS_DEF" type="string">
    <cfproperty name="RATING_SCALE_DEF"  type="string">
	<cfproperty name="SCORES_DEF"  type="string">

	<cfscript>

	public UgTESDataProvider function init() {
		variables.args = arguments;
		super.init();
		return this;
	}


	</cfscript>
	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_QUESTION",OBJECT_DATASOURCE=" {fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="UD_USER",OBJECT_DATASOURCE="{pg_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="EVAL_SUPERVISOR_USER",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="DEPARTMENT",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="DIVISION",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="EVAL_SUPERVISOR_APPOINTMENT",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
		]>
		<cfreturn grants>
	</cffunction>

	<cffunction name="getSQLS" access="public" output="true" returntype="struct">
		<cfset var sqls = StructNew()>
		<cfset evalListSql = ''>
		<cfset ratingCountSql = ''>
		<cfset scoresCountSql = ''>
		<cfset totalscoresSql = ''>
		<cfset comparisonsSql = ''>
		<cfset commentsSql 	  = ''>
	    <cfset ratingGroupsSql =''>
		<cfset questionDefinitionsSql = ''>
		<cfset ratingScalesSql = ''>
		<cfset scoresSql = ''>

		<cfsavecontent variable ="ratingGroupsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
						{% set data =[{rating_group_cd:1,rating_group_desc:'Rating Descriptin'}]%}
						{{macros.SQLFromArray(data)}}
				{%else %}
						SELECT DISTINCT
							    C.RATING_GROUP_CD,
							    C.RATING_GROUP_DESC,
						    	EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
							FROM
							    {fbx_datasource}.EVAL_QUESTION A,
							    {fbx_datasource}.RATING_SCALE_GROUP C,
							    (
							        SELECT
							            RATING_CD,
							            RATING_DESC,
							            RATING_GROUP_CD,
							            RATING_NR,
							            RATING_VALUE,
							            WEIGHT
							        FROM
							            {fbx_datasource}.RATING_SCALE
							        UNION
							        SELECT
							            0 AS RATING_CD,
							            'N/A' AS RATING_DESC,
							            RATING_GROUP_CD AS RATING_GROUP_CD,
							            0 AS RATING_NR,
							            '0' AS RATING_VALUE,
							            0 AS WEIGHT
							        FROM
							            {fbx_datasource}.RATING_SCALE_GROUP
							    ) D,
							    TES_CONFIG TC,
							    DOC_SETUP_ELIGIBLE_USERS EU
							WHERE
							        A.QUESTION_TYPE_CD = 1
							    AND
							        A.RATING_GROUP_CD = C.RATING_GROUP_CD
							    AND
							        C.RATING_GROUP_CD = D.RATING_GROUP_CD
							    AND EU.DOC_SETUP_ID = TC.DOC_SETUP_ID
							    AND
							        TC.EVAL_FORM_CD = A.EVAL_FORM_CD
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					   ORDER BY  RATING_GROUP_CD ASC
				 {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="questionDefinitionsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,question_desc:'Overall Teaching Effectiveness: Overall for this clinical experience, what is your opinion of the effectiveness of the instructor?',RATING_GROUP_CD:1},
								  {question_cd:2,question_desc:'Evaluation (if applicable): A variety of observation methods were utilized including case discussion and direct observation. Evaluation was fair, well constructed and provided in a timely manner. Feedback was received and was constructive and informative.',RATING_GROUP_CD:1}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
						DISTINCT
						B.RATING_GROUP_CD,
    					B.QUESTION_CD,
					    REPLACE(B.QUESTION_DESC,chr(34),chr(39)) as QUESTION_DESC,
					    B.QUESTION_TYPE_CD,
					    C.QUESTION_TYPE_DESC,
					    B.QUESTION_NR,
					    G.GROUP_NR,
						EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
					FROM
					    {fbx_datasource}.EVAL_FORM A,
					    {fbx_datasource}.EVAL_QUESTION B,
					    {fbx_datasource}.eval_group g,
					    {fbx_datasource}.QUESTION_TYPE C,
					    UG_QUESTIONS_MAPPING_VIEW TCQ,
						DOC_SETUP_ELIGIBLE_USERS EU
					WHERE
					        B.QUESTION_CD = TCQ.GROUP_QUESTION_ID
    					AND
					        A.EVAL_FORM_CD = B.EVAL_FORM_CD
					    AND
					        B.QUESTION_TYPE_CD = C.QUESTION_TYPE_CD
					    AND
					        C.QUESTION_TYPE_DESC = 'Rating Scale'
					  	AND B.GROUP_CD = G.GROUP_CD
					  	AND EU.DOC_SETUP_ID = TCQ.DOC_SETUP_ID
					  	{% if DATA.DOC_SETUP_ID is defined %}
							AND TCQ.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					    ORDER BY G.GROUP_NR ASC,B.QUESTION_NR ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="ratingScalesSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{rating_cd:1,rating_desc:'Poor',rating_nr:1,rating_value:1,weight:1,rating_group_cd:1},
								  {rating_cd:2,rating_desc:'Fair',rating_nr:2,rating_value:2,weight:2,rating_group_cd:1},
								  {rating_cd:3,rating_desc:'Good',rating_nr:3,rating_value:3,weight:3,rating_group_cd:1},
								  {rating_cd:4,rating_desc:'Satisfactory',rating_nr:4,rating_value:4,weight:4,rating_group_cd:1},
								  {rating_cd:5,rating_desc:'Excellent',rating_nr:5,rating_value:5,weight:5,rating_group_cd:1}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT DISTINCT
					    D.RATING_CD,
					    D.RATING_DESC,
					    D.RATING_NR,
					    D.RATING_VALUE,
					    D.WEIGHT,
					    C.RATING_GROUP_CD,
					    C.RATING_GROUP_DESC,
						EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
					FROM
					    {fbx_datasource}.EVAL_QUESTION A,
					    {fbx_datasource}.RATING_SCALE_GROUP C,
					    (
					        SELECT
					            RATING_CD,
					            RATING_DESC,
					            RATING_GROUP_CD,
					            RATING_NR,
					            RATING_VALUE,
					            WEIGHT
					        FROM
					            {fbx_datasource}.RATING_SCALE
					        UNION
					        SELECT
					            0 AS RATING_CD,
					            'N/A' AS RATING_DESC,
					            RATING_GROUP_CD AS RATING_GROUP_CD,
					            NULL AS RATING_NR,
					            '' AS RATING_VALUE,
					            0 AS WEIGHT
					        FROM
					            {fbx_datasource}.RATING_SCALE_GROUP
					    ) D,
					    TES_CONFIG TC,
						DOC_SETUP_ELIGIBLE_USERS EU
					WHERE
					        A.QUESTION_TYPE_CD = 1
					    AND
					        A.EVAL_FORM_CD = TC.EVAL_FORM_CD
					    AND
					        A.RATING_GROUP_CD = C.RATING_GROUP_CD
					    AND
					        C.RATING_GROUP_CD = D.RATING_GROUP_CD
					   	AND EU.DOC_SETUP_ID = TC.DOC_SETUP_ID
						  	{% if DATA.DOC_SETUP_ID is defined %}
								AND TC.DOC_SETUP_ID  in ({{DATA.DOC_SETUP_ID}})
					    	{% endif %}
						   ORDER BY  RATING_NR ASC,RATING_GROUP_CD ASC,D.WEIGHT ASC
			 {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="scoresSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{score_type_id:1,score_type_desc:'Mean Value',score_type_code:'MEAN_VALUE'},
								  {score_type_id:2,score_type_desc:'Median Value',score_type_code:'MEDIAN_VALUE'},
								  {score_type_id:3,score_type_desc:'Standard Deviation Value',score_type_code:'SDV'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
					SELECT
					    SC.SCORE_TYPE_ID,
					    SC.SCORE_TYPE_DESC,
					    SC.SCORE_TYPE_CODE,
						EU.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
					FROM
					    TES_SCORE SC,
					    TES_CONFIG_SCORES TCS,
					    TES_CONFIG TC,
						DOC_SETUP_ELIGIBLE_USERS EU
					WHERE
					        SC.SCORE_TYPE_ID = TCS.SCORE_TYPE_ID
					    AND
					        TC.TES_CONFIG_ID = TCS.TES_CONFIG_ID
					    AND EU.DOC_SETUP_ID = TC.DOC_SETUP_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
							AND TC.DOC_SETUP_ID  in ({{DATA.DOC_SETUP_ID}})
					    {% endif %}
					    order by SCORE_TYPE_ID ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>

		<cfsavecontent variable ="evalListSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,rating_cd:1,weight:1,group_count:3},
								  {question_cd:1,rating_cd:2,weight:2,group_count:0},
								  {question_cd:1,rating_cd:3,weight:3,group_count:2},
								  {question_cd:1,rating_cd:4,weight:4,group_count:1},
								  {question_cd:1,rating_cd:5,weight:5,group_count:5},
								  {question_cd:2,rating_cd:1,weight:1,group_count:0},
								  {question_cd:2,rating_cd:2,weight:2,group_count:1},
								  {question_cd:2,rating_cd:3,weight:3,group_count:2},
								  {question_cd:2,rating_cd:4,weight:4,group_count:3},
								  {question_cd:2,rating_cd:5,weight:5,group_count:2}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH USED_EVALUATIONS AS (
					  SELECT
						    A.DOC_SNAPSHOT_CD,
						    A.USER_ID,
						    A.EVALUATION_ID AS USER_EVAL_CD
						FROM
						    DOC_SNAPSHOT_EVALUATIONS A,
						    DOC_SNAPSHOT_BASE B,
						    DOC_INSTANCE C
						WHERE
						        A.DOC_SNAPSHOT_CD = B.DOC_SNAPSHOT_CD
						    AND
						        B.INSTANCE_ID = C.INSTANCE_ID
						    AND B.INSTANCE_ID = 6
				),
				MAPPING_QUESTIONS AS (
					     SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM UG_QUESTIONS_MAPPING_VIEW
							WHERE 1=1
								{% if DATA.DOC_SETUP_ID is defined %}
								    AND DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                		{% endif %}
					),
				EVALUATIONS AS (
				   SELECT
				          A.USER_EVAL_CD AS EVALUATION_ID,
                		  U.USER_ID||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS DOC_SNAPSHOT_CD
				    FROM
				           {fbx_datasource}.s4y_eval_schedule b,
					      S4Y_USER_EVAL A,
					        (
					            SELECT DISTINCT
					                UE.USER_EVAL_CD,
					                UE.COMPLETE_DT
					            FROM
					                 {fbx_datasource}.S4Y_USER_EVAL UE,
					                 {fbx_datasource}.S4Y_USER_ANSWER D,
					                 {fbx_datasource}.EVAL_QUESTION E
					            WHERE
					                    UE.USER_EVAL_CD = D.USER_EVAL_CD
					                AND
					                    D.QUESTION_CD = E.QUESTION_CD
					                AND (
					                    E.QUESTION_TYPE_CD = 1
					                )
					        ) Q,
					         {fbx_datasource}.EVAL_SUPERVISOR S,
					         {fbx_datasource}.S_USER U,
					        UG_HIERARCHY_CONFIG_COURSES TCC,
					        DOC_SETUP D,
					        ( SELECT
                				G.EVALUATION_ID AS USER_EVAL_CD,
				            	G.DOC_SETUP_ID
				            	FROM
				          		DOC_SETUP_UNPUBLISHED_EVALS G
				          	WHERE 1=1
				          		AND G.PROCESS_ID = '{{DATA.PROCESSID}}'
	                      	{% if DATA.DOC_SETUP_ID is defined %}
			    				AND G.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
             				{% endif %}
						    {% if DATA.USER_ID is defined %}
								AND G.USER_ID in ({{DATA.USER_ID}})
					    	{% endif %}
					            AND NOT EXISTS (SELECT 1 FROM USED_EVALUATIONS WHERE USER_EVAL_CD = G.EVALUATION_ID )
					        ) EVALS,
				        DOC_SETUP_ELIGIBLE_USERS EU,
				        TES_CONFIG TC,
				        UG_QUESTIONS_MAPPING_VIEW TCQ,
				        {fbx_datasource}.EVAL_QUESTION EQ,
        				UG_USER_UNPUBLISHED_EVALS UUE
				    WHERE
				           A.EVAL_SCHEDULE_CD = B.EVAL_SCHEDULE_CD
				           AND
				        	A.EVAL_FORM_CD = EQ.EVAL_FORM_CD
				        AND (
				                NVL(
				                    EQ.MAIN_QUESTION_CD,
				                    EQ.QUESTION_CD
				                ) = TCQ.QUESTION_ID
				            OR
				                NVL(
				                    EQ.MAIN_QUESTION_CD,
				                    EQ.QUESTION_CD
				                ) = TCQ.GROUP_QUESTION_ID
				        )
					        AND (
					            B.COURSE_CD = TCC.COURSE_CD
					        ) AND (
					            S.SUPERVISOR_ID = U.SUPERVISOR_ID
					        ) AND (
					            A.ADDRESSEE_ID = U.USER_ID
					        ) AND (
					            A.USER_EVAL_CD = Q.USER_EVAL_CD (+)
					        ) AND
					            TCC.TES_CONFIG_ID = TC.TES_CONFIG_ID
					        AND
					            TC.DOC_SETUP_ID = D.DOC_SETUP_ID
					        AND TCQ.TES_CONFIG_ID = TC.TES_CONFIG_ID
					        AND A.USER_EVAL_CD = EVALS.USER_EVAL_CD
					     	AND A.COMPLETE_DT IS NOT NULL
					     	AND U.USER_ID = EU.USER_ID
					     	AND EU.DOC_SETUP_ID=EVALS.DOC_SETUP_ID
					    {% if DATA.DOC_SETUP_ID is defined %}
					    	AND EU.DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                {% endif %}
					    {% if DATA.USER_ID is defined %}
							AND EU.USER_ID in ({{DATA.USER_ID}})
					    {% endif %}
					    AND a.user_eval_cd not in (select distinct user_eval_cd from used_evaluations)
					    AND EU.USER_ID = UUE.USER_ID
					    AND EU.DOC_SETUP_ID = UUE.DOC_SETUP_ID
       					AND TC.DOC_SETUP_ID = EU.DOC_SETUP_ID
       					AND TC.MIN_EVALS <=UUE.DISTINCT_EVALUATORS
				)
				SELECT
					 evaluation_id,
                     doc_snapshot_cd
				FROM EVALUATIONS
					GROUP BY
						evaluation_id,
                    	doc_snapshot_cd
				   ORDER BY  evaluation_id
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="ratingCountSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,rating_cd:1,weight:1,group_count:3},
								  {question_cd:1,rating_cd:2,weight:2,group_count:0},
								  {question_cd:1,rating_cd:3,weight:3,group_count:2},
								  {question_cd:1,rating_cd:4,weight:4,group_count:1},
								  {question_cd:1,rating_cd:5,weight:5,group_count:5},
								  {question_cd:2,rating_cd:1,weight:1,group_count:0},
								  {question_cd:2,rating_cd:2,weight:2,group_count:1},
								  {question_cd:2,rating_cd:3,weight:3,group_count:2},
								  {question_cd:2,rating_cd:4,weight:4,group_count:3},
								  {question_cd:2,rating_cd:5,weight:5,group_count:2}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH evaluations AS (
				    SELECT
				        doc_setup_id,
				        doc_snapshot_cd,
				        user_eval_cd
				    FROM
				        (
				            SELECT
				                doc_snapshot_cd,
				                doc_setup_id,
				                regexp_substr(
				                    evaluation_list,
				                    '[^,]+',
				                    1,
				                    evaluations.column_value
				                ) AS user_eval_cd
				            FROM
				                (
				                    SELECT
				                        a.doc_snapshot_cd,
				                        a.doc_setup_id,
				                        (
				                            SELECT
				                                string_agg(evaluation_id)
				                            FROM
				                                TABLE ( pljson_table.json_table(
				                                    d.value,
				                                    pljson_varray('[*].EVALUATION_ID'),
				                                    pljson_varray('EVALUATION_ID'),
				                                    table_mode   => 'nested'
				                                ) )
				                        ) evaluation_list
				                    FROM
				                        doc_snapshot_base a,
				                        doc_snapshot_data d
					where a.doc_setup_id in ({{DATA.DOC_SETUP_ID}})
					and a.process_id='{{DATA.PROCESSID}}'
					AND D.KEY               = 'EVALUATION_LIST'
					and a.doc_snapshot_cd=d.doc_snapshot_cd),
				    TABLE ( CAST(MULTISET(SELECT LEVEL FROM DUAL CONNECT BY LEVEL <= LENGTH(REGEXP_REPLACE(EVALUATION_LIST,'[^,]+') ) + 1) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
					)
					GROUP BY user_eval_cd,doc_snapshot_cd,doc_setup_id
					),
					MAPPING_QUESTIONS AS (
					        SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM UG_QUESTIONS_MAPPING_VIEW
							WHERE 1=1
								{% if DATA.DOC_SETUP_ID is defined %}
								    AND DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                		{% endif %}
					),
					 QUESTIONS AS (
				       SELECT
					        TCQ.GROUP_QUESTION_ID AS QUESTION_CD,
					        D.RATING_CD,
					        D.RATING_NR,
					        D.WEIGHT,
					            CASE
					                WHEN SAFE_TO_NUMBER(UA.ANSWER_TEXT) = D.RATING_CD THEN 1
					                ELSE 0
					            END
					        AS ANSWERED,
					        UA.USER_ANSWER_CD,
					        EV.ADDRESSEE_ID
					         || '_'
					         || EVL.DOC_SETUP_ID
					         || '_{{DATA.PROCESSID}}' AS DOC_SNAPSHOT_CD
				     FROM
					         {fbx_datasource}.EVAL_QUESTION A,
					         {fbx_datasource}.S4Y_USER_ANSWER UA,
					         {fbx_datasource}.RATING_SCALE_GROUP C,
					        (
					            SELECT
					                RATING_CD,
					                RATING_DESC,
					                RATING_GROUP_CD,
					                RATING_NR,
					                RATING_VALUE,
					                WEIGHT
					            FROM
					                 {fbx_datasource}.RATING_SCALE
					            UNION
					            SELECT
					                0 AS RATING_CD,
					                'N/A' AS RATING_DESC,
					                RATING_GROUP_CD AS RATING_GROUP_CD,
					                NULL AS RATING_NR,
					                '0' AS RATING_VALUE,
					                0 AS WEIGHT
					            FROM
					                 {fbx_datasource}.RATING_SCALE_GROUP
					        ) D,
					         {fbx_datasource}.S4Y_USER_EVAL EV,
					        EVALUATIONS EVL,
					        MAPPING_QUESTIONS TCQ,
					        DOC_SETUP_ELIGIBLE_USERS EU
					    WHERE
					            A.QUESTION_TYPE_CD = 1
					        AND
					            A.EVAL_FORM_CD = EV.EVAL_FORM_CD
					        AND
					            UA.USER_EVAL_CD = EV.USER_EVAL_CD
					        AND
					            UA.QUESTION_CD = A.QUESTION_CD
					        AND
					            A.RATING_GROUP_CD = C.RATING_GROUP_CD
					        AND
					            C.RATING_GROUP_CD = D.RATING_GROUP_CD
					        AND
					            EV.USER_EVAL_CD = EVL.USER_EVAL_CD
					        AND EV.ADDRESSEE_ID = EU.USER_ID
					        AND EU.DOC_SETUP_ID  = TCQ.DOC_SETUP_ID
					        {% if DATA.USER_ID is defined %}
								AND EU.USER_ID in ({{DATA.USER_ID}})
							{% endif %}
							{% if DATA.DOC_SETUP_ID is defined %}
								AND EU.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
							{% endif %}
					        AND (
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID
						        OR
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID
						    )

					    ORDER BY A.QUESTION_DESC
					),
					DISTINCT_QUESTIONS AS (
						SELECT DISTINCT * FROM QUESTIONS
					)
					SELECT
					    QUESTION_CD,
					    RATING_CD,
					    RATING_NR,
					    WEIGHT,
					    SUM(ANSWERED) AS GROUP_COUNT,
					    DOC_SNAPSHOT_CD
					FROM
					    DISTINCT_QUESTIONS
					GROUP BY
					    QUESTION_CD,
					    RATING_CD,
					    RATING_NR,
					    WEIGHT,
					    DOC_SNAPSHOT_CD
					ORDER BY
					    QUESTION_CD ASC,
					    RATING_NR ASC,
					    WEIGHT ASC
				{% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="scoresCountSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,MEAN_VALUE:3.12,MEDIAN_VALUE:3,SDV:0.86},
								  {question_cd:2,MEAN_VALUE:1,MEDIAN_VALUE:1,SDV:3}
								  ]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH EVALUATIONS AS (
				   SELECT
				        doc_setup_id,
				        doc_snapshot_cd,
				        user_eval_cd
				    FROM
				        (
				            SELECT
				                doc_snapshot_cd,
				                doc_setup_id,
				                regexp_substr(
				                    evaluation_list,
				                    '[^,]+',
				                    1,
				                    evaluations.column_value
				                ) AS user_eval_cd
				            FROM
				                (
				                    SELECT
				                        a.doc_snapshot_cd,
				                        a.doc_setup_id,
				                        (
				                            SELECT
				                                string_agg(evaluation_id)
				                            FROM
				                                TABLE ( pljson_table.json_table(
				                                    d.value,
				                                    pljson_varray('[*].EVALUATION_ID'),
				                                    pljson_varray('EVALUATION_ID'),
				                                    table_mode   => 'nested'
				                                ) )
				                        ) evaluation_list
				                    FROM
				                        doc_snapshot_base a,
				                        doc_snapshot_data d
					where a.doc_setup_id in ({{DATA.DOC_SETUP_ID}})
					and a.process_id='{{DATA.PROCESSID}}'
					AND D.KEY               = 'EVALUATION_LIST'
					and a.doc_snapshot_cd=d.doc_snapshot_cd),
				    TABLE ( CAST(MULTISET(SELECT LEVEL FROM DUAL CONNECT BY LEVEL <= LENGTH(REGEXP_REPLACE(EVALUATION_LIST,'[^,]+') ) + 1) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
					)
					GROUP BY user_eval_cd,doc_snapshot_cd,doc_setup_id
					),
					MAPPING_QUESTIONS AS (
					        SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM UG_QUESTIONS_MAPPING_VIEW
							WHERE 1=1
								{% if DATA.DOC_SETUP_ID is defined %}
								    AND DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                		{% endif %}
					),
				QUESTIONS AS (
				        SELECT
					        TCQ.GROUP_QUESTION_ID AS QUESTION_CD,
					        D.RATING_CD,
					        D.RATING_NR,
					        SAFE_TO_NUMBER(D.RATING_VALUE) AS RATING_VALUE,
					        D.WEIGHT,
					            CASE
					                WHEN SAFE_TO_NUMBER(UA.ANSWER_TEXT) = D.RATING_CD THEN 1
					                ELSE 0
					            END
					        AS ANSWERED,
					        UA.USER_ANSWER_CD,
					        EV.ADDRESSEE_ID
					         || '_'
					         || EVL.DOC_SETUP_ID
					         || '_{{DATA.PROCESSID}}' AS DOC_SNAPSHOT_CD
				     FROM
					         {fbx_datasource}.EVAL_QUESTION A,
					         {fbx_datasource}.S4Y_USER_ANSWER UA,
					         {fbx_datasource}.RATING_SCALE_GROUP C,
					        (
					            SELECT
					                RATING_CD,
					                RATING_DESC,
					                RATING_GROUP_CD,
					                RATING_NR,
					                SAFE_TO_NUMBER(RATING_VALUE) AS RATING_VALUE,
					                WEIGHT
					            FROM
					                 {fbx_datasource}.RATING_SCALE
					            UNION
					            SELECT
					                0 AS RATING_CD,
					                'N/A' AS RATING_DESC,
					                RATING_GROUP_CD AS RATING_GROUP_CD,
					                NULL AS RATING_NR,
					                0 AS RATING_VALUE,
					                0 AS WEIGHT
					            FROM
					                 {fbx_datasource}.RATING_SCALE_GROUP
					        ) D,
					         {fbx_datasource}.S4Y_USER_EVAL EV,
					        EVALUATIONS EVL,
					        MAPPING_QUESTIONS TCQ,
					        DOC_SETUP_ELIGIBLE_USERS EU
					    WHERE
					            A.QUESTION_TYPE_CD = 1
					        AND
					            A.EVAL_FORM_CD = EV.EVAL_FORM_CD
					        AND
					            UA.USER_EVAL_CD = EV.USER_EVAL_CD
					        AND
					            UA.QUESTION_CD = A.QUESTION_CD
					        AND
					            A.RATING_GROUP_CD = C.RATING_GROUP_CD
					        AND
					            C.RATING_GROUP_CD = D.RATING_GROUP_CD
					        AND
					            EV.USER_EVAL_CD = EVL.USER_EVAL_CD
					        AND EV.ADDRESSEE_ID = EU.USER_ID
					        AND EU.DOC_SETUP_ID  = TCQ.DOC_SETUP_ID
					        {% if DATA.USER_ID is defined %}
								AND EU.USER_ID in ({{DATA.USER_ID}})
							{% endif %}
							{% if DATA.DOC_SETUP_ID is defined %}
								AND EU.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
							{% endif %}
					        AND (
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID
						        OR
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID
						    )

					    ORDER BY A.QUESTION_DESC
				),
				DISTINCT_QUESTIONS AS (
						SELECT DISTINCT * FROM QUESTIONS
				) SELECT
				    QUESTION_CD,
				    doc_snapshot_cd,
				    TO_CHAR(AVG(TO_NUMBER(RATING_VALUE)), '0.90') AS MEAN_VALUE,
				    MEDIAN(TO_NUMBER(RATING_VALUE)) AS MEDIAN_VALUE,
				    TO_CHAR(STDDEV(TO_NUMBER(RATING_VALUE)), '0.90') AS SDV
				FROM
				    DISTINCT_QUESTIONS
				WHERE
				    ANSWERED = 1
				    AND safe_to_number(RATING_VALUE)>0
				GROUP BY
				doc_snapshot_cd,
				QUESTION_CD
			  {% endif %}
			</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="totalscoresSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{NR_EVALS:7,NR_STUDENTS:4,TES_SCORE:3.86,MIN_START_DT:'05-Mar-17',MAX_END_DT:'05-Jun-17',MIN_COMPLETION_DT:'05-Apr-17',MAX_COMPLETION_DT:'05-May-17'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH EVALUATIONS AS (
					 SELECT
				        doc_setup_id,
				        doc_snapshot_cd,
				        user_eval_cd
				    FROM
				        (
				            SELECT
				                doc_snapshot_cd,
				                doc_setup_id,
				                regexp_substr(
				                    evaluation_list,
				                    '[^,]+',
				                    1,
				                    evaluations.column_value
				                ) AS user_eval_cd
				            FROM
				                (
				                    SELECT
				                        a.doc_snapshot_cd,
				                        a.doc_setup_id,
				                        (
				                            SELECT
				                                string_agg(evaluation_id)
				                            FROM
				                                TABLE ( pljson_table.json_table(
				                                    d.value,
				                                    pljson_varray('[*].EVALUATION_ID'),
				                                    pljson_varray('EVALUATION_ID'),
				                                    table_mode   => 'nested'
				                                ) )
				                        ) evaluation_list
				                    FROM
				                        doc_snapshot_base a,
				                        doc_snapshot_data d
					where a.doc_setup_id in ({{DATA.DOC_SETUP_ID}})
					and a.process_id='{{DATA.PROCESSID}}'
					AND D.KEY               = 'EVALUATION_LIST'
					and a.doc_snapshot_cd=d.doc_snapshot_cd),
				    TABLE ( CAST(MULTISET(SELECT LEVEL FROM DUAL CONNECT BY LEVEL <= LENGTH(REGEXP_REPLACE(EVALUATION_LIST,'[^,]+') ) + 1) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
					)
					GROUP BY user_eval_cd,doc_snapshot_cd,doc_setup_id
					),
					MAPPING_QUESTIONS AS (
					       SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM UG_QUESTIONS_MAPPING_VIEW
							WHERE 1=1
								{% if DATA.DOC_SETUP_ID is defined %}
								    AND DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                		{% endif %}
					),
				QUESTIONS AS (
				     SELECT
				     		EV.USER_EVAL_CD,
				     		UA.USER_ID AS TRAINEE_ID,
					        TCQ.GROUP_QUESTION_ID AS QUESTION_CD,
					        D.RATING_CD,
					        D.RATING_NR,
					        SAFE_TO_NUMBER(D.RATING_VALUE) AS RATING_VALUE,
					        D.WEIGHT,
					            CASE
					                WHEN SAFE_TO_NUMBER(UA.ANSWER_TEXT) = D.RATING_CD THEN 1
					                ELSE 0
					            END
					        AS ANSWERED,
					        UA.USER_ANSWER_CD,
					        EV.COMPLETE_DT,
					        ES.END_DT,
					        ES.START_DT,
					        EV.ADDRESSEE_ID
					         || '_'
					         || EVL.DOC_SETUP_ID
					         || '_{{DATA.PROCESSID}}' AS DOC_SNAPSHOT_CD
				     FROM
					         {fbx_datasource}.EVAL_QUESTION A,
					         {fbx_datasource}.S4Y_USER_ANSWER UA,
					         {fbx_datasource}.RATING_SCALE_GROUP C,
					        (
					            SELECT
					                RATING_CD,
					                RATING_DESC,
					                RATING_GROUP_CD,
					                RATING_NR,
					                SAFE_TO_NUMBER(RATING_VALUE) AS RATING_VALUE,
					                WEIGHT
					            FROM
					                 {fbx_datasource}.RATING_SCALE
					            UNION
					            SELECT
					                0 AS RATING_CD,
					                'N/A' AS RATING_DESC,
					                RATING_GROUP_CD AS RATING_GROUP_CD,
					                0 AS RATING_NR,
					                0 AS RATING_VALUE,
					                0 AS WEIGHT
					            FROM
					                 {fbx_datasource}.RATING_SCALE_GROUP
					        ) D,
					         {fbx_datasource}.S4Y_USER_EVAL EV,
					         {fbx_datasource}.S4Y_EVAL_SCHEDULE ES,
					        EVALUATIONS EVL,
					        MAPPING_QUESTIONS TCQ,
					        DOC_SETUP_ELIGIBLE_USERS EU
					    WHERE
					            A.QUESTION_TYPE_CD = 1
					        AND
					            A.EVAL_FORM_CD = EV.EVAL_FORM_CD
					        AND
					            UA.USER_EVAL_CD = EV.USER_EVAL_CD
					        AND
					            UA.QUESTION_CD = A.QUESTION_CD
					        AND
					            A.RATING_GROUP_CD = C.RATING_GROUP_CD
					        AND
					            C.RATING_GROUP_CD = D.RATING_GROUP_CD
					        AND
					            EV.USER_EVAL_CD = EVL.USER_EVAL_CD
					        AND EV.ADDRESSEE_ID = EU.USER_ID
					        AND EU.DOC_SETUP_ID  = TCQ.DOC_SETUP_ID
					        {% if DATA.USER_ID is defined %}
								AND EU.USER_ID in ({{DATA.USER_ID}})
							{% endif %}
							{% if DATA.DOC_SETUP_ID is defined %}
								AND EU.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
							{% endif %}
					        AND (
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID
						        OR
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID
						    )
						    AND EV.EVAL_SCHEDULE_CD = ES.EVAL_SCHEDULE_CD
					    ORDER BY A.QUESTION_DESC
				),
				DISTINCT_QUESTIONS AS (
						SELECT DISTINCT * FROM QUESTIONS
				) SELECT
				    COUNT( DISTINCT USER_EVAL_CD) AS NR_EVALS,
				    COUNT( DISTINCT TRAINEE_ID) AS NR_STUDENTS,
				 	TO_CHAR(MIN(START_DT),'dd-Mon-yyyy') AS MIN_START_DT,
				    TO_CHAR(MAX(END_DT),'dd-Mon-yyyy') AS MAX_END_DT,
				    TO_CHAR(MIN(COMPLETE_DT),'dd-Mon-yyyy') AS MIN_COMPLETION_DT,
 					TO_CHAR(MAX(COMPLETE_DT),'dd-Mon-yyyy') AS MAX_COMPLETION_DT,
				   	TO_CHAR(AVG(NULLIF(TO_NUMBER(RATING_VALUE),0)), '0.90') AS TES_SCORE,
				   	DOC_SNAPSHOT_CD
				FROM
				    DISTINCT_QUESTIONS
				WHERE ANSWERED=1
				    AND safe_to_number(RATING_VALUE)>0
				group by
				DOC_SNAPSHOT_CD
				{% endif %}
		</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="comparisonsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,ALL_TEACHERS_MEAN_VALUE:3.12,ALL_TEACHERS_MEDIAN_VALUE:3,ALL_TEACHERS_SDV:0.86},
								  {question_cd:2,ALL_TEACHERS_MEAN_VALUE:1,ALL_TEACHERS_MEDIAN_VALUE:1,ALL_TEACHERS_SDV:3}
								  ]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				WITH MAPPING_QUESTIONS AS (
					    SELECT
       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM UG_QUESTIONS_MAPPING_VIEW
							WHERE 1=1
								{% if DATA.DOC_SETUP_ID is defined %}
								    AND DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
		                		{% endif %}
					),
				 ELIGIBLE_USERS AS (
				 	SELECT
				 		EU.DOC_SETUP_ID,
				 		EU.USER_ID
				 	FROM
				 		DOC_SETUP_ELIGIBLE_USERS EU
				 	WHERE 1=1
				 	{% if DATA.USER_ID is defined %}
						AND EU.USER_ID in ({{DATA.USER_ID}})
					{% endif %}
					{% if DATA.DOC_SETUP_ID is defined %}
						AND EU.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					{% endif %}
					)
				 ,QUESTIONS AS (
				   SELECT
					          TCQ.GROUP_QUESTION_ID AS QUESTION_CD,
				        D.RATING_CD,
					    SAFE_TO_NUMBER(D.RATING_VALUE) AS RATING_VALUE,
				        D.WEIGHT,
				            CASE
				                WHEN safe_to_number(UA.ANSWER_TEXT) = D.RATING_CD THEN 1
				                ELSE 0
				            END
				        AS ANSWERED,
				        UA.USER_ANSWER_CD,
				        EU.USER_ID,
               			EU.DOC_SETUP_ID
				     FROM
					         {fbx_datasource}.EVAL_QUESTION A,
					         {fbx_datasource}.S4Y_USER_ANSWER UA,
					         {fbx_datasource}.RATING_SCALE_GROUP C,
					        (
					            SELECT
					                RATING_CD,
					                RATING_DESC,
					                RATING_GROUP_CD,
					                RATING_NR,
					                SAFE_TO_NUMBER(RATING_VALUE) AS RATING_VALUE,
					                WEIGHT
					            FROM
					                 {fbx_datasource}.RATING_SCALE
					            UNION
					            SELECT
					                0 AS RATING_CD,
					                'N/A' AS RATING_DESC,
					                RATING_GROUP_CD AS RATING_GROUP_CD,
					                NULL AS RATING_NR,
					                0 AS RATING_VALUE,
					                0 AS WEIGHT
					            FROM
					                 {fbx_datasource}.RATING_SCALE_GROUP
					        ) D,
					         {fbx_datasource}.S4Y_USER_EVAL EV,
					         DOC_SETUP_ELIGIBLE_EVALS EVALS,
					         MAPPING_QUESTIONS TCQ,
					         ELIGIBLE_USERS EU
					    WHERE
					            A.QUESTION_TYPE_CD = 1
					        AND
					            A.EVAL_FORM_CD = EV.EVAL_FORM_CD
					        AND
					            UA.USER_EVAL_CD = EV.USER_EVAL_CD
					        AND EVALS.EVALUATION_ID = EV.USER_EVAL_CD
					        AND EVALS.DOC_SETUP_ID = TCQ.DOC_SETUP_ID
				    		AND EVALS.PROCESS_ID = '{{DATA.PROCESSID}}'
				    		AND EVALS.TRAINING_SESSION_CD IN (select U.TRAINING_SESSION_CD from DOC_SETUP_UNPUBLISHED_EVALS U WHERE U.PROCESS_ID = '{{DATA.PROCESSID}}' AND U.user_id = eU.user_id)
					        AND
					            UA.QUESTION_CD = A.QUESTION_CD
					        AND
					            A.RATING_GROUP_CD = C.RATING_GROUP_CD
					        AND
					            C.RATING_GROUP_CD = D.RATING_GROUP_CD
					        AND (
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID
						        OR
						            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID
						    )

					    ORDER BY A.QUESTION_DESC
					),DISTINCT_QUESTIONS AS (
						SELECT DISTINCT * FROM QUESTIONS
					) SELECT
				    QUESTION_CD,
				    TO_CHAR(AVG(TO_NUMBER(RATING_VALUE)), '0.90') AS ALL_TEACHERS_MEAN_VALUE,
				    MEDIAN(TO_NUMBER(RATING_VALUE)) AS ALL_TEACHERS_MEDIAN_VALUE,
				    TO_CHAR(STDDEV(TO_NUMBER(RATING_VALUE)), '0.90') AS ALL_TEACHERS_SDV,
				    user_id||'_'||DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
				FROM
				    DISTINCT_QUESTIONS
				WHERE
				    ANSWERED = 1
				    AND safe_to_number(RATING_VALUE)>0
				GROUP BY
				QUESTION_CD,
				user_id,
				DOC_SETUP_ID
				{% endif %}
		</cfoutput>
		</cfsavecontent>
		<cfsavecontent variable ="commentsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{question_cd:1,ANSWER_COMMENTS:' Nice overview with good interactivity and great discussion around writing orders clearly and avoiding mistakes. ',USER_ANSWER_CD:3},
								  {question_cd:2,ANSWER_COMMENTS:'Enjoyed working with Dr. Doe.He had a wealth of knowledge and was also responsive to my questions.',USER_ANSWER_CD:3}
								  ]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
				SELECT
					Y.QUESTION_CD,
            		Y.QUESTION_DESC,
            		Y.QUESTION_DESC_SHORT,
            		Y.QUESTION_NR,
            		Y.USER_ANSWER_CD,
           			Y.DOC_SNAPSHOT_CD,
    				(SELECT clobEscape(ANSWER_TEXT) FROM  {fbx_datasource}.S4Y_USER_ANSWER XX WHERE XX.USER_ANSWER_CD =Y.USER_ANSWER_CD) AS ANSWER_COMMENTS
				FROM (
					WITH mapping_questions AS (
					     SELECT
	       						DOC_SETUP_ID,TES_CONFIG_ID,GROUP_QUESTION_ID,QUESTION_ID FROM UG_QUESTIONS_MAPPING_VIEW
								WHERE 1=1
									{% if DATA.DOC_SETUP_ID is defined %}
									    AND DOC_SETUP_ID in	({{DATA.DOC_SETUP_ID}})
			                		{% endif %}
					), QUESTIONS AS (
					     SELECT
					        A.QUESTION_CD,
							A.QUESTION_DESC,
							A.QUESTION_DESC_SHORT,
							A.QUESTION_NR,
					        UA.USER_ANSWER_CD,
					        eU.user_id||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd
					    FROM
					             {fbx_datasource}.EVAL_QUESTION A,
						         {fbx_datasource}.S4Y_USER_ANSWER UA,
						         {fbx_datasource}.S4Y_USER_EVAL EV,
						        MAPPING_QUESTIONS TCQ,
						        TES_CONFIG TC,
		    					DOC_SETUP_ELIGIBLE_USERS EU,
		    					DOC_SETUP_ELIGIBLE_EVALS EVALS
					    WHERE
					              A.QUESTION_TYPE_CD = 8
						        AND
						            A.EVAL_FORM_CD = EV.EVAL_FORM_CD
						        AND
						            UA.USER_EVAL_CD = EV.USER_EVAL_CD
						        AND
						            UA.QUESTION_CD = A.QUESTION_CD
						        AND TC.SHOW_COMMENTS='Y'
						        AND (
							            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.QUESTION_ID
							        OR
							            NVL(A.MAIN_QUESTION_CD,A.QUESTION_CD) = TCQ.GROUP_QUESTION_ID
							    )
						    AND EU.DOC_SETUP_ID = TC.DOC_SETUP_ID
						    AND EVALS.EVALUATION_ID = EV.USER_EVAL_CD
						    AND EVALS.DOC_SETUP_ID = TCQ.DOC_SETUP_ID
						    AND EVALS.PROCESS_ID = '{{DATA.PROCESSID}}'
        					AND EVALS.TRAINING_SESSION_CD IN (select U.TRAINING_SESSION_CD from DOC_SETUP_UNPUBLISHED_EVALS U WHERE U.PROCESS_ID = '{{DATA.PROCESSID}}' AND U.user_id = eU.user_id)
						    AND EV.ADDRESSEE_ID = EU.USER_ID
					  		{% if DATA.DOC_SETUP_ID is defined %}
								AND TC.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
						    {% endif %}
						    {% if DATA.USER_ID is defined %}
								AND EU.USER_ID in ({{DATA.USER_ID}})
							{% endif %}
					    ORDER BY A.QUESTION_DESC
					) SELECT
					 	DISTINCT
						    QUESTION_CD,
							QUESTION_DESC,
							QUESTION_DESC_SHORT,
							QUESTION_NR,
						    USER_ANSWER_CD,
						    doc_snapshot_cd
						FROM
						    QUESTIONS
					ORDER BY QUESTION_NR,QUESTION_DESC_SHORT,QUESTION_DESC,QUESTION_CD ASC
					) Y
				{% endif %}
		</cfoutput>
		</cfsavecontent>
			<cfsavecontent variable ="detailsSql">
			<cfoutput>
				{% if DATA.IS_DUMMY|default(false)%}
					{% set data =[{USER_ID:1,SUPERVISOR_NAME:'Jhon Doe',DOC_SETUP_NAME:'UG Pediatrics TES',APPOINTMENT:'Anesthesia',COURSE_LIST:'Family Medicine'}]%}
					{{macros.SQLFromArray(data)}}
				{%else %}
			 		SELECT
				 		EU.DOC_SETUP_ID,
				 		EU.USER_ID,
				 		U.LAST_NAME||', '||U.FIRST_NAME AS SUPERVISOR_NAME,
				 		EU.USER_ID ||'_'||EU.DOC_SETUP_ID||'_{{DATA.PROCESSID}}' AS doc_snapshot_cd,
				 		DS.DOC_SETUP_NAME,
				 		D.DEPARTMENT_DESC || DECODE(DIV.DIVISION_DESC,null,'','/'||DIV.DIVISION_DESC) as APPOINTMENT,
				        (SELECT
				        	STRING_AGG(DISTINCT C.COURSE_NAME)
				        		FROM {fbx_datasource}.S4Y_USER_EVAL A
				        			JOIN DOC_SETUP_UNPUBLISHED_EVALS EV ON (A.USER_EVAL_CD=EV.EVALUATION_ID)
				        			LEFT JOIN  {fbx_datasource}.EVAL_FORM B ON (A.EVAL_FORM_CD =B.EVAL_FORM_CD)
				        			LEFT JOIN  {fbx_datasource}.COURSE C  ON (C.COURSE_CD=B.COURSE_CD)
				        		WHERE EV.DOC_SETUP_ID = EU.DOC_SETUP_ID
				        			AND EV.USER_ID = EU.USER_ID
				        			AND EV.PROCESS_ID='{{DATA.PROCESSID}}') AS COURSE_LIST
				 	FROM
				 		DOC_SETUP_ELIGIBLE_USERS EU,
				 		{pg_datasource}.UD_USER U,
				 		{fbx_datasource}.EVAL_SUPERVISOR_USER EUS,
				 		{fbx_datasource}.EVAL_SUPERVISOR_APPOINTMENT AP,
				 		{fbx_datasource}.DEPARTMENT D,
				 		{fbx_datasource}.DIVISION DIV,
				 		DOC_SETUP DS
				 	WHERE 1=1
				 		AND EU.USER_ID = U.USER_ID
				 		AND EU.DOC_SETUP_ID = DS.DOC_SETUP_ID
				 		AND EU.USER_ID = EUS.USER_ID(+)
				 		AND EUS.SUPERVISOR_ID = AP.SUPERVISOR_ID(+)
				 		AND AP.DEPARTMENT_CD = D.DEPARTMENT_CD(+)
				 		AND AP.DIVISION_CD = DIV.DIVISION_CD(+)
				 	{% if DATA.USER_ID is defined %}
						AND EU.USER_ID in ({{DATA.USER_ID}})
					{% endif %}
					{% if DATA.DOC_SETUP_ID is defined %}
						AND EU.DOC_SETUP_ID in ({{DATA.DOC_SETUP_ID}})
					{% endif %}
				{% endif %}
		</cfoutput>
		</cfsavecontent>
		<cfset sqls["RATING_GROUPS_DEF"] = ratingGroupsSql>
		<cfset sqls["QUESTIONS_DEF"] = questionDefinitionsSql>
		<cfset sqls["RATING_SCALE_DEF"]	= ratingScalesSql>
		<cfset sqls["SCORES_DEF"]	= scoresSql>
		<cfset sqls["EVALUATION_LIST"] = evalListSql>
		<cfset sqls["QUESTION_RATINGS_COUNT"] = ratingCountSql>
		<cfset sqls["QUESTION_SCORES"] = scoresCountSql>
		<cfset sqls["TOTAL_SCORES"] = totalscoresSql>
		<cfset sqls["QUESTION_COMPARISONS"] = comparisonsSql>
		<cfset sqls["COMMENTS"] = commentsSql>
		<cfset sqls["DOCUMENT_DETAILS"] = detailsSql>

		<cfreturn sqls>
	</cffunction>

</cfcomponent>