<cfcomponent name="UG_TesSetupFilter" code="QB_UG_TES_REPORT_FILTER" displayName="UG_TesSetupFilter" output="false" extends="sis_core.model.blAutomation.EntityBase">
	<cfproperty name="datasource" label="" inject="coldbox:setting:datasource" scope="variables"/>
	<cfproperty name="COURSE_NAME"  type="string" label="Course" id="COURSE_CD" restriction_type="DOCUMENT_BUILDER_COURSE">
	<cfproperty name="TEMPLATE_NAME"  type="string" label="Template Name" id="TEMPLATE_ID">
	<cfproperty name="SETUP_NAME"  type="string" label="Setup Name" id="SETUP_ID">
	<cfproperty name="EVAL_FORM_DESC"  type="string" label="Evaluation Form" id="EVAL_FORM_CD">


	<cffunction name="getGrants" returntype="array" access="public">
		<cfset var grants = [
			{OBJECT="EVAL_FORM",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"},
			{OBJECT="SUPERVISOR_APPOINTMENT",OBJECT_DATASOURCE="{fbx_datasource}",GRANT_TYPES="SELECT"}
		]>
		<cfreturn grants>
	</cffunction>

	<cfscript>

	public UG_TesSetupFilter function init() {
		variables.args = arguments;
		super.init();
		return this;
	}


    public string function getDataProvider_COURSE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
       var _sql = "SELECT DISTINCT
					    C.COURSE_CD as idField,
					    '['||C.COURSE_CODE||'] '|| C.COURSE_NAME as descField,
					    C.COURSE_NAME
					FROM
					     {fbx_datasource}.COURSE C,
					     {fbx_datasource}.CURRICULUM R
					WHERE
					    C.COURSE_CD = R.COURSE_CD
					ORDER BY C.COURSE_NAME ASC";
        return _sql;
    }
    public string function getDataProvider_TEMPLATE_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.LETTER_TEMPLATE_CD as idField,
                        A.LETTER_TEMPLATE_NAME as descField
                    FROM
                       DocumentTemplate_VIEW A
                    WHERE INSTANCE_ID = 6
                    ORDER BY UPPER(A.LETTER_TEMPLATE_NAME)";
        return _sql;
    }
    public string function getDataProvider_SETUP_NAME() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
         				DISTINCT
                        A.DOC_SETUP_ID as idField,
                        A.DOC_SETUP_NAME as descField
                    FROM
                       DOCSETUPELIGIBLEUSERS_VIEW A
                    WHERE INSTANCE_ID = 6
                    ORDER BY UPPER(A.DOC_SETUP_NAME)";
        return _sql;
    }

    public string function getDataProvider_EVAL_FORM_DESC() {
        var datasourceUtil = application.wirebox.getInstance("sis_core.model.util.Datasource");
        var _sql = "SELECT
       				 	DISTINCT
                        A.EVAL_FORM_CD as idField,
                        A.EVAL_FORM_DESC ||' ['||A.EVAL_FORM_CD||']' as descField
                    FROM
                       {fbx_datasource}.EVAL_FORM A
                       WHERE A.MAIN_FORM_CD IS NULL
                    ORDER BY UPPER(A.EVAL_FORM_DESC)";
        return _sql;
    }

    </cfscript>


</cfcomponent>