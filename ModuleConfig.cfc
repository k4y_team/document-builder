component output="false" extends="sis_core.model.FbxModuleConfig" {
	property name="Interceptor" inject="Interceptor@CORE";

	private void function loadSettings() {
		var tmp = StructNew();
		tmp.FACULTY = 'Faculty of Health Sciences';
		tmp.OFFICE = 'Postgraduate Medical Education';
		tmp.PHONE = '905.525.9140';
		tmp.EXT = '22116';
		tmp.FAX = '905.527.2707';
		tmp.WEBSITE = 'fhs.mcmaster.ca/postgrad';
		tmp.ADDRESS = 'MDCL-3101A<br>1280 Main Street West<br>Hamilton, ON, L8S 4K1<br>Canada';
		registerSetting('CLIENTINFO', tmp);
		registerSetting('INSTANCE_ID', 1);
		registerSetting('REQ_DOC_INSTANCE', 1);
		registerSetting('BACKWARD_COMPATIBILITY', false);

		registerSetting('DOC_PROCESS_COMPONENT', "medsisOld.modules.DocumentBuilder.Model.BL.DocProcess");
		registerSetting('DOC_ADMIN_COMPONENT', "medsisOld.modules.DocumentBuilder.Model.BL.DocAdmin");

		registerCircuit("DOCUMENT_BUILDER","DocumentBuilder","/Modules/DocumentBuilder");
		registerCircuit("DOC_PROCESS","DocProcess","/Modules/DocumentBuilder/Process");
		registerCircuit("DOCUMENT_REPORT","DocReport","/Modules/DocumentBuilder/Report");
		registerCircuit("DOCUMENT_SETUP","DocSetup","/Modules/DocumentBuilder/Setup");
		registerCircuit("DOC_TEMPLATE","DocTemplate","/Modules/DocumentBuilder/Setup/Template");
		registerCircuit("DOCUMENT_SETUP_GROUP","DocLetterMap","/Modules/DocumentBuilder/Setup/LetterMap");
		registerCircuit("DOCUMENT_SETUP_SIGNATURES","DocumentSignatures","/Modules/DocumentBuilder/Setup/Signatures");
		registerCircuit("DOCUMENT_SETUP_SETTINGS","DocumentSettings","/Modules/DocumentBuilder/Setup/Settings");
		registerCircuit("DOCUMENT_REPORT_SETUP","ReportSetup","/Modules/DocumentBuilder/Setup/ReportSetup");
		registerCircuit("DOCUMENT_TASKS","DocumentTasks","/Modules/DocumentBuilder/Tasks");
		registerCircuit("DOC_LETTERS","DocumentLetters","/Modules/DocumentBuilder/Letters");
	}

	public void function onLoad() {
		registerRBSetting('ALIAS', 'DOCUMENT_BUILDER_MODULE');
		registerRBSetting('FILENAME', 'document_builder');

		if (StructKeyExists(url, "db_refresh")) {
			QB = application.wirebox.getInstance('QueryBuilder@CORE');
		    var interceptor =   application.wirebox.getInstance("Interceptor@CORE");
			QB.unregisterAll(tags="DOCUMENT_BUILDER_MODULE");


		 	QB.registerEntity("QB_DOCUMENT_TEMPLATE", "DocumentTemplate", "medsisOld.modules.DocumentBuilder.Model.Entities.DocumentTemplate", "DOCUMENT_BUILDER_MODULE", {code="QB_DOCUMENT_TEMPLATE", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_TES_CONFIG_DATA_PROVIDER", "TESConfigDataProvider", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.TES.TESConfigDataProvider","DOCUMENT_BUILDER_MODULE", {code="QB_TES_CONFIG_DATA_PROVIDER", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_TES_DATA_PROVIDER", "TESDataProvider", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.TES.TESDataProvider","DOCUMENT_BUILDER_MODULE", {code="QB_TES_DATA_PROVIDER", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_TES_RENDERER", "TESRenderer", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.TES.TESRenderer","DOCUMENT_BUILDER_MODULE", {code="QB_TES_RENDERER", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_TES_REPORT_FILTER", "TesReportFilter", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.TES.TesReportFilter","DOCUMENT_BUILDER_MODULE", {code="QB_TES_REPORT_FILTER", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_TES_SETUP_FILTER", "TesSetupFilter", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.TES.TesSetupFilter","DOCUMENT_BUILDER_MODULE", {code="QB_TES_SETUP_FILTER", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_UG_HIERARCHY_CONFIG_COURSES", "UgHierarchyCourses", "medsisOld.modules.DocumentBuilder.Model.Entities.UgHierarchyCourses","DOCUMENT_BUILDER_MODULE", {code="QB_UG_HIERARCHY_CONFIG_COURSES", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("QB_PG_DOCUMENT", "PGDocument", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.TES.PGDocument","DOCUMENT_BUILDER_MODULE", {code="QB_PG_DOCUMENT", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_UG_DOCUMENT", "UGDocument", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.UG_TES.UGDocument","DOCUMENT_BUILDER_MODULE", {code="QB_UG_DOCUMENT", module="DOCUMENT_BUILDER_MODULE"});


			QB.registerEntity("QB_DOC_SETUP_ELIGIBLE_USERS", "DocSetupEligibleUsers", "medsisOld.modules.DocumentBuilder.Model.Entities.DocSetupEligibleUsers", "DOCUMENT_BUILDER_MODULE", {code="QB_DOC_SETUP_ELIGIBLE_USERS", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_DOC_SNAPSHOT", "DocSnapshot", "medsisOld.modules.DocumentBuilder.Model.Entities.DocSnapshot", "DOCUMENT_BUILDER_MODULE", {code="QB_DOC_SNAPSHOT", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_DOC_SNAPSHOT_GROUP", "DocSnapshotGroup", "medsisOld.modules.DocumentBuilder.Model.Entities.DocSnapshotGroup", "DOCUMENT_BUILDER_MODULE", {code="QB_DOC_SNAPSHOT_GROUP", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("DOCUMENT_SETUP_FORM","DocSetupForm", "medsisOld.modules.DocumentBuilder.Model.Entities.DocSetupForm", "DOCUMENT_BUILDER_MODULE", {code="DOCUMENT_SETUP_FORM", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("DOCUMENT_SETUP_DETAIL","SetupDetail", "medsisOld.modules.DocumentBuilder.Model.Entities.SetupDetail", "DOCUMENT_BUILDER_MODULE", {code="DOCUMENT_SETUP_DETAIL", module="DOCUMENT_BUILDER_MODULE"});


			QB.registerEntity("TES_CONFIG_FORM","TesConfigForm", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.TES.TesConfigForm", "DOCUMENT_BUILDER_MODULE", {code="TES_CONFIG_FORM", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("TASKS_FORM","TasksForm", "medsisOld.modules.DocumentBuilder.Model.Entities.TasksForm", "DOCUMENT_BUILDER_MODULE", {code="TASKS_FORM", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("QB_QUESTIONS_MAPPING", "QuestionsMapping", "medsisOld.modules.DocumentBuilder.Model.Entities.QuestionsMapping", "DOCUMENT_BUILDER_MODULE", {code="QB_QUESTIONS_MAPPING", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_UG_QUESTIONS_MAPPING", "UgQuestionsMapping", "medsisOld.modules.DocumentBuilder.Model.Entities.UgQuestionsMapping", "DOCUMENT_BUILDER_MODULE", {code="QB_UG_QUESTIONS_MAPPING", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("QB_UNPUBLISHED_USERS", "UserUnpublishedEvaluations", "medsisOld.modules.DocumentBuilder.Model.Entities.UserUnpublishedEvaluations", "DOCUMENT_BUILDER_MODULE", {code="QB_UNPUBLISHED_USERS", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("QB_UGUNPUBLISHED_USERS", "UgUserUnpublishedEvaluations", "medsisOld.modules.DocumentBuilder.Model.Entities.UgUserUnpublishedEvaluations", "DOCUMENT_BUILDER_MODULE", {code="QB_UGUNPUBLISHED_USERS", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("QB_UG_TES_REPORT_FILTER", "UG_TesReportFilter", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.UG_TES.UG_TesReportFilter","DOCUMENT_BUILDER_MODULE", {code="QB_UG_TES_REPORT_FILTER", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_UG_TES_SETUP_FILTER", "UG_TesSetupFilter", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.UG_TES.UG_TesSetupFilter","DOCUMENT_BUILDER_MODULE", {code="QB_UG_TES_SETUP_FILTER", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("UG_TES_CONFIG_FORM", "UGTesConfigForm", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.UG_TES.UGTesConfigForm","DOCUMENT_BUILDER_MODULE", {code="UG_TES_CONFIG_FORM", module="DOCUMENT_BUILDER_MODULE"});


			QB.registerEntity("DOC_SETUP","DOC_SETUP", "medsisOld.modules.DocumentBuilder.Model.Entities.db.DOC_SETUP", "DOCUMENT_BUILDER_MODULE", {code="DOC_SETUP",type="DB_ENTITY", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("TES_CONFIG","TES_CONFIG", "medsisOld.modules.DocumentBuilder.Model.Entities.db.TES_CONFIG", "DOCUMENT_BUILDER_MODULE", {code="TES_CONFIG",type="DB_ENTITY", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("TES_CONFIG_SCORES","TES_CONFIG_SCORES", "medsisOld.modules.DocumentBuilder.Model.Entities.db.TES_CONFIG_SCORES", "DOCUMENT_BUILDER_MODULE", {code="TES_CONFIG_SCORES",type="DB_ENTITY", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("TES_CONFIG_PROGRAMS","TES_CONFIG_PROGRAMS", "medsisOld.modules.DocumentBuilder.Model.Entities.db.TES_CONFIG_PROGRAMS", "DOCUMENT_BUILDER_MODULE", {code="TES_CONFIG_PROGRAMS",type="DB_ENTITY", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("TES_CONFIG_SESSIONS","TES_CONFIG_SESSIONS", "medsisOld.modules.DocumentBuilder.Model.Entities.db.TES_CONFIG_SESSIONS", "DOCUMENT_BUILDER_MODULE", {code="TES_CONFIG_SESSIONS",type="DB_ENTITY", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("TES_CONFIG_COMPARISONS","TES_CONFIG_COMPARISONS", "medsisOld.modules.DocumentBuilder.Model.Entities.db.TES_CONFIG_COMPARISONS", "DOCUMENT_BUILDER_MODULE", {code="TES_CONFIG_COMPARISONS",type="DB_ENTITY", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("UG_TES_CONFIG_COURSES","UG_TES_CONFIG_COURSES", "medsisOld.modules.DocumentBuilder.Model.Entities.db.UG_TES_CONFIG_COURSES", "DOCUMENT_BUILDER_MODULE", {code="UG_TES_CONFIG_COURSES",type="DB_ENTITY", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("QB_UG_TES_CONFIG_DATA_PROVIDER", "UGTESConfigDataProvider", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.UG_TES.UGTESConfigDataProvider","DOCUMENT_BUILDER_MODULE", {code="QB_UG_TES_CONFIG_DATA_PROVIDER", module="DOCUMENT_BUILDER_MODULE"});
			QB.registerEntity("QB_UG_TES_DATA_PROVIDER", "UGTESDataProvider", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.UG_TES.UGTESDataProvider","DOCUMENT_BUILDER_MODULE", {code="QB_UG_TES_DATA_PROVIDER", module="DOCUMENT_BUILDER_MODULE"});

			QB.registerEntity("QB_UG_TES_RENDERER", "UGTESRenderer", "medsisOld.modules.DocumentBuilder.Model.Entities.instanceConfig.UG_TES.UGTESRenderer","DOCUMENT_BUILDER_MODULE", {code="QB_UG_TES_RENDERER", module="DOCUMENT_BUILDER_MODULE"});
			interceptor.clearByTags("DOCUMENT_BUILDER_MODULE");

			interceptor.registerHandler(handlerCode="CALCULATE_ELIGIBLE_USER_SETUPS",handlerName="Calculate Document Eligible Users",handlerDetails={type = 'COMPONENT',path ='medsisOld.modules.DocumentBuilder.Model.Bl.DocumentTasks',method = 'calculateEligibleUsers'},tag="DOCUMENT_BUILDER_MODULE",handlerParams={formEntity="CALCULATE_USERS_FORM", hasPreview = "false"});
			interceptor.registerHandler(handlerCode="MAP_SETUP_QUESTIONS",handlerName="Map Document Setup Questions",handlerDetails={type = 'COMPONENT',path ='medsisOld.modules.DocumentBuilder.Model.Bl.DocumentTasks',method = 'mapSetupQuestions'},tag="DOCUMENT_BUILDER_MODULE",handlerParams={formEntity="CALCULATE_USERS_FORM", hasPreview = "false"});

			interceptor.registerHandler(handlerCode="GENERATE_DOCUMENTS",handlerName="Generate Documents",handlerDetails={type = 'COMPONENT',path ='medsisOld.modules.DocumentBuilder.Model.Bl.DocumentTasks',method = 'generateTes'},tag="DOCUMENT_BUILDER_MODULE",handlerParams={formEntity="TASKS_FORM", hasPreview = "false"});
			interceptor.registerHandler(handlerCode="PUBLISH_DOCUMENTS",handlerName="Publish Documents",handlerDetails={type = 'COMPONENT',path ='medsisOld.modules.DocumentBuilder.Model.Bl.DocumentTasks',method = 'publish'},tag="DOCUMENT_BUILDER_MODULE",handlerParams={formEntity="TASKS_FORM", hasPreview = "false"});

			QB.registerEntity("CALCULATE_USERS_FORM", "CalculateEligibleUsersForm", "medsisOld.modules.DocumentBuilder.Model.Bl.CalculateEligibleUsersForm","DOCUMENT_BUILDER_MODULE", {code="CALCULATE_USERS_FORM", module="DOCUMENT_BUILDER_MODULE"});

			var WorkflowRegistry = application.wirebox.getInstance("WorkflowRegistry@CORE");
			var workflowXml = FileRead(ExpandPath("/medsisOld/modules/DocumentBuilder/Workflows/workflow.xml"));
			workflowRegistry.register("Document Builder Workflow","DOCUMENTBUILDER_WF","DOCUMENTBUILDER_WF",workflowXml);
		}
		super.onLoad();
	}
}