<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<style>
	.group{
		background-color: #c4dce7 !important;
		font-weight: bold;
		text-color: black  !important;
		line-height: 24px !important;
		font-size: 12px !important;
	}
	.dropdown-menu{
        	position:absolute;
        	min-width:110px;
        	z-index: 10;
        }
</style>
<cfoutput>
	<form id="reportFrm" name="reportFrm" action="" method="post">
	<input type="hidden" name="filter_selected_users" value="">
	<input type="hidden" name="filter_excluded_users" value="">
	<input type="hidden" name="filter_selected_documents" value="">
	<input type="hidden" name="filter_excluded_documents" value="">
	<cfset tableId="loaResultlkTbl_#filter.getFILTER_DATA_STRUCT().DOC_LETTER_INSTANCE_CD#_#CreateUUID()#"/>
	<div class="row-fluid" id="tableDiv">
		<div class="head clearfix">
			<div class="isw-list"></div>
			<h1>Documents</h1>
			<h1 class="total_records">Total: <span id="#tableId#_total_records"></span></h1>
		</div>
		<div class="block-fluid table-sorting clearfix no-marg-bot">
			<table class="table" id="#tableId#"></table>
		</div>
		<div style="display:none" id="fake-table-toolbar">

			<div class="floatr" name="addButtonDiv" id="addButtonDiv">
				<cfif SecurityService.checkAccess('VIEW_DOC_REPORT','READ_WRITE')>
					<smart-button
								id="btnGenerate"
								icon="ico ico-bolt"
								class="btn btn-mini"
								confirmation = '{
								     "TITLE": "Generate Confirmation",
								     "BODY": "You are generating the selected TES reports. This action can not be undone. Do you wish to proceed?",
								     "PLACEMENT": "bottom",
								     "BUTTONS": [
								          {
								               "ICON": "ico ico-check",
								               "LABEL": "Yes",
								               "KEEP_OPEN": false,
								               "ON_CLICK": "generateDocuments()",
								               "TRIGGER": false
								          },
								          {
								               "ICON": "ico ico-times",
								               "LABEL": "No",
								               "KEEP_OPEN": false,
								               "ON_CLICK": "",
								               "TRIGGER": false
								          }
								     ]
								}'
								triggeredText="Generating Reports..."
								showConfirmation = 'true'
								> Generate
				</smart-button>
				</cfif>
				<cfif SecurityService.checkAccess('VIEW_DOC_REPORT','READ_WRITE')>
					<smart-button
								id="btnPublish"
								icon="ico ico-paper-plane"
								class="btn btn-mini"
								confirmation = '{
								     "TITLE": "Publish Confirmation",
								     "BODY": "You are publishing the selected TES reports. This action can not be undone. Do you wish to proceed?",
								     "PLACEMENT": "bottom",
								     "BUTTONS": [
								          {
								               "ICON": "ico ico-check",
								               "LABEL": "Yes",
								               "KEEP_OPEN": false,
								               "ON_CLICK": "publishDocuments()",
								               "TRIGGER": false
								          },
								          {
								               "ICON": "ico ico-times",
								               "LABEL": "No",
								               "KEEP_OPEN": false,
								               "ON_CLICK": "",
								               "TRIGGER": false
								          }
								     ]
								}'
								triggeredText="Publishing ..."
								showConfirmation = 'true'
								> Publish
				</smart-button>
				</cfif>
				<cfif SecurityService.checkAccess('DELETE_DOCUMENT','READ_WRITE')>
					<smart-button
									id="btnDelete"
									icon="ico ico-trash"
									class="btn btn-mini"
									confirmation = '{
									     "TITLE": "Delete Confirmation",
									     "BODY": "You are removing the selected TES reports. This action can not be undone. Do you wish to proceed?",
									     "PLACEMENT": "bottom",
									     "BUTTONS": [
									          {
									               "ICON": "ico ico-check",
									               "LABEL": "Yes",
									               "KEEP_OPEN": false,
									               "ON_CLICK": "deleteDocuments()",
									               "TRIGGER": false
									          },
									          {
									               "ICON": "ico ico-times",
									               "LABEL": "No",
									               "KEEP_OPEN": false,
									               "ON_CLICK": "",
									               "TRIGGER": false
									          }
									     ]
									}'
									triggeredText="Deleting ..."
									showConfirmation = 'true'
									> Delete
					</smart-button>
				</cfif>
				<k4y:button type="button" id="btnPrint" icon="PRINT" dictKey="PRINT" customAttributes="triggered-text='Exporting as PDF...'">
					Print All
				</k4y:button>
			</div>
			<div style="display:none;" class="s_loader floatr hide marg-right"></div>
		</div>
	</div>


	<script type="text/javascript">
		var deleteDocuments = function (){
			if (($('input.docSnapshotCd:checked').length == 0) && !$('##selectAllApplicants').prop('checked')) {
				setButtonState($('##btnPublish'),'active');
				alert('#moduleConfig.getResource4JS(resource="lbl.THERE_ARE_NO_DOCUMENTS_SELECTED_FOR_PUBLISH", default="There are no documents selected for publish.")#');
			} else {
				UIHelper.getComponent('btnDelete').setPressed(true);
				$.ajax({
        			type: "POST",
					async: true,
        			url: '#request.app.myself##XFA.Delete#',
        			data: $("##frmFilter").serialize() + "&" + $("##reportFrm").serialize() + "&filter_select_all="+$('##selectDisplayed').prop('checked'),
      				success: function(response) {
      					setButtonState($('##btnDelete'),'active');
      					getDocumentsList();
      				},
					error: function(){
						setButtonState($('##btnDelete'),'active');
 						var data = $.parseJSON(html);
					}
      			});
			}
		}
		var  generateDocuments = function (){
			if (($('input.userCd:checked').length == 0) && !$('##selectAllApplicants').prop('checked')) {
				setButtonState($('##btnGenerate'),'active');
				alert('#moduleConfig.getResource4JS(resource="lbl.THERE_ARE_NO_TRAINEES_SELECTED_FOR_GENERATE", default="There are no documents selected for generation.")#');
			} else {
				UIHelper.getComponent('btnGenerate').setPressed(true);
				$.ajax({
	        		type: "POST",
					async: true,
	        					url: '#request.app.myself##XFA.Generate#',
	        					data: $("##frmFilter").serialize() + "&" + $("##reportFrm").serialize()+ "&filter_select_all="+$('##selectDisplayed').prop('checked'),
	        						success: function(response) {
	        							setButtonState($('##btnGenerate'),'active');
	        							getDocumentsList();
	        						},
							error: function(){
										setButtonState($('##btnGenerate'),'active');
	   						var data = $.parseJSON(html);
							}
	        					});
			}
		}
		var publishDocuments = function (){
			if (($('input.docSnapshotCd:checked').length == 0) && !$('##selectAllApplicants').prop('checked')) {
				setButtonState($('##btnPublish'),'active');
				alert('#moduleConfig.getResource4JS(resource="lbl.THERE_ARE_NO_DOCUMENTS_SELECTED_FOR_PUBLISH", default="There are no documents selected for publish.")#');
			} else {
				UIHelper.getComponent('btnPublish').setPressed(true);
				$.ajax({
        			type: "POST",
					async: true,
        			url: '#request.app.myself##XFA.Publish#',
        			data: $("##frmFilter").serialize() + "&" + $("##reportFrm").serialize() + "&filter_select_all="+$('##selectDisplayed').prop('checked'),
      				success: function(response) {
      					setButtonState($('##btnPublish'),'active');
      					getDocumentsList();
      				},
					error: function(){
						setButtonState($('##btnPublish'),'active');
 						var data = $.parseJSON(html);
					}
      			});
			}
		}

		function previewDocument(docsnapshotCd){
		    var mWindow = window.open("#request.app.myself##XFA.Preview#&DOC_SNAPSHOT_CD="+docsnapshotCd+"&isDocument=1",'_new');
		}
		function printSnapshot(docsnapshotCd,user_name){
			var generateData = {};
			generateData.url = "#application.cbController.getSetting('modulesApplicationURL')#/index.cfm?fuseaction=DocumentLetters.viewTesLetter&instance_id=#filter.getFILTER_DATA_STRUCT().DOC_LETTER_INSTANCE_CD#&DOC_SNAPSHOT_CD="+docsnapshotCd+"&isDocument=1&pdf=1";
			generateData.document_page_orientation = "landscape";
			generateData.document_page_size ="letter";
			generateData.document_page_shrink_to_fit = true;
			generateData.document_filename = user_name.replace(",","_");
			generateData.document_page_margins = '{"top":50,"bottom":30,"left":70,"right":70}';
			$.fileDownload('#application.cbController.getRequestService().getContext().buildLink('Utils.generatePdf')#', {
							successCallback: function(url) {
								setButtonState($('##generatePdfLink'),'active');
							},
							failCallback: function (html, url) {
								setButtonState($('##generatePdfLink'),'active');
								UIHelper.MsgBox.drawMessage('error',"Sorry, the PDF file could not be generated!",{autoClose:false});
							},
							httpMethod: "POST",
							data: generateData
						});
		}
		function publishSnapshot(docsnapshotCd){
			$('input[name="filter_selected_documents"]').val(docsnapshotCd);
			$.ajax({
           		type: "POST",
				async: false,
           		url: '#request.app.myself##XFA.Publish#',
           		data: $("##frmFilter").serialize() + "&" + $("##reportFrm").serialize() + "&filter_select_all="+$('##selectDisplayed').prop('checked'),
           		success: function(response) {
           			setButtonState($('##btnPublish'),'active');
           			getDocumentsList();
           		},
 				error: function(){
   					setButtonState($('##btnPublish'),'active');
		    		var data = $.parseJSON(html);
 				}
           });
		}

		(function($) {
			$(function() {
				var xhr1;
				var selectedUserArr = [];
				var excludedUserArr = [];
				var selectedDocumentArr = [];
				var excludedDocumentArr = [];
				setButtonState($('##btnSearch'),'active');
				var createFilterData = function() {
					var filterArray = [];
					$("div.headInfo input,select").each(function() {
						var filterStruct = {};
						filterStruct["name"] = $(this).prop("name");
						filterStruct["value"] = $(this).val();
						filterArray.push(filterStruct);
					});
					return filterArray;
				}
				 oTable = $("###tableId#").dataTable({
					"bAutoWidth": false,
					"aaSorting": [[ 3, "asc" ]],
					"bStateSave": true,
					"aLengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
					"sPaginationType": "full_numbers",
					"sDom": '<"table-toolbar"><"top">rt<"bottom"ilp><"clear">',
					"bServerSide": true,
					"bProcessing": true,
					"aoColumns": [
						{"sTitle": "<input type='checkbox' id='selectDisplayed' value='1'>","sName": "user_id","sWidth":10, "bSortable": false, "sClass" : "tac"},
						{"sTitle": "Documents", "sName": "letter_template_name", "bSortable": false },
						{"sTitle": "Details", "sName": "Details", "bSortable": false,"bAlign" :"center" },
						{"sTitle": "User", "sName": "user_name", "bSortable": false ,"bVisible" :false },
						{"sTitle": "Historical Reports", "sName": "HISTORICAL_TES_REPORTS", "bSortable": false ,"bVisible" :false},
						{"sTitle": "Unpublished Evaluations", "sName": "unpublished_evaluations", "bSortable": false ,"bVisible" :false },
						{"sTitle": "Status", "sName": "PUBLISHED_BY", "bSortable": false},
						{"sTitle": "Published By", "sName": "PUBLISHED_BY", "bSortable": false},
						{"sTitle": "Published Date", "sName": "DOC_PUBLISHED_DT", "bSortable": false},
						{"sTitle": "Generated By", "sName": "GENERATED_BY", "bSortable": false, "sClass" : "tac" },
						{"sTitle": "Generation Date", "sName": "GENERATE_DT", "bSortable": false, "sClass" : "tac" },
						{"sTitle": "Actions", "sName": "doc_snapshot_cd", "sWidth":80, "bSortable": false, "sClass" : "tac" },
						{"sTitle": "setup id", "sName": "doc_setup_id", "sWidth":80, "bSortable": false, "sClass" : "tac","bVisible" :false },
						{"sTitle": "setup name", "sName": "doc_setup_name", "sWidth":80, "bSortable": false, "sClass" : "tac","bVisible" :false},
						{"sTitle": "setup name", "sName": "DOC_SETUP_ELIGIBLE_USER_ID", "sWidth":80, "bSortable": false, "sClass" : "tac","bVisible" :false},
						{"sTitle": "setup name", "sName": "DISTINCT_EVALUATORS", "sWidth":80, "bSortable": false, "sClass" : "tac","bVisible" :false},
						{"sTitle": "setup status", "sName": "SETUP_STATUS", "sWidth":80, "bSortable": false, "sClass" : "tac","bVisible" :false}
					],
					"aoColumnDefs":[
						{"mRender": function(data, type, row) {
							 if (row[11]>0 ){
							 	return '<label class="checkbox smart-checkbox"><input type="checkbox" name="FILTER_DOC_SNAPSHOT_CD" class="docSnapshotCd checkbox FILTER_DOC_SNAPSHOT_CD_'+row[14]+'" value="'+row[11]+'"/><span></span></label>';
							 }else{
							 	return '';
							 }
		                },
		                "aTargets": [0]},
		                {"mRender": function(data, type, row) {
		                	return (row[6] !== "") ?'<span class="label label-success">Published</span>':'<span class="label label-danger">Not Published</span>';

		                },
		                "aTargets": [6]},
		                {"mRender": function(data, type, row) {
		                	if(!row[8]==""){
								return moment(row[8]).format(MEDSIS_CUSTOM.dateFormat.momentDateFormat);
							}else{
                      	 		return "";
                      	 	}
		                },
		                "aTargets": [8]},
		                {"mRender": function(data, type, row) {
		                	if (row[6] !== ""){
		                		return '<div class="btn-group" style="width:116px;"><button class="btn btn-mini no-marg-bot" style="width:88px;" onclick="previewDocument('+row[11]+');return false;"><i class="ico ico-search"></i> Preview</button><button type="button" class="btn btn-mini dropdown-toggle more-actions-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-left:none;"><span class="ico ico-chevron-down"></span></button><ul class="dropdown-menu" role="menu" id="actions" aria-labelledby="dropdownMenu1"><li><a onclick="printSnapshot('+row[11]+','+'&apos;'+row[3]+'&apos;);return false;" href=""><i class="ico ico-print"></i> Print</a></li></ul></div>';
		                	}else{
		                		return '<div class="btn-group" style="width:116px;"><button class="btn btn-mini no-marg-bot" style="width:88px;" onclick="previewDocument('+row[11]+');return false;"><i class="ico ico-search"></i> Preview</button><button type="button" class="btn btn-mini dropdown-toggle more-actions-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border-left:none;"><span class="ico ico-chevron-down"></span></button><ul class="dropdown-menu" role="menu" id="actions" aria-labelledby="dropdownMenu1"><cfif SecurityService.checkAccess('VIEW_DOC_REPORT','READ_WRITE')><li><a onclick="publishSnapshot('+row[11]+');return false;" href=""><i class="ico ico-paper-plane"></i> Publish</a></li></cfif><li><a onclick="printSnapshot('+row[11]+','+'&apos;'+row[3]+'&apos;);return false;" href=""><i class="ico ico-print"></i> Print</a></li></ul></div>';
		                	}
		                	///return '<a target="_blank" href="#request.app.myself##XFA.Preview#&instance_id=5&DOC_SNAPSHOT_CD='+row[10]+'&isDocument=1" ><span class="ico ico-search"></span></a>';
		                },
		                "aTargets": [11]}
		            ],
					"fnServerParams": function ( aoData ) {
						$.merge(aoData, createFilterData());
					},
					"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
					    oSettings.jqXHR = $.ajax( {
					        "dataType": 'json',
					        "type": "POST",
					        "url": "#request.app.self#?fuseaction=#xfa.GetData#",
					        "data": aoData,
					        "success": function(result) {
					        	$("###tableId#_total_records").html(result.iTotalRecords);
			       		        if (result.iTotalRecords != 0) {
									$('##tableDiv').find('button').each(function(){
										$(this).removeAttr('disabled');
									});
									$('##tableDiv').find('input:checkbox').each(function(){
										$(this).removeAttr('disabled');
									});
			       		        }
			                	fnCallback(result);
						    }
					    } );
			        },
					"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
					},
					"fnDrawCallback": function( oSettings ) {
						if ( oSettings.aiDisplay.length == 0 ) {
							return;
						}
						var existingGroups = [];
						var nTrs = $('###tableId# tbody tr').not("[class='groupRow']");
						var iColspan = nTrs[0].getElementsByTagName('td').length;
						var sLastGroup = "";
						for ( var i = 0 ; i < nTrs.length; i++ ) {
							var sGroup = oSettings.aoData[oSettings.aiDisplay[i]]._aData[0]+'_'+oSettings.aoData[oSettings.aiDisplay[i]]._aData[12];
							var setupId = oSettings.aoData[oSettings.aiDisplay[i]]._aData[2];
							if (sGroup!==sLastGroup&&_.indexOf(existingGroups,sGroup)==-1) {
								var nGroup = document.createElement('tr');
								nGroup.className = "groupRow";
								var nCell1 = document.createElement('td');
								nCell1.className = "group tac";
								nCell1.innerHTML = '<label class="checkbox smart-checkbox"><input type="checkbox" name="FILTER_USER_CD" class="userCd checkbox" value="'+ oSettings.aoData[oSettings.aiDisplay[i]]._aData[14] + '" /><span></span></label>';
								nGroup.appendChild(nCell1);
								var nCell = document.createElement('td');
								nCell.colSpan = iColspan-1;
								nCell.className = "group";
								if(oSettings.aoData[oSettings.aiDisplay[i]]._aData[16] == 'Ready'){
									nCell.innerHTML = '<span class="group">' + oSettings.aoData[oSettings.aiDisplay[i]]._aData[3] +' / '+oSettings.aoData[oSettings.aiDisplay[i]]._aData[13] + '</span><span class="group" style="float:right;margin-right:20px;">'+oSettings.aoData[oSettings.aiDisplay[i]]._aData[4]+' Historical TES Reports, ' +oSettings.aoData[oSettings.aiDisplay[i]]._aData[5]+' Unpublished Evaluations, ' +oSettings.aoData[oSettings.aiDisplay[i]]._aData[15]+' Distinct Evaluators <span class="label label-success">'+oSettings.aoData[oSettings.aiDisplay[i]]._aData[16]+'</span></span></span>';
								}else{
									nCell.innerHTML = '<span class="group">' + oSettings.aoData[oSettings.aiDisplay[i]]._aData[3] +' / '+oSettings.aoData[oSettings.aiDisplay[i]]._aData[13] + '</span><span class="group" style="float:right;margin-right:20px;">'+oSettings.aoData[oSettings.aiDisplay[i]]._aData[4]+' Historical TES Reports, ' +oSettings.aoData[oSettings.aiDisplay[i]]._aData[5]+' Unpublished Evaluations, ' +oSettings.aoData[oSettings.aiDisplay[i]]._aData[15]+' Distinct Evaluators <span class="label label-grey">'+oSettings.aoData[oSettings.aiDisplay[i]]._aData[16]+'</span></span></span>';
								}
								nGroup.appendChild(nCell);
								$(nTrs[i]).before(nGroup);
								if(oSettings.aoData[oSettings.aiDisplay[i]]._aData[4] == 0){
									$(nTrs[i]).hide();
								}
								sLastGroup = sGroup;
							}
						}
					}
				});
				$('##selectDisplayed').on('click', function() {
					var thisChk = $(this);
					$('.docSnapshotCd').each(function(index, elem) {
						$(elem).prop('checked', $(thisChk).prop('checked'));
					});
					$('.userCd').each(function(index, elem) {
						$(elem).prop('checked', $(thisChk).prop('checked'));
					});
				});
				$(document).on('click', '.userCd', function () {
					var thisCheckbox = $(this);
					if ($('##selectDisplayed').prop('checked')) {
						selectedUserArr = [];
						if (!thisCheckbox.is(':checked')) {
							excludedUserArr.push(thisCheckbox.val());
							$('.FILTER_DOC_SNAPSHOT_CD_'+thisCheckbox.val()).each(function(index, elem) {
								var index = selectedDocumentArr.indexOf($(elem).val());
								excludedDocumentArr.push($(elem).val());
								$(elem).removeAttr('checked');
							});
						} else {
							$('.FILTER_DOC_SNAPSHOT_CD_'+thisCheckbox.val()).each(function(index, elem) {
								var index = excludedDocumentArr.indexOf($(elem).val());
								excludedDocumentArr.splice(index, 1);
								$(elem).prop('checked','checked');
							});
							var index = selectedUserArr.indexOf(thisCheckbox.val());
							excludedUserArr.splice(index, 1);
						}
					} else {
						excludedUserArr = [];
						if (thisCheckbox.is(':checked')) {
							selectedUserArr.push(thisCheckbox.val());
							$('.FILTER_DOC_SNAPSHOT_CD_'+thisCheckbox.val()).each(function(index, elem) {
								selectedDocumentArr.push($(elem).val());
								$(elem).prop('checked','checked');
							});
						} else {
							$('.FILTER_DOC_SNAPSHOT_CD_'+thisCheckbox.val()).each(function(index, elem) {
								var index = selectedDocumentArr.indexOf($(elem).val());
								selectedDocumentArr.splice(index, 1);
								$(elem).removeAttr('checked');
							});
							var index = selectedUserArr.indexOf(thisCheckbox.val());
							selectedUserArr.splice(index, 1);
						}
					}
					$('input[name="filter_selected_documents"]').val(selectedDocumentArr.toString());
					$('input[name="filter_excluded_documents"]').val(excludedDocumentArr.toString());
					$('input[name="filter_selected_users"]').val(selectedUserArr.toString());
					$('input[name="filter_excluded_users"]').val(excludedUserArr.toString());
				});
				$(document).on('click', '.docSnapshotCd', function () {
					var thisCheckbox = $(this);
					if ($('##selectDisplayed').prop('checked')) {
						selectedDocumentArr = [];
						if (!thisCheckbox.is(':checked')) {
							excludedDocumentArr.push(thisCheckbox.val());
						} else {
							var index = selectedDocumentArr.indexOf(thisCheckbox.val());
							excludedDocumentArr.splice(index, 1);
						}
					} else {
						excludedDocumentArr = [];
						if (thisCheckbox.is(':checked')) {
							selectedDocumentArr.push(thisCheckbox.val());
						} else {
							var index = selectedDocumentArr.indexOf(thisCheckbox.val());
							selectedDocumentArr.splice(index, 1);
						}
					}

					$('input[name="filter_selected_documents"]').val(selectedDocumentArr.toString());
					$('input[name="filter_excluded_documents"]').val(excludedDocumentArr.toString());
				});

				$('div.table-toolbar').append($('##fake-table-toolbar').children().detach());
				$('div.table-toolbar').css('float', 'none');


				$('##btnPrint').on('click', function(e) {
					e.preventDefault();
					if (($('input.docSnapshotCd:checked').length == 0) && !$('##selectAllApplicants').prop('checked')) {
						setButtonState($('##btnPrint'),'active');
						alert('#moduleConfig.getResource4JS(resource="lbl.THERE_ARE_NO_DOCUMENTS_SELECTED_FOR_PRINT", default="There are no documents selected for print.")#');
					} else {
						$('##export').val('');
						$.fileDownload('#request.app.myself##XFA.Print#', {
		            		successCallback: function(url) {
		            			setButtonState($('##btnPrint'),'active');
		            		},
		    				failCallback: function (html, url) {
		            			setButtonState($('##btnPrint'),'active');
			    				var data = $.parseJSON(html);
			    				//PWR.showNotif(data[0].MESSAGE, data[0].TYPE);
						    },
					        httpMethod: "POST",
					        data: $("##frmFilter").serialize() + "&" + $("##reportFrm").serialize() + "&filter_select_all="+$('##selectDisplayed').prop('checked'),
					        cookieName: 'print-letters'
					    });
					}
				});

			});
		}(jQuery))
	</script>
	</form>
</cfoutput>