<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<style type="text/css">
		.row-form select {
			background-color: ##FFFFFF !important;
		}
		.table div.checker {
			margin: 0 !important;
		}
	</style>
	<div class="row-fluid">
		<form name="frmFilter" id="frmFilter" action="#request.app.self##XFA.Search#" method="POST">
			<input type="hidden" name="fuseaction" value="">
			<input type="hidden" name="rpcCall" value="">
			<input type="hidden" name="forceRefresh" value="true">
			<!--- <input type="hidden" id="instance_id" name="instance_id" value="#attributes.instance_id#"> --->
			<input type="hidden" id="export" name="filter_export" value="">
			<div class="headInfo">
				<div class="row-form clearfix no-border width1000">
					<div class="span1 control-label">Session:</div>
					<div class="span3">
						<k4y:multiselect
                            id="FILTER_TRAINING_SESSION_CD"
                            name="FILTER_TRAINING_SESSION_CD"
                            query="trainingSessionsQry"
                            keyField="training_session_cd"
                            descField="description"
                            selectValues="#filterObj.getFILTER_ITEM('TRAINING_SESSION_CD')#"
                            order="DESC"
							allowNA="false"
							single="true"
                        />
					</div>
					<div class="span1 control-label">Program:</div>
					<div class="span3">
						<k4y:treeSelect
							name="FILTER_PROGRAM_CD"
							placeholder=""
							emptyText="-- All --"
							multiple="true"
							width="100%"
							height="200"
							url="#request.app.self#?fuseaction=PgTblProgram.getPrograms&checkTrainingLevelType=false"
							value="#filterObj.getFILTER_ITEM('PROGRAM_CD')#"
							readOnly="false"
						/>
					</div>
					<div class="span2 control-label"><k4y:label dictKey="TRAINEE_TYPE">Trainee Type</k4y:label></div>
					<div class="span2">
						<k4y:multiselect
                            id="trainee_type_cd"
                            name="filter_trainee_type_cd"
                            query="TraineeTypeQry"
                            keyField="student_type_cd"
                            descField="student_type_desc"
                            selectValues="#filterObj.getFILTER_ITEM('TRAINEE_TYPE_CD')#"
							naDesc="-- All --"
							single="true"
                        />
					</div>
					<!--- <div class="offset1 span1 control-label">Letter:</div>
										<div class="span3">
											<k4y:select
					                            id="LETTERTYPE"
					                            name="filter_LETTERTYPE"
					                            query="documentInstancesQry"
					                            keyField="instance_id"
					                            descField="instance_name"
					                            selectValue="#filterObj.getFILTER_ITEM("LETTERTYPE")#"
					                            showAll="true"
					                         />
										</div> --->
	      		</div>
	      		<div class="row-form clearfix no-border width1000">
					<div class="span1 control-label">Template:</div>
					<div class="span3">
						<k4y:multiSELECT
							name="filter_doc_letter_template_cd"
							query="DocTemplateQry"
							keyfield="doc_letter_template_cd"
							descField="letter_template_name"
							selectValues="#filterObj.getFILTER_ITEM('DOC_LETTER_TEMPLATE_CD')#"
							naDesc="-- All --"
						/>
					</div>
					<div class="span1 control-label">Letter Type:</div>
					<div class="span3">
						<k4y:multiSELECT
							name="filter_doc_letter_type_cd"
							id="doc_letter_type_cd"
							query="DocLetterTypeQry"
							keyfield="DOC_LETTER_TYPE_CD"
							descField="LETTER_TYPE_DESC"
							selectValues="#filterObj.getFILTER_ITEM('DOC_LETTER_TYPE_CD')#"
							naDesc="-- All --"
							single="true"
						/>
					</div>
					<div class="span2 control-label">Change Type:</div>
					<div class="span2">
						<k4y:multiSELECT
							name="filter_doc_change_type_cd"
							id="doc_change_type_cd"
							query="ChangeTypeQry"
							keyfield="DOC_CHANGE_TYPE_CD"
							descField="DOC_CHANGE_DESC"
							selectValues="#filterObj.getFILTER_ITEM('DOC_CHANGE_TYPE_CD')#"
							naDesc="-- All --"
							single="true"
						/>
					</div>
	      		</div>
				<div class="row-form clearfix no-border width1000">
					<div class="span1 control-label"><k4y:label dictKey="TRAINEE">Trainee</k4y:label></div>
					<div class="span3">
						<k4y:autocomplete
							name="filter_student_name"
							hiddenName="filter_student_id"
							class="autocomplete"
							placeholder="#moduleConfig.getResource(resource='lbl.TYPE_IN_TRAINEE_NAME', default='Type in Trainee Name')#"
							style="height: 22px !important;"
							requestURL="#request.app.myself##XFA.Trainees#"
							value="#filterObj.getFILTER_ITEM('STUDENT_NAME')#"
							hiddenValue="#filterObj.getFILTER_ITEM('STUDENT_ID')#"
						/>
					</div>
					<div class="span1 control-label">Status:</div>
					<div class="span3">
						<cfset loaStatusQry = queryNew("DOC_STATUS_CD, DOC_STATUS_DESC","VarChar, VarChar")>
						<cfset QueryAddRow( loaStatusQry ) />
						<cfset QuerySetCell(loaStatusQry, "DOC_STATUS_CD", "NOT_PUBLISHED")>
						<cfset QuerySetCell(loaStatusQry, "DOC_STATUS_DESC", "Not published")>
						<cfset QueryAddRow( loaStatusQry ) />
						<cfset QuerySetCell(loaStatusQry, "DOC_STATUS_CD", "PUBLISHED")>
						<cfset QuerySetCell(loaStatusQry, "DOC_STATUS_DESC", "Published")>

						<k4y:multiSELECT
							name="filter_status"
							id="status"
							query="loaStatusQry"
							keyfield="DOC_STATUS_CD"
							descField="DOC_STATUS_DESC"
							selectValues="#filterObj.getFILTER_ITEM('STATUS')#"
							naDesc="-- All --"
							single="true"
						/>
					</div>
					<div class="span2 control-label">Sign-off:</div>
					<div class="span2">
						<cfset loaSignoffStatusQry = queryNew("DOC_SIGNOFF_STATUS_CD, DOC_SIGNOFF_STATUS_DESC","VarChar, VarChar")>
						<cfset QueryAddRow( loaSignoffStatusQry ) />
						<cfset QuerySetCell(loaSignoffStatusQry, "DOC_SIGNOFF_STATUS_CD", "TRAINEE_SIGNED")>
						<cfset QuerySetCell(loaSignoffStatusQry, "DOC_SIGNOFF_STATUS_DESC", "#moduleConfig.getResource(resource='opt.BY_TRAINEE', default='By Trainee')#")>
						<cfset QueryAddRow( loaSignoffStatusQry ) />
						<cfset QuerySetCell(loaSignoffStatusQry, "DOC_SIGNOFF_STATUS_CD", "STAFF_SIGNED")>
						<cfset QuerySetCell(loaSignoffStatusQry, "DOC_SIGNOFF_STATUS_DESC", "By PGME Staff")>
						<cfset QueryAddRow( loaSignoffStatusQry ) />
						<cfset QuerySetCell(loaSignoffStatusQry, "DOC_SIGNOFF_STATUS_CD", "NOT_SIGNED")>
						<cfset QuerySetCell(loaSignoffStatusQry, "DOC_SIGNOFF_STATUS_DESC", "Outstanding")>

						<k4y:multiSELECT
							name="filter_signoff"
							id="signoff"
							query="loaSignoffStatusQry"
							keyfield="DOC_SIGNOFF_STATUS_CD"
							descField="DOC_SIGNOFF_STATUS_DESC"
							selectValues="#filterObj.getFILTER_ITEM('SIGNOFF')#"
							naDesc="-- All --"
							single="true"
						/>
					</div>
				</div>
				<div class="row-form clearfix no-border width1000">
					<div class="span12" style="text-align: right;">
						<k4y:button type="button" id="btnSearch" icon="SEARCH" dictKey="SEARCH" class="btn-primary" customAttributes="triggered-text='Searching...'">
							Search
						</k4y:button>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		$(function() {
			$('##btnSearch').click(function(ev){
				submitReport();
				hideData();
			});
		});
	</script>
</cfoutput>