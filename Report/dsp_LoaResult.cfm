<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<form id="reportFrm" name="reportFrm" action="" method="post">
	<div class="row-fluid" id="tableDiv">
		<div class="head clearfix">
			<div class="isw-list"></div>
			<h1><k4y:text dictKey="lbl.LETTERS_REPORT">Letters Report</k4y:text></h1>
			<h1 class="total_records">Total: <span id="total_records">#DocReportQry.RecordCount#</span></h1>
		</div>
		<div class="block-fluid table-sorting clearfix no-marg-bot">
			<table class="table" id="loaResultlkTbl">
				<thead>
					<tr>
						<th width="28" class="tac vam">
							<label class="checkbox smart-checkbox">
								<input type="checkbox" id="selectDisplayed" name="selectDisplayed" value="true" disabled class="checkbox">
								<span></span>
							</label>
						</th>
	                    <th width="65" class="vam">Session</th>
						<th class="vam">Student Number</th>
						<th class="vam"><k4y:text dictKey="lbl.TRAINEE_NAME">Trainee Name</k4y:text></th>
						<th class="vam"><k4y:text dictKey="lbl.TRAINEE_TYPE">Trainee Type</k4y:text></th>
	                    <th class="vam">Department / Program</th>
	                    <th class="vam">Letter</th>
						<th class="vam">Letter Template</th>
						<th class="vam">Letter Type</th>
	                   	<th class="vam">Email Date</th>
						<th class="vam"><k4y:text dictKey="lbl.LETTER_DUE_DATE">Letter Due Date</k4y:text></th>
	                    <th class="vam">Sign-off</th>
						<th class="vam">Print</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		<div style="display:none" id="fake-table-toolbar">
			<label class="checkbox smart-checkbox" style="float:left;margin-left:6px;">
				<input type="checkbox" name="FILTER_DOC_SELECTED" id="selectAllApplicants" value="Y" class="checkbox">
				<span>Select trainees meeting criteria including those that are not displayed</span>
			</label>
			<div class="floatr" name="addButtonDiv" id="addButtonDiv">
				<k4y:button type="button" id="btnExportExcel" icon="EXCEL" dictKey="EXCEL" customAttributes="triggered-text='Exporting as Excel...'">
					Excel
				</k4y:button>
				<k4y:button type="button" id="btnPrint" icon="PRINT" dictKey="PRINT" customAttributes="triggered-text='Exporting as PDF...'">
					Print
				</k4y:button>
			</div>
			<div style="display:none;" class="s_loader floatr hide marg-right"></div>
		</div>
	</div>
	</form>

	<script type="text/javascript">
		(function($) {
			$(function() {
				setButtonState($('##btnSearch'),'active');
				var createFilterData = function() {
					var filterArray = [];
					$("div.headInfo input,select").each(function() {
						var filterStruct = {};
						filterStruct["name"] = $(this).prop("name");
						filterStruct["value"] = $(this).val();
						filterArray.push(filterStruct);
					});
					return filterArray;
				}

				var oTable = $("##loaResultlkTbl").dataTable({
					"bAutoWidth": false,
					"aaSorting": [[ 0, "asc" ]],
					"bStateSave": true,
					"aLengthMenu": [[25, 50, 100], [25, 50, 100]],
					"sPaginationType": "full_numbers",
					"sDom": '<"table-toolbar"><"top">rt<"bottom"ilp><"clear">',
					"bServerSide": true,
					"aoColumns": [
						{ "sName": "1", "bSortable": false, "sClass" : "tac"},
						{ "sName": "training_session", "bSortable": true },
						{ "sName": "trainee_id", "bSortable": false},
						{ "sName": "trainee_full_name", "bSortable": true },
						{ "sName": "trainee_type", "bSortable": true },
						{ "sName": "department_program", "bSortable": true},
						{ "sName": "instance_name", "bSortable": true },
						{ "sName": "letter_template_name", "bSortable": true },
						{ "sName": "email_dt", "bSortable": true },
						{ "sName": "doc_due_date", "bSortable": true },
						{ "sName": "doc_submitted_dt", "bSortable": true },
						{ "sName": "doc_template", "bSortable": true },
						{ "sName": "doc_sign_status", "bSortable": false, "sClass" : "tac" }
					],
					"fnServerParams": function ( aoData ) {
						$.merge(aoData, createFilterData());
					},
					"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
					    oSettings.jqXHR = $.ajax( {
					        "dataType": 'json',
					        "type": "POST",
					        "url": "#request.app.self#?fuseaction=#xfa.GetData#",
					        "data": aoData,
					        "success": function(result) {
					        	$("##total_records").html(result.iTotalRecords);
			       		        if (result.iTotalRecords != 0) {
									$('##tableDiv').find('button').each(function(){
										$(this).removeAttr('disabled');
									});
									$('##tableDiv').find('input:checkbox').each(function(){
										$(this).removeAttr('disabled');
									});
			       		        }
			                	fnCallback(result);
						    }
					    } );
			        },
					"fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
						var td = $('td:eq(0)', nRow);
						td.css("text-align", "center");
					}
				});
				oTable.fnSort( [ [3,'asc'] ] );
				$('##selectDisplayed').on('click', function() {
					var thisChk = $(this);
					$('.docSnapshotCd').each(function(index, elem) {
						$(elem).prop('checked', $(thisChk).prop('checked'));
					});
				});

				$('div.table-toolbar').html($('##fake-table-toolbar').html());
				$('div.table-toolbar').css('float', 'none');

				$('##btnPrint').on('click', function(e) {
					e.preventDefault();
					if (($('input.docSnapshotCd:checked').length == 0) && !$('##selectAllApplicants').prop('checked')) {
						setButtonState($('##btnPrint'),'active');
						alert('#moduleConfig.getResource4JS(resource="lbl.THERE_ARE_NO_TRAINEES_SELECTED_FOR_PRINT", default="There are no trainees selected for print.")#');
					} else {
						$('##export').val('');
						$.fileDownload('#request.app.myself##XFA.Print#', {
		            		successCallback: function(url) {
		            			setButtonState($('##btnPrint'),'active');
		            		},
		    				failCallback: function (html, url) {
		            			setButtonState($('##btnPrint'),'active');
			    				var data = $.parseJSON(html);
			    				//PWR.showNotif(data[0].MESSAGE, data[0].TYPE);
						    },
					        httpMethod: "POST",
					        data: $("##frmFilter").serialize() + "&" + $("##reportFrm").serialize(),
					        cookieName: 'print-letters'
					    });
					}
				});

				$('##btnExportExcel').on('click', function(e) {
					e.preventDefault();
					if (($('input.docSnapshotCd:checked').length == 0) && !$('##selectAllApplicants').prop('checked')) {
						setButtonState($('##btnExportExcel'),'active');
						alert('#moduleConfig.getResource4JS(resource="lbl.THERE_ARE_NO_TRAINEES_SELECTED_FOR_EXPORT", default="There are no trainees selected for export.")#');
					} else {
						$('##export').val('1');
						$.fileDownload('#request.app.myself##XFA.Excel#', {
		            		successCallback: function(url) {
								setButtonState($('##btnExportExcel'),'active');
		            		},
		    				failCallback: function (html, url) {
			    				var data = $.parseJSON(html);
			    				setButtonState($('##btnExportExcel'),'active');
			    				//PWR.showNotif(data[0].MESSAGE, data[0].TYPE);
						    },
					        httpMethod: "POST",
					        data: $("##frmFilter").serialize() + "&" + $("##reportFrm").serialize(),
							cookieName: 'export-letters'
					    });
					}
				});
			});
		}(jQuery))
	</script>
</cfoutput>