<cfset breadCrumbCode = 'DOC_REPORT'>
<cfset activeOption = 'DOC_REPORT'>
<cfset request.securitycode = "DOC_REPORT">

<cfif !ListFindNoCase("TesletterPreview,printDocuments", fusebox.fuseaction)>
	<cfmodule template="#fusebox.rootPath##request.app.self#"
		fuseaction="security.setcircsec"
		funcList = "VIEW_DOC_REPORT"
		linksList = "DocReport.showReport"
		funcGroup = "DOC_REPORT"
		section = "DOCUMENT_BUILDER_MODULE"
	>

	<cfmodule template="#fusebox.rootPath##request.app.self#"
		fuseaction="security.setfuseaccess"
		fuseActionList ="saveDocChanges,showFilter,showReport,showResults,getData,exportResults,generateDocuments,deleteDocuments,publishDocuments,print,printDocuments,getTrainees4Auto,PopulatePrograms,letterPreview,downloadLetter,downloadTesLetter,getInstanceFilter,TesletterPreview,showDocumentsResults,showDocumentsFilter,GetDocumentsData,fixDocumentData"
		functionName = "VIEW_DOC_REPORT"
		fuseactionName="#fusebox.fuseaction#"
		security="#security#"
		>

	<cfif not isDefined("security.checked")>
		<cf_location url="#request.app.self#?fuseaction=security.shownotauth">
	</cfif>
</cfif>