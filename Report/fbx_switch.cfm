<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value ="showreport">
		<cfset pageTitle 	= "Document Report">
		<cfmodule template="#fusebox.rootPath##request.app.self#" fuseaction="s4y.pageTemplateAjax"
			refreshFilter = "DocReport.showFilter"
			refreshData = "DocReport.showResults"
			forceRefresh = "false"
			>
	</cfcase>
	<cfcase value="showDocumentsFilter">
		<cfset XFA.Search	= "DocReport.showDocumentsResults">
		<cfset documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset documentInstancesQry = documentInstances.getInstances(isUsed=true) />
		<cfset filterObj = application.wirebox.getInstance("sis_core.model.SearchFilter")>
		<cfset filterObj.initFromStruct(attributes)>
		<cfinclude template="dsp_DocumentsFilter.cfm" />
	</cfcase>
	<cfcase value="getInstanceFilter">
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceConfig = documentInstances.getInstanceConfig(attributes.FILTER_DOC_LETTER_INSTANCE_CD) />
		<cfset attributes.instance_id = attributes.FILTER_DOC_LETTER_INSTANCE_CD>
		<cfif !StructIsEmpty(instanceConfig) and structKeyExists(instanceConfig,"FILTER_ENTITY") >
			<cfset filterEntity = queryBuilder.getEntity(instanceConfig.FILTER_ENTITY) />
		<cfelse>
			<cfset filterEntity = "" >
		</cfif>
		<cfinclude template="dsp_getInstanceFilter.cfm" />
		<cfabort>
	</cfcase>
	<cfcase value="saveDocChanges">
		<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
		<cfset DocReport.saveDocChanges(attributes)>
		<cfabort>
	</cfcase>
	<cfcase value="showDocumentsResults">
		<cfset Filter = application.wirebox.getInstance("sis_core.model.blAutomation.Filter").init()/>
		<cfset previewCondition = Filter.getFilterCondition(filter=DeserializeJSON(attributes.documents_Filter))/>
		<cfset XFA.download="DocumentLetters.downloadLetter">
		<cfset XFA.Print = "DocReport.printDocuments&instance_id=#attributes.FILTER_DOC_LETTER_INSTANCE_CD#" />
		<cfset XFA.GetData = "DocReport.GetDocumentsData&instance_id=#attributes.FILTER_DOC_LETTER_INSTANCE_CD#" />
		<cfset XFA.Preview	= "DocReport.TesletterPreview">
		<cfset XFA.Search="DocReport.showDocumentsResults&instance_id=#attributes.FILTER_DOC_LETTER_INSTANCE_CD#" />
		<cfset XFA.Generate = "DocReport.generateDocuments" />
		<cfset XFA.Publish = "DocReport.publishDocuments" />
		<cfset XFA.Delete = "DocReport.deleteDocuments" />
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset filter.initFromStruct(attributes) />
		<cfset userSessionData = request.cbcontroller.getPlugin('SessionStorage').getVar('LoggedInUser')/>
		<cfset UserDirectoryAPI = application.wirebox.getInstance(name="", dsl="API@UserDirectory")/>
		<cfset SecurityService = UserDirectoryAPI.getUserSecurity(1, userSessionData.user_id) />
		<cfinclude template="dsp_DocumentsResult.cfm">
		<cfabort>
	</cfcase>
	<cfcase value="downloadLetter">
		<cfmodule template="#fusebox.rootPath##request.app.self#"
			fuseaction="DocumentLetters.downloadLetter"
			student_id="#attributes.student_id#"
			doc_snapshot_cd="#attributes.doc_snapshot_cd#"
			instance_id="#attributes.instance_id#"
			>
	</cfcase>
	<cfcase value="downloadTesLetter">
		<cfmodule template="#fusebox.rootPath##request.app.self#"
			fuseaction="DocumentLetters.downloadTesLetter"
			doc_snapshot_cd="#attributes.doc_snapshot_cd#"
			instance_id="#attributes.instance_id#"
			>
	</cfcase>
	<cfcase value="letterPreview">
		<cfparam name="attributes.XFA_close" default="DocReport.showResults" />
		<cfmodule template="#fusebox.rootPath##request.app.self#"
			fuseaction="DocumentLetters.viewLetter"
			student_id="#attributes.student_id#"
			stdPreview="true"
			doc_snapshot_cd="#attributes.doc_snapshot_cd#"
			instance_id="#attributes.instance_id#"
			XFA_download="DocReport.downloadLetter"
			XFA_close="#attributes.XFA_close#"
			>
	</cfcase>
	<cfcase value="TesletterPreview">
		<cfparam name="attributes.XFA_close" default="DocReport.showResults" />
		<cfmodule template="#fusebox.rootPath##request.app.self#"
			fuseaction="DocumentLetters.viewTesLetter"
			stdPreview="true"
			XFA_Publish = "DocReport.publishDocuments"
			doc_snapshot_cd="#attributes.doc_snapshot_cd#"
			XFA_download="DocReport.downloadTesLetter"
			XFA_close="#attributes.XFA_close#"
			>
	</cfcase>
	<cfcase value="getData">
		<cfset XFA.Preview="DocReport.letterPreview">
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset filter.initFromStruct(attributes) />
		<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
		<cfset DocReportQry = DocReport.getReport(filter)>
		<cfset aaData = ArrayNew(1)>
		<cfloop query="DocReportQry">
			<cfset rowData = ArrayNew(1)>
			<cfset ArrayAppend(rowData, "<label class='checkbox smart-checkbox'><input type='checkbox' name='FILTER_DOC_SNAPSHOT_CD' class='docSnapshotCd checkbox' value='#DocReportQry.DOC_SNAPSHOT_CD#' /><span></span></label>")>
			<cfset ArrayAppend(rowData, " " & DocReportQry.Training_Session)>
			<cfset ArrayAppend(rowData, DocReportQry.STUDENT_NUMBER)>
			<cfset ArrayAppend(rowData, " " & DocReportQry.TRAINEE_FULL_NAME)>
			<cfset ArrayAppend(rowData, DocReportQry.TRAINEE_TYPE)>
			<cfset ArrayAppend(rowData, DocReportQry.DEPARTMENT_PROGRAM)>
			<cfset ArrayAppend(rowData, DocReportQry.INSTANCE_NAME)>
			<cfset ArrayAppend(rowData, DocReportQry.LETTER_TEMPLATE_NAME)>
			<cfif DocReportQry.LETTER_TYPE_CODE EQ "REVISED" and DocReportQry.DOC_CHANGE_DESC NEQ "" >
				<cfset ArrayAppend(rowData, DocReportQry.Loa_Type & " (#DOC_CHANGE_DESC#)")>
			<cfelse>
				<cfset ArrayAppend(rowData, DocReportQry.Loa_Type)>
			</cfif>
			<cfset ArrayAppend(rowData, DateFormat(DocReportQry.Email_dt, medsis_date_format))>
			<cfset ArrayAppend(rowData, DateFormat(DocReportQry.DOC_DUE_DATE, medsis_date_format))>
			<cfif DocReportQry.DOC_SIGN_STATUS eq "SIGNED_BY_STAFF" or DocReportQry.DOC_SIGN_STATUS eq "SIGNED_BY_TRAINEE">
				<cfset ArrayAppend(rowData, DateFormat(DocReportQry.DOC_SUBMITTED_DT, medsis_date_format))>
			<cfelse>
				<cfset ArrayAppend(rowData, "") />
			</cfif>
			<cfif uCase(DocReportQry.DOC_SIGN_STATUS) eq "SIGNED_BY_STAFF" >
				<cfsavecontent variable = "downloadLink">
					<cfimport prefix="k4y" taglib="/sis_core/cftags">
					<cfoutput>
						<k4y:downloadFile fileCD="#DocReportQry.DOC_PROOF_FILE_CD#" showIcon="true" showFileName="false" showTextLink="true" TextLink=""/>
					</cfoutput>
				</cfsavecontent>
 				<cfset ArrayAppend(rowData, downloadLink)>
			<cfelse>
				<cfset ArrayAppend(rowData, '<a target="_blank" href="#request.app.myself##XFA.Preview#&instance_id=#DocReportQry.INSTANCE_ID#&DOC_SNAPSHOT_CD=#DOC_SNAPSHOT_CD#&student_id=#DocReportQry.STUDENT_ID#&isDocument=1" ><span class="ico ico-search"></span></a>') />
			</cfif>
			<cfset ArrayAppend(aaData, rowData)>
		</cfloop>
		<cfset  result = {
			"sEcho": Int(Val(attributes.sEcho)),
			"iTotalRecords": "#iif(DocReportQry.recordCount eq 0, DocReportQry.recordCount, DocReportQry.total_records)#",
			"iTotalDisplayRecords": "#iif(DocReportQry.recordCount eq 0, DocReportQry.recordCount, DocReportQry.total_records)#",
			"aaData": aaData
			}>

		<cfoutput>#serializeJson(result)#</cfoutput><cfabort>
	</cfcase>
	<cfcase value="getDocumentsData">
		<cfset XFA.Preview="DocReport.TesletterPreview">
		<cfset XFA.download="DocReport.downloadTesLetter">
		<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset filter.initFromStruct(attributes) />
		<cfset DocReportQry = DocReport.getDocuments(filter)>
		<cfset UsersTblRenderer = application.wirebox.getInstance(name="sis_core.model.DataTableRenderer",initArguments={dataQry=DocReportQry, columns=attributes.sColumns, metadata={sEcho:attributes.sEcho}})>
		<cfoutput>
			#UsersTblRenderer.render()#
		</cfoutput>
		<cfabort>
	</cfcase>
	<cfcase value="exportresults">
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset filter.initFromStruct(attributes) />
		<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init(attributes.instance_id) />
		<cfset DocReportQry=DocReport.getReport(filter)>
		<cfset filename = "AGREEMENT_LETTER_REPORT_" & DateFormat(Now(), "dd_mm_yyyy") & ".xls">
		<cfinclude template="dsp_DocReportExport.cfm">
	</cfcase>
	<cfcase value="print">
		<cfset docSnapshot 	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot').init(attributes.instance_id)>
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset filter.initFromStruct(attributes) />
		<!--- genereate preview files and put all genereate files in zip folder ands save on disk --->
		<cfset print = docSnapshot.CreatePDFPreviews(filter)>
	</cfcase>
	<cfcase value="printDocuments">
		<cfset docSnapshot 	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot').init()>
		<cfset documentList ="">
		<cfset excludedDocuments="">
		<cfset selectedDocuments="">
		<cfset DOCAdmin = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DOCAdmin").init()>
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset filter.initFromStruct(attributes) />
		<cfloop list="#attributes.filter_selected_documents#" delimiters=",|" index="record">
			<cfset selectedDocuments =ListAppend(selectedDocuments,ListLast(record,'_',true))>
		</cfloop>
		<cfloop list="#attributes.filter_excluded_documents#" delimiters=",|" index="record">
			<cfset excludedDocuments =ListAppend(excludedDocuments,ListLast(record,'_',true))>
		</cfloop>
		<cfif attributes.filter_select_all>
			<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
			<cfset filter.setFILTER_ITEM('DOC_SNAPSHOT_CD','')/>
			<cfset filter.setFILTER_ITEM('USER_CD','')/>
			<cfset DocReportQry = DocReport.getDocuments(filter)>
			<cfset documentList = ListAppend(documentList,ListRemoveDuplicates(ValueList(DocReportQry.doc_snapshot_cd)))>
			<cfloop list="#arrayToList(listToArray(excludedDocuments))#" index="item">
				<cfif ListFind(documentList,item) GT 0>
					<cfset documentList = listDeleteAt(documentList, ListFind(documentList,item)) />
				</cfif>
			</cfloop>
		<cfelse>
			<cfset documentList= ListAppend(documentList,arrayToList(listToArray(selectedDocuments)))>
		</cfif>
		<cfset filter.setFILTER_ITEM('DOC_SNAPSHOT_CD',documentList) />

		<!--- genereate preview files and put all genereate files in zip folder ands save on disk --->
		<cfset print = docSnapshot.CreateTesPDFPreviews(filter)>
	</cfcase>
	<cfcase value="generateDocuments">
		<cfset userList ="">
		<cfset excludedUsers="">
		<cfset selectedUsers="">
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset attributes.FILTER_DOC_SNAPSHOT_CD = "">
		<cfset filter.initFromStruct(attributes) />
		<cfloop list="#attributes.filter_selected_users#" delimiters=",|" index="record">
			<cfset selectedUsers =ListAppend(selectedUsers,ListLast(record,'_',true))>
		</cfloop>
		<cfloop list="#attributes.filter_excluded_users#" delimiters=",|" index="record">
			<cfset excludedUsers =ListAppend(excludedUsers,ListLast(record,'_',true))>
		</cfloop>
		<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
		<cfif attributes.filter_select_all>
			<cfset DocReportQry = DocReport.getDocuments(filter)>
			<cfset userList = ListAppend(userList,ListRemoveDuplicates(ValueList(DocReportQry.DOC_SETUP_ELIGIBLE_USER_ID)))>
			<cfloop list="#arrayToList(listToArray(excludedUsers))#" index="item">
				<cfset userList = listDeleteAt(userList, ListFind(userList,item)) />
			</cfloop>
		<cfelse>
			<cfset userList= ListAppend(userList,arrayToList(listToArray(selectedUsers)))>
		</cfif>
		<cfset DocumentTasks = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocumentTasks" ).init()/>
		<cfset params.INSTANCE_ID = filter_doc_letter_instance_cd>
		<cfset params.DOC_SETUP_ELIGIBLE_USER_ID = userList>
		<!--- publish  --->
		<cfset result = DocumentTasks.generateTes(params) />
	</cfcase>

	<cfcase value="deleteDocuments">
		<cfset DocumentTasks = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocumentTasks" ).init()/>
		<cfset docSnapshot 	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot').init()>
		<cfset documentList ="">
		<cfset excludedDocuments="">
		<cfset selectedDocuments="">
		<cfset DOCAdmin = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DOCAdmin").init()>
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset filter.initFromStruct(attributes) />
		<cfloop list="#attributes.filter_selected_documents#" delimiters=",|" index="record">
			<cfset selectedDocuments =ListAppend(selectedDocuments,ListLast(record,'_',true))>
		</cfloop>
		<cfloop list="#attributes.filter_excluded_documents#" delimiters=",|" index="record">
			<cfset excludedDocuments =ListAppend(excludedDocuments,ListLast(record,'_',true))>
		</cfloop>
		<cfif attributes.filter_select_all>
			<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
			<cfset filter.setFILTER_ITEM('DOC_SNAPSHOT_CD','')/>
			<cfset filter.setFILTER_ITEM('USER_CD','')/>
			<cfset DocReportQry = DocReport.getDocuments(filter)>
			<cfset documentList = ListAppend(documentList,ListRemoveDuplicates(ValueList(DocReportQry.doc_snapshot_cd)))>
			<cfloop list="#arrayToList(listToArray(excludedDocuments))#" index="item">
				<cfif ListFind(documentList,item) GT 0>
					<cfset documentList = listDeleteAt(documentList, ListFind(documentList,item)) />
				</cfif>
			</cfloop>
		<cfelse>
			<cfset documentList= ListAppend(documentList,arrayToList(listToArray(selectedDocuments)))>
		</cfif>
		<cfset filter.setFILTER_ITEM('DOC_SNAPSHOT_CD',documentList) />
		<cfset result = DocumentTasks.deleteDocuments(filter) />
	</cfcase>

	<cfcase value="publishDocuments">
		<cfset documentList ="">
		<cfset excludedDocuments="">
		<cfset selectedDocuments="">
		<cfset DocumentTasks = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocumentTasks" ).init()/>
		<cfset filter = application.wirebox.getInstance("SearchFilter")>
		<cfset attributes.FILTER_DOC_SNAPSHOT_CD = "">
		<cfset filter.initFromStruct(attributes) />
		<cfif isDefined("attributes.filter_selected_documents")>
			<cfloop list="#attributes.filter_selected_documents#" delimiters=",|" index="record">
				<cfset selectedDocuments =ListAppend(selectedDocuments,ListLast(record,'_',true))>
			</cfloop>
			<cfloop list="#attributes.filter_excluded_documents#" delimiters=",|" index="record">
				<cfset excludedDocuments =ListAppend(excludedDocuments,ListLast(record,'_',true))>
			</cfloop>
			<cfif attributes.filter_select_all>
				<cfset DocReport = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocReport").init() />
				<cfset DocReportQry = DocReport.getDocuments(filter)>
				<cfset documentList = ListAppend(documentList,ListRemoveDuplicates(ValueList(DocReportQry.doc_snapshot_cd)))>
				<cfloop list="#arrayToList(listToArray(excludedDocuments))#" index="item">
					<cfset documentList = listDeleteAt(documentList, ListFind(documentList,item)) />
				</cfloop>
			<cfelse>
				<cfset documentList= ListAppend(documentList,arrayToList(listToArray(selectedDocuments)))>
			</cfif>
		<cfelse>
			<cfset documentList = attributes.doc_snapshot_cd>
		</cfif>
		<cfset params.DOC_SNAPSHOT_CD = documentList >
        <cfset params.INSTANCE_ID = filter_doc_letter_instance_cd>
		<cfset result = DocumentTasks.publish(params) />
	</cfcase>
	<cfcase value="fixDocumentData">
		<cfinclude template="fixComments.cfm">
	</cfcase>
</cfswitch>
