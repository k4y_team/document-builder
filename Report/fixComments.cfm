<cfset  DataSourceUtil = application.wirebox.getInstance(name="sis_core.model.util.datasource")/>
<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
<cfset queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils")>
<cfset coreDSN = application.wirebox.getInstance(dsl="coldbox:setting:datasource")>
<cfset fbxDSN = application.wirebox.getInstance(dsl="coldbox:setting:fbx_datasource")>
<cfset fbxUserDSNUser = DataSourceUtil.getUserName(fbxDSN)/>
<cfset docInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />

<cfset url.properties = "QUESTIONS_DEF,QUESTION_RATINGS_COUNT,QUESTION_SCORES">

<cfquery name="getComments" datasource="#coreDSN#">
SELECT
	DISTINCT
	    C.PROCESS_ID AS PROCESSID,
	    C.DOC_SETUP_ID,
		C.INSTANCE_ID,
		C.USER_ID,
		C.DOC_SNAPSHOT_CD
	FROM
	   DOC_SNAPSHOT_DATA A,
	   DOC_SNAPSHOT_BASE C
	WHERE A.KEY='QUESTIONS_DEF'
	AND A.DOC_SNAPSHOT_CD=C.DOC_SNAPSHOT_CD

   <cfif isDefined('url.DOC_SNAPSHOT_CD')>
		AND A.DOC_SNAPSHOT_CD='#url.DOC_SNAPSHOT_CD#'
   </cfif>
	AND A.DOC_SNAPSHOT_CD in (SELECT
		    a.doc_snapshot_cd
		FROM
		    doc_snapshot_data a
		WHERE
		        1=1
		    and A.KEY='QUESTIONS_DEF'
		    and value like '%1668611%'
		    and not exists (
		    SELECT
		    *
		FROM
		    doc_snapshot_data b
		WHERE
		        1=1
		    and a.doc_snapshot_cd = b.doc_snapshot_cd
		    and b.KEY='QUESTION_COMPARISONS'
		    and b.value like '%1668611%'))
   <cfif isDefined('url.INSTANCE_ID')>
		AND C.INSTANCE_ID='#url.INSTANCE_ID#'
   </cfif>
</cfquery>

<cftry>
<cfset properties = "QUESTION_COMPARISONS,QUESTIONS_DEF,QUESTION_RATINGS_COUNT,QUESTION_SCORES,COMMENTS"/>
<cfif isDefined('url.properties')>
	<cfset properties = url.properties />
</cfif>


<cfloop query="getComments">
	<cfquery name="mergeComments" datasource="#coreDSN#">
	MERGE INTO DOC_SETUP_UNPUBLISHED_EVALS D USING
			( SELECT
			    B.TRAINING_SESSION_CD,
			    Z.DOC_SNAPSHOT_CD,
			    Z.INSTANCE_ID,
			    Z.USER_ID,
			    Z.DOC_SETUP_ID,
			    Z.PROCESS_ID,
			    Z.EVALUATION_ID
			FROM
			    (
			        SELECT
			            REGEXP_SUBSTR(
			                EVALUATION_LIST,
			                '[^,]+',
			                1,
			                EVALUATIONS.COLUMN_VALUE
			            ) AS EVALUATION_ID,
			            DOC_SNAPSHOT_CD,
			            INSTANCE_ID,
			            USER_ID,
			            DOC_SETUP_ID,
			            PROCESS_ID
			        FROM
			            (
			                SELECT
			                    A.DOC_SNAPSHOT_CD,
			                    A.DOC_SETUP_ID,
			                    A.INSTANCE_ID,
			                    A.USER_ID,
			                    A.PROCESS_ID,
			                    (
			                        SELECT
			                            LISTAGG(
			                                EVALUATION_ID,
			                                ','
			                            ) WITHIN GROUP(ORDER BY A.DOC_SNAPSHOT_CD) AS EVALUATION_LIST
			                        FROM
			                            TABLE ( PLJSON_TABLE.JSON_TABLE(
			                                D.VALUE,
			                                PLJSON_VARRAY('[*].EVALUATION_ID'),
			                                PLJSON_VARRAY('EVALUATION_ID'),
			                                TABLE_MODE   => 'NESTED'
			                            ) )
			                    ) EVALUATION_LIST
			                FROM
			                    DOC_SNAPSHOT_BASE A,
			                    DOC_SNAPSHOT_DATA D
			                WHERE
			                        A.DOC_SNAPSHOT_CD = D.DOC_SNAPSHOT_CD
			                    AND
			                        D.KEY = 'EVALUATION_LIST'
			                    AND
			                        A.PROCESS_ID = '#getComments.PROCESSID#'
			            ),
			            TABLE ( CAST(MULTISET(
			                SELECT
			                    LEVEL
			                FROM
			                    DUAL
			                CONNECT BY
			                    LEVEL <= LENGTH(REGEXP_REPLACE(
			                        EVALUATION_LIST,
			                        '[^,]+'
			                    ) ) + 1
			            ) AS SYS.ODCINUMBERLIST) ) EVALUATIONS
			        WHERE
			            DOC_SNAPSHOT_CD = #getComments.DOC_SNAPSHOT_CD#
			    ) Z
			    JOIN #fbxUserDSNUser#.S4Y_USER_EVAL A ON (
			        Z.EVALUATION_ID = A.USER_EVAL_CD
			    )
			    JOIN #fbxUserDSNUser#.S4Y_EVAL_SCHEDULE B ON (
			        A.EVAL_SCHEDULE_CD = B.EVAL_SCHEDULE_CD
			    )
			)
			S ON (
			    S.PROCESS_ID = D.PROCESS_ID
			AND
			    S.EVALUATION_ID = D.EVALUATION_ID
			AND
			    S.USER_ID = D.USER_ID
			) WHEN NOT MATCHED
				THEN INSERT (D.DOC_SETUP_ID,D.EVALUATION_ID,D.TRAINING_SESSION_CD,D.USER_ID,D.INSTANCE_ID,D.PROCESS_ID )
				VALUES ( S.DOC_SETUP_ID,S.EVALUATION_ID,S.TRAINING_SESSION_CD,S.USER_ID,S.INSTANCE_ID,S.PROCESS_ID )
	</cfquery>

	<cfset dataStruct.PROCESSID = getComments.PROCESSID />
	<cfset dataStruct.DOC_SETUP_ID = getComments.DOC_SETUP_ID />
	<cfset dataStruct.USER_ID = getComments.USER_ID />

	<cfset instanceConfig = docInstances.getInstanceConfig(getComments.instance_id) />
	<cfset dataProviderEntity = queryBuilder.getEntity(instanceConfig.DOCUMENT_DATA_PROVIDER_ENTITY) />
	<cfloop list="#properties#" index="property">
		<cfset qrySql = "Select * from (" & dataProviderEntity.getSQL('#property#') & ") where 1=0">

		<cfset propertQry = queryBuilder.runQuery(sql=qrySql,datasource=dataProviderEntity.GETENTITYDATASOURCE(),params=dataStruct)>
		<cfset columnList1 = ArrayToList(propertQry.getColumnList())>

		<cfset listPos = ListFind(columnList1,'DOC_SNAPSHOT_CD',",")>



		<cfif listPos>
				<cfset columnList1 = ListDeleteAt( columnList1, listPos ,",") />
		</cfif>
								<cfset sql ="Select '[' "/>
								<cfset sql= sql & " ||dbms_xmlgen.convert(RTRIM(XMLAGG(XMLELEMENT(E, '{">
								<cfset loopCount = 0>
								<cfloop index="i" list="#columnList1#">
									<cfset  sql = sql & '"#i#":"''||#i#||''"'/>
									<cfset loopCount=loopCount+1>
									<cfif (loopCount lt ListLen(columnList1))>
										<cfset  sql = sql & ',' />
									</cfif>
								</cfloop>
								<cfset sql = sql & "}',',').EXTRACT('//text()') ORDER BY order_nr asc,doc_snapshot_cd DESC NULLS LAST).getclobval(),','),1) ||']' AS VALUE,'#property#' as key, doc_snapshot_cd from ("/>
								<cfset newSql = sql & 'SELECT ROWNUM AS ORDER_NR ,Z.* FROM ('& dataProviderEntity.getSQL('#property#') & ') Z)	group by doc_snapshot_cd'>
								<cfset resultsql= queryBuilder.getQuery(sql=newSql,datasource = dataProviderEntity.GETENTITYDATASOURCE(),params=dataStruct)>

			<cfquery name="insertMetadata" datasource="#coreDSN#">
				merge into DOC_SNAPSHOT_DATA x
				using (
				Select
				g.* from (
				select
				  (select doc_snapshot_cd from doc_snapshot_base where user_id||'_'||doc_setup_id||'_'||process_id = j.doc_snapshot_cd) as doc_snapshot_cd,
				  j.key AS KEY,
				  j.value AS VALUE,
				  j.value as TEMP_VALUE
				from
					(#preserveSingleQuotes(resultsql)#) j) g where g.doc_snapshot_cd is not null
				) y
			    on (y.doc_snapshot_cd=x.doc_snapshot_cd and UPPER(y.KEY)=UPPER(x.KEY))
			    when not matched then
			      insert (
			      		X.DOC_SNAPSHOT_DATA_CD,
			      		X.DOC_SNAPSHOT_CD,
			      		X.KEY,
			      		X.VALUE,
			      		x.temp_value,
			      		x.creation_dt)
				  values (
				    DOC_SNAPSHOT_DATA_SEQ.nextval,
				  	y.doc_snapshot_cd,
				  	y.key,
				  	y.value,
				  	y.temp_value,
				  	SYSDATE)
			  	WHEN MATCHED THEN UPDATE
			  		SET x.temp_value = y.temp_value,
			  			x.modification_dt = sysdate,
			  			x.value=y.value,
			  			x.modification_id=1
			</cfquery>
		</cfloop>
		<cfquery name="updateQuestion1" datasource="#coreDSN#">
			update doc_snapshot_data set value = replace(value,'1668579','975673') where DOC_SNAPSHOT_CD='#getComments.DOC_SNAPSHOT_CD#' and key='QUESTION_COMPARISONS'
		</cfquery>
		<cfquery name="updateQuestion2" datasource="#coreDSN#">
			update doc_snapshot_data set value = replace(value,'1668610','975736') where DOC_SNAPSHOT_CD='#getComments.DOC_SNAPSHOT_CD#' and key='QUESTION_COMPARISONS'
		</cfquery>
		<cfquery name="updateQuestion3" datasource="#coreDSN#">
			update doc_snapshot_data set value = replace(value,'1668581','975675') where DOC_SNAPSHOT_CD='#getComments.DOC_SNAPSHOT_CD#' and key='QUESTION_COMPARISONS'
		</cfquery>
		<cfquery name="updateQuestion4" datasource="#coreDSN#">
			update doc_snapshot_data set value = replace(value,'1668611','975672') where DOC_SNAPSHOT_CD='#getComments.DOC_SNAPSHOT_CD#' and key='QUESTION_COMPARISONS'
		</cfquery>
		<cfquery name="updateQuestion5" datasource="#coreDSN#">
			update doc_snapshot_data set value = replace(value,'1668580','975674') where DOC_SNAPSHOT_CD='#getComments.DOC_SNAPSHOT_CD#' and key='QUESTION_COMPARISONS'
		</cfquery>
		<cfquery name="updateQuestion6" datasource="#coreDSN#">
			update doc_snapshot_data set value = replace(value,'1668587','975671') where DOC_SNAPSHOT_CD='#getComments.DOC_SNAPSHOT_CD#' and key='QUESTION_COMPARISONS'
		</cfquery>
		<cfquery name="updateQuestion7" datasource="#coreDSN#">
			update doc_snapshot_data set value = replace(value,'1669402','975790') where DOC_SNAPSHOT_CD='#getComments.DOC_SNAPSHOT_CD#' and key='QUESTION_COMPARISONS'
		</cfquery>
		<cfquery name="updateQuestion8" datasource="#coreDSN#">
			update doc_snapshot_data set value = replace(value,'1668582','975665') where DOC_SNAPSHOT_CD='#getComments.DOC_SNAPSHOT_CD#' and key='QUESTION_COMPARISONS'
		</cfquery>
</cfloop>


<cfcatch>
				  	<cfdump var="#cfcatch#"><cfabort>

			  	</cfcatch>
				</cftry>
<cfdump var="Done"><cfabort>
