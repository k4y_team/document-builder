<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<style type="text/css">
		.row-form select {
			background-color: ##FFFFFF !important;
		}
		.table div.checker {
			margin: 0 !important;
		}
	</style>
	<div class="row-fluid">
		<form name="frmFilter" id="frmFilter" action="#request.app.self##XFA.Search#" method="POST">
			<input type="hidden" name="fuseaction" value="">
			<input type="hidden" name="rpcCall" value="">
			<input type="hidden" name="forceRefresh" value="true">

			<!--- <input type="hidden" id="instance_id" name="instance_id" value="#attributes.instance_id#"> --->


			<input type="hidden" id="export" name="filter_export" value="">
			<div class="headInfo">
				<div class="row-form clearfix no-border width1000">
					<input type="hidden" name="filter_params" value="" id="filter_params">
					<input type="hidden" name="filter_user_role_code" value="ADMIN">

					<div class="span1 control-label" style="width:90px;">Document Type:</div>
					<div class="span3">
						<k4y:multiSELECT
							name="filter_doc_letter_instance_cd"
							query="documentInstancesQry"
							keyfield="instance_id"
							descField="instance_name"
							selectValues="4"
							single = true
							naDesc="-- Select --"
							onChange="onReportTypeChange()"

						/>
					</div>
	      		</div>
	      		<div class="row-form clearfix no-border width1000" id="filterSection" style="display:none;">
					<div class="span1 control-label" style="width:90px;">Setup Filter:</div>
					<div class="span8" id="documentsFilter">

					</div>
	      		</div>
			</div>

		</br>
		<div id="documentsTable">
		</div>
		</form>
	</div>
	<script type="text/javascript">
		function onReportTypeChange(id){
        	var instanceId = $("input[name=filter_doc_letter_instance_cd]").val();
			$("##filterSection").hide();
        	$("##documentsFilter").hide();
        	$("##documentsTable").html("");
			if (instanceId >0){
				$("##filterSection").show();
        		$("##documentsFilter").show();
        		$.ajax({
            	url: '#request.app.self#?fuseaction=ReportSetup.getInstanceFilter',
            	data: $('##frmFilter').serialize(),
            	type: 'POST',
            	dataType:'html',
	            }).done(function (result) {
	                 $("##documentsFilter").html(result);
	            });
            }
        };
	</script>
</cfoutput>