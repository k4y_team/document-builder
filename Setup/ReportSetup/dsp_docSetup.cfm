<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<style type="text/css">
		.dataTables_filter {
			margin-top: 0 !important;
			width: auto !important;
		}
		.dataTables_filter label {
			display: inline-block !important;
			width: auto;
		}
		.dataTables_filter .wrap-div {
			display: inline-block;
		}
		.dataTables_filter .wrap-div .control-label {
			display: inline-block;
			width: 38px;
		}
		.dataTables_filter .wrap-div .filter_select {
			margin-bottom: 0px;
			width: 210px;
			height: 22px !important;
		}
	</style>

	<div class="row-fluid">
		<div class="head clearfix">
			<div class="isw-list"></div>
			<h1>Document Setups</h1>
			<h1 class="total_records">Total:<span id="total_records"></span> </h1>
		</div>
		<div class="block-fluid table-sorting clearfix">
			<TABLE class="table" id="setuplkTbl">
			</TABLE>
		</div>
	</div>

	<div style="display: none;" id="fake-control-toolbar">
		<cfif NOT readOnly>
			<div class="btn-group" name="addButtonDiv" id="addButtonDiv">
				<k4y:button type="button" onclick="loadTabContent('#xfa.edit#', '&doc_setup_id=0');return false;" icon="ADD" dictKey="ADD_NEW">
					Add New
				</k4y:button>
			</div>
		</cfif>
	</div>
	<script type="text/javascript">
        var createFilterData = function() {
			var filterArray = [];
			$("div.headInfo input,select").each(function() {
				var filterStruct = {};
				filterStruct["name"] = $(this).prop("name");
				filterStruct["value"] = $(this).val();
				filterArray.push(filterStruct);
			});
			return filterArray;
		}

		$(function() {
			var oTable = $("##setuplkTbl").dataTable({
				"bAutoWidth": false,
				"aaSorting": [[ 1, "desc" ]],
				"bStateSave": true,
				"aLengthMenu": [[10,25, 50, 100], [10,25, 50, 100]],
				"sPaginationType": "full_numbers",
				"sDom": '<"table-toolbar tar"><"top">frt<"bottom"ilp><"clear">',
				"bServerSide": true,
				"fnServerParams": function ( aoData ) {
					$.merge(aoData, createFilterData());
				},
				"aoColumns": [
					{"sTitle": "Setup Name", "sName": "doc_setup_name","sWidth":200, "bSortable": true},
					{"sTitle": "Document Template ", "sName": "letter_template_name","sWidth":200,"bSortable": true},
					{"sTitle": "Document Type", "sName": "instance_name", "bSortable": true},
					{"sTitle": "Setup Details", "sName": "DETAILS", "bSortable": true},
					{"sName": "doc_letter_template_cd", "bVisible": false},
					{"sName": "doc_setup_id", "bVisible": false},
					{"sName": "instance_id", "bVisible": false},
					{"sTitle": "Activity End Date", "sName": "ACTIVITY_END_DT", "bSortable": true},
					{"sTitle": "Generate Date", "sName": "GENERATE_DT", "bSortable": true},
					{"sTitle": "Publish Date", "sName": "PUBLISH_DT", "bSortable": true}
				],
				"aoColumnDefs": [
					{"mRender": function ( data, type, row ) {
						var cellContent =  data ;
						cellContent = cellContent + '<a id="preview_form" style="float:right;" target="_blank" href="#request.app.myself##xfa.Preview#&&instance_id=' + row[6] + '&isDocument=1&docLetterTemplateCD=' + row[4] + '" return false;"><span class="ico ico-search"></span></a>';
						return cellContent;
					}, "aTargets": [ 1 ]},
					{"mRender": function ( data, type, row ) {
						var cellContent = '<a href="javascript:;" onClick="loadTabContent(\'#xfa.Edit#\', \'doc_setup_id=' + row[5] + '\')">' + data + '</a>';
						return cellContent;
					}, "aTargets": [0]},
					{"mRender": function(data, type, row) {
							if(!row[7]==""){
                      	 		return moment(row[7]).format(MEDSIS_CUSTOM.dateFormat.momentDateFormat);
							}else{
                      	 		return "";
                      	 	}                       },"aTargets": [7]},
					{"mRender": function(data, type, row) {
							if(!row[8]==""){
                      	 		return moment(row[8]).format(MEDSIS_CUSTOM.dateFormat.momentDateFormat)+ ', ' + moment(row[8]).format(MEDSIS_CUSTOM.dateFormat.momentTimeFormat);
							}else{
                      	 		return "";
                      	 	}                    },"aTargets": [8]},
					{"mRender": function(data, type, row) {
							if(!row[9]==""){
                      	 		return moment(row[9]).format(MEDSIS_CUSTOM.dateFormat.momentDateFormat)+ ', ' + moment(row[9]).format(MEDSIS_CUSTOM.dateFormat.momentTimeFormat);
							}else{
                      	 		return "";
                      	 	}
                    },"aTargets": [9]}
				],
				"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
					oSettings.jqXHR = $.ajax( {
						"dataType": 'json',
						"type": "POST",
						"url": "#request.app.self#?fuseaction=#XFA.getData#",
						"data": aoData,
						"success": function(result) {
							$("##total_records").html(result.iTotalRecords);
							fnCallback(result);
						}
					});
				}
			});

			$(document).off('change', '##instance_id');
			$(document).on('change', '##instance_id', function (e) {
				e.preventDefault();
				oTable.fnDraw();
			});
			$(document).off('change', '##course_cd');
			$(document).on('change', '##course_cd', function (e) {
				e.preventDefault();
				oTable.fnDraw();
			});

			$("div.dataTables_filter").append($('##fake-filter-toolbar').children().detach());
			$("div.table-toolbar").append($('##fake-control-toolbar').children().detach());
			$('##fake-filter-toolbar').remove();
			$('##fake-control-toolbar').remove();
		});
	</script>
</cfoutput>