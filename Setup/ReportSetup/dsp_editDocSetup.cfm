<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">

<cfoutput>
<div class="row-fluid">
	<div class="head clearfix">
		<div class=" isw-edit"></div>
		<cfif setupQry.recordcount EQ 0>
			<h1 id="formTitle">Add Document Setup</h1>
		<cfelse>
			<h1 id="formTitle">Edit Document Setup</h1>
		</cfif>
	</div>

	<div class="block-fluid clearfix">
		<form name="templatesfrmEdit" id="templatesfrmEdit" action=""  method="post">
			<div class="block-fluid accordion clearfix">



			#formHtml#
			<div id="configForm">
			</div>
			<!---
			--->
			<div class="footer" id="actions">
				<cfset UserSecurity = application.wirebox.getInstance("API@UserDirectory").getUserSecurity(application.cbController.getSetting("ud_instance_id"), userSessionData.USER_ID)>
				<cfset datesAccessQry = UserSecurity.checkAccessByCodeList('DOCUMENT_SETUP_DATES', true)/>
				<cfset setupAccessQry = UserSecurity.checkAccessByCodeList('DOCUMENT_REPORT_SETUP', true)/>
				<cfif setupAccessQry.access_type_id EQ 2 or datesAccessQry.access_type_id EQ 2>
					<smart-button id="saveDocSetup" name="saveDocSetup" icon="ico ico-save" class="btn-primary" triggeredText="Saving..."> Save </smart-button>
				</cfif>
				<cfif setupAccessQry.access_type_id NEQ 1>
				<cfif attributes.doc_setup_id GT 0>
						<smart-button
									id="deleteDocSetup"
									name="deleteDocSetup"
									icon="ico ico-trash"
									class="btn-red"
									confirmation = '{
									     "TITLE": "Delete Confirmation",
									     "BODY": "Are you sure you want to delete this setup?",
									     "PLACEMENT": "top",
									     "BUTTONS": [
									          {
									               "ICON": "ico ico-check",
									               "LABEL": "Yes",
									               "KEEP_OPEN": false,
									               "ON_CLICK": "deleteDocSetup()",
									               "TRIGGER": true
									          },
									          {
									               "ICON": "ico ico-times",
									               "LABEL": "No",
									               "KEEP_OPEN": false,
									               "ON_CLICK": "",
									               "TRIGGER": false
									          }
									     ]
									}'
									triggeredText="Deleting..."
									showConfirmation = 'true'
									> Delete
								</smart-button>
					</cfif>
				</cfif>
				<smart-button id="cancelBtn" name="cancelBtn" icon="ico ico-cancel"> Cancel </smart-button>
			</div>
		</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	setTimeout(function() {
		onInstanceChange();
	},100);
	$('##cancelBtn').on('click', function() {
		refreshActiveTab();
	});
	$('##saveDocSetup').on('click', function(ev) {
		ev.preventDefault();
		$.ajax({
			type: "POST",
			data: $('##templatesfrmEdit').serialize(),
			url: "#request.app.self#?fuseaction=#xfa.save#",
			success: function(data) {
				 var retData = JSON.parse(data);
				 if (retData.STATUS == 'success' ) {
			         refreshActiveTab();
					 UIHelper.MsgBox.drawMessage("success", retData.MESSAGE);
				} else {
			        renderValidationMessages($("##templatesfrmEdit"), { MESSAGE: retData.MESSAGE});
			        UIHelper.getComponent('saveDocSetup').setButtonState('active');
			     }
			}
		});
	});
});
function deleteDocSetup (){
	$.ajax({
		type: "POST",
		data: $('##templatesfrmEdit').serialize(),
		url: "#request.app.self#?fuseaction=#xfa.delete#",
		success: function(result) {
			var retData = JSON.parse(result);
			refreshActiveTab();
 			if (retData.STATUS == 'success' ) {
				refreshActiveTab();
				UIHelper.MsgBox.drawMessage("success", retData.MESSAGE);
			} else {
				UIHelper.MsgBox.drawMessage("error", retData.MESSAGE);
			    UIHelper.getComponent('deleteDocSetup').setButtonState('active');
			}
		}
	});
}
</script>
</cfoutput>