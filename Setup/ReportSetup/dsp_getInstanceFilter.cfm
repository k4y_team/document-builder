<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
<script type="text/javascript">
   var submitEvaluationFilter = function(filter) {
   	var filterData = {};
   	var data = filter.search;
   	filterData['SEARCH_STRING'] = data.search_string;
   	for(var i = 0;i<data.search_fields.length;i++) {
		if(typeof data.search_fields[i]['ID'] !== 'undefined') {
			if(data.search_fields[i]['ID'].split('-')[1]=='PUBLISHED_DT'){
				filterData[data.search_fields[i]['ID'].split('-')[1]] = {'VALUE':data.search_fields[i].VAL,'TYPE':data.search_fields[i].TYPE};
			}else{
				filterData[data.search_fields[i]['ID'].split('-')[1]] = data.search_fields[i].VAL;
			}
		} else {
			filterData[data.search_fields[i]['NAME'].split('-')[1]] = {'VALUE': data.search_fields[i].DESC,'TYPE':data.search_fields[i].TYPE};
		}
	}
   	  $('##filter_params').val(JSON.stringify(filterData));
   	  getDocumentsList();
   }
   var xhr = null;
   var getDocumentsList= function(filterData) {
   		if( xhr != null ) {
        	xhr.abort();
            xhr = null;
     	}
   		var formData = $("##frmFilter").serialize();
 	    $("##documentsTable").html('<div class="tac"><span class="c_loader"></span><span>Loading...</span></div>');
 	    xhr  = $.ajax({
     		url: '#request.app.self#?fuseaction=ReportSetup.getDocumentsSetup',
          	data:formData,
          	type: 'POST',
          	dataType:'html',
      	}).done(function (result) {
           $("##documentsTable").html(result);
      	});
 	}
</script>
<cfset sessionStorage = application.wirebox.getInstance(dsl="coldbox:plugin:SessionStorage") />
<cfset user_Id = sessionStorage.getVar("LoggedInUser").USER_ID >
<cfset EntityRestrictions =  application.wirebox.getInstance(name='sis_core.model.blAutomation.EntityRestrictions') />
<cfset programRestrictionsList = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_PROGRAM',userId=user_Id,securityCode="DOCUMENT_BUILDER_MODULE") />
<cfset courseRestrictionsList = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_COURSE',userId=user_Id,securityCode="DOCUMENT_BUILDER_MODULE") />
<cfset Restrictions = application.wirebox.getInstance(dsl="medsisOld.modules.DocumentBuilder.Model.BL.Restrictions") />

<cfset entityCodeList = "">

<cfloop list="#filterEntity.GETPROPERTIESASLIST()#" index="property">
	<cfset entityCodeList = ListAppend(entityCodeList,"#instanceConfig.SETUP_FILTER_ENTITY#"&"."&"#property#")>
</cfloop>

<cfset restrictionData = {}/>
<cfset filterFieldsConditions = {}>

<cfset restrictionData = {"TesSetupFilter.PROGRAM_NAME" : "DOCUMENT_BUILDER_PROGRAM","TesSetupFilter.TEACHING_LOCATION":"DOCUMENT_BUILDER_LOCATION","UG_TesSetupFilter.TEACHING_LOCATION":"DOCUMENT_BUILDER_LOCATION","UG_TesSetupFilter.COURSE_NAME" : "DOCUMENT_BUILDER_COURSE"} >
<cfif programRestrictionsList NEQ "*">
	<cfset setupQryResult = queryBuilder.runQuery(sql=Restrictions.getDocumentSetupsSQL(user_Id),datasource=application.wirebox.getInstance(dsl="coldbox:setting:datasource")) />
	<cfset setupList = ValueList(setupQryResult.ACCESS_LIST) />
	<cfset StructAppend(filterFieldsConditions,{"TesSetupFilter.TEMPLATE_NAME" : "SetupDetail.DOC_SETUP_ID in (#setupList#)","TesSetupFilter.SETUP_NAME": "SetupDetail.DOC_SETUP_ID in (#setupList#)"},true)/>
</cfif>
<cfif courseRestrictionsList NEQ "*">
	<cfset setupQryResult = queryBuilder.runQuery(sql=Restrictions.getDocumentSetupsSQL(user_Id),datasource=application.wirebox.getInstance(dsl="coldbox:setting:datasource")) />
	<cfset setupList = ValueList(setupQryResult.ACCESS_LIST) />
	<cfset StructAppend(filterFieldsConditions,{"UG_TesSetupFilter.TEMPLATE_NAME" : "SetupDetail.DOC_SETUP_ID in (#setupList#)","UG_TesSetupFilter.SETUP_NAME": "SetupDetail.DOC_SETUP_ID in (#setupList#)"},true)/>
</cfif>


<k4y:entityFilter
	name="documents_setup_Filter"
	showMode="Mode1"
	filterID=""
	placeholder="All Setups"
	autoSubmit="true"
	restrictionData = "#restrictionData#"
	defaultFields ="#entityCodeList#"
	filterFieldsConditions="#filterFieldsConditions#"
	defaultFilter = ""
	renderCallback="renderEvaluationCallback"
	submitCallback="submitEvaluationFilter"
	autoPersistence="true"
	persistenceKey="#attributes.FILTER_DOC_LETTER_INSTANCE_CD#_DOC_DOCUMENTS_SETUP_FILTER_#user_Id#"
	userID=user_Id
	allowSaveFilter="true"
/>
</cfoutput>