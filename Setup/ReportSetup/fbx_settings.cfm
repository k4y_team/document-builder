<cfset activeTab = "Templates">
<cfmodule template="#fusebox.rootPath##request.app.self#"
	fuseaction="security.setfuseaccess"
	fuseActionList ="showDocumentsSetup,editSetup,getData,saveSetup,deleteSetup,getInstanceForm,getInstanceFilter,getDocumentsSetup"
	functionName = "DOCUMENT_REPORT_SETUP"
	fuseactionName="#fusebox.fuseaction#"
	security="#security#"
>

<cfif not isDefined("security.checked")>
	<cf_location url="#request.app.self#?fuseaction=security.shownotauth">
</cfif>