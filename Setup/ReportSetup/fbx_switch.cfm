<cfswitch expression="#fusebox.fuseaction#">

	<cfcase value="showDocumentsSetup">
		<cfset XFA.self = "DocTemplate.showTemplates&INSTANCE_ID=#ATTRIBUTES.INSTANCE_ID#">
		<cfset XFA.Edit = "ReportSetup.editSetup">
		<cfset XFA.Preview	= "DocTemplate.templatePreview&fb=1">
		<cfset XFA.Print = "DocTemplate.printPreview&INSTANCE_ID=#ATTRIBUTES.INSTANCE_ID#" />
		<cfset xfa.GetData = "ReportSetup.getData" />
		<cfset sortDataVO = createobject('component','sis_core.model.SortOptions').init()>
		<cfset sortDataVO.initFromStruct(attributes) />
		<cfif sortDataVO.getFIELD() EQ "">
			<cfset SortDataVO.setFIELD('LETTER_TEMPLATE_NAME')>
			<cfset SortDataVO.setDIRECTION('ASC')>
		</cfif>
		<cfset searchFilter = application.wirebox.getInstance("sis_core.model.SearchFilter") >
		<cfset searchFilter.initFromStruct(attributes) >
		<cfset docTemplateManager = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init()>
		<cfset docLetterTemplateQry = docTemplateManager.getAll(searchFilter, SortDataVO)>
		<cfset doaInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />

		<cfset documentInstancesQry = doaInstances.getInstances() />
		<!------>
		<cfset readOnly = true>
		<cfif IsDefined("security.accessType") and security.accessType EQ "1">
            <cfset readOnly = false>
        </cfif>
		<cfset XFA.Search	= "DocReport.showDocumentsResults">
		<cfset documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset documentInstancesQry = documentInstances.getInstances(isUsed=true) />
		<cfset filterObj = application.wirebox.getInstance("sis_core.model.SearchFilter")>
		<cfset filterObj.initFromStruct(attributes)>
		<cfinclude template="dsp_DocSetupFilter.cfm" />
	</cfcase>
	<cfcase value="getDocumentsSetup">

		<cfset XFA.self = "DocTemplate.showTemplates&INSTANCE_ID=#ATTRIBUTES.INSTANCE_ID#">
		<cfset XFA.Edit = "ReportSetup.editSetup">
		<cfset XFA.Preview	= "DocTemplate.templatePreview&fb=1">
		<cfset XFA.Print = "DocTemplate.printPreview&INSTANCE_ID=#ATTRIBUTES.INSTANCE_ID#" />
		<cfset xfa.GetData = "ReportSetup.getData" />
		<cfset sortDataVO = createobject('component','sis_core.model.SortOptions').init()>
		<cfset sortDataVO.initFromStruct(attributes) />
		<cfif sortDataVO.getFIELD() EQ "">
			<cfset SortDataVO.setFIELD('LETTER_TEMPLATE_NAME')>
			<cfset SortDataVO.setDIRECTION('ASC')>
		</cfif>
		<cfset searchFilter = application.wirebox.getInstance("sis_core.model.SearchFilter") >
		<cfset searchFilter.initFromStruct(attributes) >
		<cfset docTemplateManager = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init()>
		<cfset docLetterTemplateQry = docTemplateManager.getAll(searchFilter, SortDataVO)>
		<cfset doaInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />

		<cfset documentInstancesQry = doaInstances.getInstances() />
		<!------>
		<cfset readOnly = true>
		<cfif IsDefined("security.accessType") and security.accessType EQ "1">
            <cfset readOnly = false>
        </cfif>
		<cfinclude template="dsp_DocSetup.cfm" />
		<cfabort>
	</cfcase>


	<cfcase value="getInstanceFilter">
		<cfset queryBuilder = application.wirebox.getInstance(name="sis_core.model.blAutomation.queryBuilder") />
		<cfset documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceConfig = documentInstances.getInstanceConfig(attributes.FILTER_DOC_LETTER_INSTANCE_CD) />
		<cfset attributes.instance_id = attributes.FILTER_DOC_LETTER_INSTANCE_CD>
		<cfif !StructIsEmpty(instanceConfig) and structKeyExists(instanceConfig,"SETUP_FILTER_ENTITY") >
			<cfset filterEntity = queryBuilder.getEntity(instanceConfig.SETUP_FILTER_ENTITY) />
		<cfelse>
			<cfset filterEntity = "" >
		</cfif>
		<cfinclude template="dsp_getInstanceFilter.cfm" />
		<cfabort>
	</cfcase>

	<cfcase value="getData">
		<cfset SearchFilter = application.wirebox.getInstance("sis_core.model.SearchFilter") >
		<cfset SearchFilter.initFromStruct(attributes) >
		<cfset sortDataVO = createobject('component','sis_core.model.SortOptions').init()>
		<cfset sortDataVO.initFromStruct(attributes) />
		<cfset DocSetup = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSetup" ).init()/>
		<cfset reportDocumentsQry = DocSetup.getDocSetups(SearchFilter)>
		<cfset dataTableRenderer = application.wirebox.getInstance(name="sis_core.model.DataTableRenderer",initArguments={dataQry=reportDocumentsQry, columns=attributes.sColumns, metadata={sEcho:attributes.sEcho}})>
		<cfoutput>#dataTableRenderer.render()#</cfoutput>
		<cfabort>
	</cfcase>

	<cfcase value="editSetup">
		<cfset xfa.save = "ReportSetup.saveSetup" />
		<cfset xfa.delete = "ReportSetup.deleteSetup" />
		<cfset entityRegistry = application.wirebox.getInstance("EntityRegistry@CORE") />
		<cfset DocSetup = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSetup" ).init()/>
		<cfset setupQry = DocSetup.getDocSetupByPk(attributes) >
		<cfset queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils") />
		<cfset params = queryUtils.queryToStruct(setupQry)>
		<cfset EntityRestrictions =  application.wirebox.getInstance(name='sis_core.model.blAutomation.EntityRestrictions')/>
		<cfset userSessionData = application.wirebox.getInstance(dsl='coldbox:plugin:SessionStorage' ).getVar("LoggedInUser")>
	 	<cfset instanceRestrictionSql = EntityRestrictions.getUserRestrictions(restrictionType='DOCUMENT_BUILDER_INSTANCE',userId=userSessionData.USER_ID,securityCode="DOCUMENT_SETUP") />
		<cfif instanceRestrictionSql NEQ "*">
			<cfset params.INSTANCE_RESTRICTIONS_LIST = listToArray(instanceRestrictionSql,",",false,true)>
		</cfif>
		<cfset readOnly = true>
		<cfif IsDefined("security.accessType") and security.accessType EQ "1">
            <cfset readOnly = false>
        </cfif>
		<cfset formHtml = EntityRegistry.renderEntityForm(entityCode="DOCUMENT_SETUP_FORM", rbAlias=moduleConfig.getRBSettings().ALIAS,params=params, readOnly = readOnly,values=params)/>
		<cfinclude template="dsp_editDocSetup.cfm" />
	</cfcase>

	<cfcase value="saveSetup">
		<cfset DocSetup = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSetup" ).init()/>
		<cfset Response = application.wirebox.getInstance("sis_core.model.Response").init()>
		<cfset result = DocSetup.save(attributes) />
		<cfif result>
			<cfset Response.setSTATUS("success")>
			<cfset Response.setREDIRECT_URL("#request.app.myself#ReportSetup.editSetup")>
		<cfelse>
			<cfset Response.setSTATUS("error")>
		</cfif>
		<cfoutput>#Response.generateJSON()#</cfoutput><cfabort>

	</cfcase>

	<cfcase value="deleteSetup">
		<cfset DocSetup = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSetup" ).init()/>
		<cfset Response = application.wirebox.getInstance("sis_core.model.Response").init()>
		<cfset result = DocSetup.delete(attributes) />
		<cfif result>
			<cfset Response.setSTATUS("success")>
			<cfset Response.setREDIRECT_URL("#request.app.myself#ReportSetup.editSetup")>
		<cfelse>
			<cfset Response.setSTATUS("error")>
		</cfif>
		<cfoutput>#Response.generateJSON()#</cfoutput><cfabort>
	</cfcase>

	<cfcase value="getInstanceForm">
		<cfset DocSetup = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocSetup" ).init()/>
		<cfif attributes.doc_setup_id EQ "">
			<cfset  attributes.doc_setup_id = 0 >
		</cfif>
		<cfset doaInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset instanceConfig = doaInstances.getInstanceConfig(attributes.instance_id) />
		<cfset setupQry = DocSetup.getDocSetupByPk(attributes) >
		<cfset queryUtils = application.wirebox.getInstance(dsl="coldbox:myplugin:QueryUtils") />
		<cfset params = queryUtils.queryToStruct(setupQry)>
		<cfset entityRegistry = application.wirebox.getInstance("EntityRegistry@CORE") />

		<cfif  !StructIsEmpty(instanceConfig) and structKeyExists(instanceConfig,"SETUP_CONFIG_FORM") >
			<cfset configFormHtml = EntityRegistry.renderEntityForm(entityCode="#instanceConfig.SETUP_CONFIG_FORM#", rbAlias=moduleConfig.getRBSettings().ALIAS,params=params,values=params)/>
		<cfelse>
			<cfset configFormHtml = "" />
		</cfif>
		<cfinclude template="dsp_getInstanceForm.cfm" />
		<cfabort>
	</cfcase>

</cfswitch>