<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<cfif DocLetterTemplateFVO.getDOC_LETTER_TEMPLATE_CD() EQ "">
		<cfset disabled="">
	<cfelse>
		<cfset disabled="disabled">
	</cfif>
	<cfset availableTags=valuelist(availableTags.tag_code,',')>
	<cfset taglist="">
	<cfloop index = "ListElement" list ="#availableTags#">
	   <cfset a=Insert('{',ListElement,0)>
	   <cfset b=Insert(a,'}',0)>
	   <cfset taglist =listAppend(taglist,b)>
	</cfloop>
	<div class="row-fluid">
		<div class="head clearfix">
			<div class=" isw-edit"></div>
			<cfif DocLetterTemplateFVO.getDOC_LETTER_TEMPLATE_CD() EQ ''>
				<h1 id="formTitle">Add Template</h1>
			<cfelse>
				<h1 id="formTitle">Edit Template</h1>
			</cfif>
		</div>
		<div class="block-fluid clearfix">
			<form name="templatesfrmEdit" id="templatesfrmEdit" action=""  method="post">
				<input type="hidden" value="#taglist#"  id="taglist">
				<input type="hidden" name="DOC_LETTER_TEMPLATE_CD" value="#DocLetterTemplateFVO.getDOC_LETTER_TEMPLATE_CD()#">
				<div class="row-form clearfix no-border width800">
					<div class="span2 control-label">Template <span class="red-txt"> *</span>: </div>
					<div class="span10">
						<k4y:inputTEXT name="LETTER_TEMPLATE_NAME" id="template_name" value="#DocLetterTemplateFVO.getLETTER_TEMPLATE_NAME()#" maxlength="100" readonly="#readOnly#"/>
					</div>
					<div id="detailDiv" class="pull-right">
						<cfset statusCode = "ACTIVE">
						<cfif DocLetterTemplateFVO.getSTATUS() NEQ "">
							<cfset statusCode = DocLetterTemplateFVO.getSTATUS()>
						</cfif>
						<div>
							<k4y:inputTOGGLE readonly="#readOnly#" name="status" on_label="ACTIVE" off_label="INACTIVE" on_value="ACTIVE" off_value="INACTIVE" value="#statusCode#"/>
						</div>
					</div>
		        </div>
		        <div class="row-form clearfix no-border width800">
			        <div class="span2 control-label"> Document Type:  <span class="red-txt"> *</span>: </div>
					<div class="span5">
						<k4y:multiSELECT
							name="instance_id"
							id="instance_id"
							query="documentInstancesQry"
							keyfield="INSTANCE_ID"
							descField="INSTANCE_NAME"
							selectValues="#DocLetterTemplateFVO.getINSTANCE_ID()#"
							single="true"
							readonly="#readOnly#"
						/>
					</div>
		        </div>
				<div class="row-form clearfix no-border width800">
					<div class="span2 control-label">Body <span class="red-txt"> *</span>: </div>
					<div class="span10">
						<cfif NOT readOnly>
						    <cfset bodyConfig = StructNew()>
							<cfset bodyConfig['toolbar'] = "K4Y_DOC_TAGS">
							<cfset bodyConfig['width'] = "100%">
							<cfset bodyConfig['height'] = "300px">
							<k4y:HtmlEditor externalCSS="#application.cbController.getSetting('modulesApplicationURL')#/modules/AgreementLetters/includes/css/letter.css" name="HTML_LETTER_TEMPLATE_BODY" config=#bodyConfig# value="#DocLetterTemplateFVO.getHTML_LETTER_TEMPLATE_BODY()#"/>
						<cfelse>
							<div class="table-border wrap">
								#DocLetterTemplateFVO.getHTML_LETTER_TEMPLATE_BODY()#
							</div>
						</cfif>
					</div>
		        </div>
				<div class="row-form clearfix no-border width800">
					<div class="span2 control-label">Instructions: </div>
					<div class="span10">
						<cfif NOT readOnly>
							<cfset instructionConfig = StructNew()>
							<cfset instructionConfig['toolbar'] = "K4Y_LOA">
							<cfset instructionConfig['width'] = "100%">
							<cfset instructionConfig['height'] = "150px">
							<k4y:HtmlEditor externalCSS="#application.cbController.getSetting('modulesApplicationURL')#/modules/AgreementLetters/includes/css/letter.css" name="INSTRUCTIONS" config=#instructionConfig# value="#DocLetterTemplateFVO.getINSTRUCTIONS()#"/>
						<cfelse>
							<div class="table-border wrap">
								#DocLetterTemplateFVO.getINSTRUCTIONS()#
							</div>
						</cfif>
					</div>
				</div>
		        <cfquery name="signQry" dbtype="query">
			   		SELECT Distinct(DOC_SIGNATURE_CD),DISPLAY_NAME,FILE_NAME FROM CompleteSignatures.FILENAMES
				</cfquery>
		        <div class="row-form clearfix no-border width800">
					<div class="span2 control-label">Signature: </div>
					<div class="span5">
						<k4y:multiselect
							id="DOC_SIGNATURE_CD"
							name="DOC_SIGNATURE_CD"
							query="signQry"
							keyfield="DOC_SIGNATURE_CD"
							descField="DISPLAY_NAME"
							selectValues="#DocLetterTemplateFVO.getDOC_SIGNATURE_CD()#"
							readonly="#readOnly#"
							single="true"
						/>
					</div>
		        </div>
				<div class="footer" id="actions">
					<cfif DocLetterTemplateFVO.getDOC_LETTER_TEMPLATE_CD() neq "">
						<k4y:button type="button" class="btn btn-mini floatl" id="previewTemplates" name="previewTemplates" onClick="window.open('#request.app.myself##XFA.preview#&docLetterTemplateCD=#DocLetterTemplateFVO.getDOC_LETTER_TEMPLATE_CD()#', '_blank');" icon="VIEW" dictKey="PREVIEW">
							Preview
						</k4y:button>
					</cfif>

					<cfif NOT readOnly>
						<smart-button id="saveTemplates" name="saveTemplates" icon="ico ico-save" class="btn-primary" triggeredText="Saving..."> Save </smart-button>
						<cfif disabled NEQ "">
							<smart-button
								id="deleteTemplates"
								name="deleteTemplates"
								icon="ico ico-trash"
								class="btn-red"
								confirmation = '{
								     "TITLE": "Delete Confirmation",
								     "BODY": "Are you sure you want to delete this template?",
								     "PLACEMENT": "top",
								     "BUTTONS": [
								          {
								               "ICON": "ico ico-check",
								               "LABEL": "Yes",
								               "KEEP_OPEN": false,
								               "ON_CLICK": "deleteDocTemplates()",
								               "TRIGGER": true
								          },
								          {
								               "ICON": "ico ico-times",
								               "LABEL": "No",
								               "KEEP_OPEN": false,
								               "ON_CLICK": "",
								               "TRIGGER": false
								          }
								     ]
								}'
								triggeredText="Deleting..."
								showConfirmation = 'true'
								> Delete
							</smart-button>
						</cfif>
					</cfif>
					<smart-button id="cancelBtn" name="cancelBtn" icon="ico ico-cancel"> Cancel </smart-button>
				</div>
			</form>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
			<cfif NOT readOnly>
				$('##templatesfrmEdit').ajaxForm(function() {
					UIHelper.getComponent('saveTemplates').setButtonState('active');
					<cfif disabled NEQ "">
						UIHelper.getComponent('deleteTemplates').setButtonState('active');
					</cfif>
				});

				$('##saveTemplates').on('click', function() {
					$('##HTML_LETTER_TEMPLATE_BODY').val(CKEDITOR.instances.HTML_LETTER_TEMPLATE_BODY.getData());
					$('##INSTRUCTIONS').val(CKEDITOR.instances.INSTRUCTIONS.getData());
					$('##templatesfrmEdit').attr('action', '#request.app.self#?fuseaction=#xfa.save#').submit();
				});
			</cfif>

			$('##cancelBtn').on('click', function() {
				refreshActiveTab();
			});
		});

		<cfif NOT readOnly>
			function deleteDocTemplates() {
				$('##templatesfrmEdit').attr('action', '#request.app.self#?fuseaction=#xfa.delete#').submit();
			}
		</cfif>

		function showTags(){
			var infoDiv = document.getElementById('infoDiv');
			var elmIMG = document.getElementById('imgShowWnd');
			var elmTBL = document.getElementById('tbl1');

			if (infoDiv.style.display == 'none'){
				infoDiv.style.display = 'block';
				elmIMG.src = "images/hide_wnd.gif";
				elmTBL.width = '100%';
			}
			else{
				infoDiv.style.display = 'none';
				elmIMG.src = "images/show_wnd.gif";
				elmTBL.width = '';
			}
		}
		var available_tags = '#taglist#';
	</script>
</cfoutput>


