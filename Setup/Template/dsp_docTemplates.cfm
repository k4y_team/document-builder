<cfimport prefix="k4y" taglib="/SIS_CORE/cftags">
<cfoutput>
	<style type="text/css">
		.dataTables_filter {
			margin-top: 0 !important;
			width: auto !important;
		}
		.dataTables_filter label {
			display: inline-block !important;
			width: auto;
		}
		.dataTables_filter .wrap-div {
			display: inline-block;
		}
		.dataTables_filter .wrap-div .control-label {
			display: inline-block;
			width: 38px;
		}
		.dataTables_filter .wrap-div .filter_select {
			margin-bottom: 0px;
			width: 210px;
			height: 22px !important;
		}
	</style>

	<div class="row-fluid">
		<div class="head clearfix">
			<div class="isw-list"></div>
			<h1>Templates</h1>
			<h1 class="total_records">Total: #docLetterTemplateQry.RecordCount#</h1>
		</div>
		<div class="block-fluid table-sorting clearfix">
			<TABLE class="table" id="templatelkTbl">
			</TABLE>
		</div>
	</div>
	<div style="display:none" id="fake-filter-toolbar">
		<div class="wrap-div">
			<span style="width:100px;" class="control-label">Document Type:</span>
			<span style="width:200px;display:inline-block;">
				<k4y:multiselect
					id="letter_type"
					name="filter_letter_type"
					query="documentInstancesQry"
					keyfield="INSTANCE_ID"
					descField="INSTANCE_NAME"
					selectValues=""
					naDesc="-- All Documents --"
					dropdownFixed="true"
					single="true"
				/>
			</span>
		</div>
	</div>
	<div style="display: none;" id="fake-control-toolbar">
		<cfif NOT readOnly>
			<div class="btn-group" name="addButtonDiv" id="addButtonDiv">
				<k4y:button type="button" onclick="loadTabContent('#xfa.edit#', '&docLetterTemplateCD=0');return false;" icon="ADD" dictKey="ADD_NEW">
					Add New
				</k4y:button>
			</div>
		</cfif>
	</div>
	<script type="text/javascript">
        function createFilterData() {
			var filterArray = [];
			var filterStruct = {};
			filterStruct["name"] = "filter_lettertype";
			filterStruct["value"] = $('##letter_type').val();
			filterArray.push(filterStruct);
			return filterArray;
		};

		$(function() {
			var oTable = $("##templatelkTbl").dataTable({
				"bAutoWidth": false,
				"aaSorting": [[ 1, "desc" ]],
				"bStateSave": true,
				"aLengthMenu": [[25, 50,100], [25, 50,100]],
				"sPaginationType": "full_numbers",
				"sDom": '<"table-toolbar tar"><"top">frt<"bottom"ilp><"clear">',
				"bServerSide": true,
				"fnServerParams": function ( aoData ) {
					$.merge(aoData, createFilterData());
				},
				"aoColumns": [
					{"sTitle": "Template", "sName": "letter_template_name", "bSortable": true},
					{"sTitle": "Document Type", "sName": "instance_name", "bSortable": true},
					{"sTitle": "Last Session Used", "sName": "description", "bSortable": false},
					{"sTitle": "Last Modified", "sName": "last_modified", "bSortable": false},
					{"sTitle": "Status", "sName": "status", "bSortable": true, "sClass": "center"},
					{"sName": "doc_letter_template_cd", "bVisible": false},
					{"sName": "usedBy", "bVisible": false},
					{"sName": "instance_id", "bVisible": false}
				],
				"aoColumnDefs": [
					{"mRender": function ( data, type, row ) {
						var cellContent = '<a href="javascript:;" onClick="loadTabContent(\'#xfa.Edit#\', \'doclettertemplatecd=' + row[5] + '&instance_id=' + row[7] + '\')">' + data + '</a>';
						cellContent = cellContent + '<a id="preview_form" style="float:right;" target="_blank" href="#request.app.myself##xfa.Preview#&instance_id=' + row[7] + '&isDocument=1&docLetterTemplateCD=' + row[5] + '" return false;"><span class="ico ico-search"></span></a>';
						return cellContent;
					}, "aTargets": [ 0 ]},
					{"mRender": function ( data, type, row ) {
						var activeClass = 'label label-important';
						if (data == 'ACTIVE')
							activeClass = 'label label-success';
						return '<span class="' + activeClass + '">' + data + '</span>';
					}, "aTargets": [ 4 ]},
					{"mRender": function ( data, type, row ) {
						var d = new Date(data);
						var monthNames = [
						  "Jan", "Feb", "Mar",
						  "Apr", "May", "Jun", "Jul",
						  "Aug", "Sep", "Oct",
						  "Nov", "Dec"
						];

						var day = d.getDate();
						if (day <= 9)
							day = '0' + day;
						var monthIndex = d.getMonth();
						var year = d.getFullYear();
						return row[6] + ' [' + day + '-' + monthNames[monthIndex] + '-' + year + ']';
					}, "aTargets": [ 3 ]}
				],
				"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
					oSettings.jqXHR = $.ajax( {
						"dataType": 'json',
						"type": "POST",
						"url": "#request.app.self#?fuseaction=#XFA.getData#",
						"data": aoData,
						"success": function(result) {
							$("##total_records").html(result.iTotalRecords);
							fnCallback(result);
						}
					});
				}
			});

			$(document).off('change', '##letter_type');
			$(document).on('change', '##letter_type', function (e) {
				e.preventDefault();
				oTable.fnDraw();
			});

			$("div.dataTables_filter").append($('##fake-filter-toolbar').children().detach());
			$("div.table-toolbar").append($('##fake-control-toolbar').children().detach());
			$('##fake-filter-toolbar').remove();
			$('##fake-control-toolbar').remove();
		});
	</script>
</cfoutput>