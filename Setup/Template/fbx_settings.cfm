<cfset activeTab = "Templates">
<cfmodule template="#fusebox.rootPath##request.app.self#"
	fuseaction="security.setfuseaccess"
	fuseActionList ="showTemplates,editTemplate,templatePreview,printPreview,saveTemplate,deleteTemplate,getData"
	functionName = "DOC_TEMPLATE"
	fuseactionName="#fusebox.fuseaction#"
	security="#security#"
>

<cfif not isDefined("security.checked")>
	<cf_location url="#request.app.self#?fuseaction=security.shownotauth">
</cfif>