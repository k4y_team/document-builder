<cfswitch expression="#fusebox.fuseaction#">

	<cfcase value="showTemplates">
		<cfset XFA.self = "DocTemplate.showTemplates&INSTANCE_ID=#ATTRIBUTES.INSTANCE_ID#">
		<cfset XFA.Edit = "DocTemplate.editTemplate">
		<cfset XFA.Preview	= "DocTemplate.templatePreview&fb=1">
		<cfset XFA.Print = "DocTemplate.printPreview&INSTANCE_ID=#ATTRIBUTES.INSTANCE_ID#" />
		<cfset xfa.GetData = "DocTemplate.getData" />
		<cfset sortDataVO = createobject('component','sis_core.model.SortOptions').init()>
		<cfset sortDataVO.initFromStruct(attributes) />
		<cfif sortDataVO.getFIELD() EQ "">
			<cfset SortDataVO.setFIELD('LETTER_TEMPLATE_NAME')>
			<cfset SortDataVO.setDIRECTION('ASC')>
		</cfif>
		<cfset searchFilter = application.wirebox.getInstance("sis_core.model.SearchFilter") >
		<cfset searchFilter.initFromStruct(attributes) >
		<cfset DocTemplateManager = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init()>
		<cfset docLetterTemplateQry = DocTemplateManager.getAll(searchFilter, SortDataVO)>
		<cfset documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset documentInstancesQry = documentInstances.getInstances() />
        <cfset readOnly = true>
		<cfif IsDefined("security.accessType") and security.accessType EQ "1">
            <cfset readOnly = false>
        </cfif>

		<cfinclude template="dsp_docTemplates.cfm" />
	</cfcase>

	<cfcase value="getData">
		<cfset SearchFilter = application.wirebox.getInstance("sis_core.model.SearchFilter") >
		<cfset SearchFilter.initFromStruct(attributes) >
		<cfset sortDataVO = createobject('component','sis_core.model.SortOptions').init()>
		<cfset sortDataVO.initFromStruct(attributes) />
		<cfset DocTemplateManager = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init()>

		<cfset docLetterTemplateQry = DocTemplateManager.getAll(SearchFilter, SortDataVO)>

		<cfset dataTableRenderer = application.wirebox.getInstance(name="sis_core.model.DataTableRenderer",initArguments={dataQry=docLetterTemplateQry, columns=attributes.sColumns, metadata={sEcho:attributes.sEcho}})>
		<cfoutput>#dataTableRenderer.render()#</cfoutput>
		<cfabort>
	</cfcase>

	<cfcase value="editTemplate">
		<cfset XFA.Save = "DocTemplate.saveTemplate">
		<cfset XFA.Cancel = "DocTemplate.showTemplates">
		<cfset XFA.Delete = "DocTemplate.deleteTemplate">
		<cfset XFA.Preview	= "DocTemplate.templatePreview&isDocument=1&INSTANCE_ID=#attributes.INSTANCE_ID#">
		<cfset Edit="1">
		<cfset DocTemplateManager = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init(attributes.INSTANCE_ID)>
		<cfset DocLetterTemplateFVO = DocTemplateManager.get(attributes.docLetterTemplateCD) />
		<cfset DocType = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocType").init(DocLetterTemplateFVO.getINSTANCE_ID())>
		<cfset CompleteSignatures = DocTemplateManager.getAllSignatures()>
		<cfset availableTags = DocType.getTags()>
		<cfset documentInstances = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.Bl.DocInstances").init() />
		<cfset documentInstancesQry = documentInstances.getInstances() />
        <cfset readOnly = true>
		<cfif IsDefined("security.accessType") and security.accessType EQ "1">
            <cfset readOnly = false>
        </cfif>

		<cfinclude template="dsp_EditTemplate.cfm" />
	</cfcase>

	<cfcase value="templatePreview">
		<cfmodule template="#fusebox.rootPath##request.app.self#"
			fuseaction="DocumentLetters.viewLetter"
			instance_id="#attributes.instance_id#"
			doc_letter_template_cd="#attributes.docLetterTemplateCD#"
			XFA_close="DocTemplate.editTemplate"
			>
	</cfcase>

	<cfcase value="printPreview">
		<cfset XFA.Print = "DocTemplate.printPreview" />
		<cfset doc_snapshot_cd="-1"/>
		<cfset DocRenderer	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocRender').init(attributes.instance_id, 'PDF')>
		<cfset renderedDoc 	= DocRenderer.dummyLOA(attributes.docLetterTemplateCD)>
		<cfheader name="Content-Disposition" value="attachment;filename=AgreementLetter.pdf;application/pdf">
		<cfinclude template="../../dsp_PreviewPDF.cfm">
	</cfcase>

	<cfcase value="saveTemplate">
		<cfset DocLetterTemplateFVO = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocLetterTemplateFVO").init() />
		<cfset DocLetterTemplateFVO.initFromStruct(attributes) />
		<cfset DocTemplateManager = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init()>
		<cfset success = DocTemplateManager.save(DocLetterTemplateFVO) />
		<cfif success>
			<cfset Response.setSTATUS("success")>
			<cfset Response.setREDIRECT_URL("#request.app.myself#DocTemplate.showTemplates")>
		<cfelse>
			<cfset Response.setSTATUS("error")>
		</cfif>
		<cfoutput>
			#Response.generateJSON()#
		</cfoutput>
		<cfabort>
	</cfcase>

	<cfcase value="deleteTemplate">
		<cfset XFA.index = "DocTemplate.showTemplates">
		<cfset DocLetterTemplateFVO = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.DocLetterTemplateFVO").init() />
		<cfset DocLetterTemplateFVO.initFromStruct(attributes) />
		<cfset DocTemplateManager = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocTemplateManager").init(attributes.instance_id)>
		<cfset success = DocTemplateManager.deleteDocTemplate(DocLetterTemplateFVO) />
		<cfif success>
			<cfset Response.setSTATUS("success")>
			<cfset Response.setREDIRECT_URL(request.app.self & '?fuseaction=' & XFA.index)>
		<cfelse>
			<cfset Response.setSTATUS("error")>
		</cfif>
		<cfoutput>#Response.generateJSON()#</cfoutput><cfabort>
	</cfcase>

</cfswitch>