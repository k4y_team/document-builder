<cfif not fusebox.isCustomTag>
	<cfif not isDefined("url.ajax")>
		<cfswitch expression="#fusebox.fuseaction#">
			<cfcase value="templatePreview">
				<cfset fusebox.layoutFile="lay_preview.cfm">
				<cfset fusebox.layoutDir="">
			</cfcase>
			<cfdefaultcase>
				<cfset fusebox.layoutFile="lay_default.cfm">
				<cfset fusebox.layoutDir="">
			</cfdefaultcase>
		</cfswitch>
	<cfelse>
		<cfset fusebox.layoutfile="lay_blank.cfm">
		<cfset fusebox.layoutDir="">
	</cfif>
<cfelse>
	<cfset fusebox.layoutFile="">
</cfif>
