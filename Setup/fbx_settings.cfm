<cfset pageTitle="Document Setup">
<cfset breadCrumbCode = "DOCUMENT_SETUP">
<cfset activeOption = "DOCUMENT_SETUP">
<cfset request.securitycode = "DOCUMENT_SETUP">

<cfmodule template="#fusebox.rootPath##request.app.self#"
	fuseaction="security.setcircsec"
	funcList = "DOC_TEMPLATE,DOCUMENT_SETUP_GROUP,DOCUMENT_SETUP_SIGNATURES,DOCUMENT_SETUP_SETTINGS,DOCUMENT_REPORT_SETUP"
	linksList = "DocTemplate.showTemplates,DocLetterMap.showLetterMap,DocumentSignatures.showSignatures,DocumentSettings.showSettings,ReportSetup.showDocumentsSetup"
	funcGroup = "DOCUMENT_SETUP"
	section = "DOCUMENT_BUILDER_MODULE"
>

<cfif fusebox.fuseaction EQ "default">
	<cf_location url="#request.app.self#?fuseaction=#security.defaultLink#">
</cfif>