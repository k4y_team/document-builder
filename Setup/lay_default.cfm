<cfif Not isDefined("tabList")>
	<cfset tabList = "">
</cfif>
<cfif Not isDefined("linksList")>
	<cfset linksList = "">
</cfif>
<cfif Not isDefined("activeTab")>
	<cfset activeTab = "">
</cfif>
<cfif isDefined("security.functionsList")>
	<cfset tabList = security.functionsList>
</cfif>
<cfif isDefined("security.functionsLinks")>
	<cfset linksList = security.functionsLinks>
</cfif>

<cfoutput>#MsgBox.renderIt(clearMessage=true,header=true)#</cfoutput>
<cfif not isDefined("noTabs")>
	<cfmodule template="#fusebox.rootPath##request.app.self#"
			fuseaction="frames.showPageHeader"
			tabList = "#tabList#"
			linksList = "#linksList#"
			activeTab="#activeTab#">
<cfelse>
	<cfoutput>#fusebox.layout#</cfoutput>
</cfif>
<script type="text/javascript">
	$(function(){
		$("#LOA").find("#SETUP").addClass("active");
	});
</script>