<cfswitch expression="#fusebox.fuseaction#">
	<cfcase value="runDOCProcess">
		<cfparam name="attributes.onDemand" default="false">
		<cfset data = StructNew()>
		<cfset StructInsert(data,"doc_template_cd",attributes.selectedTemplates)>
		<cfset StructInsert(data,"training_session_cd",attributes.trainingSession)>
		<cfset StructInsert(data,"user_id",attributes.selectedUsers)>

		<cfset objectFactory = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocObjectFactory").init() />
		<cfset docProcessBL = objectFactory.create("DOC_PROCESS_COMPONENT").init()>
		<cfset docProcessBL.run(data, attributes.onDemand) />
	</cfcase>

	<cfcase value="regenerateLetterPDF">
		<cfparam name="attributes.bFullReg" default="false">
		<cfif StructKeyExists(attributes, "docSnapshotCds")>
            <cfset DOCSnapshot = CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot').init(-1) />
            <cfloop list="#attributes.docSnapshotCds#" item="doc_snapshot_cd">
                <cfset fileCD = DOCSnapshot.generatePDF(doc_snapshot_cd, attributes.bFullReg) />
            </cfloop>
		</cfif>
		<cfabort>
	</cfcase>
	<cfcase value="calculateEligibleUsers">
		<cfset DocumentTasks = createObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocumentTasks" ).init()/>
		<cfset result = DocumentTasks.calculateEligibleUsers(attributes) />
		<cfabort>
	</cfcase>

</cfswitch>