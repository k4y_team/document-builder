SET DEFINE OFF;

INSERT INTO ud_data_provider (
    data_provider_id,
    data_provider_name,
    connection_string,
    data_query,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    active
) VALUES (
    UD_DATA_PROVIDER_SEQ.nextval,
    'Documents Course Restrictions',
    'datasource={fbx_datasource}',
    'SELECT ''DB_COURSE_''  || course_cd AS access_node_code,
  course_name                    AS access_node_name,
  course_name                    AS access_node_desc,
  null       AS parent_access_node_code,
  course_cd                      AS reference_id,
  ''DB Course''                  AS label
  from course
'
,
    NULL,
    NULL,
    NULL,
    NULL,
    'Y'
);


INSERT INTO UD_ACCESS_NODE (ACCESS_NODE_ID,ACCESS_NODE_NAME,ACCESS_NODE_CODE,ACCESS_NODE_DESC,PARENT_NODE_ID,IS_DYNAMIC,CREATION_ID,CREATION_DT,INSTANCE_ID,DATA_PROVIDER_ID,DISABLED)
VALUES (
	UD_ACCESS_NODE_SEQ.nextVal,
	'Course',
	'DOCUMENT_BUILDER_PROGRAM_RESTRICTIONS',
	'Document Course Restrictions',
	(SELECT ACCESS_NODE_ID FROM UD_ACCESS_NODE WHERE ACCESS_NODE_CODE = 'DOCUMENT_BUILDER_RES'),
	'Y',
	1,
	SYSDATE,
	1,
	(SELECT DATA_PROVIDER_ID FROM UD_DATA_PROVIDER WHERE DATA_PROVIDER_NAME = 'Documents Course Restrictions'),
	'N'
);