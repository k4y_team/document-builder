INSERT INTO ud_data_provider (
    data_provider_id,
    data_provider_name,
    connection_string,
    data_query,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    active
) VALUES (
    UD_DATA_PROVIDER_SEQ.nextval,
    'Documents Location  Restrictions',
    'datasource={cbx_datasource}',
    'SELECT ''DB_LOC''
    || X.LOCATION_ID AS access_node_code,
    X.LOCATION_NAME  AS access_node_desc,
    X.LOCATION_NAME  AS access_node_name,
    X.LOCATION_ID    AS reference_id,
    DECODE(Y.LOCATION_ID,NULL,NULL,''DB_LOC'' ||  Y.LOCATION_ID)    AS parent_access_node_code,
    ''DB Location''    AS label
  FROM
    (SELECT A.ORG_CD  AS LOCATION_ID,
      A.ORG_NAME      AS LOCATION_NAME,
      B.ORG_TYPE_CD   AS LOCATION_TYPE_ID,
      B.ORG_TYPE_DESC AS LOCATION_TYPE_NAME,
      A.PARENT_CD     AS PARENT_LOCATION_ID,
      A.SITE_LABEL    AS LOCATION_CODE
    FROM ORGANIZATION A,
      ORG_TYPE B
    WHERE A.ORG_TYPE_CD         = B.ORG_TYPE_CD
    ) X,
    (SELECT A.ORG_CD  AS LOCATION_ID,
      A.ORG_NAME      AS LOCATION_NAME,
      B.ORG_TYPE_CD   AS LOCATION_TYPE_ID,
      B.ORG_TYPE_DESC AS LOCATION_TYPE_NAME,
      A.PARENT_CD     AS PARENT_LOCATION_ID,
      A.SITE_LABEL    AS LOCATION_CODE
    FROM ORGANIZATION A,
      ORG_TYPE B
    WHERE A.ORG_TYPE_CD         = B.ORG_TYPE_CD
    ) Y
  WHERE X.PARENT_LOCATION_ID = Y.LOCATION_ID (+)
 '
,
    NULL,
    NULL,
    NULL,
    NULL,
    'Y'
);


INSERT INTO UD_ACCESS_NODE (ACCESS_NODE_ID,ACCESS_NODE_NAME,ACCESS_NODE_CODE,ACCESS_NODE_DESC,PARENT_NODE_ID,IS_DYNAMIC,CREATION_ID,CREATION_DT,INSTANCE_ID,DATA_PROVIDER_ID,DISABLED)
VALUES (
	UD_ACCESS_NODE_SEQ.nextVal,
	'Teaching Location',
	'DOCUMENT_BUILDER_LOCATION_RESTRICTIONS',
	'Document Builder Location  Restrictions',
	(SELECT ACCESS_NODE_ID FROM UD_ACCESS_NODE WHERE ACCESS_NODE_CODE = 'DOCUMENT_BUILDER_RES'),
	'Y',
	1,
	SYSDATE,
	1,
	(SELECT DATA_PROVIDER_ID FROM UD_DATA_PROVIDER WHERE DATA_PROVIDER_NAME = 'Documents Location  Restrictions'),
	'N'
);