CREATE TABLE DOC_SNAPSHOT_EVAL_PERIOD
   ( DOC_SNAPSHOT_CD NUMBER,
     DOC_SETUP_ID NUMBER,
     EVALUATION_PERIOD VARCHAR2(200 BYTE) 
   ); 
MERGE INTO doc_snapshot_eval_period d USING
    ( SELECT
        a.doc_snapshot_cd,
        a.doc_setup_id,
        (
            SELECT
                min_start_dt || '/' || max_end_dt
            FROM
                TABLE ( pljson_table.json_table(
                    d.value,
                    pljson_varray(
                        '[*].MIN_START_DT',
                        '[*].MAX_END_DT'
                    ),
                    pljson_varray(
                        'MIN_START_DT',
                        'MAX_END_DT'
                    ),
                    table_mode   => 'NESTED'
                ) )
        ) evaluation_period
    FROM
        doc_snapshot_base a,
        doc_snapshot_data d
    WHERE
            a.doc_snapshot_cd = d.doc_snapshot_cd
        AND
            d.key = 'TOTAL_SCORES'
    )
s ON (
    s.doc_snapshot_cd = d.doc_snapshot_cd
) WHEN NOT MATCHED THEN 
 INSERT ( d.doc_snapshot_cd,d.doc_setup_id,d.evaluation_period ) 
 VALUES ( s.doc_snapshot_cd,s.doc_setup_id,s.evaluation_period);