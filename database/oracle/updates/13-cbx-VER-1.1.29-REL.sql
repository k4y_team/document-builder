alter table doc_instance ADD HAS_SETUP	VARCHAR2(1 BYTE) DEFAULT ('Y');
UPDATE  doc_instance set HAS_SETUP ='N' where INSTANCE_CODE='DAILY_ASSESMENT';
update doc_instance set tags=tags ||',HEADER_INFO_GRAD_STUDIES,HEADER_INFO_MIDWIFERY,HEADER_INFO_NURSING,HEADER_INFO_PT_TES,HEADER_INFO_OT_TES' where instance_code='PG_TES_REPORT';

SET DEFINE OFF;

INSERT INTO ud_data_provider (
    data_provider_id,
    data_provider_name,
    connection_string,
    data_query,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    active
) VALUES (
    UD_DATA_PROVIDER_SEQ.nextval,
    'Documents Type Restrictions',
    'datasource={cbx_datasource}',
    'SELECT ''DB_INSTANCE_''  || instance_id AS access_node_code,
  instance_name                    AS access_node_name,
  instance_name                    AS access_node_desc,
  null       AS parent_access_node_code,
  instance_id                      AS reference_id,
  ''DB INSTANCE''                  AS label
  from doc_instance where has_setup=''Y''
'
,
    NULL,
    NULL,
    NULL,
    NULL,
    'Y'
);



INSERT INTO UD_ACCESS_NODE (ACCESS_NODE_ID,ACCESS_NODE_NAME,ACCESS_NODE_CODE,ACCESS_NODE_DESC,PARENT_NODE_ID,IS_DYNAMIC,CREATION_ID,CREATION_DT,INSTANCE_ID,DATA_PROVIDER_ID,DISABLED)
VALUES (
	UD_ACCESS_NODE_SEQ.nextVal,
	'Document Type',
	'DOCUMENT_BUILDER_TYPE_RESTRICTIONS',
	'Document Type Restrictions',
	(SELECT ACCESS_NODE_ID FROM UD_ACCESS_NODE WHERE ACCESS_NODE_CODE = 'DOCUMENT_BUILDER_RES'),
	'Y',
	1,
	SYSDATE,
	1,
	(SELECT DATA_PROVIDER_ID FROM UD_DATA_PROVIDER WHERE DATA_PROVIDER_NAME = 'Documents Type Restrictions'),
	'N'
);
