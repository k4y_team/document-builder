INSERT INTO ud_access_node (
    access_node_id,
    access_node_name,
    access_node_code,
    access_node_desc,
    parent_node_id,
    is_dynamic,
    reference_id,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    instance_id,
    data_provider_id,
    disabled,
    label

) VALUES (
    (SELECT MAX(ACCESS_NODE_ID)+1 FROM UD_ACCESS_NODE),
    'Document Setup Dates',
    'DOCUMENT_SETUP_DATES',
    'Document Setup Dates',
    (select ACCESS_NODE_ID from UD_ACCESS_NODE where access_node_code='DOCUMENT_SETUP'),
    'N',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL,
    'N',
    NULL
);

INSERT INTO ud_access_node_types (
    access_node_id,
    access_type_id
) VALUES (
    (
        SELECT
            access_node_id
        FROM
            ud_access_node
        WHERE
            access_node_code = 'DOCUMENT_SETUP_DATES'
    ),
    (
        SELECT
            access_type_id
        FROM
            ud_access_type
        WHERE
            access_type_code = 'READ-ONLY'
    )
);

INSERT INTO ud_access_node_types (
    access_node_id,
    access_type_id
) VALUES (
    (
        SELECT
            access_node_id
        FROM
            ud_access_node
        WHERE
            access_node_code = 'DOCUMENT_SETUP_DATES'
    ),
    (
        SELECT
            access_type_id
        FROM
            ud_access_type
        WHERE
            access_type_code = 'READ_WRITE'
    )
);