create or replace function CLOBEscape(pStr in clob) return clob is
  vResult clob := pStr;
begin
  if vResult is not null then
    vResult := replace(vResult, '\ ', '\\');
    vResult := replace(vResult, '"', '\"');
    vResult := replace(vResult, '/', '\/');
    vResult := replace(vResult, chr(8), '\b');
    vResult := replace(vResult, chr(9), '\t');
    vResult := replace(vResult, chr(10), '\n');
    vResult := replace(vResult, chr(12), '\f');
    vResult := replace(vResult, chr(13), '\r');
  end if;
  return vResult;
end;