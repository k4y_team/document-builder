INSERT INTO ud_access_node (
    access_node_id,
    access_node_name,
    access_node_code,
    access_node_desc,
    parent_node_id,
    is_dynamic,
    reference_id,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    instance_id,
    data_provider_id,
    disabled,
    label

) VALUES (
    (SELECT MAX(ACCESS_NODE_ID)+1 FROM UD_ACCESS_NODE),
    'Document Builder',
    'DOCUMENT_BUILDER_MODULE',
    'Document Builder',
    1,
    'N',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL,
    'N',
    NULL
);

INSERT INTO ud_access_node (
    access_node_id,
    access_node_name,
    access_node_code,
    access_node_desc,
    parent_node_id,
    is_dynamic,
    reference_id,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    instance_id,
    data_provider_id,
    disabled,
    label

) VALUES (
    (SELECT MAX(ACCESS_NODE_ID)+1 FROM UD_ACCESS_NODE),
    'Document Setup',
    'DOCUMENT_SETUP',
    'Document Setup',
    (select ACCESS_NODE_ID from UD_ACCESS_NODE where access_node_code='DOCUMENT_BUILDER_MODULE'),
    'N',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL,
    'N',
    NULL
);
INSERT INTO ud_access_node (
    access_node_id,
    access_node_name,
    access_node_code,
    access_node_desc,
    parent_node_id,
    is_dynamic,
    reference_id,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    instance_id,
    data_provider_id,
    disabled,
    label

) VALUES (
    (SELECT MAX(ACCESS_NODE_ID)+1 FROM UD_ACCESS_NODE),
    'Document Template',
    'DOC_TEMPLATE',
    'Document TEmplate',
    (select ACCESS_NODE_ID from UD_ACCESS_NODE where access_node_code='DOCUMENT_SETUP'),
    'N',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL,
    'N',
    NULL
);

INSERT INTO ud_access_node (
    access_node_id,
    access_node_name,
    access_node_code,
    access_node_desc,
    parent_node_id,
    is_dynamic,
    reference_id,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    instance_id,
    data_provider_id,
    disabled,
    label

) VALUES (
    (SELECT MAX(ACCESS_NODE_ID)+1 FROM UD_ACCESS_NODE),
    'Document Setup',
    'DOCUMENT_REPORT_SETUP',
    'Document Setup',
    (select ACCESS_NODE_ID from UD_ACCESS_NODE where access_node_code='DOCUMENT_SETUP'),
    'N',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL,
    'N',
    NULL
);


INSERT INTO ud_access_node (
    access_node_id,
    access_node_name,
    access_node_code,
    access_node_desc,
    parent_node_id,
    is_dynamic,
    reference_id,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    instance_id,
    data_provider_id,
    disabled,
    label

) VALUES (
    (SELECT MAX(ACCESS_NODE_ID)+1 FROM UD_ACCESS_NODE),
    'Documents Report',
    'DOC_REPORT',
    'Documents Report',
    (select ACCESS_NODE_ID from UD_ACCESS_NODE where access_node_code='DOCUMENT_BUILDER_MODULE'),
    'N',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL,
    'N',
    NULL
);

INSERT INTO ud_access_node (
    access_node_id,
    access_node_name,
    access_node_code,
    access_node_desc,
    parent_node_id,
    is_dynamic,
    reference_id,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    instance_id,
    data_provider_id,
    disabled,
    label

) VALUES (
    (SELECT MAX(ACCESS_NODE_ID)+1 FROM UD_ACCESS_NODE),
    'View Documents',
    'VIEW_DOC_REPORT',
    'View Documents',
    (select ACCESS_NODE_ID from UD_ACCESS_NODE where access_node_code='DOC_REPORT'),
    'N',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    1,
    NULL,
    'N',
    NULL
);