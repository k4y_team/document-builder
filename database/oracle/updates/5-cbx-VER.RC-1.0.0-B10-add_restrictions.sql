INSERT INTO ud_access_node_types (
    access_node_id,
    access_type_id
) VALUES (
    (
        SELECT
            access_node_id
        FROM
            ud_access_node
        WHERE
            access_node_code = 'DOCUMENT_SETUP'
    ),
    (
        SELECT
            access_type_id
        FROM
            ud_access_type
        WHERE
            access_type_code = 'READ-ONLY'
    )
);

INSERT INTO ud_access_node_types (
    access_node_id,
    access_type_id
) VALUES (
    (
        SELECT
            access_node_id
        FROM
            ud_access_node
        WHERE
            access_node_code = 'DOCUMENT_SETUP'
    ),
    (
        SELECT
            access_type_id
        FROM
            ud_access_type
        WHERE
            access_type_code = 'READ_WRITE'
    )
);
INSERT INTO ud_access_node_types (
    access_node_id,
    access_type_id
) VALUES (
    (
        SELECT
            access_node_id
        FROM
            ud_access_node
        WHERE
            access_node_code = 'DOC_TEMPLATE'
    ),
    (
        SELECT
            access_type_id
        FROM
            ud_access_type
        WHERE
            access_type_code = 'READ-ONLY'
    )
);

INSERT INTO ud_access_node_types (
    access_node_id,
    access_type_id
) VALUES (
    (
        SELECT
            access_node_id
        FROM
            ud_access_node
        WHERE
            access_node_code = 'DOC_TEMPLATE'
    ),
    (
        SELECT
            access_type_id
        FROM
            ud_access_type
        WHERE
            access_type_code = 'READ_WRITE'
    )
);