INSERT INTO doc_instance (
    instance_id,
    instance_name,
    access_node,
    restriction_nodes,
    template_entities,
    header_content,
    refers_to,
    tags,
    entity_code,
    instance_code,
    instance_config
) VALUES (
    (select max(instance_id)+1 from doc_instance),
    'DAILY_ASSESMENT',
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    'HEADER_INFO,NR_EVALS,NR_STUDENTS,RATING_SCALES,COMMENTS,TES_SCORE',
    NULL,
    'DAILY_ASSESMENT',
    '{"Filter_Entity":"","CONFIG_DATA_PROVIDER_ENTITY":"AggConfigDataProvider","DOCUMENT_DATA_PROVIDER_ENTITY":"","DOCUMENT_RENDERER_ENTITY":"AGGRenderer"}'

);

INSERT INTO doc_letter_template (
    doc_letter_template_cd,
    instance_id,
    letter_template_code,
    letter_template_name,
    status,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    instructions
) VALUES (
    (select max(doc_letter_template_cd)+1 from doc_letter_template),
    (select instance_id from doc_instance where instance_name='DAILY_ASSESMENT'),
    'Daily Assesments',
    'Daily Assesments',
    'ACTIVE',
    1,
    TO_DATE('30-APR-20','DD-MON-RR'),
    1,
    TO_DATE('02-JUN-20','DD-MON-RR'),
    NULL
);
