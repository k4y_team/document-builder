SET DEFINE OFF;

INSERT INTO ud_data_provider (
    data_provider_id,
    data_provider_name,
    connection_string,
    data_query,
    creation_id,
    creation_dt,
    modification_id,
    modification_dt,
    active
) VALUES (
    UD_DATA_PROVIDER_SEQ.nextval,
    'Documents Program Restrictions',
    'datasource={pg_datasource}',
    'SELECT ''DB_PROG_''  || program_id AS access_node_code,
  program_name                    AS access_node_name,
  program_name                    AS access_node_desc,
  null       AS parent_access_node_code,
  program_id                      AS reference_id,
  ''DB Program''                  AS label
  from rs_program 
'
,
    NULL,
    NULL,
    NULL,
    NULL,
    'Y'
);
INSERT INTO UD_ACCESS_NODE (ACCESS_NODE_ID,ACCESS_NODE_NAME,ACCESS_NODE_CODE,ACCESS_NODE_DESC,PARENT_NODE_ID,IS_DYNAMIC,CREATION_ID,CREATION_DT,INSTANCE_ID,DATA_PROVIDER_ID,DISABLED)
VALUES (
	UD_ACCESS_NODE_SEQ.nextVal,
	'Documents',
	'DOCUMENT_BUILDER_RES',
	'Document Restrictions',
	(SELECT ACCESS_NODE_ID FROM UD_ACCESS_NODE WHERE ACCESS_NODE_CODE = 'DATA_RESTRICTIONS'),
	'N',
	1,
	SYSDATE,
	1,
	null,
	'N'
);

INSERT INTO UD_ACCESS_NODE (ACCESS_NODE_ID,ACCESS_NODE_NAME,ACCESS_NODE_CODE,ACCESS_NODE_DESC,PARENT_NODE_ID,IS_DYNAMIC,CREATION_ID,CREATION_DT,INSTANCE_ID,DATA_PROVIDER_ID,DISABLED)
VALUES (
	UD_ACCESS_NODE_SEQ.nextVal,
	'Programs',
	'DOCUMENT_BUILDER_PROGRAM_RESTRICTIONS',
	'Document Programs Restrictions',
	(SELECT ACCESS_NODE_ID FROM UD_ACCESS_NODE WHERE ACCESS_NODE_CODE = 'DOCUMENT_BUILDER_RES'),
	'Y',
	1,
	SYSDATE,
	1,
	(SELECT DATA_PROVIDER_ID FROM UD_DATA_PROVIDER WHERE DATA_PROVIDER_NAME = 'Documents Program Restrictions'),
	'N'
);