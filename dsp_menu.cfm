<cfoutput>
	<div class="menu" id="DOCUMENT_BUILDER">
		<ul class="navigation">
			<li>
				<a href="#request.settings.mainApplicationURL#">
					<span class="isw-home"></span>
					<span class="text">
						Home
					</span>
				</a>
			</li>
			<cfif getGroup('DOCUMENT_SETUP')>
				<li id="SETUP" <cfif attributes.activeOption eq "DOCUMENT_SETUP">class="active"</cfif>>
					<a href="#request.app.self#?fuseaction=DocSetup.default">
						<span class="isw-loa-setup"></span>
						<span class="text">
							Setup
						</span>
					</a>
				</li>
			</cfif>
			<cfif getGroup('DOC_REPORT')>
				<li id="REPORT" <cfif attributes.activeOption eq "DOC_REPORT">class="active"</cfif>>
					<a href="#request.app.self#?fuseaction=DocReport.showDocumentsFilter">
						<span class="isw-loa-report"></span>
						<span class="text">
						 Documents
						</span>
					</a>
				</li>
			</cfif>
		</ul>
	</div>
</cfoutput>