<cfif NOT fusebox.isCustomTag>
	<cfswitch expression="#fusebox.fuseaction#">
		<cfcase value="showresults,confirmResults">
			<cfset fusebox.layoutFile="lay_blank.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="LoaPreview">
			<cfset fusebox.layoutFile="lay_preview.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="showdocuments">
			<cfset fusebox.layoutFile="lay_blank.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="loadtraineedocuments">
			<cfset fusebox.layoutFile="lay_blank.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="loadactivedocuments">
			<cfset fusebox.layoutFile="lay_blank.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="templatePreview">
			<cfset fusebox.layoutFile="lay_preview.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="emailPreview">
			<cfset fusebox.layoutFile="lay_blank.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="getLastProcess">
			<cfset fusebox.layoutFile="lay_blank.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="LoaSignPDF">
			<cfset fusebox.layoutFile="lay_preview.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfcase value="SignLOA">
			<cfset fusebox.layoutFile="lay_default.cfm">
			<cfset fusebox.layoutDir="">
		</cfcase>
		<cfdefaultcase>
			<cfset fusebox.layoutFile="lay_default.cfm">
			<cfset fusebox.layoutDir="">
		</cfdefaultcase>
	</cfswitch>

<cfelse>
	<cfset fusebox.layoutFile="">
	<cfset fusebox.layoutDir="">
</cfif>