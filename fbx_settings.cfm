<cfparam name="activeOption" default="">
<cfset modulePath = GetDirectoryFromPath(GetBaseTemplatePath()) & "modules\DocumentBuilder\">
<cfset breadCrumbCode = "DOCUMENT_BUILDER_MODULE">

<cfset MsgBox = application.wirebox.getInstance(dsl="coldbox:myplugin:MsgBox").init(application.cbController)>
<cfset Response = application.wirebox.getInstance("sis_core.model.Response").init()>
<cfset moduleConfig = application.ModuleManager.getModuleConfig("DOCUMENT_BUILDER_MODULE") />
<cfset request.dictionary = moduleConfig.getRBSettings().ALIAS>
<cfset request.securitycode = "DOCUMENT_BUILDER_MODULE">
<cfparam name="attributes.instance_id" default="5">
<cfset instance_id = attributes.instance_id />