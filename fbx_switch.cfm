<cfswitch expression="#fusebox.fuseaction#">

    <cfcase value="start">

		<cfset userID = session.user_id>
		<cfset section = "DOCUMENT_BUILDER_MODULE">
		<cfinclude template="#fusebox.rootpath#Security/qry_getUserGroups.cfm">
		<cfinclude template="#fusebox.rootpath#Security/qry_getUserFunctions.cfm">
		<cfinclude template="#fusebox.rootpath#Security/act_checkGroups.cfm">
		<cfinclude template="#fusebox.rootpath#Security/act_checkFunctions.cfm">

		<cfif getGroup('DOC_REPORT')>
			<cf_location url="#request.app.self#?fuseaction=DocReport.showDocumentsFilter">
		</cfif>

		<cfif getGroup('DOCUMENT_SETUP')>
			<cf_location url="#request.app.self#?fuseaction=DocSetup.default">
		</cfif>



    </cfcase>

    <cfcase value="buildMenu">
		<cfset userID = session.user_id>
		<cfset section = "DOCUMENT_BUILDER_MODULE">
		<cfinclude template="#fusebox.rootpath#Security/qry_getUserSections.cfm">
		<cfinclude template="#fusebox.rootpath#Security/qry_getUserGroups.cfm">
		<cfinclude template="#fusebox.rootpath#Security/qry_getUserFunctions.cfm">
		<cfinclude template="#fusebox.rootpath#Security/act_checkSection.cfm">
		<cfinclude template="#fusebox.rootpath#Security/act_checkGroups.cfm">
		<cfinclude template="#fusebox.rootpath#Security/act_checkFunctions.cfm">

		<cfinclude template="dsp_menu.cfm">
	</cfcase>

	<cfcase value="LoaSignPDF">
		<cfset docSnapshot 		= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot').init(attributes.instance_id)>
		<cfset docSnapshotQry 	= docSnapshot.get(attributes.doc_snapshot_cd) />
		<cfset DocRenderer	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocRender').init(attributes.instance_id, 'PDF')>
		<cfset renderedDoc 	= DocRenderer.renderLOA(attributes.doc_snapshot_cd)>
		<cfset stdInfo = application.wirebox.getInstance("InstanceFactory@TraineeRegistration").create("Student") />
		<cfset doaInstances = CreateObject("component","medsisOld.modules.DocumentBuilder.Model.BL.DocInstances").init()>
		<cfset documentInstancesQry = doaInstances.getInstanceInfo(attributes.instance_id)>
		<cfset qStudentInfo = stdInfo.getByID(docSnapshotQry.student_id)>
		<cfset fileName = "#documentInstancesQry.INSTANCE_NAME#_#Replace(qStudentInfo.LAST_NAME,' ','','all')#_#Replace(qStudentInfo.FIRST_NAME,' ','','all')#_#qStudentInfo.OPHRDC#.pdf" />
		<cfheader name="Content-Disposition" value="attachment;filename=#fileName#;application/pdf">
		<cfinclude template="dsp_PreviewPDF.cfm">
	</cfcase>

	<cfcase value="SignLOA">
		<cfset pageTitle = "LOA Sign off">
		<cfset XFA.Print = "LOA.LoaSignPDF" />

		<cfset fromSign = true />
		<cfinclude template="dsp_TraineeSingLOA.cfm">
	</cfcase>

	<cfcase value="LoaPreview">
		<cfset pageTitle = "LOA Sign off">
		<cfset XFA.Print = "LOA.LoaSignPDF" />

		<cfset DocRenderer	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DocRender').init(attributes.instance_id)>
		<cfset renderedDoc 	= DocRenderer.renderLOA(attributes.doc_snapshot_cd)>

		<cfinclude template="dsp_LetterOfAppContent.cfm">
	</cfcase>

	<cfcase value="loaSubmitSignature">
		<cfset pageTitle = "LOA Sign">
		<cfset SignFVO 				= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.SignFVO').init()>
		<cfset docSnapshot 	= CreateObject('component','medsisOld.modules.DocumentBuilder.Model.BL.DOCSnapshot').init(attributes.instance_id)>

		<cfset externalService = CreateObject("component", "medsisOld.modules.DocumentBuilder.Model.BL.ExternalService").init() />
		<cfset currentTrainingSession = externalService.getCurrentTrainingSession() />

		<cfset signVal = SignFVO.initfromform2(form)>
		<cfset SignFVO.setSTUDENT_ID(application.sessionManager.getStudentId())>

		<cfset serv = docSnapshot.traineeSignOff(SignFVO)>
		<cfset fields="&msg=#SignFVO.getFormMsg().getMessages()[1].msg#&msgType=#SignFVO.getFormMsg().getMessages()[1].type#">

		<cfmodule template="#fusebox.rootPath#/Student/Alert/fbx_switch.cfm"
				trainingSessionCD="#currentTrainingSession.TRAINING_SESSION_CD#"
				studentID="#application.sessionManager.getStudentId()#"
				fuseaction="calcStdAlert">

		<cf_post_location url="#request.app.self#?fuseaction=StdLOA.showLOAprofile" fields="#fields#">
	</cfcase>

</cfswitch>