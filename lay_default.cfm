<cfimport prefix="k4y" taglib="/Medsis/ctags">
<cfparam name="breadCrumbCode" default="">
<cfoutput>
	<cfset appConfiguration = application.cbController.getSetting('applicationConfiguration')>
	<aside id="left-panel" class="">
		<cfset userSessionData = request.cbcontroller.getPlugin('SessionStorage').getVar('LoggedInUser')>
		<div class="login-info">
			<span>
				<img src="#request.settings.mainApplicationURL#includes/img/icons/user-icon.png" alt="">
				<div class="user-name">
					<div class="aligned-user-name">
						#userSessionData.name#
					</div>
				</div>
			</span>
		</div>
		<cfmodule template="#fusebox.rootPath##request.app.self#"
				  fuseaction="DocumentBuilder.buildMenu"
				  activeOption="#activeOption#">
		<span class="minifyme">
			<i class="ico ico-arrow-circle-left hit"></i>
		</span>
	</aside>

	<div class="wrapper">
		<div class="content" id="content">
			<div id="ribbon">
				<cfmodule template="#fusebox.rootPath##request.app.self#"
					fuseaction="Modules.breadCrumb"
					nodeCode="#breadCrumbCode#"
					XMLObj="#moduleConfig.getXmlObj()#"
					rbAlias="#moduleConfig.getRBSettings().ALIAS#">
			</div>
		    <div class="workplace">
		    	<div id="message-area"></div>
		    	#fusebox.layout#
				<div class="copyright">
					<cfset copyrightYear = Year(Now())/>
					<cfif appConfiguration.copyright.startYear NEQ Year(Now())>
						<cfset copyrightYear = appConfiguration.copyright.startYear & " - " & Year(Now())/>
					</cfif>
					&copy; #copyrightYear# <a href="#appConfiguration.copyright.url#" target="_blank">#appConfiguration.copyright.author#</a>. All rights reserved.
				</div>
		    </div>
	    </div>
	</div>
</cfoutput>